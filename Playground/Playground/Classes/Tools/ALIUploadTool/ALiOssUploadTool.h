//
//  ALiOssUploadTool.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/10.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, UploadImageState) {
    UploadImageFailed   = 0,
    UploadImageSuccess  = 1
};

@interface ALiOssUploadTool : NSObject

/**
 异步请求上传单张图片

 @param image 图片
 @param bucketName bucket名称
 @param object_key 单张图片的地址
 @param object_keys nil
 @param complete 完成状态
 */
+ (void)asyncUploadImage:(UIImage *)image WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys complete:(void(^)(NSArray<NSString *> *names,UploadImageState state))complete;

/**
 同步请求上传单张图片
 
 @param image 图片
 @param bucketName bucket名称
 @param object_key 单张图片的地址
 @param object_keys nil
 @param complete 完成状态
 */
+ (void)syncUploadImage:(UIImage *)image  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys complete:(void(^)(NSArray<NSString *> *names,UploadImageState state))complete;

/**
 异步请求上传多张图片
 
 @param images 图片数组
 @param bucketName bucket名称
 @param object_key nil
 @param object_keys 多张图片地址的数组
 @param complete 完成状态
 */
+ (void)asyncUploadImages:(NSArray<UIImage *> *)images  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete;

/**
 同步请求上传多张图片
 
 @param images 图片数组
 @param bucketName bucket名称
 @param object_key nil
 @param object_keys 多张图片地址的数组
 @param complete 完成状态
 */
+ (void)syncUploadImages:(NSArray<UIImage *> *)images  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete;

/**
 同步请求上传多张图片
 
 @param images 图片数组
 @param bucketName bucket名称
 @param object_key nil
 @param object_keys 多张图片地址的数组
 @param uploadProgress 当前图片上传状态
 @param complete 完成状态
 */
+ (void)uploadImages:(NSArray<UIImage *> *)images  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys isAsync:(BOOL)isAsync progress:(void (^)(BOOL  uploadProgress))uploadProgress complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete;

@end
