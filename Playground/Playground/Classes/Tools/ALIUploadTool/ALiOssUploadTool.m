//
//  ALiOssUploadTool.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/10.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "ALiOssUploadTool.h"

#import <AliyunOSSiOS/OSSService.h>
#import <AliyunOSSiOS/OSSCompat.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonHMAC.h>

@interface ALiOssUploadTool()
{
    OSSClient * client;
}

@end

@implementation ALiOssUploadTool

static NSString *const endPoint = @"https://oss-cn-zhangjiakou.aliyuncs.com";

+ (void)asyncUploadImage:(UIImage *)image WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys complete:(void(^)(NSArray<NSString *> *names,UploadImageState state))complete
{
    [self uploadImages:@[image]  WithBucketName:bucketName WithObject_key:object_key WithObject_keys:nil isAsync:YES complete:complete];
}

+ (void)syncUploadImage:(UIImage *)image  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys complete:(void(^)(NSArray<NSString *> *names,UploadImageState state))complete
{
    [self uploadImages:@[image]  WithBucketName:bucketName WithObject_key:object_key WithObject_keys:nil isAsync:NO complete:complete];
}

+ (void)asyncUploadImages:(NSArray<UIImage *> *)images  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete
{
    [self uploadImages:images  WithBucketName:bucketName WithObject_key:nil WithObject_keys:object_keys isAsync:YES complete:complete];
}

+ (void)syncUploadImages:(NSArray<UIImage *> *)images  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete
{
    [self uploadImages:images  WithBucketName:bucketName WithObject_key:nil WithObject_keys:object_keys isAsync:NO complete:complete];
}

+ (void)uploadImages:(NSArray<UIImage *> *)images  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys isAsync:(BOOL)isAsync complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete
{
    
    NSString * AccessKeyId      = [GFStaticData getObjectForKey:ALi_AccessKeyId_Image];
    NSString * SecurityToken    = [GFStaticData getObjectForKey:ALi_SecurityToken_Image];
    NSString * AccessKeySecret  = [GFStaticData getObjectForKey:ALi_AccessKeySecret_Image];

    id<OSSCredentialProvider> credential = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:AccessKeyId secretKeyId:AccessKeySecret securityToken:SecurityToken];
    
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:endPoint credentialProvider:credential];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.maxConcurrentOperationCount = images.count;
    
    NSMutableArray *callBackNames = [NSMutableArray array];
    int i = 0;
    for (UIImage *image in images) {
        if (image) {
            NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                
                //任务执行
                OSSPutObjectRequest * put = [OSSPutObjectRequest new];
                put.bucketName = bucketName;
                
                if (object_key.length >0) {
                    put.objectKey = object_key;
                }else{
                    put.objectKey = object_keys[i];
                }
                
                
                put.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
//                    NSLog(@"---- %lld, ----%lld", totalBytesSent, totalBytesExpectedToSend);
                };
                
                NSData *data = UIImageJPEGRepresentation(image, 0.3);
                put.uploadingData = data;
                
                OSSTask * putTask = [client putObject:put];
                [putTask waitUntilFinished]; // 阻塞直到上传完成
                if (!putTask.error) {
                    NSLog(@"upload object success!");
                } else {
                    NSLog(@"upload object failed, error: %@" , putTask.error);
                }
                if (isAsync) {
                    if (image == images.lastObject) {
                        NSLog(@"upload object finished!");
                        if (complete) {
                            complete([NSArray arrayWithArray:object_keys] ,UploadImageSuccess);
                        }
                    }
                }
            }];
            if (queue.operations.count != 0) {
                [operation addDependency:queue.operations.lastObject];
            }
            [queue addOperation:operation];
        }
        i++;
    }
    if (!isAsync) {
        [queue waitUntilAllOperationsAreFinished];
        NSLog(@"haha");
        if (complete) {
            if (complete) {
                complete([NSArray arrayWithArray:callBackNames], UploadImageSuccess);
            }
        }
    }
}

+ (void)uploadImages:(NSArray<UIImage *> *)images  WithBucketName:(NSString *)bucketName WithObject_key:(NSString *)object_key WithObject_keys:(NSArray *)object_keys isAsync:(BOOL)isAsync progress:(void (^)(BOOL  uploadProgress))uploadProgress complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete
{
    
    NSString * AccessKeyId      = [GFStaticData getObjectForKey:ALi_AccessKeyId_Image];
    NSString * SecurityToken    = [GFStaticData getObjectForKey:ALi_SecurityToken_Image];
    NSString * AccessKeySecret  = [GFStaticData getObjectForKey:ALi_AccessKeySecret_Image];
    
    id<OSSCredentialProvider> credential = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:AccessKeyId secretKeyId:AccessKeySecret securityToken:SecurityToken];
    
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:endPoint credentialProvider:credential];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.maxConcurrentOperationCount = images.count;
  
    NSMutableArray *callBackNames = [NSMutableArray array];
    int i = 0;
    for (UIImage *image in images) {
        if (image) {
            NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                
                //任务执行
                OSSPutObjectRequest * put = [OSSPutObjectRequest new];
                put.bucketName = bucketName;
                put.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
                    
                    if ((float)totalBytesSent / totalBytesExpectedToSend == 1) {
                        uploadProgress(YES);
                    }
                };
                
                if (object_key.length >0) {
                    put.objectKey = object_key;
                }else{
                    put.objectKey = object_keys[i];
                }
                
                NSData *data = UIImageJPEGRepresentation(image, 0.3);
                put.uploadingData = data;
                
                OSSTask * putTask = [client putObject:put];
                [putTask waitUntilFinished]; // 阻塞直到上传完成
                if (!putTask.error) {
                    NSLog(@"upload object success!");
                } else {
                    uploadProgress(NO);
                    NSLog(@"upload object failed, error: %@" , putTask.error);
                }
                if (isAsync) {
                    if (image == images.lastObject) {
                        NSLog(@"upload object finished!");
                        if (complete) {
                            complete([NSArray arrayWithArray:object_keys] ,UploadImageSuccess);
                        }
                    }
                }
            }];
            if (queue.operations.count != 0) {
                [operation addDependency:queue.operations.lastObject];
            }
            [queue addOperation:operation];
        }
        i++;
    }
    if (!isAsync) {
        [queue waitUntilAllOperationsAreFinished];
        NSLog(@"haha");
        if (complete) {
            if (complete) {
                complete([NSArray arrayWithArray:callBackNames], UploadImageSuccess);
            }
        }
    }
}


@end
