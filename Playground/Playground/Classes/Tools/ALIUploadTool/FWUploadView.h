//
//  FWUploadView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWUploadViewDelegate <NSObject>

- (void)closeButtonOnClick;

@end

@interface FWUploadView : UIView

@property(nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIImageView * coverImage;
@property (nonatomic, strong) UIImageView * iconImage;
@property(nonatomic, strong) UILabel * tipLabel;
@property(nonatomic, strong) UIProgressView * progressView;
@property (nonatomic, strong) UIButton * closeButton;
@property (nonatomic, weak) id <FWUploadViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
