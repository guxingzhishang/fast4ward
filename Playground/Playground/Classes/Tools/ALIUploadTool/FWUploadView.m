//
//  FWUploadView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUploadView.h"

@implementation FWUploadView
@synthesize bgView;
@synthesize coverImage;
@synthesize iconImage;
@synthesize tipLabel;
@synthesize progressView;
@synthesize closeButton;

- (id)init{
    
    if (self = [super init]){
        if (!bgView){
            bgView = [[UIView alloc] init];
        }
        bgView.frame = CGRectMake(0, 0, SCREEN_WIDTH,50);
        bgView.backgroundColor = [FWViewBackgroundColor_FFFFFF colorWithAlphaComponent:1];
        [self addSubview:bgView];
        
        if (!coverImage) {
            coverImage = [[UIImageView alloc] init];
        }
        coverImage.layer.cornerRadius = 2;
        coverImage.layer.masksToBounds = YES;
        coverImage.frame = CGRectMake(14, 11, 30, 30);
        coverImage.image = [UIImage imageNamed:@"placeholder"];
        coverImage.contentMode = UIViewContentModeScaleAspectFill;
        coverImage.clipsToBounds = YES;
        [bgView addSubview:self.coverImage];
        
        if (!iconImage) {
            iconImage = [[UIImageView alloc] init];
        }
        iconImage.frame = CGRectMake((CGRectGetWidth(coverImage.frame)-20)/2, (CGRectGetHeight(coverImage.frame)-20)/2, 20, 20);
        iconImage.image = [UIImage imageNamed:@"card_play"];
        [coverImage addSubview:self.iconImage];
        iconImage.hidden = YES;
        
        
        if (!tipLabel){
            tipLabel = [[UILabel alloc] init];
        }
        tipLabel.text = @"上传中";
        tipLabel.textColor = FWTextColor_272727;
        tipLabel.textAlignment = NSTextAlignmentLeft;
        tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        [bgView addSubview:tipLabel];
        [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(coverImage);
            make.left.mas_equalTo(coverImage.mas_right).mas_offset(10);
            make.right.mas_equalTo(bgView).mas_offset(-60);
            make.height.mas_equalTo(40);
            make.width.mas_greaterThanOrEqualTo(100);
        }];

        
        if (!progressView){
            progressView = [[UIProgressView alloc] init];
        }
        progressView.progressTintColor = [UIColor colorWithHexString:@"#515151"];
        progressView.trackTintColor = FWColorWihtAlpha(@"#68779E", 0.1);
        progressView.frame = CGRectMake(CGRectGetMinX(coverImage.frame), CGRectGetMaxY(coverImage.frame)+5, CGRectGetWidth(bgView.frame)-28,3);
        progressView.progressViewStyle = UIProgressViewStyleDefault;
        [bgView addSubview:progressView];
        
//        if (!closeButton) {
//            closeButton = [[UIButton alloc] init];
//        }
//        [closeButton setImage:[UIImage imageNamed:@"upload_close"] forState:UIControlStateNormal];
//        [closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
//        [bgView addSubview:closeButton];
//        [closeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.size.mas_equalTo(CGSizeMake(50, 50));
//            make.centerY.mas_equalTo(tipLabel);
//            make.right.mas_equalTo(bgView);
//        }];
    }
    
    return self;
}

#pragma mark - > 关闭
- (void)closeButtonClick{
//    if ([self.delegate respondsToSelector:@selector(closeButtonOnClick)]) {
//        [self.delegate closeButtonOnClick];
//    }
}

@end
