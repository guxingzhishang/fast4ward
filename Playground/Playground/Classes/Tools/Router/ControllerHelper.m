//
//  ControllerHelper.m
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/19.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#import "ControllerHelper.h"

@implementation ControllerHelper
+(UINavigationController *)currentSelectedNavigationController
{
    return [self currentViewController].navigationController;
}

+(UIViewController *)currentViewController{

    if ([DHAppWindow.rootViewController isKindOfClass:[CYLTabBarController class]]) {

        return  [DHAppTabbarController.selectedViewController cyl_getViewControllerInsteadOfNavigationController];
    }else{
        return DHAppWindow.rootViewController.navigationController.visibleViewController;
    }
}

+ (void)didSelectedTabBarControllerForClass:(Class)selectedClass{
    if ([DHAppWindow.rootViewController isKindOfClass:[CYLTabBarController class]]) {

        [DHAppTabbarController cyl_popSelectTabBarChildViewControllerForClassType:selectedClass completion:^(__kindof UIViewController *selectedTabBarChildViewController) {

        }];
    }
}


@end
