//
//  RouterMacro.h
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/18.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#ifndef RouterMacro_h
#define RouterMacro_h

static NSString * const  NewFeatureKey = @"NewFeature";
static NSString * const  NewFeatureController = @"FWNewFeatureViewController";

static NSString * const  HomePageKey = @"HomePage";
static NSString * const  HomePageController = @"FWHomeViewController";

static NSString * const  LoginKey = @"Login";
static NSString * const  LoginController = @"FWLoginViewController";
#endif /* RouterMacro_h */
