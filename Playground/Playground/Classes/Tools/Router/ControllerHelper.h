//
//  ControllerHelper.h
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/19.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ControllerHelper : NSObject

+(UINavigationController *)currentSelectedNavigationController;


+(UIViewController *)currentViewController;

/*!
 *  tabbar 指定切换
 *  Pop 到当前 `NavigationController` 的栈底，并改变 `TabBarController` 的 `selectedViewController` 属性
 *  @param selectedClass 需要选择的控制器所属的类
 */
+ (void)didSelectedTabBarControllerForClass:(Class)selectedClass;


@end
