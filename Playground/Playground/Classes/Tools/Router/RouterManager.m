//
//  RouterManager.m
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/17.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#import "RouterManager.h"



@interface RouterManager ()

@end


@implementation RouterManager

+ (void)load {

//    //*************同步获取 创建Window根控制器*********************
//    //同步获取 创建Window根控制器
//    [MGJRouter registerURLPattern:NewFeatureKey toObjectHandler:^id(NSDictionary *routerParameters) {
//        FWNewFeatureViewModel *viewModel=[[DHNewFeatureViewModel alloc] init];
//        return [[NSClassFromString(NewFeatureController) alloc] initWithViewModel:viewModel];
//    }];

    //登录注册等页面
    [MGJRouter registerURLPattern:LoginKey toObjectHandler:^id(NSDictionary *routerParameters) {
        FWLoginViewModel *viewModel=[[FWLoginViewModel alloc] init];
        return [[NSClassFromString(LoginController) alloc] initWithViewModel:viewModel];
    }];
    
    //**********************************

    //*************tabbar 指定切换   *********************
    [MGJRouter registerURLPattern:HomePageKey toHandler:^(NSDictionary *routerParameters) {
        [ControllerHelper didSelectedTabBarControllerForClass:[FWHomeViewController class]];
    }];

//    [MGJRouter registerURLPattern:MineKey toHandler:^(NSDictionary *routerParameters) {
//
//        [ControllerHelper didSelectedTabBarControllerForClass:[FWMineViewController class]];
//    }];

   //***************tabbar*******************




}


@end
