//
//  RouterManager.h
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/17.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGJRouter.h"
#import "ControllerHelper.h"

//#import "FWNewFeatureViewModel.h"
#import "FWHomeViewController.h"
#import "FWHomeViewModel.h"

#import "FWLoginViewController.h"
#import "FWLoginViewModel.h"


#import "FWNewMineViewController.h"
#import "FWMineViewModel.h"


@interface RouterManager : NSObject

@end
