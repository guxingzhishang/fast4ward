//
//  FWDriverInfoManager.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWDriverInfoManager.h"
#import "YTKKeyValueStore.h"

#define driver_info_tableName  @"driver_infoCache_table"
#define driver_info_storeName  @"driver_infoCache.db"
#define driver_info_JOSNKey    @"driver_infoCache_json_key"
#define driver_info_VersionKey @"driver_infoCahce_version_key"

@implementation FWDriverInfoManager

+ (id)getDriverInfoManagerInfoJSONDataFromLocal{
    
    // 根据不同的userID生成不一样的key
    NSString *key = [NSString stringWithFormat:@"%@%@",driver_info_JOSNKey, [GFStaticData getObjectForKey:kTagUserKeyID]];
    
    YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:driver_info_storeName];
    return [store getObjectById:key fromTable:driver_info_tableName];
}

+ (void)saveDriverInfoManagerInfoJSONDataToLocalWithData:(NSMutableDictionary *)newModel{
    
    // 根据不同的userID生成不一样的key
    NSString *key = [NSString stringWithFormat:@"%@%@",driver_info_JOSNKey,  [GFStaticData getObjectForKey:kTagUserKeyID]];
    
    YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:driver_info_storeName];
    if ([store isTableExists:driver_info_tableName] == NO)
    {
        [store createTableWithName:driver_info_tableName];
    }
    
    /* 本地车手缓存列表 */
    NSMutableArray * localArray = [[[self getDriverInfoManagerInfoJSONDataFromLocal] objectForKey:@"driver_list"] mutableCopy];
    
    /* 用来存取车手列表 */
    NSMutableDictionary * baomingDict = @{}.mutableCopy;
    
    BOOL isHaveBaomingID = NO;
    if (localArray.count > 0) {
        
        for (NSMutableDictionary * dict in localArray) {
            if ([dict[@"baoming_user_id"] isEqualToString:newModel[@"baoming_user_id"]]) {
                /* 如果有相同的id，将之前数据删除，将新数据插入第一个 */
                [localArray removeObject:dict];
                [localArray insertObject:newModel atIndex:0];
                isHaveBaomingID = YES;
                break;
            }
        }
        
        if (!isHaveBaomingID) {
            /* 如果有相同的id，直接将新数据插入第一个 */
            [localArray insertObject:newModel atIndex:0];
        }
        
        if (localArray.count > 0) {
            // 将所有数据包装，用来存储
            [baomingDict setObject:localArray forKey:@"driver_list"];
        }
    }else{
        NSMutableArray * tempArr = @[].mutableCopy;
        [tempArr addObject:newModel];
        
        if (tempArr.count >0) {
            
            [baomingDict setObject:tempArr forKey:@"driver_list"];
        }
    }
    
    if ([baomingDict isKindOfClass:[NSMutableDictionary class]])
    {
        [store putObject:baomingDict withId:key intoTable:driver_info_tableName];
    }
}

+ (void)saveDriverInfoManagerInfoJSONDataToLocalWithArray:(NSMutableArray *)array{
    
    // 根据不同的userID生成不一样的key
    NSString *key = [NSString stringWithFormat:@"%@%@",driver_info_JOSNKey,  [GFStaticData getObjectForKey:kTagUserKeyID]];
    
    YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:driver_info_storeName];
    if ([store isTableExists:driver_info_tableName] == NO)
    {
        [store createTableWithName:driver_info_tableName];
    }
    
    /* 用来存取车手列表 */
    NSMutableDictionary * baomingDict = @{}.mutableCopy;
    
    if (array.count >0) {
        [baomingDict setObject:array forKey:@"driver_list"];
        
        if ([baomingDict isKindOfClass:[NSMutableDictionary class]])
        {
            [store putObject:baomingDict withId:key intoTable:driver_info_tableName];
        }
    }else{
        /* 如果没有数据，直接删掉表 */
        [store dropTable:driver_info_tableName];
    }
}

@end
