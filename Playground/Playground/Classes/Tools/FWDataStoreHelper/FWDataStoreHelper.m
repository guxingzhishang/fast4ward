//
//  FWDataStoreHelper.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/20.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWDataStoreHelper.h"
#import "YTKKeyValueStore.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

@interface FWDataStoreHelper ()

@property (nonatomic, strong) YTKKeyValueStore *store;

@end
@implementation FWDataStoreHelper


+ (instancetype)sharedManager {
    
    static FWDataStoreHelper *store = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        store = [[FWDataStoreHelper alloc] init];
        [store createFWStore];
    });
    return store;
}

- (void)createFWStore {
    
    self.store = [[YTKKeyValueStore alloc] initDBWithName:[self defaultDBName]];
}


#pragma mark - > Private Method

- (NSString *)defaultDBName {
    
    return dh_base_dbname;
}

- (NSString *)pathOfDocument {
    
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

#pragma mark - > Save

- (void)saveObject:(id)object
            withId:(NSString *)Id
         intoTable:(NSString *)tableName {
    
    BOOL isValidData = ([object isKindOfClass:NSArray.class]
                        || [object isKindOfClass:NSDictionary.class]);
    
    if (isValidData) {
        
        if (!_store) {
            [self createFWStore];
        }
        
        if (![self.store isTableExists:tableName]) {
            [self.store createTableWithName:tableName];
        }
        
        [self.store putObject:object withId:Id intoTable:tableName];
    }
    else {
#if DEBUG
        NSAssert(0, @"必须是有效地数组或者字典");
#endif
    }
}

- (void)saveString:(NSString *)string
            withId:(NSString *)Id
         intoTable:(NSString *)tableName {
    
    BOOL isValidData = ([string isKindOfClass:NSString.class]);
    if (isValidData) {
        
        if (!_store) {
            [self createFWStore];
        }
        
        if (![self.store isTableExists:tableName]) {
            [self.store createTableWithName:tableName];
        }
        
        [self.store putString:string withId:Id intoTable:tableName];
        
    }
    else {
        //#if DEBUG
        //        NSAssert(0, @"必须是有效字符串");
        //#endif
    }
}

#pragma mark - > Fetch

- (id)fetchObjectWithId:(NSString *)Id
              fromTable:(NSString *)tableName {
    
    if ([Id isKindOfClass:[NSString class]]
        && [tableName isKindOfClass:[NSString class]]
        && Id.length > 0
        && tableName.length > 0) {
        
        if (!_store) {
            [self createFWStore];
        }
        
        if (![self.store isTableExists:tableName]) {
            [self.store createTableWithName:tableName];
        }
        
        return [self.store getObjectById:Id fromTable:tableName];
    }
    else {
#if DEBUG
        NSAssert(0, @"Id or tableName is not a valid string");
#endif
    }
    return nil;
}

- (NSString *)fetchStringWithId:(NSString *)Id fromTable:(NSString *)tableName {
    
    if ([Id isKindOfClass:[NSString class]]
        && [tableName isKindOfClass:[NSString class]]
        && Id.length > 0
        && tableName.length > 0) {
        
        if (!_store) {
            [self createFWStore];
        }
        
        if (![self.store isTableExists:tableName]) {
            [self.store createTableWithName:tableName];
        }
        
        return [self.store getStringById:Id fromTable:tableName];
    }
    else {
#if DEBUG
        NSAssert(0, @"Id or tableName is not a valid string");
#endif
    }
    return nil;
}

- (void)saveBool:(BOOL)boolValue
          withId:(NSString *)Id
       intoTable:(NSString *)tableName {
    
    if (!_store) {
        [self createFWStore];
    }
    
    if (![self.store isTableExists:tableName]) {
        [self.store createTableWithName:tableName];
    }
    
    NSNumber *boolNumber = [NSNumber numberWithBool:boolValue];
    [self.store putNumber:boolNumber withId:Id intoTable:tableName];
    
}

- (BOOL)fetchBoolValueWithId:(NSString *)Id
                   fromTable:(NSString *)tableName {
    
    if (!_store) {
        [self createFWStore];
    }
    
    if (![self.store isTableExists:tableName]) {
        [self.store createTableWithName:tableName];
    }
    
    NSNumber *boolValue = [self.store getNumberById:Id fromTable:tableName];
    return [boolValue boolValue];
}

-(void)clearTable:(NSString *)tableName{
    [self.store clearTable:tableName];
}

@end
