//
//  FWDriverInfoManager.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWDriverInfoManager : NSObject

/**
 *  @abstract 获得本地推荐列表信息的JSON数据缓存
 *  @discussion 存储Json 使用identity用作key存储
 *  @return 本地推荐列表 JSON串
 */
+ (id)getDriverInfoManagerInfoJSONDataFromLocal;


///**
// @param dataSource 传进来的列表当前的数据源
// @return 不同于数据源中的数据
// */
//+ (NSDictionary *)getLocalArrayWithDataSource:(NSMutableArray *)dataSource;

/**
 *  @abstract 存储本地推荐列表信息的json数据到本地，用于数据缓存
 *  @discussion 存储Json 使用identity用作key存储
 *  @param newModel 新数据
 */
+ (void)saveDriverInfoManagerInfoJSONDataToLocalWithData:(NSMutableDictionary *)newModel;

/**
 * 直接存入新数组
 */
+ (void)saveDriverInfoManagerInfoJSONDataToLocalWithArray:(NSMutableArray *)array;

@end

NS_ASSUME_NONNULL_END
