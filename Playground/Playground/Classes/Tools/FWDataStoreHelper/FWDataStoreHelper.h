//
//  FWDataStoreHelper.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/20.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

#define F4W_DEFAULT_TABLE   @"default_table" // 默认存储表，替代NSUserDefault
#define F4W_IMAGEPATH_TABLE @"imagepath_table"// 默认存储网络请求
#define F4W_NETWORK_REQUEST_RESULT_TABLE @"network_request_result_table"// 默认存储网络请求

@interface FWDataStoreHelper : NSObject


+ (instancetype)sharedManager;

- (void)saveObject:(id)object
            withId:(NSString *)Id
         intoTable:(NSString *)tableName;

- (id)fetchObjectWithId:(NSString *)Id
              fromTable:(NSString *)tableName;


- (void)saveString:(NSString *)string
            withId:(NSString *)Id
         intoTable:(NSString *)tableName;


- (NSString *)fetchStringWithId:(NSString *)Id
                      fromTable:(NSString *)tableName;

- (void)saveBool:(BOOL)boolValue
          withId:(NSString *)Id
       intoTable:(NSString *)tableName;

- (BOOL)fetchBoolValueWithId:(NSString *)Id
                   fromTable:(NSString *)tableName;

-(void)clearTable:(NSString *)tableName;

@end
