//
//  FWUploadManager.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/25.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef BOOL(^FWUploadManagerUnit)(void);

@interface FWUploadManager : NSObject

@property (nonatomic, assign) NSUInteger maximumQueueLength;

@property (nonatomic, strong) UIImage * fengmianImage;

@property (nonatomic, strong) NSString * filePath;

@property (nonatomic, strong) NSDictionary * infoDict;

@property (nonatomic, strong) NSDictionary * objectDict;

@property (nonatomic, strong) NSMutableDictionary * params;

@property (nonatomic, strong) NSArray *imagesArray;

@property (nonatomic, strong) NSString *feed_id;

+ (instancetype)sharedRunLoopWorkDistribution;

- (void)addTask:(FWUploadManagerUnit)unit withKey:(id)key;

- (void)removeAllTasks;

/**
 上传

 @param type 1、视频贴的图片和视频；2图文贴的图片
 */
- (void)requestUploadFengmianImageWithType:(NSString *)type;

@end

NS_ASSUME_NONNULL_END
