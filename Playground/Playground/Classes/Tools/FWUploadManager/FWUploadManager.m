//
//  FWUploadManager.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/25.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWUploadManager.h"
#import <objc/runtime.h>

#import "ALiOssUploadTool.h"
#import <VODUpload/VODUploadSVideoClient.h>

#import "FWPublishRequest.h"

#define FWUploadManager_DEBUG 1

@interface FWUploadManager ()<VODUploadSVideoClientDelegate>

@property (nonatomic, strong) NSMutableArray *tasks;

@property (nonatomic, strong) NSMutableArray *tasksKeys;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSTimer *delay5Secondtimer;

@property (nonatomic, assign) NSInteger currentTime;

@property (nonatomic, strong) VODUploadSVideoClient *client;

@property (nonatomic, strong) NSData * imageData;//用来存储上传进度的图片

@property (nonatomic, assign) NSInteger uploadFinishPic; // 上传成功几张

@property (nonatomic, assign) CGFloat preSubValue; // 上一次上传差值（用来记录断网后是否更新）
@property (nonatomic, assign) long long totalSize; // 上传总大小
@property (nonatomic, assign) long long currentSize; // 当前上传的大小

@property (nonatomic, assign) BOOL isFinish; // 是否完事了（成功or失败）（用来记录断网后是否提示过失败）
@property (nonatomic, assign) BOOL videoUpload5S; // 5秒内不更新数值

@end

@implementation FWUploadManager
@synthesize client;
@synthesize infoDict;
@synthesize objectDict;
@synthesize params;

- (void)removeAllTasks {
    [self.tasks removeAllObjects];
    [self.tasksKeys removeAllObjects];
}

- (void)addTask:(FWUploadManagerUnit)unit withKey:(id)key{
    [self.tasks addObject:unit];
    [self.tasksKeys addObject:key];
    if (self.tasks.count > self.maximumQueueLength) {
        [self.tasks removeObjectAtIndex:0];
        [self.tasksKeys removeObjectAtIndex:0];
    }
}

- (void)_timerFiredMethod:(NSTimer *)timer {
    //We do nothing here
}

- (instancetype)init
{
    if ((self = [super init])) {
        _maximumQueueLength = 30;
        _currentTime = 0;
        _uploadFinishPic = 0;
        _isFinish = NO;
        _preSubValue = 0.0f;
        _videoUpload5S = NO;
        _totalSize = 0.0f;
        _currentSize = 0.0f;
        _tasks = [NSMutableArray array];
        _tasksKeys = [NSMutableArray array];
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(_timerFiredMethod:) userInfo:nil repeats:YES];
        _delay5Secondtimer = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:@selector(checkStatus) userInfo:nil repeats:YES];
        [_delay5Secondtimer fire];
        [_delay5Secondtimer setFireDate:[NSDate distantFuture]];
    }
    return self;
}

- (void)checkStatus{
    
    if (self.preSubValue == 0) {
        return;
    }
//    NSLog(@"来了--");
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.preSubValue == self.totalSize - self.currentSize && self.totalSize - self.currentSize >= 0) {
            if (!self.params[@"is_draft"]) {
                // 1: 草稿  2: 发布
                self.params[@"is_draft"] = @"2";
            }
            
//            NSLog(@"走了--");

            self.preSubValue = 0;
            self.isFinish = YES;
            client = nil;
            [_delay5Secondtimer setFireDate:[NSDate distantFuture]];

            [GFStaticData saveObject:upLoadStatusFailure forKey:upLoadStatus];
            [GFStaticData saveObject:@{@"is_draft":self.params[@"is_draft"]} forKey:upLoadStatusFailure];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:upLoadStatusFailure object:nil userInfo:@{@"is_draft":self.params[@"is_draft"]}];
        }
        self.preSubValue = self.totalSize - self.currentSize;
        self.videoUpload5S = NO;
    });
}

+ (instancetype)sharedRunLoopWorkDistribution {
    static FWUploadManager *singleton;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        singleton = [[FWUploadManager alloc] init];
        [self _registerRunLoopWorkDistributionAsMainRunloopObserver:singleton];
    });
    return singleton;
}

+ (void)_registerRunLoopWorkDistributionAsMainRunloopObserver:(FWUploadManager *)runLoopWorkDistribution {
    static CFRunLoopObserverRef defaultModeObserver;
    _registerObserver(kCFRunLoopBeforeWaiting, defaultModeObserver, NSIntegerMax - 999, kCFRunLoopDefaultMode, (__bridge void *)runLoopWorkDistribution, &_defaultModeRunLoopWorkDistributionCallback);
}

static void _registerObserver(CFOptionFlags activities, CFRunLoopObserverRef observer, CFIndex order, CFStringRef mode, void *info, CFRunLoopObserverCallBack callback) {
    CFRunLoopRef runLoop = CFRunLoopGetCurrent();
    CFRunLoopObserverContext context = {
        0,
        info,
        &CFRetain,
        &CFRelease,
        NULL
    };
    observer = CFRunLoopObserverCreate(     NULL,
                                       activities,
                                       YES,
                                       order,
                                       callback,
                                       &context);
    CFRunLoopAddObserver(runLoop, observer, mode);
    CFRelease(observer);
}

static void _runLoopWorkDistributionCallback(CFRunLoopObserverRef observer, CFRunLoopActivity activity, void *info)
{
    FWUploadManager *runLoopWorkDistribution = (__bridge FWUploadManager *)info;
    if (runLoopWorkDistribution.tasks.count == 0) {
        return;
    }
    BOOL result = NO;
    while (result == NO && runLoopWorkDistribution.tasks.count) {
        FWUploadManagerUnit unit  = runLoopWorkDistribution.tasks.firstObject;
        result = unit();
        [runLoopWorkDistribution.tasks removeObjectAtIndex:0];
        [runLoopWorkDistribution.tasksKeys removeObjectAtIndex:0];
    }
}

static void _defaultModeRunLoopWorkDistributionCallback(CFRunLoopObserverRef observer, CFRunLoopActivity activity, void *info) {
    _runLoopWorkDistributionCallback(observer, activity, info);
}



#pragma mark - > ****************** 阿里云视频上传 **********************
#pragma mark - >  上传
- (void)requestUploadFengmianImageWithType:(NSString *)type{
    
    DHWeakSelf;
    if ([type isEqualToString:@"1"]) {
        // 上传视频封面
        [self addTask:^BOOL{
            
            weakSelf.imageData = nil;
            
            UIImage * getImage = [UIImage imageWithContentsOfFile:[weakSelf.infoDict objectForKey:@"UIImagePath"]];
            weakSelf.imageData = UIImagePNGRepresentation(getImage);
            
            [ALiOssUploadTool asyncUploadImage:getImage WithBucketName:[weakSelf.objectDict objectForKey:@"bucket"] WithObject_key:[weakSelf.objectDict objectForKey:@"object_key"] WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
                if (UploadImageSuccess == state) {
                    // 上传视频
                    weakSelf.isFinish = NO;

                    [weakSelf uploadVideoToALI];
                }
            }];
            
            return YES;
        } withKey:@"uploadVideo"];
    }else if ([type isEqualToString:@"2"]){
        // 上传图片数组
        [self addTask:^BOOL{

            if (weakSelf.imagesArray.count > 0) {
                weakSelf.imageData = nil;

                weakSelf.imageData = UIImagePNGRepresentation(weakSelf.imagesArray[0]);
                __block CGFloat prgress = 0;

                dispatch_async(dispatch_get_main_queue(), ^{

                    [ALiOssUploadTool uploadImages:weakSelf.imagesArray WithBucketName:[weakSelf.objectDict objectForKey:@"bucket"] WithObject_key:nil WithObject_keys:[weakSelf.objectDict objectForKey:@"object_keys"] isAsync:YES progress:^(BOOL uploadProgress) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (weakSelf.isFinish) {
                                return;
                            }
                            
                            //在主队列中进行ui操作
                            if (!weakSelf.params[@"is_draft"]) {
                                // 1: 草稿  2: 发布
                                weakSelf.params[@"is_draft"] = @"2";
                            }
                            
                            
                            if (!uploadProgress) {
                                
                                weakSelf.preSubValue = 0;
                                weakSelf.isFinish = YES;
                                
                                [GFStaticData saveObject:upLoadStatusFailure forKey:upLoadStatus];
                                [GFStaticData saveObject:@{@"is_draft":weakSelf.params[@"is_draft"]} forKey:upLoadStatusFailure];
                                
                                [[NSNotificationCenter defaultCenter] postNotificationName:upLoadStatusFailure object:nil userInfo:@{@"is_draft":weakSelf.params[@"is_draft"]}];
                                
                                return ;
                            }
                            
                            if (uploadProgress) {
                                weakSelf.uploadFinishPic += 1;//(float)totalBytesSent / totalBytesExpectedToSend)
                                weakSelf.preSubValue = weakSelf.uploadFinishPic*1.00;
                                prgress =  (float)((weakSelf.uploadFinishPic *1.00)/(weakSelf.imagesArray.count * 1.00));
                            }
                            NSDictionary * tempDict = @{
                                                        @"uploadProgress":@(prgress).stringValue,
                                                        @"feed_type":weakSelf.params[@"feed_type"],
                                                        @"imageData":weakSelf.imageData,
                                                        @"is_draft":weakSelf.params[@"is_draft"],
                                                        };
                            
                            [GFStaticData saveObject:upLoadStatusUploading forKey:upLoadStatus];
                            [GFStaticData saveObject:tempDict forKey:upLoadStatusUploading];
                            
                            
                            [GFStaticData saveObject:@"YES" forKey:@"isUploading"];

                            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTab" object:nil userInfo:nil];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedUpLoad" object:nil userInfo:tempDict];
                        });
                    } complete:^(NSArray<NSString *> *names, UploadImageState state) {
                        if (!weakSelf.params[@"is_draft"]) {
                            // 1: 草稿  2: 发布
                            weakSelf.params[@"is_draft"] = @"2";
                        }
                        
                        if (weakSelf.uploadFinishPic > 0) {
                            weakSelf.uploadFinishPic = 0;
                        }
                        
                        weakSelf.isFinish = NO;
                        
                        if (UploadImageSuccess == state) {
                            [weakSelf requestGraphSaveDraft];
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:upLoadStatusSuccess object:nil userInfo:@{@"is_draft":weakSelf.params[@"is_draft"]}];
                            [GFStaticData saveObject:upLoadStatusSuccess forKey:upLoadStatus];
                        }else{
                            [[NSNotificationCenter defaultCenter] postNotificationName:upLoadStatusFailure object:nil userInfo:@{@"is_draft":weakSelf.params[@"is_draft"]}];
                            [GFStaticData saveObject:upLoadStatusFailure forKey:upLoadStatus];
                        }
                    }];
                });
            }else{
                /* 没有图片 */
                [weakSelf requestGraphSaveDraft];
            }

            return YES;
        } withKey:@"uploadImages"];
    }else if ([type isEqualToString:@"3"]){
        // 商店上传图片
        [self addTask:^BOOL{
            
            [ALiOssUploadTool asyncUploadImages:weakSelf.imagesArray WithBucketName:[weakSelf.objectDict objectForKey:@"bucket"] WithObject_key:nil WithObject_keys:[weakSelf.objectDict objectForKey:@"object_keys"] complete:^(NSArray<NSString *> *names, UploadImageState state) {
                if (UploadImageSuccess == state) {

                    [weakSelf requestSubmitAddGoods];
                }
            }];
            return YES;
        } withKey:@"uploadImages"];
    }else if ([type isEqualToString:@"4"]){
        // 商店上传图片
        [self addTask:^BOOL{
            
            [ALiOssUploadTool asyncUploadImages:weakSelf.imagesArray WithBucketName:[weakSelf.objectDict objectForKey:@"bucket"] WithObject_key:nil WithObject_keys:[weakSelf.objectDict objectForKey:@"object_keys"] complete:^(NSArray<NSString *> *names, UploadImageState state) {
                if (UploadImageSuccess == state) {
                    
                    [weakSelf requestUpdateShopInfo];
                }
            }];
            return YES;
        } withKey:@"uploadImages"];
    }
}

#pragma mark - > 上传视频到阿里云
- (void)uploadVideoToALI{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString * accessKeyId      = [GFStaticData getObjectForKey:ALi_AccessKeyId_Video];
        NSString * accessKeySecret  = [GFStaticData getObjectForKey:ALi_AccessKeySecret_Video];
        NSString * securityToken    = [GFStaticData getObjectForKey:ALi_SecurityToken_Video];
        
        NSString *imagePath = [infoDict objectForKey:@"UIImagePath"];
        
        VodSVideoInfo *info = [VodSVideoInfo new];
        
        if ([self.params[@"feed_title"] length] > 0) {
            info.title = self.params[@"feed_title"];
        }else{
            info.title = [NSString stringWithFormat:@"iOS_%@",[Utility currentTimeStr]];
        }
        
        client = [VODUploadSVideoClient new];
        client.delegate = self;
        //是否转码，建议开启转码
        client.transcode = true;
        [client uploadWithVideoPath:self.filePath imagePath:imagePath svideoInfo:info accessKeyId:accessKeyId accessKeySecret:accessKeySecret accessToken:securityToken];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(20 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 开启定时器
            [_delay5Secondtimer setFireDate:[NSDate date]];
        });
    });
}

#pragma mark - > 上传成功
-(void)uploadSuccessWithResult:(VodSVideoUploadResult *)result {
    NSLog(@"****成功了:%@, imageurl:%@",result.videoId, result.imageUrl);
    
//    dispatch_source_cancel(_gcdTimer);
    [_delay5Secondtimer setFireDate:[NSDate distantFuture]];


    [self.params setObject:result.videoId forKey:@"video_id"];
    [self.params setObject:@"2" forKey:@"flag_check"];
    
    [self requestVideoSaveDraft];
    client = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.params[@"is_draft"]) {
            // 1: 草稿  2: 发布
            self.params[@"is_draft"] = @"2";
        }
        
        self.isFinish = YES;

        [GFStaticData saveObject:upLoadStatusSuccess forKey:upLoadStatus];
        [GFStaticData saveObject:@{@"is_draft":self.params[@"is_draft"]} forKey:upLoadStatusSuccess];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:upLoadStatusSuccess object:nil userInfo:@{@"is_draft":self.params[@"is_draft"]}];
    });
}

#pragma mark - > 上传进度
-(void)uploadProgressWithUploadedSize:(long long)uploadedSize totalSize:(long long)totalSize {
    NSLog(@"****上传了：%lld， 总共：%lld",uploadedSize,totalSize);
    
    if (uploadedSize <= totalSize) {
        if (self.isFinish) {
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (self.preSubValue != totalSize - uploadedSize && !self.videoUpload5S) {
                self.videoUpload5S = YES;
                self.preSubValue = totalSize - uploadedSize;
            }
            
            self.currentSize = uploadedSize;
            self.totalSize = totalSize;
            
            if (!self.params[@"is_draft"]) {
                // 1: 草稿  2: 发布
                self.params[@"is_draft"] = @"2";
            }

            CGFloat progressValue = [@(uploadedSize).stringValue floatValue] / [@(totalSize).stringValue floatValue];

            NSDictionary * tempDict = @{
                                        @"uploadProgress":@(progressValue).stringValue,
                                        @"feed_type":self.params[@"feed_type"],
                                        @"imageData":self.imageData,
                                        @"is_draft":self.params[@"is_draft"],
                                        };
            
            [GFStaticData saveObject:upLoadStatusUploading forKey:upLoadStatus];
            [GFStaticData saveObject:tempDict forKey:upLoadStatusUploading];
            

            if (![GFStaticData getObjectForKey:@"isUploading"]) {
                [GFStaticData saveObject:@"YES" forKey:@"isUploading"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTab" object:nil userInfo:nil];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedUpLoad" object:nil userInfo:tempDict];
        });
    }
}

#pragma mark - > 上传失败
-(void)uploadFailedWithCode:(NSString *)code message:(NSString *)message {
    NSLog(@"**** 失败了");
    client = nil;

    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.params[@"is_draft"]) {
            // 1: 草稿  2: 发布
            self.params[@"is_draft"] = @"2";
        }
        
        self.isFinish = YES;
        [_delay5Secondtimer setFireDate:[NSDate distantFuture]];

        [GFStaticData saveObject:upLoadStatusFailure forKey:upLoadStatus];
        [GFStaticData saveObject:@{@"is_draft":self.params[@"is_draft"]} forKey:upLoadStatusFailure];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:upLoadStatusFailure object:nil userInfo:@{@"is_draft":self.params[@"is_draft"]}];
    });
}

-(void)uploadTokenExpired {/* token 过期了 */}


#pragma mark - > 视频发布 or 存草稿箱 (1为保存草稿，2为发布帖子)
- (void)requestVideoSaveDraft{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    [request startWithParameters:params WithAction:Submit_add_feed WithDelegate:nil  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            NSLog(@"上传后已传至服务器成功！！！");
            
            
            if ([[params objectForKey:@"is_draft"] isEqualToString:@"1"]) {
                NSString * feedid = [params objectForKey:@"feed_id"];
                NSMutableArray * tempArr = @[].mutableCopy;

                // 草稿
                if ([GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
                    
                    
                    tempArr = [[GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]] mutableCopy];

                    if(![tempArr containsObject:feedid]){
                        [tempArr addObject:feedid];
                    }
                    [GFStaticData saveObject:tempArr forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
                }else{
                    [tempArr addObject:feedid];
                    [GFStaticData saveObject:tempArr forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
                }
            }else{
                // 发布成功后，如果本地有这个feed_id,就删除
                NSMutableArray * array = [GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
                
                NSArray * tempArr = [NSArray arrayWithArray:array];
                NSSet *set = [NSSet setWithArray:tempArr];
                [array removeAllObjects];
                for (NSString * str in set){
                    [array addObject:str];
                }
                
                if ([array containsObject:params[@"draft_id"]]) {
                    [array removeObject:params[@"draft_id"]];
                    
                    [GFStaticData saveObject:array forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
                }
            }
        }
    }];
}

#pragma mark - > 图片发布 or 存草稿箱 (1为保存草稿，2为发布帖子)
- (void)requestGraphSaveDraft{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSMutableDictionary * tempDict = self.params.mutableCopy;
    
    [tempDict setObject:@"2" forKey:@"flag_check"];
    
    NSDictionary * parmas = tempDict.copy;
    
    [request startWithParameters:parmas WithAction:Submit_add_feed WithDelegate:nil  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([[parmas objectForKey:@"is_draft"] isEqualToString:@"1"]) {
                // 保存草稿
                DHWeakSelf;
                
                NSMutableArray * tempArr = @[].mutableCopy;

                if ([GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
                    tempArr = [[GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]] mutableCopy];
                    
                    if (![tempArr containsObject:[[back objectForKey:@"data"] objectForKey:@"feed_id"]]) {
                        [tempArr addObject:[[back objectForKey:@"data"] objectForKey:@"feed_id"]];
                    }
                    [GFStaticData saveObject:tempArr forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
                }else{
                    if (![tempArr containsObject:[[back objectForKey:@"data"] objectForKey:@"feed_id"]]) {
                        [tempArr addObject:[[back objectForKey:@"data"] objectForKey:@"feed_id"]];
                    }
                    [GFStaticData saveObject:tempArr forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
                }

                dispatch_async(dispatch_queue_create(0,0), ^{
                    NSMutableArray * tempArray = @[].mutableCopy;
                    
                    for (int i =0;i < weakSelf.imagesArray.count;i++) {
                        
                        UIImage * image = weakSelf.imagesArray[i];
                        
                        //设置一个图片的存储路径
                        NSDate *date = [NSDate date];
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
                        NSString *dataString = [dateFormatter stringFromDate:date];
                        
                        //设置一个图片的存储路径
                        
                        NSString *path  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
                        
                        NSString * imageName = [NSString stringWithFormat:@"/%@_%d.png",dataString,i];
                        NSString * imagePath = [path stringByAppendingFormat:@"%@",imageName];
                        
                        /*
                         把图片直接保存到指定的路径（同时应该把图片的名称存起来，在取的时候，拼上前面的路径，因为每次路径是变的。）
                         */
                        [UIImagePNGRepresentation(image) writeToFile:imagePath atomically:YES];
                        [tempArray addObject:imageName];
                    }
                    
                    [GFStaticData saveObject:tempArray forKey:[[back objectForKey:@"data"] objectForKey:@"feed_id"]];
                });
            }else{
                // 发布
                NSMutableArray * array = [GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];

                NSArray * tempArr = [NSArray arrayWithArray:array];
                NSSet *set = [NSSet setWithArray:tempArr];
                [array removeAllObjects];
                for (NSString * str in set){
                    [array addObject:str];
                }
                
                if ([array containsObject:tempDict[@"draft_id"]]) {
                    [array removeObject:tempDict[@"draft_id"]];
                    [GFStaticData saveObject:array forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
                }
            }
        }
    }];
}

#pragma mark - > 添加商品
- (void)requestSubmitAddGoods{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSMutableDictionary * tempDict = self.params.mutableCopy;
    
    [tempDict setObject:@"2" forKey:@"flag_check"];
    
    NSDictionary * parmas = tempDict.copy;
    
    [request startWithParameters:parmas WithAction:Submit_add_goods WithDelegate:nil  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            NSLog(@"后台上传成功");
        }
    }];
}

#pragma mark - > 更新店铺信息
- (void)requestUpdateShopInfo{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSMutableDictionary * tempDict = self.params.mutableCopy;
    
    [tempDict setObject:@"2" forKey:@"flag_check"];
    
    NSDictionary * parmas = tempDict.copy;
    
    [request startWithParameters:parmas WithAction:Update_shop_info WithDelegate:nil  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            NSLog(@"后台上传成功");
        }
    }];
}
@end
