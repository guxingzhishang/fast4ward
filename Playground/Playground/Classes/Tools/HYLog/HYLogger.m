//
//  HYLogger.m
//  NSLogStudyDemo
//
//  Created by 韩丛旸 on 16/2/16.
//  Copyright © 2016年 Han. All rights reserved.
//

#import "HYLogger.h"

#import <CocoaLumberjack/DDLog.h>
#import <CocoaLumberjack/DDTTYLogger.h>
#import <CocoaLumberjack/DDFileLogger.h>
#import <CocoaLumberjack/DDASLLogger.h>
#import <CocoaLumberjack/DDLegacyMacros.h>


static int ddLogLevel;

@interface HYLogger() <DDLogFormatter>

@end

@implementation HYLogger

+ (instancetype)sharedInstance
{
    static HYLogger *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[HYLogger alloc] init];
        _instance.isRuntimeLogEffective = isRuntimeLogEffectiveDefault;
    });
    return _instance;
}

+ (void)startWithLogLevel:(HYLogLevel)logLevel
{
    [self sharedInstance];
    [[self sharedInstance] hy_setLogLevel:logLevel];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        // 启动Xcode Colors环境
        setenv("XcodeColors", "YES", 0);

        [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
        
        UIColor *pink = [UIColor colorWithRed:(255/255.0) green:(58/255.0) blue:(159/255.0) alpha:1.0];
        
        [[DDTTYLogger sharedInstance] setForegroundColor:pink backgroundColor:nil forFlag:DDLogFlagInfo];

        // sends log statements to Xcode console - if available
        [[DDTTYLogger sharedInstance] setLogFormatter:self];
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
        
        // sends log statements to Apple System Logger, so they show up on Console.app
//        [[DDASLLogger sharedInstance] setLogFormatter:self];
//        [DDLog addLogger:[DDASLLogger sharedInstance]];
        
    }
    return self;
}

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage
{
    NSString *logLevel = nil;
    switch (logMessage->_flag)
    {
        case DDLogLevelError:
            logLevel = @"[ERROR] > ";
            break;
        case DDLogLevelWarning:
            logLevel = @"[WARN]  > ";
            break;
        case DDLogLevelInfo:
            logLevel = @"[INFO]  > ";
            break;
        case DDLogLevelDebug:
            logLevel = @"[DEBUG] > ";
            break;
        default:
            logLevel = @"[VBOSE] > ";
            break;
    }
    
    NSString *formatStr = [NSString stringWithFormat:@"%@%@",
                           logLevel, logMessage->_message];
    return formatStr;
}

- (void)hy_setLogLevel:(HYLogLevel)logLevel
{
    _logLevel = logLevel;
    switch (_logLevel) {
        case HYLogLevelDEBUG:
            ddLogLevel = DDLogLevelDebug;
            break;
        case HYLogLevelINFO:
            ddLogLevel = DDLogLevelInfo;
            break;
        case HYLogLevelWARN:
            ddLogLevel = DDLogLevelWarning;
            break;
        case HYLogLevelERROR:
            ddLogLevel = DDLogLevelError;
            break;
        case HYLogLevelOFF:
            ddLogLevel = DDLogLevelOff;
            break;
        default:
            break;
    }
}

//! 记录日志(有格式)
- (void)logLevel:(HYLogLevel)level format:(NSString *)format, ...
{
    if (format)
    {
        va_list args;
        va_start(args, format);
        NSString *message = [[NSString alloc] initWithFormat:format arguments:args];
        va_end(args);
        [self logLevel:level message:message];
    }
}

//! 记录日志(无格式)
- (void)logLevel:(HYLogLevel)level message:(NSString *)message
{
    if (message.length > 0)
    {
        switch (level)
        {
            case HYLogLevelERROR:
                DDLogError(@"\n*************************************************************\n\n                      💀💀💀ERROR!!!\n                      💀💀💀ERROR!!!\n          \n*************************************************************\n%@\n============================================================end", message);
                break;
                
            case HYLogLevelWARN:
                DDLogWarn(@"%@\n============================================================end", message);
                break;
                
            case HYLogLevelINFO:
                DDLogInfo(@"%@\n============================================================end", message);
                break;
                
            case HYLogLevelDEBUG:
                DDLogDebug(@"%@\n============================================================end", message);
                break;
                
            default:
                DDLogVerbose(@"%@\n============================================================end", message);
                break;
        }
    }
}

- (id)getObjectFromDictionary:(id)dictionary withKeys:(id)firstKey, ...
{
    NSMutableArray *tempArray = @[].mutableCopy;
    
    va_list args;
    va_start(args, firstKey);
    for (NSString *arg = firstKey; arg != nil; arg = va_arg(args, id))
    {
        [tempArray addObject:arg];
    }
    va_end(args);
    
    id tempObject = dictionary;
    for (NSString *key in tempArray)
    {
        
        if ([tempObject respondsToSelector:@selector(objectForKey:)])
        {
            tempObject = [tempObject objectForKey:key];
        }
        else
        {
            tempObject = nil;
        }
    }
    
    if ([tempObject isEqual:[NSNull null]])
    {
    }
    
    if ([tempObject isKindOfClass:[NSNumber class]]) {
        tempObject = ((NSNumber *)tempObject).stringValue;
    }
    
    if ([tempObject isKindOfClass:[NSArray class]]) {
        tempObject = ((NSArray *)tempObject).mutableCopy;
    }
    
    if (![tempObject isKindOfClass:[NSString class]]
        && ![tempObject isKindOfClass:[NSDictionary class]]
        && ![tempObject isKindOfClass:[NSArray class]]
        && ![tempObject isKindOfClass:[NSMutableArray class]]
        && ![tempObject isKindOfClass:[NSMutableDictionary class]]
        )
    {
        return @"";
    }
    
    tempObject = [self processDictionaryIsNSNull:tempObject];

    
    return tempObject?tempObject:@"";
    
}

- (BOOL)compareStringFrom:(id)dictionary
                  compareString:(NSString *)compareString
                  withValueKeys:(id)firstKey, ... NS_REQUIRES_NIL_TERMINATION;
{
    if (![dictionary isKindOfClass:[NSDictionary class]]) return nil;
    
    NSMutableArray *tempArray = @[].mutableCopy;
    
    va_list args;
    va_start(args, firstKey);
    for (NSString *arg = firstKey; arg != nil; arg = va_arg(args, id))
    {
        [tempArray addObject:arg];
    }
    va_end(args);
    
    id tempObject = dictionary;
    for (NSString *key in tempArray)
    {
        
        if ([tempObject respondsToSelector:@selector(objectForKey:)])
        {
            tempObject = [tempObject objectForKey:key];
        }
        else
        {
            tempObject = nil;
        }
    }
    
    if ([tempObject isKindOfClass:[NSString class]])
    {
        if ([tempObject isEqualToString:compareString])
        {
            return YES;
        }
    }
    

    return NO;

}

- (id)processDictionaryIsNSNull:(id)obj{
    const NSString *blank = @"";
    if ([obj isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dt = [(NSMutableDictionary*)obj mutableCopy];
        for(NSString *key in [dt allKeys]) {
            id object = [dt objectForKey:key];
            if([object isKindOfClass:[NSNull class]]) {
                [dt setObject:blank
                       forKey:key];
            }
            else if ([object isKindOfClass:[NSString class]]){
                NSString *strobj = (NSString*)object;
                if ([strobj isEqualToString:@"<null>"]) {
                    [dt setObject:blank
                           forKey:key];
                }
            }
            else if ([object isKindOfClass:[NSArray class]]){
                NSArray *da = (NSArray*)object;
                da = [self processDictionaryIsNSNull:da];
                [dt setObject:da
                       forKey:key];
            }
            else if ([object isKindOfClass:[NSDictionary class]]){
                NSDictionary *ddc = (NSDictionary*)object;
                ddc = [self processDictionaryIsNSNull:object];
                [dt setObject:ddc forKey:key];
            }
        }
        return [dt copy];
    }
    else if ([obj isKindOfClass:[NSArray class]]){
        NSMutableArray *da = [(NSMutableArray*)obj mutableCopy];
        for (int i=0; i<[da count]; i++) {
            NSDictionary *dc = [obj objectAtIndex:i];
            dc = [self processDictionaryIsNSNull:dc];
            [da replaceObjectAtIndex:i withObject:dc];
        }
        return [da copy];
    }else if([obj isKindOfClass:[NSNull class]]) {
        
        return @{@"":@""};
        
    }else {
        return obj;
    }
}

@end
