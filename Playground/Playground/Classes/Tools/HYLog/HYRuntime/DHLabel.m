//
//  DHLabel.m
//  Project_Model
//
//  Created by 韩丛旸 on 2017/1/11.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import "DHLabel.h"

static NSString *currentDeviceType;

@implementation DHLabel

+ (void)load{
    [super load];
    
    currentDeviceType = [UIDevice getCurrentDeviceScreenSize];
    
}

- (id)mutableCopy{
    
    DHLabel *copyLabel = [[DHLabel alloc] init];

    copyLabel.font = self.font;
    copyLabel.textAlignment = self.textAlignment;
    copyLabel.textColor = self.textColor;
    copyLabel.backgroundColor = self.backgroundColor;
    copyLabel.numberOfLines = self.numberOfLines;

    return copyLabel;
}

+ (instancetype)labelWithType:(DHLabelType)type title:(NSString *)title{
    
    DHLabel *tempLabel = [[DHLabel alloc] init];
    tempLabel.text = title;
    
    switch (type) {
        case DHLabelTypeDefault:{
            
        }
            break;
            
        default:
            break;
    }
    
    return tempLabel;
}

+ (instancetype)labelWithType:(DHLabelType)type title:(NSString *)title textColor:(id)textColor fontsize:(CGFloat)fontsize backgroundColor:(id)backgroundColor{
    
    DHLabel *tempLabel = [[DHLabel alloc] init];
    tempLabel.font = [UIFont systemFontOfSize:fontsize];
    tempLabel.text = title?title:@"";
    
    if ([textColor isKindOfClass:[UIColor class]]) {
        tempLabel.textColor = textColor;
    } else if ([textColor isKindOfClass:[NSString class]]) {
        tempLabel.textColor = [UIColor colorWithHexString:textColor];
    } else {
        tempLabel.textColor = [UIColor whiteColor];
    }
    
    if ([backgroundColor isKindOfClass:[UIColor class]]) {
        tempLabel.backgroundColor = backgroundColor;
    } else if ([backgroundColor isKindOfClass:[NSString class]]) {
        tempLabel.backgroundColor = [UIColor colorWithHexString:backgroundColor];
    } else {
        tempLabel.backgroundColor = [UIColor whiteColor];
    }
    
    switch (type) {
        case DHLabelTypeDefault:{
            tempLabel.textAlignment = NSTextAlignmentLeft;
            tempLabel.numberOfLines = 0;
            tempLabel.backgroundColor = [UIColor clearColor];

        }
            break;
            
        default:
            break;
    }
    
    return tempLabel;
    
}

@end
