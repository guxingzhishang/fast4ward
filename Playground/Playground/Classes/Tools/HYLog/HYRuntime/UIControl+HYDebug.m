#//
//  UIControl+HYDebug.m
//  Object-CPlayGround
//
//  Created by 韩丛旸 on 2017/6/15.
//  Copyright © 2017年 韩丛旸. All rights reserved.
//

#import "UIControl+HYDebug.h"
#import "HYSwizzleMethod.h"
#import <objc/runtime.h>

@implementation UIControl (HYDebug)

+ (void)load {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        SEL systemSel = @selector(sendAction:to:forEvent:);
        SEL swizzSel = @selector(ah_sendAction:to:forEvent:);
        HYSwizzleMethod([self class], systemSel, swizzSel);
        
    });
    
}

- (void)ah_sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event
{
//    if ([NSStringFromClass([target class]) isEqualToString:@"SignViewController"]
//        && [NSStringFromSelector(action) isEqualToString:@"nbs_btnCancleAction"]) {
//        
//        [[NSNotificationCenter defaultCenter] postNotificationName:DHSignCancelButtolClickedNotification
//                                                            object:nil];
//        
//    }
//    
    NSString *selector = NSStringFromSelector(action);
    if ([selector containsString:@"nbs_"]) {
        selector = [selector stringByReplacingOccurrencesOfString:@"nbs_" withString:@""];
    }
    // HYLogDebug
    NSLog(@"********矮油,你怼了肆放app一下₍₍ (ง ᐛ )ว ⁾⁾********\n\t***方法触发器:%@\n\t***方法名:%@\n\t***方法的执行者:%@\n*************哒哒 \\\\ ( ᐛ )  //就这些啦*************", NSStringFromClass([self class]), selector, NSStringFromClass([target class]));
    [self ah_sendAction:action to:target forEvent:event];
}


@end
