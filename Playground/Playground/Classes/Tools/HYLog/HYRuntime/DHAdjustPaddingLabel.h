//
//  DHAdjustPaddingLabel.h
//  Project_Model
//
//  Created by 韩丛旸 on 2017/10/27.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHAdjustPaddingLabel : UILabel

- (instancetype)initWithTextEdgeInsets:(UIEdgeInsets)edgeInsets;

@end
