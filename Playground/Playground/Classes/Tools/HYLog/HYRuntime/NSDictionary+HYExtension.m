//
//  NSDictionary+HYExtension.m
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/15.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import "NSDictionary+HYExtension.h"

@implementation NSDictionary (HYExtension)

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil || [jsonString isEqualToString:@""]) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

@end
