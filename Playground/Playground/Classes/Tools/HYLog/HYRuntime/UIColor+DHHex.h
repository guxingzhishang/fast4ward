//
//  UIColor+DHHex.h
//  Mine
//
//  Created by Pactera on 16/8/12.
//  Copyright © 2016年 Pactera. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - > 字体颜色预设
#define FWClearColor [UIColor clearColor]

#define FWColor(X)  [UIColor colorWithHexString:X]
#define FWColorWihtAlpha(X,Alpha)  [UIColor colorWithHexString:X alpha:Alpha]

#define FWImage(Named)  [UIImage imageNamed:Named];

#define FWTextColor_000000  [UIColor colorWithHexString:@"000000"]
#define FWTextColor_0D0A1B  [UIColor colorWithHexString:@"0D0A1B"]
#define FWTextColor_0D0E15  [UIColor colorWithHexString:@"0D0E15"]
#define FWTextColor_131314  [UIColor colorWithHexString:@"131314"]
#define FWTextColor_161426  [UIColor colorWithHexString:@"161426"]
#define FWTextColor_180942  [UIColor colorWithHexString:@"180942"]
#define FWTextColor_1E1C2D  [UIColor colorWithHexString:@"1E1C2D"]
#define FWTextColor_1E1E1E  [UIColor colorWithHexString:@"1E1E1E"]

#define FWTextColor_222222  [UIColor colorWithHexString:@"222222"]
#define FWTextColor_2B98FA  [UIColor colorWithHexString:@"2B98FA"]
#define FWTextColor_363445  [UIColor colorWithHexString:@"363445"]
#define FWTextColor_414042  [UIColor colorWithHexString:@"414042"]
#define FWTextColor_45485B  [UIColor colorWithHexString:@"45485B"]
#define FWTextColor_515151  [UIColor colorWithHexString:@"515151"]
#define FWTextColor_525467  [UIColor colorWithHexString:@"525467"]
#define FWTextColor_575757  [UIColor colorWithHexString:@"575757"]
#define FWTextColor_626D8F  [UIColor colorWithHexString:@"626D8F"]
#define FWTextColor_646464  [UIColor colorWithHexString:@"646464"]
#define FWTextColor_71768C  [UIColor colorWithHexString:@"71768C"]
#define FWTextColor_797980  [UIColor colorWithHexString:@"797980"]
#define FWTextColor_7B8292  [UIColor colorWithHexString:@"7B8292"]
#define FWTextColor_8F8F8F  [UIColor colorWithHexString:@"8F8F8F"]
#define FWTextColor_969696  [UIColor colorWithHexString:@"969696"]
#define FWTextColor_9C9C9C  [UIColor colorWithHexString:@"9C9C9C"]
#define FWTextColor_C9C9C9  [UIColor colorWithHexString:@"C9C9C9"]

#define FWTextColor_A5AEC4  [UIColor colorWithHexString:@"A5AEC4"]
#define FWTextColor_A6A6A6  [UIColor colorWithHexString:@"A6A6A6"]
#define FWTextColor_B5B5B5  [UIColor colorWithHexString:@"B5B5B5"]
#define FWTextColor_B8B8B8  [UIColor colorWithHexString:@"B8B8B8"]
#define FWTextColor_BCBCBC  [UIColor colorWithHexString:@"BCBCBC"]
#define FWTextColor_BDBDBD  [UIColor colorWithHexString:@"BDBDBD"]
#define FWTextColor_D8D8D8  [UIColor colorWithHexString:@"D8D8D8"]
#define FWTextColor_D9E2E9  [UIColor colorWithHexString:@"D9E2E9"]

#define FWViewBackgroundColor_E5E5E5 [UIColor colorWithHexString:@"E5E5E5"]
#define FWViewBackgroundColor_EEEEEE  [UIColor colorWithHexString:@"EEEEEE"]
#define FWViewBackgroundColor_F0F0F0  [UIColor colorWithHexString:@"F0F0F0"]
#define FWViewBackgroundColor_FF6F00  [UIColor colorWithHexString:@"FF6F00"]
#define FWViewBackgroundColor_FBBE2D  [UIColor colorWithHexString:@"FBBE2D"]
#define FWViewBackgroundColor_F3F3F3  [UIColor colorWithHexString:@"F3F3F3"]
#define FWViewBackgroundColor_F3F4F6  [UIColor colorWithHexString:@"F3F4F6"]
#define FWBgColor_Lightgray_F7F8FA [UIColor colorWithHexString:@"F7F8FA"]
#define FWViewBackgroundColor_FFFFFF  [UIColor colorWithHexString:@"FFFFFF"]

#define FWGradual_Orange_FFBD2F [UIColor colorWithHexString:@"FFBD2F"]
#define FWGradual_Red_E67436 [UIColor colorWithHexString:@"E67436"]
#define FWGradual_Orange_FF8C11 [UIColor colorWithHexString:@"FF8C11"]
#define FWTextColor_Orange_FF9315 [UIColor colorWithHexString:@"FF9315"]
#define FWLightOrange [UIColor colorWithHexString:@"FFB429"]
#define FWOrange [UIColor colorWithHexString:@"FBBE2D"]

#define DHTextHexColor_Dark_222222  [UIColor colorWithHexString:@"222222"]
#define DHTextHexColor_Dark_333333  [UIColor colorWithHexString:@"333333"]
#define DHTitleColor_666666  [UIColor colorWithHexString:@"666666"]
#define DHLineHexColor_LightGray_999999 [UIColor colorWithHexString:@"999999"]

#define DHPinkColorFF8B8C [UIColor colorWithHexString:@"FF8B8C"]
#define DHRedColorff6f00 [UIColor colorWithHexString:@"ff6f00"]

#define DHViewBackgroundColor_E2E2E2  [UIColor colorWithHexString:@"E2E2E2"]
#define DHViewBackgroundColor_F6F6F6  [UIColor colorWithHexString:@"F6F6F6"]
#define DHLineHexColor_LightGray_CCCCCC [UIColor colorWithHexString:@"CCCCCC"]

#define DHTitleColor_FFFFFF  [UIColor colorWithHexString:@"FFFFFF"]


#define DHPieChartColor_Orange [UIColor colorWithHexString:@"FF9801"]
#define DHSegementColor_Blue [UIColor colorWithHexString:@"7FBFFF"]


// 第三份UI色值
#define FWTextColor_12101D  [UIColor colorWithHexString:@"12101D"]
#define FWTextColor_121619  [UIColor colorWithHexString:@"121619"]
#define FWTextColor_141414  [UIColor colorWithHexString:@"141414"]
#define FWTextColor_252527  [UIColor colorWithHexString:@"252527"]
#define FWTextColor_272727  [UIColor colorWithHexString:@"272727"]
#define FWTextColor_2E2E2E  [UIColor colorWithHexString:@"2E2E2E"]
#define FWTextColor_5F6680  [UIColor colorWithHexString:@"5F6680"]
#define FWTextColor_616C91  [UIColor colorWithHexString:@"616C91"]
#define FWTextColor_657299  [UIColor colorWithHexString:@"657299"]
#define FWTextColor_66739A  [UIColor colorWithHexString:@"66739A"]
#define FWTextColor_67769E  [UIColor colorWithHexString:@"67769E"]
#define FWTextColor_68769F  [UIColor colorWithHexString:@"68769F"]
#define FWTextColor_858585  [UIColor colorWithHexString:@"858585"]
#define FWTextColor_919191  [UIColor colorWithHexString:@"919191"]
#define FWTextColor_9EA3AB  [UIColor colorWithHexString:@"9EA3AB"]
#define FWTextColor_A7ADB4  [UIColor colorWithHexString:@"A7ADB4"]
#define FWTextColor_ADADAD  [UIColor colorWithHexString:@"ADADAD"]
#define FWTextColor_AEAEAE  [UIColor colorWithHexString:@"AEAEAE"]
#define FWTextColor_B6BCC4  [UIColor colorWithHexString:@"B6BCC4"]
#define FWTextColor_FAFAFA  [UIColor colorWithHexString:@"FAFAFA"]
#define FWTextColor_F6F8FA  [UIColor colorWithHexString:@"F6F8FA"]
#define FWTextColor_F8F8F8  [UIColor colorWithHexString:@"F8F8F8"]
#define FWTextColor_FFAF3C  [UIColor colorWithHexString:@"FFAF3C"]
#define FWTextColor_C4C4C4  [UIColor colorWithHexString:@"C4C4C4"]
#define FWTextColor_ECECEC  [UIColor colorWithHexString:@"ECECEC"]
#define FWViewBackgroundColor_F1F1F1  [UIColor colorWithHexString:@"F1F1F1"]
#define FWTextColor_F5F5F5  [UIColor colorWithHexString:@"F5F5F5"]

#define FWRed_CC3333        [UIColor colorWithHexString:@"CC3333"]
#define FWBlue_4598F4       [UIColor colorWithHexString:@"4598F4"]
#define FWBlue_7595FF       [UIColor colorWithHexString:@"7595FF"]
#define FWGreen_00FF1D      [UIColor colorWithHexString:@"00FF1D"]


#define RGBA_COLOR(R, G, B, A) [UIColor colorWithRed:((R) / 255.0f) green:((G) / 255.0f) blue:((B) / 255.0f) alpha:A]
#define RGB_COLOR(R, G, B) [UIColor colorWithRed:((R) / 255.0f) green:((G) / 255.0f) blue:((B) / 255.0f) alpha:1.0f]

@interface UIColor (DHHex)

+ (UIColor *)colorWithHexString:(NSString *)color;

//color:支持@“#123456”、 @“0X123456”、 @“123456”三种格式
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;

@end
