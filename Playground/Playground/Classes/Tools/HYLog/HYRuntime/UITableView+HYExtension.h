//
//  UITableView+HYExtension.h
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/4.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, FWTableViewType) {
    FWTableViewTypeDefault,// 默认类型
    FWTableViewTypeAutomaticDimension,// 高度自适应类型
};

@interface UITableView (HYExtension)

+ (instancetype)tableViewWithType:(FWTableViewType)type delegate:(id)delegate;


@end
