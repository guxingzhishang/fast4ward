//
//  UIImage+HYExtension.h
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/12.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (HYExtension)

+ (UIImage *)encodeQRImageWithContent:(NSString *)content size:(CGSize)size;

@end
