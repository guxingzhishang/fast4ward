//
//  UIDevice+HYExtension.h
//  Project_Model
//
//  Created by 韩丛旸 on 16/10/21.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (HYExtension)

+ (NSString *)getCurrentDeviceScreenSize;

// 获得当前设备平台类型
+ (NSString *)getCurrentDeviceInfo;
// 获得当前系统版本型号
+ (CGFloat)currentDeviceSystemVersion;

@end
