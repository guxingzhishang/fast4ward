//
//  NSDictionary+HYExtension.h
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/15.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (HYExtension)

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

@end
