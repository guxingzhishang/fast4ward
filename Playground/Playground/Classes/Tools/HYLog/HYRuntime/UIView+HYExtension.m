//
//  UIView+HYExtension.m
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/3.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import "UIView+HYExtension.h"

@implementation UIView (HYExtension)

- (void)hy_addSubviews:(NSArray *)subViews
{
    [subViews enumerateObjectsUsingBlock:^(UIView * subView, NSUInteger index, BOOL * stop)
    {
        [self addSubview:subView];
    }];
}

- (void)hy_removeSubviews:(NSArray *)subViews
{
    [subViews enumerateObjectsUsingBlock:^(UIView * subView, NSUInteger index, BOOL * stop)
     {
         [subView removeFromSuperview];
     }];
}

- (UIView *)hy_copy
{
    UIView *view = [[UIView alloc] init];
    view.userInteractionEnabled = self.userInteractionEnabled;
    view.backgroundColor = self.backgroundColor;
    return view;
}
@end
