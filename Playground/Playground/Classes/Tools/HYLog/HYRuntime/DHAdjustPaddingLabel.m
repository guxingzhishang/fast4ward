//
//  DHAdjustPaddingLabel.m
//  Project_Model
//
//  Created by 韩丛旸 on 2017/10/27.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import "DHAdjustPaddingLabel.h"

@interface DHAdjustPaddingLabel ()

@property (nonatomic, assign) UIEdgeInsets textDrawEdgeInsets;


@end

@implementation DHAdjustPaddingLabel

- (instancetype)initWithTextEdgeInsets:(UIEdgeInsets)edgeInsets {
    
    self = [super init];
    if (self) {
        self.textDrawEdgeInsets = edgeInsets;
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.textDrawEdgeInsets)];
}

@end
