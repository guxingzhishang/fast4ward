//
//  UIView+HYExtension.h
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/3.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (HYExtension)

- (void)hy_addSubviews:(NSArray *)subViews;

- (void)hy_removeSubviews:(NSArray *)subViews;

@property (nonatomic, strong) UIView *hy_copy;

@end
