//
//  UIFont+DHExtension.h
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/4.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DHSystemFontOfSize_9  [UIFont systemFontOfSize:9.0f]

#define DHSystemFontOfSize_10  [UIFont systemFontOfSize:10.0f]

#define DHSystemFontOfSize_11  [UIFont systemFontOfSize:11.0f]

#define DHSystemFontOfSize_12  [UIFont systemFontOfSize:12.0f]

#define DHSystemFontOfSize_13  [UIFont systemFontOfSize:13.0f]
#define DHBoldSystemFontOfSize_13  [UIFont boldSystemFontOfSize:13.0f]

#define DHSystemFontOfSize_14  [UIFont systemFontOfSize:14.0f]
#define DHBoldSystemFontOfSize_14  [UIFont boldSystemFontOfSize:14.0f]

#define DHSystemFontOfSize_15  [UIFont systemFontOfSize:15.0f]
#define DHBoldSystemFontOfSize_15  [UIFont boldSystemFontOfSize:15.0f]

#define DHSystemFontOfSize_16  [UIFont systemFontOfSize:16.0f]
#define DHBoldSystemFontOfSize_16  [UIFont boldSystemFontOfSize:16.0f]

#define DHSystemFontOfSize_17  [UIFont systemFontOfSize:17.0f]
#define DHBoldSystemFontOfSize_17  [UIFont boldSystemFontOfSize:17.0f]

#define DHSystemFontOfSize_18  [UIFont systemFontOfSize:18.0f]
#define DHBoldSystemFontOfSize_18  [UIFont boldSystemFontOfSize:18.0f]

#define DHSystemFontOfSize_19  [UIFont systemFontOfSize:19.0f]
#define DHBoldSystemFontOfSize_19  [UIFont boldSystemFontOfSize:19.0f]

#define DHSystemFontOfSize_20  [UIFont systemFontOfSize:20.0f]
#define DHBoldSystemFontOfSize_20  [UIFont boldSystemFontOfSize:20.0f]

#define DHSystemFontOfSize_21  [UIFont systemFontOfSize:21.0f]

#define DHBoldSystemFontOfSize_22  [UIFont boldSystemFontOfSize:22.0f]

#define DHSystemFontOfSize_22  [UIFont systemFontOfSize:22.0f]

#define DHSystemFontOfSize_23  [UIFont systemFontOfSize:23.0f]
#define DHSystemFontOfSize_24  [UIFont systemFontOfSize:24.0f]
#define DHSystemFontOfSize_25  [UIFont systemFontOfSize:25.0f]
#define DHBoldSystemFontOfSize_24  [UIFont boldSystemFontOfSize:24.0f]
#define DHBoldSystemFontOfSize_25  [UIFont boldSystemFontOfSize:25.0f]

#define DHSystemFontOfSize_29  [UIFont systemFontOfSize:29.0f]
#define DHSystemFontOfSize_30  [UIFont systemFontOfSize:30.0f]

#define DHSystemFontOfSize_36  [UIFont systemFontOfSize:36.0f]

#define DHBoldSystemFontOfSize_36  [UIFont boldSystemFontOfSize:36.0f]

#define DHDINAlternateBoldFont(s) [UIFont fontWithName:@"DINAlternate-Bold" size:s]

@interface UIFont (HYExtension)



@end
