//
//  DHLabel.h
//  Project_Model
//
//  Created by 韩丛旸 on 2017/1/11.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, DHLabelType) {
    DHLabelTypeDefault,
};

@interface DHLabel : UILabel

@property (nonatomic, strong) NSIndexPath *indexPath;

+ (instancetype)labelWithType:(DHLabelType)type title:(NSString *)title;

+ (instancetype)labelWithType:(DHLabelType)type title:(NSString *)title textColor:(id)textColor fontsize:(CGFloat)fontsize backgroundColor:(id)backgroundColor;

@end
