//
//  UITableView+HYExtension.m
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/4.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import "UITableView+HYExtension.h"

@implementation UITableView (HYExtension)


+ (instancetype)tableViewWithType:(FWTableViewType)type delegate:(id)delegate{
    
    return [UITableView tableViewWithType:type delegate:delegate backgroundColor:nil];

}

+ (instancetype)tableViewWithType:(FWTableViewType)type delegate:(id)delegate backgroundColor:(NSString *)hexColor{
    
    UITableView *tableView = [[UITableView alloc] init];
    
    if (hexColor.length > 0) {
        tableView.backgroundColor = [UIColor colorWithHexString:hexColor];
    }
    
    switch (type) {
        case FWTableViewTypeAutomaticDimension:{
            tableView.delegate = delegate;
            tableView.dataSource = delegate;
            tableView.estimatedRowHeight = 60;
            tableView.rowHeight = UITableViewAutomaticDimension;
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        }
            break;
        case FWTableViewTypeDefault:{
            tableView.delegate = delegate;
            tableView.dataSource = delegate;
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        }
            
        default:
            break;
    }
    
    return tableView;
}
@end
