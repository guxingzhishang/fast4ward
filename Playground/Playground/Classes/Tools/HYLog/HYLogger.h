//
//  HYLogger.h
//  NSLogStudyDemo
//
//  Created by 韩丛旸 on 16/2/16.
//  Copyright © 2016年 Han. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(UInt8, HYLogLevel) {
    HYLogLevelDEBUG         = 1,
    HYLogLevelINFO          = 2,
    HYLogLevelWARN          = 3,
    HYLogLevelERROR         = 4,
    HYLogLevelOFF           = 5,
};

#define  isEmptyNSString(string) ([string isKindOfClass:[NSString class]] && \
((NSString *)string).length == 0)

#define  isNSStringContainCharacter(string) ([string isKindOfClass:[NSString class]] && \
((NSString *)string).length > 0)

#define HYGET_OBJECT_FROMDIC(dictionary, frmt, ...)     [[HYLogger sharedInstance] getObjectFromDictionary:dictionary withKeys:frmt ,##__VA_ARGS__, nil]

#define HYCOMPARE_STRING_FROMDIC(dictionary, string, frmt, ...)      [[HYLogger sharedInstance] compareStringFrom:dictionary compareString:string withValueKeys:frmt, ##__VA_ARGS__, nil]


#if DEBUG
    #define __HY_FUNCTION__ NSLog(@"function = %s, line = %d", __PRETTY_FUNCTION__, __LINE__);
#else
    #define __HY_FUNCTION__ 
#endif

#define HY_LOG_MACRO(level, fmt, ...)     [[HYLogger sharedInstance] logLevel:level format:(fmt), ##__VA_ARGS__]
#define HY_LOG_PRETTY(level, fmt, ...)    \
do {HY_LOG_MACRO(level, @"%s 第%d行 \n============================================================begin\n" fmt, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);} while(0)

#define HY_LOG_Description(discription, level, fmt, ...)    \
do {HY_LOG_MACRO(level, @"%s 第%d行 \n============================================================begin\n**************当前日志描述**************begin\n%@\n**************************************end\n" fmt, __PRETTY_FUNCTION__, __LINE__, discription, ##__VA_ARGS__);} while(0)


#ifdef DEBUG
#define HYLogError(frmt, ...)   HY_LOG_PRETTY(HYLogLevelERROR, frmt, ##__VA_ARGS__)
#define HYLogWarn(frmt, ... )   HY_LOG_PRETTY(HYLogLevelWARN,  frmt, ##__VA_ARGS__)
#define HYLogInfo(frmt, ... )   HY_LOG_PRETTY(HYLogLevelINFO,  frmt, ##__VA_ARGS__)
#define HYLogDebug(frmt, ...)   HY_LOG_PRETTY(HYLogLevelDEBUG, frmt, ##__VA_ARGS__)

#define HYLogErrorWithDiscription(discription,frmt, ...) HY_LOG_Description(discription, HYLogLevelERROR, frmt, ##__VA_ARGS__)
#define HYLogWarnWithDiscription(discription,frmt, ... ) HY_LOG_Description(discription, HYLogLevelWARN,  frmt, ##__VA_ARGS__)
#define HYLogInfoWithDiscription(discription,frmt, ... ) HY_LOG_Description(discription, HYLogLevelINFO,  frmt, ##__VA_ARGS__)
#define HYLogDebugWithDiscription(discription,frmt, ...) HY_LOG_Description(discription, HYLogLevelDEBUG, frmt, ##__VA_ARGS__)

#define isRuntimeLogEffectiveDefault 1

#else

#define HYLogError(frmt, ...)
#define HYLogWarn(frmt, ... )
#define HYLogInfo(frmt, ... )
#define HYLogDebug(frmt, ...)   
#define HYLogDebugWithDiscription(discription,frmt, ...)
#define HYLogErrorWithDiscription(discription,frmt, ...)
#define HYLogWarnWithDiscription(discription,frmt, ... )
#define HYLogInfoWithDiscription(discription,frmt, ... )

#define isRuntimeLogEffectiveDefault 0

#endif

@interface HYLogger : NSObject

@property (nonatomic, assign) HYLogLevel logLevel;
@property (nonatomic, assign) BOOL isRuntimeLogEffective;//< 是否开启运行时Log事件 默认为YES;

+ (instancetype)sharedInstance;
+ (void)startWithLogLevel:(HYLogLevel)logLevel;

- (void)logLevel:(HYLogLevel)level format:(NSString *)format, ...;
- (void)logLevel:(HYLogLevel)level message:(NSString *)message;

- (id)getObjectFromDictionary:(id)dictionary withKeys:(id)firstKey, ... NS_REQUIRES_NIL_TERMINATION;

- (BOOL)compareStringFrom:(id)dictionary
            compareString:(NSString *)compareString
            withValueKeys:(id)firstKey, ... NS_REQUIRES_NIL_TERMINATION;

@end
