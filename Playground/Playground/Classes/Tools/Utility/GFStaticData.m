//
//  GFStaticData.m
//  StockMaster
//
//  Created by Rebloom on 14-8-11.
//  Copyright (c) 2014年 Rebloom. All rights reserved.
//

#import "GFStaticData.h"

@implementation GFStaticData

static GFStaticData * gfStaticData = nil;

+ (GFStaticData *)defaultDataCenter
{
    if (!gfStaticData){
        gfStaticData = [[GFStaticData alloc] init];
    }
    return gfStaticData;
}

+ (BOOL)saveObject:(id)object forKey:(NSString *)key
{
    if (!object && key){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    }
    else if (key){
        [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    }
    else{
        assert(object);
    }
    return [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (id)getObjectForKey:(NSString *)key
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key]){
        return [[NSUserDefaults standardUserDefaults] objectForKey:key];
    }
    else{
        return nil;
    }
}

// 做一些数据的保存相关方法（区分用户的）
+ (BOOL)saveObject:(id)data forKey:(NSString *)key forUser:(NSString *)user
{
    if ([key length] == 0 || [user length] == 0 || data == nil){
        return NO;
    }
    
    // 判断当前文件夹是否存在
    NSFileManager *fileManger = [NSFileManager defaultManager];
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dirPath = [path stringByAppendingPathComponent:[Utility md5:user]];
    if (![fileManger fileExistsAtPath:dirPath]){
        [fileManger createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    // 保存的文件路径
    NSString *filePath = [dirPath stringByAppendingFormat:@"/%@.plist",[Utility md5:key]];
    
    // 返回保存结果
    return [NSKeyedArchiver archiveRootObject:data toFile:filePath];
}

// 获取一些数据的相关方法（区分用户的）
+ (id)getObjectForKey:(NSString*)key forUser:(NSString*)user
{
    if ([key length] == 0 || [user length] == 0){
        return nil;
    }
    

    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dirPath = [path stringByAppendingPathComponent:[Utility md5:user]];
    // 对应的保存文件路径
    NSString *filePath = [dirPath stringByAppendingFormat:@"/%@.plist",[Utility md5:key]];
   
    return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
}

+ (void)saveLoginData:(NSDictionary *)dic
{
    if ([dic objectForKey:kTagUserKeyMobileState]){
        NSString * mobile = [[dic objectForKey:kTagUserKeyMobileState] description];
        [self saveObject:mobile forKey:kTagUserKeyMobileState];
    }
    if ([dic objectForKey:kTagUserKeyHeaderIndex]) {
        NSString * header_index = [[dic objectForKey:kTagUserKeyHeaderIndex] description];
        [self saveObject:header_index forKey:kTagUserKeyHeaderIndex];
    }
    if ([dic objectForKey:kTagUserMaxVideoDuration]){
        NSString * max_video_duration = [[dic objectForKey:kTagUserMaxVideoDuration] description];
        [self saveObject:max_video_duration forKey:kTagUserMaxVideoDuration];
    }
    if ([dic objectForKey:kTagUserKeyWXStatus]){
        NSString * mobile = [[dic objectForKey:kTagUserKeyWXStatus] description];
        [self saveObject:mobile forKey:kTagUserKeyWXStatus];
    }
    if ([dic objectForKey:kTagUserKeyToken]){
        NSString * token = [[dic objectForKey:kTagUserKeyToken] description];
        [self saveObject:token forKey:kTagUserKeyToken];
    }
    if ([dic objectForKey:kTagUserKeyID]){
        NSString * userID = [[dic objectForKey:kTagUserKeyID] description];
        [self saveObject:userID forKey:kTagUserKeyID];
    }
    if ([dic objectForKey:kTagUserKeySex]){
        NSString * sex = [[dic objectForKey:kTagUserKeySex] description];
        [self saveObject:sex forKey:kTagUserKeySex];
    }
    if ([dic objectForKey:kTagUserKeyName]){
        NSString * name = [[dic objectForKey:kTagUserKeyName] description];
        [self saveObject:name forKey:kTagUserKeyName];
    }
    if ([dic objectForKey:kTagUserKeyLevelName]){
        NSString * levelname = [[dic objectForKey:kTagUserKeyLevelName] description];
        [self saveObject:levelname forKey:kTagUserKeyLevelName];
    }
    if ([dic objectForKey:kTagUserKeyUserLevel]){
        NSString * userlevel = [[dic objectForKey:kTagUserKeyUserLevel] description];
        [self saveObject:userlevel forKey:kTagUserKeyUserLevel];
    }
    if ([dic objectForKey:kTagUserKeySign]){
        NSString * sign = [[dic objectForKey:kTagUserKeySign] description];
        [self saveObject:sign forKey:kTagUserKeySign];
    }
    if ([dic objectForKey:kTagUserKeyPhone]){
        NSString * phone = [[dic objectForKey:kTagUserKeyPhone] description];
        [self saveObject:phone forKey:kTagUserKeyPhone];
    }
    if ([dic objectForKey:kTagUserKeyImageUrl]){
        NSString * imageUrl = [[dic objectForKey:kTagUserKeyImageUrl] description];
        [self saveObject:imageUrl forKey:kTagUserKeyImageUrl];
    }
    if ([dic objectForKey:kTagUserIsShowFillPage]){
        NSString * is_show_fill_page = [[dic objectForKey:kTagUserIsShowFillPage] description];
        [self saveObject:is_show_fill_page forKey:kTagUserIsShowFillPage];
    }
    
    if ([dic objectForKey:kTagPC_qrcode_url]){
        NSString * pc_qrcode_url = [[dic objectForKey:kTagPC_qrcode_url] description];
        [self saveObject:pc_qrcode_url forKey:kTagPC_qrcode_url];
    }
    
    if ([dic objectForKey:kTagCert_status]){
        NSString * cert_status = [[dic objectForKey:kTagCert_status] description];
        [self saveObject:cert_status forKey:kTagCert_status];
    }
    
    if ([dic objectForKey:kTagCar_cert_status]){
        NSString * car_cert_status = [[dic objectForKey:kTagCar_cert_status] description];
        [self saveObject:car_cert_status forKey:kTagCar_cert_status];
    }
    
    if ([dic objectForKey:kTagMerchant_cert_status]){
        NSString * merchant_cert_status = [[dic objectForKey:kTagMerchant_cert_status] description];
        [self saveObject:merchant_cert_status forKey:kTagMerchant_cert_status];
    }
    
    if ([dic objectForKey:kTagDriver_cert_status]){
        NSString * driver_cert_status = [[dic objectForKey:kTagDriver_cert_status] description];
        [self saveObject:driver_cert_status forKey:kTagDriver_cert_status];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kTagLoginFinishedNoti object:nil];
}

+ (void)clearUserData
{
    [self saveObject:nil forKey:kTagUserKeyID];
    [self saveObject:nil forKey:kTagUserKeySign];
    [self saveObject:nil forKey:kTagUserKeyName];
    [self saveObject:nil forKey:kTagUserKeyToken];
    [self saveObject:nil forKey:Tags_History_Array];
    [self saveObject:nil forKey:kTagUserKeyMobileState];
    [self saveObject:nil forKey:kTagUserKeyHeaderIndex];
    [self saveObject:nil forKey:kTagUserMaxVideoDuration];
    [self saveObject:nil forKey:kTagUserKeyWXStatus];
    [self saveObject:nil forKey:kTagUserKeySex];
    [self saveObject:nil forKey:kTagUserKeyLevelName];
    [self saveObject:nil forKey:kTagUserKeyUserLevel];
    [self saveObject:nil forKey:kTagUserKeySign];
    [self saveObject:nil forKey:kTagUserKeyPhone];
    [self saveObject:nil forKey:kTagUserKeyImageUrl];
    [self saveObject:nil forKey:kTagUserIsShowFillPage];
}


@end
