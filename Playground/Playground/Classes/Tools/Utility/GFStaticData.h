//
//  GFStaticData.h
//  StockMaster
//
//  Created by Rebloom on 14-8-11.
//  Copyright (c) 2014年 Rebloom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utility.h"

#define kTagUserRealName    @"kTagUserRealName"
#define kTagWeiboToken      @"KTagWeiboToken"

#define kTagPushPublish     @"kTagPushPublish" // 跳转到发布页的通知
#define IsShowedActivity    @"IsShowedActivity" // 是否展示过活动页


#define kTagWXToken         @"KTagWXToken"
#define kTagTencentToken    @"KTagTencentToken"
#define kTagRequestAccessToken  @"kTagRequestAccessToken"
#define kTagWXShareFinished @"kTagWXShareFinished" //根据code请求token
#define kTagWXLoginRefresh  @"kTagWXLoginRefresh"  //请求fast4接口后，刷新
#define kTagWeiXinLoginInfo @"kTagWeiXinLoginInfo" //登录信息
#define kTagWeiXinUserInfo  @"kTagWeiXinUserInfo" //微信个人信息
#define kTagIsWeiXinLogin   @"kTagIsWeiXinLogin" //从微信登录的

#define upLoadStatus @"upLoadStatus" // 上传状态
#define upLoadStatusFailure @"upLoadStatusFailure" // 上传失败
#define upLoadStatusUploading @"upLoadStatusUploading" // 上传中
#define upLoadStatusSuccess @"upLoadStatusSuccess" // 上传成功


#define kTagUserKeyID       @"uid"
#define kTagUserKeySex      @"sex"
#define kTagUserKeyPhone    @"mobile_encode"
#define kTagUserKeyMobileState   @"mobile_status"
#define kTagUserKeyWXStatus @"weixin_bind_status"
#define kTagUserKeyToken    @"token"
#define kTagUserKeyName     @"nickname"
#define kTagPC_qrcode_url     @"pc_qrcode_url"
#define kTagCert_status      @"cert_status" // =2 为付费商家
#define kTagUserKeyUserLevel     @"user_level"
#define kTagUserKeyLevelName     @"level_name"
#define kTagUserKeySign     @"autograph"
#define kTagUserKeyImageUrl @"header_url"
#define kTagUserKeyHeaderIndex @"header_index"
#define kTagUserIsShowFillPage   @"is_show_fill_page"
#define kTagUserMaxVideoDuration   @"max_video_duration"
#define kTagCar_cert_status @"car_cert_status"
#define kTagMerchant_cert_status    @"merchant_cert_status"
#define kTagDriver_cert_status  @"driver_cert_status"

#define kTagUserKeyClientID @"kTagUserKeyClientID"
#define kTagLoginFinishedNoti @"kTagLoginFinishedNoti"

#define kKefuUID        @"kKefuUID"
#define kKefuNickname   @"kKefuNickname"
#define kPublishIdelImageUrl   @"kPublishIdelImageUrl"

/* 如下是阿里用的临时凭证 */
#define ALi_AccessKeySecret_Video  @"AccessKeySecret_Video"
#define ALi_AccessKeyId_Video      @"AccessKeyId_Video"
#define ALi_SecurityToken_Video    @"SecurityToken_Video"
#define ALi_Expiration_Video       @"Expiration_Video"

#define ALi_AccessKeySecret_Image  @"AccessKeySecret_Image"
#define ALi_AccessKeyId_Image      @"AccessKeyId_Image"
#define ALi_SecurityToken_Image    @"SecurityToken_Image"
#define ALi_Expiration_Image       @"Expiration_Image"

#define is_First_Login             @"is_First_Login"//用户第一次安装
#define is_Push_Publish            @"is_Push_Publish"//是否跳转到发布页，如果跳转到，需要置空
#define Tags_History_Array         @"Tags_History_Array"//历史标签数组
#define Search_History_Array       @"Search_History_Array"//历史搜索数组

#define Push_Home_Segement         @"Push_Home_Segement" //跳转到首页哪个segement

#define Delete_Work_FeedID         @"Delete_Work_FeedID" // 删除个人作品的feed_id,退到上一个页面时，应该删除该作品，每次启动清空

#define Frome_ViewController       @"Frome_ViewController" // 从哪来的（首页、个人中心）

#define Current_Network            @"Current_Network"   // 当前网络状态
#define Last_Network               @"Last_Network"      // 之前网络状态

#define DeviceToken               @"DeviceToken"        // 设备号

#define Allow_4G_Play              @"Allow_4G_Play"     // 允许4G播放
#define Network_Available          @"Network_Available" // 有没有网

#define Draft_Feed_id              @"Draft_Feed_id"     // 存到本地草稿对应的feed_id

#define Attention_Feed_id          @"Attention_Feed_id" // 存到本地当前用户关注的feed_id列表

#define is_Show_Pic_Tip            @"is_Show_Pic_Tip"     // 是否显示过图文贴提示
#define is_Show_Video_Tip          @"is_Show_Video_Tip"   // 是否显示过视频提示

#define is_Show_AD                 @"is_Show_AD"          // 是否显示过开屏广告
#define ADStore                    @"ADStore"             // 存储开屏广告图片地址

#define LastADShowTime             @"LastADShowTime"      // 广告上一次显示的时间
#define LastADIndex                @"LastADIndex"         // 显示广告的顺序

#define REQUEST_USER               @"REQUEST_USER"
#define REQUEST_COMPREHENSIVE      @"REQUEST_COMPREHENSIVE"
#define RongYunUserID              @"RongYunUserID"

#define RongYunRecieveNofi         @"RongYunRecieveNofi"
#define RongYunChangeRoomNofi      @"RongYunChangeRoomNofi"
#define HideKeyboard               @"HideKeyboard"  // 用来通知IM收键盘用

#define LiveRequestNoti            @"LiveRequestNoti" // 重新请求直播状态
#define MonitorLiveNetworking      @"MonitorLiveNetworking" // 监听直播网络状态
#define NetworkUnAvailableNoti     @"NetworkUnAvailableNoti"// 没有网络的通知

#define VideoPlayFinished          @"videoPlayFinished" // 视频播放完成

#define CurrentTagsDictionary           @"CurrentTagsDictionary"// 当前标签模型（用于标签页发帖，直接带过去）
#define HomeUpdateVersion              @"HomeUpdateVersion" // 记录当前请求的版本号，如果下次请求不一致，就显示升级提示(首页用)
#define MineUpdateVersion              @"MineUpdateVersion" // 记录当前请求的最新版本号，个人中心点过就不显示点(个人中心用)

@interface GFStaticData : NSObject

+ (GFStaticData *)defaultDataCenter;

+ (BOOL)saveObject:(id)object forKey:(NSString *)key;

+ (id)getObjectForKey:(NSString *)key;

+ (BOOL)saveObject:(id)data forKey:(NSString *)key forUser:(NSString *)user;

+ (id)getObjectForKey:(NSString*)key forUser:(NSString*)user;

+ (void)saveLoginData:(NSDictionary *)dic;

+ (void)clearUserData;

+ (void)updateStockListInfo:(NSDictionary *)dic;

@end
