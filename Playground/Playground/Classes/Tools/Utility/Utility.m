//
//  Utility.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/29.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "Utility.h"
#import <UserNotifications/UserNotifications.h>
#import <AdSupport/AdSupport.h>

NSString * const KEY_UDID_INSTEAD = @"com.myapp.udid.test";

@implementation Utility
#define signedKey       @"FastF0rWard6]zQ#2O18(hu@N6P+"


static Utility * _sInst = nil;

+ (Utility*)instance{
    if (!_sInst) {
        _sInst = [[Utility alloc] init];
    }
    return _sInst;
}

+ (NSString *)getDeviceType
{
    struct utsname systemInfo;
    uname(&systemInfo); // 获取系统设备信息
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    NSDictionary *dict = @{
                           // iPhone
                           @"iPhone5,3" : @"iPhone 5c",
                           @"iPhone5,4" : @"iPhone 5c",
                           @"iPhone6,1" : @"iPhone 5s",
                           @"iPhone6,2" : @"iPhone 5s",
                           @"iPhone7,1" : @"iPhone 6 Plus",
                           @"iPhone7,2" : @"iPhone 6",
                           @"iPhone8,1" : @"iPhone 6s",
                           @"iPhone8,2" : @"iPhone 6s Plus",
                           @"iPhone8,4" : @"iPhone SE",
                           @"iPhone9,1" : @"iPhone 7",
                           @"iPhone9,2" : @"iPhone 7 Plus",
                           @"iPhone10,1" : @"iPhone 8",
                           @"iPhone10,4" : @"iPhone 8",
                           @"iPhone10,2" : @"iPhone 8 Plus",
                           @"iPhone10,5" : @"iPhone 8 Plus",
                           @"iPhone10,3" : @"iPhone X",
                           @"iPhone10,6" : @"iPhone X",
                           @"iPhone11,2" : @"iPhone XS",
                           @"iPhone11,4" : @"iPhone XS Max",
                           @"iPhone11,6" : @"iPhone XS Max",
                           @"iPhone11,8" : @"iPhone XR",
                           
                           @"iPhone12,1" : @"iPhone 11",
                           @"iPhone12,3" : @"iPhone 11 Pro",
                           @"iPhone12,5" : @"iPhone 11 Pro Max",
                           
                           @"i386" : @"iPhone Simulator",
                           @"x86_64" : @"iPhone Simulator",
                           // iPad
                           @"iPad4,1" : @"iPad Air",
                           @"iPad4,2" : @"iPad Air",
                           @"iPad4,3" : @"iPad Air",
                           @"iPad5,3" : @"iPad Air 2",
                           @"iPad5,4" : @"iPad Air 2",
                           @"iPad6,7" : @"iPad Pro 12.9",
                           @"iPad6,8" : @"iPad Pro 12.9",
                           @"iPad6,3" : @"iPad Pro 9.7",
                           @"iPad6,4" : @"iPad Pro 9.7",
                           @"iPad6,11" : @"iPad 5",
                           @"iPad6,12" : @"iPad 5",
                           @"iPad7,1" : @"iPad Pro 12.9 inch 2nd gen",
                           @"iPad7,2" : @"iPad Pro 12.9 inch 2nd gen",
                           @"iPad7,3" : @"iPad Pro 10.5",
                           @"iPad7,4" : @"iPad Pro 10.5",
                           @"iPad7,5" : @"iPad 6",
                           @"iPad7,6" : @"iPad 6",
                           // iPad mini
                           @"iPad2,5" : @"iPad mini",
                           @"iPad2,6" : @"iPad mini",
                           @"iPad2,7" : @"iPad mini",
                           @"iPad4,4" : @"iPad mini 2",
                           @"iPad4,5" : @"iPad mini 2",
                           @"iPad4,6" : @"iPad mini 2",
                           @"iPad4,7" : @"iPad mini 3",
                           @"iPad4,8" : @"iPad mini 3",
                           @"iPad4,9" : @"iPad mini 3",
                           @"iPad5,1" : @"iPad mini 4",
                           @"iPad5,2" : @"iPad mini 4",
                           };
    NSString *name = dict[platform];
    
    return name ? name : platform;
}

+ (NSString *)getDeviceSystemVersion
{
    return [NSString stringWithFormat:@"%@_%@",[UIDevice currentDevice].systemName,[UIDevice currentDevice].systemVersion];
}

+ (NSString *)getDeviceID
{
    NSUUID *security = [ASIdentifierManager sharedManager].advertisingIdentifier;
    return (security.UUIDString.length > 0) ? security.UUIDString : @"";
}

+ (NSString *)dictionaryDataToSignString:(NSDictionary *)orderDic
{
    NSString * signedString = @"";
    
    // 签名字符串键值以升序排列
    NSArray *dictAllKey = [orderDic allKeys];
    NSArray *sortAllKey = [dictAllKey sortedArrayUsingComparator:
                           ^NSComparisonResult(id obj1, id obj2)
                           {
                               return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
                           }];
    
    for (NSString * dictKey in sortAllKey)
    {
        id object = [orderDic objectForKey:dictKey];
        if ([object isKindOfClass:[NSString class]]) {
            NSString * value = (NSString *)object;
            signedString = [signedString stringByAppendingString:[NSString stringWithFormat:@"%@=%@&",dictKey,value]];
        }
        else if ([object isKindOfClass:[NSDictionary class]]) {
            // 签名字符串键值以升序排列
            NSDictionary * dic = (NSDictionary *)object;
            NSArray *tempDictKey = [dic allKeys];
            NSArray *sortDictKey = [tempDictKey sortedArrayUsingComparator:
                                    ^NSComparisonResult(id obj1, id obj2)
                                    {
                                        return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
                                    }];
            
            for (NSString * key in sortDictKey)
            {
                signedString = [signedString stringByAppendingString:[NSString stringWithFormat:@"%@_%@=%@&",dictKey, key,[dic objectForKey:key]]];
            }
        }
    }
    
    signedString = [signedString substringToIndex:signedString.length-1];
    
    signedString = [signedString stringByAppendingString:signedKey];
    
    signedString = [Utility md5:signedString];
    
    signedString = [signedKey stringByAppendingString:signedString];
    
    signedString = [Utility sha1:signedString];
    
    return signedString;
}


+ (NSMutableAttributedString *)stringToAttributeString:(NSString *)text{
    //先把普通的字符串text转化生成Attributed类型的字符串
    NSMutableAttributedString * attStr = [[NSMutableAttributedString alloc]initWithString:text];
    //正则表达式 ,例如  [(呵呵)] = 😑
    NSString * zhengze = @"\\[\\([a-zA-Z0-9\u4e00-\u9fa5]+\\)\\]";
    NSError * error;
    NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:zhengze options:NSRegularExpressionCaseInsensitive error:&error];
    if (!re)
    {
        //打印错误😓
        NSLog(@"error😓=%@",[error localizedDescription]);
    }
    NSArray * arr = [re matchesInString:text options:0 range:NSMakeRange(0, text.length)];//遍历字符串,获得所有的匹配字符串
    NSBundle *bundle = [NSBundle mainBundle];
    NSString * path = [bundle pathForResource:@"emj" ofType:@"plist"];  //plist文件,制作一个 数组,包含文字,表情图片名称
    NSArray * face = [[NSArray alloc]initWithContentsOfFile:path];//获取 所有的数组
    //如果有多个表情图，必须从后往前替换，因为替换后Range就不准确了
    for (int j =(int) arr.count - 1; j >= 0; j--) {
        //NSTextCheckingResult里面包含range
        NSTextCheckingResult * result = arr[j];
        for (int i = 0; i < face.count; i++) {
            if ([[text substringWithRange:result.range] isEqualToString:face[i][@"key"]])//从数组中的字典中取元素
            {
                NSString * imageName = [NSString stringWithString:face[i][@"picture"]];
                //添加附件,图片
                NSTextAttachment * textAttachment = [[NSTextAttachment alloc]init];
                //调节表情大小
                textAttachment.bounds=CGRectMake(0, 0, 20, 20);
                textAttachment.image = [UIImage imageNamed:imageName];
                NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:textAttachment];
                //替换未图片附件
                [attStr replaceCharactersInRange:result.range withAttributedString:imageStr];
                break;
            }
        }
    }
    return attStr;
}

+ (NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}


+ (NSString*)sha1:(NSString *)str
{
    const char *cstr = [str cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:str.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    if ([[Utility instance] is64bit])
    {
        CC_SHA1(data.bytes, (CC_LONG) data.length, digest);
    }
    else
    {
        CC_SHA1(data.bytes, (CC_LONG) data.length, digest);
    }
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

+ (BOOL)updateTimeForRow:(NSString *)lastTimeString {
    // 获取当前时时间戳 1466386762.345715 十位整数 6位小数
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 上次时间戳
    NSTimeInterval lastTime = [lastTimeString longLongValue]/1000;
    // 时间差
    NSTimeInterval time = currentTime - lastTime;
    
    NSInteger sec = time/60;
    // 大于40分钟
    if (sec >= 40) {
        
        [GFStaticData saveObject:@"NO" forKey:is_Show_AD];

        return YES;
    }
    
    return NO;
}

//获取当前时间戳
+ (NSString *)currentTimeStr{
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time=[date timeIntervalSince1970]*1000;// *1000 是精确到毫秒，不乘就是精确到秒
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    return timeString;
}


//对一个字符串进行base解码
+ (NSString *)base64decodeString:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string options:0];
    return [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
}

- (BOOL)is64bit
{
#if defined(__LP64__) && __LP64__
    return YES;
#else
    return NO;
#endif
}
@end
