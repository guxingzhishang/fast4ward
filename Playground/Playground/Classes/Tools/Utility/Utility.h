//
//  Utility.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/29.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sys/utsname.h>
#include <CommonCrypto/CommonDigest.h>

@interface Utility : NSObject

+ (Utility *)instance;

+ (NSString *)getDeviceType;
+ (NSString *)getDeviceSystemVersion;
+ (NSString *)getDeviceID;
+ (NSString *)dictionaryDataToSignString:(NSDictionary *)orderDic;
+ (NSString *)md5:(NSString *)str;
+ (NSString *)sha1:(NSString *)str;
+ (NSString *)base64decodeString:(NSString *)string;
+ (NSMutableAttributedString *)stringToAttributeString:(NSString *)text;

// 获取距离上次存储过了多少分钟
+ (BOOL)updateTimeForRow:(NSString *)lastTimeString;
// 获取当前时间戳
+ (NSString *)currentTimeStr;


/**
 本方法是得到 UUID 后存入系统中的 keychain 的方法
 不用添加 plist 文件
 程序删除后重装,仍可以得到相同的唯一标示
 但是当系统升级或者刷机后,系统中的钥匙串会被清空,此时本方法失效
 */
+(NSString *)getDeviceIDInKeychain;
@end
