//
//  AppDelegate.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWTabBarControllerConfig.h"
#import "WXApi.h"
#import "MWApi.h"
#import "DHWindow.h"
#import  <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,WXApiDelegate,RCIMUserInfoDataSource,RCIMGroupInfoDataSource, RCIMConnectionStatusDelegate, RCIMReceiveMessageDelegate,RCIMUserInfoDataSource,RCIMClientReceiveMessageDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) DHWindow *window;
@property (strong, nonatomic) FWTabBarControllerConfig *tabBarControllerConfig;
@property (strong, nonatomic) CYLTabBarController *tabBarController;
@property (strong, nonatomic) NSDictionary *Dict;

@end

