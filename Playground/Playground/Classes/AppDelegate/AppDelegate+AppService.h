//
//  AppDelegate+AppService.h
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/13.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#import "AppDelegate.h"
#import "FWTabBarViewModel.h"

@interface AppDelegate (AppService)
//初始化 window
-(void)initWindow;

//初始化网络配置
-(void)NetWorkConfig;

//初始化消息监听
-(void)initAPPNotification;

//调试(DEBUG)模式下的工具条
- (void)configDebugModelTools;

//单例
+ (AppDelegate *)shareAppDelegate;


@end
