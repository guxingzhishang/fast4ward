//
//  AppDelegate.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "AppDelegate.h"
#import "FWMatchViewController.h"
#import "FWConversationViewController.h"
#import "FWHotLiveListViewController.h"
#import "FWPlayerRankViewController.h"
#import "FWPlayerArchivesViewController.h"
#import "FWPlayerScoreDetailViewController.h"
#import "FWHotLiveListViewController.h"
#import "FWCarDetailViewController.h"
#import "FWFunsViewController.h"
#import "FWRegistrationDetailViewController.h"
#import "FWTicketDetailViewController.h"
#import "FWAskAndAnswerMessageViewController.h"


@interface AppDelegate ()
/* 是否进入后台了 */
@property (nonatomic, assign) BOOL  isBackGround;

/* 刚启动 */
@property (nonatomic, assign) BOOL  isFirstLaunch;

/* 是否支付成功 */
@property (nonatomic, assign) BOOL  isPayOK;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//    #if DEBUG
//        [[NSBundle bundleWithPath:@"/Applications/InjectionIII.app/Contents/Resources/iOSInjection.bundle"] load];
//    #endif

   
    if (![TalkingData handlePushMessage:launchOptions]) {
        // 非来自TalkingData的消息，可以在此处处理该消息。
        
        self.Dict = @{};
        
        //初始化window
        [self initWindow];
        
        //初始化消息监听
        [self initAPPNotification];
        
        
        UMessageRegisterEntity* entity = [[UMessageRegisterEntity alloc] init];
        entity.types= UMessageAuthorizationOptionBadge|UMessageAuthorizationOptionAlert|UMessageAuthorizationOptionSound;
        [UNUserNotificationCenter currentNotificationCenter].delegate=self;
        [UMessage registerForRemoteNotificationsWithLaunchOptions:launchOptions Entity:entity completionHandler:^(BOOL granted, NSError* _Nullable error) {
            
            if(granted) {
                // 用户选择了接收Push消息
            }else{
                // 用户拒绝接收Push消息
            }
        }];
        self.isFirstLaunch = YES;

        [UMessage setBadgeClear:NO];
        
        if (@available(iOS 10.0, *)) {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
                 if( !error ){
                     [[UIApplication sharedApplication] registerForRemoteNotifications];
                 }
             }];
        }

        if (application.applicationIconBadgeNumber > 0) {
            application.applicationIconBadgeNumber = 0;
        }
    }

    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    
    [UMessage registerDeviceToken:deviceToken];
    [TalkingData setDeviceToken:deviceToken];
    
    if (![deviceToken isKindOfClass:[NSData class]]) return;
    
     const unsigned *tokenBytes = (const unsigned *)[deviceToken bytes];
     NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                           ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                           ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                           ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
     NSLog(@"deviceToken:%@",hexToken);
    
     [[RCIMClient sharedRCIMClient] setDeviceToken:hexToken];
    
     [GFStaticData saveObject:hexToken forKey:DeviceToken];
}

/* 即将进入后台，不活跃状态 */
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.

    // 用于直播回看时网络切换
    NSString * currentWork = [GFStaticData getObjectForKey:Current_Network];
    [GFStaticData saveObject:currentWork forKey:Last_Network];
    
    [GFStaticData saveObject:@"NO" forKey:@"backgroundToShowLoading"];
}

/* 进入后台 */
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //是从后台进入，用于广告时间计算
    self.isBackGround = YES;
    
    // 用于直播回看时网络切换
    NSString * currentWork = [GFStaticData getObjectForKey:Current_Network];
    [GFStaticData saveObject:currentWork forKey:Last_Network];
}

/* 将要从后台进入前台 */
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    self.isPayOK = NO;

    [GFStaticData saveObject:@"YES" forKey:@"backgroundToShowLoading"];

//    if ([[GFStaticData getObjectForKey:@"missShow"] isEqualToString:@"没显示过"]) {
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//            UIViewController * currentViewController =  ((UINavigationController *)self.tabBarController.selectedViewController).topViewController;
//
//            if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusSuccess]) {
//                /* 发布/保存成功 */
//                NSDictionary * dict = [GFStaticData getObjectForKey:upLoadStatusSuccess];
//
//                NSString * tipTitle = @"帖子上传成功";
//                if ([dict[@"is_draft"] isEqualToString:@"1"]) {
//                    /* 草稿 */
//                    tipTitle = @"已保存到我的草稿";
//                }
//
//                [[FWHudManager sharedManager] showErrorMessage:tipTitle toController:currentViewController];
//            }else if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusFailure]) {
//                /* 发布/保存失败 */
//                NSDictionary * dict = [GFStaticData getObjectForKey:upLoadStatusFailure];
//
//                NSString * tipTitle = @"上传遇到了问题，请重新上传";
//                if ([dict[@"is_draft"] isEqualToString:@"1"]) {
//                    /* 草稿 */
//                    tipTitle = @"草稿保存失败，请稍后再试";
//                }
//
//                [[FWHudManager sharedManager] showErrorMessage:tipTitle toController:currentViewController];
//            }
//        });
//    }
    
    [application setApplicationIconBadgeNumber:0]; //清除角标

    NSString * order_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"订单号"];

    if (order_id && order_id.length > 0){
        /* 查询支付订单状态 */
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            /* 延迟0.5秒，待下面回调结束，在去查询订单状态， */
            if (!self.isPayOK) {

                FWMemberRequest * request = [[FWMemberRequest alloc] init];

                NSDictionary * params = @{
                                          @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                          @"pay_order_id":order_id,
                                          };

                [request startWithParameters:params WithAction:Get_pay_order_status WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

                    NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");

                    if ([code isEqualToString:NetRespondCodeSuccess]) {

                        if ([[back objectForKey:@"notify_status"] isEqualToString:@"1"]) {
                            /* 支付成功给一个通知 */
                            [[NSNotificationCenter defaultCenter] postNotificationName:FWWXReturnSussessPayNotification object:nil];
                        }else{
                            [[NSNotificationCenter defaultCenter] postNotificationName:FWSelfControlFailedPayNotification object:nil];
                        }
                    }
                }];
            }
        });
    }
}
/* 进入活跃状态 */
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.


    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:MonitorLiveNetworking object:nil];
    });

    NSString * lastTime = [GFStaticData getObjectForKey:LastADShowTime];
    
    if (lastTime && [Utility updateTimeForRow:lastTime]) {
        
        [GFStaticData saveObject:[Utility currentTimeStr] forKey:LastADShowTime];

        if (![[GFStaticData getObjectForKey:is_Show_AD] boolValue]) {
            
            [GFStaticData saveObject:@"YES" forKey:is_Show_AD];

            UIViewController * currentViewController =  ((UINavigationController *)self.tabBarController.selectedViewController).topViewController;

            if (self.isBackGround) {
                FWADViewController *vc = [[FWADViewController alloc] init];
                vc.navi = currentViewController.navigationController;
                [DHWindow addDHViewController:vc priority:4000];
            }
        }
    }
    
    self.isBackGround = NO;
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark ————— OpenURL 回调 —————
// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options{
    
    self.isPayOK = YES;
    
    BOOL result = YES;
    NSString *urlHost = url.host;
    
    if([urlHost isEqualToString:@"oauth"]||
       [urlHost isEqualToString:@"platformId=wechat"]){
        // 微信 (授权、分享)
        result = [WXApi handleOpenURL:url delegate:(id<WXApiDelegate>)self];
    }else if ([urlHost isEqualToString:@"s4ppta.mlinks.cc"]||
              [urlHost isEqualToString:@"openTagsPage"]||
              [urlHost isEqualToString:@"openUserInfoPage"]||
              [urlHost isEqualToString:@"openGraphPage"]||
              [urlHost isEqualToString:@"openVideoPage"]||
              [urlHost isEqualToString:@"openDriverPage"]||
              [urlHost isEqualToString:@"openScoreDetail"] ||
              [urlHost isEqualToString:@"openLiveListPage"] ||
              [urlHost isEqualToString:@"openTicketDetail"] ||
              [urlHost isEqualToString:@"openArticleDetail"] ||
              [urlHost isEqualToString:@"openCarDetail"]||
              [urlHost isEqualToString:@"openBaoMingDetail"])
    {
        // 魔窗
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self judgeURL:url params:nil];
//        });
    }else if ([url.host isEqualToString:@"pay"]) {
        // 微信支付
        result = [WXApi handleOpenURL:url delegate:(id<WXApiDelegate>)self];
    }else if ([url.host isEqualToString:@"safepay"]) {
#warning 当前版本先不显示,以后删掉下面的这个布尔值

//        // 支付宝支付
//        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//            NSLog(@"result = %@",resultDic);
//            if ([[resultDic objectForKey:@"resultStatus"] isEqualToString:@"9000"]) {
//
//                [[NSNotificationCenter defaultCenter] postNotificationName:FWWXReturnSussessPayNotification object:nil];
//            }else{
//
//                [[NSNotificationCenter defaultCenter] postNotificationName:FWWXReturnFailedPayNotification object:nil];
//            }
//        }];
    }
    
    return result;
}

#pragma mark - 魔窗
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler{
    
    //如果使用了Universal link ，此方法必写
//    NSLog(@"----%@",userActivity.webpageURL.host);
    
    return [MWApi continueUserActivity:userActivity];
}

- (void)judgeURL:(NSURL*)url params:(NSDictionary *)params {
    
    NSString * urlStrin = [url absoluteString];
    
    
//    UINavigationController * nav = self.tabBarControllerConfig.viewControllers[self.tabBarController.selectedIndex];
    UIViewController * currentViewController =  ((UINavigationController *)self.tabBarController.selectedViewController).topViewController;
    
    //包含ABeh(iOS9 以上) 或openTagsPage（iOS 9以下） 跳转到标签页
    if (([urlStrin rangeOfString:@"ABeh"].location != NSNotFound) ||
        ([urlStrin rangeOfString:@"openTagsPage"].location != NSNotFound))
    {
        if (![GFStaticData getObjectForKey:kTagUserKeyID])
        {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
        }
        else
        {
            if (![currentViewController isKindOfClass:[FWTagsViewController class]])
            {
                //跳转到标签页
                FWTagsViewController *vc = [[FWTagsViewController alloc] init];
                vc.tags_id = params[@"tag_id"];
                [currentViewController.navigationController pushViewController:vc animated:YES];
            }
        }
    }
    
    //包含ABei(iOS9 以上) 或openUserInfoPage（iOS 9以下） 跳转到用户页
    if (([urlStrin rangeOfString:@"ABei"].location != NSNotFound) ||
        ([urlStrin rangeOfString:@"openUserInfoPage"].location != NSNotFound))
    {
        if (![GFStaticData getObjectForKey:kTagUserKeyID])
        {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
        }
        else
        {
            if ([params[@"uid"] isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
                // 跳转到个人页
                if (![currentViewController isKindOfClass:[FWNewMineViewController class]])
                {
                    self.tabBarController.selectedIndex = 3;
                }
            }else{
                if (![currentViewController isKindOfClass:[FWNewUserInfoViewController class]])
                {
                    // 跳转到用户页
                    FWNewUserInfoViewController *vc = [[FWNewUserInfoViewController alloc] init];
                    vc.user_id = params[@"uid"];
                    [currentViewController.navigationController pushViewController:vc animated:YES];
                }
            }
        }
    }
    
    // 跳转成绩详情
    if (([urlStrin rangeOfString:@"openScoreDetail"].location != NSNotFound)||
        ([urlStrin rangeOfString:@"openDriverPage"].location != NSNotFound)){
        if (![currentViewController isKindOfClass:[FWPlayerRankViewController class]]){
            
            FWPlayerRankViewController *controller = [[FWPlayerRankViewController alloc] init];
            [currentViewController.navigationController pushViewController:controller animated:YES];
        }
    }
   
    // 跳转门票订单详情
    if (([urlStrin rangeOfString:@"openTicketDetail"].location != NSNotFound)){
        if (![currentViewController isKindOfClass:[FWTicketDetailViewController class]]){

            NSArray * firstArray = [urlStrin componentsSeparatedByString:@"ticket_id="];
            NSArray * secondArray = [firstArray[1] componentsSeparatedByString:@"&"];
            if ([secondArray[0] length] > 0) {
                FWTicketDetailViewController *controller = [[FWTicketDetailViewController alloc] init];
                controller.ticket_id = secondArray[0];
                [currentViewController.navigationController pushViewController:controller animated:YES];
            }
        }
    }
    
    // 跳转直播列表页
    if ([urlStrin rangeOfString:@"openLiveListPage"].location != NSNotFound){
        if (![currentViewController isKindOfClass:[FWHotLiveListViewController class]]){
            
            FWHotLiveListViewController *controller = [[FWHotLiveListViewController alloc] init];
            [currentViewController.navigationController pushViewController:controller animated:YES];
        }
    }
    
    //跳转到文章帖(当前版本唤起app即可)
    if ([urlStrin rangeOfString:@"openArticleDetail"].location != NSNotFound)
    {
        if (![currentViewController isKindOfClass:[FWArticalDetailViewController class]]){

            NSArray * firstArray = [urlStrin componentsSeparatedByString:@"feed_id="];
            NSArray * secondArray = [firstArray[1] componentsSeparatedByString:@"&"];
            if ([secondArray[0] length] > 0) {
                FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
                DVC.feed_id = secondArray[0];
                [currentViewController.navigationController pushViewController:DVC animated:YES];
            }
        }
    }
    
    //座驾详情
    if ([urlStrin rangeOfString:@"openCarDetail"].location != NSNotFound)
    {
        if (![currentViewController isKindOfClass:[FWCarDetailViewController class]]){
            
            NSArray * firstArray = [urlStrin componentsSeparatedByString:@"user_car_id="];
            NSArray * secondArray = [firstArray[1] componentsSeparatedByString:@"&"];
            if ([secondArray[0] length] > 0) {
                FWCarDetailViewController * DVC = [[FWCarDetailViewController alloc] init];
                DVC.user_car_id = secondArray[0];
                [currentViewController.navigationController pushViewController:DVC animated:YES];
            }
        }
    }
    
    //报名详情
    if ([urlStrin rangeOfString:@"openBaoMingDetail"].location != NSNotFound)
    {
        if (![currentViewController isKindOfClass:[FWRegistrationDetailViewController class]]){
            
            NSArray * firstArray = [urlStrin componentsSeparatedByString:@"baoming_id="];
            NSArray * secondArray = [firstArray[1] componentsSeparatedByString:@"&"];
            if ([secondArray[0] length] > 0) {
                FWRegistrationDetailViewController * DVC = [[FWRegistrationDetailViewController alloc] init];
                DVC.baoming_id = secondArray[0];
                [currentViewController.navigationController pushViewController:DVC animated:YES];
            }
        }
    }
   
    //跳转到图文贴(当前版本唤起app即可)
    if (([urlStrin rangeOfString:@"ABeY"].location != NSNotFound) ||
        ([urlStrin rangeOfString:@"openGraphPage"].location != NSNotFound))
    {
        if (![GFStaticData getObjectForKey:kTagUserKeyID])
        {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
        }
    }
    
    //跳转到视频贴(当前版本唤起app即可)
    if (([urlStrin rangeOfString:@"ABeM"].location != NSNotFound) ||
        ([urlStrin rangeOfString:@"openVideoPage"].location != NSNotFound))
    {
        if (![GFStaticData getObjectForKey:kTagUserKeyID]){
            
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
        }
    }
}

// 微信分享到朋友圈->通知页面
-(void)onResp:(BaseResp *)resp{
    
    if([resp isKindOfClass:[PayResp class]]){
        switch (resp.errCode) {
            case WXSuccess:{
//                NSLog(@"支付成功");
                [[NSNotificationCenter defaultCenter] postNotificationName:FWWXReturnSussessPayNotification object:resp];
            }
                break;
            default:{
//                NSLog(@"支付失败:%d",resp.errCode);
                [[NSNotificationCenter defaultCenter] postNotificationName:FWWXReturnFailedPayNotification object:resp];
            }
                break;
        }
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:kTagWXShareFinished object:resp];
    }
}


/**
 收到推送的回调
 @param application  UIApplication 实例
 @param userInfo 推送时指定的参数
 @param completionHandler 完成回调
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    if (![TalkingData handlePushMessage:userInfo]) {
        // 非来自TalkingData的消息，可以在此处处理该消息。
        
        [UMessage setAutoAlert:NO];
        //统计点击数
        [UMessage didReceiveRemoteNotification:userInfo];
        if([[[UIDevice currentDevice] systemVersion]intValue] < 10){
            [UMessage didReceiveRemoteNotification:userInfo];
            completionHandler(UIBackgroundFetchResultNewData);
        }
        
//        NSLog(@"userInfo---%@",userInfo);
         [self pushToTargetViewController:userInfo];
    }
}

-(void)userNotificationCenter:(UNUserNotificationCenter*)center willPresentNotification:(UNNotification*)notification withCompletionHandler:(void(^)(UNNotificationPresentationOptions))completionHandler{
    
    NSDictionary* userInfo = notification.request.content.userInfo;
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        
        //应用处于前台时的远程推送接受
        
        //关闭U-Push自带的弹出框
        [UMessage setAutoAlert:NO];
        //（前台、后台）的消息处理
        [UMessage didReceiveRemoteNotification:userInfo];
    }else{
        //应用处于前台时的本地推送接受
    }
    //当应用处于前台时提示设置，需要哪个可以设置哪一个
    completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionAlert);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler{
    

    NSDictionary * userInfo = response.notification.request.content.userInfo;
    
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        
        //应用处于后台时的远程推送接受
        
        //（前台、后台）的消息处理
        [UMessage didReceiveRemoteNotification:userInfo];
        
        [self pushToTargetViewController:userInfo];
    }
    else{ //应用处于后台时的本地推送接受
    }
}

/**
 * 1:赛事主页   2:直播页  3:回放页   4:文章详情页  5:图文详情页   6:话题页  7:商家店铺页  8:商品详情页   9:H5 10:商家排行榜   11：车友排行榜   12:无跳转  13：视频贴 14：车手排名     15 直播列表   16：长视频详情页  17：问答   18：改装分享   21 点赞   22关注  23 评论  24 问答消息  25 @我
 */
#pragma mark - > push消息点击跳转事件
- (void)pushToTargetViewController:(NSDictionary *)userInfo{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        UIViewController * currentViewController =  ((UINavigationController *)self.tabBarController.selectedViewController).topViewController;
        
        NSString * click_type = @"click_type";
        NSString * val        = @"val";
        
        if (userInfo[click_type] && userInfo[val]) {
            
            if ([userInfo[click_type] isEqualToString:@"1"]){
                /* 赛事主页 */
//                FWMatchViewController * MVC = [[FWMatchViewController alloc] init];
//                [currentViewController.navigationController pushViewController:MVC animated:YES];
                currentViewController.navigationController.tabBarController.selectedIndex = 1;
            }else if ([userInfo[click_type] isEqualToString:@"4"]){
                /* 文章详情页 */
                if (![currentViewController isKindOfClass:[FWArticalDetailViewController class]]){
                    FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
                    DVC.feed_id = userInfo[val];
                    [currentViewController.navigationController pushViewController:DVC animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"5"]){
                /* 图文详情页 */
                if (![currentViewController isKindOfClass:[FWGraphTextDetailViewController class]]){
                    FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
                    TVC.feed_id = userInfo[val];
                    TVC.requestType = FWPushMessageRequestType;
                    [currentViewController.navigationController pushViewController:TVC animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"6"]){
                /* 标签（话题）页 */
                if (![currentViewController isKindOfClass:[FWTagsViewController class]]){
                    FWTagsViewController *vc = [[FWTagsViewController alloc] init];
                    vc.tags_id = userInfo[val];
                    [currentViewController.navigationController pushViewController:vc animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"7"]) {
                /* 商家店铺页*/
                if (![currentViewController isKindOfClass:[FWCustomerShopViewController class]]){
                    FWCustomerShopViewController * CSVC = [[FWCustomerShopViewController alloc] init];
                    CSVC.user_id = userInfo[val];;
                    CSVC.isUserPage = NO;
                    [currentViewController.navigationController pushViewController:CSVC animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"8"]) {
                /* 商品详情页 */
                if (![currentViewController isKindOfClass:[FWGoodsDetailViewController class]]){
                    FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
                    GDVC.goods_id = userInfo[val];;
                    [currentViewController.navigationController pushViewController:GDVC animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"9"]){
                /* 打开H5活动页 */
                FWWebViewController * WVC = [[FWWebViewController alloc] init];
                WVC.pageType = WebViewTypeURL;
                WVC.htmlStr = userInfo[val];
                [currentViewController.navigationController pushViewController:WVC animated:YES];
            }else if ([userInfo[click_type] isEqualToString:@"13"]){
                /* 视频详情页 */
                if (![currentViewController isKindOfClass:[FWVideoPlayerViewController class]]){
                    FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
                    controller.currentIndex = 0;
                    controller.feed_id = userInfo[val];
                    controller.requestType = FWPushMessageRequestType;
                    [currentViewController.navigationController pushViewController:controller animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"14"]){
                /* 车手排名 */
                if (![currentViewController isKindOfClass:[FWPlayerRankViewController class]]){
                    FWPlayerRankViewController *controller = [[FWPlayerRankViewController alloc] init];
                    [currentViewController.navigationController pushViewController:controller animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"15"]){
                /* 直播列表 */
                if (![currentViewController isKindOfClass:[FWHotLiveListViewController class]]){
                    FWHotLiveListViewController *controller = [[FWHotLiveListViewController alloc] init];
                    [currentViewController.navigationController pushViewController:controller animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"16"]){
                /* 长视频详情页 */
                if (![currentViewController isKindOfClass:[FWLongVideoPlayerViewController class]]){
                    FWLongVideoPlayerViewController *controller = [[FWLongVideoPlayerViewController alloc] init];
                    controller.feed_id = userInfo[val];
                    controller.myBlock = ^(FWFeedListModel *listModel) {};
                    [currentViewController.navigationController pushViewController:controller animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"17"]){
                /* 问答详情 */
                FWAskAndAnswerDetailViewController * controller = [[FWAskAndAnswerDetailViewController alloc] init];
                controller.feed_id = userInfo[val];;
                [currentViewController.navigationController pushViewController:controller animated:YES];
            }else if ([userInfo[click_type] isEqualToString:@"18"]){
                /* 改装分享 */
                FWRefitCaseDetailViewController * controller = [[FWRefitCaseDetailViewController alloc] init];
                controller.feed_id = userInfo[val];;
                [currentViewController.navigationController pushViewController:controller animated:YES];
            }else if ([userInfo[click_type] isEqualToString:@"21"]){
                /* 点赞 */
                FWThumbUpViewController * controller = [[FWThumbUpViewController alloc] init];
                controller.listType = FWMessageThumbUpList;
                [currentViewController.navigationController pushViewController:controller animated:YES];
            }else if ([userInfo[click_type] isEqualToString:@"22"]){
                /* 关注 */
                if (![currentViewController isKindOfClass:[FWFunsViewController class]]){
                    FWFunsViewController * controller = [[FWFunsViewController alloc] init];
                    controller.funsType = @"1";
                    controller.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
                    [currentViewController.navigationController pushViewController:controller animated:YES];
                }
            }else if ([userInfo[click_type] isEqualToString:@"23"]){
                /* 评论 */
                FWThumbUpViewController * controller = [[FWThumbUpViewController alloc] init];
                controller.listType = FWMessageCommentList;
                [currentViewController.navigationController pushViewController:controller animated:YES];
            }else if ([userInfo[click_type] isEqualToString:@"24"]){
                /* 问答（消息） */
                FWAskAndAnswerMessageViewController * controller = [[FWAskAndAnswerMessageViewController alloc] init];
                controller.msg_type = @"4";
                controller.titleString = @"问答消息";
                [currentViewController.navigationController pushViewController:controller animated:YES];
            }else if ([userInfo[click_type] isEqualToString:@"25"]){
                /* @我（消息） */
                FWAskAndAnswerMessageViewController * controller = [[FWAskAndAnswerMessageViewController alloc] init];
                controller.msg_type = @"5";
                controller.titleString = @"@我";
                [currentViewController.navigationController pushViewController:controller animated:YES];
            }else {
                /* 打开app */
            }
        }
        
        /* 融云推送 */
        if ([[userInfo[@"rc"] objectForKey:@"cType"] isEqualToString:@"PR"]) {
            /* 单聊 */
            
            FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:[userInfo[@"rc"] objectForKey:@"fId"]];
            
            [currentViewController.navigationController pushViewController:CVC animated:YES];
        }
    });
}

/* 本地通知（接收融云消息） */
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{

    NSDictionary * userInfo = notification.userInfo;

    /* 只有不是刚启动，才调用本方法 */
    if ([[userInfo[@"rc"] objectForKey:@"cType"] isEqualToString:@"PR"]) {
        [self pushToTargetViewController:userInfo];
    }
}
@end
