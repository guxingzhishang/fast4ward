//
//  AppDelegate+AppService.m
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/13.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#import "AppDelegate+AppService.h"
#import "AFNetworking.h"

//#import "RouterManager.h"

#import "FWNavigationController.h"
#import "FWMatchViewController.h"

#import "FWLoginViewController.h"
#import "CYLPlusButtonSubclass.h"
#import "TalkingData.h"
#import "MWApi.h"

#import "RCCRRongCloudIMManager.h"
#import "RCChatroomWelcome.h"
#import "RCChatroomStart.h"
#import "RCChatroomEnd.h"
#import "RCChatroomUserBan.h"
#import "RCChatroomUserUnBan.h"
#import "RCChatroomUserBlock.h"
#import "RCChatroomUserUnBlock.h"
#import "RCChatroomNotification.h"
#import "RCCRManager.h"

#define DEVICE_IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define RANDOM_COLOR [UIColor colorWithHue: (arc4random() % 256 / 256.0) saturation:((arc4random()% 128 / 256.0 ) + 0.5) brightness:(( arc4random() % 128 / 256.0 ) + 0.5) alpha:1]

@implementation AppDelegate (AppService)
#pragma mark ————— 初始化服务 —————
//初始化注册网络状态监听
-(void)initService{

}

#pragma mark ————— 初始化window —————
-(void)initWindow{

    self.window = [[DHWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = DHTitleColor_FFFFFF;
    
    // 重置rootViewController
    [self resetWindowRootViewController];

    [self.window makeKeyAndVisible];

    //新手引导版本号
    [[NSUserDefaults standardUserDefaults] setValue:GuidePageVersionConfig forKey:DHGuidePageVersionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

    /* 每次启动，播放视频时都要重新问 */
    [GFStaticData saveObject:@"0" forKey:Allow_4G_Play];

    /* 每次启动，都置空关注列表 */
    [GFStaticData saveObject:nil forKey:Attention_Feed_id];

    /* 每次启动，都置空是否显示过开屏广告 */
    [GFStaticData saveObject:nil forKey:is_Show_AD];

    /* 每次启动，都将上次展示广告的时间设置成当前时间 */
    [GFStaticData saveObject:[Utility currentTimeStr] forKey:LastADShowTime];
    
    /* 用于直播或回放时，网络切换 */
    [GFStaticData saveObject:nil forKey:Last_Network];

    /* 记录上传状态，每次重新启动后，都标记成功（上传中禁止再次上传和退出） */
    [GFStaticData saveObject:upLoadStatusSuccess forKey:upLoadStatus];
    
    /* 每次重新启动清空deviceToken */
    [GFStaticData saveObject:nil forKey:DeviceToken];

    /* 每次进来先置成YES，允许提示上传成功、失败，只有置到后台，才设置成NO，回到前台切回YES */
    [GFStaticData saveObject:@"YES" forKey:@"backgroundToShowLoading"];

    /* 每次启动，未显示上传/保存成功或失败提示，置空 */
    [GFStaticData saveObject:nil forKey:@"missShow"];

    /* 每次启动，将正在上传置空，只有在上传视频/图片时，置成YES， */
    [GFStaticData saveObject:nil forKey:@"isUploading"];
    
    /* 每次启动，将发布传车系用的置空， */
    [GFStaticData saveObject:nil forKey:ChooseCarTypeBackToPublish];

    
    if (![GFStaticData getObjectForKey:kTagUserKeyID]) {
        /* 如果用户有UID则直接跳过
           如果没有UID，则判断是否第一次安装，如果是，清空uid，去登录页，
                                        如果不是，uid设置0，去首页*/
        [GFStaticData saveObject:@"0" forKey:kTagUserKeyID];
        [GFStaticData saveObject:@"0" forKey:kTagUserKeyName];

        if (![GFStaticData getObjectForKey:is_First_Login]) {
            [GFStaticData saveObject:@"YES" forKey:is_First_Login];
            [GFStaticData saveObject:nil forKey:kTagUserKeyID];
        }
    }else if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]){
        
        [GFStaticData saveObject:@"0" forKey:kTagUserKeyID];
        [GFStaticData saveObject:@"0" forKey:kTagUserKeyName];
    }
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]&&
        ![[GFStaticData getObjectForKey:kTagUserKeyMobileState] isEqualToString:@"1"]) {
        /* 针对微信登录，但是杀死app没有绑定手机的用户 */
        /* 给予游客身份 */
        [GFStaticData saveObject:@"0" forKey:kTagUserKeyID];
        [GFStaticData saveObject:@"0" forKey:kTagUserKeyName];
    }

    // 注册微信
    [WXApi registerApp:WXAppKey];

    // 注册TD （渠道 ID: 是渠道标识符，可通过不同渠道单独追踪数据）
    [TalkingData setLogEnabled:YES];
    [TalkingData setExceptionReportEnabled:YES];
    [TalkingData setSignalReportEnabled:YES];
    
    [TalkingData sessionStarted:TKAppKey withChannelId:TDChannel];


    // 初始化魔窗
    [MWApi registerApp:MWAppKey];

    // 初始化融云
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] initRongCloud:RongyunAppKey];
    [[RCIM sharedRCIM] initWithAppKey:RongyunAppKey];

    //开启消息撤回功能
    [RCIM sharedRCIM].enableMessageRecall = YES;
    
    // 设置代理,代理方法要返回用户信息
    [[RCIM sharedRCIM] setUserInfoDataSource:self];
    
    // 友盟推送
    [UMConfigure initWithAppkey:UMengAppKey channel:TDChannel];

    [RCIM sharedRCIM].disableMessageAlertSound = YES;
    
    //注册自定义消息
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] registerRongCloudMessageType:[RCChatroomWelcome class]];
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] registerRongCloudMessageType:[RCChatroomStart class]];
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] registerRongCloudMessageType:[RCChatroomEnd class]];
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] registerRongCloudMessageType:[RCChatroomUserBlock class]];
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] registerRongCloudMessageType:[RCChatroomUserUnBlock class]];
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] registerRongCloudMessageType:[RCChatroomNotification class]];
}

#pragma mark - >  RCIMUserInfoDataSource的代理方法（以下注释是去提示用的---在类簇中的方法可能会被覆盖的警告）
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"
- (void)getUserInfoWithUserId:(NSString *)userId completion:(void (^)(RCUserInfo *))completion {
    // 每次展示用户头像等信息都会调用,正常开发应该从app服务器获取,保存在本地
//    NSLog(@"userId===%@",userId);

    if (!userId) {
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"f_uid":userId};
    
    [request startWithParameters:params WithAction:Get_user_profile  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            NSDictionary * infoDict = [[back objectForKey:@"data"] objectForKey:@"user_info"];
            RCUserInfo *info = [[RCUserInfo alloc] initWithUserId:userId name:infoDict[@"nickname"] portrait:infoDict[@"header_url"]];
            completion(info);
        }
    }];
}
#pragma clang diagnostic pop


#pragma mark - 创建Window根控制器
-(void)resetWindowRootViewController{
    UIViewController *viewController=[self createInitialViewController];

    if (![viewController isKindOfClass:[UINavigationController class]] &&
        ![viewController isKindOfClass:[CYLTabBarController class]]) {

        FWNavigationController *navigationController = [[FWNavigationController alloc] initWithRootViewController:viewController];
        [self.window setRootViewController:navigationController];

    }else{
         [self.window setRootViewController:viewController];
    }
}

-(UIViewController *)createInitialViewController{

    //判断是否进入注册登录页面\ 目前直接进入主页
    
    if (![GFStaticData getObjectForKey:kTagUserKeyID]||
        [[GFStaticData getObjectForKey:kTagUserKeyName] description].length<=0){
       
        FWLoginViewController * loginViewController =[MGJRouter objectForURL:LoginKey];
        return loginViewController;
    }else{
        FWTabBarViewModel *tabBarViewModel= [[FWTabBarViewModel alloc] init];
        
        [CYLPlusButtonSubclass registerPlusButton];

        self.tabBarControllerConfig = [[FWTabBarControllerConfig alloc] initWithViewModel:tabBarViewModel];
        self.tabBarController = self.tabBarControllerConfig.tabBarController;
        
        [self changeLineOfTabbarColor];
        [self changeTabbarColor];

        if ([self.Dict[@"rootViewController"] isEqualToString:@"match"]) {
            self.tabBarController.selectedIndex = 1;
        }else if ([self.Dict[@"rootViewController"] isEqualToString:@"message"]) {
            self.tabBarController.selectedIndex = 2;
        }else if ([self.Dict[@"rootViewController"] isEqualToString:@"mine"]) {
            self.tabBarController.selectedIndex = 3;
        }
        return self.tabBarController ;
    }
}

- (void)pushToLoginViewController:(NSString *)lastIndex{
   
    UINavigationController * nav = self.tabBarControllerConfig.viewControllers[self.tabBarController.selectedIndex];
    
    if (![nav.visibleViewController isKindOfClass:[FWLoginViewController class]]){
        UIViewController * currentViewController =  ((UINavigationController *)self.tabBarController.selectedViewController).topViewController;
        
        FWSubLoginViewController * loginViewController = [[FWSubLoginViewController alloc] init];
        loginViewController.lastIndex = lastIndex;
        [GFStaticData saveObject:@"0" forKey:kTagUserKeyID];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.4];
        [animation setType: kCATransitionMoveIn];
        [animation setSubtype: kCATransitionFromTop];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        [currentViewController.navigationController pushViewController:loginViewController animated:NO];
        [currentViewController.navigationController.view.layer addAnimation:animation forKey:nil];
    }
}


//修改tabBar顶部横线颜色
- (void)changeLineOfTabbarColor {
    CGRect rect = CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, 0.5);
    UIGraphicsBeginImageContextWithOptions(rect.size,NO, 0);
    CGContextRef context =UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect);
    UIImage *image =UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.tabBarController.tabBar setShadowImage:image];
    [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
    
    self.tabBarController.tabBar.layer.shadowColor = FWViewBackgroundColor_F1F1F1.CGColor;
    self.tabBarController.tabBar.layer.shadowOffset = CGSizeMake(0, -3);
    self.tabBarController.tabBar.layer.shadowOpacity = 0.4;
}

//修改tabBar背景色
- (void)changeTabbarColor {
    CGRect frame;
    UIView *tabBarView = [[UIView alloc] init];
    tabBarView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    if (DEVICE_IS_IPHONE_X) {
        frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 83);
    }else {
        frame = self.tabBarController.tabBar.bounds;
    }
    tabBarView.frame = frame;
    [[UITabBar appearance] insertSubview:tabBarView atIndex:0];
}

+ (AppDelegate *)shareAppDelegate{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - 初始化消息监听
- (void)initAPPNotification
{
    @weakify(self);
    /// 监听切换根控制器的通知
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:DHSwitchRootViewControllerNotification object:nil] subscribeNext:^(NSNotification * noti) {
        @strongify(self);
        self.Dict = (NSDictionary *)(noti.userInfo);

        if ([self.Dict[@"rootViewController"] isEqualToString:Login_with_pop]) {
         
            if ([self.Dict[@"last_index"] integerValue] >= 0) {
                [self pushToLoginViewController:self.Dict[@"last_index"]];
            }else{
                [self pushToLoginViewController:0];
            }
            return ;
        }
        
        [self resetWindowRootViewController];
    }];

}



#pragma mark ————— 配置第三方 —————
-(void)configUSharePlatforms{
    /* 设置微信的appKey和appSecret */
    //    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:kAppKey_Wechat appSecret:kSecret_Wechat redirectURL:nil];

}


@end
