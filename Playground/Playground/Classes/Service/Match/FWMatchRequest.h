//
//  FWMatchRequest.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/23.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWNetworkRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchRequest : FWNetworkRequest

@end

NS_ASSUME_NONNULL_END
