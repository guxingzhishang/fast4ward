//
//  FWMemberRequest.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWNetworkRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMemberRequest : FWNetworkRequest

@end

NS_ASSUME_NONNULL_END
