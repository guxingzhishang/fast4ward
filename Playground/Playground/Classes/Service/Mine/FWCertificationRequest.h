//
//  FWCertificationRequest.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 认证的相关请求
 */
#import "FWNetworkRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCertificationRequest : FWNetworkRequest

@end

NS_ASSUME_NONNULL_END
