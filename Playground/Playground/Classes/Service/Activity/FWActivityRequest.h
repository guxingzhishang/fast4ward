//
//  FWActivityRequest.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/12.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWNetworkRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWActivityRequest : FWNetworkRequest

@end

NS_ASSUME_NONNULL_END
