//
//  CommonService.m
//  Project_Model
//
//  Created by GaoFeng on 9/8/16.
//  Copyright © 2016 北京搭伙科技. All rights reserved.
//
#import <objc/runtime.h>
#import "CommonService.h"

#define NSUserDefaultKey  @"UserDefaultsVariableModelV1.2.0"

static NSLock *_lock;
@implementation CommonService
#pragma mark - UILabel的高度自适应,根据宽度和字体
/**
 UILabel的高度自适应,根据宽度和字体

 @param width 宽度
 @param title Text
 @param font 字体
 @return 自适应的高度
 */
+ (CGFloat)getHeightByWidth:(CGFloat)width title:(NSString *)title font:(UIFont *)font {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    label.text = title;
    label.font = font;
    label.numberOfLines = 0;
    [label sizeToFit];
    CGFloat height = label.frame.size.height;
    return height;
}
#pragma mark - UILabel的宽度自适应，根据字体
/**
 UILabel的宽度自适应，根据字体

 @param title Text
 @param font 字体
 @return 自适应的宽度
 */
+ (CGFloat)getWidthWithTitle:(NSString *)title font:(UIFont *)font {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1000, 0)];
    label.text = title;
    label.font = font;
    [label sizeToFit];
    return label.frame.size.width;
}

#pragma mark - Hook Utility
/**
 替换Selector
 @param cls 要替换的class
 @param originalSelector 原始的 Selector
 @param swizzledSelector swizzled Selector
 */
+ (void)swizzlingInClass:(Class)cls originalSelector:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector{
    Class class = cls;
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    BOOL didAddMethod = class_addMethod(class, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    if(didAddMethod){
        class_replaceMethod(class, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    }
    else{
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}
#pragma mark - 组合一个图片到另一个图片
/**
 组合一个图片到另一个图片
 @param smallImg 小图
 @param bigImg 大图
 @param drawPoint 小图画的位置
 @param contextSize 画布大小
 @return 合并后的图
 */
+ (UIImage *)composeImgWithImage:(UIImage *)smallImg toImage:(UIImage *)bigImg andSmallDrawPoint:(CGPoint)drawPointS andContextSize:(CGSize) contextSize{

    //以bigImg的图大小为画布创建上下文
    UIGraphicsBeginImageContext(contextSize);
    
    // Draw bigImg
    [bigImg drawInRect:CGRectMake(0, 0, contextSize.width, contextSize.height)];
    // Draw smallImg
    [smallImg drawInRect:CGRectMake(drawPointS.x,drawPointS.y,smallImg.size.width, smallImg.size.height)];
    //从当前上下文中获得最终图片
    UIImage *resultImg = UIGraphicsGetImageFromCurrentImageContext();
    //关闭上下文
    UIGraphicsEndImageContext();
    
    return resultImg;
    
}


#pragma mark - 获取app当前版本号
/**
 获取app当前版本号

 @return app当前版本号
 */
+(NSString *)getAppCurrentVersion{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}
#pragma mark - 登录密码校验
/**
 *    @brief    登录、注册密码校验
 *
 *    @param    pass    登录、注册密码
 *
 *    @return   BOOL true验证通过,false验证不通过。
 */
+(BOOL)isPassWordFormatValided:(NSString *)pass
{
    NSString *regular = @"^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$";
    NSPredicate *passwordFormatCheckPrdicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regular];
    return [passwordFormatCheckPrdicate evaluateWithObject:pass];
}

#pragma mark - 提现交易密码校验
/**
 *    @brief    提现交易密码校验
 *
 *    @param    pass    提现交易密码
 *
 *    @return   BOOL true验证通过,false验证不通过。
 */
+(BOOL)isTransactionPassWordFormatValided:(NSString *)pass
{
    NSString *regular = @"^\\d{6}$";
    NSPredicate *passwordFormatCheckPrdicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regular];
    return [passwordFormatCheckPrdicate evaluateWithObject:pass];
}

#pragma mark - 身份证校验
/**
 *    @brief    身份证校验
 *
 *    @param    IDCardNumber    身份证号
 *
 *    @return   提示msg，msg是空字符处表示通过，msg有值表示不通过并提示信息。
 */
+ (NSString *)verifyIDCardNumber:(NSString *)IDCardNumber{
    
    IDCardNumber = [IDCardNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *stringMsg = @"请输入正确的身份证号码";
    //
    if ([IDCardNumber length] == 18) {
        if ([self verifyIDCardNumber_18:IDCardNumber]) {
            stringMsg =@"";

        }
    }
    else if ([IDCardNumber length] == 15) {
        if ([self verifyIDCardNumber_15:IDCardNumber]) {
            stringMsg =@"";
        }

    }
    //
    return stringMsg;
}

#pragma mark - 根据身份证获取用户性别
/**
 *  @brief     根据身份证获取用户性别
 *
 *  @param     IDCardNumber     身份证号
 *
 *  @return    sexStr
 */
+ (NSString *)getUserSexFromIDCardNumber:(NSString *)IDCardNumber
{
    IDCardNumber = [IDCardNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *sexStr;
    if ([IDCardNumber length] == 18) {
        int sex = [[IDCardNumber substringWithRange:NSMakeRange(16, 1)] intValue];
        if (sex % 2 == 0) {
            sexStr = @"女";
        } else {
            sexStr = @"男";
        }
    }else if ([IDCardNumber length] == 15) {
        int sex = [[IDCardNumber substringWithRange:NSMakeRange(13, 1)] intValue];
        if (sex % 2 == 0) {
            sexStr = @"女";
        } else {
            sexStr = @"男";
        }
    }
    return sexStr;
}

#pragma mark - 根据身份证获取用户出生年月
/**
 *  @brief    根据身份证获取用户出生年月
 *
 *  @param    IDCardNumber     身份证号
 *
 *  @return   birthdayStr
 */
+ (NSString *)getUserBirthdayFromIDCardNumber:(NSString *)IDCardNumber
{
    IDCardNumber = [IDCardNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *birthdayStr;
    if ([IDCardNumber length] == 18) {
        NSString *year = [IDCardNumber substringWithRange:NSMakeRange(6, 4)];
        NSString *month = [IDCardNumber substringWithRange:NSMakeRange(10, 2)];
        NSString *day = [IDCardNumber substringWithRange:NSMakeRange(12, 2)];
        birthdayStr = [NSString stringWithFormat:@"%@-%@-%@",year,month,day];
    } else if ([IDCardNumber length] == 15) {
        NSString *year = [IDCardNumber substringWithRange:NSMakeRange(6, 2)];
        NSString *month = [IDCardNumber substringWithRange:NSMakeRange(8, 2)];
        NSString *day = [IDCardNumber substringWithRange:NSMakeRange(10, 2)];
        birthdayStr = [NSString stringWithFormat:@"%@-%@-%@",year,month,day];
    }
    return birthdayStr;
}

#pragma mark - 身份证校验
/**
 *    @brief    身份证校验
 *    
 验证身份证必须满足一下规则：
 1、长度必须是18位，前17位必须是数字，第十八位可以是数字或X；
 2、前两位必须是以下情形中的一种：11,12,13,14,15,21,22,23,31,32,33,34,35,36,37,41,42,43,44,45,46,50,51,52,53,54,61,62,63,64,65,71,81,82,91；
 3、第7到第14位出生年月日。第7到第10位为出生年份；11到12位表示月份，范围为01-12；13到14位为合法的日期
 4、 第17位表示性别，双数表示女，单数表示男
 5、 第18位为前17位的校验位
 6、出生年份的前两位必须是19或20
 
 *    @param    IDCardNumber    身份证号
 *
 *    @return   BOOL true验证通过,false验证不通过
 */
+ (BOOL)verifyIDCardNumber_18:(NSString *)IDCardNumber{
    
    IDCardNumber = [IDCardNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([IDCardNumber length] != 18)
    {
        return NO;
    }
    NSString *mmdd = @"(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8])))";
    NSString *leapMmdd = @"0229";
    NSString *year = @"(19|20)[0-9]{2}";
    NSString *leapYear = @"(19|20)(0[48]|[2468][048]|[13579][26])";
    NSString *yearMmdd = [NSString stringWithFormat:@"%@%@", year, mmdd];
    NSString *leapyearMmdd = [NSString stringWithFormat:@"%@%@", leapYear, leapMmdd];
    NSString *yyyyMmdd = [NSString stringWithFormat:@"((%@)|(%@)|(%@))", yearMmdd, leapyearMmdd, @"20000229"];
    NSString *area = @"(1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5]|82|[7-9]1)[0-9]{4}";
    NSString *regex = [NSString stringWithFormat:@"%@%@%@", area, yyyyMmdd , @"[0-9]{3}[0-9Xx]"];
    
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if (![regexTest evaluateWithObject:IDCardNumber]){
        return NO;
    }
    int summary = ([IDCardNumber substringWithRange:NSMakeRange(0,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(10,1)].intValue) *7+ ([IDCardNumber substringWithRange:NSMakeRange(1,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(11,1)].intValue) *9+ ([IDCardNumber substringWithRange:NSMakeRange(2,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(12,1)].intValue) *10+ ([IDCardNumber substringWithRange:NSMakeRange(3,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(13,1)].intValue) *5+ ([IDCardNumber substringWithRange:NSMakeRange(4,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(14,1)].intValue) *8+ ([IDCardNumber substringWithRange:NSMakeRange(5,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(15,1)].intValue) *4+ ([IDCardNumber substringWithRange:NSMakeRange(6,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(16,1)].intValue) *2+ [IDCardNumber substringWithRange:NSMakeRange(7,1)].intValue *1 + [IDCardNumber substringWithRange:NSMakeRange(8,1)].intValue *6+ [IDCardNumber substringWithRange:NSMakeRange(9,1)].intValue *3;
    NSInteger remainder = summary % 11;
    NSString *checkBit = @"";
    NSString *checkString = @"10X98765432";
    checkBit = [checkString substringWithRange:NSMakeRange(remainder,1)];// 判断校验位
    return [checkBit isEqualToString:[[IDCardNumber substringWithRange:NSMakeRange(17,1)] uppercaseString]];
    
}

#pragma mark - 身份证校验
/**
 *    @brief    身份证校验
 *
 验证身份证必须满足一下规则：
 1、长度必须是15位；
 2、前两位必须是以下情形中的一种：11,12,13,14,15,21,22,23,31,32,33,34,35,36,37,41,42,43,44,45,46,50,51,52,53,54,61,62,63,64,65,71,81,82,91；
 3、第7到第12位出生年月日。第7到第8位为出生年份（2位）；9到10位表示月份，范围为01-12；11到12位为合法的日期
 4、 第14位表示性别，双数表示女，单数表示男
 *    @param    IDCardNumber    身份证号
 *
 *    @return   BOOL true验证通过,false验证不通过
 */
+ (BOOL)verifyIDCardNumber_15:(NSString *)IDCardNumber{
    
    IDCardNumber = [IDCardNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([IDCardNumber length] != 15)
    {
        return NO;
    }
    NSString *mmdd = @"(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8])))";
    NSString *leapMmdd = @"0229";
    NSString *year = @"[0-9]{2}";
    NSString *leapYear = @"(0[48]|[2468][048]|[13579][26])";
    NSString *yearMmdd = [NSString stringWithFormat:@"%@%@", year, mmdd];
    NSString *leapyearMmdd = [NSString stringWithFormat:@"%@%@", leapYear, leapMmdd];
    NSString *yyMmdd = [NSString stringWithFormat:@"((%@)|(%@))", yearMmdd, leapyearMmdd];
    NSString *area = @"(1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5]|82|[7-9]1)[0-9]{4}";
    NSString *regex = [NSString stringWithFormat:@"%@%@%@", area, yyMmdd , @"[0-9]{3}"];
    
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [regexTest evaluateWithObject:IDCardNumber];
    
}

#pragma mark - 截屏处理
/**
 * @brief 截屏处理.
 *
 * @return 当前屏幕的NSData数据
 */
+ (NSData *)cutterViewForNSData{
    UIWindow *screenWindow = [[UIApplication sharedApplication] keyWindow];
    
    UIGraphicsBeginImageContextWithOptions(screenWindow.frame.size, NO, 0.0);
    
    [screenWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    NSData *screenShotPNG = UIImagePNGRepresentation(screenShot);
    //    LSYLog(@"cutterView data: %@",screenShotPNG);
    return screenShotPNG;
}
#pragma mark - 截屏处理
/**
 * @brief 截屏处理.
 *
 * @return 当前屏幕的UIImage数据
 */
+(UIImage *)cutterViewForUIImage{
    UIWindow *screenWindow = [[UIApplication sharedApplication] keyWindow];
    
    UIGraphicsBeginImageContextWithOptions(screenWindow.frame.size, NO, 0.0);
    
    [screenWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return screenShot;
}

//#pragma mark - 把Unix时间戳转换成指定格式的日期字符串。
///**
// * @brief 把Unix时间戳转换成指定格式的日期字符串
// *
// * @param  stringTimeIntervalSince1970 Unix时间戳
// *
// * @param  stringDateFormat 日期格式("yyyy-MM-dd HH:mm:ss")
// *
// * @return 指定格式的日期字符串
// */
//+(NSString *)getDateWithTimeIntervalSince1970:(NSString *)stringTimeIntervalSince1970 andDateFormat:(NSString *)stringDateFormat {
//    NSDate *date = [NSDate dateWithTimeIntervalSince1970:stringTimeIntervalSince1970.doubleValue];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:stringDateFormat];
//    return [dateFormatter stringFromDate:date];
//}

+ (BOOL)checkIsChineseCharacters:(NSString *)string
{
    // 兼容少数民族姓名 fix 2017-08-03
    
    NSString *match;
    if ([string containsString:@"·"] || [string containsString:@"•"])
        match = @"(^[\\u4e00-\\u9fa5]+[·•][\\u4e00-\\u9fa5]+$)";
    else
        match = @"^[\\u4e00-\\u9fa5]+$";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
    return [predicate evaluateWithObject:string];
}

@end
