//
//  CommonService.h
//  Project_Model
//  公共服务类
//  Created by GaoFeng on 9/8/16.
//  Copyright © 2016 北京搭伙科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonService : NSObject
#pragma mark - UILabel的高度自适应,根据宽度和字体
/**
 UILabel的高度自适应,根据宽度和字体
 
 @param width 宽度
 @param title Text
 @param font 字体
 @return 自适应的高度
 */
+ (CGFloat)getHeightByWidth:(CGFloat)width title:(NSString *)title font:(UIFont *)font;
#pragma mark - UILabel的宽度自适应，根据字体
/**
 UILabel的宽度自适应，根据字体
 
 @param title Text
 @param font 字体
 @return 自适应的宽度
 */
+ (CGFloat)getWidthWithTitle:(NSString *)title font:(UIFont *)font;
#pragma mark - Hook Utility
/**
 替换Selector
 @param cls 要替换的class
 @param originalSelector 原始的 Selector
 @param swizzledSelector swizzled Selector
 */
+ (void)swizzlingInClass:(Class)cls originalSelector:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector;
#pragma mark - 组合一个图片到另一个图片
/**
 组合一个图片到另一个图片
 @param smallImg 小图
 @param bigImg 大图
 @param drawPoint 小图画的位置
 @param contextSize 画布大小
 @return 合并后的图
 */
+ (UIImage *)composeImgWithImage:(UIImage *)smallImg toImage:(UIImage *)bigImg andSmallDrawPoint:(CGPoint)drawPointS andContextSize:(CGSize) contextSize;

#pragma mark - 获取省市街道列表

/**
 获取省市街道列表
 
 @param partentCode
 @param block RequestCompleteModelBlock
 */
//+(void)getProvinceCityDistrictListWithPartentCode:(NSString *)partentCode onComplete:(RequestCompleteModelBlock)block;

#pragma mark - 获取app当前版本号
/**
 获取app当前版本号
 
 @return app当前版本号
 */
+(NSString *)getAppCurrentVersion;
#pragma mark - 登录密码校验
/**
 *    @brief    登录、注册密码校验
 *
 *    @param    pass    登录、注册密码
 *
 *    @return   BOOL true验证通过,false验证不通过。
 */
+(BOOL)isPassWordFormatValided:(NSString *)pass;

#pragma mark - 提现交易密码校验
/**
 *    @brief    提现交易密码校验
 *
 *    @param    pass    提现交易密码
 *
 *    @return   BOOL true验证通过,false验证不通过。
 */
+(BOOL)isTransactionPassWordFormatValided:(NSString *)pass;

#pragma mark - 身份证校验
/**
 *    @brief    身份证校验
 *
 *    @param    IDCardNumber    身份证号
 *
 *    @return   提示msg，msg是空字符处表示通过，msg有值表示不通过并提示信息。
 */
+ (NSString *)verifyIDCardNumber:(NSString *)IDCardNumber;

#pragma mark - 根据身份证获取用户性别
/**
 *  @brief     根据身份证获取用户性别
 *
 *  @param     IDCardNumber     身份证号
 *
 *  @return    sexStr
 */
+ (NSString *)getUserSexFromIDCardNumber:(NSString *)IDCardNumber;

#pragma mark - 根据身份证获取用户出生年月
/**
 *  @brief    根据身份证获取用户出生年月
 *
 *  @param    IDCardNumber     身份证号
 *
 *  @return   birthdayStr
 */
+ (NSString *)getUserBirthdayFromIDCardNumber:(NSString *)IDCardNumber;

#pragma mark - 截屏处理
/**
 * @brief 截屏处理.
 *
 * @return 当前屏幕的NSData数据
 */
+ (NSData *)cutterViewForNSData;
#pragma mark - 截屏处理
/**
 * @brief 截屏处理.
 *
 * @return 当前屏幕的UIImage数据
 */
+(UIImage *)cutterViewForUIImage;
#pragma mark - 注册登记window下的view时先清除以前的数据。
/**
 * @brief 注册登记window下的view时先清除以前的数据。
 *
 * @return void
 */
+(void)beginRegisterWindowSubViews;
#pragma mark - 注册登记window下的view
/**
 * @brief 注册登记window下的view（注册第一个时一定要先调用beginRegisterWindowSubViews方法）
 * 1）在window上添加view
 * 2）在window上重新显示view的话
 *    以上两种情况都需要登记注册。
 *
 * @param  view 注册的View
 *
 * @param  tag view.tag
 *
 * @return void
 */
+(void)registerWindowSubViewsWithView:(UIView *)view andTag:(int)tag;
#pragma mark - 隐藏 window下登记过的view
/**
 * @brief 隐藏 window下登记过的view
 *
 * @return void
 */
+(void)hiddenWindowSubViews;
#pragma mark - 显示 window下登记过的view
/**
 * @brief 显示 window下登记过的view
 *
 * @return void
 */
+(void)showWindowSubViews;

#pragma mark - 把Unix时间戳转换成指定格式的日期字符串。
/**
 * @brief 把Unix时间戳转换成指定格式的日期字符串
 *
 * @param  stringTimeIntervalSince1970 Unix时间戳
 *
 * @param  stringDateFormat 日期格式("yyyy-MM-dd HH:mm:ss")
 *
 * @return 指定格式的日期字符串
 */
+(NSString *)getDateWithTimeIntervalSince1970:(NSString *)stringTimeIntervalSince1970 andDateFormat:(NSString *)stringDateFormat;


/**
 *  检查是否是正确的中文姓名
 *  使用该校验规则的控制器名称 
 *  DHParnterInfoViewController 升级专业代理人分支1-补充资料页面
 *  DHInputPracticeCerViewController 升级专业代理人分支2-专业代理人申请页面
 *  DHAddBankInfoViewController 添加和修改银行卡
 */
+ (BOOL)checkIsChineseCharacters:(NSString *)string;


@end
