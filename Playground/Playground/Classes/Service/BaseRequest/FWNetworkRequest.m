//
//  FWNetworkRequest.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWNetworkRequest.h"
#import <AdSupport/AdSupport.h>
#import "YQImageCompressTool.h"
#import "UIImage+image90.h"
#import "FWLoginModel.h"

@implementation FWNetworkRequest

- (NSString *)requestBaseURL {
    
    return SERVER_URL;
}

- (FWNetworkRequestType)networkRequestType {
    
    return FWNetworkRequestTypePost;
}

#pragma mark--SSL证书验证
- (AFSecurityPolicy*)securityPolicy{
    AFSecurityPolicy *securityPolicy;
    if (IsUseTrustSSLCertificate) {
        /**** SSL Pinning ****/
        
        // /先导入证书
        NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"www_dahuobaoxian_com" ofType:@"cer"];
        NSData *certData = [NSData dataWithContentsOfFile:cerPath];
        
        // AFSSLPinningModeCertificate 使用证书验证模式
        securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
        
        // allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
        // 如果是需要验证自建证书，需要设置为YES
        securityPolicy.allowInvalidCertificates = NO;
        
        //validatesDomainName 是否需要验证域名，默认为YES；
        //假如证书的域名与你请求的域名不一致，需把该项设置为NO；如设成NO的话，即服务器使用其他可信任机构颁发的证书，也可以建立连接，这个非常危险，建议打开。
        //置为NO，主要用于这种情况：客户端请求的是子域名，而证书上的是另外一个域名。因为SSL证书上的域名是独立的，假如证书上注册的域名是www.google.com，那么mail.google.com是无法验证通过的；当然，有钱可以注册通配符的域名*.google.com，但这个还是比较贵的。
        //如置为NO，建议自己添加对应域名的校验逻辑。
        securityPolicy.validatesDomainName = YES;
        
        NSMutableSet *certificates = [NSMutableSet set];
        [certificates addObject:certData];
        securityPolicy.pinnedCertificates = certificates;
    }
    else{
        securityPolicy = [[AFSecurityPolicy alloc] init];
        securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        securityPolicy.allowInvalidCertificates = YES;
        securityPolicy.validatesDomainName = NO;
    }
    return securityPolicy;
}

#pragma mark - > 处理请求

- (void)addParamsInHeaderField:(AFHTTPRequestSerializer *)requestSerializer {
    
    NSMutableDictionary *headerFieldParams = @{}.mutableCopy;
    [headerFieldParams addEntriesFromDictionary:[self defaultHeadeFieldParams]];
    [headerFieldParams addEntriesFromDictionary:self.headerFieldAdditionalInfo];
    
    [headerFieldParams  enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
        [requestSerializer setValue:value forHTTPHeaderField:key];
    }];
}

- (NSMutableDictionary<NSString *,NSString *> *)defaultHeadeFieldParams {
    
    NSMutableDictionary *params = @{}.mutableCopy;

    return params;
}

#pragma mark - > 处理收到的结果

- (void)handleResponseObject:(NSDictionary *)responseObject dataTask:(NSURLSessionDataTask *)dataTask{
    
    if (self.isNeedShowHud) {
        [[FWHudManager sharedManager] hideAllHudToController:nil];
    }
    
    NSMutableDictionary *result = ((NSDictionary *)responseObject).mutableCopy;
    
    //这里处理结果
    NSString *resultCode = HYGET_OBJECT_FROMDIC(result, @"errno");
    if (self.completeAction) self.completeAction(dataTask, resultCode ,result, nil);
}


- (NSDictionary *) headerBody:(NSDictionary *)param WithAction:(NSString *)action{
    
    NSMutableDictionary * postDic = [NSMutableDictionary dictionary];
    NSMutableDictionary * headerDic = [NSMutableDictionary dictionary];
    
    [headerDic setObject:@"2" forKey:@"push_id"];
    [headerDic setObject:FWVersion forKey:@"version"];
    if (![GFStaticData getObjectForKey:DeviceToken]) {
        [headerDic setObject:@"" forKey:@"device_token"];
    }else{
        [headerDic setObject:[GFStaticData getObjectForKey:DeviceToken] forKey:@"device_token"];
    }
    [headerDic setObject:kTagChannelID forKey:@"channel_no"];
    [headerDic setObject:[Utility getDeviceID] forKey:@"device_id"];
    [headerDic setObject:[NSString stringWithFormat:@"%@_%@",[Utility getDeviceType],[Utility getDeviceSystemVersion]] forKey:@"client_type"];
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YYYY-MM-dd"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    [headerDic setObject:locationString forKey:@"client_time"];
   
    if ([GFStaticData getObjectForKey:kTagUserKeyToken])
    {
        [headerDic setObject:[GFStaticData getObjectForKey:kTagUserKeyToken] forKey:@"token"];
    }
    else
    {
        [headerDic setObject:@"" forKey:@"token"];
    }
    
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    if (param!= nil){
        dict = [param mutableCopy];
    }
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]){
        [dict setObject:[[GFStaticData getObjectForKey:kTagUserKeyID] description] forKey:@"uid"];
    }else{
        [dict setObject:@"0"  forKey:@"uid"];
    }
    param = [dict mutableCopy];
    
    [postDic setObject:action forKey:@"action"];
    
    if (param == nil){
        NSMutableDictionary * p = [NSMutableDictionary dictionary];
        [p setObject:@"0" forKey:@"uid"];
        [postDic setObject:p forKey:@"data"];
    }else{
        [postDic setObject:param forKey:@"data"];
    }

    [postDic setObject:headerDic forKey:@"header"];
    [headerDic setObject:[Utility dictionaryDataToSignString:postDic] forKey:@"sign"];
    [postDic setObject:headerDic forKey:@"header"];
    
    return postDic;
}

- (void)startWithParameters:(id)parameters
                 WithAction:(NSString *)action
                 WithDelegate:(id )delegate
             completeAction:(FWRequestCompleteAction)completeAction {
    
    UIViewController * vc = (UIViewController *)delegate;

    
    if (![[GFStaticData getObjectForKey:Network_Available] boolValue] &&
        ![action isEqualToString:Get_settings]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NetworkUnAvailableNoti object:nil];
        [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:vc];
        return;
    }
    if (self.isNeedShowHud) {
        [[FWHudManager sharedManager] showLoadingHudToController:nil isForbidUserInteraction:YES];
    }
    
    NSString * questURL = [NSString stringWithFormat:@"%@",self.requestBaseURL];
    self.action = action;
    
    if (completeAction) self.completeAction = completeAction;
    
    //需要经过一步 签名 和 包装
    NSMutableDictionary * postDict = ((NSMutableDictionary *)parameters).mutableCopy;
    parameters = [self headerBody:postDict WithAction:action];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setSecurityPolicy:[self securityPolicy]];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 30.f;
    [self addParamsInHeaderField:manager.requestSerializer];
    
    if (self.headerFieldAdditionalInfo
        && self.headerFieldAdditionalInfo.allKeys.count > 0) {
        
        [self.headerFieldAdditionalInfo enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull value, BOOL * _Nonnull stop) {
            [manager.requestSerializer setValue:value forHTTPHeaderField:key];
        }];
    }
    
    if (self.requestType == FWRequestTypePutParamJSONStringInHeaderField) {
        
        if (self.commonRequestType == FWNetworkRequestTypeGet) {
            [manager GET:questURL
              parameters:parameters
                progress:nil
                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                     [self handleResponseObject:responseObject dataTask:task];
                 } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     [self handleErrorTask:task error:error];
                 }];
        }
        else {
            [self questPutParamsJSONStringInHTTPBody:parameters requestURL:questURL];
        }
    }
    else {
        if (self.commonRequestType == FWNetworkRequestTypePost) {
            
            [manager POST:questURL
               parameters:parameters
                 progress:nil
                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                      
                      NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
                      
                      if ([action isEqualToString:@"get_im_token"]) {
//                          NSLog(@"back===%@",back);
                      }
                      
                      if (![back isKindOfClass:[NSDictionary class]]) {
                          return ;
                      }
                      if ([[back objectForKey:@"errno"] isEqualToString:@"-3"]&&
                          [[back objectForKey:@"errmsg"] isEqualToString:@"访问受限,错误的token验证"]) {
                          
                          UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"强制退出" message:@"您的账号在别的设备登录，如果不是您本人操作，请尽快联系客服人员，确保您的账号安全。" preferredStyle:UIAlertControllerStyleAlert];
                          UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                              [GFStaticData clearUserData];

                              [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
                          }];
                          [alertController addAction:okAction];
                          [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                          return ;
                      }
                      [self handleResponseObject:responseObject dataTask:task];
                      [self handleResult:responseObject WithAction:action];
                  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                      NSLog(@"************* %@ - 接口出错了************************\n%@",action,error);
                      [self handleErrorTask:task error:error];
                      [[NSNotificationCenter defaultCenter] postNotificationName:NetworkUnAvailableNoti object:nil];
                  }];
        }
        else if (self.commonRequestType == FWNetworkRequestTypeGet) {
            
            [manager GET:questURL
              parameters:parameters
                progress:nil
                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                     [self handleResponseObject:responseObject dataTask:task];
                 } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     [self handleErrorTask:task error:error];
                 }];
        }
    }
}

#pragma mark - > 微信相关请求
- (void)WXSettingStartRequestWithURL:(NSString *)URL WithAction:(NSString *)action {

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ([action isEqualToString:WXLogin]) {
            [self loginNextOpertion:responseObject];
        }else if ([action isEqualToString:WXInfo]){
            [self bindNextOpertion:responseObject WithAction:[GFStaticData getObjectForKey:WXOperation]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"微信登录过成功error==%@",error);
    }];
}

#pragma mark - > 根据code，请求token
- (void)getAccessTokenWithCode:(NSString *)codes  WithAction:(NSString *)action{
    [GFStaticData saveObject:@"NO" forKey:kTagRequestAccessToken];
    
    NSString * url = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WXAppKey,WXAppSecret,codes];
    
    [self WXSettingStartRequestWithURL:url WithAction:action];
}


#pragma mark - > 根据token，请求微信个人信息
- (void)getWXUserInfoWithtoken:(NSString *)token  withOpenId:(NSString *)openID
{
    NSString * url = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",token,openID];
    [self WXSettingStartRequestWithURL:url WithAction:WXInfo];
}

#pragma mark - > 登录后操作（获取微信个人信息）
- (void)loginNextOpertion:(id)responseObject{
    
    [GFStaticData saveObject:nil forKey:kTagRequestAccessToken];
    
    NSDictionary * back = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
    
    if ([[back objectForKey:@"errcode"] integerValue]){
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"授权失败" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
    }else{
        [GFStaticData saveObject:back forKey:kTagWeiXinLoginInfo];
        
        [self getWXUserInfoWithtoken:[back objectForKey:@"access_token"] withOpenId:[back objectForKey:@"openid"]];
    }
}

#pragma mark - > 绑定微信后的操作
- (void)bindNextOpertion:(id)responseObject WithAction:(NSString *)action{
    
    NSString * back = [responseObject mj_JSONString];
    
    [GFStaticData saveObject:back forKey:kTagWeiXinUserInfo];
    
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:[[GFStaticData getObjectForKey:kTagWeiXinLoginInfo] objectForKey:@"unionid"] forKey:@"weixin_union_id"];
    [param setObject:[[[GFStaticData getObjectForKey:kTagWeiXinLoginInfo] objectForKey:@"access_token"] description] forKey:@"weixin_access_token"];
    [param setObject:[[[GFStaticData getObjectForKey:kTagWeiXinLoginInfo] objectForKey:@"openid"] description] forKey:@"weixin_open_id"];
    
    [param setObject:back forKey:@"weixin_info"];
    
    if ([action isEqualToString:@""]) {
        [param setObject:@"uid" forKey:[GFStaticData getObjectForKey:kTagUserKeyID]];
    }
    
    [self startWithParameters:param WithAction:[GFStaticData getObjectForKey:WXOperation] WithDelegate:nil  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {}];
}

- (void)handleResult:(id)responseObject WithAction:(NSString *)action {
    
    if ([action isEqualToString:Submit_weixin_login]) {
        //如果是微信登录
        if ([[responseObject  objectForKey:@"errno"] integerValue] == 0){
            [GFStaticData saveObject:nil forKey:WXOperation];

            
            [GFStaticData saveLoginData:[responseObject objectForKey:@"data"]];
            [GFStaticData saveObject:@"0" forKey:kTagUserKeyName];
            [GFStaticData saveObject:@"YES" forKey:kTagIsWeiXinLogin];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kTagLoginFinishedNoti object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kTagWXLoginRefresh object:[responseObject objectForKey:@"data"]];
        }else{
            UIViewController * vc = [UIApplication sharedApplication].keyWindow.rootViewController;
            [[FWHudManager sharedManager] showErrorMessage:[responseObject objectForKey:@"errmsg"] toController: vc];
        }
    }else if ([action isEqualToString:Submit_weixin_bind]){
        //微信绑定
        if ([[responseObject  objectForKey:@"errno"] integerValue] == 0){
            [GFStaticData saveObject:nil forKey:WXOperation];
            
            [GFStaticData saveLoginData:[responseObject objectForKey:@"data"]];
            [GFStaticData saveObject:@"NO" forKey:kTagIsWeiXinLogin];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kTagLoginFinishedNoti object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kTagWXLoginRefresh object:[responseObject objectForKey:@"data"]];
        }else{
            UIViewController * vc = [UIApplication sharedApplication].keyWindow.rootViewController;
            [[FWHudManager sharedManager] showErrorMessage:[responseObject objectForKey:@"errmsg"] toController: vc];
        }
    }
}

- (void)handleErrorTask:(NSURLSessionDataTask *)task error:(NSError *)error {
    
    if (self.isNeedShowHud) {
        [[FWHudManager sharedManager] hideAllHudToController:nil];
    }
}

- (void)questPutParamsJSONStringInHTTPBody:(NSDictionary *)params requestURL:(NSString *)requestURL {
    
    if (!params) params = @{};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setSecurityPolicy:[self securityPolicy]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:@"POST"
                                                                      URLString:requestURL
                                                                     parameters:nil
                                                                          error:nil];
    [request setValue:@"application/json,text/json,text/javascript,text/html;encoding=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSMutableDictionary *headerFieldParams = @{}.mutableCopy;
    [headerFieldParams addEntriesFromDictionary:[self defaultHeadeFieldParams]];
    [headerFieldParams addEntriesFromDictionary:self.headerFieldAdditionalInfo];
    
    [headerFieldParams  enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
        [request setValue:value forHTTPHeaderField:key];
    }];
    
    
    [self addParamsInHeaderField:manager.requestSerializer];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (error) {
            
            [self handleErrorTask:nil error:error];
        }
        else {
            
            [self handleResponseObject:responseObject dataTask:nil];
        }
        
    }] resume];
}

#pragma mark - >  获取阿里云sts临时凭证(1为上传普通图片，2为上传视频)
- (void)requestStsTokenWithType:(NSString *)type{
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"type":type,
                              };
    
    [self startWithParameters:params WithAction:Get_sts_token  WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            NSDictionary * dataDict = [back objectForKey:@"data"] ;
            if ([type isEqualToString:@"1"]) {
                [GFStaticData saveObject:[dataDict objectForKey:@"Expiration"] forKey:ALi_Expiration_Image];
                [GFStaticData saveObject:[dataDict objectForKey:@"SecurityToken"]  forKey:ALi_SecurityToken_Image];
                [GFStaticData saveObject:[dataDict objectForKey:@"AccessKeyId"]  forKey:ALi_AccessKeyId_Image];
                [GFStaticData saveObject:[dataDict objectForKey:@"AccessKeySecret"]  forKey:ALi_AccessKeySecret_Image];
            }else{
                [GFStaticData saveObject:[dataDict objectForKey:@"Expiration"] forKey:ALi_Expiration_Video];
                [GFStaticData saveObject:[dataDict objectForKey:@"SecurityToken"]  forKey:ALi_SecurityToken_Video];
                [GFStaticData saveObject:[dataDict objectForKey:@"AccessKeyId"]  forKey:ALi_AccessKeyId_Video];
                [GFStaticData saveObject:[dataDict objectForKey:@"AccessKeySecret"]  forKey:ALi_AccessKeySecret_Video];
            }
        }
    }];
}

#pragma mark - > 统计
- (void)requestTraceWithURL:(NSString *)url{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {}];
}
@end
