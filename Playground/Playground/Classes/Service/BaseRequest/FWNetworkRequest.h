//
//  FWNetworkRequest.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "YTKBaseRequest.h"
#import "FWBaseViewController.h"

@interface FWUploadPicture : NSObject

@property (nonatomic, strong) NSData *fileData;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *fileName;
@property (nonatomic, copy) NSString *mimeType;

@end


typedef NS_ENUM(NSUInteger, FWRequestType) {
    FWRequestTypeDefault,
    FWRequestTypePutParamJSONStringInHeaderField,// 将参数封装到body中
};

typedef NS_ENUM(NSUInteger, FWNetworkRequestType) {
    FWNetworkRequestTypePost,
    FWNetworkRequestTypeGet,
};

typedef void (^FWRequestCompleteAction)(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error);

@interface FWNetworkRequest : NSObject


// 如果是用作测试的话则接口会直接请求测试服务器


@property (nonatomic, assign) FWRequestType requestType;
@property (nonatomic, assign) FWNetworkRequestType commonRequestType;

@property (nonatomic, assign) BOOL isNeedShowHud;


@property (nonatomic, copy) NSString *requestBaseURL;
@property (nonatomic, copy) NSString *requestAdditionalURL;
@property (nonatomic, copy) NSString *appendURL;
@property (nonatomic, copy) NSString *action;

@property (nonatomic, strong) NSDictionary<NSString *, NSString *> *headerFieldAdditionalInfo;

@property (nonatomic, copy) FWRequestCompleteAction completeAction;

- (void)startWithParameters:(id)parameters
                 WithAction:(NSString *)action
               WithDelegate:(id )delegate
             completeAction:(FWRequestCompleteAction)completeAction;

#pragma mark - > 微信相关请求
- (void)WXSettingStartRequestWithURL:(NSString *)URL WithAction:(NSString *)action;

#pragma mark - > 根据code，请求token
- (void)getAccessTokenWithCode:(NSString *)codes WithAction:(NSString *)action;

#pragma mark - > 根据token，请求微信个人信息
- (void)getWXUserInfoWithtoken:(NSString *)token  withOpenId:(NSString *)openID;

#pragma mark - >  获取阿里云sts临时凭证 （1为上传普通图片，2为上传视频）
- (void)requestStsTokenWithType:(NSString *)type;

#pragma mark - > 统计
- (void)requestTraceWithURL:(NSString *)url;
@end
