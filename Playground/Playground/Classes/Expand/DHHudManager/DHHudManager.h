//
//  DHHudManager.h
//  Project_Model
//
//  Created by 韩丛旸 on 2017/5/4.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIViewController+HUD.h"
@interface DHHudManager : NSObject

@property (nonatomic, assign) BOOL isKeyBoardShowOnTheScreen;
@property (nonatomic, assign) BOOL isHudShowOnTheScreen;// 是否有HUD显示在当前屏幕上

@property (nonatomic, strong) NSMutableArray<MBProgressHUD *> *hudArray;



+ (instancetype)sharedManager;

/**
 @brief 在当前控制器view上显示加载图标
 @param controller 传入nil则显示在rootWindow上
 */
- (void)showLoadingHudToController:(UIViewController *)controller;

- (void)showLoadingHudToController:(UIViewController *)controller isForbidUserInteraction:(BOOL)isForbidUserInteraction;


/**
 @brief 在当前控制器view显示成功信息
 @param controller 传入nil则显示在rootWindow上
 @note 如果涉及到页面跳转则传入nil
 */
- (void)showSuccessMessage:(NSString *)message toController:(UIViewController *)controller;


/**
 @brief 在当前控制器view显示错误信息
 @param controller 传入nil则显示在rootWindow上
 @note 如果涉及到页面跳转则传入nil
 */
- (void)showErrorMessage:(NSString *)message toController:(UIViewController *)controller;

- (void)hideAllHudToController:(UIViewController *)controller;

/**
 根据键盘是否弹起确定HUD的位置
 */
- (void)startMonitorKeyBoard;

- (void)showMessage:(NSString *)messsage toController:(UIViewController *)controller hudType:(FWHUDType)hudType;

// 高仿安卓Toast
- (void)showToast:(NSString *)toast;

@end
