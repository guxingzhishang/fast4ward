//
//  DHHudManager.m
//  Project_Model
//
//  Created by 韩丛旸 on 2017/5/4.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import "DHHudManager.h"
#import <Toast/UIView+Toast.h>

@implementation DHHudManager

+ (instancetype)sharedManager{
    static DHHudManager *center = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        center = [[DHHudManager alloc] init];
        center.hudArray = @[].mutableCopy;
        
    });
    return center;
}
- (void)startMonitorKeyBoard {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    self.isKeyBoardShowOnTheScreen = YES;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    self.isKeyBoardShowOnTheScreen = NO;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - > HUD显示相关方法

- (void)showLoadingHudToController:(UIViewController *)controller {
    
    [self showLoadingHudToController:controller isForbidUserInteraction:NO];
}

- (void)showLoadingHudToController:(UIViewController *)controller isForbidUserInteraction:(BOOL)isForbidUserInteraction {
    NSAssert(([controller isKindOfClass:[UIViewController class]] || (controller == nil)) == YES, @"传入的参数既不是nil又不是VC");
    
    if (self.isHudShowOnTheScreen == YES || (controller != nil && controller.isShowOnTheScreen == NO)) return;
    self.isHudShowOnTheScreen = YES;
    
    UIView *view;
    if (controller == nil) {
        view = DHAppWindow;
    } else {
        view = controller.view;
    }
    
    [self showHudWithMessage:nil toView:view hudType:FWHUDType_Loading isForbidUserInteraction:isForbidUserInteraction];
}

- (void)showSuccessMessage:(NSString *)message toController:(UIViewController *)controller {
    
    NSAssert(([controller isKindOfClass:[UIViewController class]] || (controller == nil)) == YES, @"传入的参数既不是nil又不是VC");

    if (self.isHudShowOnTheScreen == YES || (controller != nil && controller.isShowOnTheScreen == NO)) return;
    self.isHudShowOnTheScreen = YES;

    UIView *view;
    
    if (controller == nil) {
        view = DHAppWindow;
    } else {
        view = controller.view;
    }
    
    [self showHudWithMessage:message toView:view hudType:FWHUDType_Success isForbidUserInteraction:YES];
}

- (void)showErrorMessage:(NSString *)message toController:(UIViewController *)controller {
    
    NSAssert(([controller isKindOfClass:[UIViewController class]] || (controller == nil)) == YES, @"传入的参数既不是nil又不是VC");

    if (self.isHudShowOnTheScreen == YES || (controller != nil && controller.isShowOnTheScreen == NO)) return;
    self.isHudShowOnTheScreen = YES;
    UIView *view;
    
    if (controller == nil) {
        view = DHAppWindow;
    } else {
        view = controller.view;
    }
    
    [self showHudWithMessage:message toView:view hudType:FWHUDType_Error isForbidUserInteraction:YES];
}

- (void)showMessage:(NSString *)messsage toController:(UIViewController *)controller hudType:(FWHUDType)hudType {
    
    if (self.isHudShowOnTheScreen == YES || (controller != nil && controller.isShowOnTheScreen == NO)) return;
    self.isHudShowOnTheScreen = YES;
    [controller FW_showHUD:messsage type:hudType];
}

#pragma mark - > HUD消失相关方法

- (void)hideAllHudToController:(UIViewController *)controller {
    
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    [MBProgressHUD hideHUDForView:DHAppWindow animated:YES];
    if ([controller isKindOfClass:[UIViewController class]]) {
        [MBProgressHUD hideHUDForView:controller.view animated:YES];
    }
    [self.hudArray enumerateObjectsUsingBlock:^(MBProgressHUD * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj hide:NO];
    }];
    [self.hudArray removeAllObjects];
    self.hudArray = @[].mutableCopy;
    self.isHudShowOnTheScreen = NO;

}

- (void)showHudWithMessage:(NSString *)message toView:(UIView *)view hudType:(FWHUDType)hudType isForbidUserInteraction:(BOOL)isForbidUserInteraction {
    
    NSAssert([view isKindOfClass:[UIView class]], @"传入的不是view");

    switch (hudType)  {
        case FWHUDType_Success: {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
                hud.labelText = message;
                hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", @"success.png"]]];
                hud.mode = MBProgressHUDModeCustomView;
                hud.removeFromSuperViewOnHide = YES;
                hud.userInteractionEnabled = isForbidUserInteraction;
                
                if (self.isKeyBoardShowOnTheScreen == YES) {
                    if (CGRectEqualToRect(view.frame, [UIScreen mainScreen].bounds) == YES) {
                        hud.yOffset = - 100;
                    }
                }
                [hud hide:YES afterDelay:1.0];
                
            });
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.isHudShowOnTheScreen = NO;
            });
        }
            break;
        case FWHUDType_Error: {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
                hud.labelText = message;
                hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", @"error.png"]]];
                hud.mode = MBProgressHUDModeCustomView;
                hud.removeFromSuperViewOnHide = YES;
                hud.userInteractionEnabled = isForbidUserInteraction;
                
                if (self.isKeyBoardShowOnTheScreen == YES) {
                    if (CGRectEqualToRect(view.frame, [UIScreen mainScreen].bounds) == YES) {
                        hud.yOffset = - 100;
                    }
                }
                
                [hud hide:YES afterDelay:1.0];
            });
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.isHudShowOnTheScreen = NO;
            });
            
        }
            break;
        case FWHUDType_Loading: {
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
            
            if (self.isKeyBoardShowOnTheScreen == YES) {
                if (CGRectEqualToRect(view.frame, [UIScreen mainScreen].bounds) == YES) {
                    hud.yOffset = - 100;
                }
            }
            
            hud.labelText = message;
            hud.minShowTime = 0.2;
            hud.removeFromSuperViewOnHide = YES;
            hud.dimBackground = NO;
            hud.userInteractionEnabled = isForbidUserInteraction;
            [self.hudArray addObject:hud];
            
//            __weak MBProgressHUD *weakHud = hud;
//            // 用于强制关闭hud
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(60 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                if (weakHud != nil) {
//                    [weakHud hide:YES];
//                    self.isHudShowOnTheScreen = NO;
//                }
//            });
        }
        default:
            break;
    }

}

- (void)showToast:(NSString *)toast {
    
    UINavigationController *currentNavi = DHAppWindow;
    [currentNavi.view makeToast:toast duration:3.5f position:CSToastPositionCenter];
}

@end
