//
//  HYConditionManager.m
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/21.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//

#import "HYConditionManager.h"

@implementation HYConditionChecker

- (HYConditionChecker *(^)(NSArray<NSNumber *> *))require
{
    return ^id(NSArray<NSNumber *> *array)
    {
        __block BOOL tempBOOL = YES;
        
        [array enumerateObjectsUsingBlock:^(NSNumber * arrayBoolElement, NSUInteger idx, BOOL * _Nonnull stop)
         {
             
             if (tempBOOL == YES)
             {
                 tempBOOL = arrayBoolElement.boolValue & tempBOOL;
             }
             
         }];
        
        if (self.conditionBool == YES)
        {
            if (tempBOOL != YES)
            {
                [self.boolStoreArrayM removeLastObject];
                [self.boolStoreArrayM addObject:@(NO)];
            }
        }
        else
        {
            [self.boolStoreArrayM removeLastObject];
            [self.boolStoreArrayM addObject:@(YES)];
        }
        
        return self;
    };
}

- (NSMutableArray<NSNumber *> *)boolStoreArrayM
{
    if (_boolStoreArrayM == nil)
    {
        _boolStoreArrayM = @[].mutableCopy;
    }
    return _boolStoreArrayM;
}

- (HYConditionChecker *(^)(BOOL))combain
{
    return ^id(BOOL attribute)
    {
        self.conditionBool = attribute;
        return self;
    };
}

- (HYConditionChecker *(^)(BOOL))is
{
    return ^id(BOOL attribute)
    {
        BOOL tempBOOL = (attribute == self.conditionBool);
        [self.boolStoreArrayM addObject:@(tempBOOL)];
        self.conditionBool = tempBOOL;
        return self;
    };
}

@end

#pragma mark - > HYConditionManager

@interface HYConditionManager ()

@property (nonatomic, strong) HYConditionChecker *checker;

@end

@implementation HYConditionManager

+ (instancetype)manager
{
    static HYConditionManager *_manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[HYConditionManager alloc] init];
        _manager.checker = [[HYConditionChecker alloc] init];
    });
    return _manager;
}

- (BOOL)hy_checkCondition:(void(^)(HYConditionChecker *condition))block;
{
    [self.checker.boolStoreArrayM removeAllObjects];
    
    block(self.checker);
    
    __block BOOL tempBool = YES;
    
    [self.checker.boolStoreArrayM enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        tempBool = tempBool & obj.boolValue;
    }];
    
    return tempBool;
}

@end
