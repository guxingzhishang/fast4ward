//
//  HYConditionManager.h
//  Project_Model
//
//  Created by 韩丛旸 on 16/11/21.
//  Copyright © 2016年 北京搭伙科技. All rights reserved.
//


#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, HYCheckType)
{
    Check_isUserIsSeniorPartnerAndAuthenticationed,
    Check_isUserLoginIn,
};

@interface HYConditionChecker : NSObject

@property (nonatomic, strong) NSMutableArray<NSNumber *> *boolStoreArrayM;

@property (nonatomic, assign) BOOL conditionBool;

- (HYConditionChecker *(^)(BOOL condition))combain;
- (HYConditionChecker *(^)(BOOL condition))is;
- (HYConditionChecker *(^)(NSArray<NSNumber *> *condition))require;

@end

@interface HYConditionManager : NSObject

+ (instancetype)manager;

- (BOOL)hy_checkCondition:(void(^)(HYConditionChecker *condition))block;

@end
