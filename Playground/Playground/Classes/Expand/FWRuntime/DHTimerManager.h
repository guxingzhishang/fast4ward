//
//  DHTimerManager.h
//  Project_Model
//
//  Created by 韩丛旸 on 2017/3/22.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DHTimerType) {
    DHTimerTypeRegister,
    DHTimerTypeLoginByMessage,
    DHTimerTypeResetPassWord,
};

/**
 用于保存系统中的时间计数
 */
@interface DHTimerManager : NSObject

@property (nonatomic, assign) NSInteger regRemainTime;// 新用户注册短信验证码
@property (nonatomic, assign) NSInteger loginByMessageRemainTime;// 用户短信登录剩余时间
@property (nonatomic, assign) NSInteger resetRemainTime;// 用户短信登录剩余时间


- (void)startCountingWithTimerType:(DHTimerType)type;
+ (instancetype)sharedManager;

@end
