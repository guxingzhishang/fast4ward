//
//  DHTimerManager.m
//  Project_Model
//
//  Created by 韩丛旸 on 2017/3/22.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import "DHTimerManager.h"

@interface DHTimerManager ()

@property (nonatomic, strong) NSTimer *registerTim;
@property (nonatomic, strong) NSTimer *loginByMessageTim;
@property (nonatomic, strong) NSTimer *resetTim;

@end

@implementation DHTimerManager

+ (instancetype)sharedManager{
    static DHTimerManager *timer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        timer = [[DHTimerManager alloc] init];
    });
    return timer;
}

- (void)startCountingWithTimerType:(DHTimerType)type {
    switch (type) {
        case DHTimerTypeRegister: {
            if (self.registerTim != nil) return;
            self.regRemainTime = 60;
            self.registerTim = [NSTimer scheduledTimerWithTimeInterval:1
                                                                target:self
                                                              selector:@selector(startReduceRegisterCount:)
                                                              userInfo:nil
                                                               repeats:YES];
        }
            break;
        case DHTimerTypeLoginByMessage: {
            
            if (self.loginByMessageTim != nil) return;
            self.loginByMessageRemainTime = 60;
            self.loginByMessageTim = [NSTimer scheduledTimerWithTimeInterval:1
                                                                      target:self
                                                                    selector:@selector(startReduceRegisterCount:)
                                                                    userInfo:nil
                                                                     repeats:YES];
        }
            break;
        case DHTimerTypeResetPassWord: {
            if (self.resetTim != nil) return;
            self.resetRemainTime = 60;
            self.resetTim = [NSTimer scheduledTimerWithTimeInterval:1
                                                             target:self
                                                           selector:@selector(startReduceRegisterCount:)
                                                           userInfo:nil
                                                            repeats:YES];
        }
            break;
        default:
            break;
    }
}

- (void)startReduceRegisterCount:(NSTimer *)timer {
    
    if ([timer isEqual:_registerTim] && _registerTim != nil) {
        if (self.regRemainTime >= 1) {
            self.regRemainTime = self.regRemainTime - 1;
        } else {
            [self.registerTim invalidate];
            self.registerTim = nil;
        }
    }
    
    if ([timer isEqual:_loginByMessageTim] && _loginByMessageTim != nil) {
        
        if (self.loginByMessageRemainTime >= 1) {
            self.loginByMessageRemainTime = self.loginByMessageRemainTime - 1;
        } else {
            [self.loginByMessageTim invalidate];
            self.loginByMessageTim = nil;
        }
    }
    
    if ([timer isEqual:_resetTim] && _resetTim != nil) {
        
        if (self.resetRemainTime >= 1) {
            self.resetRemainTime = self.resetRemainTime - 1;
        } else {
            [self.resetTim invalidate];
            self.resetTim = nil;
        }
    }
}

@end
