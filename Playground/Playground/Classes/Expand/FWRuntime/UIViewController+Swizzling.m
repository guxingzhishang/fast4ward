//
//  UIViewController+Swizzling.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "UIViewController+Swizzling.h"
#import "SwizzlingDefine.h"

@implementation UIViewController (Swizzling)


+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        swizzling_exchangeMethod([UIViewController class] ,@selector(viewWillAppear:), @selector(swizzling_viewWillAppear:));
        
        swizzling_exchangeMethod([UIViewController class] ,@selector(viewWillDisappear:), @selector(swizzling_viewWillDisappear:));
    });
}


- (void)swizzling_viewWillAppear:(BOOL)animated{
    [self swizzling_viewWillAppear:animated];
    
    if (self.parentViewController == self.navigationController)
    {
        if ([NSStringFromClass([self class]) isEqual:@"AliyunRecordViewController"]||
            [NSStringFromClass([self class]) isEqual:@"AliyunPhotoViewController"]||
            [NSStringFromClass([self class]) isEqual:@"AliyunCropViewController"]) {
            
            //为解决 阿里云短视频，子控制器导航栏显示的问题
            self.navigationController.delegate = self;
            self.fd_interactivePopDisabled = YES;
            
            if ([NSStringFromClass([self class]) isEqual:@"AliyunCropViewController"]) {
                for (UIView * view in self.view.subviews) {
                    if ([NSStringFromClass([view class]) isEqual:@"AliyunCropViewBottomView"]) {

                        for (UIButton * button in view.subviews) {
                            if (button.frame.origin.x >= 100 &&
                                button.frame.origin.x <= 200) {
                                button.hidden = YES;
                            }
                        }
                    }
                }
            }
        }else{
            if (![self swizzling_isHiddenNavigationBar] && self.navigationController)
            {
                if ([NSStringFromClass([self class]) isEqual:@"FWPublishViewController"]) {
                    self.fd_prefersNavigationBarHidden = YES;
                    self.navigationController.navigationBar.hidden = YES;
                }
                return;
            }
        }
    }
}

- (void)swizzling_viewWillDisappear:(BOOL)animated{
    [self swizzling_viewWillDisappear:animated];
    
    if (self.parentViewController == self.navigationController)
    {
        if([[GFStaticData getObjectForKey:@"ALI"] boolValue]){
            //为解决 阿里云短视频，子控制器导航栏显示的问题
            self.navigationController.delegate = self;
            self.fd_interactivePopDisabled = YES;
            self.fd_prefersNavigationBarHidden = YES;
            self.parentViewController.navigationController.navigationBar.hidden = YES;
            [GFStaticData saveObject:@"NO" forKey:@"ALI"];

            return;
        }
    }
}

#pragma mark - Private
- (BOOL)swizzling_isHiddenNavigationBar
{
    SEL  sel = NSSelectorFromString(@"hiddenNavigationBar");
    BOOL use = NO;
    if ([self respondsToSelector:sel]) {
        SuppressPerformSelectorLeakWarning(use = (BOOL)[self performSelector:sel]);
    }
    return use;
}

@end
