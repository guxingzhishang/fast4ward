//
//  UIAlertController+DHExtension.m
//  Project_Model
//
//  Created by 韩丛旸 on 2017/5/24.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import "UIAlertController+DHExtension.h"

@implementation UIAlertController (DHExtension)

- (instancetype)initWithAlertType:(DHAlertControllerType)type title:(NSString *)title message:(NSString *)message actionTitles:(NSArray<NSString *> *)actionTitles handler:(void (^)(NSInteger index))handler {
    
    self = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if (self) {
        
        if ([self respondsToSelector:NSSelectorFromString(@"_attributedTitle")]) {
            
            NSAttributedString *attributeTitle;
            switch (type) {
                case DHAlertControllerTypeRedTitleType:
                case DHAlertControllerTypeTitleAndFirstActionTitleRedType: {
                    
                    if (title.length != 0) {
                        attributeTitle = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:DHColor(@"ff6f00"),NSFontAttributeName:DHFont(16)}];
                    }
                }
                    break;
                case DHAlertControllerTypeTitleBoldType:
                {
                    if (title.length != 0) {
                        attributeTitle = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:DHColor(@"222222"),NSFontAttributeName:DHBoldFont(16)}];
                    }
                }
                    break;
                case DHAlertControllerTypeTitleRedBoldType:
                {
                    if (title.length != 0) {
                        attributeTitle = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:DHColor(@"ff6f00"),NSFontAttributeName:DHBoldFont(16)}];
                    }
                }
                    break;
                case DHAlertControllerTypeDefault:
                default:
                {
    
                    if (title.length != 0)
                    {
                        attributeTitle = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:DHColor(@"222222"),NSFontAttributeName:DHFont(16)}];
                    }
                }
                    break;
            }
            
            [self setValue:attributeTitle forKey:@"attributedTitle"];
        }
        
        if ([self respondsToSelector:NSSelectorFromString(@"_attributedMessage")]) {
            
            if (message.length != 0) {
                NSAttributedString *attributeMessage = [[NSAttributedString alloc] initWithString:message attributes:@{NSForegroundColorAttributeName:FWTextColor_646464,NSFontAttributeName:DHFont(14)}];
                [self setValue:attributeMessage forKey:@"attributedMessage"];
            }
        }
        
        [actionTitles enumerateObjectsUsingBlock:^(NSString * _Nonnull title, NSUInteger idx, BOOL * _Nonnull stop) {
            
            __block NSUInteger index_ = idx;
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if (handler) {
                    handler(index_);
                }
            }];
            
            if (idx == 0) {
                
                switch (type) {
                    
                    case DHAlertControllerTypeTitleAndFirstActionTitleRedType:
                    {
                        if ([action respondsToSelector:NSSelectorFromString(@"_titleTextColor")])
                        {
                            [action setValue:DHColor(@"ff6f00") forKey:@"titleTextColor"];
                        }
                    }
                        break;
                    case DHAlertControllerTypeDefault:
                    default:
                    {
                        if ([action respondsToSelector:NSSelectorFromString(@"_titleTextColor")]) {
                            [action setValue:DHColor(@"222222") forKey:@"titleTextColor"];
                        }
                    }
                        break;
                }
            } else {
                
                switch (type)
                {
                    case DHAlertControllerTypeTitleRightButtonRedType:
                        if ([action respondsToSelector:NSSelectorFromString(@"_titleTextColor")])
                        {
                            [action setValue:DHColor(@"ff6f00") forKey:@"titleTextColor"];
                        }
                        break;
                        
                    default:
                    {
                        if ([action respondsToSelector:NSSelectorFromString(@"_titleTextColor")]) {
                            [action setValue:DHColor(@"222222") forKey:@"titleTextColor"];
                        }
                    }
                        break;
                }
            }
            
            [self addAction:action];
        }];
        
    }
    return self;
}

@end
