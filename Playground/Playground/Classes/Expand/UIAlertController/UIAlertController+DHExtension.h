//
//  UIAlertController+DHExtension.h
//  Project_Model
//
//  Created by 韩丛旸 on 2017/5/24.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, DHAlertControllerType)
{
    DHAlertControllerTypeDefault,
    DHAlertControllerTypeTitleAndFirstActionTitleRedType,
    DHAlertControllerTypeRedTitleType,
    DHAlertControllerTypeTitleBoldType,
    DHAlertControllerTypeTitleRedBoldType,
    DHAlertControllerTypeTitleRightButtonRedType
};

@interface UIAlertController (DHExtension)

- (instancetype)initWithAlertType:(DHAlertControllerType)type title:(NSString *)title message:(NSString *)message actionTitles:(NSArray<NSString *> *)actionTitles handler:(void (^)(NSInteger index))handler;



@end
