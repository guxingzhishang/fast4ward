//
//  UIViewController+HUD.h
//  Project_Model
//
//  Created by 韩丛旸 on 2017/4/25.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FWHUDType){
    FWHUDType_Success = 1,
    FWHUDType_Error = 2,
    FWHUDType_Loading = 3,
    //FWHUDType_None = 4,
};

@interface UIViewController (HUD)

@property (nonatomic, assign) BOOL isShowOnTheScreen;// 判断当前控制器的view是否显示在屏幕上
@property (nonatomic, assign) BOOL isHUDOnTheScreen;// 判断当前是否有HUD显示在屏幕上

// 通用方法(默认HUD添加到UIViewControllerView上)
- (void)FW_showHUD:(NSString *)title type:(FWHUDType)type;
// 如果涉及到页面跳转情况那么view填写 [AppDelegate shareAppDelegate].window
// 如果view传nil则默认为[AppDelegate shareAppDelegate].window
- (void)FW_showHUD:(NSString *)title type:(FWHUDType)type onView:(UIView *)view;



@end
