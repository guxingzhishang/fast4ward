//
//  UIImage+PageControlSize.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (PageControlSize)

+ (UIImage*)imageWithColor:(UIColor *)color forSize:(CGSize)size;

@end

NS_ASSUME_NONNULL_END
