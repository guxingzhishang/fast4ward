//
//  UIImage+PageControlSize.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "UIImage+PageControlSize.h"

@implementation UIImage (PageControlSize)


+ (UIImage*)imageWithColor:(UIColor *)color forSize:(CGSize)size
{
    if (size.width <= 0 || size.height<= 0 )
    {
        return nil;
    }
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
