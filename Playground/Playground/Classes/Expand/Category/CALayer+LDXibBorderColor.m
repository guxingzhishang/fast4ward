//
//  CALayer+LDXibBorderColor.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "CALayer+LDXibBorderColor.h"
@implementation CALayer (LDXibBorderColor)

- (void)setBorderColorWithUIColor:(UIColor *)color{
    
    self.borderColor = color.CGColor;
}

-(void)setShadowColorWithUIColor:(UIColor *)color{
    
    self.shadowColor = color.CGColor;
}

@end
