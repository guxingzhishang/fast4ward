//
//  UIViewController+HUD.m
//  Project_Model
//
//  Created by 韩丛旸 on 2017/4/25.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import "UIViewController+HUD.h"
#import <objc/runtime.h>
#import "CommonService.h"

static BOOL isHUDOnTheScreenToken;

@implementation UIViewController (HUD)

static char viewControllerOnScreenKey;
static char HUDOnScreenKey;

#pragma mark - > addProperty

- (void)setIsShowOnTheScreen:(BOOL)isViewOnTheScreen {
    objc_setAssociatedObject(self, &viewControllerOnScreenKey, @(isViewOnTheScreen), OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)isShowOnTheScreen {
    
    NSNumber *hudBool = objc_getAssociatedObject(self, &viewControllerOnScreenKey);
    return [hudBool boolValue];
}

- (void)setIsHUDOnTheScreen:(BOOL)isHUDOnTheScreen {
    
    isHUDOnTheScreenToken = isHUDOnTheScreen;
}

- (BOOL)isHUDOnTheScreen {
    NSNumber *hudBool = objc_getAssociatedObject(self, &HUDOnScreenKey);
    return [hudBool boolValue];
}

#pragma mark - > 方法替换

+ (void)load {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SEL systemSel = @selector(viewDidLoad);
        SEL swizzSel = @selector(FW_viewDidLoad);
        [CommonService swizzlingInClass:[self class] originalSelector:systemSel swizzledSelector:swizzSel];
        
        SEL systemSel1 = @selector(viewWillDisappear:);
        SEL swizzSel1 = @selector(FW_viewWillDisappear:);
        [CommonService swizzlingInClass:[self class] originalSelector:systemSel1 swizzledSelector:swizzSel1];
        
        SEL systemSel2 = @selector(viewWillAppear:);
        SEL swizzSel2 = @selector(FW_viewWillAppear:);
        [CommonService swizzlingInClass:[self class] originalSelector:systemSel2 swizzledSelector:swizzSel2];
    });
}

- (void)FW_viewWillAppear:(BOOL)animated {
    
    self.isShowOnTheScreen = YES;
    [self FW_viewWillAppear:animated];
}

- (void)FW_viewWillDisappear:(BOOL)animated {
    self.isShowOnTheScreen = NO;
    [self FW_viewWillDisappear:animated];
}

- (void)FW_viewDidLoad {
    [self FW_viewDidLoad];
    self.isShowOnTheScreen = YES;
}

#pragma mark - > 当前HUD逻辑

- (void)FW_showHUD:(NSString *)title type:(FWHUDType)type onView:(UIView *)view {
    
    if (self.isHUDOnTheScreen == YES) {
        return;
    }
    
    self.isHUDOnTheScreen = YES;
    
    if ([view isEqual:self.view] && self.isShowOnTheScreen == NO) {
        return;
    }
    
    if (view == nil) {
        view = [AppDelegate shareAppDelegate].window;
    }
    
    switch (type)  {
        case FWHUDType_Success: {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                         (int64_t)(0.2 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               
                               MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
                               hud.labelText = title;
                               hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", @"success.png"]]];
                               hud.mode = MBProgressHUDModeCustomView;
                               hud.removeFromSuperViewOnHide = YES;
                               hud.userInteractionEnabled = NO;
                               if([FWHudManager sharedManager].isKeyBoardShowOnTheScreen == YES) hud.yOffset = - 100;
                               [hud hide:YES afterDelay:1.0];
                           });
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                         (int64_t)(1.3 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               self.isHUDOnTheScreen = NO;
                           });
        }
            break;
        case FWHUDType_Error: {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                         (int64_t)(0.2 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               
                               MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
                               hud.labelText = title;
                               hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", @"error.png"]]];
                               hud.mode = MBProgressHUDModeCustomView;
                               hud.removeFromSuperViewOnHide = YES;
                               hud.userInteractionEnabled = NO;
                               if([FWHudManager sharedManager].isKeyBoardShowOnTheScreen == YES) hud.yOffset = - 100;
                               
                               [hud hide:YES afterDelay:1.0];
                           });
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                         (int64_t)(1.3 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                               self.isHUDOnTheScreen = NO;
                           });
            
        }
            break;
        case FWHUDType_Loading: {
            
            if (view == nil) {
                view = [AppDelegate shareAppDelegate].window;
            }
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
            if([FWHudManager sharedManager].isKeyBoardShowOnTheScreen == YES) hud.yOffset = - 100;
            hud.labelText = title;
            hud.minShowTime = 0.2;
            hud.removeFromSuperViewOnHide = YES;
            hud.dimBackground = NO;
            hud.userInteractionEnabled = NO;
        }
            break;
        default:
            break;
    }
    
}


- (void)FW_showHUD:(NSString *)title type:(FWHUDType)type {
    
    [self FW_showHUD:title type:type onView:self.view];
}

- (void)FW_hideAllCurrentVCHud {
    
    [self FW_hideAllCurrentVCHudOnView:[AppDelegate shareAppDelegate].window];
    [self FW_hideAllCurrentVCHudOnView:self.view];
}

- (void)FW_hideAllCurrentVCHudOnView:(UIView *)view {
    
    self.isHUDOnTheScreen = NO;
    [MBProgressHUD hideHUDForView:view animated:NO];
}
@end
