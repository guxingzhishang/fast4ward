//
//  AppMacro.h
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/11.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#ifndef AppMacro_h
#define AppMacro_h

//获取系统对象
#define DHApplication        [UIApplication sharedApplication]
#define DHAppWindow          [UIApplication sharedApplication].delegate.window
#define DHAppDelegate        [AppDelegate shareAppDelegate]
#define DHRootViewController [UIApplication sharedApplication].delegate.window.rootViewController
#define DHAppTabbarControllerConfig   DHAppDelegate.tabBarControllerConfig
#define DHAppTabbarController DHAppDelegate.tabBarController

#define DHUserDefaults       [NSUserDefaults standardUserDefaults]
#define DHNotificationCenter [NSNotificationCenter defaultCenter]


/// 存储应用版本的key
static NSString * const DHGuidePageVersionKey = @"guidePageVersionKey";



//用户信息缓存 名称
#define DHUserCacheName @"DHUserCacheName"

//用户model缓存
#define DHUserModelCache @"DHUserModelCache"



/// 应用名称
#define DH_APP_DisplayName    ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"])
/// 应用版本号
#define DH_APP_VERSION ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"])
/// 应用build
#define DH_APP_BUILD   ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"])


#define Adaptive SCREEN_WIDTH / 750.0

#define DHAdaptive(float) ((Adaptive) * float)
#define DHAdaptiveFor375(float) ((Adaptive * 2) * float)

//屏幕高度判断
#define SCREEN_SIZE             [UIScreen mainScreen].bounds.size
#define SCREEN_FRAME            [UIScreen mainScreen].bounds
#define HEIGHT_STATUS_BAR       [UIApplication sharedApplication].statusBarFrame.size.height

#define HEIGHT_NAVIGATION       self.navigationController.navigationBar.frame.size.height
#define HEIGHT_NAVIGATION_WITHOUT_VC    ((DHNavigationController *)[AppDelegate shareAppDelegate].tabBarVc.selectedViewController).navigationBar.frame.size.height

/// 整个应用的视图的背景色 BackgroundColor
#define DH_Main_BackgroundColor   UIColorHex(FFFFFF)

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

#pragma mark - > 快捷方法

#define DHValidString(string) (([string isKindOfClass:[NSString class]] && ((NSString *)string).length > 0) ? string : @"")
#define DHValidStringWithDefaultString(string, defaultString) (([string isKindOfClass:[NSString class]]) ? string : defaultString)

#define DHValidArray(array) (([array isKindOfClass:[NSArray class]]) ? array : @[])
#define DHValidDictionary(dictionary) (([dictionary isKindOfClass:[NSDictionary class]]) ? dictionary : @{})


#define DHImage(image) [UIImage imageNamed:image]
#define DHImageView(image) [[UIImageView alloc] initWithImage:DHImage(image)]
#define DHColor(color) [UIColor colorWithHexString:color]
#define DHFont(font) [UIFont systemFontOfSize:font]
#define DHBoldFont(font) [UIFont boldSystemFontOfSize:font]
#define DHStringEqualToString(string1, string2) [string1 isEqualToString:string2]
#define DHSeniorPartnerUserInfo [DHUpdateSeniorPartnerManager sharedManager].userInfoModel
#define DHURL(string) [NSURL URLWithString:string]
#define DHUserInfo [DHUserCenter sharedManager]
#define DHUserDefaults [NSUserDefaults standardUserDefaults]
#define DHPostANotification(key, _object) [[NSNotificationCenter defaultCenter] postNotificationName:key object:_object]

#define DHWeakSelf __weak typeof(self) weakSelf = self

#pragma mark - > 机器型号判断

// 是否iPhone4 iPhone4s (640 × 960   Default@2x)
#define DH_isiPhone4     CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 480))

// 是否iPhone5 iPhone5s iPhone5c iPhoneSE (640 × 1136    Default-568h@2x)
#define DH_isiPhone5      CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320, 568))

// 是否iPhone6 iPhone6s iPhone7 (750 × 1334  Default-667h@2x)
#define DH_isiPhone6  CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(375, 667))

// 是否iPhonePlus (1242 × 2208  Default-736h@3x)
#define DH_isiPhone6Plus  CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(414, 736))

//判断iPHoneXr iPhone 11 (828 x 1792   Default-896h@2x)
//#define DH_isiPhoneXR CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size)
#define DH_isiPhoneXR ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size)  : NO)

//判断iPHoneX iPhoneXs iPhone 11 Pro (1125 × 2436  Default-812h@3x)
#define DH_isiPhoneX    CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(375, 812))

//判断iPhoneXs Max iPhone 11 Pro Max (1242 x 2688   Default-896h@3x)
//#define DH_isiPhoneXMax  CGSizeEqualToSize([UIScreen mainScreen].bounds.size,CGSizeMake(414, 896))
#define DH_isiPhoneXMax ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)

/// 导航条高度
#define APPLICATION_TOP_BAR_HEIGHT (DH_isiPhoneX?88.0f:64.0f)
/// tabBar高度
#define APPLICATION_TAB_BAR_HEIGHT (DH_isiPhoneX?83.0f:49.0f)
/// 状态栏高度
#define APPLICATION_STATUS_BAR_HEIGHT (DH_isiPhoneX?44:20.0f)


//常用提示语
static NSString * const MESSAGE_CONNECTIONERROR = @"连接失败,请检查您的网络";


typedef void (^VoidBlock)(void);

#endif /* AppMacro_h */
