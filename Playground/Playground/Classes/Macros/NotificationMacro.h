//
//  NotificationMacro.h
//  MVVMRACProject
//
//  Created by zhuluole on 2018/9/11.
//  Copyright © 2018年 zhuluole. All rights reserved.
//

#ifndef NotificationMacro_h
#define NotificationMacro_h

// 切换根控制器的通知 新特性
static NSString * const DHSwitchRootViewControllerNotification = @"DHSwitchRootViewControllerNotification";

#endif /* NotificationMacro_h */
