//
//  FWHomeViewModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWHomeViewModel.h"

@implementation FWHomeViewModel

- (void)initialize
{
    [super initialize];
    
    self.title=@"首页";
    
    self.prefersNavigationBarHidden = YES;
}
@end
