//
//  FWLoginViewModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWLoginViewModel.h"

@implementation FWLoginViewModel

- (void)initialize
{
    [super initialize];
    
    self.prefersNavigationBarHidden = YES;
}
@end
