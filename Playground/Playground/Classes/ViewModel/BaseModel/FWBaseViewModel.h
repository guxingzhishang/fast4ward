//
//  FWBaseModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWNetworkRequest.h"

typedef NS_ENUM(NSUInteger, FWNavigationBarType) {
    DHNavigationBarTypeWhite = 0,//白色底 黑色字
    DHNavigationBarTypeNormal ,// 系统导航栏主色调   例如 红色底、白色字
};

typedef NS_ENUM(NSUInteger, FWNavigationBarBackItemTitleColorType) {
    DHNavigationBarBackItemTitleColorTypeWhite = 0 ,//白色底，黑色字
    DHNavigationBarBackItemTitleColorTypeNormal ,// 系统导航栏主色调   例如 红色底、白色字
};

@interface FWBaseViewModel : NSObject

@property (strong, nonatomic)FWNetworkRequest *request;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) FWNavigationBarType navigationBarType;

@property (nonatomic, assign) UIColor *BackItemTitleColor;

/// FDFullscreenPopGesture. (是否取消掉左滑pop到上一层的功能（栈底控制器无效），默认为NO，不取消)
@property (nonatomic, assign) BOOL interactivePopDisabled;

/// 是否隐藏该控制器的导航栏 默认是不隐藏 (NO)
@property (nonatomic, assign) BOOL prefersNavigationBarHidden;

/// IQKeyboardManager
/// 是否让IQKeyboardManager的管理键盘的事件 默认是YES（键盘管理）
@property (nonatomic, assign) BOOL keyboardEnable;
/// 是否键盘弹起的时候，点击其他局域键盘弹起 默认是 YES
@property (nonatomic, assign) BOOL shouldResignOnTouchOutside;

- (instancetype)initWithModel:(id)model;

- (void)initialize;
@end
