//
//  FWTabBarModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewModel.h"
#import "FWHomeViewModel.h"
#import "FWMineViewModel.h"

@interface FWTabBarViewModel : FWBaseViewModel

@property (nonatomic, strong) FWHomeViewModel *homeViewModel;

@property (nonatomic, strong) FWMineViewModel *mineViewModel;

@end
