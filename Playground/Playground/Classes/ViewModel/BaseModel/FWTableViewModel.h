//
//  FWTableViewModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWTableViewModel.h"

@interface FWTableViewModel : FWBaseViewModel

// The data source of table view. 这里不能用NSMutableArray，因为NSMutableArray不支持KVO，不能被RACObserve
@property (nonatomic, strong) NSArray *dataSource;

@property (nonatomic, assign) UITableViewStyle style;
/// 是否数据是多段  defalut is NO
@property (nonatomic, assign) BOOL shouldMultiSections;

/// 需要支持下来刷新 defalut is NO
@property (nonatomic, assign) BOOL shouldPullDownToRefresh;
/// 需要支持上拉加载 defalut is NO
@property (nonatomic, assign) BOOL shouldPullUpToLoadMore;
/// 是否在上拉加载后的数据,dataSource.count < pageSize 提示没有更多的数据.default is NO 默认做法是数据不够时，隐藏mj_footer
@property (nonatomic, assign) BOOL shouldEndRefreshingWithNoMoreData;

/// 当前页 defalut is 1
@property (nonatomic,  assign) NSUInteger page;
/// 每一页的数据 defalut is 20
@property (nonatomic,  assign) NSUInteger perPage;


/// 选中命令 eg:  didSelectRowAtIndexPath:
@property (nonatomic,  strong) RACCommand *didSelectCommand;
/// 请求服务器数据的命令
@property (nonatomic,  strong) RACCommand *requestRemoteDataCommand;

- (id)fetchLocalData;

/** request remote data or local data, sub class can override it
 *  page - 请求第几页的数据
 */
- (RACSignal *)requestRemoteDataSignalWithPage:(NSUInteger)page;

@end
