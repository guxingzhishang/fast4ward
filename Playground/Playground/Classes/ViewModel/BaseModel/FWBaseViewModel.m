//
//  FWBaseViewModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewModel.h"
#import <ReactiveObjC.h>

@implementation FWBaseViewModel

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    FWBaseViewModel *viewModel = [super allocWithZone:zone];
    
    if (viewModel) {
        [viewModel initialize];
    }
    return viewModel;
}

- (instancetype)initWithModel:(id)model {
    
    self = [super init];
    if (self) {
    }
    return self;
}


/// sub class can override
- (void)initialize {
    
    
}

@end
