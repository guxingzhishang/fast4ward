//
//  FWPlayerRegistrationView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerRegistrationView.h"
#import "UILabel+YBAttributeTextTapAction.h"
#import "UIBarButtonItem+Item.h"

@interface FWPlayerRegistrationView()<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@end

@implementation FWPlayerRegistrationView

- (id)init{
    self = [super init];
    if (self) {
        self.type = @"1";
        self.firstArray = @[].mutableCopy;
        
        self.backgroundColor = FWTextColor_F6F8FA;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setInfoModel:(FWBaomingInfoModel *)infoModel{
    _infoModel = infoModel;

    if (self.infoModel.mobile_notice.length > 0) {
        self.tipLabel.text = self.infoModel.mobile_notice;
        self.tipLabel.frame = CGRectMake(CGRectGetMinX(self.phoneLabel.frame), CGRectGetMaxY(self.phoneTF.frame)+15, 250, 20);
    }else{
        self.tipLabel.frame = CGRectMake(CGRectGetMinX(self.phoneLabel.frame), CGRectGetMaxY(self.phoneTF.frame), 250, 0.01);
    }
    
    if ([self.infoModel.extra_status isEqualToString:@"1"]) {
        /* 显示选择器 */
        
        self.chooseLabel.hidden = NO;
        self.chooseTF.hidden = NO;
        self.arrowImageView.hidden = NO;
        self.chooseLabel.text = self.infoModel.extra_desc;
        self.chooseTF.placeholder = self.infoModel.extra_tip;

        self.chooseLabel.frame = CGRectMake(CGRectGetMinX(self.tipLabel.frame), CGRectGetMaxY(self.tipLabel.frame)+10, 100, 20);
        self.chooseTF.frame = CGRectMake(CGRectGetMinX(self.chooseLabel.frame), CGRectGetMaxY(self.chooseLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.chooseLabel.frame), 38);

        [self.chooseLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.chooseTF);
            make.top.mas_equalTo(self.chooseTF.mas_bottom);
            make.right.mas_equalTo(self.chooseTF);
            make.height.mas_equalTo(0.5);
            make.width.mas_greaterThanOrEqualTo(100);
            make.bottom.mas_equalTo(self.topView).mas_offset(-20);
        }];
    }else{
        /* 不显示选择器 */
        self.chooseLabel.hidden = YES;
        self.chooseTF.hidden = YES;
        self.arrowImageView.hidden = YES;
        
        [self.chooseLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.tipLabel);
            make.top.mas_equalTo(self.tipLabel.mas_bottom);
            make.right.mas_equalTo(self.topView).mas_offset(-14);
            make.height.mas_equalTo(0.01);
            make.width.mas_greaterThanOrEqualTo(100);
            make.bottom.mas_equalTo(self.topView).mas_offset(-10);
        }];
    }
    
    [self.middleView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.topView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(52);
    }];
    
    [self.selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self.middleView);
        make.height.mas_equalTo(52);
        make.width.mas_equalTo(50);
    }];
    
    if ([infoModel.extra_status isEqualToString:@"1"]) {
        for (FWExtraListModel * extraModel in self.infoModel.extra_list) {
            [self.firstArray addObject:extraModel.val];
        }
    }
}

- (void)setupSubviews{
    
    self.topView = [[UIView alloc] init];
    self.topView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.topView];
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(100);
    }];
    
    self.realNameLabel = [UILabel new];
    self.realNameLabel.text = @"真实姓名";
    self.realNameLabel.frame = CGRectMake(15, 18, 100, 16);
    self.realNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.realNameLabel.textColor = FWTextColor_12101D;
    self.realNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.realNameLabel];
    
    self.realNameTF = [[UITextField alloc] init];
    self.realNameTF.delegate = self;
    self.realNameTF.placeholder = @"请填写真实姓名";
    self.realNameTF.textColor = FWTextColor_12101D;
    self.realNameTF.frame = CGRectMake(CGRectGetMinX(self.realNameLabel.frame), CGRectGetMaxY(self.realNameLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.realNameLabel.frame), 38);
    [self.topView addSubview:self.realNameTF];
    
    self.realNameLineView =  [[UIView alloc] init];
    self.realNameLineView.frame = CGRectMake(CGRectGetMinX(self.realNameLabel.frame), CGRectGetMaxY(self.realNameTF.frame), CGRectGetWidth(self.realNameTF.frame), 0.5);
    self.realNameLineView.backgroundColor = FWTextColor_141414;
    [self.topView addSubview:self.realNameLineView];
    
    self.selectLabel = [UILabel new];
    self.selectLabel.text = @"选择证件";
    self.selectLabel.frame = CGRectMake(CGRectGetMinX(self.realNameLabel.frame), CGRectGetMaxY(self.realNameLineView.frame)+20, 100, 16);
    self.selectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.selectLabel.textColor = FWTextColor_12101D;
    self.selectLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.selectLabel];
    
    self.idCardButton = [[UIButton alloc] init];
    self.idCardButton.selected = YES;
    [self.idCardButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.idCardButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    self.idCardButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.idCardButton setTitle:@" 身份证" forState:UIControlStateNormal];
    [self.idCardButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.idCardButton addTarget:self action:@selector(idCardButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.idCardButton];
    self.idCardButton.frame = CGRectMake(CGRectGetMinX(self.realNameLabel.frame), CGRectGetMaxY(self.selectLabel.frame)+20, 80, 30);
    [self.idCardButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.selectLabel);
        make.top.mas_equalTo(self.selectLabel.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(80);
    }];
    
    self.postButton = [[UIButton alloc] init];
    [self.postButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.postButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    self.postButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.postButton setTitle:@" 护照/港澳证" forState:UIControlStateNormal];
    [self.postButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.postButton addTarget:self action:@selector(postButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.postButton];
    [self.postButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.idCardButton.mas_right).mas_offset(30);
        make.centerY.mas_equalTo(self.idCardButton);
        make.height.mas_equalTo(self.idCardButton);
        make.width.mas_equalTo(110);
    }];
    
    self.cardNumLabel = [UILabel new];
    self.cardNumLabel.text = @"证件号码";
    self.cardNumLabel.frame = CGRectMake(CGRectGetMinX(self.selectLabel.frame), CGRectGetMaxY(self.idCardButton.frame)+5, 100, 20);
    self.cardNumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.cardNumLabel.textColor = FWTextColor_12101D;
    self.cardNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.cardNumLabel];
    
    self.cardNumTF = [[UITextField alloc] init];
    self.cardNumTF.delegate = self;
    self.cardNumTF.placeholder = @"请填写证件号码";
    self.cardNumTF.textColor = FWTextColor_12101D;
    self.cardNumTF.frame = CGRectMake(CGRectGetMinX(self.cardNumLabel.frame), CGRectGetMaxY(self.cardNumLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.cardNumLabel.frame), 38);
    [self.topView addSubview:self.cardNumTF];
    
    self.cardNumLineView =  [[UIView alloc] init];
    self.cardNumLineView.frame = CGRectMake(CGRectGetMinX(self.cardNumTF.frame), CGRectGetMaxY(self.cardNumTF.frame), CGRectGetWidth(self.cardNumTF.frame), 0.5);
    self.cardNumLineView.backgroundColor = FWTextColor_141414;
    [self.topView addSubview:self.cardNumLineView];
    
    self.phoneLabel = [UILabel new];
    self.phoneLabel.text = @"手机号";
    self.phoneLabel.frame = CGRectMake(CGRectGetMinX(self.cardNumLabel.frame), CGRectGetMaxY(self.cardNumLineView.frame)+15, 100, 20);
    self.phoneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.phoneLabel.textColor = FWTextColor_12101D;
    self.phoneLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.phoneLabel];
    
    self.phoneTF = [[UITextField alloc] init];
    self.phoneTF.delegate = self;
    self.phoneTF.placeholder = @"请填写手机号码";
    self.phoneTF.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.phoneTF.textColor = FWTextColor_12101D;
    self.phoneTF.frame = CGRectMake(CGRectGetMinX(self.phoneLabel.frame), CGRectGetMaxY(self.phoneLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.phoneLabel.frame), 38);
    [self.topView addSubview:self.phoneTF];
    
    self.phoneLineView =  [[UIView alloc] init];
    self.phoneLineView.frame = CGRectMake(CGRectGetMinX(self.phoneTF.frame), CGRectGetMaxY(self.phoneTF.frame), CGRectGetWidth(self.phoneTF.frame), 0.5);
    self.phoneLineView.backgroundColor = FWTextColor_141414;
    [self.topView addSubview:self.phoneLineView];
    
    self.tipLabel = [UILabel new];
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.tipLabel.textColor = FWTextColor_9EA3AB;
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.tipLabel];
    
    if (self.infoModel.mobile_notice.length > 0) {
        self.tipLabel.text = self.infoModel.mobile_notice;
        self.tipLabel.frame = CGRectMake(CGRectGetMinX(self.phoneLabel.frame), CGRectGetMaxY(self.phoneTF.frame)+15, 250, 20);
    }else{
        self.tipLabel.frame = CGRectMake(CGRectGetMinX(self.phoneLabel.frame), CGRectGetMaxY(self.phoneTF.frame), 250, 0.01);
    }
    
    self.chooseLabel = [UILabel new];
    self.chooseLabel.frame = CGRectMake(CGRectGetMinX(self.tipLabel.frame), CGRectGetMaxY(self.tipLabel.frame)+15, 100, 20);
    self.chooseLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.chooseLabel.textColor = FWTextColor_12101D;
    self.chooseLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.chooseLabel];
    
    self.chooseTF = [[UITextField alloc] init];
    self.chooseTF.delegate = self;
    self.chooseTF.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.chooseTF.textColor = FWTextColor_12101D;
    self.chooseTF.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.chooseTF.frame = CGRectMake(CGRectGetMinX(self.chooseLabel.frame), CGRectGetMaxY(self.chooseLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.chooseLabel.frame), 38);
    [self.topView addSubview:self.chooseTF];
    
    self.arrowImageView = [[UIImageView alloc] init];
    self.arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.chooseTF addSubview:self.arrowImageView];
    [self.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.centerY.mas_equalTo(self.chooseTF);
        make.right.mas_equalTo(self.chooseTF).mas_offset(-10);
    }];
    
    self.chooseLineView =  [[UIView alloc] init];
    self.chooseLineView.backgroundColor = FWTextColor_141414;
    [self.topView addSubview:self.chooseLineView];
    
    self.pickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.pickerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.pickerView.backgroundColor=[UIColor clearColor];
    
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    
    UIBarButtonItem *doneButton = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar.items = @[cancelButton,flexItem, fixItem, doneButton];
    
    self.chooseTF.inputView = self.pickerView;
    self.chooseTF.inputAccessoryView = bar;

    if ([self.infoModel.extra_status isEqualToString:@"1"]) {
        /* 显示选择器 */
        
        self.chooseLabel.hidden = NO;
        self.chooseTF.hidden = NO;
        self.arrowImageView.hidden = NO;
        
        [self.chooseLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.chooseTF);
            make.top.mas_equalTo(self.chooseTF.mas_bottom);
            make.right.mas_equalTo(self.chooseTF);
            make.height.mas_equalTo(0.5);
            make.width.mas_greaterThanOrEqualTo(100);
            make.bottom.mas_equalTo(self.topView).mas_offset(-20);
        }];
    }else{
        /* 不显示选择器 */
        self.chooseLabel.hidden = YES;
        self.chooseTF.hidden = YES;
        self.arrowImageView.hidden = YES;
        
        [self.chooseLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.tipLabel);
            make.top.mas_equalTo(self.tipLabel.mas_bottom);
            make.right.mas_equalTo(self.topView).mas_offset(-14);
            make.height.mas_equalTo(0.5);
            make.width.mas_greaterThanOrEqualTo(100);
            make.bottom.mas_equalTo(self.topView).mas_offset(-20);
        }];
    }
    
    
    self.middleView = [[UIView alloc] init];
    self.middleView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.middleView];
    [self.middleView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.topView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(52);
    }];
    
    self.selectButton = [[UIButton alloc] init];
    self.selectButton.selected = NO;
    [self.selectButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.selectButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    [self.selectButton addTarget:self action:@selector(selectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.middleView addSubview:self.selectButton];
    [self.selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self.middleView);
        make.height.mas_equalTo(52);
        make.width.mas_equalTo(50);
    }];
    
    NSString *protocolText = @"我已认真阅读并同意车手参赛协议。";
    NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc]initWithString:protocolText];
    [attributedString2 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, protocolText.length)];
    [attributedString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"9EA3AB" alpha:1] range:NSMakeRange(0, 9)];
    [attributedString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"69759B" alpha:1] range:NSMakeRange(9, 7)];
    
    self.protocolLabel = [[UILabel alloc] init];
    self.protocolLabel.backgroundColor = FWClearColor;
    self.protocolLabel.numberOfLines = 1;
    self.protocolLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];;
    self.protocolLabel.attributedText = attributedString2;
    //设置是否有点击效果，默认是YES
    self.protocolLabel.enabledTapEffect = NO;
    [self.middleView addSubview:self.protocolLabel];
    [self.protocolLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.selectButton);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(250);
        make.left.mas_equalTo(self.selectButton.mas_right).mas_offset(-10);
    }];
    
    [self.protocolLabel yb_addAttributeTapActionWithStrings:@[@"车手参赛协议。"] tapClicked:^(NSString *string, NSRange range, NSInteger index) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = self.infoModel.h5_agreement;
        [self.vc.navigationController pushViewController:WVC animated:YES];
    }];
    
    self.commitButton = [[UIButton alloc] init];
    self.commitButton.layer.cornerRadius = 2;
    self.commitButton.layer.masksToBounds = YES;
    self.commitButton.backgroundColor = FWTextColor_222222;
    self.commitButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    [self.commitButton setTitle:@"提交报名信息" forState:UIControlStateNormal];
    [self.commitButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.commitButton addTarget:self action:@selector(commitButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.commitButton];
    [self.commitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.middleView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self).mas_offset(30);
        make.right.mas_equalTo(self).mas_offset(-30);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(SCREEN_WIDTH-60);
        make.bottom.mas_greaterThanOrEqualTo(self).mas_offset(-125);
    }];
}

#pragma mark - > 选择身份证
- (void)idCardButtonClick{
    self.type = @"1";
    self.idCardButton.selected = YES;
    self.postButton.selected = NO;
}

#pragma mark - > 选择护照
- (void)postButtonClick{
    self.type = @"2";
    self.idCardButton.selected = NO;
    self.postButton.selected = YES;
}

#pragma mark - > 同意协议
- (void)selectButtonClick{
    self.selectButton.selected = !self.selectButton.selected;
}

#pragma mark - > 提交信息
- (void)commitButtonOnClick{
    
    self.commitButton.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.commitButton.enabled = YES;
    });
    
    /* 真实姓名 */
    NSString * realName = [self checkString:self.realNameTF.text];
    if (realName.length == 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写真实姓名" toController:self.vc];
        return;
    }
    
    /* 身份证 */
    NSString * idcardString = [self checkString:self.cardNumTF.text];
    if (idcardString.length == 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写证件号码" toController:self.vc];
        return;
    }else if ([self.type isEqualToString:@"1"] && idcardString.length != 18){
        [[FWHudManager sharedManager] showErrorMessage:@"证件号格式错误" toController:self.vc];
        return;
    }
    
    /* 手机号 */
    NSString * phoneString = [self checkString:self.phoneTF.text];
    if (phoneString.length != 11) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写11位手机号码" toController:self.vc];
        return;
    }
    
    /* 阅读协议 */
    if (!self.selectButton.isSelected) {
        [[FWHudManager sharedManager] showErrorMessage:@"您尚未同意车手参赛协议" toController:self.vc];
        return;
    }
    
    /* 选择扩展列表 */
    if ([self.infoModel.extra_status isEqualToString:@"1"]){
        if (self.extra_id.length <= 0) {
            [[FWHudManager sharedManager] showErrorMessage:self.infoModel.extra_tip toController:self.vc];
            return;
        }
    }else{
        self.extra_id = @"0";
    }
   
    
    if ([self.registrationDelegate respondsToSelector:@selector(commitButtonClick)]) {
        [self.registrationDelegate commitButtonClick];
    }
}

#pragma mark - > 去掉前后空格
- (NSString *)checkString:(NSString *)string{
    
    NSCharacterSet  *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    string = [string stringByTrimmingCharactersInSet:set];
    return string;
}

#pragma mark - > pickerview数据源及代理方法
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

#pragma mark 数据源  numberOfRowsInComponent
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.firstArray.count;
}

#pragma delegate 显示信息方法
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.firstArray objectAtIndex:row];
}

#pragma mark 选中行信息
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
}

#pragma mark 显示行高
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.00;
}

#pragma mark - pickerView 每列宽度
- (CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    return SCREEN_WIDTH;
}

#pragma mark - > 选中
- (void)doneButtonClick:(UIBarButtonItem *)button{
    [self endEditing:YES];
    
    NSInteger onerow=[self.pickerView selectedRowInComponent:0];
    
    if (self.firstArray.count > onerow) {
        self.chooseTF.text = self.firstArray[onerow];
        self.extra_id = self.infoModel.extra_list[onerow].extra_id;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = (UILabel *)view;
    if (lbl == nil) {
        lbl = [[UILabel alloc]init];
        //在这里设置字体相关属性
        lbl.font = [UIFont systemFontOfSize:18];
        lbl.textColor = FWTextColor_000000;
        [lbl setTextAlignment:1];
        [lbl setBackgroundColor:[UIColor clearColor]];
    }
    //重新加载lbl的文字内容
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return lbl;
}
#pragma mark - > 取消
- (void)cancelButtonClick:(UIBarButtonItem *)button{
    [self endEditing:YES];
}

@end
