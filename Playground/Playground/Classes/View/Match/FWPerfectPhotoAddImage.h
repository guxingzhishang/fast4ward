//
//  FWPerfectPhotoAddImage.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWPerfectPhotoAddImageDelegate <NSObject>

#pragma mark - > 对图片进行了操作（添加或删除）
- (void)operationPicture;

@end

@interface FWPerfectPhotoAddImage : UIView<UIGestureRecognizerDelegate>

/**
 *  存储所有的照片(UIImage)
 */
@property (nonatomic, strong) NSMutableArray *images;

@property (nonatomic, strong) NSMutableArray * showImage;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) id<FWPerfectPhotoAddImageDelegate> delegate;

/* 最多显示图片个数 */
@property (nonatomic, assign) NSInteger  maxCount;

- (void)stop;

- (NSMutableArray *)getImagesArray;

@end

NS_ASSUME_NONNULL_END
