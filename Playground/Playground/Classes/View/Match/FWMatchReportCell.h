//
//  FWMatchReportCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchReportCell : UITableViewCell

@property (nonatomic, strong) UIImageView * picImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, strong) UIImageView * typeImageView;
@property (nonatomic, strong) UILabel * typeLabel;

@property (nonatomic, strong) FWF4WMatchNewsModel * newsModel;

- (void)configForCell:(FWF4WMatchNewsModel *)model;

@end

NS_ASSUME_NONNULL_END
