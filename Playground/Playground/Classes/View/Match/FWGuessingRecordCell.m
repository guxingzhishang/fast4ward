//
//  FWGuessingRecordCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGuessingRecordCell.h"
#import "FWGuessingRecordModel.h"

@implementation FWGuessingRecordCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{

    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.height.mas_equalTo(195);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-1);
    }];
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.statusImageView = [[UIImageView alloc] init];
    [self.bgView addSubview:self.statusImageView];
    [self.statusImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(48, 48));
        make.top.right.mas_equalTo(self.bgView);
    }];
    
    self.OneLabel = [[UILabel alloc] init];
    self.OneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.OneLabel.textColor = FWTextColor_222222;
    self.OneLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.OneLabel];
    [self.OneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(12);
        make.left.mas_equalTo(self.bgView).mas_offset(14);
        make.right.mas_equalTo(self.statusImageView.mas_left).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];
    
    self.TwoLabel = [[UILabel alloc] init];
    self.TwoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.TwoLabel.textColor = FWTextColor_222222;
    self.TwoLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.TwoLabel];
    [self.TwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.OneLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.OneLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.OneLabel);
        make.height.mas_equalTo(self.OneLabel);
    }];

    self.ThreeLabel = [[UILabel alloc] init];
    self.ThreeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.ThreeLabel.textColor = FWTextColor_222222;
    self.ThreeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.ThreeLabel];
    [self.ThreeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.TwoLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.TwoLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.TwoLabel);
        make.height.mas_equalTo(self.TwoLabel);
    }];

    self.FourLabel = [[UILabel alloc] init];
    self.FourLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.FourLabel.textColor = FWTextColor_222222;
    self.FourLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.FourLabel];
     [self.FourLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.ThreeLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.ThreeLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.ThreeLabel);
        make.height.mas_equalTo(self.ThreeLabel);
    }];
    
    self.FiveLabel = [[UILabel alloc] init];
    self.FiveLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.FiveLabel.textColor = FWTextColor_222222;
    self.FiveLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.FiveLabel];
    [self.FiveLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.FourLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.FourLabel);
        make.width.mas_equalTo((SCREEN_WIDTH-4*14)/2);
        make.height.mas_equalTo(self.FourLabel);
    }];
    
    self.SixLabel = [[UILabel alloc] init];
    self.SixLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.SixLabel.textColor = FWTextColor_222222;
    self.SixLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.SixLabel];
    [self.SixLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.FiveLabel);
        make.left.mas_equalTo(self.FiveLabel.mas_right).mas_offset(FWAdaptive(30));
        make.width.mas_equalTo(self.FiveLabel);
        make.right.mas_equalTo(self.bgView).mas_offset(14);
        make.height.mas_equalTo(self.FiveLabel);
    }];

    self.SevenLabel = [[UILabel alloc] init];
    self.SevenLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.SevenLabel.textColor = FWTextColor_222222;
    self.SevenLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.SevenLabel];
    [self.SevenLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.FiveLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.FiveLabel);
        make.width.mas_equalTo(self.FiveLabel);
        make.height.mas_equalTo(self.FiveLabel);
    }];
    
    self.EightLabel = [[UILabel alloc] init];
    self.EightLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.EightLabel.textColor = FWTextColor_222222;
    self.EightLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.EightLabel];
    [self.EightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.SevenLabel);
        make.left.mas_equalTo(self.SevenLabel.mas_right).mas_offset(12);
        make.width.mas_equalTo(self.FiveLabel);
        make.right.mas_equalTo(self.bgView).mas_offset(14);
        make.height.mas_equalTo(self.FiveLabel);
    }];
}

- (void)configForCell:(id)model{
    
    FWGuessingRecordListModel * listModel = (FWGuessingRecordListModel *)model;
    
    NSString * result ;
    
    if ([listModel.guess_status isEqualToString:@"1"]) {
        result = @"赢";
        self.SevenLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"竞猜结果：%@",result] WithLength:5];
    }else if ([listModel.guess_status isEqualToString:@"2"]) {
        result = @"输";
        self.SevenLabel.text = [NSString stringWithFormat:@"竞猜结果：%@",result];
    }else{
        result = @"--";
        self.SevenLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"竞猜结果：%@",result] WithLength:5];
    }
    
    if ([listModel.win_car_no isEqualToString:listModel.car_1_info.car_no]) {
        self.FourLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"已选：%@",listModel.car_1_info.nickname] WithLength:3];
    }else{
        self.FourLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"已选：%@",listModel.car_2_info.nickname] WithLength:3];
    }

    
    NSString * peilv ;
    NSString * getGolden ;

    if ([listModel.status isEqualToString:@"1"]) {
        /* 进行中 */
        self.statusImageView.image = [UIImage imageNamed:@"guess_ing"];
        
        peilv = @"--";
        getGolden = @"--";
    }else if ([listModel.status isEqualToString:@"2"]) {
        /* 封盘 */
        self.statusImageView.image = [UIImage imageNamed:@"guess_fengpan"];
        peilv = listModel.fp_odds;
        getGolden = @"--";
    }else if ([listModel.status isEqualToString:@"4"]) {
        /* 已结束 */
        self.statusImageView.image = [UIImage imageNamed:@"guess_end"];
        peilv = listModel.fp_odds;
        getGolden = listModel.real_get_gold;
    }else{
        self.statusImageView.image = [UIImage imageNamed:@""];
    }
    
    self.OneLabel.text = listModel.sport_name;
    self.TwoLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"场次：%@ %@",listModel.group_name,listModel.phase_name] WithLength:3];
    self.ThreeLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"对决：%@  VS  %@",listModel.car_1_info.nickname,listModel.car_2_info.nickname] WithLength:3];
    self.SixLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"投注金币：%@",listModel.guess_gold] WithLength:5];
   
    self.FiveLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"封盘赔率：%@",peilv] WithLength:5];
    self.EightLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"获得金币：%@",getGolden] WithLength:5];
}

- (NSMutableAttributedString *)attributedString:(NSString *)text WithLength:(NSInteger)length{
    
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attrStr addAttribute: NSForegroundColorAttributeName value: FWColor(@"ff6f00") range: NSMakeRange(length, attrStr.length-length)];
    return attrStr;
}
@end
