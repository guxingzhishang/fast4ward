//
//  FWMatchPicsCollectionCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchPicsCollectionCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView * picImageView;
@end

NS_ASSUME_NONNULL_END
