//
//  FWHotLiveNoticeCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHotLiveNoticeCell.h"

@implementation FWHotLiveNoticeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = FWTextColor_F8F8F8;
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews{
    
    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(0);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_equalTo(169);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-10);
    }];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.image = [UIImage imageNamed:@"placeholder"];
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.iconImageView.clipsToBounds = YES;
    [self.bgView addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(169, 169));
        make.top.left.mas_equalTo(self.bgView).mas_offset(0);
    }];
  
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconImageView).mas_offset(14);
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(10);
        make.right.mas_equalTo(self.bgView).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(20);
    }];
   
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.numberOfLines = 3;
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.timeLabel.textColor = FWTextColor_A7ADB4;
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.titleLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];
    
    
    self.photoImageView = [UIImageView new];
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    self.photoImageView.layer.cornerRadius = 33/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.bgView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel).mas_offset(0);
        make.width.mas_equalTo(33);
        make.height.mas_equalTo(33);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-12);
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.photoImageView addGestureRecognizer:tap];
    
    self.authenticationView = [UIImageView new];
    [self.bgView addSubview:self.authenticationView];
    [self.authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.photoImageView).mas_offset(2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(2);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nameLabel.textColor = FWTextColor_272727;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.bgView).mas_offset(-10);
        make.height.mas_equalTo(20);
    }];
}

- (void)configForCell:(id)model{

    self.listModel = (FWLiveListModel *)model;
    
    self.titleLabel.text = self.listModel.live_title;
    self.timeLabel.text = self.listModel.live_time;
    self.nameLabel.text = self.listModel.user_info.nickname;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.live_cover]];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.user_info.header_url]];

    /* 认证 */
    if ([self.listModel.user_info.cert_status isEqualToString:@"2"])
    {
        self.authenticationView.hidden = NO;
        self.authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        self.authenticationView.hidden = YES;
    }
}

#pragma mark - > 跳转至个人页
- (void)tapClick{
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.user_info.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

@end
