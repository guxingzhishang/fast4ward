//
//  FWFinalsRankView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/28.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 *
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWFinalsRankView : UIScrollView

/* 有几列 */
@property (assign, nonatomic) NSInteger lineCount;
/* 第一列，有几行（理论上第一列是数据最多的）*/
@property (assign, nonatomic) NSInteger maxRowCount;

@property (nonatomic, weak) UIViewController * vc;


- (void)configForViewWithData:(id)model;

@end

NS_ASSUME_NONNULL_END
