//
//  FWFinalsBettleView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/28.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 * 决赛排名 两人对决
 */

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface FWFinalsBettleView : UIView

@property (nonatomic, strong) UIView * containerView;

@property (nonatomic, strong) UIImageView * upImageView;

@property (nonatomic, strong) UIImageView * downImageView;

@property (nonatomic, strong) UIImageView * vsImageView;

@property (nonatomic, strong) UIImageView * firstImageView;

@property (nonatomic, strong) UIImageView * secondImageView;


@property (nonatomic, strong) UILabel * upNameLabel;

@property (nonatomic, strong) UILabel * downNameLabel;

@property (nonatomic, strong) UILabel * upTimeLabel;

@property (nonatomic, strong) UILabel * downTimeLabel;

@property (nonatomic, strong) UIView * henView;

@property (nonatomic, strong) UIView * henFowardView;

@property (nonatomic, strong) UIView * upVerView;

@property (nonatomic, strong) UIView * downVerView;

@property (nonatomic, strong) NSString * upTime;

@property (nonatomic, strong) NSString * downTime;

@property (nonatomic, strong) FWFinalSubListModel * model;

@property (nonatomic, weak) UIViewController * vc;


- (void)refreshData:(id)dataSource withIndex:(NSInteger)index WithTotal:(NSInteger)total;

@end

NS_ASSUME_NONNULL_END
