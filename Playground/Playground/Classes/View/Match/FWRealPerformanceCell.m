//
//  FWRealPerformanceCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/7.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWRealPerformanceCell.h"
#import "FWScoreModel.h"
#import "FWNewShareScoreViewController.h"

@implementation FWRealPerformanceCell
@synthesize moreButton;
@synthesize leftWinnerImageView;
@synthesize rightWinnerImageView;
@synthesize leftImageView;
@synthesize rightImageView;
@synthesize leftNameLabel;
@synthesize rightNameLabel;
@synthesize leftNoLabel;
@synthesize rightNoLabel;
@synthesize leftPointView;
@synthesize rightPointView;
@synthesize leftCarLabel;
@synthesize rightCarLabel;
@synthesize leftFoulLabel;
@synthesize rightFoulLabel;;
@synthesize leftETLabel;
@synthesize rightETLabel;
@synthesize leftETNumberLabel;
@synthesize rightETNumberLabel;
@synthesize leftWeisuLabel;
@synthesize rightWeisuLabel;
@synthesize leftWeisuNumberLabel;
@synthesize rightWeisuNumberLabel;
@synthesize leftRTLabel;
@synthesize rightRTLabel;
@synthesize leftRTNumberLabel;
@synthesize rightRTNumberLabel;
@synthesize VSImageView;
@synthesize shareButton;
@synthesize lineView;
@synthesize matchModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
     
        [self setupMiddleSubViews];
        [self setupLeftSubViews];
        [self setupRightSubViews];
    }
    
    return self;
}

- (void)setupMiddleSubViews{
    
    VSImageView = [[UIImageView alloc] init];
    VSImageView.image = [UIImage imageNamed:@"match_vs"];
    [self.contentView addSubview:VSImageView];
    [VSImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.contentView).mas_offset(20);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    shareButton = [[UIButton alloc] init];
    [shareButton setImage:[UIImage imageNamed:@"match_share"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:shareButton];
    [shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 25));
    }];
    
    moreButton = [[UIButton alloc] init];
    moreButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    [moreButton setTitleColor:FWColor(@"9E9E9E") forState:UIControlStateNormal];
    [moreButton setTitle:@"查看更多" forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:moreButton];
    [moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.shareButton);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-5);
        make.size.mas_equalTo(CGSizeMake(60, 30));
        make.top.mas_equalTo(shareButton.mas_bottom).mas_offset(10);
    }];
    
    
    lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
    }];
}

- (void)setupLeftSubViews{
    
    leftImageView = [[UIImageView alloc] init];
    leftImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    leftImageView.layer.cornerRadius = 15;
    leftImageView.layer.masksToBounds = YES;
    leftImageView.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
    leftImageView.layer.borderWidth = 0.5;
    leftImageView.contentMode = UIViewContentModeScaleAspectFill;
    leftImageView.userInteractionEnabled = YES;
    [self.contentView addSubview:leftImageView];
    [leftImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(15);
        make.top.mas_equalTo(self.contentView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    [leftImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftImageViewClick)]];

    
    leftWinnerImageView = [[UIImageView alloc] init];
    leftWinnerImageView.image = FWImage(@"real_rank");
    [self.contentView addSubview:leftWinnerImageView];
    [leftWinnerImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(leftImageView);
        make.bottom.mas_equalTo(leftImageView.mas_top).mas_offset(12/2-3);
        make.size.mas_equalTo(CGSizeMake(18, 12));
    }];
    leftWinnerImageView.hidden = YES;
    
    leftNoLabel = [[UILabel alloc] init];
    leftNoLabel.font = DHSystemFontOfSize_14;
    leftNoLabel.textColor = FWTextColor_000000;
    leftNoLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftNoLabel];
    [leftNoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftImageView.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(leftImageView);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(30);
    }];

    leftNameLabel = [[UILabel alloc] init];
    leftNameLabel.font = DHSystemFontOfSize_14;
    leftNameLabel.textColor = FWTextColor_000000;
    leftNameLabel.textAlignment = NSTextAlignmentLeft;
    [leftNameLabel sizeToFit];
    [self.contentView addSubview:leftNameLabel];
    [leftNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftNoLabel.mas_right).mas_offset(5);
        make.top.mas_equalTo(leftNoLabel);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(VSImageView.mas_left).mas_offset(-5);
    }];

    
    leftPointView = [[UIImageView alloc] init];
    leftPointView.backgroundColor = FWTextColor_D9E2E9;
    leftPointView.layer.cornerRadius = 2;
    leftPointView.layer.masksToBounds = YES;
    [self.contentView addSubview:leftPointView];
    [leftPointView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftNoLabel);
        make.top.mas_equalTo(leftNoLabel.mas_bottom).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(4, 4));
    }];
    
    leftCarLabel = [[UILabel alloc] init];
    leftCarLabel.font = DHSystemFontOfSize_10;
    leftCarLabel.textColor = FWTextColor_646464;
    leftCarLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftCarLabel];
    [leftCarLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftPointView.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(leftPointView);
        make.height.mas_equalTo(15);
        make.right.mas_equalTo(VSImageView.mas_left).mas_offset(-5);
    }];
    
    
    leftFoulLabel = [[UILabel alloc] init];
    leftFoulLabel.font = DHSystemFontOfSize_14;
    leftFoulLabel.textColor = FWOrange;
    leftFoulLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftFoulLabel];
    [leftFoulLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftImageView).mas_offset(0);
        make.top.mas_equalTo(leftImageView.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(55);
        make.width.mas_equalTo((SCREEN_WIDTH/2-20-15-15)/3);
//        make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
    }];
    leftFoulLabel.hidden = YES;
    
    leftETLabel = [[UILabel alloc] init];
    leftETLabel.text = @"ET";
    leftETLabel.font = DHSystemFontOfSize_10;
    leftETLabel.textColor = FWOrange;
    leftETLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftETLabel];
    [leftETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftImageView).mas_offset(0);
        make.top.mas_equalTo(leftImageView.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo((SCREEN_WIDTH/2-20-15-15)/3);
    }];

    
    leftETNumberLabel = [[UILabel alloc] init];
    leftETNumberLabel.font = DHSystemFontOfSize_10;
    leftETNumberLabel.textColor = FWOrange;
    leftETNumberLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftETNumberLabel];
    [leftETNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftETLabel).mas_offset(0);
        make.top.mas_equalTo(leftETLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(leftETLabel);
//        make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
    }];
    
    leftWeisuLabel = [[UILabel alloc] init];
    leftWeisuLabel.text = @"尾速";
    leftWeisuLabel.font = DHSystemFontOfSize_10;
    leftWeisuLabel.textColor = FWTextColor_9C9C9C;
    leftWeisuLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftWeisuLabel];
    [leftWeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftETLabel.mas_right).mas_offset(5);
        make.top.mas_equalTo(leftETLabel);
        make.height.mas_equalTo(leftETLabel);
        make.width.mas_equalTo(leftETLabel);
    }];
    
    leftWeisuNumberLabel = [[UILabel alloc] init];
    leftWeisuNumberLabel.font = DHSystemFontOfSize_10;
    leftWeisuNumberLabel.textColor = FWTextColor_646464;
    leftWeisuNumberLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftWeisuNumberLabel];
    [leftWeisuNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftWeisuLabel).mas_offset(0);
        make.top.mas_equalTo(leftWeisuLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(leftWeisuLabel);
        make.width.mas_equalTo(leftWeisuLabel);
    }];
    
    leftRTLabel = [[UILabel alloc] init];
    leftRTLabel.text = @"RT";
    leftRTLabel.font = DHSystemFontOfSize_10;
    leftRTLabel.textColor = FWTextColor_9C9C9C;
    leftRTLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftRTLabel];
    [leftRTLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftWeisuLabel.mas_right).mas_offset(5);
        make.top.mas_equalTo(leftWeisuLabel);
        make.height.mas_equalTo(leftWeisuLabel);
        make.width.mas_equalTo(leftWeisuLabel);
    }];
    
    leftRTNumberLabel = [[UILabel alloc] init];
    leftRTNumberLabel.font = DHSystemFontOfSize_10;
    leftRTNumberLabel.textColor = FWTextColor_646464;
    leftRTNumberLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:leftRTNumberLabel];
    [leftRTNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftRTLabel).mas_offset(0);
        make.top.mas_equalTo(leftRTLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(leftRTLabel);
        make.width.mas_equalTo(leftRTLabel);
    }];
}

- (void)setupRightSubViews{
    
    rightImageView = [[UIImageView alloc] init];
    rightImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    rightImageView.layer.cornerRadius = 15;
    rightImageView.layer.masksToBounds = YES;
    rightImageView.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
    rightImageView.contentMode = UIViewContentModeScaleAspectFill;
    rightImageView.layer.borderWidth = 0.5;
    rightImageView.userInteractionEnabled = YES;
    [self.contentView addSubview:rightImageView];
    [rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.top.mas_equalTo(leftImageView);
        make.size.mas_equalTo(leftImageView);
    }];
    [rightImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightImageViewClick)]];

    
    rightWinnerImageView = [[UIImageView alloc] init];
    rightWinnerImageView.image = FWImage(@"real_rank");
    [self.contentView addSubview:rightWinnerImageView];
    [rightWinnerImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(rightImageView);
        make.bottom.mas_equalTo(rightImageView.mas_top).mas_offset(15/2);
        make.size.mas_equalTo(CGSizeMake(20, 15));
    }];
    rightWinnerImageView.hidden = YES;
    
    rightNoLabel = [[UILabel alloc] init];
    rightNoLabel.font = DHSystemFontOfSize_14;
    rightNoLabel.textColor = FWTextColor_000000;
    rightNoLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightNoLabel];
    [rightNoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_greaterThanOrEqualTo(VSImageView.mas_right).mas_offset(3);
        make.top.mas_equalTo(rightImageView);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(30);
    }];
    
    
    rightNameLabel = [[UILabel alloc] init];
    rightNameLabel.font = DHSystemFontOfSize_14;
    rightNameLabel.textColor = FWTextColor_000000;
    rightNameLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightNameLabel];
    [rightNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightImageView.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(rightNoLabel);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(rightNoLabel.mas_right).mas_offset(5);
    }];
    [rightNoLabel setContentHuggingPriority:UILayoutPriorityDefaultLow
                              forAxis:UILayoutConstraintAxisHorizontal];
    [rightNameLabel setContentHuggingPriority:UILayoutPriorityRequired
                              forAxis:UILayoutConstraintAxisHorizontal];
    
    
    rightCarLabel = [[UILabel alloc] init];
    rightCarLabel.font = DHSystemFontOfSize_10;
    rightCarLabel.textColor = FWTextColor_646464;
    rightCarLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightCarLabel];
    [rightCarLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(rightPointView.mas_right).mas_offset(5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(leftPointView);
        make.height.mas_equalTo(15);
        make.right.mas_equalTo(rightImageView.mas_left).mas_offset(-5);
    }];
    
    rightPointView = [[UIImageView alloc] init];
    rightPointView.backgroundColor = FWTextColor_D9E2E9;
    rightPointView.layer.cornerRadius = 2;
    rightPointView.layer.masksToBounds = YES;
    [self.contentView addSubview:rightPointView];
    [rightPointView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightCarLabel.mas_left).mas_offset(-10);
        make.top.mas_equalTo(leftPointView);
        make.size.mas_equalTo(leftPointView);
    }];
    
    rightFoulLabel = [[UILabel alloc] init];
    rightFoulLabel.font = DHSystemFontOfSize_14;
    rightFoulLabel.textColor = FWOrange;
    rightFoulLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightFoulLabel];
    [rightFoulLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightImageView).mas_offset(0);
        make.top.mas_equalTo(leftFoulLabel);
        make.height.mas_equalTo(55);
        make.width.mas_equalTo((SCREEN_WIDTH/2-20-15-15)/3);
//        make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
    }];
    rightFoulLabel.hidden = YES;
    
    rightETLabel = [[UILabel alloc] init];
    rightETLabel.text = @"ET";
    rightETLabel.font = DHSystemFontOfSize_10;
    rightETLabel.textColor = FWOrange;
    rightETLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightETLabel];
    [rightETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightImageView).mas_offset(0);
        make.top.mas_equalTo(leftETLabel);
        make.height.mas_equalTo(leftETLabel);
        make.width.mas_equalTo(leftETLabel);
    }];
    
    rightETNumberLabel = [[UILabel alloc] init];
    rightETNumberLabel.font = DHSystemFontOfSize_10;
    rightETNumberLabel.textColor = FWOrange;
    rightETNumberLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightETNumberLabel];
    [rightETNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightETLabel).mas_offset(0);
        make.top.mas_equalTo(rightETLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(rightETLabel);
        make.width.mas_equalTo(rightETLabel);
    }];
    
    rightWeisuLabel = [[UILabel alloc] init];
    rightWeisuLabel.text = @"尾速";
    rightWeisuLabel.font = DHSystemFontOfSize_10;
    rightWeisuLabel.textColor = FWTextColor_9C9C9C;
    rightWeisuLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightWeisuLabel];
    [rightWeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightETLabel.mas_left).mas_offset(-5);
        make.top.mas_equalTo(leftETLabel);
        make.height.mas_equalTo(leftETLabel);
        make.width.mas_equalTo(leftETLabel);
    }];
    
    rightWeisuNumberLabel = [[UILabel alloc] init];
    rightWeisuNumberLabel.font = DHSystemFontOfSize_10;
    rightWeisuNumberLabel.textColor = FWTextColor_646464;
    rightWeisuNumberLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightWeisuNumberLabel];
    [rightWeisuNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightWeisuLabel).mas_offset(0);
        make.top.mas_equalTo(rightWeisuLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(leftWeisuLabel);
        make.width.mas_equalTo(leftWeisuLabel);
    }];
    
    rightRTLabel = [[UILabel alloc] init];
    rightRTLabel.text = @"RT";
    rightRTLabel.font = DHSystemFontOfSize_10;
    rightRTLabel.textColor = FWTextColor_9C9C9C;
    rightRTLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightRTLabel];
    [rightRTLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightWeisuLabel.mas_left).mas_offset(-5);
        make.top.mas_equalTo(rightWeisuLabel);
        make.height.mas_equalTo(leftETLabel);
        make.width.mas_equalTo(leftETLabel);
    }];
    
    rightRTNumberLabel = [[UILabel alloc] init];
    rightRTNumberLabel.font = DHSystemFontOfSize_10;
    rightRTNumberLabel.textColor = FWTextColor_646464;
    rightRTNumberLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightRTNumberLabel];
    [rightRTNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightRTLabel).mas_offset(0);
        make.top.mas_equalTo(rightRTLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(rightRTLabel);
        make.width.mas_equalTo(rightRTLabel);
    }];
}

#pragma mark - > 赋值
- (void)configForModel:(id)model{
    
    leftNameLabel.text = @"暂无选手";
    leftCarLabel.text = @"暂无车辆";
    leftETNumberLabel.text = @"--";
    leftWeisuNumberLabel.text = @"--";
    leftRTNumberLabel.text = @"--";
    leftNoLabel.text = @"";
    
    rightNameLabel.text = @"暂无选手";
    rightCarLabel.text = @"暂无车辆";
    rightETNumberLabel.text = @"--";
    rightWeisuNumberLabel.text = @"--";
    rightRTNumberLabel.text = @"--";
    rightNoLabel.text = @"";
    
    matchModel = (FWScoreListModel *)model;
    
    /* 赢的人显示小皇冠 */
//    leftWinnerImageView.hidden = YES;
//    rightWinnerImageView.hidden = YES;
//    if ([matchModel.winner_car_no isEqualToString:matchModel.car_1_info.car_no]) {
//        leftWinnerImageView.hidden = NO;
//        rightWinnerImageView.hidden = YES;
//    }else if ([matchModel.winner_car_no isEqualToString:matchModel.car_2_info.car_no]){
//        leftWinnerImageView.hidden = YES;
//        rightWinnerImageView.hidden = NO;
//    }
    
    if (matchModel.car_1_info.player_name.length > 0) {
        leftNameLabel.text = matchModel.car_1_info.player_name;
    }
    
    if (matchModel.car_2_info.player_name.length > 0) {
        rightNameLabel.text = matchModel.car_2_info.player_name;
    }

    if (matchModel.car_1_info.car_no.length > 0) {
        leftNoLabel.text = matchModel.car_1_info.car_no;
    }
    
    if (matchModel.car_2_info.car_no.length > 0) {
        rightNoLabel.text = matchModel.car_2_info.car_no;
    }
    
    if (matchModel.car_1_info.car_type.length > 0) {
        leftCarLabel.text = matchModel.car_1_info.car_type;
    }
    
    if (matchModel.car_2_info.car_type.length > 0) {
        rightCarLabel.text = matchModel.car_2_info.car_type;
    }
    
    if ([matchModel.car_1_info.match_status isEqualToString:@"2"]) {
        leftFoulLabel.hidden = NO;

        leftETLabel.hidden = YES;
        leftETNumberLabel.hidden = YES;
        
        leftFoulLabel.text = matchModel.car_1_info.match_text;
    }else{
        leftFoulLabel.hidden = YES;
        
        leftETLabel.hidden = NO;
        leftETNumberLabel.hidden = NO;
    }
    
    if ([matchModel.car_2_info.match_status isEqualToString:@"2"]) {
        rightFoulLabel.hidden = NO;
        
        rightETLabel.hidden = YES;
        rightETNumberLabel.hidden = YES;
        
        rightFoulLabel.text = matchModel.car_2_info.match_text;
    }else{
        rightFoulLabel.hidden = YES;
        
        rightETLabel.hidden = NO;
        rightETNumberLabel.hidden = NO;
    }
    
    [leftImageView sd_setImageWithURL:[NSURL URLWithString:matchModel.car_1_info.header] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    [rightImageView sd_setImageWithURL:[NSURL URLWithString:matchModel.car_2_info.header] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];

    if([matchModel.phase_code isEqualToString:@"F"]){
        // 决赛
        
        NSString * leftET = matchModel.car_1_info.elapsed_time?matchModel.car_1_info.elapsed_time:@"--";
        NSString * leftRT = matchModel.car_1_info.reaction_time?matchModel.car_1_info.reaction_time:@"--";

        NSString * rightET = matchModel.car_2_info.reaction_time?matchModel.car_2_info.elapsed_time:@"--";
        NSString * rightRT = matchModel.car_2_info.reaction_time?matchModel.car_2_info.reaction_time:@"--";

        NSString * leftETPlusRT = [NSString stringWithFormat:@"%@+%@",leftET,leftRT];
        if ([leftET isEqualToString:@"--"] ||
            [leftET isEqualToString:@""]) {
            leftETPlusRT = @"--";
        }
        
        NSString * rightETPlusRT = [NSString stringWithFormat:@"%@+%@",rightET,rightRT];
        if ([rightET isEqualToString:@"--"]||
            [rightET isEqualToString:@""]) {
            rightETPlusRT = @"--";
        }
        
        leftETNumberLabel.text = leftETPlusRT;
        rightETNumberLabel.text = rightETPlusRT;
        
        
        if (matchModel.car_1_info.vehicle_speed.length > 0) {
            leftWeisuNumberLabel.text = matchModel.car_1_info.vehicle_speed;
        }
 
        if (matchModel.car_2_info.vehicle_speed.length > 0) {
            rightWeisuNumberLabel.text = matchModel.car_2_info.vehicle_speed;
        }
        
        [self showETPlusRT];
    }else{
        // 预赛
        if (matchModel.car_1_info.elapsed_time.length > 0) {
            leftETNumberLabel.text = matchModel.car_1_info.elapsed_time;
        }

        if (matchModel.car_2_info.elapsed_time.length > 0) {
            rightETNumberLabel.text = matchModel.car_2_info.elapsed_time;
        }
        
        if (matchModel.car_1_info.vehicle_speed.length > 0) {
            leftWeisuNumberLabel.text = matchModel.car_1_info.vehicle_speed;
        }
        
        if (matchModel.car_2_info.vehicle_speed.length > 0) {
            rightWeisuNumberLabel.text = matchModel.car_2_info.vehicle_speed;
        }
        
        if (matchModel.car_1_info.reaction_time.length > 0) {
            leftRTNumberLabel.text = matchModel.car_1_info.reaction_time;
        }
        
        
        if (matchModel.car_2_info.reaction_time.length > 0) {
            rightRTNumberLabel.text = matchModel.car_2_info.reaction_time;
        }
        
        [self showNormalETAndRt];
    }
}

#pragma mark - > 分享
- (void)shareButtonOnClick{
    
    NSString * type = @"2";
    if (nil == self.matchModel.car_1_info ||
        nil == self.matchModel.car_2_info ||
        [@"" isEqualToString:self.matchModel.car_1_info.player_name]||
        [@"" isEqualToString:self.matchModel.car_2_info.player_name]) {
        type = @"1";
    }
    
    FWNewShareScoreViewController * vc = [[FWNewShareScoreViewController alloc] init];
    vc.shareType = type;
    vc.listModel = self.matchModel;
    vc.scoreModel = self.scoreModel;
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.viewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.vc presentViewController:vc animated:YES completion:nil];
}


#pragma mark - > 决赛展示ET+RT
- (void)showETPlusRT{
    
    leftETLabel.text = @"ET+RT";
    rightETLabel.text = @"ET+RT";
    
    leftRTLabel.hidden = YES;
    leftRTNumberLabel.hidden = YES;
    rightRTLabel.hidden = YES;
    rightRTNumberLabel.hidden = YES;
    
    CGFloat labelWidth = FWAdaptive(170);

    if ([matchModel.car_1_info.match_status isEqualToString:@"2"]) {
        
        [leftFoulLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftImageView).mas_offset(0);
            make.top.mas_equalTo(leftImageView.mas_bottom).mas_offset(20);
            make.height.mas_equalTo(55);
            make.width.mas_equalTo((SCREEN_WIDTH/2-20-15-15)/3);
//            make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
        }];
    }else{
        
        [leftETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftImageView).mas_offset(0);
            make.top.mas_equalTo(leftImageView.mas_bottom).mas_offset(20);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(labelWidth);
        }];
        
        [leftETNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftETLabel).mas_offset(0);
            make.top.mas_equalTo(leftETLabel.mas_bottom).mas_offset(5);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(labelWidth);
//            make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
        }];
    }
    
    if ([matchModel.car_2_info.match_status isEqualToString:@"2"]) {

        [rightFoulLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(rightImageView).mas_offset(0);
            make.top.mas_equalTo(leftFoulLabel);
            make.height.mas_equalTo(55);
            make.width.mas_equalTo((SCREEN_WIDTH/2-20-15-15)/3);
//            make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
        }];
    }else{
        
        [rightETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(rightImageView).mas_offset(0);
            make.top.mas_equalTo(leftETLabel);
            make.height.mas_equalTo(leftETLabel);
            make.width.mas_equalTo(labelWidth);
        }];
        
        [rightETNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(rightETLabel).mas_offset(0);
            make.top.mas_equalTo(rightETLabel.mas_bottom).mas_offset(5);
            make.height.mas_equalTo(rightETLabel);
            make.width.mas_equalTo(labelWidth);
        }];
    }
    
    [leftWeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftImageView).mas_offset(labelWidth+5);
        make.right.mas_equalTo(shareButton.mas_left).mas_offset(-5);
        make.top.mas_equalTo(leftImageView.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(15);
    }];
    
    [leftWeisuNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftWeisuLabel).mas_offset(0);
        make.top.mas_equalTo(leftWeisuLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(leftWeisuLabel);
        make.width.mas_equalTo(leftWeisuLabel);
    }];
    
    
    [rightWeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightImageView).mas_offset(-5-labelWidth);
        make.left.mas_equalTo(shareButton.mas_right).mas_offset(5);
        make.top.mas_equalTo(leftWeisuLabel);
        make.height.mas_equalTo(leftWeisuNumberLabel);
    }];
    
    [rightWeisuNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightWeisuLabel).mas_offset(0);
        make.top.mas_equalTo(rightWeisuLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(leftWeisuLabel);
        make.width.mas_equalTo(leftWeisuLabel);
    }];
}

#pragma mark - > 预赛展示ET & RT
- (void)showNormalETAndRt{
    
    leftETLabel.text = @"ET";
    rightETLabel.text = @"ET";
    
    leftRTLabel.hidden = NO;
    leftRTNumberLabel.hidden = NO;
    rightRTLabel.hidden = NO;
    rightRTNumberLabel.hidden = NO;
    
    CGFloat labelWidth = (SCREEN_WIDTH/2-20-15-15)/3;
    
    if ([matchModel.car_1_info.match_status isEqualToString:@"2"]) {
        
        [leftFoulLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftImageView).mas_offset(0);
            make.top.mas_equalTo(leftImageView.mas_bottom).mas_offset(20);
            make.height.mas_equalTo(55);
            make.width.mas_equalTo(labelWidth);
//            make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
        }];
    }else{
        [leftETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftImageView).mas_offset(0);
            make.top.mas_equalTo(leftImageView.mas_bottom).mas_offset(20);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(labelWidth);
        }];
        
        [leftETNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftETLabel).mas_offset(0);
            make.top.mas_equalTo(leftETLabel.mas_bottom).mas_offset(5);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(leftETLabel);
//            make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
        }];
    }
    
    if ([matchModel.car_2_info.match_status isEqualToString:@"2"]) {
        
        [rightFoulLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(rightImageView).mas_offset(0);
            make.top.mas_equalTo(leftFoulLabel);
            make.height.mas_equalTo(55);
            make.width.mas_equalTo(labelWidth);
//            make.bottom.mas_equalTo(self.contentView).mas_offset(-15);
        }];
    }else{
        
        [rightETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(rightImageView).mas_offset(0);
            make.top.mas_equalTo(leftETLabel);
            make.height.mas_equalTo(leftETLabel);
            make.width.mas_equalTo(leftETLabel);
        }];
        
        [rightETNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(rightETLabel).mas_offset(0);
            make.top.mas_equalTo(rightETLabel.mas_bottom).mas_offset(5);
            make.height.mas_equalTo(rightETLabel);
            make.width.mas_equalTo(rightETLabel);
        }];
    }
    
    [leftWeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftImageView).mas_offset(labelWidth+5);
        make.top.mas_equalTo(leftImageView.mas_bottom).mas_offset(20);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(labelWidth);
    }];
    
    [leftWeisuNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftWeisuLabel).mas_offset(0);
        make.top.mas_equalTo(leftWeisuLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(leftWeisuLabel);
        make.width.mas_equalTo(leftWeisuLabel);
    }];
    
    
    [rightWeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightImageView).mas_offset(-labelWidth -5);
        make.top.mas_equalTo(leftWeisuLabel);
        make.height.mas_equalTo(leftWeisuLabel);
        make.width.mas_equalTo(leftWeisuLabel);
    }];
    
    [rightWeisuNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(rightWeisuLabel).mas_offset(0);
        make.top.mas_equalTo(rightWeisuLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(leftWeisuLabel);
        make.width.mas_equalTo(leftWeisuLabel);
    }];
}

- (void)leftImageViewClick{
    
    if (![matchModel.car_1_info.uid isEqualToString:@"0"]) {
        FWNewUserInfoViewController * UVC = [[FWNewUserInfoViewController alloc] init];
        UVC.user_id = matchModel.car_1_info.uid;
        [self.vc.navigationController pushViewController:UVC animated:YES];
    }
}
- (void)rightImageViewClick{
   
    if (![matchModel.car_2_info.uid isEqualToString:@"0"]) {
        FWNewUserInfoViewController * UVC = [[FWNewUserInfoViewController alloc] init];
        UVC.user_id = matchModel.car_2_info.uid;
        [self.vc.navigationController pushViewController:UVC animated:YES];
    }
}

#pragma mark - > 查看更多
- (void)moreButtonOnClick{
 
    if ([self.delegate respondsToSelector:@selector(moreButtonClick)]) {
        [self.delegate moreButtonClick];
    }
}

- (void)allViewIsHidden:(BOOL)isHidden{

//    leftWinnerImageView.hidden = isHidden;
//    rightWinnerImageView.hidden = isHidden;
    leftImageView.hidden = isHidden;
    rightImageView.hidden = isHidden;
    VSImageView.hidden = isHidden;
    shareButton.hidden = isHidden;
    lineView.hidden = isHidden;
    leftPointView.hidden = isHidden;
    rightPointView.hidden = isHidden;
    
    if (isHidden) {
        leftNameLabel.text = @"";
        rightNameLabel.text = @"";
        leftNoLabel.text = @"";
        rightNoLabel.text = @"";
        leftCarLabel.text = @"";
        rightCarLabel.text = @"";
        leftFoulLabel.text = @"";
        rightFoulLabel.text = @"";
        leftETLabel.text = @"";
        rightETLabel.text = @"";
        leftETNumberLabel.text = @"";
        rightETNumberLabel.text = @"";
        leftWeisuLabel.text = @"";
        rightWeisuLabel.text = @"";
        leftWeisuNumberLabel.text = @"";
        rightWeisuNumberLabel.text = @"";
        leftRTLabel.text = @"";
        rightRTLabel.text = @"";
        leftRTNumberLabel.text = @"";
        rightRTNumberLabel.text = @"";
    }else{
        /* 赢的人显示小皇冠 */
//        leftWinnerImageView.hidden = YES;
//        rightWinnerImageView.hidden = YES;
//        if ([matchModel.winner_car_no isEqualToString:matchModel.car_1_info.car_no]) {
//            leftWinnerImageView.hidden = NO;
//            rightWinnerImageView.hidden = YES;
//        }else if ([matchModel.winner_car_no isEqualToString:matchModel.car_2_info.car_no]){
//            leftWinnerImageView.hidden = YES;
//            rightWinnerImageView.hidden = NO;
//        }
        
        if([matchModel.phase_code isEqualToString:@"F"]){
            /* 决赛 */
            leftETLabel.text = @"ET+RT";
            rightETLabel.text = @"ET+RT";
        }else{
            /* 预赛 */
            leftETLabel.text = @"ET";
            rightETLabel.text = @"ET";

            leftRTLabel.text = @"RT";
            rightRTLabel.text = @"RT";
        }
        leftWeisuLabel.text = @"尾速";
        rightWeisuLabel.text = @"尾速";
    }
}
@end
