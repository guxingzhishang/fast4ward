//
//  FWLiveTipView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/7.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWLiveTipViewDelegate <NSObject>

- (void)continueLiveMatch;

@end

@interface FWLiveTipView : UIView

@property (nonatomic, strong) UIImageView * bgImageView;

@property (nonatomic, strong) UILabel * tipLabel;

@property (nonatomic, strong) UIButton * continueButton;

@property (nonatomic, weak) id<FWLiveTipViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
