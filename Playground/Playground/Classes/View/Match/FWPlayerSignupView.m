//
//  FWPlayerSignupView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerSignupView.h"

@implementation FWPlayerSignupView

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWTextColor_F6F8FA;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
 
    self.mainView = [[UIView alloc] init];
    [self addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    self.topView = [[UIView alloc] init];
    self.topView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.topView];
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.mainView);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(200);
    }];
    
    self.picImageView = [[UIImageView alloc] init];
    self.picImageView.image = [UIImage imageNamed:@"player_signup"];
    [self.topView addSubview:self.picImageView];
    [self.picImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.topView);
        make.size.mas_equalTo(CGSizeMake(101, 127));
        make.top.mas_equalTo(self.topView).mas_offset(8);
    }];
    
    self.resultLabel = [[UILabel alloc] init];
    self.resultLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19.9];
    self.resultLabel.textColor = FWColor(@"030303");
    self.resultLabel.textAlignment = NSTextAlignmentCenter;
    [self.topView addSubview:self.resultLabel];
    [self.resultLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 25));
        make.top.mas_equalTo(self.picImageView.mas_bottom).mas_offset(3);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.numberOfLines = 0;
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.tipLabel.textColor = FWTextColor_9EA3AB;
    self.tipLabel.textAlignment = NSTextAlignmentCenter;
    [self.topView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topView).mas_offset(50);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-100, 80));
        make.top.mas_equalTo(self.resultLabel.mas_bottom).mas_offset(25);
        make.bottom.mas_equalTo(self.topView).mas_offset(-24);
    }];
    
    self.middleView = [[UIView alloc] init];
    self.middleView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.middleView];
    [self.middleView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.top.mas_equalTo(self.topView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(119);
    }];
    
    self.confirmLabel = [[UILabel alloc] init];
    self.confirmLabel.text = @"确认信息";
    self.confirmLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.confirmLabel.textColor = FWTextColor_272727;
    self.confirmLabel.textAlignment = NSTextAlignmentLeft;
    [self.middleView addSubview:self.confirmLabel];
    [self.confirmLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.middleView).mas_offset(13);
        make.size.mas_equalTo(CGSizeMake(100, 25));
        make.top.mas_equalTo(self.middleView).mas_offset(23);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.middleView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.confirmLabel);
        make.right.mas_equalTo(self.middleView).mas_offset(-3);
        make.width.mas_equalTo(SCREEN_WIDTH-26);
        make.height.mas_greaterThanOrEqualTo (20);
        make.top.mas_equalTo(self.confirmLabel.mas_bottom).mas_offset(10);
    }];
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.timeLabel.textColor = FWTextColor_000000;
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.middleView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.middleView).mas_offset(-3);
        make.width.mas_equalTo(SCREEN_WIDTH-26);
        make.height.mas_greaterThanOrEqualTo (20);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(3);
    }];
    
    self.typeLabel = [[UILabel alloc] init];
    self.typeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.typeLabel.textColor = FWTextColor_000000;
    self.typeLabel.textAlignment = NSTextAlignmentLeft;
    [self.middleView addSubview:self.typeLabel];
    [self.typeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.middleView).mas_offset(-3);
        make.width.mas_equalTo(SCREEN_WIDTH-26);
        make.height.mas_greaterThanOrEqualTo (20);
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(3);
    }];
    
    self.sumLabel = [[UILabel alloc] init];
    self.sumLabel.text = @"总金额：";
    self.sumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.sumLabel.textColor = FWTextColor_000000;
    self.sumLabel.textAlignment = NSTextAlignmentLeft;
    [self.middleView addSubview:self.sumLabel];
    [self.sumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.size.mas_equalTo(CGSizeMake(60, 20));
        make.top.mas_equalTo(self.typeLabel.mas_bottom).mas_offset(3);
        make.bottom.mas_equalTo(self.middleView).mas_offset(-16);
    }];
    
    self.sumNumLabel = [[UILabel alloc] init];
    self.sumNumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.sumNumLabel.textColor = DHRedColorff6f00;
    self.sumNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.middleView addSubview:self.sumNumLabel];
    [self.sumNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.sumLabel.mas_right).mas_offset(0);
        make.right.mas_equalTo(self.middleView).mas_offset(-14);
        make.height.mas_equalTo(self.sumLabel);
        make.centerY.mas_equalTo(self.sumLabel);
    }];
    

    self.thirdView = [[UIView alloc] init];
    self.thirdView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.thirdView];
    [self.thirdView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 42));
        make.top.mas_equalTo(self.middleView.mas_bottom).mas_offset(10);
    }];
    
    self.selectButton = [[UIButton alloc] init];
    [self.selectButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.selectButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    [self.selectButton addTarget:self action:@selector(selectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.thirdView addSubview:self.selectButton];
    [self.selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.thirdView);
        make.left.mas_equalTo(self.thirdView).mas_offset(9);
        make.size.mas_equalTo(CGSizeMake(31, 42));
    }];
    
    self.taocanLabel = [[UILabel alloc] init];
    self.taocanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.taocanLabel.textColor = FWTextColor_222222;
    self.taocanLabel.textAlignment = NSTextAlignmentLeft;
    [self.thirdView addSubview:self.taocanLabel];
    [self.taocanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(self.thirdView);
        make.right.mas_equalTo(self.thirdView).mas_offset(-20);
        make.left.mas_equalTo(self.selectButton.mas_right);
    }];
    
    self.thirdRightView = [[UIImageView alloc] init];
    self.thirdRightView.image = [UIImage imageNamed:@"right_arrow"];
    [self.thirdView addSubview:self.thirdRightView];
    [self.thirdRightView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.thirdView);
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.right.mas_equalTo(self.thirdView).mas_offset(-10);
    }];
    
    self.bottomView = [[UIView alloc] init];
    self.bottomView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.bottomView];
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.top.mas_equalTo(self.thirdView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(97);
    }];
    
    self.confirmLabel = [[UILabel alloc] init];
    self.confirmLabel.text = @"请选择支付方式";
    self.confirmLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.confirmLabel.textColor = FWTextColor_12101D;
    self.confirmLabel.textAlignment = NSTextAlignmentLeft;
    [self.bottomView addSubview:self.confirmLabel];
    [self.confirmLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bottomView).mas_offset(13);
        make.size.mas_equalTo(CGSizeMake(150, 25));
        make.top.mas_equalTo(self.bottomView).mas_offset(18);
    }];
    
    NSArray * titleArray = @[@"微信支付"];
    NSArray * iconArray = @[@"wechat_pay"];
    
    for (int i = 0; i < titleArray.count; i++) {
        
        UIButton * payMethodButton = [[UIButton alloc] init];
        payMethodButton.tag = 1000+i;
        [payMethodButton addTarget:self action:@selector(payMethodButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:payMethodButton];
        [payMethodButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.bottomView).mas_offset(0);
            make.top.mas_equalTo(self.confirmLabel.mas_bottom).mas_offset(5);
            make.height.mas_equalTo(53);
            make.width.mas_equalTo(SCREEN_WIDTH);
            
            if (i == 1) {
                make.bottom.mas_equalTo(self.bottomView).mas_offset(0);
            }
        }];
        
        UIImageView * icon = [UIImageView new];
        icon.image = [UIImage imageNamed:iconArray[i]];
        [payMethodButton addSubview:icon];
        [icon mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payMethodButton);
            make.size.mas_equalTo(CGSizeMake(19, 19));
            make.left.mas_equalTo(payMethodButton).mas_offset(20);
        }];
        
        UILabel * payTitleLabel = [UILabel new];
        payTitleLabel.text = titleArray[i];
        payTitleLabel.font = DHSystemFontOfSize_15;
        payTitleLabel.textColor = FWTextColor_12101D;
        payTitleLabel.textAlignment = NSTextAlignmentLeft;
        [payMethodButton addSubview:payTitleLabel];
        [payTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payMethodButton);
            make.size.mas_equalTo(CGSizeMake(200, 25));
            make.left.mas_equalTo(icon.mas_right).mas_offset(5);
        }];
        
        UIImageView * selectImageView = [UIImageView new];
        selectImageView.tag = payMethodButton.tag +1000;
        [payMethodButton addSubview:selectImageView];
        [selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payMethodButton);
            make.size.mas_equalTo(CGSizeMake(19, 19));
            make.right.mas_equalTo(payMethodButton).mas_offset(-20);
        }];
        
        if (i == 0) {
            selectImageView.image = [UIImage imageNamed:@"radio_save_sel"];
        }else{
            selectImageView.image = [UIImage imageNamed:@"radio_save_unsel"];
        }
    }
    
    
    self.payButton = [[UIButton alloc] init];
    self.payButton.layer.cornerRadius = 2;
    self.payButton.layer.masksToBounds = YES;
    self.payButton.backgroundColor = FWTextColor_222222;
    self.payButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    [self.payButton setTitle:@"立即支付" forState:UIControlStateNormal];
    [self.payButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.payButton addTarget:self action:@selector(payButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.payButton];
    [self.payButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bottomView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.mainView).mas_offset(30);
        make.right.mas_equalTo(self.mainView).mas_offset(-30);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(SCREEN_WIDTH-60);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-10);
    }];
}


- (void)configForView:(FWPlayerSignupModel *)model{
    
    self.signupModel = model;
    
    self.resultLabel.text = model.pay_info.title;
    self.tipLabel.text = model.pay_info.desc;
    self.titleLabel.text = model.sport_name;
    self.sumNumLabel.text = [NSString stringWithFormat:@"%@",model.show_money];
    self.timeLabel.text = [NSString stringWithFormat:@"比赛时间：%@",model.sport_date];
    self.typeLabel.text = [NSString stringWithFormat:@"缴费类型：%@",model.show_fee_type];
    
    if([model.goods.goods_status_sale isEqualToString:@"1"]){
        [self.thirdView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goodsDetail)]];

        self.taocanLabel.text = model.goods.goods_title;
        self.thirdView.hidden = NO;

    }else{
        self.thirdView.hidden = YES;
        [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self);
            make.top.mas_equalTo(self.middleView.mas_bottom).mas_offset(10);
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.height.mas_greaterThanOrEqualTo(97);
        }];
    }
}

#pragma mark - > 商品详情
- (void)goodsDetail{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.signupModel.goods.goods_h5_url;
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

- (void)selectButtonClick{
    
    self.selectButton.selected = !self.selectButton.selected;
    
    if (self.selectButton.isSelected) {
        self.sumNumLabel.text = [NSString stringWithFormat:@"%@",self.signupModel.pay_info.show_money_total];
    }else{
        self.sumNumLabel.text = [NSString stringWithFormat:@"%@",self.signupModel.show_money];
    }
}

#pragma mark - > 支付方式
- (void)payMethodButtonClick:(UIButton *)sender{
    
}

#pragma mark - > 立即支付
- (void)payButtonOnClick{
    
    if ([self.signupDelegate respondsToSelector:@selector(payButtonClick:)]) {
        [self.signupDelegate payButtonClick:@"wechat"];
    }
}

@end
