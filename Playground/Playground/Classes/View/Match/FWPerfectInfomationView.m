//
//  FWPerfectInfomationView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPerfectInfomationView.h"
#import "UIBarButtonItem+Item.h"
#import "ALiOssUploadTool.h"
#import "FWADArearViewController.h"

static NSString * sizeType = @"sizeType";
static NSString * jinqiType = @"jinqiType";
static NSString * qudongType = @"qudongType";

@interface FWPerfectInfomationView ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) NSMutableArray * qudongArray;
@property (nonatomic, strong) NSMutableArray * sizeArray;
@property (nonatomic, strong) NSMutableArray * brandArray;
@property (nonatomic, strong) NSMutableArray * zubieArray;
@property (nonatomic, strong) NSMutableArray * jiyouArray;
@property (nonatomic, strong) NSMutableArray * luntaiArray;
@property (nonatomic, strong) NSMutableArray * jinqiArray;

@property (nonatomic, assign) NSInteger sizeIndex;
@property (nonatomic, assign) NSInteger jinqiIndex;
@property (nonatomic, assign) NSInteger qudongIndex;

@property (nonatomic, strong) NSString * brandID;
@property (nonatomic, strong) NSString * zubieID;
@property (nonatomic, strong) NSString * jiyouID;
@property (nonatomic, strong) NSString * luntaiID;

@property (nonatomic, strong) NSDictionary * dataDict;
@property (nonatomic, strong) NSDictionary * ad_plan_dataDict;

@property (nonatomic, strong) UIImagePickerController * picker;
@property (nonatomic, strong) FWPerfectInfoModel * arrayInfoModel;

@property (nonatomic, strong) NSString * idCard_img;
@property (nonatomic, strong) NSString * driver_img;
@property (nonatomic, strong) NSString * mianguan_img;
@property (nonatomic, strong) NSString * head_img;
@property (nonatomic, strong) NSString * foot_img;
@property (nonatomic, strong) NSString * body_img;

@property (nonatomic, strong) NSString * current_type;// 1: 身份证  2:驾照
@property (nonatomic, strong) NSString * ad_position_imgs;

@property (nonatomic, strong) NSMutableArray * adImages;

@end

@implementation FWPerfectInfomationView
@synthesize picker;

- (void)setCanEdited:(BOOL)canEdited{
    _canEdited = canEdited;
    
    self.wxNumTF.userInteractionEnabled = canEdited;
    self.brandTF.userInteractionEnabled = canEdited;
    self.carStyleTF.userInteractionEnabled = canEdited;
    self.clubTF.userInteractionEnabled = canEdited;
    self.frameNumTF.userInteractionEnabled = canEdited;
    self.maliDataTF.userInteractionEnabled = canEdited;
    self.pailiangTF.userInteractionEnabled = canEdited;
    self.luntaiTF.userInteractionEnabled = canEdited;
    self.jiyouTF.userInteractionEnabled = canEdited;
    self.gaizhuangSumTF.userInteractionEnabled = canEdited;
    self.gaizhuangTV.userInteractionEnabled = canEdited;
    self.zubieTF.userInteractionEnabled = canEdited;
    
    self.idcardImageView.userInteractionEnabled = canEdited;
    self.carCardImageView.userInteractionEnabled = canEdited;
    self.photographImageView.userInteractionEnabled = canEdited;

    self.jinqiSegement.userInteractionEnabled = canEdited;
    self.qudongSegement.userInteractionEnabled = canEdited;
    self.sizeSegement.userInteractionEnabled = canEdited;
    
    self.adPlanButton.userInteractionEnabled = canEdited;
    self.headImageView.userInteractionEnabled = canEdited;
    self.footImageView.userInteractionEnabled = canEdited;
    self.bodyImageView.userInteractionEnabled = canEdited;
    
    self.saveButton.hidden = !canEdited;
    
    self.zubieArrowImageView.hidden = !canEdited;
    self.brandArrowImageView.hidden = !canEdited;
    self.luntaiArrowImageView.hidden = !canEdited;
    self.jiyouArrowImageView.hidden = !canEdited;
    
    if (!canEdited) {
        self.idcardReUploadLabel.hidden = YES;
        self.carCardReUploadLabel.hidden = YES;
        self.photographReUploadLabel.hidden = YES;
        self.headReUploadLabel.hidden = YES;
        self.footReUploadLabel.hidden = YES;
        self.bodyReUploadLabel.hidden = YES;
        
        /* 查看 */
        self.adShadowBgView.hidden = YES;
        [self.announceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.zubieTF);
            make.width.mas_equalTo(self.zubieTF);
            make.height.mas_greaterThanOrEqualTo(10);
            make.top.mas_equalTo(self.zubieTF.mas_bottom).mas_offset(20);
            make.bottom.mas_equalTo(self.seventhView).mas_offset(-20);
        }];
    }
}
#pragma mark - > 请求阿里临时验证
- (void)requestStsData{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    [request requestStsTokenWithType:@"1"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestADPlanUploadName];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestUploadName];
    });
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"driver_quota",
                              @"number":@"3",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.dataDict = [back objectForKey:@"data"];
        }
    }];
}

#pragma mark - > 获取广告车照片文件名
- (void)requestADPlanUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"ad_position",
                              @"number":@"3",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.ad_plan_dataDict = [back objectForKey:@"data"];
            self.adImages = [[self.ad_plan_dataDict objectForKey:@"object_keys"] mutableCopy];
        }
    }];
}

- (id)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = FWTextColor_F6F8FA;

        self.dataDict = @{};
        self.originalDict = @{};
        self.ad_plan_dataDict = @{};
        
        [self requestStsData];
        
        
        self.adImages = @[].mutableCopy;
        self.qudongArray = @[].mutableCopy;
        self.sizeArray = @[].mutableCopy;
        self.brandArray = @[].mutableCopy;
        self.zubieArray = @[].mutableCopy;
        self.jiyouArray = @[].mutableCopy;
        self.luntaiArray = @[].mutableCopy;
        self.jinqiArray = @[].mutableCopy;

        self.sizeIndex = -1;
        self.jinqiIndex = -1;
        self.qudongIndex = -1;
        
        [self setupFirstSubviews];
        [self setupSecondSubviews];
        [self setupThirdSubviews];
        [self setupFourthSubviews];
        [self setupFifthSubviews];
        [self setupSixthSubviews];
        [self setupSeventhSubviews];
    }
    
    return self;
}

#pragma mark - > 查看组别介绍
- (void)lookButtonOnClick{
    
    [self cancelButtonClick];

    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.arrayInfoModel.h5_group_desc;
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 查看尺码表
- (void)playerClothFormButtonOnClick{
    
    [self cancelButtonClick];

    FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
    browser.isFullWidthForLandScape = YES;
    browser.isNeedLandscape = YES;
    browser.currentImageIndex = 0;
    browser.imageArray = @[self.arrayInfoModel.size_img].mutableCopy;
    browser.originalImageArray = @[self.arrayInfoModel.size_img].copy;
    browser.vc = self.vc;
    browser.fromType = 1;
    [browser show];
}

#pragma mark - > 选择品牌
- (void)doneBrandButtonClick{
    
    NSInteger onerow=[self.brandPickerview selectedRowInComponent:0];
    self.brandTF.text = self.brandArray[onerow];
    self.brandID = self.arrayInfoModel.list_brand[onerow].select_id;
    [self cancelButtonClick];
}

#pragma mark - > 选择组别
- (void)doneZubieButtonClick{
 
    NSInteger onerow=[self.zubiePickerView selectedRowInComponent:0];
    self.zubieTF.text = self.zubieArray[onerow];
    self.zubieID = self.arrayInfoModel.list_zubie[onerow].select_id;
    [self cancelButtonClick];
}

#pragma mark - > 选择轮胎
- (void)doneLuntaiButtonClick{
    
    NSInteger onerow=[self.luntaiPickerView selectedRowInComponent:0];
    self.luntaiTF.text = self.luntaiArray[onerow];
    self.luntaiID = self.arrayInfoModel.list_luntai[onerow].select_id;
    [self cancelButtonClick];
}

#pragma mark - > 选择机油
- (void)doneJiyouButtonClick{
    
    NSInteger onerow=[self.jiyouPickerView selectedRowInComponent:0];
    self.jiyouTF.text = self.jiyouArray[onerow];
    self.jiyouID = self.arrayInfoModel.list_jiyou[onerow].select_id;
    [self cancelButtonClick];
}

#pragma mark - > 取消选择器
- (void)cancelButtonClick{

    [self endEditing:YES];
}

#pragma mark - > 保存后先上传广告位
- (void)saveButtonClick{
    
    [self cancelButtonClick];
    
    /* 身份证、驾驶证、免冠寸照判断 */
    if (self.idCard_img.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请上传身份证照片" toController:self.vc];
        return;
    }
    
    if (self.driver_img.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请上传驾驶证照片" toController:self.vc];
        return;
    }
    
    if (self.mianguan_img.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请上传免冠寸照" toController:self.vc];
        return;
    }
    
    /* 品牌 */
    if (self.brandID.length < 0 || [self.brandID isEqualToString:@"0"] ) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择参赛车辆品牌" toController:self.vc];
        return;
    }
    
    /* 轮胎 */
    if (self.luntaiID.length < 0 || [self.luntaiID isEqualToString:@"0"]) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择参赛车辆轮胎品牌" toController:self.vc];
        return;
    }
    
    /* 机油 */
    
    if (![self.jinqi isEqualToString:@"4"] && (self.jiyouID.length < 0 || [self.jiyouID isEqualToString:@"0"])) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择参赛车辆机油品牌" toController:self.vc];
        return;
    }
    
    /* 组别 */
    if (self.zubieID.length < 0 || [self.zubieID isEqualToString:@"0"]) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择参赛组别" toController:self.vc];
        return;
    }
    
    if ([self.arrayInfoModel.plan_status isEqualToString:@"1"] && [self.arrayInfoModel.car_ad isEqualToString:@"1"]) {
        /* 开启了广告位并且勾选了 */
        
        /* 车头图片 */
        if (self.head_img.length <= 0){
            [[FWHudManager sharedManager] showErrorMessage:@"请上传车头部图片" toController:self.vc];
            return;
        }
        
        /* 车尾图片 */
        if (self.foot_img.length <= 0){
            [[FWHudManager sharedManager] showErrorMessage:@"请上传车尾部图片" toController:self.vc];
            return;
        }
        
        /* 主驾驶侧图片 */
        if (self.body_img.length <= 0){
            [[FWHudManager sharedManager] showErrorMessage:@"请上传主驾驶侧图片" toController:self.vc];
            return;
        }
    }
    
    
    if ([self.jinqi isEqualToString:@"4"]) {
        /* 电动 */
        self.pailiangTF.text = @"";
        self.jiyouID = @"0";
    }
    [self requestSaveData];
}

#pragma mark - > 提交
- (void)requestSaveData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * parmas = [[self getParams] copy];
    
    [request startWithParameters:parmas WithAction:Submit_perfect_baoming_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"信息保存成功" toController:self.vc];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.vc.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

- (NSDictionary *)getParams{
    
    if (!self.clothes_size) {
        self.clothes_size = @"";
    }

    if (!self.idCard_img) {
        self.idCard_img = @"";
    }
    
    if (!self.driver_img) {
        self.driver_img = @"";
    }
    
    if (!self.mianguan_img) {
        self.mianguan_img = @"";
    }
    
    if (!self.head_img) {
        self.head_img = @"";
    }
    
    if (!self.foot_img) {
        self.foot_img = @"";
    }
    
    if (!self.body_img) {
        self.body_img = @"";
    }
    
    if (!self.brandID) {
        self.brandID = @"";
    }
    
    if (!self.jinqi) {
        self.jinqi = @"";
    }
    
    if (!self.qudong) {
        self.qudong = @"";
    }
    
    if (!self.zubieID) {
        self.zubieID = @"";
    }
    
    if (!self.jiyouID) {
        self.jiyouID = @"";
    }
    
    if (!self.luntaiID) {
        self.luntaiID = @"";
    }
    
    if (self.pailiangTF.text.length < 0) {
        self.pailiangTF.text = @"0";
    }
    
    if ([self.arrayInfoModel.plan_status isEqualToString:@"1"] && [self.arrayInfoModel.car_ad isEqualToString:@"1"]) {
        /* 开启了广告位 */
        /* 车身图片 */
        if (!self.ad_position_imgs ||self.ad_position_imgs.length < 0){
            self.ad_position_imgs = @"";
        }else{
            
            self.ad_position_imgs = [self.adImages mj_JSONString];
        }
    }
    
    if ([self.arrayInfoModel.plan_status integerValue] != 1) {
        /* 只要不为1，都设置成2 */
        self.arrayInfoModel.plan_status = @"2";
    }
    
    NSMutableDictionary * params = @{
                                      @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                      @"weixin_id":self.wxNumTF.text,
                                      @"clothes_size":self.clothes_size,
                                      @"idcard_img":self.idCard_img,
                                      @"driver_img":self.driver_img,
                                      @"mianguan_img":self.mianguan_img,
                                      @"brand":self.brandID,
                                      @"car_style":self.carStyleTF.text,
                                      @"club":self.clubTF.text,
                                      @"carframe_number":self.frameNumTF.text,
                                      @"jinqi":self.jinqi,
                                      @"qudong":self.qudong,
                                      @"mali":self.maliDataTF.text,
                                      @"pailiang":self.pailiangTF.text,
                                      @"gaizhuang_order":self.gaizhuangTV.text,
                                      @"zubie":self.zubieID,
                                      @"jiyou":self.jiyouID,
                                      @"luntai":self.luntaiID,
                                      @"baoming_user_id":self.baoming_user_id,
                                      @"gaizhuang_fee":self.gaizhuangSumTF.text,
                                      @"join_ad_plan":self.arrayInfoModel.plan_status,
                                      }.mutableCopy;
    
    if ([self.arrayInfoModel.plan_status isEqualToString:@"1"] &&
        [self.arrayInfoModel.car_ad isEqualToString:@"1"]) {
        [params setValue:self.ad_position_imgs forKey:@"ad_position_imgs"];
    }
    
    return [params copy];
}

#pragma mark - > 设值
- (void)configForViewWithArrayModel:(nonnull FWPerfectInfoModel *)arrayModel{
    
    FWPlayerSignupPerfectInfoModel * model = arrayModel.perfect_info;
    /* 设置segement */
    [self setupSegementWithModel:model withArrayModel:arrayModel];

    /* pickerview数组 */
    [self setupPickerViewWithArrayModel:arrayModel];
    
    if ([arrayModel.car_ad isEqualToString:@"1"]) {
        
        if ([arrayModel.plan_status isEqualToString:@"1"]) {
            self.adPlanButton.selected = YES;
        }else{
            self.adPlanButton.selected = NO;
        }
        
        /* 显示广告计划 */
        self.adPlanDescLabel.text = arrayModel.plan_desc;
        self.adPlanTitleLabel.text = arrayModel.plan_title;
        self.adPlanTitleLabel2.text = arrayModel.plan_title2;
        
        self.adShadowBgView.hidden = NO;
        [self.announceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.adShadowBgView);
            make.width.mas_equalTo(self.adShadowBgView);
            make.height.mas_greaterThanOrEqualTo(10);
            make.top.mas_equalTo(self.adShadowBgView.mas_bottom).mas_offset(20);
            make.bottom.mas_equalTo(self.seventhView).mas_offset(-20);
        }];
        
        NSMutableArray * tempArr = @[].mutableCopy;
        
        /* 广告图片 */
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if (tempArr.count > 0) {
                [tempArr removeAllObjects];
            }
            
            for (NSString * img_url in model.ad_position_imgs) {
                
                NSData *data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:img_url]];
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    [tempArr addObject:image];
                }
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                if (tempArr.count > 0) {
                    self.head_img = model.ad_position_imgs[0];
                    self.headImageView.image = tempArr[0];
                    if (self.canEdited) {
                        self.headReUploadLabel.hidden = NO;
                    }else{
                        self.headReUploadLabel.hidden = YES;
                    }
                    
                    if (self.adImages.count >0) {
                        [self.adImages replaceObjectAtIndex:0 withObject:self.head_img];
                    }
                }
                
                if (tempArr.count > 1) {
                    self.foot_img = model.ad_position_imgs[1];
                    self.footImageView.image = tempArr[1];
                    if (self.canEdited) {
                        self.footReUploadLabel.hidden = NO;
                    }else{
                        self.footReUploadLabel.hidden = YES;
                    }
                    if (self.adImages.count >1) {
                        [self.adImages replaceObjectAtIndex:1 withObject:self.foot_img];
                    }
                }
                
                if (tempArr.count > 2) {
                    self.body_img = model.ad_position_imgs[2];
                    self.bodyImageView.image = tempArr[2];
                    if (self.canEdited) {
                        self.bodyReUploadLabel.hidden = NO;
                    }else{
                        self.bodyReUploadLabel.hidden = YES;
                    }
                    if (self.adImages.count >2) {
                        [self.adImages replaceObjectAtIndex:2 withObject:self.body_img];
                    }
                }
            });
        });
    }else{
        self.adShadowBgView.hidden = YES;
        self.arrayInfoModel.plan_status = @"2";
        
        [self.announceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.zubieTF);
            make.width.mas_equalTo(self.zubieTF);
            make.height.mas_greaterThanOrEqualTo(10);
            make.top.mas_equalTo(self.zubieTF.mas_bottom).mas_offset(20);
            make.bottom.mas_equalTo(self.seventhView).mas_offset(-20);
        }];
    }
    
    if (model.ad_position_imgs.count <= 0 && !self.canEdited) {
        /* 不可编辑下，且没有广告图片，不显示该模块 */
        self.adShadowBgView.hidden = YES;
        self.arrayInfoModel.plan_status = @"2";
        
        [self.announceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.zubieTF);
            make.width.mas_equalTo(self.zubieTF);
            make.height.mas_greaterThanOrEqualTo(10);
            make.top.mas_equalTo(self.zubieTF.mas_bottom).mas_offset(20);
            make.bottom.mas_equalTo(self.seventhView).mas_offset(-20);
        }];
    }
    
    self.topInfoLabel.text = arrayModel.tip;
    self.nickNameLabel.text = arrayModel.driver_info.nickname;
    self.secondTipLabel.text = arrayModel.mianguan_img_desc;
    self.realNameLabel.text = [NSString stringWithFormat:@"姓名:%@",arrayModel.driver_info.real_name];
    self.phoneLabel.text = [NSString stringWithFormat:@"手机号:%@",arrayModel.driver_info.mobile];
    self.cardNumLabel.text =  [NSString stringWithFormat:@"证件号:%@",arrayModel.driver_info.idcard];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:arrayModel.driver_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    [self.idcardImageView sd_setImageWithURL:[NSURL URLWithString:model.idcard_img] placeholderImage:[UIImage imageNamed:@"upload_card"]];
    [self.carCardImageView sd_setImageWithURL:[NSURL URLWithString:model.driver_img] placeholderImage:[UIImage imageNamed:@"upload_card"]];
    [self.photographImageView sd_setImageWithURL:[NSURL URLWithString:model.mianguan_img] placeholderImage:[UIImage imageNamed:@"upload_card"]];

    self.idCard_img = model.idcard_img;
    self.driver_img = model.driver_img;
    self.mianguan_img = model.mianguan_img;

    if (model.idcard_img.length > 0 && self.canEdited) {
        self.idcardReUploadLabel.hidden = NO;
    }else{
        self.idcardReUploadLabel.hidden = YES;
    }
    
    if (model.driver_img.length > 0 && self.canEdited) {
        self.carCardReUploadLabel.hidden = NO;
    }else{
        self.carCardReUploadLabel.hidden = YES;
    }
    
    if (model.mianguan_img.length > 0 && self.canEdited) {
        self.photographReUploadLabel.hidden = NO;
    }else{
        self.photographReUploadLabel.hidden = YES;
    }
    
    if (self.head_img.length > 0 && self.canEdited) {
        self.headReUploadLabel.hidden = NO;
    }else{
        self.headReUploadLabel.hidden = YES;
    }
    
    if (self.foot_img.length > 0 && self.canEdited) {
        self.footReUploadLabel.hidden = NO;
    }else{
        self.footReUploadLabel.hidden = YES;
    }
    
    if (self.body_img.length > 0 && self.canEdited) {
        self.bodyReUploadLabel.hidden = NO;
    }else{
        self.bodyReUploadLabel.hidden = YES;
    }
    self.wxNumTF.text = model.weixin_id;
    self.carStyleTF.text = model.car_style;
    self.clubTF.text = model.club;
    self.frameNumTF.text = model.carframe_number;
    self.maliDataTF.text = model.mali;
    self.pailiangTF.text = model.pailiang;
    self.gaizhuangTV.text = model.gaizhuang_order;
    self.gaizhuangSumTF.text = model.gaizhuang_fee;

    self.zubieID = model.zubie;
    self.brandID = model.brand;
    self.luntaiID = model.luntai;
    self.jiyouID = model.jiyou;

    
    NSInteger brandIndex = [model.brand integerValue]-1;
    NSInteger zubieIndex = [model.zubie integerValue]-1;
    NSInteger jiyouIndex = [model.jiyou integerValue]-1;
    NSInteger luntaiIndex = [model.luntai integerValue]-1;

    if (brandIndex < 0) {
        brandIndex = 0;
        self.brandID = @"0";
        self.brandTF.text = @"";
    }else{
        FWPerfectInfoBrandListModel * brandModel = arrayModel.list_brand[brandIndex];
        self.brandTF.text = brandModel.val;
    }
   
    if (zubieIndex < 0) {
        zubieIndex = 0;
        self.zubieID = @"0";
        self.zubieTF.text = @"";
    }else{
        FWPerfectInfoZubieListModel * zubieModel = arrayModel.list_zubie[zubieIndex];
        self.zubieTF.text = zubieModel.val;
    }
    
    if (jiyouIndex < 0) {
        jiyouIndex = 0;
        self.jiyouID = @"0";
        self.jiyouTF.text = @"";
    }else{
        FWPerfectInfoJiyouListModel * jiyouModel = arrayModel.list_jiyou[jiyouIndex];
        self.jiyouTF.text = jiyouModel.val;
    }
    
    if (luntaiIndex < 0) {
        luntaiIndex = 0;
        self.luntaiID = @"0";
        self.luntaiTF.text = @"";
    }else{
        FWPerfectInfoLuntaiListModel * luntaiModel = arrayModel.list_luntai[luntaiIndex];
        self.luntaiTF.text = luntaiModel.val;
    }
    
    self.originalDict = [self getParams];
}

- (void)setupSegementWithModel:(FWPlayerSignupPerfectInfoModel *)model withArrayModel:(nonnull FWPerfectInfoModel *)arrayModel{
    
    self.arrayInfoModel = arrayModel;
    
    if (self.sizeArray.count > 0) {
        [self.sizeArray removeAllObjects];
    }
    if (self.jinqiArray.count > 0) {
        [self.jinqiArray removeAllObjects];
    }
    if (self.qudongArray.count > 0) {
        [self.qudongArray removeAllObjects];
    }
    
    for (int i = 0; i < arrayModel.list_size.count; i ++) {
        FWPerfectInfoSizeListModel * sizeModel = arrayModel.list_size[i];
        [self.sizeArray addObject:sizeModel.val];
        
        if ([sizeModel.select_id isEqualToString:model.clothes_size]) {
            self.sizeIndex = i;
        }
    }
    
    for (int i = 0; i < arrayModel.list_jinqi.count; i ++) {
        FWPerfectInfoJinqiListModel * jinqiModel = arrayModel.list_jinqi[i];
        [self.jinqiArray addObject:jinqiModel.val];
        
        if ([jinqiModel.select_id isEqualToString:model.jinqi]) {
            self.jinqiIndex = i;
        }
    }
    
    for (int i = 0; i < arrayModel.list_qudong.count; i ++) {
        FWPerfectInfoQudongListModel * qudongModel = arrayModel.list_qudong[i];
        [self.qudongArray addObject:qudongModel.val];
        
        if ([qudongModel.select_id isEqualToString:model.qudong]) {
            self.qudongIndex = i;
        }
    }
    
    [self.sizeSegement deliverTitles:self.sizeArray];
    [self.jinqiSegement deliverTitles:self.jinqiArray];
    [self.qudongSegement deliverTitles:self.qudongArray];
    
    if (self.sizeIndex >= 0) {
        [self.sizeSegement menuUpdateStyle:self.sizeIndex];
        [self segementItemClickTap:self.sizeIndex WithType:sizeType];
        self.sizeSegement.lastIndex = self.sizeIndex;
    }
    
    if (self.jinqiIndex>= 0) {
        [self.jinqiSegement menuUpdateStyle:self.jinqiIndex];
        [self segementItemClickTap:self.jinqiIndex WithType:jinqiType];
        self.jinqiSegement.lastIndex = self.jinqiIndex;
    }
    
    if (self.qudongIndex>= 0) {
        [self.qudongSegement menuUpdateStyle:self.qudongIndex];
        [self segementItemClickTap:self.qudongIndex WithType:qudongType];
        self.qudongSegement.lastIndex = self.qudongIndex;
    }
}

#pragma mark - > segement选择
-(void)segementItemClickTap:(NSInteger)index WithType:(NSString *)type{
    
    if ([type isEqualToString:sizeType]) {
        
        FWPerfectInfoSizeListModel * sizeModel = self.arrayInfoModel.list_size[index];
        if (self.sizeIndex != index) {
            self.sizeIndex = index;
        }
        self.clothes_size = sizeModel.select_id;
    }else if ([type isEqualToString:jinqiType]) {
        
        FWPerfectInfoJinqiListModel * jinqiModel = self.arrayInfoModel.list_jinqi[index];
        if (self.jinqiIndex != index) {
            self.jinqiIndex = index;
        }
        self.jinqi = jinqiModel.select_id;
        
        if ([jinqiModel.select_id isEqualToString:@"4"]) {
            
            self.pailiangTF.hidden = YES;
            self.pailiangLabel.hidden = YES;
            self.pailiangLineView.hidden = YES;
            
            self.jiyouTF.hidden = YES;
            self.jiyouLabel.hidden = YES;
            self.jiyouLineView.hidden = YES;
            
            [self.luntaiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.pailiangLabel);
                make.size.mas_equalTo(self.pailiangLabel);
                make.top.mas_equalTo(self.maliDataLineView.mas_bottom).mas_offset(15);
            }];
            
            [self.luntaiLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.luntaiLabel);
                make.height.mas_equalTo(0.5);
                make.width.mas_equalTo(self.luntaiTF);
                make.top.mas_equalTo(self.luntaiTF.mas_bottom).mas_offset(0);
                make.bottom.mas_equalTo(self.fifthView.mas_bottom).mas_offset(-20);
            }];
            
        }else{
            self.pailiangTF.hidden = NO;
            self.pailiangLabel.hidden = NO;
            self.pailiangLineView.hidden = NO;
            
            self.jiyouTF.hidden = NO;
            self.jiyouLabel.hidden = NO;
            self.jiyouLineView.hidden = NO;
            
            [self.luntaiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.pailiangLabel);
                make.size.mas_equalTo(self.pailiangLabel);
                make.top.mas_equalTo(self.pailiangLineView.mas_bottom).mas_offset(15);
            }];
            
            [self.luntaiLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.luntaiLabel);
                make.height.mas_equalTo(0.5);
                make.width.mas_equalTo(self.luntaiTF);
                make.top.mas_equalTo(self.luntaiTF.mas_bottom).mas_offset(0);
            }];
            
            [self.jiyouLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.jiyouLabel);
                make.height.mas_equalTo(0.5);
                make.width.mas_equalTo(self.jiyouTF);
                make.top.mas_equalTo(self.jiyouTF.mas_bottom).mas_offset(0);
                make.bottom.mas_equalTo(self.fifthView.mas_bottom).mas_offset(-20);
            }];
        }
        
    }else if ([type isEqualToString:qudongType]) {
        
        FWPerfectInfoQudongListModel * qudongModel = self.arrayInfoModel.list_qudong[index];
        if (self.qudongIndex != index) {
            self.qudongIndex = index;
        }
        self.qudong = qudongModel.select_id;
    }
}

#pragma mark - > 去掉前后空格
- (NSString *)checkString:(NSString *)string{
    
    NSCharacterSet  *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    string = [string stringByTrimmingCharactersInSet:set];
    return string;
}

#pragma mark - > 上传证件照
-(void)uploadPhotoClickWithType:(UITapGestureRecognizer *)tap{
    
    [self cancelButtonClick];

    NSInteger index = tap.view.tag -22222;
    
    if (index == 0) {
        self.current_type = @"1";
    }else if (index == 1){
        self.current_type = @"2";
    }else if (index == 6){
        self.current_type = @"6";
    }else{
        
        if (self.adPlanButton.isSelected) {
            if (index == 2){
                self.current_type = @"3";
            }else if (index == 3){
                self.current_type = @"4";
            }else if (index == 4){
                self.current_type = @"5";
            }
        }else{
             [[FWHudManager sharedManager] showErrorMessage:@"请先加入广告车招募计划" toController:self.vc];
            return;
        }
    }
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"上传证件照" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:1];
    }];
    UIAlertAction *selectAction = [UIAlertAction actionWithTitle:@"选择相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:2];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:takePhotoAction];
    [alertController addAction:selectAction];
    [alertController addAction:cancelAction];
    
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 选择相册
- (void)selectLibraryWithType:(NSInteger)type{
    
    if (@available(iOS 11, *)) {
        UIScrollView.appearance.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
    }
    
    NSUInteger sourceType = 0;
    
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeCamera;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
    }else{
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
    }
    
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.delegate                 = self;
    self.picker.sourceType               = sourceType;
//    self.picker.allowsEditing            = YES;
    self.picker.navigationController.navigationBar.hidden = NO;
    self.picker.fd_prefersNavigationBarHidden = NO;
    self.picker.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.vc.navigationController presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *avatar = info[UIImagePickerControllerOriginalImage];
    //处理完毕，回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    NSArray * object_keys = [self.dataDict objectForKey:@"object_keys"];
    NSArray * ad_object_keys = [self.ad_plan_dataDict objectForKey:@"object_keys"];

    NSString * imageString ;
    if ([self.current_type isEqualToString:@"1"]) {
        /* 身份证 */
        imageString = object_keys[0];
    }else if ([self.current_type isEqualToString:@"2"]){
        /* 驾驶证 */
        imageString = object_keys[1];
    }else if ([self.current_type isEqualToString:@"3"]){
        /* 车头部 */
        imageString = ad_object_keys[0];
    }else if ([self.current_type isEqualToString:@"4"]){
        /* 车尾部 */
        imageString = ad_object_keys[1];
    }else if ([self.current_type isEqualToString:@"5"]){
        /* 主驾驶侧 */
        imageString = ad_object_keys[2];
    }else if ([self.current_type isEqualToString:@"6"]){
        /* 免冠照片 */
        imageString = object_keys[2];
    }
    
    [ALiOssUploadTool asyncUploadImage:avatar WithBucketName:[self.dataDict objectForKey:@"bucket"] WithObject_key:imageString WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
        if (UploadImageSuccess == state) {
            
            dispatch_async(dispatch_get_main_queue(), ^{

                //在主队列中进行ui操作
                if ([self.current_type isEqualToString:@"1"]) {
                       /* 身份证 */
                       self.idCard_img = imageString;
                       self.idcardReUploadLabel.hidden = NO;
                       self.idcardImageView.image = avatar;
                   }else if ([self.current_type isEqualToString:@"2"]){
                       /* 驾驶证 */
                       self.driver_img = imageString;
                       self.carCardReUploadLabel.hidden = NO;
                       self.carCardImageView.image = avatar;
                   }else if ([self.current_type isEqualToString:@"3"]){
                       /* 车头部 */
                       self.head_img = imageString;
                       self.headReUploadLabel.hidden = NO;
                       self.headImageView.image = avatar;
                       [self.adImages replaceObjectAtIndex:0 withObject:ad_object_keys[0]];
                   }else if ([self.current_type isEqualToString:@"4"]){
                       /* 车尾部 */
                       self.foot_img = imageString;
                       self.footReUploadLabel.hidden = NO;
                       self.footImageView.image = avatar;
                       [self.adImages replaceObjectAtIndex:1 withObject:ad_object_keys[1]];
                   }else if ([self.current_type isEqualToString:@"5"]){
                       /* 主驾驶侧 */
                       self.body_img = imageString;
                       self.bodyReUploadLabel.hidden = NO;
                       self.bodyImageView.image = avatar;
                       [self.adImages replaceObjectAtIndex:2 withObject:ad_object_keys[2]];
                   }else if ([self.current_type isEqualToString:@"6"]){
                       /* 免冠头像 */
                       self.mianguan_img = imageString;
                       self.photographReUploadLabel.hidden = NO;
                       self.photographImageView.image = avatar;
                   }
            });
        }
    }];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([UIDevice currentDevice].systemVersion.floatValue < 11) {
        return;
    }
    if ([viewController isKindOfClass:NSClassFromString(@"PUPhotoPickerHostViewController")]) {
        [viewController.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.frame.size.width < 42) {
                [viewController.view sendSubviewToBack:obj];
                *stop = YES;
            }
        }];
    }
}
#pragma mark - > 设置pickerview的数组
- (void)setupPickerViewWithArrayModel:(nonnull FWPerfectInfoModel *)arrayModel{
    
    if (self.brandArray.count > 0) {
        [self.brandArray removeAllObjects];
    }
    if (self.zubieArray.count > 0) {
        [self.zubieArray removeAllObjects];
    }
    if (self.jiyouArray.count > 0) {
        [self.jiyouArray removeAllObjects];
    }
    if (self.luntaiArray.count > 0) {
        [self.luntaiArray removeAllObjects];
    }
    
    
    for (FWPerfectInfoBrandListModel * brandModel in arrayModel.list_brand) {
        [self.brandArray addObject:brandModel.val];
    }
    
    for (FWPerfectInfoZubieListModel * zubieModel in arrayModel.list_zubie) {
        [self.zubieArray addObject:zubieModel.val];
    }
    
    for (FWPerfectInfoLuntaiListModel * luntaiModel in arrayModel.list_luntai) {
        [self.luntaiArray addObject:luntaiModel.val];
    }
    
    for (FWPerfectInfoJiyouListModel * jiyouModel in arrayModel.list_jiyou) {
        [self.jiyouArray addObject:jiyouModel.val];
    }
}

#pragma mark - > pickerview数据源及代理方法
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

#pragma mark 数据源  numberOfRowsInComponent
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == self.brandPickerview) {
        return self.brandArray.count;
    }else if (pickerView == self.jiyouPickerView){
        return self.jiyouArray.count;
    }else if (pickerView == self.luntaiPickerView){
        return self.luntaiArray.count;
    }
    return self.zubieArray.count;
}

#pragma delegate 显示信息方法
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == self.brandPickerview) {
        return [self.brandArray objectAtIndex:row];
    }else if (pickerView == self.jiyouPickerView){
        return [self.jiyouArray objectAtIndex:row];
    }else if (pickerView == self.luntaiPickerView){
        return [self.luntaiArray objectAtIndex:row];
    }
    return [self.zubieArray objectAtIndex:row];
}

#pragma mark 选中行信息
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
}

#pragma mark 显示行高
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.00;
}

#pragma mark - pickerView 每列宽度
- (CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    return SCREEN_WIDTH;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = (UILabel *)view;
    if (lbl == nil) {
        lbl = [[UILabel alloc]init];
        //在这里设置字体相关属性
        lbl.font = [UIFont systemFontOfSize:18];
        lbl.textColor = FWTextColor_000000;
        [lbl setTextAlignment:1];
        [lbl setBackgroundColor:[UIColor clearColor]];
    }
    //重新加载lbl的文字内容
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return lbl;
}

#pragma mark - > 勾选/取消 广告计划
- (void)adPlanButtonOnClick{

    self.adPlanButton.selected = !self.adPlanButton.selected;
    
    if (self.adPlanButton.isSelected) {
        self.arrayInfoModel.plan_status = @"1";
    }else{
        self.arrayInfoModel.plan_status = @"2";
    }
}

#pragma mark - > 车架号输入都转大写
- (void)textViewDidChange:(UITextView *)textView{
    
    if (textView == self.frameNumTF) {
        if ([textView.text containsString:@" "]) {
            textView.text = [textView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        
        textView.text = [textView.text uppercaseString];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (textView == self.frameNumTF) {
        
        //如果是删除减少字数，都返回允许修改
        if ([text isEqualToString:@""]) {
            return YES;
        }
        
        if (range.location >= 17){
            return NO;
        }
        
        /* 只允许输入字母和数字 */
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"] invertedSet];
        NSString *filtered = [[text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [text isEqualToString:filtered];
    }
    return YES;
}

// 禁止复制粘贴之类的输入
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {

    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController) {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}
#pragma mark - > ************************* UI布局 *************************
- (void)setupFirstSubviews{
    
    self.firstView = [[UIView alloc] init];
    self.firstView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.firstView];
    [self.firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(400);
    }];
    
    UIImageView * lingdangImageView = [[UIImageView alloc] init];
    lingdangImageView.image = [UIImage imageNamed:@"lingdang"];
    [self.firstView addSubview:lingdangImageView];
    [lingdangImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(12,13));
        make.left.mas_equalTo(self.firstView).mas_offset(20);
        make.top.mas_equalTo(self.firstView).mas_offset(11);
    }];
    
    self.topInfoLabel = [[UILabel alloc] init];
    self.topInfoLabel.numberOfLines = 0;
    self.topInfoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.topInfoLabel.textColor = FWColor(@"ff6f00");
    self.topInfoLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:self.topInfoLabel];
    [self.topInfoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lingdangImageView.mas_right).mas_offset(5);
        make.width.mas_equalTo(SCREEN_WIDTH-50);
        make.height.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.firstView).mas_offset(9);
    }];
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.firstView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topInfoLabel.mas_bottom).mas_offset(16);
        make.left.mas_equalTo(self.firstView).mas_offset(14);
        make.right.mas_equalTo(self.firstView).mas_offset(-14);
        make.height.mas_equalTo(142);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
    }];
    
    self.infoView = [[UIView alloc] init];
    self.infoView.layer.cornerRadius = 5;
    self.infoView.layer.masksToBounds = YES;
    [self.shadowView addSubview:self.infoView];
    [self.infoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 30/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.infoView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.infoView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    self.nickNameLabel = [[UILabel alloc] init];
    self.nickNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nickNameLabel.textColor = FWTextColor_222222;
    self.nickNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.infoView addSubview:self.nickNameLabel];
    [self.nickNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(11);
        make.right.mas_equalTo(self.infoView).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(20);
    }];
    
    self.realNameLabel = [[UILabel alloc] init];
    self.realNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.realNameLabel.textColor = FWTextColor_222222;
    self.realNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.infoView addSubview:self.realNameLabel];
    [self.realNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.nickNameLabel);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(self.photoImageView.mas_bottom).mas_offset(10);
    }];
    
    self.phoneLabel = [[UILabel alloc] init];
    self.phoneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.phoneLabel.textColor = FWTextColor_222222;
    self.phoneLabel.textAlignment = NSTextAlignmentLeft;
    [self.infoView addSubview:self.phoneLabel];
    [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.realNameLabel);
        make.right.mas_equalTo(self.realNameLabel);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(self.realNameLabel.mas_bottom).mas_offset(4);
    }];
    
    self.cardNumLabel = [[UILabel alloc] init];
    self.cardNumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.cardNumLabel.textColor = FWTextColor_222222;
    self.cardNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.infoView addSubview:self.cardNumLabel];
    [self.cardNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.realNameLabel);
        make.right.mas_equalTo(self.realNameLabel);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(self.phoneLabel.mas_bottom).mas_offset(4);
    }];
    
    self.wxNumLabel = [[UILabel alloc] init];
    self.wxNumLabel.text = @"微信号码";
    self.wxNumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.wxNumLabel.textColor = FWTextColor_222222;
    self.wxNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:self.wxNumLabel];
    [self.wxNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.firstView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.top.mas_equalTo(self.shadowView.mas_bottom).mas_offset(20);
    }];
    
    self.wxNumTF = [[UITextField alloc] init];
    self.wxNumTF.delegate = self;
    self.wxNumTF.placeholder = @"微信号用于建立车手群";
    self.wxNumTF.textColor = FWTextColor_222222;
    [self.firstView addSubview:self.wxNumTF];
    [self.wxNumTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.wxNumLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.wxNumLabel.mas_bottom).mas_offset(0);
    }];
    
    self.wxNumLineView =  [[UIView alloc] init];
    self.wxNumLineView.backgroundColor = FWTextColor_141414;
    [self.firstView addSubview:self.wxNumLineView];
    [self.wxNumLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.wxNumLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.wxNumTF);
        make.top.mas_equalTo(self.wxNumTF.mas_bottom).mas_offset(0);
    }];
    
    self.playerClothLabel = [[UILabel alloc] init];
    self.playerClothLabel.text = @"车手服装";
    self.playerClothLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.playerClothLabel.textColor = FWTextColor_222222;
    self.playerClothLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:self.playerClothLabel];
    [self.playerClothLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.wxNumLabel);
        make.size.mas_equalTo(CGSizeMake(100, 16));
        make.top.mas_equalTo(self.wxNumLineView.mas_bottom).mas_offset(20);
    }];
    
    self.playerClothFormButton = [[UIButton alloc] init];
    self.playerClothFormButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.playerClothFormButton setImage:[UIImage imageNamed:@"perfect_arrow"] forState:UIControlStateNormal];
    [self.playerClothFormButton setTitle:@"查看尺码表" forState:UIControlStateNormal];
    [self.playerClothFormButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    CGFloat leftlab = [@"查看尺码表" sizeWithAttributes:@{NSFontAttributeName : self.playerClothFormButton.titleLabel.font}].width;
    [self.playerClothFormButton setImageEdgeInsets:UIEdgeInsetsMake(0, leftlab+10, 0, 0)];
    [self.playerClothFormButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    [self.playerClothFormButton addTarget:self action:@selector(playerClothFormButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.firstView addSubview:self.playerClothFormButton];
    [self.playerClothFormButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.playerClothLabel);
        make.size.mas_equalTo(CGSizeMake(80, 40));
        make.right.mas_equalTo(self.firstView).mas_offset(-10);
    }];
    
    self.sizeSegement = [[FWPerfectSegement alloc]init];
    self.sizeSegement.type = sizeType;
    self.sizeSegement.itemDelegate = self;
    [self.firstView addSubview:self.sizeSegement];
    [self.sizeSegement mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 60));
        make.left.mas_equalTo(self.firstView);
        make.top.mas_equalTo(self.playerClothLabel.mas_bottom).mas_offset(10);
    }];
}

- (void)setupSecondSubviews{
    
    self.secondView = [[UIView alloc] init];
    self.secondView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.secondView];
    [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.firstView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(219-124+(82*(SCREEN_WIDTH-52)/3)/112+20);
    }];
    
    self.cardUploadLabel = [[UILabel alloc] init];
    self.cardUploadLabel.text = @"证件上传";
    self.cardUploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.cardUploadLabel.textColor = FWTextColor_222222;
    self.cardUploadLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:self.cardUploadLabel];
    [self.cardUploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.secondView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(100, 16));
        make.top.mas_equalTo(self.secondView).mas_offset(20);
    }];
    
    self.idcardLabel = [[UILabel alloc] init];
    self.idcardLabel.text = @"身份证";
    self.idcardLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.idcardLabel.textColor = FWColor(@"BCBCBC");
    self.idcardLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:self.idcardLabel];
    [self.idcardLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cardUploadLabel);
        make.size.mas_equalTo(CGSizeMake(100, 16));
        make.top.mas_equalTo(self.cardUploadLabel.mas_bottom).mas_offset(17);
    }];
    
    self.idcardImageView = [[UIImageView alloc] init];
    self.idcardImageView.tag = 22222;
    self.idcardImageView.layer.cornerRadius = 2;
    self.idcardImageView.layer.masksToBounds = YES;
    self.idcardImageView.userInteractionEnabled = YES;
    self.idcardImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.idcardImageView.clipsToBounds = YES;
    self.idcardImageView.image = [UIImage imageNamed:@"upload_card_placeholder"];
    [self.secondView addSubview:self.idcardImageView];
    [self.idcardImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.idcardLabel);
        make.width.mas_equalTo((SCREEN_WIDTH-52)/3);
        make.height.mas_equalTo((82*(SCREEN_WIDTH-52)/3)/112);
        make.top.mas_equalTo(self.idcardLabel.mas_bottom).mas_offset(9);
    }];
    [self.idcardImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType:)]];

    self.idcardReUploadLabel = [[UILabel alloc] init];
    self.idcardReUploadLabel.text = @"重新上传";
    self.idcardReUploadLabel.layer.cornerRadius = 2;
    self.idcardReUploadLabel.layer.masksToBounds = YES;
    self.idcardReUploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.idcardReUploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.idcardReUploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.idcardReUploadLabel.textAlignment = NSTextAlignmentCenter;
    [self.idcardImageView addSubview:self.idcardReUploadLabel];
    [self.idcardReUploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.idcardImageView);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(self.idcardImageView);
        make.bottom.mas_equalTo(self.idcardImageView).mas_offset(0);
    }];
    self.idcardReUploadLabel.hidden = YES;
    
    self.carCardImageView = [[UIImageView alloc] init];
    self.carCardImageView.tag = 22223;
    self.carCardImageView.layer.cornerRadius = 2;
    self.carCardImageView.layer.masksToBounds = YES;
    self.carCardImageView.userInteractionEnabled = YES;
    self.carCardImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.carCardImageView.clipsToBounds = YES;
    self.carCardImageView.image = [UIImage imageNamed:@"upload_card"];
    [self.secondView addSubview:self.carCardImageView];
    [self.carCardImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.idcardImageView.mas_right).mas_offset(7);
        make.width.mas_equalTo(self.idcardImageView);
        make.height.mas_equalTo(self.idcardImageView);
        make.centerY.mas_equalTo(self.idcardImageView);
    }];
    [self.carCardImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType:)]];

    self.carCardReUploadLabel = [[UILabel alloc] init];
    self.carCardReUploadLabel.text = @"重新上传";
    self.carCardReUploadLabel.layer.cornerRadius = 2;
    self.carCardReUploadLabel.layer.masksToBounds = YES;
    self.carCardReUploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.carCardReUploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.carCardReUploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.carCardReUploadLabel.textAlignment = NSTextAlignmentCenter;
    [self.carCardImageView addSubview:self.carCardReUploadLabel];
    [self.carCardReUploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.carCardImageView);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(self.carCardImageView);
        make.bottom.mas_equalTo(self.carCardImageView).mas_offset(0);
    }];
    self.carCardReUploadLabel.hidden = YES;

    self.carCardLabel = [[UILabel alloc] init];
    self.carCardLabel.text = @"驾驶证";
    self.carCardLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.carCardLabel.textColor = FWColor(@"BCBCBC");
    self.carCardLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:self.carCardLabel];
    [self.carCardLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carCardImageView);
        make.size.mas_equalTo(self.idcardLabel);
        make.centerY.mas_equalTo(self.idcardLabel);
    }];
    
    
    self.photographImageView = [[UIImageView alloc] init];
    self.photographImageView.tag = 22228;
    self.photographImageView.layer.cornerRadius = 2;
    self.photographImageView.layer.masksToBounds = YES;
    self.photographImageView.userInteractionEnabled = YES;
    self.photographImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photographImageView.clipsToBounds = YES;
    self.photographImageView.image = [UIImage imageNamed:@"upload_card"];
    [self.secondView addSubview:self.photographImageView];
    [self.photographImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carCardImageView.mas_right).mas_offset(7);
        make.width.mas_equalTo(self.carCardImageView);
        make.height.mas_equalTo(self.carCardImageView);
        make.centerY.mas_equalTo(self.carCardImageView);
    }];
    [self.photographImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType:)]];

    self.photographReUploadLabel = [[UILabel alloc] init];
    self.photographReUploadLabel.text = @"重新上传";
    self.photographReUploadLabel.layer.cornerRadius = 2;
    self.photographReUploadLabel.layer.masksToBounds = YES;
    self.photographReUploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.photographReUploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.photographReUploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.photographReUploadLabel.textAlignment = NSTextAlignmentCenter;
    [self.photographImageView addSubview:self.photographReUploadLabel];
    [self.photographReUploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.photographImageView);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(self.photographImageView);
        make.bottom.mas_equalTo(self.photographImageView).mas_offset(0);
    }];
    self.photographReUploadLabel.hidden = YES;

    self.photographLabel = [[UILabel alloc] init];
    self.photographLabel.text = @"免冠寸照";
    self.photographLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.photographLabel.textColor = FWColor(@"BCBCBC");
    self.photographLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:self.photographLabel];
    [self.photographLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photographImageView);
        make.size.mas_equalTo(self.idcardLabel);
        make.centerY.mas_equalTo(self.idcardLabel);
    }];
    
    
    self.secondTipLabel = [[UILabel alloc] init];
    self.secondTipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.secondTipLabel.textColor = FWColor(@"ff6f00");
    self.secondTipLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:self.secondTipLabel];
    [self.secondTipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.secondView);
        make.left.mas_equalTo(self.idcardLabel);
        make.height.mas_equalTo(26);
        make.width.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.idcardImageView.mas_bottom).mas_offset(5);
    }];
}

- (void)setupThirdSubviews{
    
    self.thirdView = [[UIView alloc] init];
    self.thirdView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.thirdView];
    [self.thirdView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.secondView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(200);
    }];
    
    self.brandLabel = [[UILabel alloc] init];
    self.brandLabel.text = @"品牌";
    self.brandLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.brandLabel.textColor = FWTextColor_222222;
    self.brandLabel.textAlignment = NSTextAlignmentLeft;
    [self.thirdView addSubview:self.brandLabel];
    [self.brandLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.thirdView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.top.mas_equalTo(self.thirdView).mas_offset(20);
    }];
    
    self.brandTF = [[UITextField alloc] init];
    self.brandTF.delegate = self;
    self.brandTF.placeholder = @"请选择参赛车辆品牌";
    self.brandTF.textColor = FWTextColor_222222;
    self.brandTF.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.thirdView addSubview:self.brandTF];
    [self.brandTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.brandLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.brandLabel.mas_bottom).mas_offset(0);
    }];
    
    self.brandArrowImageView = [[UIImageView alloc] init];
    self.brandArrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.brandTF addSubview:self.brandArrowImageView];
    [self.brandArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.centerY.mas_equalTo(self.brandTF);
        make.right.mas_equalTo(self.brandTF).mas_offset(-10);
    }];
    
    self.brandPickerview=[[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.brandPickerview.delegate=self;
    self.brandPickerview.dataSource=self;
    self.brandPickerview.backgroundColor=[UIColor clearColor];
    
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    UIBarButtonItem *doneButton = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneBrandButtonClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar.items = @[cancelButton,flexItem, fixItem, doneButton];
    
    self.brandTF.inputView = self.brandPickerview;
    self.brandTF.inputAccessoryView = bar;
    
    self.brandLineView =  [[UIView alloc] init];
    self.brandLineView.backgroundColor = FWTextColor_141414;
    [self.thirdView addSubview:self.brandLineView];
    [self.brandLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.brandLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.brandTF);
        make.top.mas_equalTo(self.brandTF.mas_bottom).mas_offset(0);
    }];
    
    self.carStyleLabel = [[UILabel alloc] init];
    self.carStyleLabel.text = @"车型";
    self.carStyleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.carStyleLabel.textColor = FWTextColor_222222;
    self.carStyleLabel.textAlignment = NSTextAlignmentLeft;
    [self.thirdView addSubview:self.carStyleLabel];
    [self.carStyleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.brandLabel);
        make.size.mas_equalTo(self.brandLabel);
        make.top.mas_equalTo(self.brandLineView.mas_bottom).mas_offset(15);
    }];
    
    self.carStyleTF = [[IQTextView alloc] init];
    self.carStyleTF.placeholder = @"请填写参赛车型";
    self.carStyleTF.textContainerInset = UIEdgeInsetsMake(5,-5,0,0);
    self.carStyleTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.carStyleTF.textColor = FWTextColor_222222;
    self.carStyleTF.placeholderTextColor = FWTextColor_BCBCBC;
    [self.thirdView addSubview:self.carStyleTF];
    [self.carStyleTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carStyleLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.carStyleLabel.mas_bottom).mas_offset(0);
    }];
    
    self.carStyleLineView =  [[UIView alloc] init];
    self.carStyleLineView.backgroundColor = FWTextColor_141414;
    [self.thirdView addSubview:self.carStyleLineView];
    [self.carStyleLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carStyleLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.carStyleTF);
        make.top.mas_equalTo(self.carStyleTF.mas_bottom).mas_offset(0);
    }];
    
    self.clubLabel = [[UILabel alloc] init];
    self.clubLabel.text = @"参赛俱乐部";
    self.clubLabel.frame = CGRectMake(15, 18, 250, 16);
    self.clubLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.clubLabel.textColor = FWTextColor_222222;
    self.clubLabel.textAlignment = NSTextAlignmentLeft;
    [self.thirdView addSubview:self.clubLabel];
    [self.clubLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carStyleLabel);
        make.size.mas_equalTo(CGSizeMake(150, 16));
        make.top.mas_equalTo(self.carStyleLineView.mas_bottom).mas_offset(15);
    }];
    
    self.clubTF = [[IQTextView alloc] init];
    self.clubTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.clubTF.placeholder = @"无请填写“个人”";
    self.clubTF.textContainerInset = UIEdgeInsetsMake(5,-5,0,0);
    self.clubTF.textColor = FWTextColor_222222;
    self.clubTF.placeholderTextColor = FWTextColor_BCBCBC;
    [self.thirdView addSubview:self.clubTF];
    [self.clubTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.clubLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.clubLabel.mas_bottom).mas_offset(0);
    }];
    
    self.clubLineView =  [[UIView alloc] init];
    self.clubLineView.backgroundColor = FWTextColor_141414;
    [self.thirdView addSubview:self.clubLineView];
    [self.clubLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.clubLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.clubTF);
        make.top.mas_equalTo(self.clubTF.mas_bottom).mas_offset(0);
    }];
    
    self.frameNumLabel = [[UILabel alloc] init];
    self.frameNumLabel.text = @"车架号";
    self.frameNumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.frameNumLabel.textColor = FWTextColor_222222;
    self.frameNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.thirdView addSubview:self.frameNumLabel];
    [self.frameNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.clubLabel);
        make.size.mas_equalTo(self.clubLabel);
        make.top.mas_equalTo(self.clubLineView.mas_bottom).mas_offset(15);
    }];
    
    self.frameNumTF = [[IQTextView alloc] init];
    self.frameNumTF.delegate = self;
    self.frameNumTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.frameNumTF.placeholder = @"17位车架号,数字+字母";
    self.frameNumTF.textContainerInset = UIEdgeInsetsMake(5,-5,0,0);
    self.frameNumTF.keyboardType = UIKeyboardTypeASCIICapable;
    self.frameNumTF.textColor = FWTextColor_222222;
    self.frameNumTF.placeholderTextColor = FWTextColor_BCBCBC;
    [self.thirdView addSubview:self.frameNumTF];
    [self.frameNumTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.frameNumLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.frameNumLabel.mas_bottom).mas_offset(0);
    }];
    
    self.frameNumLineView =  [[UIView alloc] init];
    self.frameNumLineView.backgroundColor = FWTextColor_141414;
    [self.thirdView addSubview:self.frameNumLineView];
    [self.frameNumLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.frameNumLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.frameNumTF);
        make.bottom.mas_equalTo(self.thirdView).mas_offset(-23);
        make.top.mas_equalTo(self.frameNumTF.mas_bottom).mas_offset(0);
    }];
}

- (void)setupFourthSubviews{
    
    self.fourthView = [[UIView alloc] init];
    self.fourthView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.fourthView];
    [self.fourthView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.thirdView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(183);
    }];
    
    self.jinqiLabel = [[UILabel alloc] init];
    self.jinqiLabel.text = @"进气模式";
    self.jinqiLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.jinqiLabel.textColor = FWTextColor_222222;
    self.jinqiLabel.textAlignment = NSTextAlignmentLeft;
    [self.fourthView addSubview:self.jinqiLabel];
    [self.jinqiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.fourthView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.top.mas_equalTo(self.fourthView).mas_offset(20);
    }];
    
    self.jinqiSegement = [[FWPerfectSegement alloc]init];
    self.jinqiSegement.type = jinqiType;
    self.jinqiSegement.itemDelegate = self;
    [self.fourthView addSubview:self.jinqiSegement];
    [self.jinqiSegement mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 40));
        make.left.mas_equalTo(self.fourthView);
        make.top.mas_equalTo(self.jinqiLabel.mas_bottom).mas_offset(10);
    }];
    
    self.qudongLabel = [[UILabel alloc] init];
    self.qudongLabel.text = @"驱动形式";
    self.qudongLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.qudongLabel.textColor = FWTextColor_222222;
    self.qudongLabel.textAlignment = NSTextAlignmentLeft;
    [self.fourthView addSubview:self.qudongLabel];
    [self.qudongLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.jinqiLabel);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.top.mas_equalTo(self.jinqiSegement.mas_bottom).mas_offset(11);
    }];
    
    self.qudongSegement = [[FWPerfectSegement alloc]init];
    self.qudongSegement.type = qudongType;
    self.qudongSegement.itemDelegate = self;
    [self.fourthView addSubview:self.qudongSegement];
    [self.qudongSegement mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 40));
        make.left.mas_equalTo(self.fourthView);
        make.top.mas_equalTo(self.qudongLabel.mas_bottom).mas_offset(10);
    }];
}

- (void)setupFifthSubviews{
    
    self.fifthView = [[UIView alloc] init];
    self.fifthView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.fifthView];
    [self.fifthView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.fourthView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.maliDataLabel = [[UILabel alloc] init];
    self.maliDataLabel.text = @"马力数据";
    self.maliDataLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.maliDataLabel.textColor = FWTextColor_222222;
    self.maliDataLabel.textAlignment = NSTextAlignmentLeft;
    [self.fifthView addSubview:self.maliDataLabel];
    [self.maliDataLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.fifthView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.top.mas_equalTo(self.fifthView).mas_offset(20);
    }];
    
    self.maliDataTF = [[IQTextView alloc] init];
    self.maliDataTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.maliDataTF.placeholder = @"请输入马力值";
    self.maliDataTF.textContainerInset = UIEdgeInsetsMake(5,-5,0,0);
    self.maliDataTF.keyboardType = UIKeyboardTypeNumberPad;
    self.maliDataTF.textColor = FWTextColor_222222;
    self.maliDataTF.placeholderTextColor = FWTextColor_BCBCBC;
    [self.fifthView addSubview:self.maliDataTF];
    [self.maliDataTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.maliDataLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.maliDataLabel.mas_bottom).mas_offset(0);
    }];
    
    self.maliDataLineView =  [[UIView alloc] init];
    self.maliDataLineView.backgroundColor = FWTextColor_141414;
    [self.fifthView addSubview:self.maliDataLineView];
    [self.maliDataLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.maliDataLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.maliDataTF);
        make.top.mas_equalTo(self.maliDataTF.mas_bottom).mas_offset(0);
    }];
    
    self.pailiangLabel = [[UILabel alloc] init];
    self.pailiangLabel.text = @"排量";
    self.pailiangLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.pailiangLabel.textColor = FWTextColor_222222;
    self.pailiangLabel.textAlignment = NSTextAlignmentLeft;
    [self.fifthView addSubview:self.pailiangLabel];
    [self.pailiangLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.maliDataLabel);
        make.size.mas_equalTo(self.maliDataLabel);
        make.top.mas_equalTo(self.maliDataLineView.mas_bottom).mas_offset(15);
    }];
    
    self.pailiangTF = [[IQTextView alloc] init];
    self.pailiangTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.pailiangTF.placeholder = @"如2.0T";
    self.pailiangTF.textContainerInset = UIEdgeInsetsMake(5,-5,0,0);
    self.pailiangTF.textColor = FWTextColor_222222;
    self.pailiangTF.placeholderTextColor = FWTextColor_BCBCBC;
    [self.fifthView addSubview:self.pailiangTF];
    [self.pailiangTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.pailiangLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.pailiangLabel.mas_bottom).mas_offset(0);
    }];
    
    self.pailiangLineView =  [[UIView alloc] init];
    self.pailiangLineView.backgroundColor = FWTextColor_141414;
    [self.fifthView addSubview:self.pailiangLineView];
    [self.pailiangLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.pailiangLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.pailiangTF);
        make.top.mas_equalTo(self.pailiangTF.mas_bottom).mas_offset(0);
    }];
    
    self.luntaiLabel = [[UILabel alloc] init];
    self.luntaiLabel.text = @"轮胎";
    self.luntaiLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.luntaiLabel.textColor = FWTextColor_222222;
    self.luntaiLabel.textAlignment = NSTextAlignmentLeft;
    [self.fifthView addSubview:self.luntaiLabel];
    [self.luntaiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.pailiangLabel);
        make.size.mas_equalTo(self.pailiangLabel);
        make.top.mas_equalTo(self.pailiangLineView.mas_bottom).mas_offset(15);
    }];
    
    self.luntaiTF = [[UITextField alloc] init];
    self.luntaiTF.delegate = self;
    self.luntaiTF.placeholder = @"请选择参赛车辆轮胎品牌";
    self.luntaiTF.textColor = FWTextColor_222222;
    self.luntaiTF.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.fifthView addSubview:self.luntaiTF];
    [self.luntaiTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.luntaiLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.luntaiLabel.mas_bottom).mas_offset(0);
    }];
    
    self.luntaiPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.luntaiPickerView.delegate=self;
    self.luntaiPickerView.dataSource=self;
    self.luntaiPickerView.backgroundColor=[UIColor clearColor];
    
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    UIBarButtonItem *doneButton = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneLuntaiButtonClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar.items = @[cancelButton,flexItem, fixItem, doneButton];
    
    self.luntaiTF.inputView = self.luntaiPickerView;
    self.luntaiTF.inputAccessoryView = bar;
    
    self.luntaiArrowImageView = [[UIImageView alloc] init];
    self.luntaiArrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.luntaiTF addSubview:self.luntaiArrowImageView];
    [self.luntaiArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.centerY.mas_equalTo(self.luntaiTF);
        make.right.mas_equalTo(self.luntaiTF).mas_offset(-10);
    }];
    
    self.luntaiLineView =  [[UIView alloc] init];
    self.luntaiLineView.backgroundColor = FWTextColor_141414;
    [self.fifthView addSubview:self.luntaiLineView];
    [self.luntaiLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.luntaiLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.luntaiTF);
        make.top.mas_equalTo(self.luntaiTF.mas_bottom).mas_offset(0);
    }];
    
    
    self.jiyouLabel = [[UILabel alloc] init];
    self.jiyouLabel.text = @"机油";
    self.jiyouLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.jiyouLabel.textColor = FWTextColor_222222;
    self.jiyouLabel.textAlignment = NSTextAlignmentLeft;
    [self.fifthView addSubview:self.jiyouLabel];
    [self.jiyouLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.luntaiLabel);
        make.size.mas_equalTo(self.luntaiLabel);
        make.top.mas_equalTo(self.luntaiLineView.mas_bottom).mas_offset(15);
    }];
    
    self.jiyouTF = [[UITextField alloc] init];
    self.jiyouTF.delegate = self;
    self.jiyouTF.placeholder = @"请选择参赛车辆机油品牌";
    self.jiyouTF.textColor = FWTextColor_222222;
    self.jiyouTF.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.fifthView addSubview:self.jiyouTF];
    [self.jiyouTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.jiyouLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.jiyouLabel.mas_bottom).mas_offset(0);
    }];
    
    self.jiyouPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.jiyouPickerView.delegate=self;
    self.jiyouPickerView.dataSource=self;
    self.jiyouPickerView.backgroundColor=[UIColor clearColor];
    
    UIToolbar *jiyouBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    UIBarButtonItem *jiyouDoneButton = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneJiyouButtonClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *jiyouCancelButton = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem * jiyoufixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    jiyoufixItem.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *jiyouflexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    jiyouBar.items = @[jiyouCancelButton,jiyouflexItem, jiyoufixItem, jiyouDoneButton];
    
    self.jiyouTF.inputView = self.jiyouPickerView;
    self.jiyouTF.inputAccessoryView = jiyouBar;
    
    self.jiyouArrowImageView = [[UIImageView alloc] init];
    self.jiyouArrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.jiyouTF addSubview:self.jiyouArrowImageView];
    [self.jiyouArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.centerY.mas_equalTo(self.jiyouTF);
        make.right.mas_equalTo(self.jiyouTF).mas_offset(-10);
    }];
    
    self.jiyouLineView =  [[UIView alloc] init];
    self.jiyouLineView.backgroundColor = FWTextColor_141414;
    [self.fifthView addSubview:self.jiyouLineView];
    [self.jiyouLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.jiyouLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.jiyouTF);
        make.top.mas_equalTo(self.jiyouTF.mas_bottom).mas_offset(0);
        make.bottom.mas_equalTo(self.fifthView.mas_bottom).mas_offset(-20);
    }];
}

- (void)setupSixthSubviews{
    
    self.sixthView = [[UIView alloc] init];
    self.sixthView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.sixthView];
    [self.sixthView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.fifthView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(193);
    }];
    
    self.gaizhuangLabel = [[UILabel alloc] init];
    self.gaizhuangLabel.text = @"改装清单(*选填)";
    self.gaizhuangLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.gaizhuangLabel.textColor = FWTextColor_222222;
    self.gaizhuangLabel.textAlignment = NSTextAlignmentLeft;
    [self.sixthView addSubview:self.gaizhuangLabel];
    [self.gaizhuangLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.sixthView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(200, 20));
        make.top.mas_equalTo(self.sixthView).mas_offset(20);
    }];
    
    self.gaizhuangTV = [[IQTextView alloc] init];
    self.gaizhuangTV.layer.cornerRadius = 4;
    self.gaizhuangTV.layer.masksToBounds = YES;
    self.gaizhuangTV.layer.borderColor = FWColor(@"616068").CGColor;
    self.gaizhuangTV.layer.borderWidth = 1;
    self.gaizhuangTV.placeholder = @"请输入改装清单";
    self.gaizhuangTV.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.gaizhuangTV.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.gaizhuangTV.placeholderTextColor = FWTextColor_BCBCBC;
    self.gaizhuangTV.textContainerInset = UIEdgeInsetsMake(5,5,5,5);
    [self.sixthView addSubview:self.gaizhuangTV];
    [self.gaizhuangTV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.gaizhuangLabel);
        make.top.mas_equalTo(self.gaizhuangLabel.mas_bottom).mas_offset(20);
        make.right.mas_equalTo(self.sixthView).mas_offset(-14);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_greaterThanOrEqualTo(120);
    }];
    
    self.gaizhuangSumLabel = [[UILabel alloc] init];
    self.gaizhuangSumLabel.text = @"改装总费用（单位“万元”）";
    self.gaizhuangSumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.gaizhuangSumLabel.textColor = FWTextColor_222222;
    self.gaizhuangSumLabel.textAlignment = NSTextAlignmentLeft;
    [self.sixthView addSubview:self.gaizhuangSumLabel];
    [self.gaizhuangSumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.gaizhuangLabel);
        make.size.mas_equalTo(self.gaizhuangLabel);
        make.top.mas_equalTo(self.gaizhuangTV.mas_bottom).mas_offset(15);
    }];
    
    self.gaizhuangSumTF = [[IQTextView alloc] init];
    self.gaizhuangSumTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.gaizhuangSumTF.placeholder = @"请输入改装费用，单位“万元”";
    self.gaizhuangSumTF.placeholderTextColor = FWTextColor_BCBCBC;
    self.gaizhuangSumTF.textColor = FWTextColor_222222;
    self.gaizhuangSumTF.keyboardType = UIKeyboardTypeNumberPad;
    self.gaizhuangSumTF.textContainerInset = UIEdgeInsetsMake(5,-5,0,0);
    [self.sixthView addSubview:self.gaizhuangSumTF];
    [self.gaizhuangSumTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.gaizhuangSumLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.gaizhuangSumLabel.mas_bottom).mas_offset(0);
    }];
    
    self.gaizhuangSumLineView =  [[UIView alloc] init];
    self.gaizhuangSumLineView.backgroundColor = FWTextColor_141414;
    [self.sixthView addSubview:self.gaizhuangSumLineView];
    [self.gaizhuangSumLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.gaizhuangSumLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.gaizhuangSumTF);
        make.top.mas_equalTo(self.gaizhuangSumTF.mas_bottom).mas_offset(0);
        make.bottom.mas_equalTo(self.sixthView).mas_offset(-20);
    }];
}

- (void)setupSeventhSubviews{
    
    self.seventhView = [[UIView alloc] init];
    self.seventhView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.seventhView];
    [self.seventhView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.sixthView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(107);
    }];
    
    self.zubieLabel = [[UILabel alloc] init];
    self.zubieLabel.text = @"所选组别";
    self.zubieLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.zubieLabel.textColor = FWTextColor_222222;
    self.zubieLabel.textAlignment = NSTextAlignmentLeft;
    [self.seventhView addSubview:self.zubieLabel];
    [self.zubieLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.seventhView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.top.mas_equalTo(self.seventhView).mas_offset(20);
    }];
    
    self.zubieTF = [[UITextField alloc] init];
    self.zubieTF.delegate = self;
    self.zubieTF.placeholder = @"请选择参赛组别";
    self.zubieTF.textColor = FWTextColor_222222;
    self.zubieTF.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.seventhView addSubview:self.zubieTF];
    [self.zubieTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.zubieLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 38));
        make.top.mas_equalTo(self.zubieLabel.mas_bottom).mas_offset(0);
    }];
    
    self.zubiePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.zubiePickerView.delegate=self;
    self.zubiePickerView.dataSource=self;
    self.zubiePickerView.backgroundColor=[UIColor clearColor];
    
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    UIBarButtonItem *doneButton = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneZubieButtonClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar.items = @[cancelButton,flexItem, fixItem, doneButton];
    
    self.zubieTF.inputView = self.zubiePickerView;
    self.zubieTF.inputAccessoryView = bar;
    
    self.lookButton = [[UIButton alloc] init];
    self.lookButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.lookButton setTitle:@"查看组别介绍" forState:UIControlStateNormal];
    [self.lookButton setImage:[UIImage imageNamed:@"perfect_arrow"] forState:UIControlStateNormal];
    [self.lookButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    CGFloat leftlab = [@"查看组别介绍" sizeWithAttributes:@{NSFontAttributeName : self.lookButton.titleLabel.font}].width;
    [self.lookButton setImageEdgeInsets:UIEdgeInsetsMake(0, leftlab+10, 0, 0)];
    [self.lookButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    [self.lookButton addTarget:self action:@selector(lookButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.seventhView addSubview:self.lookButton];
    [self.lookButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.zubieLabel);
        make.right.mas_equalTo(self.seventhView).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(90, 30));
    }];
    
    self.zubieArrowImageView = [[UIImageView alloc] init];
    self.zubieArrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.zubieTF addSubview:self.zubieArrowImageView];
    [self.zubieArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.centerY.mas_equalTo(self.zubieTF);
        make.right.mas_equalTo(self.zubieTF).mas_offset(-10);
    }];
    
    self.zubieLineView =  [[UIView alloc] init];
    self.zubieLineView.backgroundColor = FWTextColor_141414;
    [self.seventhView addSubview:self.zubieLineView];
    [self.zubieLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.zubieLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.zubieTF);
        make.top.mas_equalTo(self.zubieTF.mas_bottom).mas_offset(0);
    }];

    self.adShadowBgView = [[UIView alloc] init];
    self.adShadowBgView.userInteractionEnabled = YES;
    self.adShadowBgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.adShadowBgView.userInteractionEnabled = YES;
    self.adShadowBgView.layer.cornerRadius = 4;
    self.adShadowBgView.layer.shadowOffset = CGSizeMake(0, 0);
    self.adShadowBgView.layer.shadowOpacity = 0.7;
    self.adShadowBgView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.seventhView addSubview:self.adShadowBgView];
    [self.adShadowBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.zubieTF.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.seventhView).mas_offset(10);
        make.right.mas_equalTo(self.seventhView).mas_offset(-10);
        make.height.mas_greaterThanOrEqualTo(100);
        make.width.mas_equalTo(SCREEN_WIDTH-20);
    }];
    
    self.adBgView = [[UIView alloc] init];
    self.adBgView.layer.cornerRadius = 2;
    self.adBgView.layer.masksToBounds = YES;
    [self.adShadowBgView addSubview:self.adBgView];
    [self.adBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.adShadowBgView);
    }];
    
    self.adPlanButton = [[UIButton alloc] init];
    [self.adPlanButton setImage:[UIImage imageNamed:@"select_ad_plan"] forState:UIControlStateSelected];
    [self.adPlanButton setImage:[UIImage imageNamed:@"unselect_ad_plan"] forState:UIControlStateNormal];
    [self.adPlanButton addTarget:self action:@selector(adPlanButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.adBgView addSubview:self.adPlanButton];
    [self.adPlanButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.adBgView).mas_offset(3);
        make.top.mas_equalTo(self.adBgView).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(35, 35));
    }];
    
    self.adPlanTitleLabel = [[UILabel alloc] init];
    self.adPlanTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 13];
    self.adPlanTitleLabel.textColor = FWTextColor_222222;
    self.adPlanTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.adBgView addSubview:self.adPlanTitleLabel];
    [self.adPlanTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.adPlanButton.mas_right).mas_offset(-3);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(self.adPlanButton);
    }];
    
    self.adPlanTitleLabel2 = [[UILabel alloc] init];
    self.adPlanTitleLabel2.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    self.adPlanTitleLabel2.textColor = FWColor(@"ff6f00");
    self.adPlanTitleLabel2.textAlignment = NSTextAlignmentLeft;
    [self.adBgView addSubview:self.adPlanTitleLabel2];
    [self.adPlanTitleLabel2 mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.adPlanTitleLabel.mas_right).mas_offset(2);
        make.height.mas_equalTo(20);
//        make.right.mas_equalTo(self.adBgView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(self.adPlanTitleLabel).mas_offset(-0.5);
    }];
    
    self.adPlanDescLabel = [[UILabel alloc] init];
    self.adPlanDescLabel.numberOfLines = 0;
    self.adPlanDescLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.adPlanDescLabel.textColor = FWTextColor_BCBCBC;
    self.adPlanDescLabel.textAlignment = NSTextAlignmentLeft;
    [self.adBgView addSubview:self.adPlanDescLabel];
    [self.adPlanDescLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.adBgView).mas_offset(14);
        make.right.mas_equalTo(self.adBgView).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_greaterThanOrEqualTo(20);
        make.top.mas_equalTo(self.adPlanTitleLabel.mas_bottom).mas_offset(10);
    }];
    

    self.headImageView = [[UIImageView alloc] init];
    self.headImageView.tag = 22224;
    self.headImageView.userInteractionEnabled = YES;
    self.headImageView.layer.cornerRadius = 4;
    self.headImageView.layer.masksToBounds = YES;
    self.headImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.headImageView.image = [UIImage imageNamed:@"car_head"];
    [self.adBgView addSubview:self.headImageView];
    [self.headImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.adPlanDescLabel.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.adBgView).mas_offset(14);
        make.width.mas_equalTo(((SCREEN_WIDTH-20)-45)/3);
        make.height.mas_equalTo(77*(((SCREEN_WIDTH-20)-45)/3)/102);
    }];
    
    self.headReUploadLabel = [[UILabel alloc] init];
    self.headReUploadLabel.text = @"重新上传";
    self.headReUploadLabel.layer.cornerRadius = 4;
    self.headReUploadLabel.layer.masksToBounds = YES;
    self.headReUploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.headReUploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.headReUploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.headReUploadLabel.textAlignment = NSTextAlignmentCenter;
    [self.headImageView addSubview:self.headReUploadLabel];
    [self.headReUploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.headImageView);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(self.headImageView);
        make.bottom.mas_equalTo(self.headImageView).mas_offset(0);
    }];
    self.headReUploadLabel.hidden = YES;
    
    self.headLabel = [[UILabel alloc] init];
    self.headLabel.text = @"车头部";
    self.headLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.headLabel.textColor = FWTextColor_BCBCBC;
    self.headLabel.textAlignment = NSTextAlignmentCenter;
    [self.adBgView addSubview:self.headLabel];
    [self.headLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.headImageView);
        make.width.mas_equalTo(50);
        make.height.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.headImageView.mas_bottom).mas_offset(5);
        make.bottom.mas_equalTo(self.adBgView).mas_offset(-10);
    }];
    
    self.footImageView = [[UIImageView alloc] init];
    self.footImageView.tag = 22225;
    self.footImageView.userInteractionEnabled = YES;
    self.footImageView.layer.cornerRadius = 4;
    self.footImageView.layer.masksToBounds = YES;
    self.footImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.footImageView.image = [UIImage imageNamed:@"car_foot"];
    [self.adBgView addSubview:self.footImageView];
    [self.footImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImageView);
        make.left.mas_equalTo(self.headImageView.mas_right).mas_offset(7);
        make.width.mas_equalTo(self.headImageView);
        make.height.mas_equalTo(self.headImageView);
    }];
    
    self.footReUploadLabel = [[UILabel alloc] init];
    self.footReUploadLabel.text = @"重新上传";
    self.footReUploadLabel.layer.cornerRadius = 4;
    self.footReUploadLabel.layer.masksToBounds = YES;
    self.footReUploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.footReUploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.footReUploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.footReUploadLabel.textAlignment = NSTextAlignmentCenter;
    [self.footImageView addSubview:self.footReUploadLabel];
    [self.footReUploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.footImageView);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(self.footImageView);
        make.bottom.mas_equalTo(self.footImageView).mas_offset(0);
    }];
    self.footReUploadLabel.hidden = YES;
    
    self.footLabel = [[UILabel alloc] init];
    self.footLabel.text = @"车尾部";
    self.footLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.footLabel.textColor = FWTextColor_BCBCBC;
    self.footLabel.textAlignment = NSTextAlignmentCenter;
    [self.adBgView addSubview:self.footLabel];
    [self.footLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.footImageView);
        make.width.mas_equalTo(self.headLabel);
        make.height.mas_equalTo(self.headLabel);
        make.centerY.mas_equalTo(self.headLabel);
    }];
    
    self.bodyImageView = [[UIImageView alloc] init];
    self.bodyImageView.userInteractionEnabled = YES;
    self.bodyImageView.layer.cornerRadius = 2;
    self.bodyImageView.layer.masksToBounds = YES;
    self.bodyImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bodyImageView.tag = 22226;
    self.bodyImageView.image = [UIImage imageNamed:@"car_body"];
    [self.adBgView addSubview:self.bodyImageView];
    [self.bodyImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headImageView);
        make.left.mas_equalTo(self.footImageView.mas_right).mas_offset(7);
        make.width.mas_equalTo(self.headImageView);
        make.height.mas_equalTo(self.headImageView);
    }];
    
    self.bodyReUploadLabel = [[UILabel alloc] init];
    self.bodyReUploadLabel.text = @"重新上传";
    self.bodyReUploadLabel.layer.cornerRadius = 2;
    self.bodyReUploadLabel.layer.masksToBounds = YES;
    self.bodyReUploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.bodyReUploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.bodyReUploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.bodyReUploadLabel.textAlignment = NSTextAlignmentCenter;
    [self.bodyImageView addSubview:self.bodyReUploadLabel];
    [self.bodyReUploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.bodyImageView);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(self.bodyImageView);
        make.bottom.mas_equalTo(self.bodyImageView).mas_offset(0);
    }];
    self.bodyReUploadLabel.hidden = YES;
    
    self.bodyLabel = [[UILabel alloc] init];
    self.bodyLabel.text = @"主驾驶侧";
    self.bodyLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.bodyLabel.textColor = FWTextColor_BCBCBC;
    self.bodyLabel.textAlignment = NSTextAlignmentCenter;
    [self.adBgView addSubview:self.bodyLabel];
    [self.bodyLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.bodyImageView);
        make.width.mas_equalTo(self.headLabel);
        make.height.mas_equalTo(self.headLabel);
        make.centerY.mas_equalTo(self.headLabel);
    }];
    
    [self.headImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType:)]];
    [self.footImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType:)]];
    [self.bodyImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType:)]];
    
    
    self.announceLabel = [[UILabel alloc] init];
    self.announceLabel.numberOfLines = 0;
    self.announceLabel.text = @"*请务必填写真实有效信息，便于我们核对车辆及比赛成绩发布!";
    self.announceLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.announceLabel.textColor = FWTextColor_BCBCBC;
    self.announceLabel.textAlignment = NSTextAlignmentLeft;
    [self.seventhView addSubview:self.announceLabel];
    [self.announceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.adShadowBgView);
        make.width.mas_equalTo(self.adShadowBgView);
        make.height.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.adShadowBgView.mas_bottom).mas_offset(20);
        make.bottom.mas_equalTo(self.seventhView).mas_offset(-20);
    }];

    self.saveButton = [[UIButton alloc] init];
    self.saveButton.layer.cornerRadius = 2;
    self.saveButton.layer.masksToBounds = YES;
    self.saveButton.backgroundColor = FWTextColor_222222;
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [self.saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [self.saveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.saveButton addTarget:self action:@selector(saveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.saveButton];
    [self.saveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.seventhView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self).mas_offset(30);
        make.right.mas_equalTo(self).mas_offset(-30);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(SCREEN_WIDTH-60);
        make.bottom.mas_equalTo(self).mas_offset(-125);
    }];
}

@end
