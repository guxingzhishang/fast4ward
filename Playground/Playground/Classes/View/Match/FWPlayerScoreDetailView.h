//
//  FWPlayerScoreDetailView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWPlayerRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerScoreDetailView : UIScrollView<UIScrollViewDelegate>

@property (nonatomic, strong) UIImageView * topBgImageView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIButton * historyScoreButton;

@property (nonatomic, strong) UIView * matchView;
@property (nonatomic, strong) UILabel * matchNameLabel;
@property (nonatomic, strong) UILabel * groupNameLabel;
@property (nonatomic, strong) UILabel * carLabel;
@property (nonatomic, strong) UIImageView * typeImageView;

@property (nonatomic, strong) UIView * scoreView;

@property (nonatomic, strong) UILabel * gaizhuangLabel;
@property (nonatomic, strong) UILabel * gaizhuangDetailLabel;

@property (nonatomic, strong) UILabel * matchVideoLabel;
@property (nonatomic, strong) UIView * videoView;

@property (nonatomic, strong) UILabel * picsLabel;
@property (nonatomic, strong) UIView * picsView;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWScoreDetailModel * detailModel;

- (void)configForView:(FWScoreDetailModel *)model;
@end

NS_ASSUME_NONNULL_END
