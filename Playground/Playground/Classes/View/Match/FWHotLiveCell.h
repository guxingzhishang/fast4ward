//
//  FWHotLiveCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWLiveModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^LeftButtonBlock)(void);
typedef void(^RightButtonBlock)(void);

@interface FWHotLiveCell : UITableViewCell

@property (nonatomic, strong) UIView * leftView;
@property (nonatomic, strong) UIView * rightView;

@property (nonatomic, strong) UIImageView      * leftImageView;
@property (nonatomic, strong) UIImageView      * rightImageView;

@property (nonatomic, strong) UILabel          * leftTitleLable;
@property (nonatomic, strong) UILabel          * rightTitleLable;

@property (nonatomic, strong) UIImageView      * leftStatusImageView;
@property (nonatomic, strong) UIImageView      * rightStatusImageView;

@property (nonatomic, strong) UILabel      * leftScanLabel;
@property (nonatomic, strong) UILabel      * rightScanLabel;

@property (nonatomic, copy  ) LeftButtonBlock  leftButtonBlock;
@property (nonatomic, copy  ) RightButtonBlock rightButtonBlock;

@property (nonatomic, strong) FWLiveListModel * leftListModel;
@property (nonatomic, strong) FWLiveListModel * rightListModel;


- (void)configForLeftView:(id)model;
- (void)configForRightView:(id)model;

@end

NS_ASSUME_NONNULL_END
