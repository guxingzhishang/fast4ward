//
//  FWRegistrationHomeCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRegistrationHomeCell.h"
#import "FWRegistrationHomeModel.h"

@implementation FWRegistrationHomeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor= FWViewBackgroundColor_FFFFFF;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_equalTo(148);
        make.bottom.mas_equalTo(self.contentView).mas_offset(0);
    }];
    
    self.coverImageView = [[UIImageView alloc] init];
    self.coverImageView.layer.cornerRadius = 2;
    self.coverImageView.layer.masksToBounds = YES;
    self.coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.coverImageView.image = [UIImage imageNamed:@"placeholder"];
    [self.bgView addSubview:self.coverImageView];
    [self.coverImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(self.bgView).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(111, 148));
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.coverImageView.mas_right).mas_offset(14);
        make.right.mas_equalTo(self.bgView);
        make.top.mas_equalTo(self.bgView);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.statusLabel = [[UILabel alloc] init];
    self.statusLabel.layer.borderColor = FWClearColor.CGColor;
    self.statusLabel.layer.borderWidth = 1;
    self.statusLabel.layer.cornerRadius = 1;
    self.statusLabel.layer.masksToBounds = YES;
    self.statusLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.statusLabel];
    [self.statusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(62);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(10);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.layer.borderColor = FWColor(@"A8ACB3").CGColor;
    self.nameLabel.layer.borderWidth = 1;
    self.nameLabel.layer.cornerRadius = 1;
    self.nameLabel.layer.masksToBounds = YES;
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.textColor = FWColor(@"A8ACB3");
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.statusLabel.mas_right).mas_offset(5);
        make.height.mas_equalTo(self.statusLabel);
        make.width.mas_greaterThanOrEqualTo(20);
        make.centerY.mas_equalTo(self.statusLabel);
    }];
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.textColor = FWColor(@"A8ACB3");
    self.timeLabel.numberOfLines = 0;
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.bgView);
        make.top.mas_equalTo(self.statusLabel.mas_bottom).mas_offset(30);
    }];
    
    self.areaLabel = [[UILabel alloc] init];
    self.areaLabel.textColor = FWColor(@"A8ACB3");
    self.areaLabel.numberOfLines = 0;
    self.areaLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.areaLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.areaLabel];
    [self.areaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(0);
        make.right.mas_equalTo(self.bgView);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
}

- (void)configForCell:(id)model{
    
    FWRegistrationHomeListModel * listModel = (FWRegistrationHomeListModel *)model;
    
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:listModel.sport_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.coverImageView.clipsToBounds = YES;
   
    if ([listModel.baoming_status isEqualToString:@"1"]) {
        /* 未开始 */
        self.statusLabel.text = @"报名未开始";
        self.statusLabel.textColor = FWColor(@"FDB344");
        self.statusLabel.layer.borderColor = FWColor(@"FDB344").CGColor;
    }else if ([listModel.baoming_status isEqualToString:@"2"]) {
        /* 进行中*/
        self.statusLabel.text = @"正在报名中";
        self.statusLabel.textColor = FWColor(@"13C16D");
        self.statusLabel.layer.borderColor = FWColor(@"13C16D").CGColor;
    }else if ([listModel.baoming_status isEqualToString:@"3"]) {
        /* 已结束 */
        self.statusLabel.text = @"报名已结束";
        self.statusLabel.textColor = FWColor(@"ff6f00");
        self.statusLabel.layer.borderColor = FWColor(@"ff6f00").CGColor;
    }
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:listModel.sport_name];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentJustified; //设置两端对齐显示
    [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedStr.length)];
    [attributedStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Medium" size: 16] range:NSMakeRange(0, attributedStr.length)];
    
    self.titleLabel.attributedText = attributedStr;
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@%@",listModel.sport_type_name,@"   "];
    self.timeLabel.text = [NSString stringWithFormat:@"比赛时间：%@",listModel.sport_date];
    self.areaLabel.text = [NSString stringWithFormat:@"地点：%@",listModel.sport_area];
}

@end
