//
//  FWPlayerRankCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 车手排名
 */
#import <UIKit/UIKit.h>
#import "FWPlayerRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerRankCell : UICollectionViewCell

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * ETLabel;

- (void)configForCell:(id)model;
@end

NS_ASSUME_NONNULL_END
