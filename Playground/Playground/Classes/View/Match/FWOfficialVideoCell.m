//
//  FWOfficialVideoCell.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/9.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWOfficialVideoCell.h"
#import "FWOfficialVideoModel.h"

@interface FWOfficialVideoCell()

@property (nonatomic, strong) FWVodListModel * listModel;

@end

@implementation FWOfficialVideoCell
@synthesize iconImageView;
@synthesize videoIconImageView;
@synthesize contentLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    iconImageView = [[UIImageView alloc] init];
    iconImageView.clipsToBounds = YES;
    iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    iconImageView.layer.cornerRadius = 3;
    iconImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:iconImageView];
    [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-20, 200));
        make.bottom.mas_equalTo(self.contentView).mas_offset(-10);
    }];
    
    videoIconImageView = [[UIImageView alloc] init];
    videoIconImageView.image = [UIImage imageNamed:@"match_video_play"];
    [iconImageView addSubview:videoIconImageView];
    [videoIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(44, 44));
        make.centerY.centerX.mas_equalTo(iconImageView);
    }];
    
    contentLabel = [[UILabel alloc] init];
    contentLabel.font = DHBoldFont(15);
    contentLabel.textColor = FWViewBackgroundColor_FFFFFF;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    [iconImageView addSubview:contentLabel];
    [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(iconImageView).mas_offset(10);
        make.right.mas_equalTo(iconImageView).mas_offset(-5);
        make.height.mas_equalTo(20);
        make.bottom.mas_equalTo(iconImageView).mas_offset(-12);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
}

- (void)configForCell:(id)model{
    
    self.listModel = (FWVodListModel *)model;
    
    [iconImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.feed_cover] placeholderImage:[UIImage imageNamed:@"placehoder"]];
    contentLabel.text = self.listModel.feed_title;
}

@end
