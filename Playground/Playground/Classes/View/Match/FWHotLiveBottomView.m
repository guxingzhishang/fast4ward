//
//  FWHotLiveBottomView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHotLiveBottomView.h"

@implementation FWHotLiveBottomView

- (instancetype)init {
    self = [super init];
    if(self) {
        self.frame = SCREEN_FRAME;
        self.backgroundColor = ColorClear;
        self.userInteractionEnabled = YES;
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGuesture:)]];
        
        if (SCREEN_HEIGHT >= 812) {
            _commentViewBgHeight = 40;
        }else{
            _commentViewBgHeight = 20;
        }
        _container = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 50 - _commentViewBgHeight, SCREEN_WIDTH, 65 + _commentViewBgHeight)];
        _container.backgroundColor = ColorClear;
        [self addSubview:_container];
        
        _keyboardHeight = _commentViewBgHeight;
        
        _textView = [[IQTextView alloc] initWithFrame:CGRectMake(15, 12, SCREEN_WIDTH-36-14-30, 36)];
        _textView.placeholder = @"说点什么…";
        _textView.textColor = FWViewBackgroundColor_FFFFFF;
        _textView.font = SmallFont;
        _textView.delegate = self;
        _textView.returnKeyType = UIReturnKeySend;
        _textView.layer.masksToBounds = YES;
        _textView.textContainerInset = UIEdgeInsetsMake(10, LEFT_INSET, 0,LEFT_INSET);
        _textHeight = ceilf(_textView.font.lineHeight);
        [_textView setBackgroundColor:FWColorWihtAlpha(@"000000", 0.4)];//透明效果
        _textView.layer.borderColor = [UIColor clearColor].CGColor;//边框颜色
        
        [_container addSubview:_textView];
        
        
        _shareButton = [[UIButton alloc] init];
        _shareButton.layer.cornerRadius = 18;
        _shareButton.layer.masksToBounds = YES;
        [_shareButton setImage:[UIImage imageNamed:@"hot_live_share"] forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(shareButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [_container addSubview:_shareButton];
        [_shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.container).mas_offset(-14);
            make.size.mas_equalTo(CGSizeMake(36, 36));
            make.centerY.mas_equalTo(self.textView);
        }];
    }
    return self;
}


//handle guesture tap
- (void)handleGuesture:(UITapGestureRecognizer *)sender {
    CGPoint point = [sender locationInView:_textView];
    if(![_textView.layer containsPoint:point]) {
        [_textView resignFirstResponder];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    if(hitView == self){
        if(hitView.backgroundColor == ColorClear) {
            return nil;
        }
    }
    return hitView;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - > 分享
- (void)shareButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(shareMatchPost)]) {
        [self.delegate shareMatchPost];
    }
}

@end
