//
//  FWPerfectPhotoAddImage.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPerfectPhotoAddImage.h"

#define kMaxColumn 2 // 每行显示数量
#define deleImageWH 25 // 删除按钮的宽高
#define kAdeleImage @"publish_delete" // 删除按钮图片
#define kAddImage @"upload_ad_plan" // 添加按钮图片

#import "FWPhotoFilterViewController.h"

#import "TZImagePickerController.h"
#import "SZAddImage.h"
@interface FWPerfectPhotoAddImage()<TZImagePickerControllerDelegate>

// 标识被编辑的按钮 -1 为添加新的按钮
@property (nonatomic, assign) NSInteger editTag;

@property (nonatomic, assign) CGFloat  imageW;

@end

@implementation FWPerfectPhotoAddImage
@synthesize editTag;
@synthesize imageW;

- (void)setVc:(UIViewController *)vc{
    _vc = vc;
}

- (void)setShowImage:(NSMutableArray *)showImage{
    _showImage = showImage;
    editTag = -1;
    [self showPhotoWithArray:[self.showImage copy]];
}

- (NSMutableArray *)images{
    if (!_images) {
        _images = [[NSMutableArray alloc] init];
    }
    return _images;
}

- (id) init{
    self = [super init];
    if (self) {
        UIButton *btn = [self createButtonWithImage:kAddImage andSeletor:@selector(addNew:)];
        
        imageW = (SCREEN_WIDTH - 43)/2;
        
        [self addSubview:btn];
    }
    return self;
}

// 添加新的控件
- (void)addNew:(UIButton *)btn
{
    // 标识为添加一个新的图片
    editTag = -1;
    [self callImagePicker:btn];
}

// 修改旧的控件
- (void)changeOld:(UIButton *)btn
{
    // 标识为修改(tag为修改标识)
    editTag = btn.tag;
    [self callImagePicker:btn];
}

#pragma mark - >  调用图片选择器
- (void)callImagePicker:(UIButton *)btn{
    
    self.maxCount = 2;
     
    /* 只有btn 的tag为0时 才能调用相册,tag=0 是添加相片的按钮 */
    if (btn.tag == 0) {
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:(self.maxCount-self.images.count) delegate:self];
        imagePickerVc.allowPickingVideo = NO;
        imagePickerVc.allowTakePicture = NO;
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            
            [self showPhotoWithArray:photos];
            
            if ([self.delegate respondsToSelector:@selector(operationPicture)]) {
                [self.delegate operationPicture];
            }
        }];
        imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self.vc presentViewController:imagePickerVc animated:YES completion:nil];
    }else{
        [self.vc.navigationController pushViewController:self.vc animated:YES];
    }
}

- (void)showPhotoWithArray:(NSArray *)array{
    
    for (UIImage * image in array) {
        if (editTag == -1) {
            // 创建一个新的控件
            UIButton *btn = [self createButtonWithImage:image andSeletor:@selector(changeOld:)];
            [self insertSubview:btn atIndex:self.subviews.count - 1];
            [self.images addObject:image];
            if (self.subviews.count - 1 == self.maxCount) {
                [[self.subviews lastObject] setHidden:YES];
                
            }
        }
        else
        {
            // 根据tag修改需要编辑的控件
            UIButton *btn = (UIButton *)[self viewWithTag:editTag];
            int index = [self.images indexOfObject:[btn imageForState:UIControlStateNormal]];
            [self.images removeObjectAtIndex:index];
            [btn setImage:image forState:UIControlStateNormal];
            [self.images insertObject:image atIndex:index];
        }
    }
}

#pragma mark - >  根据图片名称或者图片创建一个新的显示控件
- (UIButton *)createButtonWithImage:(id)imageNameOrImage andSeletor : (SEL)selector
{
    UIImage *addImage = nil;
    if ([imageNameOrImage isKindOfClass:[NSString class]]) {
        addImage = [UIImage imageNamed:imageNameOrImage];
    }
    else if([imageNameOrImage isKindOfClass:[UIImage class]])
    {
        addImage = imageNameOrImage;
    }
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addBtn.layer.cornerRadius = 3;
    addBtn.layer.masksToBounds = YES;
    addBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [addBtn setImage:addImage forState:UIControlStateNormal];
    [addBtn addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    addBtn.tag = self.subviews.count;
    
    // 除了添加图片按钮外，都加上删除按钮
    if(addBtn.tag != 0)
    {
        UIButton *dele = [UIButton buttonWithType:UIButtonTypeCustom];
        dele.tag = 10086+addBtn.tag;
        dele.bounds = CGRectMake(0, 0, deleImageWH, deleImageWH);
        [dele setImage:[UIImage imageNamed:kAdeleImage] forState:UIControlStateNormal];
        [dele addTarget:self action:@selector(deletePic:) forControlEvents:UIControlEventTouchUpInside];
        //        dele.frame = CGRectMake(imageW - deleImageWH, 0, deleImageWH, deleImageWH);
        
        [addBtn addSubview:dele];
        [dele mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(addBtn);
            make.right.mas_equalTo(addBtn).mas_offset(1);
            make.size.mas_equalTo(CGSizeMake(21, 20));
        }];
        
        UILongPressGestureRecognizer *gester = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        [addBtn addGestureRecognizer:gester];
    }
    
    return addBtn;
}


#pragma mark - >  长按添加删除按钮
- (void)longPress : (UIGestureRecognizer *)gester
{
    if (gester.state == UIGestureRecognizerStateBegan)
    {
        
    }
}

#pragma mark - >  删除图片
- (void)deletePic : (UIButton *)btn
{
    if (self.images.count > 0) {
        for (int i = 1; i <= self.images.count; i++) {
            UIButton * button = [self viewWithTag:i];
            UIButton * deleteButton = [self viewWithTag:i+10086];
            
            if (i+10086>btn.tag){
                button.tag = i-1;
                deleteButton.tag = btn.tag-1;
            }
        }
    }
    
    [self.images removeObject:[(UIButton *)btn.superview imageForState:UIControlStateNormal]];
    
    
    [btn.superview removeFromSuperview];
    if ([[self.subviews lastObject] isHidden]) {
        [[self.subviews lastObject] setHidden:NO];
    }
    
    if ([self.delegate respondsToSelector:@selector(operationPicture)]) {
        [self.delegate operationPicture];
    }
}

#pragma mark - >  对所有子控件进行布局
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSInteger count = self.subviews.count;
    CGFloat btnW = imageW;
    CGFloat btnH = 126*imageW/166;
    
    for (int i = 0; i < count; i++) {
        UIButton *btn = self.subviews[i];
        CGFloat btnX = 14 + (imageW+15) * i ;
        CGFloat btnY = 5;
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

- (NSMutableArray *)getImagesArray{
    
    return self.images;
}

@end
