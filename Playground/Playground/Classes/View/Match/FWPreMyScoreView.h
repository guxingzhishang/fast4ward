//
//  FWPreMyScoreView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 排名 - > 预赛我的成绩
 */
#import <UIKit/UIKit.h>
#import "FWPreRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPreMyScoreView : UIView

/* 前三名的王冠 */
@property (nonatomic, strong) UIImageView * iconImageView;

@property (nonatomic, strong) UIImageView * photoImageView;

@property (nonatomic, strong) UILabel * groupLabel;

@property (nonatomic, strong) UILabel * currentStatusLabel;

@property (nonatomic, strong) UILabel * rankLabel;

@property (nonatomic, strong) UILabel * nameLabel;

@property (nonatomic, strong) UILabel * RTLabel;

@property (nonatomic, strong) UILabel * weisuLabel;

@property (nonatomic, strong) UILabel * ETLabel;

@property (nonatomic, strong) UILabel * carLabel;

@property (nonatomic, strong) FWPreRankModel * myScoreModel;

@property (nonatomic, weak) UIViewController * vc;

- (void)configForView:(id)model withMyCarNo:(NSString *)my_car_no;
@end

NS_ASSUME_NONNULL_END
