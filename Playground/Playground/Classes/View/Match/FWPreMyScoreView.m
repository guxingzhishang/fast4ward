//
//  FWPreMyScoreView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPreMyScoreView.h"

@implementation FWPreMyScoreView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];

        self.layer.shadowOffset = CGSizeMake(0, -0.5);
        self.layer.shadowOpacity = 0.7;
        self.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.groupLabel = [[UILabel alloc] init];
    self.groupLabel.font = DHSystemFontOfSize_12;
    self.groupLabel.textColor = FWTextColor_222222;
    self.groupLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.groupLabel];
    [self.groupLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(15);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self).mas_offset(14);
        make.top.mas_equalTo(self).mas_offset(5);
    }];
    
    
    self.currentStatusLabel = [[UILabel alloc] init];
    self.currentStatusLabel.font = DHSystemFontOfSize_12;
    self.currentStatusLabel.textColor = FWColor(@"9E9E9E");
    self.currentStatusLabel.layer.cornerRadius = 10;
    self.currentStatusLabel.layer.masksToBounds =YES;
    self.currentStatusLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.currentStatusLabel];
    [self.currentStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerX.mas_equalTo(self);
        make.centerY.mas_equalTo(self.groupLabel);
    }];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.image = FWImage(@"myscore_wu");
    [self addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).mas_offset(34);
        make.left.mas_equalTo(self).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    self.rankLabel = [[UILabel alloc] init];
    self.rankLabel.font = DHSystemFontOfSize_16;
    self.rankLabel.textColor = FWTextColor_222222;
    self.rankLabel.layer.cornerRadius = 10;
    self.rankLabel.layer.masksToBounds =YES;
    self.rankLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.rankLabel];
    [self.rankLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.left.mas_equalTo(self).mas_offset(15);
        make.top.mas_equalTo(self).mas_offset(34);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 20;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
    self.photoImageView.layer.borderWidth = 0.5;
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(14);
        make.centerY.mas_equalTo(self.iconImageView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    [self.photoImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewClick)]];

    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = DHSystemFontOfSize_14;
    self.nameLabel.textColor = FWColor(@"353B48");
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(50);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(12);
        make.top.mas_equalTo(self.photoImageView);
    }];
    
    self.RTLabel = [[UILabel alloc] init];
    self.RTLabel.font = DHSystemFontOfSize_12;
    self.RTLabel.textColor = FWTextColor_969696;
    self.RTLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.RTLabel];
    [self.RTLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_equalTo((SCREEN_WIDTH-112)/3);
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
    }];
    
    self.weisuLabel = [[UILabel alloc] init];
    self.weisuLabel.font = DHSystemFontOfSize_12;
    self.weisuLabel.textColor = FWTextColor_969696;
    self.weisuLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.weisuLabel];
    [self.weisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.RTLabel);
        make.left.mas_equalTo(self.RTLabel.mas_right);
        make.centerY.mas_equalTo(self.RTLabel);
    }];
    
    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = DHSystemFontOfSize_12;
    self.ETLabel.textColor = FWGradual_Orange_FF8C11;
    self.ETLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.weisuLabel.mas_right);
        make.height.mas_equalTo(self.weisuLabel);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.centerY.mas_equalTo(self.RTLabel);
    }];
    
    self.carLabel = [[UILabel alloc] init];
    self.carLabel.font = DHSystemFontOfSize_12;
    self.carLabel.textColor = FWTextColor_969696;
    self.carLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.carLabel];
    [self.carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.ETLabel);
        make.centerY.mas_equalTo(self.nameLabel);
        make.left.mas_equalTo(self.nameLabel.mas_right);
    }];
}

- (void)configForView:(id)model withMyCarNo:(NSString *)my_car_no{
    
    self.myScoreModel = (FWPreRankModel *)model;
    
   
    self.groupLabel.text = self.myScoreModel.my_player_info.group_name;
    
    if (my_car_no.length > 0 && self.myScoreModel.my_score) {
        /* 显示成绩 */
        self.currentStatusLabel.text = @"";
        
        if ([self.myScoreModel.my_score.sort_index intValue] > 0) {
            self.rankLabel.text = self.myScoreModel.my_score.sort_index;
            self.iconImageView.image = FWImage(@"");
        }else{
            self.rankLabel.text = @"";
            self.iconImageView.image = FWImage(@"myscore_wu");
        }
    }else{
        self.currentStatusLabel.text = @"当前暂无比赛成绩!";
        self.rankLabel.text = @"";
        self.iconImageView.image = FWImage(@"myscore_wu");
    }

    
    self.nameLabel.text = self.myScoreModel.my_player_info.player_name?[NSString stringWithFormat:@"%@ %@", self.myScoreModel.my_player_info.car_no,self.myScoreModel.my_player_info.player_name]:@"暂无车手";

    self.carLabel.text = self.myScoreModel.my_player_info.car_type;
    
    if ([self.myScoreModel.my_score.rt floatValue] > 0) {
        self.RTLabel.text = [NSString stringWithFormat:@"RT: %@",self.myScoreModel.my_score.rt];
    }else{
        self.RTLabel.text = @"RT: --";
    }
    
    if ([self.myScoreModel.my_score.vspeed floatValue] > 0) {
        self.weisuLabel.text = [NSString stringWithFormat:@"尾速: %@",self.myScoreModel.my_score.vspeed];
    }else{
        self.weisuLabel.text = @"尾速: --";
    }
    
    if ([self.myScoreModel.my_score.et floatValue] > 0) {
        self.ETLabel.text = [NSString stringWithFormat:@"ET: %@",self.myScoreModel.my_score.et];
    }else{
        self.ETLabel.text = @"ET: --";
    }
    
    if (![self.myScoreModel.my_score.score_status_str isEqualToString:@"正常"]) {
        self.ETLabel.text = self.myScoreModel.my_score.score_status_str;
    }
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.myScoreModel.my_player_info.header] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (error) {
            self.photoImageView.image = FWImage(@"placeholder_photo");
        }
    }];
}

#pragma mark - > 点击头像
- (void)photoImageViewClick{
    
    if (![self.myScoreModel.my_score.uid isEqualToString:@"0"]) {
        FWNewUserInfoViewController * UVC = [[FWNewUserInfoViewController alloc] init];
        UVC.user_id = self.myScoreModel.my_score.uid;
        [self.vc.navigationController pushViewController:UVC animated:YES];
    }
}
@end
