//
//  FWLiveTipView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/12/7.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWLiveTipView.h"

@implementation FWLiveTipView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.bgImageView = [[UIImageView alloc] init];
    self.bgImageView.userInteractionEnabled = YES;
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgImageView.clipsToBounds = YES;
    [self addSubview:self.bgImageView];
    self.bgImageView.frame = self.frame;
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"您正在使用移动网络，继续播放将消耗流量";
    self.tipLabel.font = DHSystemFontOfSize_14;
//    self.tipLabel.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.2];
    self.tipLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.tipLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgImageView addSubview:self.tipLabel];
    self.tipLabel.frame = CGRectMake(0, CGRectGetHeight(self.frame)/2-10, SCREEN_WIDTH, 20);
//    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.bgImageView);
//        make.centerX.mas_equalTo(self.bgImageView);
//        make.right.left.mas_equalTo(self.bgImageView);
//        make.height.mas_equalTo(20);
//    }];
    
    self.continueButton = [[UIButton alloc] init];
    self.continueButton.layer.cornerRadius = 2;
    self.continueButton.layer.masksToBounds = YES;
    self.continueButton.titleLabel.font = DHSystemFontOfSize_12;
    self.continueButton.backgroundColor = FWColorWihtAlpha(@"222222", 0.4);
    [self.continueButton setTitle:@"继续观看" forState:UIControlStateNormal];
    [self.continueButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.continueButton addTarget:self action:@selector(continueButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgImageView addSubview:self.continueButton];
//    [self.continueButton mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(70, 30));
//        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(5);
//        make.centerX.mas_equalTo(self.bgImageView);
//    }];
    self.continueButton.frame = CGRectMake(CGRectGetWidth(self.frame)/2-35, CGRectGetMaxY(self.tipLabel.frame)+5, 80, 33);

}

- (void)continueButtonClick{
    
    if ([self.delegate respondsToSelector:@selector(continueLiveMatch)]) {
        [self.delegate continueLiveMatch];
    }
}

@end
