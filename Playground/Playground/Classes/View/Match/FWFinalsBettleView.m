//
//  FWFinalsBettleView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/28.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWFinalsBettleView.h"

@implementation FWFinalsBettleView
@synthesize model;

- (id)init{
    self = [super init];
    if (self) {
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.containerView = [[UIView alloc] init];
    self.containerView.layer.cornerRadius = 4;
    self.containerView.layer.masksToBounds = YES;
    self.containerView.layer.borderWidth = 0.5;
    self.containerView.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
    [self addSubview:self.containerView];
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(27);
        make.size.mas_equalTo(CGSizeMake(182, 116));
    }];
    
    self.upImageView = [[UIImageView alloc] init];
    self.upImageView.layer.cornerRadius = 20;
    self.upImageView.layer.masksToBounds = YES;
    self.upImageView.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
    self.upImageView.layer.borderWidth = 0.5;
    self.upImageView.userInteractionEnabled = YES;
    self.upImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.upImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.containerView addSubview:self.upImageView];
    [self.upImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.containerView).mas_offset(12);
        make.left.mas_equalTo(self.containerView).mas_offset(16);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    [self.upImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(upImageViewClick)]];

    
    self.upNameLabel = [[UILabel alloc] init];
    self.upNameLabel.font = DHSystemFontOfSize_14;
    self.upNameLabel.textColor = FWTextColor_000000;
    self.upNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.containerView addSubview:self.upNameLabel];
    [self.upNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self).mas_offset(-28);
        make.width.mas_greaterThanOrEqualTo(50);
        make.left.mas_equalTo(self.upImageView.mas_right).mas_offset(5);
        make.top.mas_equalTo(self.upImageView);
    }];
    
    self.upTimeLabel = [[UILabel alloc] init];
    self.upTimeLabel.font = DHSystemFontOfSize_12;
    self.upTimeLabel.textColor = FWGradual_Orange_FF8C11;
    self.upTimeLabel.textAlignment = NSTextAlignmentLeft;
    [self.containerView addSubview:self.upTimeLabel];
    [self.upTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.upNameLabel);
        make.width.mas_greaterThanOrEqualTo(50);
        make.left.mas_equalTo(self.upNameLabel);
        make.top.mas_equalTo(self.upNameLabel.mas_bottom);
    }];
    
    
    self.downImageView = [[UIImageView alloc] init];
    self.downImageView.layer.cornerRadius = 20;
    self.downImageView.layer.masksToBounds = YES;
    self.downImageView.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
    self.downImageView.layer.borderWidth = 0.5;
    self.downImageView.userInteractionEnabled = YES;
    self.downImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.downImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.containerView addSubview:self.downImageView];
    [self.downImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.containerView).mas_offset(16);
        make.top.mas_equalTo(self.upImageView.mas_bottom).mas_offset(12);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    [self.downImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(downImageViewClick)]];

    self.downNameLabel = [[UILabel alloc] init];
    self.downNameLabel.font = DHSystemFontOfSize_14;
    self.downNameLabel.textColor = FWTextColor_000000;
    self.downNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.containerView addSubview:self.downNameLabel];
    [self.downNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.upNameLabel);
        make.width.mas_greaterThanOrEqualTo(50);
        make.left.mas_equalTo(self.downImageView.mas_right).mas_offset(5);
        make.top.mas_equalTo(self.downImageView);
    }];
    
    self.downTimeLabel = [[UILabel alloc] init];
    self.downTimeLabel.font = DHSystemFontOfSize_12;
    self.downTimeLabel.textColor = FWGradual_Orange_FF8C11;
    self.downTimeLabel.textAlignment = NSTextAlignmentLeft;
    [self.containerView addSubview:self.downTimeLabel];
    [self.downTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.downNameLabel);
        make.width.mas_greaterThanOrEqualTo(50);
        make.left.mas_equalTo(self.downNameLabel);
        make.top.mas_equalTo(self.downNameLabel.mas_bottom);
    }];

    self.henFowardView = [[UIView alloc] init];
    self.henFowardView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self addSubview:self.henFowardView];
    [self.henFowardView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25.9, 0.5));
        make.right.mas_equalTo(self.containerView.mas_left);
        make.centerY.mas_equalTo(self.containerView);
    }];
    
    self.henView = [[UIView alloc] init];
    self.henView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self addSubview:self.henView];
    [self.henView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29.5, 0.5));
        make.left.mas_equalTo(self.containerView.mas_right);
        make.centerY.mas_equalTo(self.containerView);
    }];
    
    
    self.vsImageView = [[UIImageView alloc] init];
    self.vsImageView.image = [UIImage imageNamed:@"rank_vs"];
    [self addSubview:self.vsImageView];
    [self.vsImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29, 29));
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(self.containerView).mas_offset(29/2);
    }];
    
    self.upVerView = [[UIView alloc] init];
    self.upVerView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self addSubview:self.upVerView];
    
    
    self.downVerView = [[UIView alloc] init];
    self.downVerView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self addSubview:self.downVerView];
  
    self.firstImageView = [[UIImageView alloc] init];
    self.firstImageView.image = FWImage(@"match_second");
    [self.containerView addSubview:self.firstImageView];
    [self.firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(18, 30));
        make.top.mas_equalTo(self.containerView);
        make.left.mas_equalTo(self.containerView).mas_offset(0);
    }];
    
    self.secondImageView = [[UIImageView alloc] init];
    self.secondImageView.image = FWImage(@"match_first");
    [self.containerView addSubview:self.secondImageView];
    [self.secondImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(18, 30));
        make.top.mas_equalTo(self.upTimeLabel.mas_bottom);
        make.left.mas_equalTo(self.containerView).mas_offset(0);
    }];
    
    self.firstImageView.hidden = YES;
    self.secondImageView.hidden = YES;
}

- (void)refreshData:(id)dataSource withIndex:(NSInteger)index WithTotal:(NSInteger)total{
    
    self.model = (FWFinalSubListModel *)dataSource;
    
    self.upNameLabel.text = model.score_list.car_1_info.player_name?[NSString stringWithFormat:@"%@ %@", model.score_list.car_1_info.car_no,model.score_list.car_1_info.player_name]:@"暂无车手";
    self.downNameLabel.text = model.score_list.car_2_info.player_name?[NSString stringWithFormat:@"%@ %@", model.score_list.car_2_info.car_no,model.score_list.car_2_info.player_name]:@"暂无车手";

    self.upTime = model.score_list.car_1_info.rtandet_time?model.score_list.car_1_info.rtandet_time:@"--";
    
    if ([model.score_list.car_1_info.match_status isEqualToString:@"2"]) {
        // 犯规
        self.upTimeLabel.text = model.score_list.car_1_info.match_text;
    }else if([self.upTime isEqualToString:@"--"]){
        self.upTimeLabel.text = self.upTime;
    }else{
        self.upTimeLabel.text = [NSString stringWithFormat:@"ET+RT: %@",self.upTime];
    }
    
    self.downTime = model.score_list.car_2_info.rtandet_time?model.score_list.car_2_info.rtandet_time:@"--";
    
    if ([model.score_list.car_2_info.match_status isEqualToString:@"2"]) {
        // 犯规
        self.downTimeLabel.text = model.score_list.car_2_info.match_text;
    }else if([self.downTime isEqualToString:@"--"]){
        self.downTimeLabel.text = self.downTime;
    }else{
        self.downTimeLabel.text = [NSString stringWithFormat:@"ET+RT: %@",self.downTime ];
    }
    
    [self.upImageView sd_setImageWithURL:[NSURL URLWithString:model.score_list.car_1_info.header] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    [self.downImageView sd_setImageWithURL:[NSURL URLWithString:model.score_list.car_2_info.header] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    /* 判断车手1、2谁是冠军 */
    if ([model.score_list.winner_car_no isEqualToString:model.score_list.car_1_info.car_no]) {
        
        self.firstImageView.image = FWImage(@"match_first");
        self.secondImageView.image = FWImage(@"match_second");
    }else if ([model.score_list.winner_car_no isEqualToString:model.score_list.car_2_info.car_no]){
        
        self.firstImageView.image = FWImage(@"match_second");
        self.secondImageView.image = FWImage(@"match_first");
    }
    
    [self refreshContrait:index WithTotal:total];
}

- (void)refreshContrait:(NSInteger)index  WithTotal:(NSInteger)total{
    
    [self.upVerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.henFowardView);
        make.left.mas_equalTo(self.henFowardView);
        make.height.mas_equalTo((10.5+116/2)*index);
    }];
    
    [self.downVerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(0.5);
        make.left.mas_equalTo(self.henFowardView);
        make.height.mas_equalTo(self.upVerView);
        make.top.mas_equalTo(self.henFowardView);
    }];
    
    if (index == 0) {
        self.henFowardView.hidden = YES;
    }else{
        self.henFowardView.hidden = NO;
    }
    
    if (index == total -1) {
        /* 最后一组不显示 对抗图标 */
        self.vsImageView.hidden = YES;
        self.henView.hidden = YES;
        
        if ([self.upTime floatValue] > 0) {
            self.firstImageView.hidden = NO;
            
            
        }
        
        if([self.downTime floatValue] > 0) {
            self.secondImageView.hidden = NO;
        }
    }else{
        self.vsImageView.hidden = NO;
        self.henView.hidden = NO;
    }
    
}


- (void)upImageViewClick{
    
    if (![model.score_list.car_1_info.uid isEqualToString:@"0"]) {
        FWNewUserInfoViewController * UVC = [[FWNewUserInfoViewController alloc] init];
        UVC.user_id = model.score_list.car_1_info.uid;
        [self.vc.navigationController pushViewController:UVC animated:YES];
    }
}

- (void)downImageViewClick{
    
    if (![model.score_list.car_2_info.uid isEqualToString:@"0"]) {
        FWNewUserInfoViewController * UVC = [[FWNewUserInfoViewController alloc] init];
        UVC.user_id = model.score_list.car_2_info.uid;
        [self.vc.navigationController pushViewController:UVC animated:YES];
    }
}

@end
