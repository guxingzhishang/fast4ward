//
//  FWPreRankCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/28.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 * 预赛排名
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWPreRankCell : UITableViewCell

/* 前三名的王冠 */
@property (nonatomic, strong) UIImageView * iconImageView;

@property (nonatomic, strong) UIImageView * photoImageView;

@property (nonatomic, strong) UIImageView * rankNumImageView;

@property (nonatomic, strong) UILabel * rankLabel;

@property (nonatomic, strong) UILabel * nameLabel;

@property (nonatomic, strong) UILabel * RTLabel;

@property (nonatomic, strong) UILabel * weisuLabel;

@property (nonatomic, strong) UILabel * ETLabel;

@property (nonatomic, strong) UILabel * carLabel;

@property (nonatomic, strong) FWPreRankListModel * listModel;

@property (nonatomic, weak) UIViewController * vc;


- (void)configForCell:(id)model WithIndexRow:(NSInteger)row;

@end

NS_ASSUME_NONNULL_END
