//
//  FWMatchReportCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchReportCell.h"

@implementation FWMatchReportCell
@synthesize titleLabel;
@synthesize timeLabel;
@synthesize picImageView;
@synthesize lineView;
@synthesize typeImageView;
@synthesize typeLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{

    picImageView = [UIImageView new];
    picImageView.layer.cornerRadius = 2;
    picImageView.layer.masksToBounds = YES;
    picImageView.image = [UIImage imageNamed:@"placeholder"];
    picImageView.contentMode = UIViewContentModeScaleAspectFill;
    picImageView.frame = CGRectMake(0, 11, 151, 92);
    [self.contentView addSubview:picImageView];
    [picImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(11);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(151, 92));
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    typeLabel = [[UILabel alloc] init];
    typeLabel.layer.cornerRadius = 2;
    typeLabel.layer.masksToBounds = YES;
    typeLabel.backgroundColor = FWViewBackgroundColor_EEEEEE;
    typeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    typeLabel.textColor = FWTextColor_515151;
    typeLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:typeLabel];
    [typeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(48, 21));
        make.bottom.mas_equalTo(picImageView);
        make.left.mas_equalTo(picImageView.mas_right).mas_offset(10);
    }];
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 2;
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(picImageView);
        make.left.mas_equalTo(picImageView.mas_right).mas_offset(13);
        make.right.mas_equalTo(self.contentView).mas_offset(-10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    timeLabel.textColor = FWTextColor_B6BCC4;
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(typeLabel);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];
}

- (void)configForCell:(FWF4WMatchNewsModel *)model{
    
    self.newsModel = (FWF4WMatchNewsModel *)model;
   
    titleLabel.text = self.newsModel.title;
    timeLabel.text = self.newsModel.pub_date;

    [picImageView sd_setImageWithURL:[NSURL URLWithString:self.newsModel.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    if ([self.newsModel.feed_info.feed_type isEqualToString:@"1"]) {
        typeLabel.text = @"视频";
    }else if([self.newsModel.feed_info.feed_type isEqualToString:@"2"]){
        typeLabel.text = @"图片";
    }else{
        typeLabel.text = @"文章";
    }
}

@end
