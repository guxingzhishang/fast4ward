//
//  FWPlayerArchivesCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerArchivesCell.h"
#import "FWPlayerArchivesModel.h"
#import "FWPlayerScoreDetailViewController.h"
#import "FWPlayerRankModel.h"

@interface FWPlayerArchivesCell()
@property (nonatomic, strong) FWScoreDetailModel * sportModel;
@end

@implementation FWPlayerArchivesCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.contentView.backgroundColor = FWTextColor_F8F8F8;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.verView = [[UIView alloc] init];
    self.verView.backgroundColor = FWTextColor_BCBCBC;
    [self.contentView addSubview:self.verView];
    [self.verView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.top.bottom.mas_equalTo(self.contentView);
        make.width.mas_equalTo(1.5);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.shadowView = [[UIView alloc]init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 2;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 5);
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.left.mas_equalTo(self.contentView).mas_offset(30);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(158);
        make.width.mas_equalTo(SCREEN_WIDTH-44);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-5);
    }];
    
    self.matchView = [[UIView alloc] init];
    self.matchView.userInteractionEnabled = YES;
    self.matchView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.matchView.layer.cornerRadius = 2;
    self.matchView.layer.masksToBounds = YES;
    [self.shadowView addSubview:self.matchView];
    [self.matchView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.typeImageView = [[UIImageView alloc] init];
    [self.matchView addSubview:self.typeImageView];
    [self.typeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(self.matchView).mas_offset(14);
       make.right.mas_equalTo(self.matchView).mas_offset(-24);
       make.size.mas_equalTo(CGSizeMake(28, 40));
    }];

    self.matchNameLabel = [[UILabel alloc] init];
    self.matchNameLabel.numberOfLines = 0;
    self.matchNameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.matchNameLabel.textColor = FWTextColor_222222;
    self.matchNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.matchNameLabel];
    [self.matchNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(20);
        make.left.mas_equalTo(self.matchView).mas_offset(14);
        make.top.mas_equalTo(self.matchView).mas_offset(13);
        make.right.mas_equalTo(self.typeImageView.mas_left).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.groupNameLabel = [[UILabel alloc] init];
    self.groupNameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    self.groupNameLabel.textColor = FWTextColor_222222;
    self.groupNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.groupNameLabel];
    [self.groupNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.matchNameLabel);
        make.top.mas_equalTo(self.matchNameLabel.mas_bottom).mas_offset(11);
        make.right.mas_equalTo(self.matchNameLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.carLabel = [[UILabel alloc] init];
    self.carLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.carLabel.textColor = FWTextColor_BCBCBC;
    self.carLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.carLabel];
    [self.carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.matchNameLabel);
        make.top.mas_equalTo(self.groupNameLabel.mas_bottom).mas_offset(12);
        make.right.mas_equalTo(self.matchView).mas_offset(-15);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.matchView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.matchNameLabel);
        make.right.mas_equalTo(self.matchView).mas_offset(-14);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.carLabel.mas_bottom).mas_offset(3);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"入榜成绩:";
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.tipLabel.textColor = FWColor(@"515151");
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.matchNameLabel);
        make.top.mas_equalTo(self.lineView.mas_bottom).mas_offset(3);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.detailLabel = [[UILabel alloc] init];
    self.detailLabel.text = @"查看";
    self.detailLabel.layer.cornerRadius = 2;
    self.detailLabel.layer.masksToBounds = YES;
    self.detailLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.detailLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.detailLabel.backgroundColor = FWTextColor_222222;
    self.detailLabel.textAlignment = NSTextAlignmentCenter;
    [self.matchView addSubview:self.detailLabel];
    [self.detailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.matchView).mas_offset(-15);
        make.right.mas_equalTo(self.matchView).mas_offset(-14);
        make.size.mas_equalTo(CGSizeMake(51, 23));
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(5);
    }];
    
    self.scoreView = [[UIView alloc] init];
    self.scoreView.layer.cornerRadius = 2;
    self.scoreView.layer.masksToBounds = YES;
    self.scoreView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.matchView addSubview:self.scoreView];
    [self.scoreView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.detailLabel);
        make.right.mas_equalTo(self.detailLabel.mas_left).mas_offset(-5);
        make.height.mas_equalTo(23);
        make.left.mas_equalTo(self.matchNameLabel);
        make.width.mas_equalTo(SCREEN_WIDTH-130);
    }];
    
    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.ETLabel.textColor = FWColor(@"515151");
    self.ETLabel.textAlignment = NSTextAlignmentLeft;
    [self.scoreView addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.scoreView);
        make.left.mas_equalTo(self.scoreView).mas_offset(0);
        make.height.mas_equalTo(self.scoreView);
        make.width.mas_equalTo((SCREEN_WIDTH-130)/3);
    }];
    
    
    self.RTLabel = [[UILabel alloc] init];
    self.RTLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.RTLabel.textColor = FWColor(@"515151");
    self.RTLabel.textAlignment = NSTextAlignmentRight;
    [self.scoreView addSubview:self.RTLabel];
    [self.RTLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.scoreView);
        make.right.mas_equalTo(self.scoreView).mas_offset(-4);
        make.height.mas_equalTo(self.scoreView);
        make.width.mas_equalTo(self.ETLabel);
    }];
    
    self.WeisuLabel = [[UILabel alloc] init];
    self.WeisuLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.WeisuLabel.textColor = FWColor(@"515151");
    self.WeisuLabel.textAlignment = NSTextAlignmentCenter;
    [self.scoreView addSubview:self.WeisuLabel];
    [self.WeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.scoreView);
        make.left.mas_equalTo(self.ETLabel.mas_right).mas_offset(0);
        make.height.mas_equalTo(self.scoreView);
        make.right.mas_equalTo(self.RTLabel.mas_left).mas_offset(0);
        make.width.mas_greaterThanOrEqualTo(self.ETLabel);
    }];

}

- (void)configForCell:(id)model{
    
    self.sportModel = (FWScoreDetailModel *)model;
    
    self.matchNameLabel.text = self.sportModel.sport_name;
    self.groupNameLabel.text = self.sportModel.car_group;
    self.carLabel.text = [NSString stringWithFormat:@"车型：%@ %@/车队：%@",self.sportModel.car_brand,self.sportModel.car_type,self.sportModel.club_name];
    self.ETLabel.text = [NSString stringWithFormat:@" ET:%@",self.sportModel.et];
    self.WeisuLabel.text = [NSString stringWithFormat:@"尾速:%@",self.sportModel.vspeed];
    self.RTLabel.text = [NSString stringWithFormat:@"RT:%@",self.sportModel.rt];

    if ([self.sportModel.sport_type isEqualToString:@"02"]) {
        self.typeImageView.image = [UIImage imageNamed:@"type_02"];
    }else{
        self.typeImageView.image = [UIImage imageNamed:@"type_04"];
    }
}

@end
