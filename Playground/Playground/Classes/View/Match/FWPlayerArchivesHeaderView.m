//
//  FWPlayerArchivesHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerArchivesHeaderView.h"

@implementation FWPlayerArchivesHeaderView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.topBgImageView = [[UIImageView alloc] init];
    self.topBgImageView.clipsToBounds = YES;
    self.topBgImageView.userInteractionEnabled = YES;
    self.topBgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topBgImageView.image = [UIImage imageNamed:@"match_detail_bg"];
    [self addSubview:self.topBgImageView];
    [self.topBgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 125));
    }];
    
    self.cirleView = [[UIView alloc] init];
    self.cirleView.layer.cornerRadius = 78/2;
    self.cirleView.layer.masksToBounds =YES;
    self.cirleView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.cirleView];
    [self.cirleView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(78, 78));
        make.left.mas_equalTo(self).mas_offset(14);
        make.top.mas_equalTo(self.topBgImageView.mas_bottom).mas_offset(-78/2);
    }];
    
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 69/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(69, 69));
        make.centerX.centerY.mas_equalTo(self.cirleView);
    }];
    UITapGestureRecognizer * photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoTapClick)];
    [self.photoImageView addGestureRecognizer:photoTap];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.cirleView.mas_right).mas_offset(10);
        make.bottom.mas_equalTo(self.topBgImageView.mas_bottom).mas_offset(-8);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.image = [UIImage imageNamed:@"player_archives"];
    [self addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.topBgImageView.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    self.partLabel = [[UILabel alloc] init];
    self.partLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.partLabel.textColor = FWColor(@"979ca2");
    self.partLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.partLabel];
    [self.partLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(16);
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(7);
        make.centerY.mas_equalTo(self.iconImageView);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
}

- (void)configForView:(id)model{
    
    self.archivesModel = (FWPlayerArchivesModel *)model;
    
    self.partLabel.text = self.archivesModel.sport_count_text;
    if (self.archivesModel.sport_list.count >0) {
        if (self.archivesModel.sport_list[0].list.count > 0) {
            self.nameLabel.text = self.archivesModel.sport_list[0].list[0].nickname;

            [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.archivesModel.sport_list[0].list[0].header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
        }
    }
}

- (void)photoTapClick{
    
    if ([self.archivesModel.driver_info.uid integerValue] == 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"该车手暂未关联肆放账号哦" toController:self.vc];
    }else{
        FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
        UIVC.user_id = self.archivesModel.driver_info.uid;
        [self.vc.navigationController pushViewController:UIVC animated:YES];
    }
}



@end
