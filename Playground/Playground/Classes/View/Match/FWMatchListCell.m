//
//  FWMatchListCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchListCell.h"

@implementation FWMatchListCell
@synthesize titleLabel;
@synthesize timeLabel;
@synthesize picImageView;
@synthesize lineView;
@synthesize addressLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    picImageView = [UIImageView new];
    picImageView.image = [UIImage imageNamed:@"placeholder"];
    picImageView.contentMode = UIViewContentModeScaleAspectFill;
    picImageView.layer.cornerRadius = 2;
    picImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:picImageView];
    [picImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(115, 75));
        make.left.mas_equalTo(self.contentView).mas_offset(24);
        make.top.mas_equalTo(self.contentView).mas_offset(11);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.numberOfLines = 2;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(picImageView);
        make.left.mas_equalTo(picImageView.mas_right).mas_offset(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-20);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    
    addressLabel = [[UILabel alloc] init];
    addressLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    addressLabel.textColor = FWTextColor_B6BCC4;
    addressLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:addressLabel];
    [addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(picImageView);
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(self.contentView).mas_offset(-20);
        make.height.mas_equalTo(16);
    }];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    timeLabel.textColor = FWTextColor_B6BCC4;
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(addressLabel.mas_top).mas_offset(0);
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(self.contentView).mas_offset(-20);
        make.height.mas_equalTo(16);
    }];
}

- (void)configForCell:(FWF4WMatchLiveListModel *)model{
    
    self.liveModel = (FWF4WMatchLiveListModel *)model;
    
    titleLabel.text = self.liveModel.sport_name;
    timeLabel.text = [NSString stringWithFormat:@"时间:%@",self.liveModel.sport_time];
    addressLabel.text = [NSString stringWithFormat:@"地点:%@",self.liveModel.area];
    [picImageView sd_setImageWithURL:[NSURL URLWithString:self.liveModel.logo] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

@end
