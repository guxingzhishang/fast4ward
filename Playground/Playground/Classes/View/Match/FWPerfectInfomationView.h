//
//  FWPerfectInfomationView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWPerfectSegement.h"
#import "FWRegistrationHomeModel.h"
#import "FWPerfectInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPerfectInfomationView : UIScrollView<FWPerfectSegementViewDelegate,UITextViewDelegate>

@property (nonatomic, strong) UIView * firstView;
@property (nonatomic, strong) UILabel * topInfoLabel;

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * infoView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nickNameLabel;
@property (nonatomic, strong) UILabel * realNameLabel;
@property (nonatomic, strong) UILabel * phoneLabel;
@property (nonatomic, strong) UILabel * cardNumLabel;

@property (nonatomic, strong) UILabel * wxNumLabel;
@property (nonatomic, strong) UITextField * wxNumTF;
@property (nonatomic, strong) UIView * wxNumLineView;

@property (nonatomic, strong) UILabel * playerClothLabel;
@property (nonatomic, strong) UIButton * playerClothFormButton;
@property (nonatomic, strong) FWPerfectSegement * sizeSegement;

@property (nonatomic, strong) UIView * secondView;
@property (nonatomic, strong) UILabel * cardUploadLabel;
@property (nonatomic, strong) UILabel * idcardLabel;
@property (nonatomic, strong) UIImageView * idcardImageView;
@property (nonatomic, strong) UILabel * idcardReUploadLabel;
@property (nonatomic, strong) UILabel * carCardLabel;
@property (nonatomic, strong) UIImageView * carCardImageView;
@property (nonatomic, strong) UILabel * carCardReUploadLabel;
@property (nonatomic, strong) UILabel * photographLabel;
@property (nonatomic, strong) UIImageView * photographImageView;
@property (nonatomic, strong) UILabel * photographReUploadLabel;
@property (nonatomic, strong) UILabel * secondTipLabel;

@property (nonatomic, strong) UIView * thirdView;
@property (nonatomic, strong) UILabel * brandLabel;
@property (nonatomic, strong) UITextField * brandTF;
@property (nonatomic, strong) UIView * brandLineView;

@property (nonatomic, strong) UILabel * carStyleLabel;
@property (nonatomic, strong) IQTextView * carStyleTF;
@property (nonatomic, strong) UIView * carStyleLineView;

@property (nonatomic, strong) UILabel * clubLabel;
@property (nonatomic, strong) IQTextView * clubTF;
@property (nonatomic, strong) UIView * clubLineView;

@property (nonatomic, strong) UILabel * frameNumLabel;
@property (nonatomic, strong) IQTextView * frameNumTF;
@property (nonatomic, strong) UIView * frameNumLineView;

@property (nonatomic, strong) UIView * fourthView;
@property (nonatomic, strong) UILabel * jinqiLabel;
@property (nonatomic, strong) FWPerfectSegement * jinqiSegement;
@property (nonatomic, strong) UILabel * qudongLabel;
@property (nonatomic, strong) FWPerfectSegement * qudongSegement;

@property (nonatomic, strong) UIView * fifthView;
@property (nonatomic, strong) UILabel * maliDataLabel;
@property (nonatomic, strong) IQTextView * maliDataTF;
@property (nonatomic, strong) UIView * maliDataLineView;

@property (nonatomic, strong) UILabel * pailiangLabel;
@property (nonatomic, strong) IQTextView * pailiangTF;
@property (nonatomic, strong) UIView * pailiangLineView;

@property (nonatomic, strong) UILabel * luntaiLabel;
@property (nonatomic, strong) UITextField * luntaiTF;
@property (nonatomic, strong) UIView * luntaiLineView;

@property (nonatomic, strong) UILabel * jiyouLabel;
@property (nonatomic, strong) UITextField * jiyouTF;
@property (nonatomic, strong) UIView * jiyouLineView;

@property (nonatomic, strong) UIView * sixthView;
@property (nonatomic, strong) UILabel * gaizhuangLabel;
@property (nonatomic, strong) IQTextView * gaizhuangTV;

@property (nonatomic, strong) UILabel * gaizhuangSumLabel;
@property (nonatomic, strong) IQTextView * gaizhuangSumTF;
@property (nonatomic, strong) UIView * gaizhuangSumLineView;

@property (nonatomic, strong) UIView * seventhView;
@property (nonatomic, strong) UIButton * lookButton;

@property (nonatomic, strong) UILabel * zubieLabel;
@property (nonatomic, strong) UITextField * zubieTF;
@property (nonatomic, strong) UIView * zubieLineView;

@property (nonatomic, strong) UIView * adShadowBgView;
@property (nonatomic, strong) UIView * adBgView;
@property (nonatomic, strong) UIButton * adPlanButton;
@property (nonatomic, strong) UILabel * adPlanTitleLabel;
@property (nonatomic, strong) UILabel * adPlanTitleLabel2;
@property (nonatomic, strong) UILabel * adPlanDescLabel;

@property (nonatomic, strong) UILabel * headLabel;// 车头部
@property (nonatomic, strong) UILabel * footLabel;// 车尾部
@property (nonatomic, strong) UILabel * bodyLabel;// 车侧身

@property (nonatomic, strong) UIImageView * headImageView;// 车头部照片
@property (nonatomic, strong) UIImageView * footImageView;// 车尾部照片
@property (nonatomic, strong) UIImageView * bodyImageView;// 车侧身照片
@property (nonatomic, strong) UILabel * headReUploadLabel;
@property (nonatomic, strong) UILabel * footReUploadLabel;
@property (nonatomic, strong) UILabel * bodyReUploadLabel;

@property (nonatomic, strong) UILabel * announceLabel;

@property (nonatomic, strong) UIButton * saveButton;

@property (nonatomic, strong) NSString * clothes_size;
@property (nonatomic, strong) NSString * jinqi;
@property (nonatomic, strong) NSString * qudong;

@property (nonatomic, strong) NSString * baoming_user_id;

@property (nonatomic, strong) UIPickerView * brandPickerview;
@property (nonatomic, strong) UIPickerView * zubiePickerView;
@property (nonatomic, strong) UIPickerView * luntaiPickerView;
@property (nonatomic, strong) UIPickerView * jiyouPickerView;

@property (nonatomic, strong) NSDictionary * originalDict;

@property (nonatomic, strong) UIImageView * zubieArrowImageView;
@property (nonatomic, strong) UIImageView * brandArrowImageView;
@property (nonatomic, strong) UIImageView * luntaiArrowImageView;
@property (nonatomic, strong) UIImageView * jiyouArrowImageView;

@property (nonatomic, weak) UIViewController * vc;

/* 是否允许编辑 */
@property (nonatomic, assign) BOOL  canEdited;

/* 获取当前值 */
- (NSDictionary *)getParams;

/* 保存 */
- (void)saveButtonClick;

/* 取消选择器 */
- (void)cancelButtonClick;

- (void)configForViewWithArrayModel:(FWPerfectInfoModel *)arrayModel;
@end

NS_ASSUME_NONNULL_END
