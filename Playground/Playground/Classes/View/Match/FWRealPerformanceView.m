//
//  FWRealPerformanceView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWRealPerformanceView.h"
#import "FWScoreModel.h"

@implementation FWRealPerformanceView

+ (instancetype)topicHeaderView
{
    return [[self alloc] init];
}

+ (instancetype)headerViewWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"RealPerformanceHeader";
    FWRealPerformanceView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if (header == nil) {
        // 缓存池中没有, 自己创建
        header = [[self alloc] initWithReuseIdentifier:ID];
    }
    return header;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        // 初始化
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews{
 
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = DHSystemFontOfSize_14;
    self.nameLabel.textColor = FWTextColor_222222;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(15);
        make.right.bottom.top.mas_equalTo(self);
    }];
}

- (void)configForModel:(id)model{
    
    if ([model isKindOfClass:[NSString class]]) {
        self.nameLabel.text = model;
    }else{
        FWScoreListModel * listModel = (FWScoreListModel *)model;
        self.nameLabel.text = listModel.day;
    }
}

@end
