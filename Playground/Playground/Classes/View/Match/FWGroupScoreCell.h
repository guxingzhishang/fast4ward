//
//  FWGroupScoreCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 组别成绩
 */
#import "FWBaseRankCell.h"
#import "FWPlayerRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGroupScoreCell : FWBaseRankCell

@property (nonatomic, strong) UILabel * ETLabel;
@property (nonatomic, strong) UILabel * WeisuLabel;

- (void)configCellForModel:(id)model withIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
