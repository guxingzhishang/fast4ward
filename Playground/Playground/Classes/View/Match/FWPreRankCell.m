//
//  FWPreRankCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/28.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWPreRankCell.h"

@implementation FWPreRankCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.rankLabel = [[UILabel alloc] init];
    self.rankLabel.font = DHSystemFontOfSize_16;
    self.rankLabel.textColor = FWTextColor_000000;
    self.rankLabel.layer.cornerRadius = 10;
    self.rankLabel.layer.masksToBounds =YES;
    self.rankLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.rankLabel];
    [self.rankLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 20;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.contentView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.rankLabel.mas_right).mas_offset(14);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    [self.photoImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewClick)]];

    self.rankNumImageView = [[UIImageView alloc] init];
    self.rankNumImageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:self.rankNumImageView];
    [self.rankNumImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
      make.centerY.mas_equalTo(self.photoImageView);
      make.size.mas_equalTo(CGSizeMake(16, 16));
        make.right.mas_equalTo(self.photoImageView.mas_left).mas_offset(-15.5);
    }];
    
    self.iconImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.photoImageView.mas_top).mas_offset(2);
        make.centerX.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(20, 15));
    }];
    self.iconImageView.hidden = YES;
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = DHSystemFontOfSize_14;
    self.nameLabel.textColor = FWColor(@"353B48");
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(50);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(12);
        make.top.mas_equalTo(self.photoImageView);
    }];
    
    self.RTLabel = [[UILabel alloc] init];
    self.RTLabel.font = DHSystemFontOfSize_12;
    self.RTLabel.textColor = FWTextColor_969696;
    self.RTLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.RTLabel];
    [self.RTLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_equalTo((SCREEN_WIDTH-112)/3);
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
    }];
    
    self.weisuLabel = [[UILabel alloc] init];
    self.weisuLabel.font = DHSystemFontOfSize_12;
    self.weisuLabel.textColor = FWTextColor_969696;
    self.weisuLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.weisuLabel];
    [self.weisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.RTLabel);
        make.left.mas_equalTo(self.RTLabel.mas_right);
        make.centerY.mas_equalTo(self.RTLabel);
    }];
    
    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = DHSystemFontOfSize_12;
    self.ETLabel.textColor = FWGradual_Orange_FF8C11;
    self.ETLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.weisuLabel.mas_right);
        make.height.mas_equalTo(self.weisuLabel);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.centerY.mas_equalTo(self.RTLabel);
    }];
    
    self.carLabel = [[UILabel alloc] init];
    self.carLabel.font = DHSystemFontOfSize_12;
    self.carLabel.textColor = FWTextColor_969696;
    self.carLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.carLabel];
    [self.carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.ETLabel);
        make.centerY.mas_equalTo(self.nameLabel);
        make.left.mas_equalTo(self.nameLabel.mas_right);
    }];
}

- (void)configForCell:(id)model WithIndexRow:(NSInteger)row{
  
    if (row == 0) {
        
        self.iconImageView.hidden = NO;
        self.iconImageView.image = FWImage(@"rank_first");
        self.rankNumImageView.image = [UIImage imageNamed:@"rank_new_first"];
        self.rankLabel.text = @"";
    }else if (row == 1){
        
        self.iconImageView.hidden = NO;
        self.iconImageView.image = FWImage(@"rank_second");
        self.rankNumImageView.image = [UIImage imageNamed:@"rank_new_second"];
        self.rankLabel.text = @"";
    }else if (row == 2){
        
        self.iconImageView.hidden = NO;
        self.iconImageView.image = FWImage(@"rank_third");
        self.rankNumImageView.image = [UIImage imageNamed:@"rank_new_third"];
        self.rankLabel.text = @"";
    }else{
        self.iconImageView.hidden = YES;
        self.rankNumImageView.image = [UIImage imageNamed:@""];

        self.rankLabel.backgroundColor = FWColor(@"FFFFFF");
        self.rankLabel.textColor = FWTextColor_000000;
    }
    
    self.listModel = (FWPreRankListModel *)model;
    
    self.rankLabel.text = @(row+1).stringValue;
    self.nameLabel.text = self.listModel.player_info.player_name?[NSString stringWithFormat:@"%@ %@", self.listModel.player_info.car_no,self.listModel.player_info.player_name]:@"暂无车手";
    self.carLabel.text = self.listModel.player_info.car_type;
    self.RTLabel.text = [NSString stringWithFormat:@"RT: %@",self.listModel.rt];
    self.weisuLabel.text = [NSString stringWithFormat:@"尾速: %@",self.listModel.vspeed];
    self.ETLabel.text = [NSString stringWithFormat:@"ET: %@",self.listModel.et];

    if (![self.listModel.score_status_str isEqualToString:@"正常"]) {
        self.ETLabel.text = self.listModel.score_status_str;
    }
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.player_info.header] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (error) {
            self.photoImageView.image = FWImage(@"placeholder_photo");
        }
    }];
}

- (void)photoImageViewClick{
    
    if (![self.listModel.uid isEqualToString:@"0"]) {
        FWNewUserInfoViewController * UVC = [[FWNewUserInfoViewController alloc] init];
        UVC.user_id = self.listModel.uid;
        [self.vc.navigationController pushViewController:UVC animated:YES];
    }
}

@end
