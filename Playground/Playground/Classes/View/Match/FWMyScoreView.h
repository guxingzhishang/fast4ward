//
//  FWMyScoreView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/12.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMyScoreView : UIView
@property (nonatomic, strong) UILabel * numberLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * groupLabel;
@property (nonatomic, strong) UILabel * signLabel;
@property (nonatomic, strong) UILabel * lineLabel;

@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * authenticationView;

@property (nonatomic, strong) UILabel * ETLabel;
@property (nonatomic, strong) UILabel * WeisuLabel;

- (void)configViewForModel:(id)model;
@end

NS_ASSUME_NONNULL_END
