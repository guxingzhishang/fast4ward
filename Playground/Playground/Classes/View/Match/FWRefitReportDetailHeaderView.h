//
//  FWRefitReportDetailHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/9.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWBannerView.h"
#import "SDCycleScrollView.h"
#import "FWModuleButton.h"
#import "FWRefitReportModel.h"
#import "FWPhotoBrowser.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWRefitReportDetailHeaderViewDelegate <NSObject>

- (void)backButtonClick;

@end

@interface FWRefitReportDetailHeaderView : UIView<SDCycleScrollViewDelegate,UIGestureRecognizerDelegate,FWPhotoBrowserDelegate>

@property (nonatomic, strong) UIImageView * topHeaderView;
/**
 * 返回按钮
 */
@property (nonatomic, strong) UIButton * backButton;

/**
 * 轮播图
 */
@property (nonatomic, strong) SDCycleScrollView * bannerView;


@property (nonatomic, strong) UIView * playerInfoView;

@property (nonatomic, strong) UIButton * attentionButton;

/**
 * 内容
 */
@property (nonatomic, strong) UILabel * contentLabel;

/**
 * 承载标签的视图
 */
@property (nonatomic, strong) UIView * containerView;

/**
 * 承载改装清单的视频
 */
@property (nonatomic, strong) UIView * gaizhuangView;

/**
 * 评论两个字
 */
@property (nonatomic, strong) UILabel * commentLabel;

/**
 * 评论数
 */
@property (nonatomic, strong) UIButton * commentNumberButton;

/* 日期 */
@property (nonatomic, strong) UILabel * timeLabel;

/* 商品背景图 */
@property (nonatomic, strong) UIView * goodsView;
/* 推荐商品 */
@property (nonatomic, strong) UILabel * recammondLabel;
/* 商品展示图 */
@property (nonatomic, strong) UIImageView * goodsImageView;
/* 商品名称 */
@property (nonatomic, strong) UILabel * goodsTitleLabel;
/* 商品价格 */
@property (nonatomic, strong) UILabel * goodsPriceLabel;
/* 商品浏览图片 */
@property (nonatomic, strong) UIImageView * goodsScanView;
/* 商品浏览数量 */
@property (nonatomic, strong) UILabel * goodsScanLabel;

@property (nonatomic, strong) UIView * goodsLineView;

@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) UIViewController *delegate;

@property (nonatomic, strong) NSMutableArray * containerArr;

@property (nonatomic, weak) id<FWRefitCaseDetailHeaderViewDelegate> headerDelegate;

@property (nonatomic, assign) CGFloat containerHeight;

@property (nonatomic, strong) FWRefitReportListModel * listModel;

- (void)configViewForModel:(id)model;

- (CGFloat)getCurrentViewHeight;


@end

NS_ASSUME_NONNULL_END
