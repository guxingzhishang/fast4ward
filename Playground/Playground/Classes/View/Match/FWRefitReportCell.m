//
//  FWRefitReportCell.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWRefitReportCell.h"
#import "FWRefitReportModel.h"
#import "FWRefitReportDetailViewController.h"

@interface FWRefitReportCell ()
@property (nonatomic, strong) FWRefitReportListModel * listModel;
@property (nonatomic, strong) FWRefitListKeyModel * listKeys;
@property (nonatomic, strong) NSString * likeType;
@end

@implementation FWRefitReportCell
@synthesize listModel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.layer.cornerRadius = 4;
    self.iconImageView.layer.masksToBounds = YES;
    self.iconImageView.clipsToBounds = YES;
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.size.mas_equalTo(CGSizeMake(130, 130));
        make.top.mas_equalTo(self.contentView).mas_offset(14);
    }];
    
    self.playerLabel = [[UILabel alloc] init];
    self.playerLabel.font = DHFont(14);
    self.playerLabel.textColor = DHLineHexColor_LightGray_999999;
    self.playerLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.playerLabel];
    [self.playerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(70, 20));
        make.top.mas_equalTo(self.iconImageView).mas_offset(5);
    }];
    
    self.playerValueLabel = [[UILabel alloc] init];
    self.playerValueLabel.font = DHFont(14);
    self.playerValueLabel.textColor = FWTextColor_222222;
    self.playerValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.playerValueLabel];
    [self.playerValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.playerLabel.mas_right).mas_offset(5);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
        make.centerY.mas_equalTo(self.playerLabel);
    }];
    
    self.carLabel = [[UILabel alloc] init];
    self.carLabel.font = DHFont(14);
    self.carLabel.textColor = DHLineHexColor_LightGray_999999;
    self.carLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.carLabel];
    [self.carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.playerLabel);
        make.size.mas_equalTo(CGSizeMake(70, 20));
        make.top.mas_equalTo(self.playerLabel.mas_bottom).mas_offset(2);
    }];
    
    self.carValueLabel = [[UILabel alloc] init];
    self.carValueLabel.font = DHFont(14);
    self.carValueLabel.textColor = FWTextColor_222222;
    self.carValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.carValueLabel];
    [self.carValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carLabel.mas_right).mas_offset(5);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
        make.centerY.mas_equalTo(self.carLabel);
    }];
    
    self.scoreLabel = [[UILabel alloc] init];
    self.scoreLabel.font = DHFont(14);
    self.scoreLabel.numberOfLines = 0;
    self.scoreLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.scoreLabel.textColor = DHLineHexColor_LightGray_999999;
    self.scoreLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.scoreLabel];
    [self.scoreLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.playerLabel);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
        make.top.mas_equalTo(self.carLabel.mas_bottom).mas_offset(2);
    }];
    
    self.scoreValueLabel = [[UILabel alloc] init];
    self.scoreValueLabel.numberOfLines = 0;
    self.scoreValueLabel.lineBreakMode = NSLineBreakByClipping;
    self.scoreValueLabel.font = DHFont(14);
    self.scoreValueLabel.textColor = FWTextColor_222222;
    self.scoreValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.scoreValueLabel];
    [self.scoreValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.scoreLabel);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
        make.top.mas_equalTo(self.scoreLabel.mas_bottom).mas_offset(2);
    }];
    
    self.detailButton = [[UIButton alloc] init];
    self.detailButton.layer.borderColor = FWColor(@"D6D6D6").CGColor;
    self.detailButton.layer.borderWidth = 1;
    self.detailButton.layer.cornerRadius = 2;
    self.detailButton.layer.masksToBounds = YES;
    self.detailButton.titleLabel.font = DHFont(12);
    [self.detailButton setTitle:@"查看详细情报" forState:UIControlStateNormal];
    [self.detailButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.detailButton addTarget:self action:@selector(detailButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.detailButton];
    [self.detailButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(110, 30));
        make.left.mas_equalTo(self.carLabel);
        make.top.mas_equalTo(self.scoreValueLabel.mas_bottom).mas_offset(10);
    }];

    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = DHFont(14);
    self.timeLabel.textColor = DHLineHexColor_LightGray_999999;
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView).mas_offset(0);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.iconImageView.mas_bottom).mas_offset(10);
    }];
    
    
    self.shareButton = [[UIButton alloc] init];
    [self.shareButton setImage:[UIImage imageNamed:@"home_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.shareButton];
    [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(0);
        make.height.mas_equalTo(40);
        make.centerY.mas_equalTo(self.timeLabel);
        make.width.mas_equalTo(40);
    }];

    self.likesButton = [[UIButton alloc] init];
    [self.likesButton addTarget:self action:@selector(likesButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.likesButton];
    [self.likesButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.shareButton.mas_left).mas_offset(0);
        make.height.mas_equalTo(self.shareButton);
        make.width.mas_equalTo(65);
        make.top.mas_equalTo(self.shareButton);
    }];
    
    self.likesLabel = [[UILabel alloc] init];
    self.likesLabel.text = @"";
    self.likesLabel.font = DHSystemFontOfSize_12;
    self.likesLabel.textColor = FWTextColor_A7ADB4;
    self.likesLabel.textAlignment = NSTextAlignmentLeft;
    [self.likesButton addSubview:self.likesLabel];
    [self.likesLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.likesButton).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(self.likesButton);
    }];
    
    self.likesImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_unlike"]];
    [self.likesButton addSubview:self.likesImageView];
    [self.likesImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.likesButton);
        make.size.mas_equalTo(CGSizeMake(19, 16));
        make.right.mas_equalTo(self.likesLabel.mas_left).mas_offset(-5);
    }];
    
    self.commentButton = [[UIButton alloc] init];
    [self.commentButton addTarget:self action:@selector(commentButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.commentButton];
    [self.commentButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.likesButton.mas_left).mas_offset(0);
        make.height.mas_equalTo(self.likesButton);
        make.width.mas_equalTo(65);
        make.top.mas_equalTo(self.likesButton);
    }];
    
    self.commentLabel = [[UILabel alloc] init];
    self.commentLabel.text = @"";
    self.commentLabel.font = DHSystemFontOfSize_12;
    self.commentLabel.textColor = FWTextColor_A7ADB4;
    self.commentLabel.textAlignment = NSTextAlignmentLeft;
    [self.commentButton addSubview:self.commentLabel];
    [self.commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.commentButton).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(self.commentButton);
    }];
    
    self.commentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pinglun"]];
    [self.commentButton addSubview:self.commentImageView];
    [self.commentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(18, 16));
        make.centerY.mas_equalTo(self.commentButton);
        make.right.mas_equalTo(self.commentLabel.mas_left).mas_offset(-5);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(self.contentView);
    }];
}


- (void)configForCell:(id)model WithRefitListKeysModel:(FWRefitListKeyModel *)listKeys{
    
    listModel = (FWRefitReportListModel *)model;
    self.listKeys = listKeys;
    
    self.playerLabel.text = listKeys.cheshou;
    self.carLabel.text = listKeys.car;
    self.scoreLabel.text = listKeys.bestscore;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:listModel.feed_info.feed_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.playerValueLabel.text = listModel.driver_info.f3;
    self.carValueLabel.text = listModel.driver_info.f10;
    self.scoreValueLabel.text = listModel.driver_info.best_score_str;

    self.timeLabel.text = [NSString stringWithFormat:@"%@   %@次浏览",listModel.feed_info.format_create_time,listModel.feed_info.count_realtime.pv_count_format];
    
    self.likesLabel.text = listModel.feed_info.count_realtime.like_count_format;
    self.commentLabel.text = listModel.feed_info.count_realtime.comment_count_format;

    // 是否点赞
    if ([self.listModel.feed_info.is_liked isEqualToString:@"2"]) {
        self.likeType = @"add";
        self.likesImageView.image = [UIImage imageNamed:@"unlike"];
    }else if ([self.listModel.feed_info.is_liked isEqualToString:@"1"]){
        self.likeType = @"cancel";
        self.likesImageView.image = [UIImage imageNamed:@"car_detail_like"];
    }
}

#pragma mark - > 查看详细情报
- (void)detailButtonClick{
    
    FWRefitReportDetailViewController * RRDVC = [[FWRefitReportDetailViewController alloc] init];
    RRDVC.feed_id = self.listModel.feed_info.feed_id;
    RRDVC.bestet_id = self.listModel.driver_info.bestet_id;
    [self.vc.navigationController pushViewController:RRDVC animated:YES];
}

#pragma mark - > 喜欢
- (void)likesButtonOnClick:(UIButton *)sender{
    
    [self checkLogin];
    
    FWFeedListModel * model = self.listModel.feed_info;

    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWLikeRequest * request = [[FWLikeRequest alloc] init];

        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.listModel.feed_info.feed_id,
                                  @"like_type":self.likeType,
                                  };

        [request startWithParameters:params WithAction:Submit_like_feed WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

            if ([code isEqualToString:NetRespondCodeSuccess]) {

                if ([self.likeType isEqualToString: @"add"]) {
                    model.is_liked = @"1";

                    if ([model.count_realtime.like_count_format integerValue] ||
                        [model.count_realtime.like_count_format integerValue] == 0) {

                        model.count_realtime.like_count_format = @([model.count_realtime.like_count_format integerValue] +1).stringValue;
                    }
                }else if ([self.likeType isEqualToString: @"cancel"]){
                    model.is_liked = @"2";

                    if ([model.count_realtime.like_count_format integerValue] ||
                        [model.count_realtime.like_count_format integerValue] == 0) {

                        model.count_realtime.like_count_format = @([model.count_realtime.like_count_format integerValue] -1).stringValue;
                    }
                }

                self.listModel.feed_info = model;
                [self configForCell:self.listModel WithRefitListKeysModel:self.listKeys];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}

#pragma mark - > 评论
- (void)commentButtonOnClick:(UIButton *)sender{
    
    FWCommentViewController * VC = [[FWCommentViewController alloc] init];
    VC.feed_id = self.listModel.feed_info.feed_id;
    [self.vc.navigationController pushViewController:VC animated:YES];
}

#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{
   
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self.vc WithType:2];
        [shareView showView];
    }
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    FWFeedListModel * model = [[FWFeedListModel alloc] init];

    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0){
            model.feed_id = @"other";
            model.share_url = self.listModel.feed_info.share_url;
            model.share_desc = self.listModel.feed_info.share_desc;
            model.share_title = self.listModel.feed_info.share_title;
            model.feed_cover = self.listModel.feed_info.feed_cover;
            [[ShareManager defaultShareManager] sendWXWithModel:model WithType:0];
        }else if (index == 1){
            
            model.feed_id = @"other";
            model.share_url = self.listModel.feed_info.share_url;
            model.share_desc = self.listModel.feed_info.share_desc;
            model.share_title = self.listModel.feed_info.share_title;
            model.feed_cover = self.listModel.feed_info.feed_cover;
            [[ShareManager defaultShareManager] sendWXWithModel:model WithType:1];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self.vc];
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
