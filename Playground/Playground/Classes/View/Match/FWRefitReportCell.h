//
//  FWRefitReportCell.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWRefitReportCell : UITableViewCell<ShareViewDelegate>

@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UILabel * playerLabel;
@property (nonatomic, strong) UILabel * carLabel;
@property (nonatomic, strong) UILabel * scoreLabel;

@property (nonatomic, strong) UILabel * playerValueLabel;
@property (nonatomic, strong) UILabel * carValueLabel;
@property (nonatomic, strong) UILabel * scoreValueLabel;

@property (nonatomic, strong) UIButton * detailButton;

@property (nonatomic, strong) UILabel * timeLabel;

/* 喜欢按钮 */
@property (nonatomic, strong) UIButton * likesButton;

/* 喜欢图标 */
@property (nonatomic, strong) UIImageView * likesImageView;

/* 喜欢数 */
@property (nonatomic, strong) UILabel * likesLabel;

/* 评论按钮 */
@property (nonatomic, strong) UIButton * commentButton;

/* 评论图标 */
@property (nonatomic, strong) UIImageView * commentImageView;

/* 评论数 */
@property (nonatomic, strong) UILabel * commentLabel;

/* 分享按钮 */
@property (nonatomic, strong) UIButton * shareButton;

/* 分享图标 */
@property (nonatomic, strong) UIImageView * shareImageView;

/* 分享数 */
@property (nonatomic, strong) UILabel * shareLabel;

@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, weak) UIViewController * vc;

- (void)configForCell:(id)model WithRefitListKeysModel:(FWRefitListKeyModel *)listKeys;

@end

NS_ASSUME_NONNULL_END
