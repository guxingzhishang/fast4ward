//
//  FWOfficialVideoCell.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/9.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWOfficialVideoCell : UITableViewCell

@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UIImageView * videoIconImageView;
@property (nonatomic, strong) UILabel * contentLabel;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
