//
//  FWGroupScoreView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWGroupScoreView : UIView
@property (nonatomic, strong) UILabel * numberLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * signLabel;
@property (nonatomic, strong) UILabel * lineLabel;
@property (nonatomic, strong) UIImageView * rightImageView;

@property (nonatomic, strong) UIImageView * rankImageView;//只有前三名有
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * authenticationView;

@property (nonatomic, strong) UILabel * ETLabel;
@property (nonatomic, strong) UILabel * WeisuLabel;

- (void)configViewForModel:(id)model;
@end

NS_ASSUME_NONNULL_END
