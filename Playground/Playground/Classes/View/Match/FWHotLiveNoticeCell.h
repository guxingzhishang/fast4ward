//
//  FWHotLiveNoticeCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 直播 - 预告
 */
#import <UIKit/UIKit.h>
#import "FWLiveModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FWHotLiveNoticeCell : UITableViewCell

@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * authenticationView;
@property (nonatomic, strong) UILabel * nameLabel;

@property (nonatomic, weak) UIViewController * vc;


@property (nonatomic, strong) FWLiveListModel * listModel;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
