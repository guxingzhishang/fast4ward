//
//  FWGroupScoreView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGroupScoreView.h"
#import "FWPlayerRankModel.h"

@implementation FWGroupScoreView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.numberLabel.textColor = FWTextColor_858585;
    self.numberLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.numberLabel];
    [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(19);
        make.centerY.mas_equalTo(self);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(20);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 41/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.backgroundColor = [UIColor lightGrayColor];
    self.photoImageView.image = [UIImage imageNamed:@""];
    [self addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel.mas_right).mas_offset(13);
        make.centerY.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(41, 41));
    }];
    
    self.rankImageView = [[UIImageView alloc] init];
    self.rankImageView.image = [UIImage imageNamed:@""];
    [self addSubview:self.rankImageView];
    [self.rankImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.photoImageView.mas_top).mas_offset(5);
        make.centerX.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(26, 18));
    }];
    self.rankImageView.hidden = YES;
    
    self.rightImageView = [[UIImageView alloc] init];
    self.rightImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self addSubview:self.rightImageView];
    [self.rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(5, 9));
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(self).mas_offset(-14);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.nameLabel.textColor = FWTextColor_12101D;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(13);
        make.top.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-90);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    self.authenticationView = [[UIImageView alloc] init];
    self.authenticationView.image = [UIImage imageNamed:@"primary_bussiness"];
    [self addSubview:self.authenticationView];
    [self.authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(0.5);
        make.left.mas_equalTo(self.nameLabel).mas_offset(0);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
    }];
    self.authenticationView.hidden = YES;
    
    
    self.signLabel = [[UILabel alloc] init];
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.signLabel.textColor = FWTextColor_858585;
    self.signLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.signLabel];
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.authenticationView.mas_right);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.nameLabel);
    }];
    
    // 优先级高一些，先撑起影响力，压缩认证
    
    self.lineLabel = [[UILabel alloc] init];
    self.lineLabel.backgroundColor = FWTextColor_ECECEC;
    [self addSubview:self.lineLabel];
    [self.lineLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel);
        make.width.mas_greaterThanOrEqualTo(100);
        make.bottom.mas_equalTo(self).mas_offset(-1);
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(self.rightImageView);
    }];
    
    self.authenticationView.hidden = YES;
    
    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 12];
    self.ETLabel.textColor = FWColor(@"#38A4F8");
    self.ETLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-10);
        make.width.mas_equalTo(80);
    }];
    
    self.WeisuLabel = [[UILabel alloc] init];
    self.WeisuLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.WeisuLabel.textColor = FWTextColor_12101D;
    self.WeisuLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.WeisuLabel];
    [self.WeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.signLabel);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-10);
        make.width.mas_equalTo(90);
    }];
}

- (void)configViewForModel:(id)model{
    
    FWScoreDetailModel * scoreListModel = (FWScoreDetailModel *)model;
    
    self.numberLabel.text = scoreListModel.index;
    self.nameLabel.text = [NSString stringWithFormat:@"%@(%@)",scoreListModel.nickname,scoreListModel.car_group];
    self.signLabel.text = [NSString stringWithFormat:@"座驾：%@",scoreListModel.car_brand];
    
    self.ETLabel.text = [NSString stringWithFormat:@"ET:%@",scoreListModel.et];
    self.WeisuLabel.text = [NSString stringWithFormat:@"尾速:%@",scoreListModel.vspeed];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:scoreListModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
}


@end
