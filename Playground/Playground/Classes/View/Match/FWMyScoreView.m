//
//  FWMyScoreView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/12.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyScoreView.h"
#import "FWPlayerRankModel.h"

@implementation FWMyScoreView


- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.numberLabel.textColor = FWTextColor_858585;
    self.numberLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.numberLabel];
    [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(19);
        make.top.mas_equalTo(self).mas_offset(39);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(20);
    }];
    
    self.groupLabel = [[UILabel alloc] init];
    self.groupLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.groupLabel.textColor = FWTextColor_222222;
    self.groupLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.groupLabel];
    [self.groupLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel.mas_right).mas_offset(13);
        make.top.mas_equalTo(self).mas_offset(5);
        make.height.mas_equalTo(14);
        make.width.mas_equalTo(200);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 41/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.backgroundColor = [UIColor lightGrayColor];
    self.photoImageView.image = [UIImage imageNamed:@""];
    [self addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.groupLabel);
        make.top.mas_equalTo(self.groupLabel.mas_bottom).mas_offset(7);
        make.size.mas_equalTo(CGSizeMake(41, 41));
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.nameLabel.textColor = FWTextColor_12101D;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(13);
        make.top.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self).mas_offset(-120);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    
    self.signLabel = [[UILabel alloc] init];
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.signLabel.textColor = FWTextColor_858585;
    self.signLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.signLabel];
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.nameLabel);
    }];
    

    
    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 12];
    self.ETLabel.textColor = FWColor(@"#38A4F8");
    self.ETLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self).mas_offset(-10);
        make.width.mas_equalTo(80);
    }];
    
    self.WeisuLabel = [[UILabel alloc] init];
    self.WeisuLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.WeisuLabel.textColor = FWTextColor_12101D;
    self.WeisuLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.WeisuLabel];
    [self.WeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.signLabel);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self).mas_offset(-10);
        make.width.mas_equalTo(90);
    }];
}

- (void)configViewForModel:(id)model{
    
    FWScoreDetailModel * scoreListModel = (FWScoreDetailModel *)model;
    
    self.numberLabel.text = scoreListModel.index;
    self.groupLabel.text = scoreListModel.group_name;

    self.nameLabel.text = [NSString stringWithFormat:@"%@(%@)",scoreListModel.nickname,scoreListModel.car_group];
    self.signLabel.text = [NSString stringWithFormat:@"座驾：%@",scoreListModel.car_brand];
    
    self.ETLabel.text = [NSString stringWithFormat:@"ET:%@",scoreListModel.et];
    self.WeisuLabel.text = [NSString stringWithFormat:@"尾速:%@",scoreListModel.vspeed];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:scoreListModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
}



@end
