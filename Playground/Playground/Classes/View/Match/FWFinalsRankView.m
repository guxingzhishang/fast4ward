//
//  FWFinalsRankView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/28.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWFinalsRankView.h"
#import "FWFinalsBettleView.h"

#define viewWidth 238
#define viewHeight 116


@implementation FWFinalsRankView
@synthesize lineCount;
@synthesize maxRowCount;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
    }
    
    return self;
}

- (void)configForViewWithData:(id)model{

    [self clearSubViews];
    
    FWFinalRankModel * finalModel = (FWFinalRankModel*)model;
    FWFinalListModel * listModel = finalModel.list[0];
    
    /* 有几列 */
    lineCount = finalModel.list.count;
    /* 第一列，有几行（理论上第一列是数据最多的）*/
    maxRowCount = listModel.list.count;

    for (int i = 0; i < lineCount; i++) {
        
        FWFinalListModel * listModel = finalModel.list[i];
        
        /* 2的i次方 */
        CGFloat mi = pow(2, i);
                         
        CGFloat height = mi*viewHeight+ (mi-1)*20;
        
        for (int j = 0; j < listModel.list.count; j++) {
            FWFinalSubListModel * subModel = listModel.list[j];
            
            FWFinalsBettleView * bettleView = [[FWFinalsBettleView alloc] init];
            [bettleView refreshData:subModel withIndex:i WithTotal:lineCount];
            bettleView.vc = self.vc;
            [self addSubview:bettleView];
            [bettleView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self).mas_offset(-10+(viewWidth) *i);
                make.top.mas_equalTo(self).mas_offset(j*(height+20));
                make.size.mas_equalTo(CGSizeMake(viewWidth, height));
            }];
        }
    }
    
    self.contentSize = CGSizeMake((viewWidth)*lineCount, 136*maxRowCount);
}

- (void)clearSubViews{
    
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
}

@end
