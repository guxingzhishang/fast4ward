//
//  FWMatchView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchView.h"
#import "FWMatchListViewController.h"
#import "FWLiveViewController.h"
#import "FWPlayerRankViewController.h"
#import "FWMatchPicsViewController.h"
#import "FWPlayerRegistrationViewController.h"
#import "FWRegistrationHomeViewController.h"
#import "FWRealScroeAndRankViewController.h"
#import "FWMatchTicketViewController.h"
#import "UIScrollView+ABSideRefresh.h"
#import "FWRefitReportViewController.h"
#import "FWOfficialVideoViewController.h"
#import "FWRefitReportDetailViewController.h"

@interface FWMatchView ()

@end

@implementation FWMatchView
@synthesize matchLiveImageView;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWTextColor_FAFAFA;
        [self setupSubviews];
    }
    
    return self;
}


#pragma mark - > 初始化头部视图
- (void)setupSubviews{
    
    self.mainView = [[UIView alloc] init];
    [self addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    [self setupConvientView];
}

#pragma mark - > 初始化便捷模块
- (void)setupConvientView{
 
    if (!self.convientView) {
        self.convientView = [[UIView alloc] init];
        [self.mainView addSubview:self.convientView];
    }
    [self.convientView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.top.mas_equalTo(self.mainView).mas_offset(20);
        make.height.mas_equalTo(90);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];

    for (UIView * view in self.convientView.subviews) {
        [view removeFromSuperview];
    }
    
    NSArray * titleArray = @[@"赛事门票",@"车手报名",@"车手排名",@"比赛回放"];
    NSArray * iconArray = @[@"match_tickets",@"match_player_signup",@"match_player_rank",@"match_playback"];

    for (int i =0; i < 4; i++) {
        FWModuleButton * button = [[FWModuleButton alloc] init];
        button.tag = 11111+i;
        button.nameLabel.textColor = FWTextColor_616C91;
        [button addTarget:self action:@selector(topButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.convientView addSubview:button];
        button.frame = CGRectMake(35 + i*(50+(SCREEN_WIDTH-70-200)/3), 5, 50, 80);
        
        [button setModuleTitle:titleArray[i]];
        [button setModuleImage:iconArray[i]];
        
        button.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        [button.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(button);
            make.size.mas_equalTo(CGSizeMake(50,50));
        }];

        button.nameLabel.font = DHSystemFontOfSize_12;
        button.nameLabel.textAlignment = NSTextAlignmentCenter;
        button.nameLabel.textColor = FWColor(@"#2d2d2d");
        [button.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(button);
            make.top.mas_equalTo(button.iconImageView.mas_bottom).mas_offset(10);
            make.height.mas_equalTo(17);
            make.centerX.mas_equalTo(button.iconImageView);
            make.width.mas_equalTo(50);
        }];
    }
}

#pragma mark - > 初始化直播
- (void)setupLiveView{
    
    if (!matchLiveImageView) {
        matchLiveImageView = [[UIImageView alloc] init];
        matchLiveImageView.layer.cornerRadius = 4;
        matchLiveImageView.layer.masksToBounds = YES;
        matchLiveImageView.userInteractionEnabled = YES;
        [self.mainView addSubview:matchLiveImageView];
        [matchLiveImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(liveButtonOnClick)]];
    }
    
    if ([self.matchModel.live_show_status isEqualToString:@"2"]) {
        [matchLiveImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-20, 0.01));
            make.left.mas_equalTo(self.mainView).mas_offset(10);
            make.top.mas_equalTo(self.convientView.mas_bottom).mas_offset(0);
        }];
    }else{
        // 直播中
        matchLiveImageView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, 146*(SCREEN_WIDTH-28)/347);
        [matchLiveImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-20, 146*(SCREEN_WIDTH-20)/355));
            make.left.mas_equalTo(self.mainView).mas_offset(10);
            make.top.mas_equalTo(self.convientView.mas_bottom).mas_offset(10);
        }];
            
        matchLiveImageView.clipsToBounds = YES;
        matchLiveImageView.contentMode = UIViewContentModeScaleAspectFill;
        [matchLiveImageView sd_setImageWithURL:[NSURL URLWithString:self.matchModel.sport_index_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
}

#pragma mark - > 初始化问答
- (void)setupAskView{
    
    if (!self.wendaView) {
        self.wendaView = [[UIView alloc] init];
        [self.mainView addSubview:self.wendaView];
    }
    
    for (UIView * view in self.wendaView.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat topHeight = 0.01;
    CGFloat wendaHeight = 0.01;


    if (self.matchModel.list_tag.count > 0) {
        wendaHeight = 10;
        topHeight = 10;
    }else{
        wendaHeight = 0.01;
        topHeight = 0.01;
    }
    
    [self.wendaView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH-20);
        make.height.mas_equalTo(wendaHeight);
        make.left.mas_equalTo(self.mainView).mas_offset(10);
        make.top.mas_equalTo(self.matchLiveImageView.mas_bottom).mas_offset(topHeight);
    }];
    
    for(int i = 0 ; i< self.matchModel.list_tag.count;i++){
        
        FWTagsModel * listModel = self.matchModel.list_tag[i];
        
        UIView * tagsView = [[UIView alloc] init];
        tagsView.tag = 12321;
        [self.wendaView addSubview:tagsView];
        [tagsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.wendaView);
            make.top.mas_equalTo(self.wendaView).mas_offset(30*i);
            make.width.mas_equalTo(SCREEN_WIDTH-20);
            make.height.mas_equalTo(30);
            if (i == self.matchModel.list_tag.count - 1) {
                make.bottom.mas_equalTo(self.wendaView);
            }
        }];
        [tagsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentLabelClick:)]];

        UIImageView * shapeView = [[UIImageView alloc] init];
        shapeView.image = [UIImage imageNamed:@"shape_question"];
        [tagsView addSubview:shapeView];
        [shapeView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(tagsView).mas_offset(9);
            make.size.mas_equalTo(CGSizeMake(13, 12));
            make.left.mas_equalTo(self.wendaView).mas_offset(0);
        }];
        
        UILabel * dongtaiLabel = [[UILabel alloc] init];
        dongtaiLabel.font = DHSystemFontOfSize_12;
        dongtaiLabel.textColor = DHLineHexColor_LightGray_999999;
        dongtaiLabel.textAlignment = NSTextAlignmentRight;
        [tagsView addSubview:dongtaiLabel];
        [dongtaiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(shapeView);
            make.right.mas_equalTo(tagsView).mas_offset(-14);
            make.height.mas_equalTo(17);
            make.width.mas_greaterThanOrEqualTo(10);
        }];

        UILabel * contentLabel = [[UILabel alloc] init];
        contentLabel.font = DHSystemFontOfSize_14;
        contentLabel.textColor = FWColor(@"2d2d2d");
        contentLabel.textAlignment = NSTextAlignmentLeft;
        [tagsView addSubview:contentLabel];
        [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(shapeView);
            make.left.mas_equalTo(shapeView.mas_right).mas_offset(14);
            make.height.mas_equalTo(26);
            make.right.mas_equalTo(dongtaiLabel.mas_left).mas_offset(-14);
            make.width.mas_greaterThanOrEqualTo(10);
        }];

        dongtaiLabel.text = [NSString stringWithFormat:@"%@条动态",listModel.feed_count];
        contentLabel.text = listModel.tag_name;
    }
}

#pragma mark - > 初始化图集
- (void)setupTujiView{
    
    if (!self.tujiView) {
        self.tujiView = [[UIView alloc] init];
        [self.mainView addSubview:self.tujiView];
    }
    [self.tujiView mas_remakeConstraints:^(MASConstraintMaker *make) {
          make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 140));
          make.left.mas_equalTo(self.mainView).mas_offset(0);
          make.top.mas_equalTo(self.wendaView.mas_bottom).mas_offset(10);
    }];
    
    for (UIView * view in self.tujiView.subviews) {
        [view removeFromSuperview];
    }
    
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.text = self.matchModel.tuji_show_name;
    titleLabel.font = DHBoldFont(16);
    titleLabel.textColor = FWTextColor_222222;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.tujiView addSubview:titleLabel];
    titleLabel.frame = CGRectMake(10, 0, 300, 22);
    
    UIImageView * arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"arrow"];
    [self.tujiView addSubview:arrowImageView];
    arrowImageView.frame = CGRectMake(SCREEN_WIDTH-10-7, 10, 7, 7);
    
    UIScrollView * tujiScrollView = [[UIScrollView alloc] init];
    tujiScrollView = [[UIScrollView alloc] init];
    tujiScrollView.tag = 9977;
    tujiScrollView.delegate = self;
    tujiScrollView.showsHorizontalScrollIndicator = NO;
    [self.tujiView addSubview:tujiScrollView];
    tujiScrollView.frame = CGRectMake(0, CGRectGetMaxY(titleLabel.frame)+6, SCREEN_WIDTH, 113);
    
    
    for(int i =0 ; i<self.matchModel.list_small.count;i++){
        
        NSString * imageURL = self.matchModel.list_small[i];
        
        UIImageView * iv = [[UIImageView alloc] init];
        iv.tag = 12306+i;
        iv.layer.cornerRadius = 2;
        iv.layer.masksToBounds = YES;
        iv.frame = CGRectMake(14+178*i, 0, 168, 122);
        iv.contentMode = UIViewContentModeScaleAspectFill;
        iv.clipsToBounds = YES;
        [iv sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        iv.userInteractionEnabled = YES;
        [tujiScrollView addSubview:iv];
        [iv addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(picsViewClick:)]];
    }
    
    if (self.matchModel.list_small.count > 2) {
        DHWeakSelf;
        tujiScrollView.abLoadMore = [ABSideLoadMore loadMoreWihtBlock:^{
            [weakSelf picsMoreButtonClick];
        }];
    }else{
        tujiScrollView.abLoadMore = nil;
    }
    
    tujiScrollView.contentSize = CGSizeMake(self.matchModel.list_small.count * 178 +30, 0);
}

#pragma mark - > 初始化改装情报
- (void)setupRefitView{
   
    if (!self.refitReportView) {
        self.refitReportView = [[UIView alloc] init];
        [self.mainView addSubview:self.refitReportView];
    }
    [self.refitReportView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.mainView).mas_offset(0);
        make.top.mas_equalTo(self.tujiView.mas_bottom).mas_offset(0);
    }];
    
    for (UIView * view in self.refitReportView.subviews) {
        [view removeFromSuperview];
    }
    
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.font = DHBoldFont(16);
    titleLabel.text = @"大神车手的改装情报";
    titleLabel.textColor = FWTextColor_222222;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.refitReportView addSubview:titleLabel];
    titleLabel.frame = CGRectMake(10, 10, 300, 22);
    
    for (int i =0; i <self.matchModel.refit_list.count; i++) {
        FWRefitReportListModel * listModel = self.matchModel.refit_list[i];
        
        UIView * view = [[UIView alloc] init];
        view.tag = 22333+i;
        [self.refitReportView addSubview:view];
        [view mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.refitReportView).mas_offset(0);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 148));
            make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(5 + i*148);
            if (i == self.matchModel.refit_list.count-1) {
                make.bottom.mas_equalTo(self.refitReportView).mas_offset(-30);
            }
        }];
        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(detailButtonClick:)]];

        
        UIImageView * iconImageView = [[UIImageView alloc] init];
        iconImageView.layer.cornerRadius = 4;
        iconImageView.layer.masksToBounds = YES;
        iconImageView.clipsToBounds = YES;
        iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        [iconImageView sd_setImageWithURL:[NSURL URLWithString:listModel.feed_info.feed_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        [view addSubview:iconImageView];
        [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(view).mas_offset(10);
            make.size.mas_equalTo(CGSizeMake(130, 130));
            make.top.mas_equalTo(view).mas_offset(7);
        }];
        
        UILabel * playerLabel = [[UILabel alloc] init];
        playerLabel.text = self.refitModel.cheshou;
        playerLabel.font = DHFont(14);
        playerLabel.textColor = DHLineHexColor_LightGray_999999;
        playerLabel.textAlignment = NSTextAlignmentLeft;
        [view addSubview:playerLabel];
        [playerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(iconImageView.mas_right).mas_offset(14);
            make.size.mas_equalTo(CGSizeMake(70, 20));
            make.top.mas_equalTo(iconImageView).mas_offset(2);
        }];
        
        UILabel * playerValueLabel = [[UILabel alloc] init];
        playerValueLabel.font = DHFont(14);
        playerValueLabel.text = listModel.driver_info.f3;
        playerValueLabel.textColor = FWTextColor_222222;
        playerValueLabel.textAlignment = NSTextAlignmentLeft;
        [view addSubview:playerValueLabel];
        [playerValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(playerLabel.mas_right).mas_offset(5);
            make.height.mas_equalTo(20);
            make.width.mas_greaterThanOrEqualTo(10);
            make.right.mas_equalTo(view).mas_offset(-5);
            make.centerY.mas_equalTo(playerLabel);
        }];
        
        UILabel * carLabel = [[UILabel alloc] init];
        carLabel.text = self.refitModel.car;
        carLabel.font = DHFont(14);
        carLabel.textColor = DHLineHexColor_LightGray_999999;
        carLabel.textAlignment = NSTextAlignmentLeft;
        [view addSubview:carLabel];
        [carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(playerLabel);
            make.size.mas_equalTo(CGSizeMake(70, 20));
            make.top.mas_equalTo(playerLabel.mas_bottom).mas_offset(2);
        }];
        
        UILabel *carValueLabel = [[UILabel alloc] init];
        
        carValueLabel.font = DHFont(14);
        carValueLabel.textColor = FWTextColor_222222;
        carValueLabel.text = listModel.driver_info.f10;
        carValueLabel.textAlignment = NSTextAlignmentLeft;
        [view addSubview:carValueLabel];
        [carValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(carLabel.mas_right).mas_offset(5);
            make.height.mas_equalTo(20);
            make.width.mas_greaterThanOrEqualTo(10);
            make.right.mas_equalTo(view).mas_offset(-5);
            make.centerY.mas_equalTo(carLabel);
        }];
        
        UILabel * scoreLabel = [[UILabel alloc] init];
        scoreLabel.text = self.refitModel.bestscore;
        scoreLabel.numberOfLines = 0;
        scoreLabel.lineBreakMode = NSLineBreakByWordWrapping;
        scoreLabel.font = DHFont(14);
        scoreLabel.textColor = DHLineHexColor_LightGray_999999;
        scoreLabel.textAlignment = NSTextAlignmentLeft;
        [view addSubview:scoreLabel];
        [scoreLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(playerLabel);
            make.width.mas_greaterThanOrEqualTo(20);
            make.height.mas_equalTo(20);
            make.right.mas_equalTo(view).mas_offset(-5);
            make.top.mas_equalTo(carLabel.mas_bottom).mas_offset(2);
        }];
        
        UILabel * scoreValueLabel = [[UILabel alloc] init];
        scoreValueLabel.text = listModel.driver_info.best_score_str;
        scoreValueLabel.font = DHFont(14);
        scoreValueLabel.numberOfLines = 0;
        scoreValueLabel.lineBreakMode = NSLineBreakByClipping;
        scoreValueLabel.textColor = FWTextColor_222222;
        scoreValueLabel.textAlignment = NSTextAlignmentLeft;
        [view addSubview:scoreValueLabel];
        [scoreValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(scoreLabel);
            make.height.mas_greaterThanOrEqualTo(20);
            make.width.mas_greaterThanOrEqualTo(10);
            make.right.mas_equalTo(view).mas_offset(-5);
            make.top.mas_equalTo(scoreLabel.mas_bottom).mas_offset(2);
        }];
        
        UIButton * detailButton = [[UIButton alloc] init];
        detailButton.enabled = NO;
        detailButton.layer.borderColor = FWColor(@"D6D6D6").CGColor;
        detailButton.layer.borderWidth = 1;
        detailButton.layer.cornerRadius = 2;
        detailButton.layer.masksToBounds = YES;
        detailButton.titleLabel.font = DHFont(12);
        [detailButton setTitle:@"查看详细情报" forState:UIControlStateNormal];
        [detailButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        [view addSubview:detailButton];
        [detailButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(110, 30));
            make.left.mas_equalTo(carLabel);
            make.top.mas_equalTo(scoreValueLabel.mas_bottom).mas_offset(10);
        }];
    }
    
    UIButton * moreRefitButton = [[UIButton alloc] init];
    moreRefitButton.titleLabel.font = DHFont(12);
    [moreRefitButton setTitle:@"查看更多车手改装情报 >" forState:UIControlStateNormal];
    [moreRefitButton setTitleColor:DHLineHexColor_LightGray_999999 forState:UIControlStateNormal];
    [moreRefitButton addTarget:self action:@selector(moreRefitButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.refitReportView addSubview:moreRefitButton];
    [moreRefitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 25));
        make.centerX.mas_equalTo(self.refitReportView);
        make.bottom.mas_equalTo(self.refitReportView).mas_offset(-5);
    }];
}

#pragma mark - > 初始化关于
- (void)setupAboutView{
    
    if (!self.aboutView) {
        self.aboutView = [[UIView alloc] init];
        [self.mainView addSubview:self.aboutView];
    }
    
    if (self.matchModel.about.count<=0) {
        [self.aboutView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 0.01));
            make.left.mas_equalTo(self.mainView).mas_offset(0);
            make.top.mas_equalTo(self.refitReportView.mas_bottom).mas_offset(0);
        }];
        
        for (UIView * view in self.aboutView.subviews) {
            [view removeFromSuperview];
        }
    }else{
        
        [self.aboutView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.height.mas_greaterThanOrEqualTo(10);
            make.left.mas_equalTo(self.mainView).mas_offset(0);
            make.top.mas_equalTo(self.refitReportView.mas_bottom).mas_offset(0);
        }];

        for (UIView * view in self.aboutView.subviews) {
            [view removeFromSuperview];
        }

        UILabel * titleLabel = [[UILabel alloc] init];
        titleLabel.font = DHBoldFont(16);
        titleLabel.text = @"关于FAST4WARD";
        titleLabel.textColor = FWTextColor_222222;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.aboutView addSubview:titleLabel];
        titleLabel.frame = CGRectMake(10, 10, 300, 22);

        for (int i =0; i <self.matchModel.about.count; i++) {
            
            FWAboutModel * aboutModel = self.matchModel.about[i];
            
            UIImageView * iconImageView = [[UIImageView alloc] init];
            iconImageView.tag = 33333+i;
            iconImageView.clipsToBounds = YES;
            iconImageView.userInteractionEnabled = YES;
            iconImageView.layer.cornerRadius = 3;
            iconImageView.layer.masksToBounds = YES;
            iconImageView.contentMode = UIViewContentModeScaleAspectFill;
            [iconImageView sd_setImageWithURL:[NSURL URLWithString:aboutModel.logo] placeholderImage:[UIImage imageNamed:@"placehoder"]];
            [self.aboutView addSubview:iconImageView];
            [iconImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(aboutImageClick:)]];

            if (i == 0) {
                [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(self.aboutView).mas_offset(10);
                    make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(10);
                    make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-30)/2, (SCREEN_WIDTH-30)/2));
                    make.bottom.mas_equalTo(self.aboutView).mas_offset(-10);
                }];
            }else if(i == 1){
                [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_equalTo(self.aboutView).mas_offset(-10);
                    make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(10);
                    make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-30)/2, ((SCREEN_WIDTH-30)/2-10)/2));
                }];
            }else if (i == 2){
                [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_equalTo(self.aboutView).mas_offset(-10);
                    make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(((SCREEN_WIDTH-30)/2-10)/2+20);
                    make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-30)/2, ((SCREEN_WIDTH-30)/2-10)/2));
                }];
            }
            
            UILabel * contentLabel = [[UILabel alloc] init];
            contentLabel.text = aboutModel.title;
            contentLabel.font = DHBoldFont(14);
            contentLabel.textColor = FWViewBackgroundColor_FFFFFF;
            contentLabel.textAlignment = NSTextAlignmentLeft;
            [iconImageView addSubview:contentLabel];
            [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(iconImageView).mas_offset(5);
                make.right.mas_equalTo(iconImageView).mas_offset(-5);
                make.height.mas_equalTo(20);
                make.bottom.mas_equalTo(iconImageView);
                make.width.mas_greaterThanOrEqualTo(20);
            }];
        }
    }
}

#pragma mark - > 初始化视频
- (void)setupVideosView{
    
    if (!self.videosView) {
        self.videosView = [[UIView alloc] init];
        [self.mainView addSubview:self.videosView];
    }
    
    [self.videosView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mainView);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-30);
        make.top.mas_equalTo(self.aboutView.mas_bottom).mas_offset(20);
    }];
    
    for (UIView * view in self.videosView.subviews) {
        [view removeFromSuperview];
    }
    
    if (self.matchModel.vod_list.count > 0) {
        
        UILabel * titleLabel = [[UILabel alloc] init];
        titleLabel.font = DHBoldFont(16);
        titleLabel.text = @"FAST4WARD官方视频";
        titleLabel.textColor = FWTextColor_222222;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.videosView addSubview:titleLabel];
        titleLabel.frame = CGRectMake(10, 0, 300, 22);
        
        for (int i =0; i <self.matchModel.vod_list.count; i++) {
            
            FWVodListModel * vodListModel = self.matchModel.vod_list[i];

            UIImageView * iconImageView = [[UIImageView alloc] init];
            iconImageView.tag = 99999+i;
            iconImageView.clipsToBounds = YES;
            iconImageView.layer.cornerRadius = 3;
            iconImageView.layer.masksToBounds = YES;
            iconImageView.contentMode = UIViewContentModeScaleAspectFill;
            iconImageView.userInteractionEnabled = YES;
            [iconImageView sd_setImageWithURL:[NSURL URLWithString:vodListModel.feed_cover] placeholderImage:[UIImage imageNamed:@"placehoder"]];
            [self.videosView addSubview:iconImageView];
            [iconImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoClick:)]];

            [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.videosView).mas_offset(10);
                make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-20, 200));
                make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(210*i+10);
                if (i == self.matchModel.vod_list.count-1) {
                    make.bottom.mas_equalTo(self.videosView).mas_offset(-40);
                }
            }];
            
            UIImageView * videoIconImageView = [[UIImageView alloc] init];
            videoIconImageView.image = [UIImage imageNamed:@"match_video_play"];
            [iconImageView addSubview:videoIconImageView];
            [videoIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(44, 44));
                make.centerY.centerX.mas_equalTo(iconImageView);
            }];
            
            UILabel * contentLabel = [[UILabel alloc] init];
            contentLabel.text = vodListModel.feed_title;
            contentLabel.font = DHBoldFont(15);
            contentLabel.textColor = FWViewBackgroundColor_FFFFFF;
            contentLabel.textAlignment = NSTextAlignmentLeft;
            [iconImageView addSubview:contentLabel];
            [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(iconImageView).mas_offset(10);
                make.right.mas_equalTo(iconImageView).mas_offset(-5);
                make.height.mas_equalTo(20);
                make.bottom.mas_equalTo(iconImageView).mas_offset(-14);
                make.width.mas_greaterThanOrEqualTo(20);
            }];
        }
        
         UIButton * moreVideoButton = [[UIButton alloc] init];
         moreVideoButton.titleLabel.font = DHFont(12);
         [moreVideoButton setTitle:@"查看更多官方视频 >" forState:UIControlStateNormal];
         [moreVideoButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
         [moreVideoButton addTarget:self action:@selector(moreVideoButtonClick) forControlEvents:UIControlEventTouchUpInside];
         [self.videosView addSubview:moreVideoButton];
         [moreVideoButton mas_remakeConstraints:^(MASConstraintMaker *make) {
             make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 25));
             make.centerX.mas_equalTo(self.videosView);
             make.bottom.mas_equalTo(self.videosView).mas_offset(-5);
         }];
    }
}

- (void)liveButtonOnClick{
    // 直播
    if ([self.matchModel.live_show_status isEqualToString:@"2"]) {

        /* 直播结束或未开始 */
        [[FWHudManager sharedManager] showErrorMessage:self.matchModel.live_info.live_status_desc toController:self.vc];
    }else if ([self.matchModel.live_show_status isEqualToString:@"1"]){
        /* 直播中 */
        FWLiveViewController * LVC = [[FWLiveViewController alloc] init];
        LVC.isLive = YES;
        LVC.sport_id = self.matchModel.sport_id;
        LVC.finalModel = self.matchModel.live_info;
        LVC.roomId = self.matchModel.live_info.chatroom_id;
        LVC.currentIndex = 0;
        [self.vc.navigationController pushViewController:LVC animated:YES];
    }else{
        FWRealScroeAndRankViewController * RARVC = [[FWRealScroeAndRankViewController alloc] init];
        RARVC.sport_id = self.matchModel.sport_id;
        RARVC.share_image = self.matchModel.live_info.live_image;
        [self.vc.navigationController pushViewController:RARVC animated:YES];
    }
}

#pragma mark - > 门票 、 回放 、排行榜、车手报名
- (void)topButtonClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 11111;
    
    if (index == 0){
        // 赛事门票
        [self.vc.navigationController pushViewController:[FWMatchTicketViewController new] animated:YES];
    }else if (index == 1){
        // 车手报名
        FWRegistrationHomeViewController * RHVC = [[FWRegistrationHomeViewController alloc] init];
        RHVC.sport_status = self.matchModel.sport_status;
        [self.vc.navigationController pushViewController:RHVC animated:YES];
    }else if (index == 2){
        // 车友排行榜
        [self.vc.navigationController pushViewController:[FWPlayerRankViewController new] animated:YES];
    }else if (index == 3){
        // 回放
        [self.vc.navigationController pushViewController:[FWMatchListViewController new] animated:YES];
    }
}

- (void)congifViewWithModel:(id)model withRefitListModel:(nonnull id)refitModel{
    
    self.matchModel = (FWF4WMatchModel *)model;
    self.refitModel = (FWRefitListKeyModel *)refitModel;
    
    FWModuleButton * menpiaoButton = [self.convientView viewWithTag:1111];
    FWModuleButton * baomingButton = [self.convientView viewWithTag:1112];

    if ([self.matchModel.ticket_status isEqualToString:@"1"]) {
        /* 出售门票 */
        menpiaoButton.iconImageView.image = [UIImage imageNamed:@"match_sale_tickets"];
    }else{
        menpiaoButton.iconImageView.image = [UIImage imageNamed:@"match_tickets"];
    }
    
    if ([self.matchModel.baoming_status isEqualToString:@"1"]) {
        /* 开启报名 */
        baomingButton.iconImageView.image = [UIImage imageNamed:@"match_player_signuping"];
    }else{
        baomingButton.iconImageView.image = [UIImage imageNamed:@"match_player_signup"];
    }
    
    [self setupLiveView];
    [self setupAskView];
    [self setupTujiView];
    [self setupRefitView];
    [self setupAboutView];
    [self setupVideosView];
}

#pragma mark - > 问答
- (void)contentLabelClick:(UITapGestureRecognizer *)tap{

    NSInteger index = tap.view.tag - 12321;
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.tags_id = self.matchModel.list_tag[index].tag_id;
    [self.vc.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 改装情报
- (void)detailButtonClick:(UITapGestureRecognizer *)tap{
 
    NSInteger index = tap.view.tag - 22333;
    
    FWRefitReportDetailViewController * RRDVC = [[FWRefitReportDetailViewController alloc] init];
    RRDVC.feed_id = self.matchModel.refit_list[index].feed_info.feed_id;
    RRDVC.bestet_id = self.matchModel.refit_list[index].driver_info.bestet_id;
    [self.vc.navigationController pushViewController:RRDVC animated:YES];
}

#pragma mark -> 图集单张查看
- (void)picsViewClick:(UITapGestureRecognizer *)tap{
    
    int index = (int)(tap.view.tag -12306);
    
    if (self.matchModel.list_original.count > 0 && index < self.matchModel.list_original.count) {
        
        FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
        browser.isFullWidthForLandScape = YES;
        browser.isNeedLandscape = YES;
        browser.currentImageIndex = index;
        browser.imageArray = [self.matchModel.list_small mutableCopy];
        browser.smallArray = [self.matchModel.list_small copy];
        browser.originalImageArray = self.matchModel.list_original;
        browser.vc = self.vc;
        browser.fromType = 3;
        [browser show];
    }
}

#pragma mark - > 更多图集
- (void)picsMoreButtonClick{
    
    FWMatchPicsViewController * vc = [FWMatchPicsViewController new];
    vc.tuiji_id = @"0";
    [self.vc.navigationController pushViewController:vc animated:YES];
}

#pragma mark - > 更多官方视频
- (void)moreVideoButtonClick{
    [self.vc.navigationController pushViewController:[FWOfficialVideoViewController new] animated:YES];
}

#pragma mark - > 更多改装案例
- (void)moreRefitButtonClick{
    
    FWRefitReportViewController * RRVC = [[FWRefitReportViewController alloc] init];
    [self.vc.navigationController pushViewController:RRVC animated:YES];
}

#pragma mark - > 关于fast4ward
- (void)aboutImageClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 33333;
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.matchModel.about[index].href;
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 查看视频
- (void)videoClick:(UITapGestureRecognizer *)tap{

    NSInteger index = tap.view.tag - 99999;

    if ([self.matchModel.vod_list[index].is_long_video isEqualToString:@"1"]) {
        /* 长视频 */
        FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
        LPVC.feed_id = self.matchModel.vod_list[index].feed_id;
        LPVC.myBlock = ^(FWFeedListModel *listModel) {};
        [self.vc.navigationController pushViewController:LPVC animated:YES];
    }else{
        // 视频流
        FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
        controller.currentIndex = 0;
        controller.feed_id = self.matchModel.vod_list[index].feed_id;
        controller.requestType = FWPushMessageRequestType;
        [self.vc.navigationController pushViewController:controller animated:YES];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    UIScrollView * tujiScrollView = [self viewWithTag:9977];
    if (scrollView == tujiScrollView) {
        CGFloat itemWidth = 178;
        
        NSInteger pageWidth = itemWidth + 10;
        
        CGFloat offSetX = scrollView.contentOffset.x+SCREEN_WIDTH;
        
        NSInteger pageNum = offSetX/pageWidth;

        if (pageNum == self.matchModel.list_small.count && offSetX >pageNum *178 + 80 ) {
            
            [self picsMoreButtonClick];
            return;
        }
    }
}
@end
