//
//  FWGuessingCompetitionCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWGuessingModel.h"

NS_ASSUME_NONNULL_BEGIN
@class FWGuessingCompetitionCell;

@protocol FWGuessingCompetitionCellDelegate <NSObject>

- (void)showButtonClickShowMore:(BOOL)isShowMore  withCell:(FWGuessingCompetitionCell *)cell withIndex:(NSInteger)index;
- (void)submitSportGuessWithType:(NSString *)type withIndex:(NSInteger)index;

@end

@interface FWGuessingCompetitionCell : UITableViewCell

@property (nonatomic, strong) UIView * bgView;

/* 赛事组别 */
@property (nonatomic, strong) UILabel * titleLabel;
/* 左侧头像 */
@property (nonatomic, strong) UIImageView * leftPhotoImageView;
/* 右侧头像 */
@property (nonatomic, strong) UIImageView * rightPhotoImageView;
/* 左侧名称 */
@property (nonatomic, strong) UILabel * leftNameLabel;
/* 右侧名称 */
@property (nonatomic, strong) UILabel * rightNameLabel;
/* 左侧车号 */
@property (nonatomic, strong) UILabel * leftNoLabel;
/* 右侧车号 */
@property (nonatomic, strong) UILabel * rightNoLabel;
/* 左侧车 */
@property (nonatomic, strong) UILabel * leftCarLabel;
/* 右侧车 */
@property (nonatomic, strong) UILabel * rightCarLabel;
/* 左侧支持率 */
@property (nonatomic, strong) UILabel * leftPercentLabel;
/* 右侧支持率 */
@property (nonatomic, strong) UILabel * rightPercentLabel;

@property (nonatomic, strong) UIView * lineView;
/* 状态 */
@property (nonatomic, strong) UILabel * statusLabel;
/* 支持率（固定文案） */
@property (nonatomic, strong) UILabel * zhichilvLabel;
/* 猜输赢（固定文案） */
@property (nonatomic, strong) UILabel * caiLabel;
/* 隐藏还是显示 */
@property (nonatomic, strong) UIButton * showButton;
@property (nonatomic, strong) UIImageView * showImageView;

/* 左侧赔率 */
@property (nonatomic, strong) UILabel * leftPeilvLabel;
/* 右侧赔率 */
@property (nonatomic, strong) UILabel * rightPeilvLabel;

/* 左侧投注 */
@property (nonatomic, strong) UIButton * leftTouzhuButton;
/* 右侧投注 */
@property (nonatomic, strong) UIButton * rightTouzhuButton;

/* 已投注 */
@property (nonatomic, strong) UILabel * touzhuLabel;
/* 预计金币 */
@property (nonatomic, strong) UILabel * yujiLabel;

/* 左赢图标 */
@property (nonatomic, strong) UIImageView * leftWinImageView;
/* 右赢图标 */
@property (nonatomic, strong) UIImageView * rightWinImageView;

@property (nonatomic, strong) FWGuessingListModel * listModel;

@property (nonatomic, weak) UIViewController * vc;
/* 当前可用余额 */
@property (nonatomic, strong) NSString * gold_balance_value;

@property (nonatomic, weak) id <FWGuessingCompetitionCellDelegate>delegate;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
