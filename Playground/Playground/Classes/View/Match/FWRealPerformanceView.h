//
//  FWRealPerformanceView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWMatchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWRealPerformanceView : UITableViewHeaderFooterView

+ (instancetype)topicHeaderView;
+ (instancetype)headerViewWithTableView:(UITableView *)tableView;

@property (nonatomic, strong) UILabel * nameLabel;

- (void)configForModel:(id)model;

@end

NS_ASSUME_NONNULL_END
