//
//  FWPlayerSignupView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWPlayerSignupModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWPlayerSignupViewDelegate <NSObject>

- (void)payButtonClick:(NSString *)payMethod;

@end

@interface FWPlayerSignupView : UIScrollView

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIView * topView;
@property (nonatomic, strong) UIImageView * picImageView;
@property (nonatomic, strong) UILabel * resultLabel;
@property (nonatomic, strong) UILabel * tipLabel;

@property (nonatomic, strong) UIView * middleView;
@property (nonatomic, strong) UILabel * confirmLabel;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * typeLabel;
@property (nonatomic, strong) UILabel * sumLabel;
@property (nonatomic, strong) UILabel * sumNumLabel;

@property (nonatomic, strong) UIView * thirdView;
@property (nonatomic, strong) UIButton * selectButton;
@property (nonatomic, strong) UILabel * taocanLabel;
@property (nonatomic, strong) UIImageView * thirdRightView;


@property (nonatomic, strong) FWPlayerSignupModel * signupModel;

@property (nonatomic, strong) UIView * bottomView;

@property (nonatomic, strong) UIButton * payButton;

@property (nonatomic, weak) id<FWPlayerSignupViewDelegate>signupDelegate;

@property (nonatomic, weak) UIViewController * vc;


- (void)configForView:(FWPlayerSignupModel *)model;

@end

NS_ASSUME_NONNULL_END
