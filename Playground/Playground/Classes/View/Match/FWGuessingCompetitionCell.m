//
//  FWGuessingCompetitionCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGuessingCompetitionCell.h"
#import "FWGuessingBettingViewController.h"

@implementation FWGuessingCompetitionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.contentView.backgroundColor = FWTextColor_FAFAFA;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{

    self.bgView = [[UIView alloc] init];
    self.bgView.layer.cornerRadius = 2;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.contentView addSubview:self.bgView];
    self.bgView.frame = CGRectMake(14, 0, SCREEN_WIDTH-28, 127);
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.titleLabel.textColor = FWTextColor_BCBCBC;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(9);
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(18);
    }];
    
    self.statusLabel = [[UILabel alloc] init];
    self.statusLabel.layer.cornerRadius = 2;
    self.statusLabel.layer.masksToBounds = YES;
    self.statusLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.statusLabel];
    [self.statusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(13);
        make.centerX.mas_equalTo(self.bgView);
        make.width.mas_equalTo(46);
        make.height.mas_equalTo(18);
    }];
    
    self.zhichilvLabel = [[UILabel alloc] init];
    self.zhichilvLabel.text = @"支持率";
    self.zhichilvLabel.textColor = FWTextColor_222222;
    self.zhichilvLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.zhichilvLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.zhichilvLabel];
    [self.zhichilvLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statusLabel.mas_bottom).mas_offset(7);
        make.centerX.mas_equalTo(self.bgView);
        make.width.mas_equalTo(46);
        make.height.mas_equalTo(18);
    }];
    
    self.leftPhotoImageView = [[UIImageView alloc] init];
    self.leftPhotoImageView.layer.cornerRadius = 15;
    self.leftPhotoImageView.layer.masksToBounds = YES;
    self.leftPhotoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.bgView addSubview:self.leftPhotoImageView];
    [self.leftPhotoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.left.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(8);
    }];
    
    self.leftWinImageView = [[UIImageView alloc] init];
    self.leftWinImageView.image = [UIImage imageNamed:@"guess_win"];
    [self.bgView addSubview:self.leftWinImageView];
    [self.leftWinImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(17, 17));
        make.right.mas_equalTo(self.leftPhotoImageView).mas_offset(5);
        make.top.mas_equalTo(self.leftPhotoImageView).mas_offset(-5);
    }];
    self.leftWinImageView.hidden = YES;
    
    self.leftNoLabel = [[UILabel alloc] init];
    self.leftNoLabel.font = DHSystemFontOfSize_13;
    self.leftNoLabel.textColor = FWTextColor_222222;
    self.leftNoLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftNoLabel];
    [self.leftNoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftPhotoImageView.mas_right).mas_offset(9);
        make.top.mas_equalTo(self.leftPhotoImageView).mas_offset((-2));
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(30);
    }];
    
    self.leftNameLabel = [[UILabel alloc] init];
    self.leftNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.leftNameLabel.textColor = FWTextColor_222222;
    self.leftNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftNameLabel];
    [self.leftNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftNoLabel.mas_right).mas_offset(0);
        make.right.mas_equalTo(self.zhichilvLabel.mas_left).mas_offset(-5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(self.leftNoLabel);
        make.height.mas_equalTo(16);
    }];
    
    self.leftCarLabel = [[UILabel alloc] init];
    self.leftCarLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.leftCarLabel.textColor = FWTextColor_BCBCBC;
    self.leftCarLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftCarLabel];
    [self.leftCarLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.leftPhotoImageView).mas_offset(2);
        make.left.mas_equalTo(self.leftNoLabel);
        make.right.mas_equalTo(self.zhichilvLabel.mas_left).mas_offset(-5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(16);
    }];
    
    self.leftPercentLabel = [[UILabel alloc] init];
    self.leftPercentLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.leftPercentLabel.textColor = FWViewBackgroundColor_FF6F00;
    self.leftPercentLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftPercentLabel];
    [self.leftPercentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leftPhotoImageView.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(self.leftPhotoImageView);
        make.right.mas_equalTo(self.zhichilvLabel.mas_left).mas_offset(-5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(16);
    }];
    
    
    self.rightPhotoImageView = [[UIImageView alloc] init];
    self.rightPhotoImageView.layer.cornerRadius = 15;
    self.rightPhotoImageView.layer.masksToBounds = YES;
    self.rightPhotoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.bgView addSubview:self.rightPhotoImageView];
    [self.rightPhotoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.leftPhotoImageView);
        make.right.mas_equalTo(self.titleLabel);
        make.centerY.mas_equalTo(self.leftPhotoImageView);
    }];
    
    self.rightWinImageView = [[UIImageView alloc] init];
    self.rightWinImageView.image = [UIImage imageNamed:@"guess_win"];
    [self.bgView addSubview:self.rightWinImageView];
    [self.rightWinImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(17, 17));
        make.right.mas_equalTo(self.rightPhotoImageView).mas_offset(5);
        make.top.mas_equalTo(self.rightPhotoImageView).mas_offset(-5);
    }];
    self.rightWinImageView.hidden = YES;
    
    self.rightNoLabel = [[UILabel alloc] init];
    self.rightNoLabel.font = DHSystemFontOfSize_13;
    self.rightNoLabel.textColor = FWTextColor_222222;
    self.rightNoLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.rightNoLabel];
    [self.rightNoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_greaterThanOrEqualTo(self.statusLabel.mas_right).mas_offset(3);
        make.top.mas_equalTo(self.rightPhotoImageView).mas_offset(-2);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(30);
    }];
    
    self.rightNameLabel = [[UILabel alloc] init];
    self.rightNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.rightNameLabel.textColor = FWTextColor_222222;
    self.rightNameLabel.textAlignment = NSTextAlignmentRight;
    [self.bgView addSubview:self.rightNameLabel];
    [self.rightNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.rightNoLabel);
        make.right.mas_equalTo(self.rightPhotoImageView.mas_left).mas_offset(-3);
        make.left.mas_equalTo(self.rightNoLabel.mas_right).mas_offset(2);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(16);
    }];
    
    [self.rightNoLabel setContentHuggingPriority:UILayoutPriorityDefaultLow
                              forAxis:UILayoutConstraintAxisHorizontal];
    [self.rightNameLabel setContentHuggingPriority:UILayoutPriorityRequired
                              forAxis:UILayoutConstraintAxisHorizontal];
    
    self.rightCarLabel = [[UILabel alloc] init];
    self.rightCarLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.rightCarLabel.textColor = FWTextColor_BCBCBC;
    self.rightCarLabel.textAlignment = NSTextAlignmentRight;
    [self.bgView addSubview:self.rightCarLabel];
    [self.rightCarLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.rightPhotoImageView).mas_offset(2);
        make.right.mas_equalTo(self.rightNameLabel);
        make.left.mas_equalTo(self.zhichilvLabel.mas_right).mas_offset(5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(16);
    }];
    
    self.rightPercentLabel = [[UILabel alloc] init];
    self.rightPercentLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.rightPercentLabel.textColor = FWViewBackgroundColor_FF6F00;
    self.rightPercentLabel.textAlignment = NSTextAlignmentRight;
    [self.bgView addSubview:self.rightPercentLabel];
    [self.rightPercentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.rightPhotoImageView.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(self.rightPhotoImageView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(16);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = FWColor(@"e9e9e9");
    [self.bgView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftPhotoImageView);
        make.right.mas_equalTo(self.rightPhotoImageView);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.leftPercentLabel.mas_bottom).mas_offset(10);
    }];
    
    self.caiLabel = [[UILabel alloc] init];
    self.caiLabel.text = @"猜输赢";
    self.caiLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.caiLabel.textColor = FWTextColor_BCBCBC;
    self.caiLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.caiLabel];
    [self.caiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.lineView.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(self.leftPhotoImageView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(16);
    }];
    
    self.showButton = [[UIButton alloc] init];
    [self.showButton addTarget:self action:@selector(showButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.showButton];
    [self.showButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView);
        make.left.right.mas_equalTo(self.bgView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 117));
    }];

    self.showImageView = [[UIImageView alloc] init];
    self.showImageView.image = [UIImage imageNamed:@"show_down"];
    [self.showButton addSubview:self.showImageView];
    [self.showImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(11, 6));
        make.right.mas_equalTo(self.showButton).mas_offset(-14);
        make.bottom.mas_equalTo(self.showButton).mas_offset(-5);
    }];
    
    
    self.leftTouzhuButton = [[UIButton alloc] init];
    self.leftTouzhuButton.layer.cornerRadius = 2;
    self.leftTouzhuButton.layer.masksToBounds = YES;
    self.leftTouzhuButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.leftTouzhuButton.backgroundColor = FWViewBackgroundColor_E5E5E5;
    [self.leftTouzhuButton setTitle:@" 下注" forState:UIControlStateNormal];
    [self.leftTouzhuButton setImage:[UIImage imageNamed:@"xiazhu"] forState:UIControlStateNormal];
    [self.leftTouzhuButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.leftTouzhuButton addTarget:self action:@selector(leftPeilvClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.leftTouzhuButton];
    [self.leftTouzhuButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.statusLabel.mas_left);
        make.top.mas_equalTo(self.caiLabel.mas_bottom).mas_offset(5);;
        make.size.mas_equalTo(CGSizeMake(67, 26));
    }];
    
    self.leftPeilvLabel = [[UILabel alloc] init];
    self.leftPeilvLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.leftPeilvLabel.textColor = FWViewBackgroundColor_FF6F00;
    self.leftPeilvLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.leftPeilvLabel];
    [self.leftPeilvLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.leftTouzhuButton);
        make.left.mas_equalTo(self.leftPhotoImageView);
        make.right.mas_equalTo(self.leftTouzhuButton.mas_left);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];

    self.rightTouzhuButton = [[UIButton alloc] init];
    self.rightTouzhuButton.layer.cornerRadius = 2;
    self.rightTouzhuButton.layer.masksToBounds = YES;
    self.rightTouzhuButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.rightTouzhuButton.backgroundColor = FWViewBackgroundColor_E5E5E5;
    [self.rightTouzhuButton setTitle:@" 下注" forState:UIControlStateNormal];
    [self.rightTouzhuButton setImage:[UIImage imageNamed:@"xiazhu"] forState:UIControlStateNormal];
    [self.rightTouzhuButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.rightTouzhuButton addTarget:self action:@selector(rightPeilvLabelClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.rightTouzhuButton];
    [self.rightTouzhuButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.statusLabel.mas_right);
        make.centerY.mas_equalTo(self.leftTouzhuButton);
        make.size.mas_equalTo(self.leftTouzhuButton);
    }];
    
    self.rightPeilvLabel = [[UILabel alloc] init];
    self.rightPeilvLabel.layer.cornerRadius = 2;
    self.rightPeilvLabel.layer.masksToBounds = YES;
    self.rightPeilvLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.rightPeilvLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.rightPeilvLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.rightPeilvLabel];
    [self.rightPeilvLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.rightTouzhuButton);
        make.left.mas_equalTo(self.rightTouzhuButton.mas_right);
        make.right.mas_equalTo(self.rightPhotoImageView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];

    
    self.touzhuLabel = [[UILabel alloc] init];
    self.touzhuLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.touzhuLabel.textColor = FWTextColor_BCBCBC;
    self.touzhuLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.touzhuLabel];
    [self.touzhuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.caiLabel.mas_bottom).mas_offset(5);;
        make.left.mas_equalTo(self.leftPhotoImageView);
        make.right.mas_equalTo(self.rightPhotoImageView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];
    
    self.yujiLabel = [[UILabel alloc] init];
    self.yujiLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.yujiLabel.textColor = FWTextColor_BCBCBC;
    self.yujiLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.yujiLabel];
    [self.yujiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.touzhuLabel.mas_bottom).mas_offset(0);
        make.left.mas_equalTo(self.leftPhotoImageView);
        make.right.mas_equalTo(self.rightPhotoImageView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];
}

- (void)configForView:(id)model{
    
    self.listModel = (FWGuessingListModel *)model;
    
    if (self.listModel.showMore) {
        self.showImageView.image = [UIImage imageNamed:@"show_up"];
    }else{
        self.showImageView.image = [UIImage imageNamed:@"show_down"];
    }
    
    if ([self.listModel.status isEqualToString:@"1"]) {
        /* 进行中 */
        self.statusLabel.text = @"进行中";
        self.statusLabel.layer.borderColor = FWClearColor.CGColor;
        self.statusLabel.backgroundColor = FWViewBackgroundColor_FF6F00;
        self.statusLabel.textColor = FWViewBackgroundColor_FFFFFF;
    }else if ([self.listModel.status isEqualToString:@"2"]) {
        /* 已封盘 */
        self.statusLabel.text = @"已封盘";
        self.statusLabel.layer.borderWidth = 1;
        self.statusLabel.layer.borderColor = FWViewBackgroundColor_FF6F00.CGColor;
        self.statusLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.statusLabel.textColor = FWViewBackgroundColor_FF6F00;
    }else if ([self.listModel.status isEqualToString:@"3"]) {
        /* 未开始 */
    }else if ([self.listModel.status isEqualToString:@"4"]) {
        /* 已结束 */
        self.statusLabel.text = @"已结束";
        self.statusLabel.backgroundColor = FWViewBackgroundColor_E5E5E5;
        self.statusLabel.textColor = FWViewBackgroundColor_FFFFFF;
        self.statusLabel.layer.borderColor = FWClearColor.CGColor;
    }
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@ %@",self.listModel.group_name,self.listModel.phase_name];
    self.leftNameLabel.text = self.listModel.car_1_info.nickname;
    self.leftCarLabel.text = self.listModel.car_1_info.car_type;
    self.leftPercentLabel.text = self.listModel.car_1_info.rate;
    self.leftNoLabel.text = self.listModel.car_1_info.car_no;

    self.rightNameLabel.text = self.listModel.car_2_info.nickname;
    self.rightCarLabel.text = self.listModel.car_2_info.car_type;
    self.rightPercentLabel.text = self.listModel.car_2_info.rate;
    self.rightNoLabel.text = self.listModel.car_2_info.car_no;

    NSString * leftName = self.leftNameLabel.text;
    NSString * rightName = self.rightNameLabel.text;

    NSInteger nameLength = 4;
    if (SCREEN_WIDTH > 375) {
        nameLength = 6;
    }
    
    if (leftName.length > nameLength) {
        leftName = [NSString stringWithFormat:@"%@...",[leftName substringToIndex:nameLength]];
    }
    
    if (rightName.length > nameLength) {
        rightName = [NSString stringWithFormat:@"%@...",[rightName substringToIndex:nameLength]];
    }
    self.leftPeilvLabel.text = [NSString stringWithFormat:@"赔率%@",self.listModel.car_1_info.odds];
    self.rightPeilvLabel.text = [NSString stringWithFormat:@"赔率%@",self.listModel.car_2_info.odds];
    
    self.leftWinImageView.hidden = YES;
    self.rightWinImageView.hidden = YES;
    
    if ([self.listModel.car_1_info.win_status isEqualToString:@"1"]) {
        self.leftWinImageView.hidden = NO;
    }
    if ([self.listModel.car_2_info.win_status isEqualToString:@"1"]) {
        self.rightWinImageView.hidden = NO;
    }
    
    if ([self.listModel.bid_status isEqualToString:@"1"]) {
        /* 已下注 */
        
        if (!self.listModel.showMore) {
            self.leftPeilvLabel.hidden = YES;
            self.leftTouzhuButton.hidden = YES;
            self.rightTouzhuButton.hidden = YES;
            self.rightPeilvLabel.hidden = YES;
            self.touzhuLabel.hidden = YES;
            self.yujiLabel.hidden = YES;
        }else{
            self.touzhuLabel.hidden = NO;
            self.yujiLabel.hidden = NO;

            if ([self.listModel.bid_car_no isEqualToString:self.listModel.car_1_info.car_no]) {
                /* 押了左边 */
                
                if ([self.listModel.status isEqualToString:@"4"]) {
                    /* 已结束 */
                    self.touzhuLabel.textColor = FWColor(@"bcbcbc");
                    self.yujiLabel.textColor = FWColor(@"bcbcbc");
                }else if ([self.listModel.status isEqualToString:@"2"]){
                    self.touzhuLabel.textColor = FWColorWihtAlpha(@"ff6f00", 0.6);
                    self.yujiLabel.textColor = FWColorWihtAlpha(@"ff6f00", 0.6);
                }else{
                    self.touzhuLabel.textColor = FWViewBackgroundColor_FF6F00;
                    self.yujiLabel.textColor = FWViewBackgroundColor_FF6F00;
                }
                
                self.leftPeilvLabel.hidden = YES;
                self.leftTouzhuButton.hidden = YES;
                self.rightPeilvLabel.hidden = YES;
                self.rightTouzhuButton.hidden = YES;

                self.touzhuLabel.textAlignment = NSTextAlignmentLeft;
                self.yujiLabel.textAlignment = NSTextAlignmentLeft;

                self.touzhuLabel.text = [NSString stringWithFormat:@"·已投注 %@：%@金币 · 当前赔率：%@",self.listModel.car_1_info.nickname,self.listModel.bid_value,self.listModel.car_1_info.odds];
                self.yujiLabel.text = [NSString stringWithFormat:@"·预计可得：%@金币 ",@(floor([self.listModel.bid_value  integerValue] * [self.listModel.car_1_info.odds floatValue])).stringValue];
            }else  {
                /* 押了右边 */
                if ([self.listModel.status isEqualToString:@"4"]) {
                    /* 已结束 */
                    self.touzhuLabel.textColor = FWColor(@"bcbcbc");
                    self.yujiLabel.textColor = FWColor(@"bcbcbc");
                }else if ([self.listModel.status isEqualToString:@"2"]){
                    self.touzhuLabel.textColor = FWColorWihtAlpha(@"ff6f00", 0.6);
                    self.yujiLabel.textColor = FWColorWihtAlpha(@"ff6f00", 0.6);
                }else{
                    self.touzhuLabel.textColor = FWViewBackgroundColor_FF6F00;
                    self.yujiLabel.textColor = FWViewBackgroundColor_FF6F00;
                }
                
                self.leftPeilvLabel.hidden = YES;
                self.leftTouzhuButton.hidden = YES;
                self.rightPeilvLabel.hidden = YES;
                self.rightTouzhuButton.hidden = YES;

                self.touzhuLabel.textAlignment = NSTextAlignmentRight;
                self.yujiLabel.textAlignment = NSTextAlignmentRight;

                self.touzhuLabel.text = [NSString stringWithFormat:@"·已投注 %@：%@金币 · 当前赔率：%@",self.listModel.car_2_info.nickname,self.listModel.bid_value,self.listModel.car_2_info.odds];
                self.yujiLabel.text = [NSString stringWithFormat:@"·预计可得：%@金币 ",@(floor([self.listModel.bid_value  integerValue] * [self.listModel.car_2_info.odds floatValue])).stringValue];
            }
        }
    }else{
        /* 未下注 */
        if (!self.listModel.showMore) {
            self.leftPeilvLabel.hidden = YES;
            self.leftTouzhuButton.hidden = YES;
            self.rightTouzhuButton.hidden = YES;
            self.rightPeilvLabel.hidden = YES;
            self.touzhuLabel.hidden = YES;
            self.yujiLabel.hidden = YES;
        }else{
            if ([self.listModel.status isEqualToString:@"4"]) {
                self.leftPeilvLabel.textColor = FWColor(@"bcbcbc");
                self.leftTouzhuButton.backgroundColor = FWColor(@"e5e5e5");
                self.rightTouzhuButton.backgroundColor = FWColor(@"e5e5e5");
                self.rightPeilvLabel.textColor = FWColor(@"bcbcbc");
            }else if ([self.listModel.status isEqualToString:@"2"]) {
                self.leftPeilvLabel.textColor = FWColorWihtAlpha(@"ff6f00", 0.6);
                self.rightPeilvLabel.textColor = FWColorWihtAlpha(@"ff6f00", 0.6);
                self.leftTouzhuButton.backgroundColor = FWColorWihtAlpha(@"ff6f00", 0.6);
                self.rightTouzhuButton.backgroundColor = FWColorWihtAlpha(@"ff6f00", 0.6);
            }else{
                self.leftPeilvLabel.textColor = FWViewBackgroundColor_FF6F00;
                self.rightPeilvLabel.textColor = FWViewBackgroundColor_FF6F00;
                self.leftTouzhuButton.backgroundColor = FWViewBackgroundColor_FF6F00;
                self.rightTouzhuButton.backgroundColor = FWViewBackgroundColor_FF6F00;
            }
            self.leftPeilvLabel.hidden = NO;
            self.rightPeilvLabel.hidden = NO;
            self.leftTouzhuButton.hidden = NO;
            self.rightTouzhuButton.hidden = NO;
            self.touzhuLabel.text = @"";
            self.yujiLabel.text = @"";
            
            self.leftTouzhuButton.enabled = YES;
            self.rightTouzhuButton.enabled = YES;
        }
    }

    
    if (!self.listModel.showMore) {
        self.bgView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, 127);
    }else if(self.listModel.showMore && [self.listModel.bid_status isEqual:@"3"]){
        self.bgView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, 161);
    }else if (self.listModel.showMore && [self.listModel.bid_status isEqual:@"1"]){
        self.bgView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, 181);
    }
    
    [self.leftPhotoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.car_1_info.header] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    [self.rightPhotoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.car_2_info.header] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
}

#pragma mark - > 压左边
- (void)leftPeilvClick:(UIButton *)sender{

    if ([self.listModel.status isEqualToString:@"1"] &&
        ![self.listModel.bid_status isEqualToString:@"1"]) {
        /* 进行中 && 未下过注 */
        NSInteger index = sender.tag - 6666;
        [self submitSportGuess:@"left" withIndex:index];
    }
}

#pragma mark - > 压右边
- (void)rightPeilvLabelClick:(UIButton *)sender{
    
    if ([self.listModel.status isEqualToString:@"1"] &&
        ![self.listModel.bid_status isEqualToString:@"1"]) {
        /* 进行中 && 未下过注 */
        NSInteger index = sender.tag - 7777;
        [self submitSportGuess:@"right" withIndex:index];
    }

}

- (void)submitSportGuess:(NSString *)type withIndex:(NSInteger)index{

    if ([self.delegate respondsToSelector:@selector(submitSportGuessWithType:withIndex:)]) {
        [self.delegate submitSportGuessWithType:type withIndex:index];
    }
}

#pragma mark - > 展示更多
-(void)showButtonClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 8899;
    
    if ([self.delegate respondsToSelector:@selector(showButtonClickShowMore:withCell:withIndex:)]) {
        [self.delegate showButtonClickShowMore:self.listModel.showMore withCell:self withIndex:index];
    }
}

@end
