//
//  FWPlayerScoreDetailView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerScoreDetailView.h"
#import "FWPlayerArchivesViewController.h"
#import "FWPlayerWonderfulVideoViewController.h"

@interface FWPlayerScoreDetailView()
@property (nonatomic, strong) NSMutableArray * imageArray;
@property (nonatomic, strong) NSMutableArray * originalImageArray;

@end

@implementation FWPlayerScoreDetailView

- (id)init{
    self = [super init];
    if (self) {
        
        self.imageArray = @[].mutableCopy;
        self.originalImageArray = @[].mutableCopy;
        
        self.showsVerticalScrollIndicator = NO;
        
        [self setupTopView];
        [self setupMatchView];
        [self setupVideoView];
        [self setupPicsView];
    }
    
    return self;
}

- (void)setupTopView{
//    CGFloat tempHeight = 47;
  
    CGFloat tempHeight = 27;

    
    self.topBgImageView = [[UIImageView alloc] init];
    self.topBgImageView.clipsToBounds = YES;
    self.topBgImageView.userInteractionEnabled = YES;
    self.topBgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topBgImageView.image = [UIImage imageNamed:@"match_detail_bg"];
    [self addSubview:self.topBgImageView];
    [self.topBgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).mas_offset(-tempHeight);
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 167));
    }];
    
    UIView * photoBgView = [[UIView alloc] init];
    photoBgView.layer.cornerRadius = 36/2;
    photoBgView.layer.masksToBounds = YES;
    photoBgView.userInteractionEnabled = YES;
    photoBgView.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.4);
    [self.topBgImageView addSubview:photoBgView];
    [photoBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topBgImageView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(36, 36));
        make.top.mas_equalTo(self.topBgImageView).mas_offset(15+tempHeight);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 30/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [photoBgView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.centerX.mas_equalTo(photoBgView);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    UITapGestureRecognizer * photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoTapClick)];
    [self.photoImageView addGestureRecognizer:photoTap];
    
    self.historyScoreButton = [[UIButton alloc] init];
    self.historyScoreButton.layer.borderColor = FWViewBackgroundColor_FFFFFF.CGColor;
    self.historyScoreButton.layer.borderWidth = 1;
    self.historyScoreButton.layer.cornerRadius = 2;
    self.historyScoreButton.layer.masksToBounds = YES;
    self.historyScoreButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.historyScoreButton setTitle:@"历史成绩" forState:UIControlStateNormal];
    [self.historyScoreButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.historyScoreButton addTarget:self action:@selector(historyScoreButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topBgImageView addSubview:self.historyScoreButton];
    [self.historyScoreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(74, 30));
        make.right.mas_equalTo(self.topBgImageView).mas_offset(-14);
        make.centerY.mas_equalTo(self.photoImageView);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.topBgImageView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.centerY.mas_equalTo(self.photoImageView);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        make.right.mas_equalTo(self.historyScoreButton.mas_left).mas_offset(-5);
        make.width.mas_greaterThanOrEqualTo(200);
    }];
}

- (void)setupMatchView{

    UIView *shadowView = [[UIView alloc]init];
    shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    shadowView.userInteractionEnabled = YES;
    shadowView.layer.cornerRadius = 2;
    shadowView.layer.shadowOffset = CGSizeMake(0, 5);
    shadowView.layer.shadowOpacity = 0.4;
    shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self addSubview:shadowView];
    [shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topBgImageView).mas_offset(60+27);
        make.left.mas_equalTo(self).mas_offset(14);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(128);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
    }];
    
    self.matchView = [[UIView alloc] init];
    self.matchView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.matchView.layer.cornerRadius = 2;
    self.matchView.layer.masksToBounds = YES;
    [shadowView addSubview:self.matchView];
    [self.matchView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(shadowView);
    }];
  
    self.typeImageView = [[UIImageView alloc] init];
    [self.matchView addSubview:self.typeImageView];
    [self.typeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.matchView).mas_offset(15);
        make.right.mas_equalTo(self.matchView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(32, 47));
    }];
    
    self.matchNameLabel = [[UILabel alloc] init];
    self.matchNameLabel.numberOfLines = 0;
    self.matchNameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.matchNameLabel.textColor = FWTextColor_252527;
    self.matchNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.matchNameLabel];
    [self.matchNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(20);
        make.left.top.mas_equalTo(self.matchView).mas_offset(15);
        make.right.mas_equalTo(self.typeImageView.mas_left).mas_offset(-15);
        make.width.mas_greaterThanOrEqualTo(10);
    }];

    self.groupNameLabel = [[UILabel alloc] init];
    self.groupNameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    self.groupNameLabel.textColor = FWTextColor_252527;
    self.groupNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.groupNameLabel];
    [self.groupNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.matchNameLabel);
        make.top.mas_equalTo(self.matchNameLabel.mas_bottom).mas_offset(14);
        make.right.mas_equalTo(self.matchNameLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.carLabel = [[UILabel alloc] init];
    self.carLabel.numberOfLines = 0;
    self.carLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.carLabel.textColor = FWTextColor_252527;
    self.carLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.carLabel];
    [self.carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(20);
        make.left.mas_equalTo(self.matchNameLabel);
        make.top.mas_equalTo(self.groupNameLabel.mas_bottom).mas_offset(12);
        make.right.mas_equalTo(self.matchView).mas_offset(-15);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.scoreView = [[UIView alloc] init];
    [self.matchView addSubview:self.scoreView];
    [self.scoreView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(20);
        make.top.mas_equalTo(self.carLabel.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.matchView).mas_offset(15);
        make.right.mas_equalTo(self.matchView).mas_offset(-15);
        make.width.mas_equalTo(SCREEN_WIDTH-58);
        make.bottom.mas_equalTo(self.matchView).mas_offset(0);
    }];
    
    self.gaizhuangLabel = [[UILabel alloc] init];
    self.gaizhuangLabel.text = @"改装清单";
    self.gaizhuangLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.gaizhuangLabel.textColor = FWTextColor_000000;
    self.gaizhuangLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.gaizhuangLabel];
    [self.gaizhuangLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(22);
        make.left.mas_equalTo(self).mas_offset(14);
        make.top.mas_equalTo(self.matchView.mas_bottom).mas_offset(20);
        make.width.mas_equalTo(100);
    }];
    
    self.gaizhuangDetailLabel = [[UILabel alloc] init];
    self.gaizhuangDetailLabel.numberOfLines = 0;
    self.gaizhuangDetailLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.gaizhuangDetailLabel.textColor = FWTextColor_000000;
    self.gaizhuangDetailLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.gaizhuangDetailLabel];
    [self.gaizhuangDetailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(1);
        make.left.mas_equalTo(self.gaizhuangLabel);
        make.top.mas_equalTo(self.gaizhuangLabel.mas_bottom).mas_offset(16);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
}

- (void)setupVideoView{
   
    self.matchVideoLabel = [[UILabel alloc] init];
    self.matchVideoLabel.text = @"比赛视频";
    self.matchVideoLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.matchVideoLabel.textColor = FWTextColor_272727;
    self.matchVideoLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.matchVideoLabel];
    [self.matchVideoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(22);
        make.left.mas_equalTo(self.gaizhuangLabel);
        make.top.mas_equalTo(self.gaizhuangDetailLabel.mas_bottom).mas_offset(16);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
     self.videoView = [[UIView alloc] init];
     [self addSubview:self.videoView];
     [self.videoView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.top.mas_equalTo(self.matchVideoLabel.mas_bottom).mas_offset(20);
         make.left.mas_equalTo(self.matchVideoLabel).mas_offset(0);
         make.right.mas_equalTo(self.matchVideoLabel).mas_offset(0);
         make.height.mas_greaterThanOrEqualTo(10);
         make.width.mas_equalTo(SCREEN_WIDTH-28);
     }];
     self.matchVideoLabel.hidden = YES;
     self.videoView.hidden = YES;
}

- (void)setupPicsView{
    
    self.picsLabel = [[UILabel alloc] init];
    self.picsLabel.text = @"赛事图集";
    self.picsLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.picsLabel.textColor = FWTextColor_272727;
    self.picsLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.picsLabel];
    [self.picsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(22);
        make.left.mas_equalTo(self.gaizhuangLabel);
        make.top.mas_equalTo(self.videoView.mas_bottom).mas_offset(16);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.picsView = [[UIView alloc] init];
    [self addSubview:self.picsView];
    [self.picsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.picsLabel.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self).mas_offset(0);
        make.right.mas_equalTo(self).mas_offset(0);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    self.picsLabel.hidden = YES;
    self.picsView.hidden = YES;
}

- (void)configForView:(FWScoreDetailModel *)model{
    
    self.detailModel = model;
    
    self.nameLabel.text = model.nick;
    self.matchNameLabel.text = model.sport_name;
    self.groupNameLabel.text = model.car_group;
    self.carLabel.text = [NSString stringWithFormat:@"车型：%@ %@/车队：%@",model.brand,model.car_type,model.club_name];
    self.gaizhuangDetailLabel.text = model.refit_list;

    if ([self.detailModel.type isEqualToString:@"02"]) {
        self.typeImageView.image = [UIImage imageNamed:@"type_02"];
    }else{
        self.typeImageView.image = [UIImage imageNamed:@"type_04"];
    }
    
    [self setupScoreView];
  
    [self.gaizhuangDetailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(1);
        make.left.mas_equalTo(self.gaizhuangLabel);
        make.top.mas_equalTo(self.gaizhuangLabel.mas_bottom).mas_offset(16);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    BOOL isHaveVideo = NO;
    
    if (model.video_list.count >0 ) {
        /* 比赛视频 */
        self.matchVideoLabel.hidden = NO;
        self.videoView.hidden = NO;
        
        [self setupVideoSubViews:model];

        isHaveVideo = YES;
    }else{
        self.matchVideoLabel.hidden = YES;
        self.videoView.hidden = YES;
        
        isHaveVideo = NO;
    }
    
    if (model.imgs.count > 0 ) {
        /* 赛事图集 */
        self.picsView.hidden = NO;
        self.picsLabel.hidden = NO;
        
        for (FWScoreDetailImgsModel * imgModel in model.imgs) {
            [self.imageArray addObject:imgModel.url];
            [self.originalImageArray addObject:imgModel.original_url];
        }
        
        [self setupPicsSubViews:model];
        if (isHaveVideo) {
           
            [self.picsView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.picsLabel.mas_bottom).mas_offset(20);
                make.left.mas_equalTo(self).mas_offset(0);
                make.right.mas_equalTo(self).mas_offset(-0);
                make.height.mas_greaterThanOrEqualTo(10);
                make.width.mas_equalTo(SCREEN_WIDTH);
                make.bottom.mas_equalTo(self).mas_offset(-20);
            }];
        }else{
            
            [self.picsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(22);
                make.left.mas_equalTo(self.gaizhuangLabel);
                make.top.mas_equalTo(self.gaizhuangDetailLabel.mas_bottom).mas_offset(16);
                make.right.mas_equalTo(self).mas_offset(-14);
                make.width.mas_greaterThanOrEqualTo(10);
            }];
            
            [self.picsView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.picsLabel.mas_bottom).mas_offset(20);
                make.left.mas_equalTo(self).mas_offset(0);
                make.right.mas_equalTo(self).mas_offset(-0);
                make.height.mas_greaterThanOrEqualTo(10);
                make.width.mas_equalTo(SCREEN_WIDTH);
                make.bottom.mas_equalTo(self).mas_offset(-20);
            }];
        }
    }else{
        self.picsLabel.hidden = YES;
        self.picsView.hidden = YES;
        
        if (isHaveVideo) {
            [self.videoView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.matchVideoLabel).mas_offset(14);
                make.right.mas_equalTo(self.matchVideoLabel).mas_offset(-14);
                make.height.mas_greaterThanOrEqualTo(10);
                make.width.mas_equalTo(SCREEN_WIDTH-28);
                make.top.mas_equalTo(self.matchVideoLabel.mas_bottom).mas_offset(20);
                make.bottom.mas_equalTo(self).mas_offset(-20);
            }];
        }
    }
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:model.header] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
}

- (void)setupScoreView{
    
    for (UIView * view in self.scoreView.subviews) {
        [view removeFromSuperview];
    }
    
    FWScoreMatchListModel * firstModel = [[FWScoreMatchListModel alloc] init];
    firstModel.et = self.detailModel.et;
    firstModel.vspeed = self.detailModel.vspeed;
    firstModel.rt = self.detailModel.rt;
    firstModel.phase_type_str = self.detailModel.phase_type_str;
    
    [self.detailModel.match_list insertObject:firstModel atIndex:0];
    
    NSInteger arrayCount  = self.detailModel.match_list.count;
    
    for (int i = 0; i <arrayCount; i++) {
        
        FWScoreMatchListModel * listModel = self.detailModel.match_list[i];
        
        UIView * leftBgView = [[UIView alloc] init];
        leftBgView.frame = CGRectMake(0, i*23, SCREEN_WIDTH-107, 23);
        [self.scoreView addSubview:leftBgView];
        [leftBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.scoreView);
            make.height.mas_equalTo(23);
            make.width.mas_equalTo((SCREEN_WIDTH-107));
            make.top.mas_equalTo(self.scoreView).mas_offset(i*23);
            if (i == arrayCount -1) {
                make.bottom.mas_equalTo(self.scoreView).mas_offset(-17);
            }
        }];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect: leftBgView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii:CGSizeMake(4,4)];

        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = leftBgView.bounds;
        maskLayer.path = maskPath.CGPath;
        leftBgView.layer.mask = maskLayer;
       
        
        UIView * rightBgView = [[UIView alloc] init];
        rightBgView.frame = CGRectMake(CGRectGetMaxY(leftBgView.frame)+5, CGRectGetMinY(leftBgView.frame), 44, 23);
        [self.scoreView addSubview:rightBgView];
        [rightBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(23);
            make.left.mas_equalTo(leftBgView.mas_right).mas_offset(5);
            make.width.mas_equalTo(44);
            make.centerY.mas_equalTo(leftBgView);
        }];
        UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect: rightBgView.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(4,4)];

        CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
        maskLayer2.frame = rightBgView.bounds;
        maskLayer2.path = maskPath2.CGPath;
        rightBgView.layer.mask = maskLayer2;
        
        UILabel * ETLabel = [[UILabel alloc] init];
        ETLabel.textAlignment = NSTextAlignmentCenter;
        [leftBgView addSubview:ETLabel];
        [ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(leftBgView);
            make.left.mas_equalTo(leftBgView).mas_offset(0);
            make.height.mas_equalTo(23);
            make.width.mas_greaterThanOrEqualTo((SCREEN_WIDTH-107)/3);
        }];
        
        UILabel * weisuLabel = [[UILabel alloc] init];
        weisuLabel.textAlignment = NSTextAlignmentCenter;
        [leftBgView addSubview:weisuLabel];
        [weisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(leftBgView);
            make.left.mas_equalTo(ETLabel.mas_right).mas_offset(0);
            make.height.mas_equalTo(ETLabel);
            make.width.mas_greaterThanOrEqualTo(ETLabel);
        }];
        
        UILabel * RTLabel = [[UILabel alloc] init];
        RTLabel.textAlignment = NSTextAlignmentCenter;
        [leftBgView addSubview:RTLabel];
        [RTLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(leftBgView);
            make.left.mas_equalTo(weisuLabel.mas_right).mas_offset(0);
            make.height.mas_equalTo(weisuLabel);
            make.width.mas_greaterThanOrEqualTo(weisuLabel);
        }];
        
        UILabel * rankLabel = [[UILabel alloc] init];
        rankLabel.textAlignment = NSTextAlignmentCenter;
        [rightBgView addSubview:rankLabel];
        [rankLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.mas_equalTo(rightBgView);
            make.right.mas_equalTo(rightBgView).mas_offset(-5);
        }];
        
        ETLabel.text = [NSString stringWithFormat:@"ET：%@",listModel.et];
        weisuLabel.text = [NSString stringWithFormat:@"尾速：%@",listModel.vspeed];
        RTLabel.text = [NSString stringWithFormat:@"RT：%@",listModel.rt];
        rankLabel.text = listModel.phase_type_str;
        
        ETLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        weisuLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        RTLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        rankLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        
        if (i == 0) {
            ETLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
            weisuLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
            RTLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
            rankLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
            
            leftBgView.backgroundColor =  FWColor(@"222222");
            rightBgView.backgroundColor =  FWColor(@"222222");
            ETLabel.textColor = FWColor(@"ffffff");
            weisuLabel.textColor = FWColor(@"ffffff");
            RTLabel.textColor = FWColor(@"ffffff");
            rankLabel.textColor = FWColor(@"ffffff");
        }else {
            if ([listModel.phase_type_str isEqualToString:@"决赛"]){
                leftBgView.backgroundColor = FWViewBackgroundColor_EEEEEE;
                rightBgView.backgroundColor = FWViewBackgroundColor_EEEEEE;
                ETLabel.textColor = FWColor(@"515151");
                weisuLabel.textColor = FWColor(@"515151");
                RTLabel.textColor = FWColor(@"515151");
                rankLabel.textColor = FWColor(@"515151");;
            }else{
                leftBgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
                rightBgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
                ETLabel.textColor = FWColor(@"515151");;
                weisuLabel.textColor = FWColor(@"515151");;
                RTLabel.textColor = FWColor(@"515151");;
                rankLabel.textColor = FWColor(@"515151");
            }
        }
    }
}

#pragma mark - > 初始化视频集
- (void)setupVideoSubViews:(FWScoreDetailModel *)model{
    
    for (int i = 0; i < model.video_list.count; i ++) {
        
        FWScoreDetailVideoInfoModel * videoModel = model.video_list[i];
        
        UIImageView * videoImageView = [[UIImageView alloc] init];
        videoImageView.tag = 10050+i;
        videoImageView.layer.cornerRadius = 2;
        videoImageView.layer.masksToBounds = YES;
        videoImageView.userInteractionEnabled = YES;
        videoImageView.image = [UIImage imageNamed:@"placeholder"];
        videoImageView.contentMode = UIViewContentModeScaleAspectFill;
        videoImageView.clipsToBounds = YES;
        [self.videoView addSubview:videoImageView];
        [videoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.videoView).mas_offset(0);
            make.right.mas_equalTo(self.videoView).mas_offset(-0);
            make.height.mas_equalTo(188);
            make.width.mas_equalTo(SCREEN_WIDTH-28);
            make.top.mas_equalTo(self.videoView).mas_offset(198 *i);
            if (i == model.video_list.count - 1) {
                make.bottom.mas_equalTo(self.videoView);
            }
        }];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoClick:)];
        [videoImageView addGestureRecognizer:tap];

        if (videoModel.video_title.length > 0) {
            NSString * title = [NSString stringWithFormat:@"%@%@",videoModel.video_title,@"   "];
            
            UILabel * titleLabel = [[UILabel alloc] init];
            titleLabel.layer.cornerRadius = 2;
            titleLabel.layer.masksToBounds = YES;
            titleLabel.text = title;
            titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
            titleLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.4);
            titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [videoImageView addSubview:titleLabel];
            [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(24);
                make.left.top.mas_equalTo(videoImageView).mas_offset(5);
                make.width.mas_lessThanOrEqualTo(185);
            }];
        }
        
        UIImageView * iconImageView = [[UIImageView alloc] init];
        iconImageView.image = [UIImage imageNamed:@"card_play"];
        iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        iconImageView.clipsToBounds = YES;
        [videoImageView addSubview:iconImageView];
        [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(videoImageView);
            make.centerY.mas_equalTo(videoImageView);
            make.height.mas_equalTo(51);
            make.width.mas_equalTo(51);
        }];

        UILabel * timeLabel = [[UILabel alloc] init];
        timeLabel.layer.cornerRadius = 2;
        timeLabel.layer.masksToBounds = YES;
        timeLabel.text = videoModel.video_time;
        timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        timeLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.6);
        timeLabel.textColor = FWViewBackgroundColor_FFFFFF;
        timeLabel.textAlignment = NSTextAlignmentCenter;
        [videoImageView addSubview:timeLabel];
        [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(24);
            make.bottom.mas_equalTo(videoImageView).mas_offset(-9);
            make.right.mas_equalTo(videoImageView).mas_offset(-9);
            make.width.mas_equalTo(55);
        }];

        [videoImageView sd_setImageWithURL:[NSURL URLWithString:videoModel.video_cover] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (error) {
                videoImageView.image = [UIImage imageNamed:@"placeholder"];
            }
        }];
    }
}


#pragma mark - > 初始化图集
- (void)setupPicsSubViews:(FWScoreDetailModel *)model{
    
    CGFloat WH = (SCREEN_WIDTH-38)/3;
    
    for (int i = 0; i < model.imgs.count; i ++) {
        UIImageView * imageView = [UIImageView new];
        imageView.tag = 12306 + i;
        imageView.layer.cornerRadius = 2;
        imageView.layer.masksToBounds = YES;
        imageView.userInteractionEnabled = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageView sd_setImageWithURL:[NSURL URLWithString:self.imageArray[i]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
//        imageView.frame = CGRectMake(14 +(5+WH)*(i%3), 35+(i/3)*(WH+5), WH, WH);
        [self.picsView addSubview:imageView];
        [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14 +(5+WH)*(i%3));
            make.top.mas_equalTo((i/3)*(WH+5));
            make.height.mas_equalTo(WH);
            make.width.mas_equalTo(WH);
            if (i == model.imgs.count-1) {
                make.bottom.mas_equalTo(self.picsView);
            }
        }];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(picsViewClick:)];
        [imageView addGestureRecognizer:tap];
    }
}

- (void)picsViewClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 12306;
    
    
    if (self.detailModel.imgs.count > 0 && index < self.detailModel.imgs.count) {
        
        FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
        browser.isFullWidthForLandScape = YES;
        browser.isNeedLandscape = YES;
        browser.currentImageIndex = (int)index;
        browser.imageArray = [self.imageArray mutableCopy];
        browser.smallArray = [self.imageArray copy];
        browser.originalImageArray = self.originalImageArray;
        browser.vc = self.vc;
        browser.fromType = 3;
        [browser show];
    }
}

#pragma mark - > 返回
- (void)backBtnClick{
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 历史成绩
- (void)historyScoreButtonClick{
    
    FWPlayerArchivesViewController * PAVC = [[FWPlayerArchivesViewController alloc] init];
    PAVC.driver_id = self.detailModel.driver_id;
    [self.vc.navigationController pushViewController:PAVC animated:YES];
}

#pragma mark - > 查看视频
- (void)videoClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 10050;
    FWScoreDetailVideoInfoModel * model = self.detailModel.video_list[index];
    
    FWPlayerWonderfulVideoViewController * PWVC = [[FWPlayerWonderfulVideoViewController alloc] init];
    PWVC.videoURL = model.video_url;
    PWVC.videoCover = model.video_cover;
    [self.vc.navigationController pushViewController:PWVC animated:YES];
}

#pragma mark - > 点击头像
- (void)photoTapClick{
    
    if ([self.detailModel.uid integerValue] == 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"该车手暂未关联肆放账号哦" toController:self.vc];
    }else{
        FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
        UIVC.user_id = self.detailModel.uid;
        [self.vc.navigationController pushViewController:UIVC animated:YES];
    }
}


@end
