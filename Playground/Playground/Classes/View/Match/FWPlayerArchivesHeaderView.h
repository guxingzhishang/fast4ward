//
//  FWPlayerArchivesHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 车手档案头部
 */
#import <UIKit/UIKit.h>
#import "FWPlayerArchivesModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerArchivesHeaderView : UIView

@property (nonatomic, strong) UIImageView * topBgImageView;
@property (nonatomic, strong) UIView * cirleView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UILabel * partLabel;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, strong) FWPlayerArchivesModel * archivesModel;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
