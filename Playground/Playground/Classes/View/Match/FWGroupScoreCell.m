//
//  FWGroupScoreCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGroupScoreCell.h"

@implementation FWGroupScoreCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.contentView.backgroundColor = FWTextColor_F8F8F8;
        [self setupsubviews];
    }
    
    return self;
}

- (void)setupsubviews{
    
    self.authenticationView.hidden = YES;
    
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(15);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(30, 40));
    }];
    
    
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(41, 41));
    }];
    
    self.rankNumImageView.hidden = NO;
    [self.rankNumImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(16, 16));
        make.right.mas_equalTo(self.numberLabel);
    }];
    
    [self.rankImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.photoImageView.mas_top).mas_offset(5);
        make.centerX.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(26, 18));
    }];
    self.rankImageView.hidden = YES;
    
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(13);
        make.top.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-90);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.authenticationView.mas_right);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.nameLabel);
    }];
    
    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 12];
    self.ETLabel.textColor = FWColor(@"#38A4F8");
    self.ETLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-10);
        make.width.mas_equalTo(80);
    }];

    
    self.WeisuLabel = [[UILabel alloc] init];
    self.WeisuLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.WeisuLabel.textColor = FWTextColor_12101D;
    self.WeisuLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.WeisuLabel];
    [self.WeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.signLabel);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-10);
        make.width.mas_equalTo(90);
    }];

}

- (void)configCellForModel:(id)model withIndex:(NSInteger)index{
 
    CGFloat rankImageHeight = 17;

    self.rankImageView.hidden = NO;
    self.rankNumImageView.hidden = NO;
    
    rankImageHeight = 24;
        
    if (index == 0) {
        self.rankImageView.image = [UIImage imageNamed:@"rank_first"];
        self.rankNumImageView.image = [UIImage imageNamed:@"rank_new_first"];
        self.numberLabel.text = @"";
    }else if (index == 1){
        self.rankImageView.image = [UIImage imageNamed:@"rank_second"];
        self.rankNumImageView.image = [UIImage imageNamed:@"rank_new_second"];
        self.numberLabel.text = @"";
    }else if (index == 2){
        self.rankImageView.image = [UIImage imageNamed:@"rank_third"];
        self.rankNumImageView.image = [UIImage imageNamed:@"rank_new_third"];
        self.numberLabel.text = @"";
    }else{
        self.rankNumImageView.image = [UIImage imageNamed:@""];
        self.rankImageView.hidden = YES;
        self.rankNumImageView.hidden = YES;
        
        rankImageHeight = 17;
    }
    
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel.mas_right).mas_offset(0);
        make.top.mas_equalTo(self.contentView).mas_offset(rankImageHeight);
        make.size.mas_equalTo(CGSizeMake(41, 41));
    }];

    [self.rankNumImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(16, 16));
        make.right.mas_equalTo(self.photoImageView.mas_left).mas_offset(-7.5);
    }];
    
    FWScoreDetailModel * scoreListModel = (FWScoreDetailModel *)model;
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@(%@)",scoreListModel.nickname,scoreListModel.car_group];
    if (scoreListModel.brand && scoreListModel.brand.length > 0 ) {
        self.signLabel.text = [NSString stringWithFormat:@"座驾：%@ %@",scoreListModel.brand,scoreListModel.model];
    }else if(scoreListModel.car_brand && scoreListModel.car_brand.length > 0){
        self.signLabel.text = [NSString stringWithFormat:@"座驾：%@ %@",scoreListModel.car_brand,scoreListModel.car_type];
    }

    self.ETLabel.text = [NSString stringWithFormat:@"ET:%@",scoreListModel.et];
    self.WeisuLabel.text = [NSString stringWithFormat:@"尾速:%@",scoreListModel.vspeed];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:scoreListModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
}
@end
