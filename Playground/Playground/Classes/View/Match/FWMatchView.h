//
//  FWMatchView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 赛事
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchView : UIScrollView<UIScrollViewDelegate>

@property (nonatomic, strong) UIView * mainView;

/* 便捷模块（4个） */
@property (nonatomic, strong) UIView * convientView;

/* 直播入口 */
@property (nonatomic, strong) UIImageView * matchLiveImageView;

/* 问答模块 */
@property (nonatomic, strong) UIView * wendaView;

/* 图集 */
@property (nonatomic, strong) UIView * tujiView;

/* 改装情报 */
@property (nonatomic, strong) UIView * refitReportView;

/* 关于fast4ward */
@property (nonatomic, strong) UIView * aboutView;

/* 官方视频 */
@property (nonatomic, strong) UIView * videosView;

@property (nonatomic, strong) FWF4WMatchModel * matchModel;
@property (nonatomic, strong) FWRefitListKeyModel * refitModel;

@property (nonatomic, weak) UIViewController * vc;

- (CGFloat)getCurrentHeight;
- (void)congifViewWithModel:(id)model withRefitListModel:(id)refitModel;

@end

NS_ASSUME_NONNULL_END
