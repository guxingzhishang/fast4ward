//
//  FWRealPerformanceCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/7.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class FWScoreMatchListModel;

@protocol FWRealPerformanceCellDelegate <NSObject>

- (void)moreButtonClick;

@end

@interface FWRealPerformanceCell : UITableViewCell

/* 胜利者 */
@property (nonatomic, strong) UIImageView * leftWinnerImageView;
@property (nonatomic, strong) UIImageView * rightWinnerImageView;

/* 头像 */
@property (nonatomic, strong) UIImageView * leftImageView;
@property (nonatomic, strong) UIImageView * rightImageView;

/* 姓名 */
@property (nonatomic, strong) UILabel * leftNameLabel;
@property (nonatomic, strong) UILabel * rightNameLabel;

/* 选手号 */
@property (nonatomic, strong) UILabel * leftNoLabel;
@property (nonatomic, strong) UILabel * rightNoLabel;

/* 点 */
@property (nonatomic, strong) UIImageView * leftPointView;
@property (nonatomic, strong) UIImageView * rightPointView;

/* 车型 */
@property (nonatomic, strong) UILabel * leftCarLabel;
@property (nonatomic, strong) UILabel * rightCarLabel;

/* 犯规 */
@property (nonatomic, strong) UILabel * leftFoulLabel;
@property (nonatomic, strong) UILabel * rightFoulLabel;

/* ET */
@property (nonatomic, strong) UILabel * leftETLabel;
@property (nonatomic, strong) UILabel * rightETLabel;

/* ET值 */
@property (nonatomic, strong) UILabel * leftETNumberLabel;
@property (nonatomic, strong) UILabel * rightETNumberLabel;

/* 尾速 */
@property (nonatomic, strong) UILabel * leftWeisuLabel;
@property (nonatomic, strong) UILabel * rightWeisuLabel;

/* 尾速值 */
@property (nonatomic, strong) UILabel * leftWeisuNumberLabel;
@property (nonatomic, strong) UILabel * rightWeisuNumberLabel;

/* RT */
@property (nonatomic, strong) UILabel * leftRTLabel;
@property (nonatomic, strong) UILabel * rightRTLabel;

/* RT值 */
@property (nonatomic, strong) UILabel * leftRTNumberLabel;
@property (nonatomic, strong) UILabel * rightRTNumberLabel;

/* vs */
@property (nonatomic, strong) UIImageView * VSImageView;

/* 分享 */
@property (nonatomic, strong) UIButton * shareButton;

/* 底线 */
@property (nonatomic, strong) UIView * lineView;

/* 查看更多 */
@property (nonatomic, strong) UIButton * moreButton;

@property (nonatomic, assign) id <FWRealPerformanceCellDelegate>delegate;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWScoreListModel * matchModel;

@property (nonatomic, strong) FWScoreModel * scoreModel;


- (void)configForModel:(id)model;

- (void)showETPlusRT;
- (void)showNormalETAndRt;
- (void)allViewIsHidden:(BOOL)isHidden;

@end

NS_ASSUME_NONNULL_END
