//
//  FWHotLiveCollectionCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHotLiveCollectionCell.h"
#import <RongIMLib/RongIMLib.h>
#import "RCCRManager.h"
#import "RCChatroomWelcome.h"
#import "RCChatroomUserQuit.h"
#import "RCChatroomFollow.h"
#import "RCChatroomLike.h"
#import "RCChatroomStart.h"
#import "RCChatroomEnd.h"
#import "RCChatroomUserBan.h"
#import "RCChatroomUserUnBan.h"
#import "RCChatroomUserBlock.h"
#import "RCChatroomUserUnBlock.h"
#import "RCChatroomNotification.h"
#import "RCCRRongCloudIMManager.h"

#define RCCRText_HEXCOLOR(rgbValue)                                                                                             \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0                                               \
green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0                                                  \
blue:((float)(rgbValue & 0xFF)) / 255.0                                                           \
alpha:1.0]

@implementation FWHotLiveCollectionCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initializedSubViews];
    }
    return self;
}

- (void)initializedSubViews {
    [self.contentView addSubview:self.contentBgView];
    [self.contentBgView addSubview:self.textLabel];
    [self.contentBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(10);
        make.centerY.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.contentView).mas_offset(2.5);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-2.5);
        make.width.mas_lessThanOrEqualTo(301);
        make.height.mas_greaterThanOrEqualTo(23);
    }];
    
    [self.textLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentBgView).mas_offset(10);
        make.right.mas_equalTo(self.contentBgView).mas_offset(-10);
        make.top.bottom.mas_equalTo(self.contentBgView);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
}

- (void)setDataModel:(RCCRMessageModel *)model {
    [super setDataModel:model];
    [self updateUI:model];
    [self.textLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentBgView).mas_offset(10);
        make.right.mas_equalTo(self.contentBgView).mas_offset(-10);
        make.top.bottom.mas_equalTo(self.contentBgView);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
}

- (void)updateUI:(RCCRMessageModel *)model {
    if ([model.content isMemberOfClass:[RCChatroomWelcome class]]) {
        
        RCChatroomWelcome *messageContent = (RCChatroomWelcome *)model.content;
        
        NSString *userName = messageContent.extra?messageContent.extra:@"游客";
        NSString *localizedMessage = @" 进入直播室";
        NSString *str =[NSString stringWithFormat:@"%@ %@",userName,localizedMessage];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:userName]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:localizedMessage]];
        [self.textLabel setAttributedText:attributedString.copy];
        return;
    } else if ([model.content isMemberOfClass:[RCChatroomUserQuit class]]) {
        
        RCChatroomWelcome *messageContent = (RCChatroomWelcome *)model.content;
        
        NSString *userName = messageContent.extra?messageContent.extra:@"游客";
        NSString *localizedMessage = @" 退出直播室";
        NSString *str =[NSString stringWithFormat:@"%@ %@",userName,localizedMessage];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:userName]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:localizedMessage]];
        [self.textLabel setAttributedText:attributedString.copy];
        return;
    }  else if ([model.content isMemberOfClass:[RCTextMessage class]]) {
        RCTextMessage *textMessage = (RCTextMessage *)self.model.content;
        if (self.model.senderUserId) {
            NSString *localizedMessage = textMessage.content;
            
            NSString *userName = textMessage.senderUserInfo.name?textMessage.senderUserInfo.name:@"游客";
            NSString *str =[NSString stringWithFormat:@"%@  %@",userName,localizedMessage];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
            
            NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:2];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0x7595FF)) range:[str rangeOfString:userName]];
            [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:localizedMessage]];
            [self.textLabel setAttributedText:attributedString.copy];
            
        }
    } else if ([model.content isMemberOfClass:[RCChatroomStart class]]) {
        RCChatroomStart *startMessage = (RCChatroomStart *)self.model.content;
        NSString *notice = @"系统通知";
        NSString *time = [self timeWithTimeInterval:startMessage.time];
        NSString *content = @"开始视频直播";
        NSString *str = [NSString stringWithFormat:@"%@ %@ %@",notice,time,content];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0x3ce1ff)) range:[str rangeOfString:time]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:notice]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:content]];
        [self.textLabel setAttributedText:attributedString.copy];
        return;
    } else if ([model.content isMemberOfClass:[RCChatroomEnd class]]) {
        RCChatroomEnd *endMessage = (RCChatroomEnd *)self.model.content;
        NSString *notice = @"系统通知";
        NSString *duration = [NSString stringWithFormat:@"%d 分钟",endMessage.duration];
        NSString *content = @"本次直播已结束，直播时长 ";
        NSString *str = [NSString stringWithFormat:@"%@ %@ %@",notice, content, duration];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0x3ce1ff)) range:[str rangeOfString:duration]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:notice]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:content]];
        [self.textLabel setAttributedText:attributedString.copy];
        return;
    }else if ([model.content isMemberOfClass:[RCChatroomUserBan class]]) {
        RCChatroomUserBan *userBanMessage = (RCChatroomUserBan *)self.model.content;
        //        RCUserInfo *userInfo = [[RCCRManager sharedRCCRManager] getUserInfo:userBanMessage.id];
        NSString *userName = model.userInfo.name;
        NSString *content = [NSString stringWithFormat:@"被禁言 %d 分钟",userBanMessage.duration];
        NSString *notice = @"系统通知";
        NSString *str = [NSString stringWithFormat:@"%@ %@ %@",notice,userName,content];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0x3ce1ff)) range:[str rangeOfString:userName]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:notice]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:content]];
        [self.textLabel setAttributedText:attributedString.copy];
        
        //设置禁言
        if ([userBanMessage.id isEqualToString:[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId] && userBanMessage.duration >= 1) {
            [[RCCRManager sharedRCCRManager] setUserBan:userBanMessage.duration];
        }
        return;
    } else if ([model.content isMemberOfClass:[RCChatroomUserUnBan class]]) {
        RCChatroomUserUnBan *userUnbanMessage = (RCChatroomUserUnBan *)self.model.content;
        //        RCUserInfo *userInfo = [[RCCRManager sharedRCCRManager] getUserInfo:userUnbanMessage.id];
        NSString *userName = model.userInfo.name;
        NSString *content = @"已被解除禁言";
        NSString *notice = @"系统通知";
        NSString *str = [NSString stringWithFormat:@"%@ %@ %@",notice,userName,content];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0x3ce1ff)) range:[str rangeOfString:userName]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:notice]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:content]];
        [self.textLabel setAttributedText:attributedString.copy];
        
        //设置解除禁言
        if ([userUnbanMessage.id isEqualToString:[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId]) {
            [[RCCRManager sharedRCCRManager] setUserUnban];
        }
        return;
    } else if ([model.content isMemberOfClass:[RCChatroomUserBlock class]]) {
        RCChatroomUserBlock *userBlockMessage = (RCChatroomUserBlock *)self.model.content;
        //        RCUserInfo *userInfo = [[RCCRManager sharedRCCRManager] getUserInfo:userBlockMessage.id];
        NSString *userName = model.userInfo.name;
        NSString *content = @"被踢出聊天室";
        NSString *notice = @"系统通知";
        NSString *str = [NSString stringWithFormat:@"%@ %@ %@",notice,userName,content];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0x3ce1ff)) range:[str rangeOfString:userName]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:notice]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:content]];
        [self.textLabel setAttributedText:attributedString.copy];
        return;
    } else if ([model.content isMemberOfClass:[RCChatroomUserUnBlock class]]) {
        RCChatroomUserUnBlock *userUnBlockMessage = (RCChatroomUserUnBlock *)self.model.content;
        //        RCUserInfo *userInfo = [[RCCRManager sharedRCCRManager] getUserInfo:userUnBlockMessage.id];
        NSString *userName = model.userInfo.name;
        NSString *content = @"已被解除封禁";
        NSString *notice = @"系统通知";
        NSString *str = [NSString stringWithFormat:@"%@ %@ %@",notice,userName,content];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0x3ce1ff)) range:[str rangeOfString:userName]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:notice]];
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xffffff)) range:[str rangeOfString:content]];
        [self.textLabel setAttributedText:attributedString.copy];
        return;
    }else if ([model.content isMemberOfClass:[RCChatroomNotification class]]) {
        RCChatroomNotification *notificationMessage = (RCChatroomNotification *)self.model.content;
        NSString *str  = notificationMessage.content;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        
        [attributedString addAttribute:NSForegroundColorAttributeName value:(RCCRText_HEXCOLOR(0xff0000)) range:[str rangeOfString:str]];
        [self.textLabel setAttributedText:attributedString.copy];
        return;
    }
}
//
//+ (CGSize)getMessageCellSize:(NSString *)content withWidth:(CGFloat)width{
//    CGSize textSize = CGSizeZero;
//    textSize.height = textSize.height + 17;
//    return textSize;
//}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.font = DHSystemFontOfSize_14;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [_textLabel setTextAlignment: NSTextAlignmentLeft];
        [_textLabel setTintColor:[UIColor whiteColor]];
        self.textLabel.numberOfLines = 3;
    }
    return _textLabel;
}

- (UIView *)contentBgView{
    if (!_contentBgView) {
        _contentBgView = [[UIView alloc] init];
        _contentBgView.backgroundColor = FWColorWihtAlpha(@"000000", 0.4);
        _contentBgView.layer.cornerRadius = 23/2;
        _contentBgView.layer.masksToBounds = YES;
    }
    
    return _contentBgView;
}

- (NSString *)timeWithTimeInterval:(long)timeInterval
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"shanghai"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy年MM月dd日 HH:mm"];
    
    // 毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:timeInterval/ 1000.0];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}

@end
