//
//  FWHotLiveBottomView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#define LEFT_INSET                 15
#define RIGHT_INSET                60
#define TOP_BOTTOM_INSET           15

NS_ASSUME_NONNULL_BEGIN

@protocol FWHotLiveBottomViewDelegate <NSObject>

- (void)shareMatchPost;

@end

@interface FWHotLiveBottomView : UIView<UITextViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView              *container;
@property (nonatomic, strong) IQTextView          *textView;
@property (nonatomic, strong) UIButton            *shareButton;

@property (nonatomic, assign) CGFloat            textHeight;
@property (nonatomic, assign) CGFloat            keyboardHeight;
@property (nonatomic, assign) CGFloat            commentViewBgHeight;

@property (nonatomic, weak) id<FWHotLiveBottomViewDelegate>    delegate;
@end

NS_ASSUME_NONNULL_END
