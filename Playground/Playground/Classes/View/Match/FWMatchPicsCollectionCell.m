//
//  FWMatchPicsCollectionCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchPicsCollectionCell.h"

@implementation FWMatchPicsCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.picImageView = [[UIImageView alloc] init];
    self.picImageView.layer.cornerRadius = 5;
    self.picImageView.layer.masksToBounds = YES;
    self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.picImageView.clipsToBounds = YES;
    self.picImageView.image = [UIImage imageNamed:@"placeholder"];
    [self.contentView addSubview:self.picImageView];
    self.picImageView.frame = CGRectMake(0, 0, (SCREEN_WIDTH-38)/3, (SCREEN_WIDTH-38)/3);
}



@end
