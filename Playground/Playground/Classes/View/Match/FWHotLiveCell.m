//
//  FWHotLiveCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHotLiveCell.h"

@implementation FWHotLiveCell
@synthesize leftImageView;
@synthesize rightImageView;
@synthesize leftTitleLable;
@synthesize rightTitleLable;
@synthesize leftView;
@synthesize rightView;
@synthesize leftScanLabel;
@synthesize rightScanLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.leftView = [[UIView alloc] init];
        self.leftView.layer.cornerRadius = 5;
        self.leftView.layer.masksToBounds = YES;
        self.leftView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self.contentView addSubview:self.leftView];
        [self.leftView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).mas_offset(14);
            make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-38)/2, (SCREEN_WIDTH-38)/2+60));
            make.top.mas_equalTo(self.contentView).mas_offset(0);
        }];
        
        UITapGestureRecognizer * leftTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftButtonOnClick)];
        [self.leftView addGestureRecognizer:leftTap];
        
        self.rightView = [[UIView alloc] init];
        self.rightView.layer.cornerRadius = 5;
        self.rightView.layer.masksToBounds = YES;
        self.rightView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self.contentView addSubview:self.rightView];
        [self.rightView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.leftView.mas_right).mas_offset(10);
            make.size.mas_equalTo(self.leftView);
            make.top.mas_equalTo(self.leftView);
        }];
        UITapGestureRecognizer * rightTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightButtonOnClick)];
        [self.rightView addGestureRecognizer:rightTap];
        
        self.leftImageView = [[UIImageView alloc] init];
        self.leftImageView.image = [UIImage imageNamed:@"placeholder"];
        [self.leftView addSubview:self.leftImageView];
        [self.leftImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self.leftView);
            make.width.height.mas_equalTo((SCREEN_WIDTH-38)/2);
        }];
        
        
        self.rightImageView = [[UIImageView alloc] init];
        self.rightImageView.image = [UIImage imageNamed:@"placeholder"];
        [self.rightView addSubview:self.rightImageView];
        [self.rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self.rightView);
            make.width.height.mas_equalTo(self.leftImageView);
        }];
        
        
        self.leftScanLabel =  [[UILabel alloc] init];
        self.leftScanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
        self.leftScanLabel.textColor = FWViewBackgroundColor_FFFFFF;
        self.leftScanLabel.textAlignment = NSTextAlignmentRight;
        [self.leftImageView addSubview:self.leftScanLabel];
        [self.leftScanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.mas_equalTo(self.leftImageView).mas_offset(-14);
            make.height.mas_equalTo(17);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
        
        self.leftStatusImageView =  [[UIImageView alloc] init];
        self.leftStatusImageView.layer.cornerRadius = 7/2;
        self.leftStatusImageView.layer.masksToBounds = YES;
        [self.leftImageView addSubview:self.leftStatusImageView];
        [self.leftStatusImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.leftScanLabel.mas_left).mas_offset(-5);
            make.centerY.mas_equalTo(self.leftScanLabel);
            make.width.height.mas_equalTo(7);
        }];
        
        
        self.rightScanLabel =  [[UILabel alloc] init];
        self.rightScanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
        self.rightScanLabel.textColor = FWViewBackgroundColor_FFFFFF;
        self.rightScanLabel.textAlignment = NSTextAlignmentRight;
        [self.rightImageView addSubview:self.rightScanLabel];
        [self.rightScanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.mas_equalTo(self.rightImageView).mas_offset(-14);
            make.height.mas_equalTo(leftScanLabel);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
        
        self.rightStatusImageView =  [[UIImageView alloc] init];
        self.rightStatusImageView.layer.cornerRadius = 7/2;
        self.rightStatusImageView.layer.masksToBounds = YES;
        [self.rightImageView addSubview:self.rightStatusImageView];
        [self.rightStatusImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.rightScanLabel.mas_left).mas_offset(-5);
            make.centerY.mas_equalTo(self.rightScanLabel);
            make.width.height.mas_equalTo(7);
        }];

        
      
        self.leftTitleLable =  [[UILabel alloc] init];
        self.leftTitleLable.numberOfLines = 2;
        self.leftTitleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
        self.leftTitleLable.textColor = FWTextColor_000000;
        self.leftTitleLable.textAlignment = NSTextAlignmentLeft;
        self.leftTitleLable.lineBreakMode = NSLineBreakByCharWrapping;
        [self.leftView addSubview:self.leftTitleLable];
        [self.leftTitleLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.leftView).mas_offset(10);
            make.right.mas_equalTo(self.leftView).mas_offset(-10);
            make.top.mas_equalTo(self.leftImageView.mas_bottom).mas_offset(0);
            make.height.mas_equalTo(60);
            make.width.mas_equalTo((SCREEN_WIDTH-38)/2-20);
        }];
        
        self.rightTitleLable =  [[UILabel alloc] init];
        self.rightTitleLable.numberOfLines = 2;
        self.rightTitleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
        self.rightTitleLable.textColor = FWTextColor_000000;
        self.rightTitleLable.textAlignment = NSTextAlignmentLeft;
        self.rightTitleLable.lineBreakMode = NSLineBreakByCharWrapping;
        [self.rightView addSubview:self.rightTitleLable];
        [self.rightTitleLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.rightView).mas_offset(10);
            make.right.mas_equalTo(self.rightView).mas_offset(-10);
            make.top.mas_equalTo(self.rightImageView.mas_bottom).mas_offset(0);
            make.height.mas_equalTo(60);
            make.width.mas_equalTo(self.leftTitleLable);
        }];
        
        
    }
    return self;
}

- (void)configForLeftView:(id)model{
    
    self.leftListModel = (FWLiveListModel *)model;
    
    self.leftTitleLable.text = self.leftListModel.live_title;
    
    if ([self.leftListModel.zhibo_status isEqualToString:@"1"]) {
        // 未直播
        self.leftStatusImageView.backgroundColor = FWClearColor;
        self.leftScanLabel.text = @"";
    }else{
        // 有直播
        self.leftStatusImageView.backgroundColor = FWGreen_00FF1D;
        self.leftScanLabel.text = [NSString stringWithFormat:@"%@人",self.leftListModel.user_total];
    }

    [self.leftImageView sd_setImageWithURL:[NSURL URLWithString:self.leftListModel.live_cover]];
}

- (void)configForRightView:(id)model{
    
    self.rightListModel = (FWLiveListModel *)model;

    self.rightTitleLable.text = self.rightListModel.live_title;
    
    if ([self.rightListModel.zhibo_status isEqualToString:@"1"]) {
        // 未直播
        self.rightStatusImageView.backgroundColor = FWClearColor;
        self.rightScanLabel.text = @"";
    }else{
        // 有直播
        self.rightStatusImageView.backgroundColor = FWGreen_00FF1D;
        self.rightScanLabel.text = [NSString stringWithFormat:@"%@人",self.leftListModel.user_total];
    }

    [self.rightImageView sd_setImageWithURL:[NSURL URLWithString:self.rightListModel.live_cover]];
}

//按钮点击事件
- (void)leftButtonOnClick{
    
    if (self.leftButtonBlock) {
        self.leftButtonBlock();
    }
}

- (void)rightButtonOnClick{
    
    if (self.rightButtonBlock) {
        self.rightButtonBlock();
    }
}

@end
