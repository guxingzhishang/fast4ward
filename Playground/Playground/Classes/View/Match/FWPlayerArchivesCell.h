//
//  FWPlayerArchivesCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 车手档案cell
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerArchivesCell : UITableViewCell

@property (nonatomic, strong) UIView *shadowView;
@property (nonatomic, strong) UIView * matchView;
@property (nonatomic, strong) UIImageView * typeImageView;
@property (nonatomic, strong) UILabel * matchNameLabel;
@property (nonatomic, strong) UILabel * groupNameLabel;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UILabel * carLabel;
@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UILabel * ETLabel;
@property (nonatomic, strong) UILabel * WeisuLabel;
@property (nonatomic, strong) UILabel * RTLabel;

@property (nonatomic, strong) UIView * verView;


@property (nonatomic, strong) UIView * scoreView;
@property (nonatomic, strong) UILabel * detailLabel;

@property (nonatomic, weak) UIViewController * vc;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
