//
//  FWGuessingRecordCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWGuessingRecordCell : UITableViewCell

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UIImageView * statusImageView;

@property (nonatomic, strong) UILabel * OneLabel;
@property (nonatomic, strong) UILabel * TwoLabel;
@property (nonatomic, strong) UILabel * ThreeLabel;
@property (nonatomic, strong) UILabel * FourLabel;
@property (nonatomic, strong) UILabel * FiveLabel;
@property (nonatomic, strong) UILabel * SixLabel;
@property (nonatomic, strong) UILabel * SevenLabel;
@property (nonatomic, strong) UILabel * EightLabel;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
