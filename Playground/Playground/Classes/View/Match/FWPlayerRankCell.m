//
//  FWPlayerRankCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerRankCell.h"

@implementation FWPlayerRankCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.contentView.layer.cornerRadius = 4;
        self.contentView.layer.masksToBounds = YES;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    self.titleLabel.textColor = FWTextColor_272727;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(20);
        make.height.mas_equalTo(18);
        make.left.right.mas_equalTo(self.contentView);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 41/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.contentView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView).mas_offset(-25);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(41, 41));
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nameLabel.textColor = FWTextColor_272727;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.photoImageView).mas_offset(2);
        make.height.mas_equalTo(18);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
    }];
    
    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.ETLabel.textColor = FWColor(@"38A4F8");
    self.ETLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.height.mas_equalTo(18);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
    }];
}

- (void)configForCell:(id)model{
    
    FWGroupListModel * listModel = (FWGroupListModel *)model;
    
    if (listModel.score_list.count > 0 ) {
        FWScoreDetailModel * scoreListModel = listModel.score_list[0];
        
        self.titleLabel.text = listModel.group_info.group_name;
        self.nameLabel.text = scoreListModel.nickname;
        self.ETLabel.text = [NSString stringWithFormat:@"ET：%@",scoreListModel.et];
        [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:scoreListModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    }
}


@end
