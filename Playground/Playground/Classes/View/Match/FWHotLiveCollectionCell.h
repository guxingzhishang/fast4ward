//
//  FWHotLiveCollectionCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "RCCRMessageBaseCell.h"
#define TextMessageFontSize 16

NS_ASSUME_NONNULL_BEGIN

@interface FWHotLiveCollectionCell : RCCRMessageBaseCell

/*!
 显示消息内容的Label
 */
@property(nonatomic, strong) UILabel *textLabel;
/* 内容背景 */
@property(nonatomic, strong) UIView * contentBgView;


+ (CGSize)getMessageCellSize:(NSString *)content withWidth:(CGFloat)width;

@end

NS_ASSUME_NONNULL_END
