//
//  FWPhoneView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWPhoneViewDelegate <NSObject>

#pragma mark - > 下一步
- (void)nextButtonClick:(UIButton *)sender;

@end

@interface FWPhoneView : UIView<UITextFieldDelegate>

/**
 * 主标题
 */
@property (nonatomic, strong) UILabel * titleLabel;

/**
 * 副标题
 */
@property (nonatomic, strong) UILabel * subTitleLabel;

/**
 * 名称
 */
@property (nonatomic, strong) UILabel * nameLabel;

/**
 * 手机号输入框
 */
@property (nonatomic, strong) UITextField * phoneTextFeild;

/**
 * 底线
 */
@property (nonatomic, strong) UIView * lineView;

/**
 * 满足条件图标
 */
@property (nonatomic, strong) UIImageView * rightImageView;

/**
 * 下一步
 */
@property (nonatomic, strong) UIButton * nextButton;

/**
 * 区别是手机号登录，还是绑定手机号
 * 1 ：手机号登录   2 ：微信登录绑定手机号
 */
@property (nonatomic, assign) NSInteger type;

@property (nonatomic, weak) id<FWPhoneViewDelegate> delegate;


@end

