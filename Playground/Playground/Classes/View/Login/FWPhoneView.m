//
//  FWPhoneView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWPhoneView.h"

@implementation FWPhoneView
@synthesize titleLabel;
@synthesize subTitleLabel;
@synthesize nameLabel;
@synthesize phoneTextFeild;
@synthesize lineView;
@synthesize rightImageView;
@synthesize nextButton;
@synthesize type;

- (id)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setType:(NSInteger)type{
    
    if (type == 1) {
        titleLabel.text = @"欢迎使用肆放";
        [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    }else if(type == 2){
        titleLabel.text = @"首次登录请绑定手机号";
        [nextButton setTitle:@"确定" forState:UIControlStateNormal];
    }else if(type == 3){
        titleLabel.text = @"请输入新手机号";
        [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    }
}

- (void)setupSubviews{
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:23];
    titleLabel.textColor = FWTextColor_12101D;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(46);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
        make.left.mas_equalTo(self).mas_offset(40);
        make.top.mas_equalTo(self).mas_offset(40);
    }];

    subTitleLabel = [[UILabel alloc] init];
    subTitleLabel.text = @"请放心使用手机号只用于登录验证";
    subTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    subTitleLabel.textColor = FWTextColor_12101D;
    subTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:subTitleLabel];
    [subTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(titleLabel);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(5);
    }];
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.text = @"手机号";
    nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    nameLabel.textColor = FWTextColor_12101D;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(18);
        make.left.mas_equalTo(subTitleLabel);
        make.width.mas_equalTo(subTitleLabel);
        make.top.mas_equalTo(subTitleLabel.mas_bottom).mas_offset(75);
    }];
    
    phoneTextFeild = [[UITextField alloc] init];
    phoneTextFeild.delegate = self;
    phoneTextFeild.placeholder = @"请输入手机号";
    phoneTextFeild.textColor = FWTextColor_12101D;
    phoneTextFeild.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:18];
    phoneTextFeild.keyboardType = UIKeyboardTypeNumberPad;
    [self addSubview:phoneTextFeild];
    [phoneTextFeild mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel);
        make.right.mas_equalTo(self).mas_offset(-40);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-80, 30));
        make.top.mas_equalTo(nameLabel.mas_bottom).mas_offset(15);
    }];
    
    rightImageView = [[UIImageView alloc] init];
    rightImageView.hidden = YES;
    rightImageView.image = [UIImage imageNamed:@"select"];
    [self addSubview:rightImageView];
    [rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(phoneTextFeild);
        make.size.mas_equalTo(CGSizeMake(12, 12));
        make.right.mas_equalTo(phoneTextFeild).mas_offset(-10);
    }];
    
    lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWTextColor_12101D;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(phoneTextFeild);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(phoneTextFeild.mas_bottom).mas_offset(5);
    }];
    
    nextButton = [[UIButton alloc] init];
    nextButton.layer.cornerRadius = 2;
    nextButton.layer.masksToBounds = YES;
    nextButton.backgroundColor = FWTextColor_222222;
    nextButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:16];;
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:nextButton];
    [nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH-80);
        make.height.mas_equalTo(48);
        make.right.mas_equalTo(self).mas_offset(-40);
        make.left.mas_equalTo(self).mas_offset(40);
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(110.5);
    }];
}

#pragma mark - > 下一步
- (void)nextButtonOnClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(nextButtonClick:)]) {
        [self.delegate nextButtonClick:sender];
    }
}

#pragma mark - > 输入框代理方法
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    

    if (textField.text.length >= 11) {
        rightImageView.hidden = NO;
        if (![string isEqualToString:@""]) {
            // 不是删除取前11位
            textField.text = [textField.text substringToIndex:10];
        }
    }else{
        rightImageView.hidden = YES;
    }
    
    return YES;
}

@end
