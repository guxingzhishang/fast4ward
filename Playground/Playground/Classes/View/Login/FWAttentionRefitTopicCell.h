//
//  FWAttentionRefitTopicCell.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/12.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWAttentionRefitTopicCell : UITableViewCell

@property (nonatomic, strong) UIView * oneBgView;
@property (nonatomic, strong) UIImageView * onePhotoImageView;
@property (nonatomic, strong) UIButton * oneSelectButton;
@property (nonatomic, strong) UILabel * oneNameLabel;
@property (nonatomic, strong) UILabel * oneDesLabel;

@property (nonatomic, strong) UIView * twoBgView;
@property (nonatomic, strong) UIImageView * twoPhotoImageView;
@property (nonatomic, strong) UIButton * twoSelectButton;
@property (nonatomic, strong) UILabel * twoNameLabel;
@property (nonatomic, strong) UILabel * twoDesLabel;

@property (nonatomic, strong) UIView * threeBgView;
@property (nonatomic, strong) UIImageView * threePhotoImageView;
@property (nonatomic, strong) UIButton * threeSelectButton;
@property (nonatomic, strong) UILabel * threeNameLabel;
@property (nonatomic, strong) UILabel * threeDesLabel;

@property (nonatomic, weak) UIViewController * vc;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
