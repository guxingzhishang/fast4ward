//
//  FWLoginView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWLoginView.h"
#import "UILabel+YBAttributeTextTapAction.h"

#define YBAlertShow(messageText,buttonName) \
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:(messageText) \
delegate:nil cancelButtonTitle:(buttonName) otherButtonTitles: nil];\
[alert show];

@implementation FWLoginView
@synthesize bgImageView;
@synthesize ballImageView;
@synthesize fast4wardLabel;
@synthesize contentLabel;
@synthesize wechatLoginButton;
@synthesize phoneButton;
@synthesize visitorButton;

- (id)init{
    self = [super init];
    if (self) {
        
        [self setupSubviews];
    }
    
    return self;
}


- (void)setupSubviews{
    
    bgImageView = [[UIImageView alloc] init];
    bgImageView.image = [UIImage imageNamed:@"login_bg"];
    [self addSubview:bgImageView];
    [bgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    ballImageView = [[UIImageView alloc] init];
    ballImageView.image = [UIImage imageNamed:@"login_middle"];
    [self addSubview:ballImageView];
    [ballImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.centerY.mas_equalTo(self).mas_offset(+60);
        make.size.mas_equalTo(CGSizeMake(80, 104));
    }];
   
    wechatLoginButton = [[UIButton alloc] init];
    wechatLoginButton.layer.borderColor = [UIColor colorWithHexString:@"FFFFFF" alpha:0.4].CGColor;
    wechatLoginButton.layer.borderWidth = 1;
    wechatLoginButton.layer.cornerRadius = 5;
    wechatLoginButton.layer.masksToBounds = YES;
    wechatLoginButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:15];
    [wechatLoginButton setTitle:@"微信登录" forState:UIControlStateNormal];
    [wechatLoginButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [wechatLoginButton addTarget:self action:@selector(wechatLoginButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:wechatLoginButton];
    [wechatLoginButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(58);
        make.right.mas_equalTo(self).mas_offset(-58);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-116, 46));
        make.top.mas_equalTo(ballImageView.mas_bottom).mas_offset(34);
    }];
    
    
    phoneButton = [[UIButton alloc] init];
    phoneButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
    [phoneButton setTitle:@"手机登录" forState:UIControlStateNormal];
    [phoneButton setTitleColor:[UIColor colorWithHexString:@"FFFFFF" alpha:0.6] forState:UIControlStateNormal];
    [phoneButton addTarget:self action:@selector(phoneButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:phoneButton];
    [phoneButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(54, 35));
        make.top.mas_equalTo(wechatLoginButton.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self).mas_offset(58);
    }];
 
    
    visitorButton = [[UIButton alloc] init];
    visitorButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
    visitorButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//    visitorButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, );
    [visitorButton setTitle:@"游客" forState:UIControlStateNormal];
    [visitorButton setTitleColor:[UIColor colorWithHexString:@"FFFFFF" alpha:0.6] forState:UIControlStateNormal];
    [visitorButton addTarget:self action:@selector(visitorButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:visitorButton];
    [visitorButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(phoneButton);
        make.top.mas_equalTo(phoneButton);
        make.right.mas_equalTo(self).mas_offset(-58);
    }];
    
    //需要点击的字符不同
    NSString *label_text2 = @"继续表示您同意 社区规范 和 隐私政策 ";
    NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc]initWithString:label_text2];
//    [attributedString2 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, label_text2.length)];
//    [attributedString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FFFFFF" alpha:0.7] range:NSMakeRange(7, 6)];
//    [attributedString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"FFFFFF" alpha:0.7] range:NSMakeRange(14, 6)];
    
    UILabel *ybLabel2 = [[UILabel alloc] init];
    ybLabel2.backgroundColor = FWClearColor;
    ybLabel2.numberOfLines = 1;
    ybLabel2.font = [UIFont fontWithName:@"PingFangSC-Light" size:12];;
    ybLabel2.textColor = [UIColor colorWithHexString:@"FFFFFF" alpha:0.7];
    ybLabel2.attributedText = attributedString2;
    //设置是否有点击效果，默认是YES
    ybLabel2.enabledTapEffect = NO;
    [self addSubview:ybLabel2];
    [ybLabel2 mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.mas_bottom).mas_offset(-34);
    }];
    
    
    [ybLabel2 yb_addAttributeTapActionWithStrings:@[@" 社区规范 ",@" 隐私政策 "] tapClicked:^(NSString *string, NSRange range, NSInteger index) {
        // 社区规范，隐私协议
        if ([self.delegate respondsToSelector:@selector(agreementClick:)]) {
            [self.delegate agreementClick:index];
        }
    }];
    
}

#pragma mark - > 微信登录
-(void) wechatLoginButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(wechatLoginButtonClick)]) {
        [self.delegate wechatLoginButtonClick];
    }
}

#pragma mark - > 手机登录
- (void)phoneButtonOnClick{
   
    if ([self.delegate respondsToSelector:@selector(phoneButtonClick)]) {
        [self.delegate phoneButtonClick];
    }
}

#pragma mark - > 游客访问
- (void)visitorButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(visitorButtonClick)]) {
        [self.delegate visitorButtonClick];
    }
}

@end
