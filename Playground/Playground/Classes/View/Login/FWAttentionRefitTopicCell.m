//
//  FWAttentionRefitTopicCell.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/12.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWAttentionRefitTopicCell.h"
#import "FWAttentionRefitTopicModel.h"

@interface FWAttentionRefitTopicCell ()
@property (nonatomic, strong) FWAttentionRefitTopicModel * topicModel;
@property (nonatomic, strong) NSArray * userArray ;
@end

@implementation FWAttentionRefitTopicCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.oneBgView = [[UIView alloc] init];
    self.oneBgView.backgroundColor = FWColor(@"f7f7f7");
    [self.contentView addSubview:self.oneBgView];
    [self.oneBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-10);
        make.left.mas_equalTo(self.contentView).mas_offset(18);
        make.width.mas_equalTo((SCREEN_WIDTH-36-20)/3);
        make.height.mas_equalTo((126*(SCREEN_WIDTH-36-20)/3)/106);
    }];
    
    self.twoBgView = [[UIView alloc] init];
    self.twoBgView.backgroundColor = FWColor(@"f7f7f7");
    [self.contentView addSubview:self.twoBgView];
    [self.twoBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.oneBgView);
        make.bottom.mas_equalTo(self.oneBgView);
        make.left.mas_equalTo(self.oneBgView.mas_right).mas_offset(10);
        make.width.mas_equalTo(self.oneBgView);
        make.height.mas_equalTo(self.oneBgView);
    }];
    
    self.threeBgView = [[UIView alloc] init];
    self.threeBgView.backgroundColor = FWColor(@"f7f7f7");
    [self.contentView addSubview:self.threeBgView];
    [self.threeBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.twoBgView);
        make.bottom.mas_equalTo(self.twoBgView);
        make.left.mas_equalTo(self.twoBgView.mas_right).mas_offset(10);
        make.width.mas_equalTo(self.twoBgView);
        make.height.mas_equalTo(self.twoBgView);
    }];
    
    self.onePhotoImageView = [[UIImageView alloc] init];
    self.onePhotoImageView.layer.cornerRadius = 65/2;
    self.onePhotoImageView.layer.masksToBounds = YES;
    [self.oneBgView addSubview:self.onePhotoImageView];
    [self.onePhotoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.oneBgView).mas_offset(10);
        make.centerX.mas_equalTo(self.oneBgView);
        make.size.mas_equalTo(CGSizeMake(65, 65));
    }];
    
    self.twoPhotoImageView = [[UIImageView alloc] init];
    self.twoPhotoImageView.layer.cornerRadius = 65/2;
    self.twoPhotoImageView.layer.masksToBounds = YES;
    [self.twoBgView addSubview:self.twoPhotoImageView];
    [self.twoPhotoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.twoBgView).mas_offset(10);
        make.centerX.mas_equalTo(self.twoBgView);
        make.size.mas_equalTo(self.onePhotoImageView);
    }];
    
    self.threePhotoImageView = [[UIImageView alloc] init];
    self.threePhotoImageView.layer.cornerRadius = 65/2;
    self.threePhotoImageView.layer.masksToBounds = YES;
    [self.threeBgView addSubview:self.threePhotoImageView];
    [self.threePhotoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.threeBgView).mas_offset(10);
        make.centerX.mas_equalTo(self.threeBgView);
        make.size.mas_equalTo(self.twoPhotoImageView);
    }];
    
    self.oneSelectButton = [[UIButton alloc] init];
    [self.oneSelectButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
    [self.oneSelectButton setImage:[UIImage imageNamed:@"new_select"] forState:UIControlStateSelected];
    [self.oneSelectButton addTarget:self action:@selector(oneSelectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.oneBgView addSubview:self.oneSelectButton];
    [self.oneSelectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerX.mas_equalTo(self.onePhotoImageView);
        make.top.mas_equalTo(self.onePhotoImageView.mas_bottom).mas_offset(-10);
    }];
    
    self.twoSelectButton = [[UIButton alloc] init];
    [self.twoSelectButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
    [self.twoSelectButton setImage:[UIImage imageNamed:@"new_select"] forState:UIControlStateSelected];
    [self.twoSelectButton addTarget:self action:@selector(twoSelectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.twoBgView addSubview:self.twoSelectButton];
    [self.twoSelectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerX.mas_equalTo(self.twoPhotoImageView);
        make.top.mas_equalTo(self.twoPhotoImageView.mas_bottom).mas_offset(-10);
    }];
    
    self.threeSelectButton = [[UIButton alloc] init];
    [self.threeSelectButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
    [self.threeSelectButton setImage:[UIImage imageNamed:@"new_select"] forState:UIControlStateSelected];
    [self.threeSelectButton addTarget:self action:@selector(threeSelectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.threeBgView addSubview:self.threeSelectButton];
    [self.threeSelectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerX.mas_equalTo(self.threePhotoImageView);
        make.top.mas_equalTo(self.threePhotoImageView.mas_bottom).mas_offset(-10);
    }];

    self.oneNameLabel = [[UILabel alloc] init];
    self.oneNameLabel.font = DHBoldFont(12);
    self.oneNameLabel.textColor = FWTextColor_222222;
    self.oneNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.oneBgView addSubview:self.oneNameLabel];
    [self.oneNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.onePhotoImageView.mas_bottom).mas_offset(15);
        make.left.right.mas_equalTo(self.oneBgView);
        make.height.mas_equalTo(17);
    }];
    
    self.twoNameLabel = [[UILabel alloc] init];
    self.twoNameLabel.font = DHBoldFont(12);
    self.twoNameLabel.textColor = FWTextColor_222222;
    self.twoNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.twoBgView addSubview:self.twoNameLabel];
    [self.twoNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.twoPhotoImageView.mas_bottom).mas_offset(15);
        make.left.right.mas_equalTo(self.twoBgView);
        make.height.mas_equalTo(17);
    }];
    
    self.threeNameLabel = [[UILabel alloc] init];
    self.threeNameLabel.font = DHBoldFont(12);
    self.threeNameLabel.textColor = FWTextColor_222222;
    self.threeNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.threeBgView addSubview:self.threeNameLabel];
    [self.threeNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.threePhotoImageView.mas_bottom).mas_offset(15);
        make.left.right.mas_equalTo(self.threeBgView);
        make.height.mas_equalTo(17);
    }];

    
    self.oneDesLabel = [[UILabel alloc] init];
    self.oneDesLabel.font = DHBoldFont(12);
    self.oneDesLabel.textColor = DHTitleColor_666666;
    self.oneDesLabel.textAlignment = NSTextAlignmentCenter;
    [self.oneBgView addSubview:self.oneDesLabel];
    [self.oneDesLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.oneNameLabel.mas_bottom).mas_offset(5);
        make.left.right.mas_equalTo(self.oneBgView);
        make.height.mas_equalTo(17);
    }];
    
    self.twoDesLabel = [[UILabel alloc] init];
    self.twoDesLabel.font = DHBoldFont(12);
    self.twoDesLabel.textColor = DHTitleColor_666666;
    self.twoDesLabel.textAlignment = NSTextAlignmentCenter;
    [self.twoBgView addSubview:self.twoDesLabel];
    [self.twoDesLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.twoNameLabel.mas_bottom).mas_offset(15);
        make.left.right.mas_equalTo(self.twoBgView);
        make.height.mas_equalTo(17);
    }];
    
    self.threeDesLabel = [[UILabel alloc] init];
    self.threeDesLabel.font = DHBoldFont(12);
    self.threeDesLabel.textColor = DHTitleColor_666666;
    self.threeDesLabel.textAlignment = NSTextAlignmentCenter;
    [self.threeBgView addSubview:self.threeDesLabel];
    [self.threeDesLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.threeNameLabel.mas_bottom).mas_offset(15);
        make.left.right.mas_equalTo(self.threeBgView);
        make.height.mas_equalTo(17);
    }];
}


- (void)configForCell:(id)model{
    
    self.userArray = (NSArray *)model;
    
    for (int i = 0; i<self.userArray.count; i++) {
        FWAttentionRefitTopicUserModel * userModel = self.userArray[i];
        
        if (i == 0) {
            if (userModel.isSelectUser) {
                self.oneSelectButton.selected = YES;
            }else{
                self.oneSelectButton.selected = NO;
            }
            self.oneNameLabel.text = userModel.nickname;
            self.oneDesLabel.text = @"";
            [self.onePhotoImageView sd_setImageWithURL:[NSURL URLWithString:userModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
        }else if(i == 1){
            if (userModel.isSelectUser) {
                self.twoSelectButton.selected = YES;
            }else{
                self.twoSelectButton.selected = NO;
            }
            self.twoNameLabel.text = userModel.nickname;
            self.twoDesLabel.text = @"";
            [self.twoPhotoImageView sd_setImageWithURL:[NSURL URLWithString:userModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
        }else if(i == 2){
            if (userModel.isSelectUser) {
                self.threeSelectButton.selected = YES;
            }else{
                self.threeSelectButton.selected = NO;
            }
            self.threeNameLabel.text = userModel.nickname;
            self.threeDesLabel.text = @"";
            [self.threePhotoImageView sd_setImageWithURL:[NSURL URLWithString:userModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
        }
    }
}

- (void)oneSelectButtonClick{
    
    FWAttentionRefitTopicUserModel * userModel = self.userArray[0];
    if (userModel.isSelectUser) {
        userModel.isSelectUser = NO;
        self.oneSelectButton.selected = NO;
    }else{
        userModel.isSelectUser = YES;
        self.oneSelectButton.selected = YES;
    }
}

- (void)twoSelectButtonClick{
    
    FWAttentionRefitTopicUserModel * userModel = self.userArray[1];
    if (userModel.isSelectUser) {
        userModel.isSelectUser = NO;
        self.twoSelectButton.selected = NO;
    }else{
        userModel.isSelectUser = YES;
        self.twoSelectButton.selected = YES;
    }
}

- (void)threeSelectButtonClick{
    
    FWAttentionRefitTopicUserModel * userModel = self.userArray[2];
    if (userModel.isSelectUser) {
        userModel.isSelectUser = NO;
        self.threeSelectButton.selected = NO;
    }else{
        userModel.isSelectUser = YES;
        self.threeSelectButton.selected = YES;
    }
}
@end
