//
//  FWLoginView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWLoginViewDelegate <NSObject>

#pragma mark - > 微信登录
-(void) wechatLoginButtonClick;

#pragma mark - > 手机登录
- (void)phoneButtonClick;

#pragma mark - > 游客访问
- (void)visitorButtonClick;

#pragma mark - > 社区规范，隐私政策
- (void)agreementClick:(NSInteger)index;
@end

@interface FWLoginView : UIView

/**
 * 背景图标
 */
@property (nonatomic, strong) UIImageView * bgImageView;

/**
 * 中间图标
 */
@property (nonatomic, strong) UIImageView * ballImageView;

/**
 * FAST4WARD字体
 */
@property (nonatomic, strong) UILabel * fast4wardLabel;

/**
 * 文案
 */
@property (nonatomic, strong) UILabel * contentLabel;

/**
 * 微信登录按钮
 */
@property (nonatomic, strong) UIButton * wechatLoginButton;

/**
 * 手机登录
 */
@property (nonatomic, strong) UIButton * phoneButton;

/**
 * 游客访问
 */
@property (nonatomic, strong) UIButton * visitorButton;

@property (nonatomic, weak) id<FWLoginViewDelegate> delegate;

@end
