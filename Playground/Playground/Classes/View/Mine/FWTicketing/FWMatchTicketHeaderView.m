//
//  FWMatchTicketHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchTicketHeaderView.h"

@implementation FWMatchTicketHeaderView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.topImageView = [[UIImageView alloc] init];
    self.topImageView.layer.cornerRadius = 2;
    self.topImageView.layer.masksToBounds = YES;
    self.topImageView.userInteractionEnabled = YES;
    [self addSubview:self.topImageView];
    [self.topImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self).mas_offset(14);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_equalTo(146*SCREEN_WIDTH/375);
    }];
    [self.topImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topImageViewClick)]];
}

- (void)topImageViewClick{
    
}

- (void)configForView:(NSString *)banner{
    
    self.topImageView.clipsToBounds = YES;
    self.topImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:banner] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

@end
