//
//  FWLogCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWLogModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWLogCell : UITableViewCell

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * nicknameLabel;
@property (nonatomic, strong) UILabel * orederNumLabel;
@property (nonatomic, strong) UILabel * countLabel;
@property (nonatomic, strong) UILabel * timeLabel;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
