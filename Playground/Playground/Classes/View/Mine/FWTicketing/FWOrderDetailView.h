//
//  FWOrderDetailView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWTicketDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWOrderDetailView : UIScrollView

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * topView;

@property (nonatomic, strong) UILabel * statusLabel;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * remainTimeLabel;
@property (nonatomic, strong) UILabel * remainTimeStringLabel;
@property (nonatomic, strong) UILabel * countLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UILabel * tipLabel;

@property (nonatomic, strong) UIView * codeMoudleView;
@property (nonatomic, strong) UIImageView * statusImageView;
@property (nonatomic, strong) UIImageView * codeImageView;
@property (nonatomic, strong) UILabel * codeLabel;
@property (nonatomic, strong) UILabel * ticketLabel;

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIButton * deleteButton;

@property (nonatomic, strong) NSString * order_id;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWWechatOrderModel * wechatModel;
@property (nonatomic, strong) FWTicketDetailModel * detailModel;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
