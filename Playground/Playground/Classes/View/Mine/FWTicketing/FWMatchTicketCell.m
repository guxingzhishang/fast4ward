//
//  FWMatchTicketCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchTicketCell.h"

@implementation FWMatchTicketCell
@synthesize titleLabel;
@synthesize timeLabel;
@synthesize picImageView;
@synthesize priceLabel;
@synthesize buyButton;
@synthesize addressLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    picImageView = [UIImageView new];
    picImageView.image = [UIImage imageNamed:@"placeholder"];
    picImageView.contentMode = UIViewContentModeScaleAspectFill;
    picImageView.layer.cornerRadius = 2;
    picImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:picImageView];
    [picImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(111, 148));
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.numberOfLines = 2;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(picImageView);
        make.left.mas_equalTo(picImageView.mas_right).mas_offset(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    timeLabel.textColor = FWColor(@"A8ACB3");
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(self.contentView).mas_offset(-20);
        make.height.mas_equalTo(16);
    }];
    
    addressLabel = [[UILabel alloc] init];
    addressLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    addressLabel.textColor = FWColor(@"A8ACB3");
    addressLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:addressLabel];
    [addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(timeLabel.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(titleLabel);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(16);
    }];
    
    priceLabel = [[UILabel alloc] init];
    priceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 20];
    priceLabel.textColor = FWColor(@"ff6f00");
    priceLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:priceLabel];
    [priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(25);
        make.bottom.mas_equalTo(self.picImageView).mas_offset(-5);
    }];
    
    buyButton = [[UIButton alloc] init];
    buyButton.layer.cornerRadius = 2;
    buyButton.layer.masksToBounds = YES;
    buyButton.titleLabel.font = DHSystemFontOfSize_12;
    buyButton.backgroundColor = FWTextColor_222222;;
    [buyButton setTitle:@"购票" forState:UIControlStateNormal];
    [buyButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    buyButton.enabled = NO;
    [self.contentView addSubview:buyButton];
    [buyButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(57, 25));
        make.right.mas_equalTo(self.titleLabel);
        make.centerY.mas_equalTo(self.priceLabel);
    }];
}

- (void)configForCell:(id)model{
    
    self.listModel = (FWMatchTicketsListModel *)model;

    titleLabel.text = self.listModel.title;
    timeLabel.text = [NSString stringWithFormat:@"日期:%@",self.listModel.sport_date];
    addressLabel.text = [NSString stringWithFormat:@"地点:%@",self.listModel.sport_area];
    priceLabel.text = self.listModel.price;
    [picImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.list_page_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}


@end
