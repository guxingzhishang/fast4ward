//
//  FWConfirmOrderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWConfirmOrderView.h"

@implementation FWConfirmOrderView

- (id)init{
    self = [super init];
    if (self) {
        self.haveCarType = @"0";
        self.sexType = @"0";
        
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.mainView = [[UIView alloc] init];
    self.mainView.userInteractionEnabled = YES;
    [self addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.mainView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mainView).mas_offset(10);
        make.left.mas_equalTo(self.mainView).mas_offset(14);
        make.right.mas_equalTo(self.mainView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
    }];

    self.topView = [[UIView alloc] init];
    [self.shadowView addSubview:self.topView];
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topView).mas_offset(15);
        make.left.mas_equalTo(self.topView).mas_offset(13);
        make.right.mas_equalTo(self.topView).mas_offset(-13);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.dateLabel.textColor = FWColor(@"A8ACB3");
    self.dateLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.dateLabel];
    [self.dateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(11);
        make.left.mas_equalTo(self.titleLabel);
        make.width.mas_equalTo(self.topView).mas_offset(0);
        make.height.mas_equalTo(17);
    }];
    
    self.areaLabel = [[UILabel alloc] init];
    self.areaLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.areaLabel.textColor = FWColor(@"A8ACB3");
    self.areaLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.areaLabel];
    [self.areaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.titleLabel);
        make.width.mas_equalTo(self.topView);
        make.height.mas_equalTo(16);
    }];
    
    self.countLabel = [[UILabel alloc] init];
    self.countLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.countLabel.textColor = FWColor(@"222222");
    self.countLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.countLabel];
    [self.countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.areaLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.titleLabel);
        make.width.mas_equalTo(self.topView);
        make.height.mas_equalTo(16);
        make.bottom.mas_equalTo(self.topView).mas_offset(-20);
    }];

    self.fillInInfoLabel =  [[UILabel alloc] init];
    self.fillInInfoLabel.text = @"请填写信息";
    self.fillInInfoLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    self.fillInInfoLabel.textColor = FWColor(@"222222");
    self.fillInInfoLabel.textAlignment = NSTextAlignmentLeft;
    [self.mainView addSubview:self.fillInInfoLabel];
    [self.fillInInfoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.shadowView.mas_bottom).mas_offset(15);
        make.left.mas_equalTo(self.mainView).mas_offset(14);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(16);
    }];

    /* 姓名 */
    self.nameLabel =  [[UILabel alloc] init];
    self.nameLabel.text = @"姓名";
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    self.nameLabel.textColor = FWColor(@"222222");
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.mainView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.fillInInfoLabel.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.fillInInfoLabel);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(37);
    }];
    
    self.nameTextView = [[IQTextView alloc] init];
    self.nameTextView.delegate = self;
    self.nameTextView.placeholder = @"请输入您的姓名";
    self.nameTextView.textAlignment = NSTextAlignmentRight;
    self.nameTextView.textContainerInset = UIEdgeInsetsMake(5,0,0,-5);
    self.nameTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.nameTextView.textColor = FWTextColor_12101D;
    [self.mainView addSubview:self.nameTextView];
    [self.nameTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mainView).mas_offset(-14);
        make.height.mas_equalTo(self.nameLabel);
        make.width.mas_greaterThanOrEqualTo(200);
        make.centerY.mas_equalTo(self.nameLabel);
    }];
    
    self.nameLineView = [[UIView alloc] init];
    self.nameLineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.mainView addSubview:self.nameLineView];
    [self.nameLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.right.mas_equalTo(self.nameTextView);
        make.height.mas_offset(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH - 28);
    }];
    
    /* 手机号 */
    self.phoneLabel =  [[UILabel alloc] init];
    self.phoneLabel.text = @"手机号";
    self.phoneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    self.phoneLabel.textColor = FWColor(@"222222");
    self.phoneLabel.textAlignment = NSTextAlignmentLeft;
    [self.mainView addSubview:self.phoneLabel];
    [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(15);
        make.left.mas_equalTo(self.nameLabel);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(37);
    }];
    
    self.phoneTextView = [[IQTextView alloc] init];
    self.phoneTextView.placeholder = @"请输入您的手机号码";
    self.phoneTextView.delegate = self;
    self.phoneTextView.textAlignment = NSTextAlignmentRight;
    self.phoneTextView.textContainerInset = UIEdgeInsetsMake(5,0,0,-5);
    self.phoneTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.phoneTextView.textColor = FWTextColor_12101D;
    [self.mainView addSubview:self.phoneTextView];
    [self.phoneTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mainView).mas_offset(-14);
        make.height.mas_equalTo(self.phoneLabel);
        make.width.mas_greaterThanOrEqualTo(200);
        make.centerY.mas_equalTo(self.phoneLabel);
    }];
    
    self.phoneLineView = [[UIView alloc] init];
    self.phoneLineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.mainView addSubview:self.phoneLineView];
    [self.phoneLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.phoneLabel);
        make.top.mas_equalTo(self.phoneLabel.mas_bottom);
        make.right.mas_equalTo(self.phoneTextView);
        make.height.mas_offset(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH - 28);
    }];
    
    /* 第三个 */
    self.threeLabel =  [[UILabel alloc] init];
    self.threeLabel.text = @"详细收货地址";
    self.threeLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    self.threeLabel.textColor = FWColor(@"222222");
    self.threeLabel.textAlignment = NSTextAlignmentLeft;
    [self.mainView addSubview:self.threeLabel];
    [self.threeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.phoneLabel.mas_bottom).mas_offset(15);
        make.left.mas_equalTo(self.phoneLabel);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(37);
    }];
    
    self.threeTextView = [[IQTextView alloc] init];
    self.threeTextView.placeholder = @"请按照 省/市/区、县/详细地址格式填写";
    self.threeTextView.delegate = self;
    self.threeTextView.textAlignment = NSTextAlignmentRight;
    self.threeTextView.textContainerInset = UIEdgeInsetsMake(9,0,0,-5);
    self.threeTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.threeTextView.textColor = FWTextColor_12101D;
    [self.mainView addSubview:self.threeTextView];
    [self.threeTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mainView).mas_offset(-14);
        make.height.mas_equalTo(self.threeLabel);
        make.width.mas_greaterThanOrEqualTo(250);
        make.centerY.mas_equalTo(self.threeLabel);
    }];
    
    self.threeLineView = [[UIView alloc] init];
    self.threeLineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.mainView addSubview:self.threeLineView];
    [self.threeLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.threeLabel);
        make.top.mas_equalTo(self.threeLabel.mas_bottom);
        make.right.mas_equalTo(self.threeTextView);
        make.height.mas_offset(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH - 28);
    }];
    
    
    /* 第四个 */
    self.fourLabel =  [[UILabel alloc] init];
    self.fourLabel.text = @"性别";
    self.fourLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    self.fourLabel.textColor = FWColor(@"222222");
    self.fourLabel.textAlignment = NSTextAlignmentLeft;
    [self.mainView addSubview:self.fourLabel];
    [self.fourLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.threeLabel.mas_bottom).mas_offset(15);
        make.left.mas_equalTo(self.threeLabel);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(37);
    }];
    
    self.femaleButton = [[UIButton alloc] init];
    self.femaleButton.titleLabel.font =  [UIFont fontWithName:@"PingFang SC" size: 14];
    [self.femaleButton setTitle:@"  女" forState:UIControlStateNormal];
    [self.femaleButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.femaleButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    [self.femaleButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.femaleButton addTarget:self action:@selector(femaleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.femaleButton];
    [self.femaleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mainView);
        make.height.mas_equalTo(self.fourLabel);
        make.centerY.mas_equalTo(self.fourLabel);
        make.width.mas_equalTo(70);
    }];
    
    self.maleButton = [[UIButton alloc] init];
    self.maleButton.titleLabel.font =  [UIFont fontWithName:@"PingFang SC" size: 14];
    [self.maleButton setTitle:@"  男" forState:UIControlStateNormal];
    [self.maleButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.maleButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    [self.maleButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.maleButton addTarget:self action:@selector(maleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.maleButton];
    [self.maleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.femaleButton.mas_left).mas_offset(-50);
        make.height.mas_equalTo(self.femaleButton);
        make.centerY.mas_equalTo(self.femaleButton);
        make.width.mas_equalTo(self.femaleButton);
    }];
    
    
    self.fourLineView = [[UIView alloc] init];
    self.fourLineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.mainView addSubview:self.fourLineView];
    [self.fourLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.fourLabel.mas_bottom);
        make.left.mas_equalTo(self.fourLabel);
        make.right.mas_equalTo(self.mainView).mas_offset(-14);
        make.height.mas_offset(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH - 28);
    }];
    
    /* 第五个 */
    self.fiveLabel =  [[UILabel alloc] init];
    self.fiveLabel.text = @"是否有车";
    self.fiveLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    self.fiveLabel.textColor = FWColor(@"222222");
    self.fiveLabel.textAlignment = NSTextAlignmentLeft;
    [self.mainView addSubview:self.fiveLabel];
    [self.fiveLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.fourLabel.mas_bottom).mas_offset(15);
        make.left.mas_equalTo(self.fourLabel);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(37);
    }];
   
    self.noCarButton = [[UIButton alloc] init];
    self.noCarButton.selected = YES;
    self.noCarButton.titleLabel.font =  [UIFont fontWithName:@"PingFang SC" size: 14];
    [self.noCarButton setTitle:@"  无" forState:UIControlStateNormal];
    [self.noCarButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.noCarButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    [self.noCarButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.noCarButton addTarget:self action:@selector(noCarButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.noCarButton];
    [self.noCarButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mainView);
        make.height.mas_equalTo(self.fiveLabel);
        make.centerY.mas_equalTo(self.fiveLabel);
        make.width.mas_equalTo(70);
    }];
    
    self.haveCarButton = [[UIButton alloc] init];
    self.haveCarButton.titleLabel.font =  [UIFont fontWithName:@"PingFang SC" size: 14];
    [self.haveCarButton setTitle:@"  有" forState:UIControlStateNormal];
    [self.haveCarButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.haveCarButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    [self.haveCarButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.haveCarButton addTarget:self action:@selector(haveCarButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.haveCarButton];
    [self.haveCarButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.noCarButton.mas_left).mas_offset(-50);
        make.height.mas_equalTo(self.noCarButton);
        make.centerY.mas_equalTo(self.noCarButton);
        make.width.mas_equalTo(self.noCarButton);
    }];
    
    
    self.fiveLineView = [[UIView alloc] init];
    self.fiveLineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.mainView addSubview:self.fiveLineView];
    [self.fiveLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.fiveLabel.mas_bottom);
        make.left.mas_equalTo(self.fiveLabel);
        make.right.mas_equalTo(self.mainView).mas_offset(-14);
        make.height.mas_offset(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH - 28);
    }];
    
    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = FWColor(@"F6F8FA");
    [self.mainView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 10));
        make.top.mas_equalTo(self.fiveLabel.mas_bottom).mas_offset(20);
    }];
    
    self.payMethodLabel =  [[UILabel alloc] init];
    self.payMethodLabel.text = @"请选择支付方式";
    self.payMethodLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    self.payMethodLabel.textColor = FWColor(@"222222");
    self.payMethodLabel.textAlignment = NSTextAlignmentLeft;
    [self.mainView addSubview:self.payMethodLabel];
    [self.payMethodLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.fillInInfoLabel);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(20);
    }];

}

- (void)configForView:(id)model{
    
    FWTicketDetailModel * detailModel = (FWTicketDetailModel *)model;
    
    self.titleLabel.text = detailModel.title;
    self.dateLabel.text = [NSString stringWithFormat:@"日期：%@",detailModel.sport_date];
    self.areaLabel.text = [NSString stringWithFormat:@"地点：%@",detailModel.sport_area];
    
    /* 姓名 */
    if ([detailModel.if_real_name isEqualToString:@"1"]) {

        self.nameLabel.text =@"姓名";
        self.nameTextView.hidden = NO;
        self.nameLineView.hidden = NO;

        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.fillInInfoLabel.mas_bottom).mas_offset(20);
            make.left.mas_equalTo(self.fillInInfoLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(37);
        }];
    }else{
        self.nameLabel.text =@"";
        self.nameTextView.hidden = YES;
        self.nameLineView.hidden = YES;

        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.fillInInfoLabel.mas_bottom).mas_offset(0.01);
            make.left.mas_equalTo(self.fillInInfoLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(0.01);
        }];
    }


    if ([detailModel.if_mobile isEqualToString:@"1"]) {
        self.phoneLabel.text =@"手机号";
        self.phoneTextView.hidden = NO;
        self.phoneLineView.hidden = NO;

        [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(15);
            make.left.mas_equalTo(self.nameLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(37);
        }];
    }else{
        self.phoneLabel.text =@"";
        self.phoneTextView.hidden = YES;
        self.phoneLineView.hidden = YES;

        [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0.01);
            make.left.mas_equalTo(self.nameLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(0.01);
        }];
    }

    if ([detailModel.if_address isEqualToString:@"1"]) {
        self.threeLabel.text =@"详细收货地址";
        self.threeTextView.hidden = NO;
        self.threeLineView.hidden = NO;

        [self.threeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.phoneLabel.mas_bottom).mas_offset(15);
            make.left.mas_equalTo(self.phoneLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(37);
        }];
    }else{
        self.threeLabel.text =@"";
        self.threeTextView.hidden = YES;
        self.threeLineView.hidden = YES;

        [self.threeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.phoneLabel.mas_bottom).mas_offset(0.01);
            make.left.mas_equalTo(self.phoneLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(0.01);
        }];
    }

    if ([detailModel.if_sex isEqualToString:@"1"]) {
        self.fourLabel.text =@"性别";
        self.femaleButton.hidden = NO;
        self.maleButton.hidden = NO;
        self.fourLineView.hidden = NO;

        self.sexType = @"0";
        
        [self.fourLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.threeLabel.mas_bottom).mas_offset(15);
            make.left.mas_equalTo(self.threeLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(37);
        }];
    }else{
        self.fourLabel.text =@"";
        self.femaleButton.hidden = YES;
        self.maleButton.hidden = YES;
        self.fourLineView.hidden = YES;

        self.sexType = @"0";
        
        [self.fourLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.threeLabel.mas_bottom).mas_offset(0.01);
            make.left.mas_equalTo(self.threeLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(0.01);
        }];
    }

    if ([detailModel.if_car isEqualToString:@"1"]) {
        self.fiveLabel.text =@"是否有车";
        self.haveCarButton.hidden = NO;
        self.noCarButton.hidden = NO;
        self.fiveLineView.hidden = NO;

        self.haveCarType = @"2";
        
        [self.fiveLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.fourLabel.mas_bottom).mas_offset(15);
            make.left.mas_equalTo(self.fourLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(37);
        }];
    }else{
        self.fiveLabel.text =@"";
        self.haveCarButton.hidden = YES;
        self.noCarButton.hidden = YES;
        self.fiveLineView.hidden = YES;

        self.haveCarType = @"0";

        [self.fiveLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.fourLabel.mas_bottom).mas_offset(0.01);
            make.left.mas_equalTo(self.fourLabel);
            make.width.mas_equalTo(80);
            make.height.mas_equalTo(0.01);
        }];
    }
    
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 10));
        make.top.mas_equalTo(self.fiveLabel.mas_bottom).mas_offset(20);
    }];
    
    [self.payMethodLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.fillInInfoLabel);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(20);
    }];
    
    [self setupPayView];
}

- (void)setupPayView{
    
    NSArray * titleArray = @[@"微信支付"];
    NSArray * iconArray = @[@"wechat_pay"];
    
    for (int i = 0; i < titleArray.count; i++) {
        
        UIButton * payMethodButton = [[UIButton alloc] init];
        payMethodButton.tag = 1000+i;
        [payMethodButton addTarget:self action:@selector(payMethodButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.mainView addSubview:payMethodButton];
        [payMethodButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mainView).mas_offset(0);
            make.top.mas_equalTo(self.payMethodLabel.mas_bottom).mas_offset(5);
            make.height.mas_equalTo(53);
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.mainView).mas_offset(-174);
        }];
        
        UIImageView * icon = [UIImageView new];
        icon.image = [UIImage imageNamed:iconArray[i]];
        [payMethodButton addSubview:icon];
        [icon mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payMethodButton);
            make.size.mas_equalTo(CGSizeMake(19, 19));
            make.left.mas_equalTo(payMethodButton).mas_offset(20);
        }];
        
        UILabel * payTitleLabel = [UILabel new];
        payTitleLabel.text = titleArray[i];
        payTitleLabel.font = DHSystemFontOfSize_15;
        payTitleLabel.textColor = FWTextColor_12101D;
        payTitleLabel.textAlignment = NSTextAlignmentLeft;
        [payMethodButton addSubview:payTitleLabel];
        [payTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payMethodButton);
            make.size.mas_equalTo(CGSizeMake(200, 25));
            make.left.mas_equalTo(icon.mas_right).mas_offset(5);
        }];
        
        UIImageView * selectImageView = [UIImageView new];
        selectImageView.tag = payMethodButton.tag +1000;
        [payMethodButton addSubview:selectImageView];
        [selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payMethodButton);
            make.size.mas_equalTo(CGSizeMake(19, 19));
            make.right.mas_equalTo(payMethodButton).mas_offset(-20);
        }];
        
        if (i == 0) {
            selectImageView.image = [UIImage imageNamed:@"radio_save_sel"];
        }else{
            selectImageView.image = [UIImage imageNamed:@"radio_save_unsel"];
        }
        
        UIView * lineView = [[UIView alloc] init];
        lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
        [self.mainView addSubview:lineView];
        [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(payMethodButton.mas_bottom).mas_offset(1);
            make.height.mas_equalTo(0.5);
            make.width.mas_greaterThanOrEqualTo(100);
            make.left.mas_equalTo(payMethodButton).mas_offset(14);
            make.right.mas_equalTo(payMethodButton).mas_offset(-14);
        }];
    }
}

#pragma mark - > 男
- (void)maleButtonClick{
    self.sexType = @"1";
    self.femaleButton.selected = NO;
    self.maleButton.selected = YES;
}

#pragma mark - > 女
- (void)femaleButtonClick{
    self.sexType = @"2";
    self.femaleButton.selected = YES;
    self.maleButton.selected = NO;
}

#pragma mark - > 有车
- (void)haveCarButtonClick{
    self.haveCarType = @"1";
    self.noCarButton.selected = NO;
    self.haveCarButton.selected = YES;
}

#pragma mark - > 无车
- (void)noCarButtonClick{
    self.haveCarType = @"2";
    self.noCarButton.selected = YES;
    self.haveCarButton.selected = NO;
}

#pragma mark - > 支付
- (void)payMethodButtonClick:(UIButton *)sender{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (textView == self.nameTextView) {
        if ([text isEqualToString:@""]) {
            return YES;
        }
        
        if ([self isInputRuleAndNumber:text]) {
            // 2. 截取
            NSString * tempString = [NSString stringWithFormat:@"%@%@",self.nameTextView.text,text];
            
            if (tempString.length > 13) {
                self.nameTextView.text = [tempString substringToIndex:12];
                return NO;
            }
        }else{
            return NO;
        }
    }

    if (textView == self.threeTextView) {
        if ([text isEqualToString:@""]) {
            return YES;
        }
        
        NSString * tempString = [textView.text stringByAppendingString:text];
        if (tempString.length >=80) {
            tempString = [tempString substringToIndex:80];
            
            self.threeTextView.text = tempString;
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - 谓词条件限制
/**
 pattern中,输入需要验证的通过的字符
 小写a-z
 大写A-Z
 汉字\u4E00-\u9FA5
 系统九宫格中文输入法下，点击按钮输出的是"➋➌➍➎➏➐➑➒" \u278b-\u2792
 "➋➌➍➎➏➐➑➒" 对应ASCII码 \u278b\u278c\u278d\u278e\u278f\u2790\u2791\u2792
 @param str 要过滤的字符
 @return YES 只允许输入字母和汉字
 */
- (BOOL)isInputRuleAndNumber:(NSString *)str {
    // \u278b-\\u2792
    NSString *pattern = @"^[a-zA-Z\\u4E00-\\u9FA5\\u278b-\\u2792]+$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:str];
    return isMatch;
}

@end
