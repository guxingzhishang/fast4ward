//
//  FWMatchTicketHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 赛事门票
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchTicketHeaderView : UIView

@property (nonatomic, strong) UIImageView * topImageView;

- (void)configForView:(NSString *)banner;

@end

NS_ASSUME_NONNULL_END
