//
//  FWConfirmOrderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWTicketDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWConfirmOrderView : UIScrollView<UITextViewDelegate>

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * topView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * dateLabel;
@property (nonatomic, strong) UILabel * areaLabel;
@property (nonatomic, strong) UILabel * countLabel;

@property (nonatomic, strong) UILabel * fillInInfoLabel;// 请填写信息
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) IQTextView * nameTextView;
@property (nonatomic, strong) UIView * nameLineView;

@property (nonatomic, strong) UILabel * phoneLabel;
@property (nonatomic, strong) IQTextView * phoneTextView;
@property (nonatomic, strong) UIView * phoneLineView;

@property (nonatomic, strong) UILabel * threeLabel;
@property (nonatomic, strong) IQTextView * threeTextView;
@property (nonatomic, strong) UIView * threeLineView;

@property (nonatomic, strong) UILabel * fourLabel;
@property (nonatomic, strong) UIButton * maleButton;
@property (nonatomic, strong) UIButton * femaleButton;
@property (nonatomic, strong) UIView * fourLineView;

@property (nonatomic, strong) UILabel * fiveLabel;
@property (nonatomic, strong) UIButton * haveCarButton;
@property (nonatomic, strong) UIButton * noCarButton;
@property (nonatomic, strong) UIView * fiveLineView;

@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UILabel * payMethodLabel;

@property (nonatomic, strong) NSString * sexType;
@property (nonatomic, strong) NSString * haveCarType;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
