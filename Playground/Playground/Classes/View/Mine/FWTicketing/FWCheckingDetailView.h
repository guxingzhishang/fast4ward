//
//  FWCheckingDetailView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWCheckModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCheckingDetailView : UIView


@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UILabel * statusLabel;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * orederNumLabel;
@property (nonatomic, strong) UILabel * checkLabel;
@property (nonatomic, strong) UILabel * hasCheckLabel;
@property (nonatomic, strong) UILabel * timeLabel;

@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UILabel * countLabel;


@property (nonatomic, strong) UIImageView * minButton;
@property (nonatomic, strong) UIImageView * maxButton;
@property (nonatomic, strong) UIButton * confirmButton;

@property (nonatomic, assign) NSInteger  count;
@property (nonatomic, assign) NSInteger tempCount;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, strong) FWCheckModel * checkModel;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
