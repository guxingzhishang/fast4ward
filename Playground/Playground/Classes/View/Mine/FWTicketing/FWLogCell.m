//
//  FWLogCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWLogCell.h"

@implementation FWLogCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-5);
    }];
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.titleLabel.textColor = FWTextColor_222222;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(15);
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-13);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_equalTo(SCREEN_WIDTH-56);
    }];
    
    self.nicknameLabel = [[UILabel alloc] init];
    self.nicknameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.nicknameLabel.textColor = FWColor(@"A8ACB3");
    self.nicknameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nicknameLabel];
    [self.nicknameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.titleLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(self.titleLabel);
    }];
    
    self.orederNumLabel = [[UILabel alloc] init];
    self.orederNumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.orederNumLabel.textColor = FWColor(@"A8ACB3");
    self.orederNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.orederNumLabel];
    [self.orederNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nicknameLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.nicknameLabel);
        make.right.mas_equalTo(self.nicknameLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(self.nicknameLabel);
    }];
    
    
    self.countLabel = [[UILabel alloc] init];
    self.countLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.countLabel.textColor = FWColor(@"A8ACB3");
    self.countLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.countLabel];
    [self.countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.orederNumLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.orederNumLabel);
        make.right.mas_equalTo(self.orederNumLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(self.orederNumLabel);
    }];

    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.timeLabel.textColor = FWColor(@"A8ACB3");
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.countLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.countLabel);
        make.right.mas_equalTo(self.countLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(self.countLabel);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-10);
    }];
    
}

- (void)configForCell:(id)model{
    
    FWLogListModel * listModel = (FWLogListModel *)model;
    
    self.titleLabel.text = listModel.title;
    self.nicknameLabel.text = [NSString stringWithFormat:@"昵称：%@",listModel.nickname];
    self.orederNumLabel.text = [NSString stringWithFormat:@"订单号：%@",listModel.order_number];
    self.countLabel.text = [NSString stringWithFormat:@"张数：%@张",listModel.check_number];
    self.timeLabel.text = [NSString stringWithFormat:@"核销时间：%@",listModel.check_time];
}


@end
