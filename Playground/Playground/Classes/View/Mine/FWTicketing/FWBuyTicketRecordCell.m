//
//  FWBuyTicketRecordCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBuyTicketRecordCell.h"

@implementation FWBuyTicketRecordCell
@synthesize titleLabel;
@synthesize timeLabel;
@synthesize picImageView;
@synthesize statusImageView;
@synthesize priceLabel;
@synthesize statusLabel;
@synthesize addressLabel;
@synthesize bgView;
@synthesize shadowView;
@synthesize countLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self setupSubViews];
    }
    
    return self;
}


- (void)setupSubViews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(7);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(176);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-7);
    }];
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    picImageView = [UIImageView new];
    picImageView.image = [UIImage imageNamed:@"placeholder"];
    picImageView.contentMode = UIViewContentModeScaleAspectFill;
    picImageView.clipsToBounds = YES;
    picImageView.layer.cornerRadius = 5;
    picImageView.layer.masksToBounds = YES;
    [self.bgView addSubview:picImageView];
    [picImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(60, 80));
        make.right.mas_equalTo(self.bgView).mas_offset(-10);
        make.top.mas_equalTo(self.bgView).mas_offset(19);
    }];
    
    statusImageView = [UIImageView new];
    statusImageView.image = [UIImage imageNamed:@"placeholder"];
    statusImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.bgView addSubview:statusImageView];
    [statusImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(89, 72));
        make.right.bottom.mas_equalTo(self.bgView);
    }];
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.numberOfLines = 2;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(picImageView);
        make.left.mas_equalTo(self.bgView).mas_offset(10);
        make.right.mas_equalTo(self.picImageView.mas_left).mas_offset(-10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    priceLabel = [[UILabel alloc] init];
    priceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 20];
    priceLabel.textColor = FWColor(@"FF3A3A");
    priceLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:priceLabel];
    [priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(25);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(10);
    }];
    
    statusLabel = [[UILabel alloc] init];
    statusLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    statusLabel.textColor = FWColor(@"FF3A3A");
    statusLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:statusLabel];
    [statusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(priceLabel.mas_right).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(25);
        make.centerY.mas_equalTo(self.priceLabel);
    }];
    
    countLabel = [[UILabel alloc] init];
    countLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    countLabel.textColor = FWColor(@"A8ACB3");
    countLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:countLabel];
    [countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(16);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(10);
    }];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    timeLabel.textColor = FWColor(@"A8ACB3");
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(countLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(self.bgView).mas_offset(-20);
        make.height.mas_equalTo(16);
    }];
    
    addressLabel = [[UILabel alloc] init];
    addressLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    addressLabel.textColor = FWColor(@"A8ACB3");
    addressLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:addressLabel];
    [addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(timeLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(self.bgView).mas_offset(-20);
        make.height.mas_equalTo(16);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-20);
    }];
}

- (void)configForCell:(id)model{
    
    self.listModel = (FWBuyTicketRecordListModel *)model;

    titleLabel.text = self.listModel.title;
    timeLabel.text = [NSString stringWithFormat:@"日期:%@",self.listModel.sport_date];
    addressLabel.text = [NSString stringWithFormat:@"地点:%@",self.listModel.sport_area];
    priceLabel.text = [NSString stringWithFormat:@"￥%@",self.listModel.pay_fee];
    countLabel.text = [NSString stringWithFormat:@"数量:%@张",self.listModel.buy_number];
    
    statusLabel.text = @"购票成功";
    statusLabel.textColor = FWColor(@"222222");
    
   if ([self.listModel.order_status isEqualToString:@"1"]) {
        /* 未支付 */
        statusLabel.text = @"未支付";
        statusLabel.textColor = FWColor(@"FF3A3A");
        statusImageView.hidden = YES;
    }else if ([self.listModel.order_status isEqualToString:@"2"]) {
        /* 未使用 */
        statusImageView.hidden = YES;
    }else if ([self.listModel.order_status isEqualToString:@"3"]) {
        /* 已使用 */
        statusImageView.hidden = NO;
        statusImageView.image = [UIImage imageNamed:@"record_status_used"];
    }else if ([self.listModel.order_status isEqualToString:@"4"]) {
        /* 订单已失效 */
        statusLabel.text = @"已取消";
        statusLabel.textColor = FWColor(@"A8ACB3");
        
        statusImageView.hidden = NO;
        statusImageView.image = [UIImage imageNamed:@"record_status_enabled"];
    }else if ([self.listModel.order_status isEqualToString:@"5"]) {
        /* 门票已过期 */
        statusImageView.hidden = NO;
        statusImageView.image = [UIImage imageNamed:@"record_status_missed"];
    }

    
    [picImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.list_page_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}


@end
