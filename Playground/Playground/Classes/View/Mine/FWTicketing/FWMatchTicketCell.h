//
//  FWMatchTicketCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 赛事门票
 */
#import <UIKit/UIKit.h>
#import "FWMatchTicketsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchTicketCell : UITableViewCell

@property (nonatomic, strong) UIImageView * picImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UIButton * buyButton;

@property (nonatomic, strong) FWMatchTicketsListModel * listModel;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
