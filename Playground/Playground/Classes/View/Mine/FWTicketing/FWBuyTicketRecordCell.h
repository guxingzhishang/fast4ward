//
//  FWBuyTicketRecordCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 购票记录
 */
#import <UIKit/UIKit.h>
#import "FWBuyTicketRecordModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWBuyTicketRecordCell : UITableViewCell

@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIView * shadowView;

@property (nonatomic, strong) UIImageView * picImageView;
@property (nonatomic, strong) UIImageView * statusImageView;

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UILabel * statusLabel;
@property (nonatomic, strong) UILabel * countLabel;

@property (nonatomic, strong) FWBuyTicketRecordListModel * listModel;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
