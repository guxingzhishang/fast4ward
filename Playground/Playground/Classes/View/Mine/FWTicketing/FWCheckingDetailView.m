//
//  FWCheckingDetailView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCheckingDetailView.h"

@implementation FWCheckingDetailView
@synthesize checkModel;

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).mas_offset(10);
        make.left.mas_equalTo(self).mas_offset(14);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
    }];
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.statusLabel = [[UILabel alloc] init];
    self.statusLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 20];
    self.statusLabel.textColor = FWColor(@"FF3A3A");
    self.statusLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.statusLabel];
    [self.statusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(15);
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-13);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(SCREEN_WIDTH-56);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.titleLabel.textColor = FWTextColor_222222;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statusLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.statusLabel);
        make.right.mas_equalTo(self.statusLabel);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_equalTo(SCREEN_WIDTH-56);
    }];
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.timeLabel.textColor = FWColor(@"A8ACB3");
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.titleLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(self.titleLabel);
    }];
    
    
    self.orederNumLabel = [[UILabel alloc] init];
    self.orederNumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.orederNumLabel.textColor = FWColor(@"A8ACB3");
    self.orederNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.orederNumLabel];
    [self.orederNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.timeLabel);
        make.right.mas_equalTo(self.timeLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(self.timeLabel);
    }];
    
    
    self.checkLabel = [[UILabel alloc] init];
    self.checkLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.checkLabel.textColor = FWColor(@"A8ACB3");
    self.checkLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.checkLabel];
    [self.checkLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.orederNumLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.orederNumLabel);
        make.right.mas_equalTo(self.orederNumLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(self.orederNumLabel);
    }];
    
    self.hasCheckLabel = [[UILabel alloc] init];
    self.hasCheckLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.hasCheckLabel.textColor = FWColor(@"A8ACB3");
    self.hasCheckLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.hasCheckLabel];
    [self.hasCheckLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.checkLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.checkLabel);
        make.right.mas_equalTo(self.checkLabel);
        make.height.mas_equalTo(16);
        make.width.mas_equalTo(self.checkLabel);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-20);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"请选择核销数量";
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 20];
    self.tipLabel.textColor = FWColor(@"222222");
    self.tipLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_bottom).mas_offset(25);
        make.centerX.mas_equalTo(self);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    
    self.countLabel = [[UILabel alloc] init];
    self.countLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 20];
    self.countLabel.textColor = FWColor(@"222222");
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.countLabel];
    [self.countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(25);
        make.centerX.mas_equalTo(self.bgView);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(50);
    }];
    
    self.maxButton = [[UIImageView alloc] init];
    self.maxButton.userInteractionEnabled = YES;
    self.maxButton.image = [UIImage imageNamed:@"check_add"] ;
    [self addSubview:self.maxButton];
    [self.maxButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.countLabel.mas_right).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(45, 40));
        make.centerY.mas_equalTo(self.countLabel);
    }];
    [self.maxButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(maxButtonClick)]];

    
    self.minButton = [[UIImageView alloc] init];
    self.minButton.userInteractionEnabled = YES;
    self.minButton.image = [UIImage imageNamed:@"check_sub"] ;
    [self addSubview:self.minButton];
    [self.minButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.countLabel.mas_left).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(45, 40));
        make.centerY.mas_equalTo(self.countLabel);
    }];
    [self.minButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(minButtonClick)]];

    [self layoutIfNeeded];
    
    self.confirmButton = [[UIButton alloc] init];
    self.confirmButton.layer.cornerRadius = 20.5;
    self.confirmButton.layer.masksToBounds = YES;
    [self.confirmButton addTarget:self action:@selector(confirmButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.confirmButton];
    self.confirmButton.frame = CGRectMake(56, SCREEN_HEIGHT-41-84-FWSafeBottom-(64+FWCustomeSafeTop), SCREEN_WIDTH-112, 41);
    
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.confirmButton.bounds;
    [self.confirmButton.layer addSublayer:gradientLayer];
    gradientLayer.colors = @[(__bridge id)FWColor(@"6B95EF").CGColor,
                             (__bridge id)FWColor(@"3E68DC").CGColor];
    
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    
    self.confirmButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [self.confirmButton setTitle:@"确认核销" forState:UIControlStateNormal];
    [self.confirmButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
}

- (void)configForView:(id)model{
    
    self.checkModel = (FWCheckModel *)model;
    
    if ([checkModel.buy_number integerValue] > [checkModel.check_number integerValue]) {
        self.count= [checkModel.buy_number integerValue] - [checkModel.check_number integerValue];
        self.tempCount = self.count;

        self.countLabel.text = @(self.count).stringValue;
    }
    self.titleLabel.text = checkModel.title;
    self.orederNumLabel.text = [NSString stringWithFormat:@"订单号：%@",checkModel.order_number];
    self.timeLabel.text = [NSString stringWithFormat:@"使用日期：%@",checkModel.sport_date];
    self.checkLabel.text = [NSString stringWithFormat:@"数量：%@张",checkModel.buy_number];
    self.hasCheckLabel.text = [NSString stringWithFormat:@"已核销：%@张",checkModel.check_number];
    
    if ([checkModel.check_status isEqualToString:@"1"]) {
        /* 未使用 */
        self.hasCheckLabel.text = @"";
        self.statusLabel.text = @"未核销入场";
        
        self.confirmButton.hidden = NO;
        self.tipLabel.hidden = NO;
        self.minButton.hidden = NO;
        self.maxButton.hidden = NO;
        self.countLabel.hidden = NO;
        
        [self.hasCheckLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.checkLabel.mas_bottom).mas_offset(8);
            make.left.mas_equalTo(self.checkLabel);
            make.right.mas_equalTo(self.checkLabel);
            make.height.mas_equalTo(0.01);
            make.width.mas_equalTo(self.checkLabel);
            make.bottom.mas_equalTo(self.bgView).mas_offset(-20);
        }];
    }else if ([checkModel.check_status isEqualToString:@"2"]) {
        /* 部分使用 */
        self.statusLabel.text = @"部分核销";
        self.confirmButton.hidden = NO;
        self.tipLabel.hidden = NO;
        self.minButton.hidden = NO;
        self.maxButton.hidden = NO;
        self.countLabel.hidden = NO;
        
    }else if ([checkModel.check_status isEqualToString:@"3"]) {
        /* 全部使用 */
        self.statusLabel.text = @"已核销入场";
        self.confirmButton.hidden = YES;
        self.confirmButton.hidden = YES;
        self.tipLabel.hidden = YES;
        self.minButton.hidden = YES;
        self.maxButton.hidden = YES;
        self.countLabel.hidden = YES;
        
    }else if ([checkModel.check_status isEqualToString:@"4"]) {
        /* 已过期 */
        self.statusLabel.text = @"门票已过期";
        self.confirmButton.hidden = YES;
        self.confirmButton.hidden = YES;
        self.tipLabel.hidden = YES;
        self.minButton.hidden = YES;
        self.maxButton.hidden = YES;
        self.countLabel.hidden = YES;
        
    }
}

- (void)maxButtonClick{
    self.tempCount += 1;

    if (self.tempCount <= self.count) {
        self.countLabel.text = @(self.tempCount).stringValue;
//        self.maxButton.image = [UIImage imageNamed:@"buy_add"] ;
//        self.minButton.image = [UIImage imageNamed:@"buy_sub"] ;
//
//        if (self.tempCount + 1 >= self.count) {
//            self.maxButton.image = [UIImage imageNamed:@"buy_max"] ;
//        }
    }else{
        self.tempCount = self.count;
        [[FWHudManager sharedManager] showErrorMessage:@"已达上限" toController:self.vc];
    }
}

- (void)minButtonClick{
    
    self.tempCount -= 1;
    
    if (self.tempCount > 0) {
        self.countLabel.text = @(self.tempCount).stringValue;
//        self.minButton.image = [UIImage imageNamed:@"buy_sub"] ;
//        self.maxButton.image = [UIImage imageNamed:@"buy_add"] ;
//
//        if (self.tempCount <= 1) {
//            self.minButton.image = [UIImage imageNamed:@"buy_min"] ;
//        }
    }else{
        self.tempCount = 1;
        [[FWHudManager sharedManager] showErrorMessage:@"最少选择一个" toController:self.vc];
    }
}

#pragma mark - > 二次确认核销
- (void)confirmButtonClick{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"请确认是否核销当前订单！" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self requestCheckData];
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 确认核销
- (void)requestCheckData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"order_id":self.checkModel.order_id,
                              @"check_number":@(self.tempCount).stringValue,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Check_ticket WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"核销成功" toController:self.vc];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.vc.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

@end

