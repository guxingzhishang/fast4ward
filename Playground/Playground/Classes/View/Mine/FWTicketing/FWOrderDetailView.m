//
//  FWOrderDetailView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWOrderDetailView.h"

@interface FWOrderDetailView()

@property (nonatomic, strong) NSTimer * checkTimer;
@property (nonatomic, assign) NSInteger time;

@end

@implementation FWOrderDetailView
@synthesize detailModel;
@synthesize checkTimer;
@synthesize time;

- (void)dealloc{
    
    [checkTimer invalidate];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)init{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySucceed) name:FWWXReturnSussessPayNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payFailed) name:FWWXReturnFailedPayNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selfPayFailed) name:FWSelfControlFailedPayNotification object:nil];

        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.mainView = [[UIView alloc] init];
    [self addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.mainView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mainView).mas_offset(10);
        make.left.mas_equalTo(self.mainView).mas_offset(14);
        make.right.mas_equalTo(self.mainView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(100);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
    }];
    
    self.topView = [[UIView alloc] init];
    [self.shadowView addSubview:self.topView];
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.statusLabel = [[UILabel alloc] init];
    self.statusLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 20];
    self.statusLabel.textColor = FWColor(@"#ff6f00");
    self.statusLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.statusLabel];
    [self.statusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topView).mas_offset(20);
        make.left.mas_equalTo(self.topView).mas_offset(13);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(25);
    }];
    
    self.remainTimeStringLabel = [[UILabel alloc] init];
    self.remainTimeStringLabel.text = @"";
    self.remainTimeStringLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 13];
    self.remainTimeStringLabel.textColor = FWColor(@"#FF3A3A");
    self.remainTimeStringLabel.textAlignment = NSTextAlignmentRight;
    [self.topView addSubview:self.remainTimeStringLabel];
    [self.remainTimeStringLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.statusLabel);
        make.right.mas_equalTo(self.topView).mas_offset(-65);
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(100);
    }];
    
    self.remainTimeLabel = [[UILabel alloc] init];
    self.remainTimeLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 13];
    self.remainTimeLabel.textColor = FWColor(@"#FF3A3A");
    self.remainTimeLabel.numberOfLines = 1;
    self.remainTimeLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.remainTimeLabel];
    [self.remainTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.remainTimeStringLabel);
        make.left.mas_equalTo(self.remainTimeStringLabel.mas_right).mas_offset(0);
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(80);
    }];
    
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.titleLabel.textColor = FWColor(@"252527");
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statusLabel.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(self.statusLabel);
        make.right.mas_equalTo(self.topView).mas_offset(-13);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.countLabel = [[UILabel alloc] init];
    self.countLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.countLabel.textColor = FWColor(@"A8ACB3");
    self.countLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.countLabel];
    [self.countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(self.titleLabel);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(18);
    }];
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.timeLabel.textColor = FWColor(@"A8ACB3");
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.countLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.titleLabel);
        make.width.mas_equalTo(self.topView).mas_offset(0);
        make.height.mas_equalTo(17);
    }];
    
    self.codeMoudleView = [[UIView alloc] init];
    self.codeMoudleView.clipsToBounds = YES;
    [self.topView addSubview:self.codeMoudleView];
    [self.codeMoudleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.topView);
        make.width.mas_equalTo(self.topView);
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(13);
        make.height.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.topView);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.codeMoudleView addSubview:self.lineView];
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.timeLabel);
        make.right.mas_equalTo(self.codeMoudleView).mas_offset(-13);
        make.top.mas_equalTo(self.codeMoudleView);
        make.height.mas_equalTo(0.5);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.tipLabel.textColor = FWColor(@"222222");
    self.tipLabel.textAlignment = NSTextAlignmentCenter;
    [self.codeMoudleView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.lineView.mas_bottom).mas_offset(15);
        make.centerX.mas_equalTo(self.codeMoudleView);
        make.width.mas_equalTo(self.codeMoudleView);
        make.height.mas_equalTo(18);
    }];
    
    
    self.codeImageView = [[UIImageView alloc] init];
    self.codeImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.codeImageView.clipsToBounds = YES;
    [self.codeMoudleView addSubview:self.codeImageView];
    [self.codeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tipLabel).mas_offset(-7);
        make.centerX.mas_equalTo(self.topView);
        make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-100), (SCREEN_WIDTH-100)));
    }];
    
    self.statusImageView = [[UIImageView alloc] init];
    [self.codeMoudleView addSubview:self.statusImageView];
    [self.statusImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.codeImageView.mas_right);
        make.right.mas_equalTo(self.topView).mas_offset(-10);
        make.top.mas_equalTo(self.codeImageView).mas_offset(0);
        make.height.width.mas_equalTo(58);
    }];
    
    self.codeLabel = [[UILabel alloc] init];
    self.codeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.codeLabel.textColor = FWColor(@"A8ACB3");
    self.codeLabel.textAlignment = NSTextAlignmentCenter;
    [self.codeMoudleView addSubview:self.codeLabel];
    [self.codeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.codeImageView.mas_bottom).mas_offset(-15);
        make.centerX.mas_equalTo(self.topView);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(17);
    }];
    
    self.ticketLabel = [[UILabel alloc] init];
    self.ticketLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.ticketLabel.textColor = FWColor(@"A8ACB3");
    self.ticketLabel.textAlignment = NSTextAlignmentCenter;
    [self.codeMoudleView addSubview:self.ticketLabel];
    [self.ticketLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.codeLabel.mas_bottom).mas_offset(13);
        make.centerX.mas_equalTo(self.topView);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(17);
        make.bottom.mas_equalTo(self.codeMoudleView).mas_offset(-30);
    }];
    
    self.bottomView = [[UIView alloc] init];
    [self.mainView addSubview:self.bottomView];
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake( SCREEN_WIDTH, 28*8));
        make.top.mas_equalTo(self.topView.mas_bottom).mas_offset(20);
        make.left.right.mas_equalTo(self.mainView);
    }];
    
    
    self.deleteButton = [[UIButton alloc] init];
    self.deleteButton.layer.cornerRadius = 2;
    self.deleteButton.layer.masksToBounds = YES;
    self.deleteButton.titleLabel.font = DHSystemFontOfSize_16;
    self.deleteButton.backgroundColor = FWTextColor_222222;
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(deleteButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.deleteButton];
}


- (void)configForView:(id)model{
    
    detailModel = (FWTicketDetailModel *)model;
    
    self.titleLabel.text = detailModel.title;
    self.countLabel.text = [NSString stringWithFormat:@"数量：%@张",detailModel.buy_number];
    self.timeLabel.text = [NSString stringWithFormat:@"入场时间：%@",detailModel.entrance_time];
    
    self.tipLabel.text = @"*凭二维码核销后入场。";
    [self.codeMoudleView bringSubviewToFront:self.tipLabel];
    
    self.codeLabel.text = [NSString stringWithFormat:@"(入场辅助码：%@)",detailModel.order_code];
    
    [self.codeImageView sd_setImageWithURL:[NSURL URLWithString:detailModel.qrcode_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    if ([detailModel.order_status isEqualToString:@"1"]) {
        /* 未支付 */
        self.statusLabel.text = @"未支付";
        self.remainTimeStringLabel.text = @"剩余支付时间：";
        self.codeMoudleView.backgroundColor = FWClearColor;
        [self.codeMoudleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.topView);
            make.width.mas_equalTo(self.topView);
            make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(13);
            make.height.mas_equalTo(0.01);
            make.bottom.mas_equalTo(self.topView);
        }];
    }else if ([detailModel.order_status isEqualToString:@"2"]) {
        /* 未使用 */
        self.statusLabel.text = @"购票成功";
        self.remainTimeStringLabel.text = @"";
    }else if ([detailModel.order_status isEqualToString:@"3"]) {
        /* 已使用 */
        self.statusLabel.text = @"购票成功";
        self.statusImageView.image = [UIImage imageNamed:@"orderdetail_status_used"];
        self.remainTimeStringLabel.text = @"";
    }else if ([detailModel.order_status isEqualToString:@"4"]) {
        /* 订单已失效 */
        self.statusLabel.text = @"已取消";
        self.codeMoudleView.backgroundColor = FWClearColor;
        self.remainTimeStringLabel.text = @"";
        [self.codeMoudleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.topView);
            make.width.mas_equalTo(self.topView);
            make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(13);
            make.height.mas_equalTo(0.01);
            make.bottom.mas_equalTo(self.topView);
        }];
    }else if ([detailModel.order_status isEqualToString:@"5"]) {
        /* 门票已过期 */
        self.statusLabel.text = @"购票成功";
        self.remainTimeStringLabel.text = @"";
        self.statusImageView.image = [UIImage imageNamed:@"orderdetail_status_missed"];
    }
    
    
    
    NSString * checkString = detailModel.buy_number;
    NSString * hasCheckString = detailModel.check_number;
    NSString * ticketString = [NSString stringWithFormat:@"已购门票%@张/已核销%@张",checkString,hasCheckString];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:ticketString attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 13],NSForegroundColorAttributeName: [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]}];
    
    [string addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:58/255.0 blue:58/255.0 alpha:1.0]} range:NSMakeRange(4, checkString.length)];
    
    [string addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:58/255.0 blue:58/255.0 alpha:1.0]} range:NSMakeRange(9+checkString.length, hasCheckString.length)];
    
    self.ticketLabel.attributedText = string;
    
    
    
    NSMutableArray * leftArray = @[@"日期",@"地点",@"单价",@"总票价",@"姓名",@"手机号码",@"性别",@"是否有车",@"订单编号",@"付款时间",@"支付方式",@"详细收货地址"].mutableCopy;
    NSMutableArray * rightArray = @[detailModel.sport_date,detailModel.sport_area,detailModel.price,detailModel.pay_fee,detailModel.real_name,detailModel.mobile,detailModel.sex,detailModel.car,detailModel.order_number,detailModel.pay_time,detailModel.pay_type,detailModel.address].mutableCopy;
    NSInteger index;
    
    if (![detailModel.if_car isEqualToString:@"1"]) {
        index = [leftArray indexOfObject:@"是否有车"];

        [leftArray removeObject:@"是否有车"];
        [rightArray removeObjectAtIndex:index];
    }else{
        index = [leftArray indexOfObject:@"是否有车"];

        if ([detailModel.car isEqualToString:@"1"]) {
            // 有车
            if (rightArray.count > index) {
                [rightArray replaceObjectAtIndex:index withObject:@"有"];
            }
        }else{
            // 无车
            if (rightArray.count > index) {
                [rightArray replaceObjectAtIndex:index withObject:@"无"];
            }
        }
    }
    
    if (![detailModel.if_sex isEqualToString:@"1"]) {
        index = [leftArray indexOfObject:@"性别"];

        [leftArray removeObject:@"性别"];
        [rightArray removeObjectAtIndex:index];
    }else{
        index = [leftArray indexOfObject:@"性别"];

        if ([detailModel.sex isEqualToString:@"1"]) {
            // 男
            if (rightArray.count > index) {
                [rightArray replaceObjectAtIndex:index withObject:@"男"];
            }
        }else{
            // 女
            if (rightArray.count > index) {
                [rightArray replaceObjectAtIndex:index withObject:@"女"];
            }
        }
    }
    
    if (![detailModel.if_real_name isEqualToString:@"1"]) {
        index = [leftArray indexOfObject:@"姓名"];

        [leftArray removeObject:@"姓名"];
        [rightArray removeObjectAtIndex:index];
    }
    
    if (![detailModel.if_mobile isEqualToString:@"1"]) {
        index = [leftArray indexOfObject:@"手机号码"];

        [leftArray removeObject:@"手机号码"];
        [rightArray removeObjectAtIndex:index];
    }
    
    if (![detailModel.if_address isEqualToString:@"1"]) {
        index = [leftArray indexOfObject:@"详细收货地址"];

        [leftArray removeObject:@"详细收货地址"];
        [rightArray removeObjectAtIndex:index];
    }
    
    if ([detailModel.pay_status isEqualToString:@"1"]) {
        /* 未支付 */
        // 删除付款时间
        index = [leftArray indexOfObject:@"付款时间"];
        [leftArray removeObject:@"付款时间"];
        [rightArray removeObjectAtIndex:index];
        
        // 删除付款方式
        index = [leftArray indexOfObject:@"支付方式"];
        [leftArray removeObject:@"支付方式"];
        [rightArray removeObjectAtIndex:index];
        
        if ([detailModel.order_status isEqualToString:@"4"]) {
            /* 4/已取消 */
            self.deleteButton.enabled = YES;
            [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        }else{
            /* 1.未支付 */
            [self Countdown];
            [self.deleteButton setTitle:@"立即支付" forState:UIControlStateNormal];
        }
    }else if ([detailModel.pay_status isEqualToString:@"2"]) {
        /* 已支付 */
        
        if ([detailModel.order_status isEqualToString:@"3"]||
            [detailModel.order_status isEqualToString:@"5"]) {
            /* 3.已使用 、5.已过期 */
            [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        }else{
            /* 2.未使用 */
            self.deleteButton.enabled = NO;
            self.deleteButton.backgroundColor = FWClearColor;
            [self.deleteButton setTitle:@"" forState:UIControlStateNormal];
        }
    }
    
    
    for (int i = 0; i<leftArray.count; i++) {
        
        UILabel * leftLabel = [[UILabel alloc] init];
        leftLabel.font = [UIFont fontWithName:@"PingFang SC" size: 13];
        leftLabel.textColor = FWTextColor_222222;
        leftLabel.text = leftArray[i];
        leftLabel.textAlignment = NSTextAlignmentLeft;
        [self.bottomView addSubview:leftLabel];
        leftLabel.frame = CGRectMake(16, i*28, 80, 28);
        
        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = [UIFont fontWithName:@"PingFang SC" size: 13];
        rightLabel.textColor = FWTextColor_222222;
        rightLabel.text = rightArray[i];
        rightLabel.numberOfLines = 0;
        rightLabel.textAlignment = NSTextAlignmentRight;
        [self.bottomView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.bottomView).mas_offset(-14);
            make.top.mas_equalTo(leftLabel);
            make.left.mas_equalTo(leftLabel.mas_right).mas_offset(20);
            make.width.mas_equalTo(SCREEN_WIDTH-110);
            if (i == leftArray.count - 1) {
                make.bottom.mas_equalTo(self.bottomView);
                make.height.mas_greaterThanOrEqualTo(28);
            }else{
                make.height.mas_equalTo(28);
            }
        }];
    }
    
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(28*leftArray.count);
        make.top.mas_equalTo(self.topView.mas_bottom).mas_offset(20);
        make.left.right.mas_equalTo(self.mainView);
    }];
    
    [self.deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mainView).mas_offset(51);
        make.right.mas_equalTo(self.mainView).mas_offset(-51);
        make.top.mas_equalTo(self.bottomView.mas_bottom).mas_offset(40);
        make.height.mas_equalTo(44);
        make.width.mas_greaterThanOrEqualTo(100);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-40);
    }];
}


// 倒计时
- (void)Countdown
{
    time = [self.detailModel.pay_time_limit integerValue];

    int minutes = (time / 60) % 60;
    int seconds = time % 60;
    
    self.remainTimeLabel.text = [NSString stringWithFormat:@"00:%02ld:%02ld",(long)minutes,(long)seconds];

    checkTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self
                                                selector:@selector(handleTimer)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)handleTimer
{
    if (time != 0) {
        time -= 1;
        
        int minutes = (time / 60) % 60;
        int seconds = time % 60;
        
        self.remainTimeLabel.text = [NSString stringWithFormat:@"00:%02ld:%02ld",(long)minutes,(long)seconds];
    }else{
        [checkTimer invalidate];
        self.remainTimeLabel.text = @"00:00:00";
        
        self.statusLabel.text = @"已取消";
        self.statusLabel.textColor = FWColor(@"A8ACB3");
        [self.deleteButton setTitle:@"" forState:UIControlStateNormal];
        self.deleteButton.backgroundColor = FWClearColor;
        self.deleteButton.enabled = NO;
    }
}

#pragma mark - > 操作按钮
- (void)deleteButtonClick{
    
    if ([detailModel.order_status isEqualToString:@"1"]) {
        [self submitOrderPay:self.order_id];
    }else if ([detailModel.order_status isEqualToString:@"3"]||
        [detailModel.order_status isEqualToString:@"4"]||
        [detailModel.order_status isEqualToString:@"5"]) {
        /* 已支付 */
        /* 删除 */
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"请确认是否删除当前订单！" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self requestDeleteData];
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)requestDeleteData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"order_id":self.order_id,
                              };
    
    [request startWithParameters:params WithAction:Delete_ticket_order WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [[FWHudManager sharedManager] showErrorMessage:@"删除成功" toController:self.vc];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.vc.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

#pragma mark - > 调起微信支付
- (void)payrequestWithModel:(FWWechatOrderModel *)model {
    
    NSString * timeStampStr = model.timestamp;
    UInt32 timesta = (UInt32)[timeStampStr intValue];
    
    PayReq *request = [[PayReq alloc] init];
    request.sign      = model.sign;
    request.package   = @"Sign=WXPay";
    request.nonceStr  = model.noncestr;
    request.prepayId  = model.prepayid;
    request.partnerId = model.partnerid;
    request.timeStamp = timesta;
    
    /* 调起支付 */
    if ([WXApi sendReq:request]) {
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"支付失败" message:@"未安装微信客户端,请使用其他支付方式" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:okAction];
        [self.vc presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - > 微信pay成功
- (void)paySucceed{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"订单号"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"支付返回"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[FWHudManager sharedManager] showErrorMessage:@"支付成功" toController:self.vc];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.vc.navigationController popViewControllerAnimated:YES];
    });
}

#pragma mark - > 微信pay失败
- (void)payFailed{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付取消" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self.vc presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 自行操作返回pay失败
- (void)selfPayFailed{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付失败，请重试" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self.vc presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 提交支付(微信/支付宝)
- (void)submitOrderPay:(NSString *)order_id{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    if (order_id.length < 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"订单号有误，请重新购买" toController:self.vc];
        return;
    }
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"order_id":order_id,
                              @"pay_service":@"weixin",
                              };
    
    [request startWithParameters:params WithAction:Submit_ticket_pay WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            /* 微信支付 */
            NSDictionary * result = [[back objectForKey:@"data"] objectForKey:@"result_weixin"];
            NSDictionary * pay_order_id = [[back objectForKey:@"data"] objectForKey:@"pay_order_id"];
            
            self.wechatModel = [FWWechatOrderModel mj_objectWithKeyValues:result];
            [self payrequestWithModel:self.wechatModel];
            
            [[NSUserDefaults standardUserDefaults] setObject:pay_order_id forKey:@"订单号"];
            
            //            if ([type isEqualToString:@"weixin"]) {
            //            }else if ([type isEqualToString:@"alipay"]){
            //                /* 支付宝支付 */
            //                //                [[NSUserDefaults standardUserDefaults] setObject:[[back objectForKey:@"data"] objectForKey:@"pay_order_id"] forKey:@"订单号"];
            //                //                [self alipayRequestWithResult:[[back objectForKey:@"data"] objectForKey:@"result"]];
            //            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

@end
