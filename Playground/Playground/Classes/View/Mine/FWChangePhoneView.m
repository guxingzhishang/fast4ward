//
//  FWChangePhoneView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWChangePhoneView.h"
#import "HYConditionManager.h"

static NSInteger  MaxLength = 4;

@interface FWChangePhoneView ()

@end

@implementation FWChangePhoneView

@synthesize customView;
@synthesize codeTextView;
@synthesize containerView;
@synthesize tipLabel;
@synthesize delegate;
@synthesize nextButton;
@synthesize subLabel;
@synthesize backButton;

- (id)init{
    
    self = [super init];
    
    if (self){
        
        self.userInteractionEnabled = YES;
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        [self createTipLabel];
        [self createCodeView];
        [self cteateOtherView];
    }
    return self;
}

- (void)setPhoneNumber:(NSString *)phoneNumber{
    
    tipLabel.text = [NSString stringWithFormat:@"验证码已发送至 %@",phoneNumber];
}

- (void)createTipLabel{
    
    backButton = [[UIButton alloc] init];
    [backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [self addSubview:backButton];
    [backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.top.mas_equalTo(self).mas_offset(30+FWCustomeSafeTop);
    }];
    
    if (!tipLabel){
        tipLabel = [[UILabel alloc] init];
    }
    tipLabel.text = [NSString stringWithFormat:@"验证码已发送至 %@",self.phoneNumber];
    tipLabel.textColor= FWTextColor_12101D;
    tipLabel.numberOfLines = 0;
    tipLabel.textAlignment=NSTextAlignmentLeft;
    tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 23];
    [self addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).with.offset(64+FWCustomeSafeTop+50);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 46));
        make.left.mas_equalTo(self).mas_offset(40);
    }];
    
    if (!subLabel) {
        subLabel = [[UILabel alloc] init];
    }
    subLabel.text = @"请输入验证码";
    subLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    subLabel.textColor = FWTextColor_12101D;
    subLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:subLabel];
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tipLabel.mas_bottom).with.offset(6);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 21));
        make.left.mas_equalTo(tipLabel);
    }];
}

- (void)createCodeView{
    
    CGFloat view_weith = (SCREEN_WIDTH-80-30)/MaxLength;

    customView = [[UIView alloc] init];
    customView.frame = CGRectMake(40, 197+SafeTop, SCREEN_WIDTH-80, view_weith);
    [self addSubview:customView];
    
    UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(beginEdit)];
    [customView addGestureRecognizer:tapGesturRecognizer];
    
    
//    codeTextView = [[UITextField alloc] init];
    codeTextView = [[IQTextView alloc] init];
    self.frame = CGRectMake(0, 0, CGRectGetWidth(customView.frame), CGRectGetHeight(customView.frame));
    codeTextView.delegate = self;
    codeTextView.keyboardType = UIKeyboardTypeNumberPad;
    [customView addSubview:codeTextView];
    [customView addDoneOnKeyboardWithTarget:self action:@selector(nextButtonOnClick)];
    if (@available(iOS 12.0, *)) {
        //Xcode 10 适配
        self.codeTextView.textContentType = UITextContentTypeOneTimeCode;
    }
    
    containerView = [[UIView alloc] init];
    containerView.frame = CGRectMake(0, 0,CGRectGetWidth(customView.frame), CGRectGetHeight(customView.frame));
    [customView addSubview:containerView];
    
    for (int i=0; i<MaxLength; i++){
        
        UIView * view = [[UIView alloc] init];
        view.frame = CGRectMake(i*(view_weith+10), 0, view_weith, view_weith);
        [containerView addSubview:view];
        
        UILabel * label = [[UILabel alloc] init];
        label.textColor = FWTextColor_12101D;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"PingFangSC-Medium" size:18];
        label.frame = CGRectMake(0, 0, CGRectGetWidth(view.frame), CGRectGetHeight(view.frame));
        [view addSubview:label];
        
        UIView * lineView = [[UIView alloc] init];
        lineView.frame = CGRectMake(0, CGRectGetMaxY(label.frame), CGRectGetWidth(label.frame), 0.5);
        lineView.backgroundColor = FWTextColor_12101D;
        [view addSubview:lineView];
    }
}

- (void)cteateOtherView{
    
    nextButton = [[UIButton alloc] init];
    [nextButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
    [nextButton setTitle:@"" forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    [nextButton addTarget:self action:@selector(nextButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:nextButton];
    [nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(customView.mas_bottom).with.offset(15);
        make.left.mas_equalTo(self).with.offset(40);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-80,44));
    }];
}

#pragma mark - > 下一步
- (void)nextButtonOnClick{
    
    [self endEditing:YES];
    
    if ([self.delegate respondsToSelector:@selector(nextButtonClick)]){
        [self.delegate nextButtonClick];
    }
}

#pragma mark - > 返回
- (void)backButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(backButtonClick)]){
        [self.delegate backButtonClick];
    }
}

//检测输入框是否有输入
- (void)checkTFTextIsEmpty{
    
    BOOL tempBool = [[HYConditionManager manager] hy_checkCondition:^(HYConditionChecker *condition) {
        condition.combain(codeTextView.text.length <6).is(NO);
    }];
    nextButton.enabled = tempBool?YES:NO;
}

-(void)beginEdit{
    [codeTextView becomeFirstResponder];
}

-(void)endEdit{
    [codeTextView resignFirstResponder];
}

//
//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//
//    [self checkTFTextIsEmpty];
//
//    NSString *str = textField.text;
//    if (str.length > MaxLength){
//
//        //大于的截取，反向赋值给textView，这样删除，才能立即删除.
//        str = [str substringToIndex:MaxLength];
//        textField.text = str;
//    }else{
//
//        //不够的就补空格。
//        long needSpaceCount = MaxLength - str.length;
//        if (needSpaceCount > 0){
//
//            NSMutableString *spaceStr = [NSMutableString string];
//            for (int i=0; i < needSpaceCount; i++){
//
//                [spaceStr appendString:@" "];
//            }
//            str = [NSString stringWithFormat:@"%@%@",str,spaceStr];
//        }
//    }
//
//
//    for (int i= 0; i < MaxLength; i++){
//
//        //每一次输入都对所有的label重新赋值，先取对应的字符，找到对应的label，给他。
//        NSString *temp = [str substringWithRange:NSMakeRange(i, 1)];
//        if (containerView.subviews.count > i){
//
//            UIView *view = [containerView.subviews objectAtIndex:i];
//            if (view.subviews.count > 0){
//
//                UILabel *label = [view.subviews objectAtIndex:0];
//                label.text = temp;
//
//                if (i == MaxLength - 1){
//
//                    if ([self.delegate respondsToSelector:@selector(codeInputView:finished:)]) {
//                        [self.delegate codeInputView:self finished:![temp isEqualToString:@" "]];
//                    }
//                }
//            }
//        }
//    }
//    return YES;
//}

- (void)textViewDidChange:(UITextView *)textView{
    
    [self checkTFTextIsEmpty];
    
    NSString *str = textView.text;
    if (str.length > MaxLength){
        
        //大于的截取，反向赋值给textView，这样删除，才能立即删除.
        str = [str substringToIndex:MaxLength];
        textView.text = str;
    }else{
        
        //不够的就补空格。
        long needSpaceCount = MaxLength - str.length;
        if (needSpaceCount > 0){
            
            NSMutableString *spaceStr = [NSMutableString string];
            for (int i=0; i < needSpaceCount; i++){
                
                [spaceStr appendString:@" "];
            }
            str = [NSString stringWithFormat:@"%@%@",str,spaceStr];
        }
    }
    
    
    for (int i= 0; i < MaxLength; i++){

        //每一次输入都对所有的label重新赋值，先取对应的字符，找到对应的label，给他。
        NSString *temp = [str substringWithRange:NSMakeRange(i, 1)];
        if (containerView.subviews.count > i){

            UIView *view = [containerView.subviews objectAtIndex:i];
            if (view.subviews.count > 0){

                UILabel *label = [view.subviews objectAtIndex:0];
                label.text = temp;

                if (i == MaxLength - 1){

                    if ([self.delegate respondsToSelector:@selector(codeInputView:finished:)]) {
                        [self.delegate codeInputView:self finished:![temp isEqualToString:@" "]];
                    }
                }
            }
        }
    }
}

-(NSString *)getCode{
    
    return codeTextView.text;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self endEditing:YES];
}


#pragma mark - > UITextFieldDelegate
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    return YES;
}

@end
