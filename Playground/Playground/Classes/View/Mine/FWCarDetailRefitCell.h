//
//  FWCarDetailRefitCell.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWCarListModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface FWCarDetailRefitDetailCell : UITableViewCell

@property (nonatomic, strong) UILabel * detailLabel;

@end

@interface FWCarDetailRefitCell : UITableViewCell

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton * editButton;
@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, strong) FWCarListModel * listModel;

@property (nonatomic, weak) UIViewController * vc;

@end

NS_ASSUME_NONNULL_END
