//
//  FWManageCarCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWManageCarCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView * bgImageView;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIImageView * selectImageView;
@property (nonatomic, weak) UIViewController * vc;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
