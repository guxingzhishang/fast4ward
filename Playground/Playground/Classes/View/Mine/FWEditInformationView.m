//
//  FWEditInformationView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWEditInformationView.h"

static NSInteger photoWidth = 100;

@implementation FWEditInformationView
@synthesize photoImageView;
@synthesize nameTextField;
@synthesize signTextField;
@synthesize maleButton;
@synthesize femaleButton;
@synthesize editButton;
@synthesize maleLabel;
@synthesize maleImageView;
@synthesize femaleLabel;
@synthesize femaleImageView;
@synthesize sexType;

- (id)init{
    self = [super init];
    if (self) {
        sexType = @"1";
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.userInteractionEnabled = YES;
    photoImageView.layer.cornerRadius = photoWidth/2;
    photoImageView.layer.masksToBounds = YES;
    [self addSubview:photoImageView];
    [photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).mas_offset(33);
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(photoWidth, photoWidth));
    }];
    [photoImageView sd_setImageWithURL:[NSURL URLWithString:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder_photo"] options:SDWebImageRefreshCached];

    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhoto)];
    [photoImageView addGestureRecognizer:gesture];
    
    editButton = [[UIButton alloc] init];
    [editButton setImage:[UIImage imageNamed:@"mine_edit"] forState:UIControlStateNormal];;
    [editButton addTarget:self action:@selector(uploadPhoto) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:editButton];
    [editButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.mas_equalTo(photoImageView).mas_offset(-photoWidth/35);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    UILabel * nameLabel = [[UILabel alloc] init];
    nameLabel.text = @"昵称";
    nameLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12.1];
    nameLabel.textColor = FWTextColor_000000;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:nameLabel];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self).mas_equalTo(30);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 20));
        make.top.mas_equalTo(photoImageView.mas_bottom).mas_offset(32);
    }];

    nameTextField = [[UITextField alloc] init];
    nameTextField.delegate = self;
    nameTextField.placeholder = @"请输入昵称";
    nameTextField.text = [GFStaticData getObjectForKey:kTagUserKeyName];
    nameTextField.textColor = FWTextColor_12101D;
    nameTextField.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:18];
    [self addSubview:nameTextField];
    [nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(nameLabel.mas_bottom).mas_equalTo(5);
        make.left.mas_equalTo(nameLabel);
        make.right.mas_equalTo(self).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-150, 30));
    }];
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWTextColor_12101D;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(nameTextField);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(nameTextField.mas_bottom);
    }];
    
    UILabel * signLabel = [[UILabel alloc] init];
    signLabel.text = @"签名";
    signLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12.1];
    signLabel.textColor = FWTextColor_12101D;
    signLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:signLabel];
    [signLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel);
        make.size.mas_equalTo(nameLabel);
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(25);
    }];

    signTextField = [[UITextField alloc] init];
    signTextField.delegate = self;
    signTextField.placeholder = @"本宝宝暂时还没想到个性的签名";
    signTextField.text = [GFStaticData getObjectForKey:kTagUserKeySign];
    signTextField.textColor = FWTextColor_12101D;
    signTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    signTextField.font = [UIFont fontWithName:@"PingFangSC-Regular" size:18];;
    [self addSubview:signTextField];
    [signTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameTextField);
        make.right.mas_equalTo(nameTextField);
        make.size.mas_equalTo(nameTextField);
        make.top.mas_equalTo(signLabel.mas_bottom).mas_offset(5);
    }];
    
    UIView * signView = [[UIView alloc] init];
    signView.backgroundColor = FWTextColor_12101D;
    [self addSubview:signView];
    [signView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(signTextField);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(signTextField.mas_bottom);
    }];
    
    UILabel * sexLabel = [[UILabel alloc] init];
    sexLabel.text = @"性别";
    sexLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12.1];
    sexLabel.textColor = FWTextColor_12101D;
    sexLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:sexLabel];
    [sexLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(signView.mas_bottom).mas_offset(25);
        make.size.mas_equalTo(CGSizeMake(150, 20));
        make.left.mas_equalTo(signLabel);
    }];
    
    maleButton = [[UIButton alloc] init];
    [maleButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateHighlighted];
    [maleButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateSelected];
    [maleButton setBackgroundImage:[FWClearColor image] forState:UIControlStateNormal];
    
    [maleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateSelected];
    [maleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateHighlighted];
    [maleButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    
    maleButton.layer.cornerRadius = 2;
    maleButton.layer.masksToBounds = YES;
    [maleButton setTitle:@"男" forState:UIControlStateNormal];
    maleButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    [maleButton addTarget:self action:@selector(maleButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:maleButton];
    [maleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(sexLabel.mas_bottom).mas_offset(11);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.left.mas_equalTo(nameTextField);
    }];
    
    
    femaleButton = [[UIButton alloc] init];
    [femaleButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateHighlighted];
    [femaleButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateSelected];
    [femaleButton setBackgroundImage:[FWClearColor image] forState:UIControlStateNormal];
    
    [femaleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateSelected];
    [femaleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateHighlighted];
    [femaleButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    femaleButton.layer.borderColor = FWTextColor_222222.CGColor;
    femaleButton.layer.borderWidth = 1;
    femaleButton.layer.cornerRadius = 2;
    femaleButton.layer.masksToBounds = YES;
    [femaleButton setTitle:@"女" forState:UIControlStateNormal];
    femaleButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    [femaleButton addTarget:self action:@selector(femaleButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:femaleButton];
    [femaleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(maleButton);
        make.size.mas_equalTo(maleButton);
        make.left.mas_equalTo(maleButton.mas_right).mas_offset(29);
    }];
    
    //如果是女性，则切换到女性头像
    if ([[GFStaticData getObjectForKey:kTagUserKeySex] isEqualToString:@"2"]) {
        [self femaleButtonOnClick];
    }else{
        [self maleButtonOnClick];
    }
}

#pragma mark - > 更换头像
- (void)uploadPhoto{

    if ([self.delegate respondsToSelector:@selector(uploadPhotoClick)]) {
        [self.delegate uploadPhotoClick];
    }
}

#pragma mark - > 选择男性
- (void)maleButtonOnClick{
    
    sexType = @"1";
    [maleButton setSelected:YES];
    [femaleButton setSelected:NO];
    
    maleButton.layer.borderColor = FWClearColor.CGColor;
    maleButton.layer.borderWidth = 0;
    
    femaleButton.layer.borderColor = FWTextColor_222222.CGColor;
    femaleButton.layer.borderWidth = 1;
}

#pragma mark - > 选择女性
- (void)femaleButtonOnClick{
    
    sexType = @"2";
    [maleButton setSelected:NO];
    [femaleButton setSelected:YES];
    
    maleButton.layer.borderColor = FWTextColor_222222.CGColor;
    maleButton.layer.borderWidth = 1;
    
    femaleButton.layer.borderColor = FWClearColor.CGColor;
    femaleButton.layer.borderWidth = 0;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
   
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    if([string isEqualToString:@"<"] ||
       [string isEqualToString:@">"] ||
       [string isEqualToString:@"/"] ||
       [string isEqualToString:@"“"] ||
       [string isEqualToString:@"•"] ||
       [string isEqualToString:@"’"] ||
       [string isEqualToString:@"…"] ||
       [string isEqualToString:@"……"] ){
        return NO;
    }
    
    if ([string isEqualToString:@""]) {
        return YES;
    }

    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
    UITextField * TFView;
    NSInteger limitNumber = 0;
    
    if (textField == nameTextField) {
        TFView = textField;
        limitNumber = 15;
    }else if (textField == signTextField){
        TFView = textField;
        limitNumber = 30;
    }
    
    if (str.length > limitNumber)
    {
        return NO;
    }
    
    
    NSString *toBeString = TFView.text;
    
    UITextInputMode *currentInputMode = TFView.textInputMode;
    NSString *lang = [currentInputMode primaryLanguage];
    
    if ([lang isEqualToString:@"zh-Hans"]) {
        // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [TFView markedTextRange];
        //获取高亮部分
        UITextPosition *position = [TFView positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position)
        {
            if (toBeString.length > limitNumber)
            {
                [TFView resignFirstResponder];
                TFView.text = [toBeString substringToIndex:limitNumber];
            }
        }
        else
        {
            // 有高亮选择的字符串，则暂不对文字进行统计和限制
        }
    }
    else
    {
        // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
        if (toBeString.length > limitNumber)
        {
            [TFView resignFirstResponder];
            TFView.text = [toBeString substringToIndex:limitNumber];
        }
    }
    
    return YES;
}

@end
