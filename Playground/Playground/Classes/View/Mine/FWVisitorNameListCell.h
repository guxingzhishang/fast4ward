//
//  FWVisitorNameListCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWListCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWVisitorNameListCell : FWListCell

@property (nonatomic, weak) UIViewController * vc;

@end

NS_ASSUME_NONNULL_END
