//
//  FWVisitorAnalysisCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVisitorAnalysisCell.h"

@implementation FWVisitorAnalysisCell
@synthesize nameLabel;
@synthesize numberLabel;
@synthesize arrowImageView;



- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.layer.cornerRadius = 2;
        self.contentView.layer.masksToBounds = YES;
        self.contentView.layer.borderColor = FWViewBackgroundColor_E5E5E5.CGColor;
        self.contentView.layer.borderWidth = 0.5;
        
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    nameLabel.textColor = FWTextColor_12101D;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(17);
        make.centerY.mas_equalTo(self.contentView);
        make.height.mas_equalTo(20);
        make.width.mas_lessThanOrEqualTo(((SCREEN_WIDTH-60/2)/2)+5);
    }];
    
    arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"visitor_arrow"];
    [self.contentView addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(8, 13));
        make.right.mas_equalTo(self.contentView).mas_offset(-10);
    }];

    numberLabel = [[UILabel alloc] init];
    numberLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    numberLabel.textColor = FWTextColor_969696;
    numberLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:numberLabel];
    [numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(self.contentView);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(nameLabel.mas_right);
    }];
}

- (void)configForAnalysisCell:(id)model{
    
    FWVisitorAnalysisListModel * listModel = (FWVisitorAnalysisListModel*)model;

    nameLabel.text = listModel.province?listModel.province:listModel.car_style;
    numberLabel.text = listModel.count;
}


@end
