//
//  FWEditInformationView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWEditInformationViewDelegate <NSObject>

- (void)uploadPhotoClick;

@end

@interface FWEditInformationView : UIView<UITextFieldDelegate>

/**
 * 头像
 */
@property (nonatomic, strong) UIImageView * photoImageView;

/**
 * 更换头像
 */
@property (nonatomic, strong) UIButton * editButton;

/**
 * 昵称
 */
@property (nonatomic, strong) UITextField * nameTextField;

/**
 * 签名
 */
@property (nonatomic, strong) UITextField * signTextField;

/**
 * 男性
 */
@property (nonatomic, strong) UIButton * maleButton;

/**
 * 男性图标
 */
@property (nonatomic, strong) UIImageView * maleImageView;

/**
 * 男性文案
 */
@property (nonatomic, strong) UILabel * maleLabel;

/**
 * 女性
 */
@property (nonatomic, strong) UIButton * femaleButton;


/**
 * 女性图标
 */
@property (nonatomic, strong) UIImageView * femaleImageView;

/**
 * 女性文案
 */
@property (nonatomic, strong) UILabel * femaleLabel;

/**
 * 1为男    2为女
 */
@property (nonatomic, strong) NSString * sexType;

@property (nonatomic, weak) UIViewController * vc;


@property (nonatomic, weak) id<FWEditInformationViewDelegate> delegate;

@end
