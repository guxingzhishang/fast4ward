//
//  FWMineCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWListCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMineCell : FWListCell

-(void)cellConfigureFor:(id)model;

@end

NS_ASSUME_NONNULL_END
