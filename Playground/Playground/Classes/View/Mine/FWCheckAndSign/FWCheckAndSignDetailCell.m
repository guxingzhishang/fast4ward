//
//  FWCheckAndSignDetailCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCheckAndSignDetailCell.h"
#import "FWBaomingModel.h"

@implementation FWCheckAndSignDetailCell

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
    
    self.bgView = [[UIView alloc] init];
    self.bgView.clipsToBounds = YES;
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size:22];
    self.numberLabel.textColor = FWTextColor_272727;
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    [self.bgView addSubview:self.numberLabel];
    [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.width.mas_greaterThanOrEqualTo(10);
       make.height.mas_equalTo(20);
       make.top.left.mas_equalTo(self.bgView).mas_offset(10);
    }];
    
    
    self.checkImageView = [[UIImageView alloc] init];
    self.checkImageView.image = [UIImage imageNamed:@"visitor_arrow"];
    [self.bgView addSubview:self.checkImageView];
    [self.checkImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.numberLabel);
        make.size.mas_equalTo(CGSizeMake(15, 16));
        make.right.mas_equalTo(self.bgView).mas_offset(-10);
    }];
    
    self.signImageView = [[UIImageView alloc] init];
    self.signImageView.image = [UIImage imageNamed:@"visitor_arrow"];
    [self.bgView addSubview:self.signImageView];
    [self.signImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.checkImageView);
        make.size.mas_equalTo(CGSizeMake(17, 16));
        make.right.mas_equalTo(self.checkImageView.mas_left).mas_offset(-10);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = FWColor(@"E9E9E9");
    [self.bgView addSubview:self.lineView];
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel);
        make.top.mas_equalTo(self.numberLabel.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(self.bgView).mas_offset(-20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(0.5);
    }];

    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:17];
    self.nameLabel.textColor = FWTextColor_12101D;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel);
        make.top.mas_equalTo(self.lineView.mas_bottom).mas_offset(9);
        make.height.mas_equalTo(22);
        make.width.mas_greaterThanOrEqualTo(10);
    }];

    self.removeLabel= [[UILabel alloc] init];
    self.removeLabel.userInteractionEnabled = YES;
    self.removeLabel.frame = CGRectMake(0, (SCREEN_WIDTH-38)/2 *101/169 - 25, (SCREEN_WIDTH-38)/2, 25);
    [self.bgView addSubview:self.removeLabel];
    [self.removeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeLabelClick:)]];

    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.removeLabel.bounds;
    [self.removeLabel.layer addSublayer:gradientLayer];
    gradientLayer.colors = @[(__bridge id)FWColor(@"6B95EF").CGColor,
                             (__bridge id)FWColor(@"3E68DC").CGColor];
    
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    
    
    UILabel * tempLabel = [[UILabel alloc] init];
    tempLabel.text = @"移除";
    tempLabel.userInteractionEnabled = NO;
    tempLabel.textColor =FWViewBackgroundColor_FFFFFF;
    tempLabel.textAlignment = NSTextAlignmentCenter;
    tempLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.removeLabel addSubview:tempLabel];
    [tempLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.removeLabel);
    }];


}

#pragma mark - >  移除
- (void)removeLabelClick:(UITapGestureRecognizer *)tap{
 
    NSInteger index = tap.view.tag - 1090;
    
    if ([self.delegate respondsToSelector:@selector(removeItem:)]) {
        [self.delegate removeItem:index];
    }
}


- (void)configForAnalysisCell:(NSMutableDictionary *)dict{

    self.nameLabel.text = dict[@"real_name"];
    self.numberLabel.text = dict[@"car_no"];
    if ([dict[@"status_qiandao"] isEqualToString:@"2"]) {
        /* 已签到 */
        self.signImageView.image = [UIImage imageNamed:@"status_sign"];
    }else{
        self.signImageView.image = [UIImage imageNamed:@"status_unsign"];
    }

    if ([dict[@"status_jianlu"] isEqualToString:@"2"]) {
        /* 已检录 */
        self.checkImageView.image = [UIImage imageNamed:@"status_check"];
    }else{
        self.checkImageView.image = [UIImage imageNamed:@"status_uncheck"];
    }
}

@end
