//
//  FWDriverListCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWDriverListCell.h"

@implementation FWDriverListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(5);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-5);
        make.left.mas_equalTo(self.contentView).mas_offset(32);
        make.right.mas_equalTo(self.contentView).mas_offset(-32);
        make.height.mas_equalTo(192);
        make.width.mas_equalTo(SCREEN_WIDTH-64);
    }];
    
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.carnoLabel = [[UILabel alloc] init];
    self.carnoLabel.text = @"车      号";
    self.carnoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.carnoLabel.textColor = FWTextColor_B6BCC4;
    self.carnoLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.carnoLabel];
    [self.carnoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(17);
        make.top.mas_equalTo(self.bgView).mas_offset(17);
        make.height.mas_equalTo(17);
        make.width.mas_equalTo(60);
    }];
    
    self.carnoValueLabel = [[UILabel alloc] init];
    self.carnoValueLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.carnoValueLabel.textColor = FWTextColor_222222;
    self.carnoValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.carnoValueLabel];
    [self.carnoValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carnoLabel.mas_right).mas_offset(7);
        make.centerY.mas_equalTo(self.carnoLabel);
        make.right.mas_equalTo(self.bgView).mas_offset(-15);
        make.height.mas_equalTo(self.carnoLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.text = @"真实姓名";
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.nameLabel.textColor = FWTextColor_B6BCC4;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carnoLabel);
        make.top.mas_equalTo(self.carnoLabel.mas_bottom).mas_offset(17);
        make.height.mas_equalTo(self.carnoLabel);
        make.width.mas_equalTo(self.carnoLabel);
    }];
    
    self.nameValueLabel = [[UILabel alloc] init];
    self.nameValueLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nameValueLabel.textColor = FWTextColor_222222;
    self.nameValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameValueLabel];
    [self.nameValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(7);
        make.centerY.mas_equalTo(self.nameLabel);
        make.right.mas_equalTo(self.bgView).mas_offset(-15);
        make.height.mas_equalTo(self.nameLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.idcardLabel = [[UILabel alloc] init];
    self.idcardLabel.text = @"证件号码";
    self.idcardLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.idcardLabel.textColor = FWTextColor_B6BCC4;
    self.idcardLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.idcardLabel];
    [self.idcardLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(17);
        make.height.mas_equalTo(self.nameLabel);
        make.width.mas_equalTo(self.nameLabel);
    }];
    
    self.idcardValueLabel = [[UILabel alloc] init];
    self.idcardValueLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.idcardValueLabel.textColor = FWTextColor_222222;
    self.idcardValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.idcardValueLabel];
    [self.idcardValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.idcardLabel.mas_right).mas_offset(7);
        make.centerY.mas_equalTo(self.idcardLabel);
        make.right.mas_equalTo(self.bgView).mas_offset(-15);
        make.height.mas_equalTo(self.idcardLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.signLabel = [[UILabel alloc] init];
    self.signLabel.text = @"签       到";
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.signLabel.textColor = FWTextColor_B6BCC4;
    self.signLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.signLabel];
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(self.idcardLabel);
       make.top.mas_equalTo(self.idcardLabel.mas_bottom).mas_offset(17);
       make.height.mas_equalTo(self.idcardLabel);
       make.width.mas_equalTo(self.idcardLabel);
    }];
    
    self.signValueLabel = [[UILabel alloc] init];
    self.signValueLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.signValueLabel.textColor = FWTextColor_222222;
    self.signValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.signValueLabel];
    [self.signValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.idcardLabel.mas_right).mas_offset(7);
        make.centerY.mas_equalTo(self.signLabel);
        make.height.mas_equalTo(self.signLabel);
        make.width.mas_equalTo(45);
    }];
    
    self.signImageView = [[UIImageView alloc] init];
    self.signImageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:self.signImageView];
    [self.signImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.signValueLabel.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.signValueLabel);
        make.size.mas_equalTo(CGSizeMake(16.5, 16));
    }];
    
    
    self.checkLabel = [[UILabel alloc] init];
    self.checkLabel.text = @"检      录";
    self.checkLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.checkLabel.textColor = FWTextColor_B6BCC4;
    self.checkLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.checkLabel];
    [self.checkLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.signLabel);
        make.top.mas_equalTo(self.signLabel.mas_bottom).mas_offset(17);
        make.height.mas_equalTo(self.signLabel);
        make.width.mas_equalTo(self.signLabel);
    }];
    
    self.checkValueLabel = [[UILabel alloc] init];
    self.checkValueLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.checkValueLabel.textColor = FWTextColor_222222;
    self.checkValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.checkValueLabel];
    [self.checkValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.checkLabel.mas_right).mas_offset(7);
        make.centerY.mas_equalTo(self.checkLabel);
        make.height.mas_equalTo(self.checkLabel);
        make.width.mas_equalTo(45);
    }];
    
    self.checkImageView = [[UIImageView alloc] init];
    self.checkImageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:self.checkImageView];
    [self.checkImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.checkValueLabel.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.checkValueLabel);
        make.size.mas_equalTo(CGSizeMake(15, 16));
    }];
}

- (void)configForCell:(FWJianluInfoModel *)model{
    
    FWJianluInfoModel * baomingModel = (FWJianluInfoModel *)model;
    
    self.carnoValueLabel.text = baomingModel.car_no;
    self.nameValueLabel.text = baomingModel.real_name;
    self.idcardValueLabel.text = baomingModel.idcard;
    
    if ([baomingModel.status_jianlu isEqualToString:@"2"]) {
        self.checkValueLabel.text = @"已检录";
        self.checkImageView.image = [UIImage imageNamed:@"status_check"];
    }else{
        self.checkValueLabel.text = @"未检录";
        self.checkImageView.image = [UIImage imageNamed:@"status_uncheck"];
    }
    
    if ([baomingModel.status_qiandao isEqualToString:@"2"]) {
       self.signValueLabel.text = @"已签到";
       self.signImageView.image = [UIImage imageNamed:@"status_sign"];
    }else{
       self.signValueLabel.text = @"未签到";
       self.signImageView.image = [UIImage imageNamed:@"status_unsign"];
    }
}


@end
