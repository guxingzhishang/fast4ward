//
//  FWSearchPlayerCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWSearchPlayerCell.h"
#import "FWBaomingModel.h"
@implementation FWSearchPlayerCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.contentView.backgroundColor  = FWViewBackgroundColor_FFFFFF;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = FWColor(@"e9e9e9");
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.height.mas_equalTo(1);
        make.width.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
    }];

    self.rightArrow = [[UIImageView alloc] init];
    self.rightArrow.image = [UIImage imageNamed:@"right_arrow"];
    [self.contentView addSubview:self.rightArrow];
    [self.rightArrow mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.nameLabel.textColor = FWTextColor_222222;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.lineView).mas_offset(0);
        make.centerY.mas_equalTo(self.contentView);
        make.height.mas_equalTo(44);
        make.width.mas_greaterThanOrEqualTo(40);
    }];
    
//    self.carnoLabel = [[UILabel alloc] init];
//    self.carnoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
//    self.carnoLabel.textColor = FWTextColor_222222;
//    self.carnoLabel.textAlignment = NSTextAlignmentLeft;
//    [self.contentView addSubview:self.carnoLabel];
//    [self.carnoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(20);
//        make.centerY.mas_equalTo(self.contentView);
//        make.height.mas_equalTo(44);
//        make.width.mas_equalTo(40);
//    }];
//
//    self.phoneLabel = [[UILabel alloc] init];
//    self.phoneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
//    self.phoneLabel.textColor = FWTextColor_222222;
//    self.phoneLabel.textAlignment = NSTextAlignmentLeft;
//    [self.contentView addSubview:self.phoneLabel];
//    [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.carnoLabel.mas_right).mas_offset(20);
//        make.centerY.mas_equalTo(self.contentView);
//        make.height.mas_equalTo(44);
//        make.width.mas_equalTo(110);
//    }];
}

- (void)configForCell:(id)model{
    
    FWBaomingListModel * listModel = (FWBaomingListModel*)model;
    
    self.nameLabel.text = listModel.name;
//    self.carnoLabel.text = @"006";
//    self.phoneLabel.text = @"15646570666";
}

@end
