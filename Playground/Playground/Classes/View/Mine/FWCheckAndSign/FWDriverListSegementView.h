//
//  FWDriverListSegementView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWDriverListSegementViewDelegate <NSObject>

/**
 * 点击了第几个item
 */
- (void)segementItemClickTap:(NSInteger)index;

@end

@interface FWDriverListSegementView : UIScrollView<UIScrollViewDelegate>

/**
 顶部菜单数据
 */
@property (nonatomic,strong)NSArray * titles;

/**
 一个页面显示几个按钮
 */
@property (nonatomic,assign)NSInteger pageNumberOfItem;

@property (nonatomic,strong)NSMutableArray *labelArys;
@property (nonatomic,strong)NSMutableArray *rectXArray;

@property (nonatomic,strong)UIView *shadowView;

@property (nonatomic,assign)NSInteger lastIndex;

@property (nonatomic,assign)NSInteger currentItem;
@property (nonatomic,assign)CGFloat menuHeight;

@property (nonatomic,strong)UIFont *titleFont;
@property (nonatomic,strong)UIFont *titleSelectFont;

@property (nonatomic,strong)UIColor *titleColor;
@property (nonatomic,strong)UIColor *titleSelectColor;

@property (nonatomic, weak) id<FWDriverListSegementViewDelegate> itemDelegate;

- (void)deliverTitles:(NSArray *)array;
- (void)clearSubViews;
- (void)menuUpdateStyle:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
