//
//  FWDriverInfoView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWBaomingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWDriverInfoView : UIScrollView<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIView * mainView;
@property (nonatomic, strong) UIView * firtShadowView;
@property (nonatomic, strong) UIView * firstBgView;

@property (nonatomic, strong) UIView * secondShadowView;
@property (nonatomic, strong) UIView * secondBgView;
@property (nonatomic, strong) UIImageView * secondIconView;
@property (nonatomic, strong) UILabel * secondNameLabel;
@property (nonatomic, strong) UIImageView * secondButtonImageView;

@property (nonatomic, strong) UIView * thirdShadowView;
@property (nonatomic, strong) UIView * thirdBgView;
@property (nonatomic, strong) UIImageView * thirdIconView;
@property (nonatomic, strong) UILabel * thirdNameLabel;
@property (nonatomic, strong) UIImageView * thirdButtonImageView;

@property (nonatomic, strong) UIView * fourthShadowView;
@property (nonatomic, strong) UIImageView * fourthButtonImageView;

@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * uploadLabel;
@property (nonatomic, strong) UIButton * guaqiButton;
@property (nonatomic, strong) UIButton * commitButton;
@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) NSString * mianze_img;
@property (nonatomic, strong) NSString * temp_mianze_img;// 临时存url，一旦请求成功后，将其赋值给mianze_img

@property (nonatomic, strong) FWJianluInfoModel * jianluInfoModel;

/**
 * 存储获取的上传资源的文件名（bucket，object-key）
 */
@property (nonatomic, strong) NSDictionary * objectDict;


/// 赋值
/// @param model 值
/// @param type 1:请求接口   2：读取c缓存
- (void)configForView:(id)model withType:(NSString *)type;

@end

NS_ASSUME_NONNULL_END
