//
//  FWSearchPlayerCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWSearchPlayerCell : UITableViewCell

@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * carnoLabel;
@property (nonatomic, strong) UILabel * phoneLabel;

@property (nonatomic, strong) UIImageView * rightArrow;
@property (nonatomic, strong) UIView * lineView;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
