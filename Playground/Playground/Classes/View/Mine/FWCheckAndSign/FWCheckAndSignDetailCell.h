//
//  FWCheckAndSignDetailCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWCheckAndSignDetailCellDelegate <NSObject>

- (void)removeItem:(NSInteger)index;

@end

@interface FWCheckAndSignDetailCell : UICollectionViewCell
@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UILabel * numberLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIImageView * signImageView;
@property (nonatomic, strong) UIImageView * checkImageView;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UILabel * removeLabel;

@property (nonatomic, weak) id <FWCheckAndSignDetailCellDelegate>delegate;


- (void)configForAnalysisCell:(NSMutableDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
