//
//  FWDriverListCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWBaomingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWDriverListCell : UITableViewCell

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UILabel * carnoLabel;
@property (nonatomic, strong) UILabel * carnoValueLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * nameValueLabel;
@property (nonatomic, strong) UILabel * idcardLabel;
@property (nonatomic, strong) UILabel * idcardValueLabel;
@property (nonatomic, strong) UILabel * signLabel;
@property (nonatomic, strong) UILabel * signValueLabel;
@property (nonatomic, strong) UIImageView * signImageView;
@property (nonatomic, strong) UILabel * checkLabel;
@property (nonatomic, strong) UILabel * checkValueLabel;
@property (nonatomic, strong) UIImageView * checkImageView;

- (void)configForCell:(FWJianluInfoModel *)model;

@end

NS_ASSUME_NONNULL_END
