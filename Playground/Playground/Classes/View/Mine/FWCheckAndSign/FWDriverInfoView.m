//
//  FWDriverInfoView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWDriverInfoView.h"
#import "FWDriverInfoManager.h"
#import "FWCheckAndSignDetailViewController.h"
#import "FWLookDriverListViewController.h"

@implementation FWDriverInfoView

#pragma mark - > 请求阿里云临时凭证
- (void)requsetStsData{
    FWNetworkRequest * requst = [[FWNetworkRequest alloc] init];
    [requst requestStsTokenWithType:@"1"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestUploadName];
    });
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"mianze_img",
                              @"number":@"1",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.objectDict = [back objectForKey:@"data"];
            [self requestMianzeURL];
        }
    }];
}

- (void)requestMianzeURL{
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
        @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
        @"mianze_img":[self.objectDict objectForKey:@"object_key"],
    };
    
    [request startWithParameters:params WithAction:Get_mianze_img_url WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.temp_mianze_img = [[back objectForKey:@"data"] objectForKey:@"mianze_img_url"];
        }
    }];
}

- (id)init{
    self = [super init];
    if (self) {
        self.temp_mianze_img = @"";
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        [self setupSubviews];
        [self requsetStsData];
    }
    
    return self;
}


- (void)setupSubviews{
    
    self.mainView = [[UIView alloc] init];
    self.mainView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    self.firtShadowView = [[UIView alloc] init];
    self.firtShadowView.userInteractionEnabled = YES;
    self.firtShadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.firtShadowView.userInteractionEnabled = YES;
    self.firtShadowView.layer.cornerRadius = 5;
    self.firtShadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.firtShadowView.layer.shadowOpacity = 0.7;
    self.firtShadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.mainView addSubview:self.firtShadowView];
    [self.firtShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mainView).mas_offset(10);
        make.left.mas_equalTo(self.mainView).mas_offset(33);
        make.right.mas_equalTo(self.mainView).mas_offset(-32);
        make.height.mas_equalTo(275);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];
      
    self.firstBgView = [[UIView alloc] init];
    [self.firtShadowView addSubview:self.firstBgView];
    [self.firstBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.firtShadowView);
    }];
    
    
    /* 发放车手出入证件 */
    self.secondShadowView = [[UIView alloc] init];
    self.secondShadowView.userInteractionEnabled = YES;
    self.secondShadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.secondShadowView.userInteractionEnabled = YES;
    self.secondShadowView.layer.cornerRadius = 5;
    self.secondShadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.secondShadowView.layer.shadowOpacity = 0.7;
    self.secondShadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.mainView addSubview:self.secondShadowView];
    [self.secondShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.firtShadowView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.firtShadowView);
        make.right.mas_equalTo(self.firtShadowView);
        make.height.mas_equalTo(63);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];

    self.secondBgView = [[UIView alloc] init];
    [self.secondShadowView addSubview:self.secondBgView];
    [self.secondBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.secondShadowView);
    }];

    self.secondIconView = [[UIImageView alloc] init];
    self.secondIconView.image = [UIImage imageNamed:@"driver_info_card"];
    [self.secondBgView addSubview:self.secondIconView];
    [self.secondIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.secondBgView);
        make.left.mas_equalTo(self.secondBgView).mas_offset(17);
        make.size.mas_equalTo(CGSizeMake(19, 16));
    }];

    self.secondNameLabel = [[UILabel alloc] init];
    self.secondNameLabel.text = @"发放车手出入证件";
    self.secondNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.secondNameLabel.textColor = FWTextColor_252527;
    self.secondNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondBgView addSubview:self.secondNameLabel];
    [self.secondNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.secondBgView);
        make.left.mas_equalTo(self.secondIconView.mas_right).mas_offset(15);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(20);
    }];

    self.secondButtonImageView = [[UIImageView alloc] init];
    self.secondButtonImageView.userInteractionEnabled = YES;
    [self.secondBgView addSubview:self.secondButtonImageView];
    [self.secondButtonImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.secondBgView);
        make.right.mas_equalTo(self.secondBgView).mas_offset(0);
        make.width.mas_greaterThanOrEqualTo(80);
        make.height.mas_equalTo(62);
    }];
    [self.secondButtonImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(secondButtonImageViewClick)]];


    /* 发放车手礼包 */
     self.thirdShadowView = [[UIView alloc] init];
     self.thirdShadowView.userInteractionEnabled = YES;
     self.thirdShadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
     self.thirdShadowView.userInteractionEnabled = YES;
     self.thirdShadowView.layer.cornerRadius = 5;
     self.thirdShadowView.layer.shadowOffset = CGSizeMake(0, 0);
     self.thirdShadowView.layer.shadowOpacity = 0.7;
     self.thirdShadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
     [self.mainView addSubview:self.thirdShadowView];
     [self.thirdShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.top.mas_equalTo(self.secondShadowView.mas_bottom).mas_offset(10);
         make.left.mas_equalTo(self.secondShadowView);
         make.right.mas_equalTo(self.secondShadowView);
         make.height.mas_equalTo(63);
         make.width.mas_equalTo(SCREEN_WIDTH-65);
     }];

     self.thirdBgView = [[UIView alloc] init];
     [self.thirdShadowView addSubview:self.thirdBgView];
     [self.thirdBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.edges.mas_equalTo(self.thirdShadowView);
     }];

     self.thirdIconView = [[UIImageView alloc] init];
     self.thirdIconView.image = [UIImage imageNamed:@"driver_info_gift"];
     [self.thirdBgView addSubview:self.thirdIconView];
     [self.thirdIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.centerY.mas_equalTo(self.thirdBgView);
         make.left.mas_equalTo(self.thirdBgView).mas_offset(17);
         make.size.mas_equalTo(CGSizeMake(19, 16));
     }];

     self.thirdNameLabel = [[UILabel alloc] init];
     self.thirdNameLabel.text = @"发放车手礼包";
     self.thirdNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
     self.thirdNameLabel.textColor = FWTextColor_252527;
     self.thirdNameLabel.textAlignment = NSTextAlignmentLeft;
     [self.thirdBgView addSubview:self.thirdNameLabel];
     [self.thirdNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.centerY.mas_equalTo(self.thirdBgView);
         make.left.mas_equalTo(self.thirdIconView.mas_right).mas_offset(15);
         make.width.mas_greaterThanOrEqualTo(100);
         make.height.mas_equalTo(20);
     }];

     self.thirdButtonImageView = [[UIImageView alloc] init];
     self.thirdButtonImageView.userInteractionEnabled = YES;
     [self.thirdBgView addSubview:self.thirdButtonImageView];
     [self.thirdButtonImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.centerY.mas_equalTo(self.thirdBgView);
         make.right.mas_equalTo(self.thirdBgView).mas_offset(0);
         make.width.mas_greaterThanOrEqualTo(80);
         make.height.mas_equalTo(62);
     }];
     [self.thirdButtonImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(thirdButtonImageViewClick)]];

     /* 上传免责照片 */
     self.fourthShadowView = [[UIView alloc] init];
     self.fourthShadowView.userInteractionEnabled = YES;
     self.fourthShadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
     self.fourthShadowView.userInteractionEnabled = YES;
     self.fourthShadowView.layer.cornerRadius = 5;
     self.fourthShadowView.layer.shadowOffset = CGSizeMake(0, 0);
     self.fourthShadowView.layer.shadowOpacity = 0.7;
     self.fourthShadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
     [self.mainView addSubview:self.fourthShadowView];
     [self.fourthShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.top.mas_equalTo(self.thirdShadowView.mas_bottom).mas_offset(10);
         make.left.mas_equalTo(self.thirdShadowView);
         make.right.mas_equalTo(self.thirdShadowView);
         make.height.mas_equalTo(170);
         make.width.mas_equalTo(SCREEN_WIDTH-65);
     }];


    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.tag = 22222;
    self.photoImageView.layer.cornerRadius = 4;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.clipsToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@"upload_mianze_placeholder"];
    [self.fourthShadowView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.fourthShadowView);
    }];
    [self.photoImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType)]];

    self.uploadLabel = [[UILabel alloc] init];
    self.uploadLabel.text = @"重新上传";
    self.uploadLabel.layer.cornerRadius = 4;
    self.uploadLabel.layer.masksToBounds = YES;
    self.uploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.uploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.uploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.uploadLabel.textAlignment = NSTextAlignmentCenter;
    self.uploadLabel.userInteractionEnabled = YES;
    [self.photoImageView addSubview:self.uploadLabel];
    [self.uploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(self.photoImageView);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(0);
    }];
    [self.uploadLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadLabelWithType)]];
    self.uploadLabel.hidden = YES;

    self.guaqiButton = [[UIButton alloc] init];
    self.guaqiButton.layer.cornerRadius = 41/2;
    self.guaqiButton.layer.masksToBounds = YES;
    self.guaqiButton.backgroundColor = FWColor(@"#E5E5E5");
    self.guaqiButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.guaqiButton setTitle:@"挂起" forState:UIControlStateNormal];
    [self.guaqiButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.guaqiButton addTarget:self action:@selector(guaqiButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.guaqiButton];
    [self.guaqiButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.photoImageView.mas_bottom).mas_offset(30);
        make.left.mas_equalTo(self.mainView).mas_offset(33);
        make.height.mas_equalTo(41);
        make.width.mas_equalTo((SCREEN_WIDTH-76)/2);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-45);
    }];

    self.commitButton = [[UIButton alloc] init];
    self.commitButton.layer.cornerRadius = 41/2;
    self.commitButton.layer.masksToBounds = YES;
    self.commitButton.backgroundColor = FWColor(@"#3E68DC");
    self.commitButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.commitButton setTitle:@"提交" forState:UIControlStateNormal];
    [self.commitButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.commitButton addTarget:self action:@selector(commitButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.commitButton];
    [self.commitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.guaqiButton);
        make.left.mas_equalTo(self.guaqiButton.mas_right).mas_offset(10);
        make.height.mas_equalTo(self.guaqiButton);
        make.width.mas_equalTo(self.guaqiButton);
    }];
}

#pragma mark - > 赋值
- (void)configForView:(id)model withType:(NSString *)type{
    
    self.jianluInfoModel = (FWJianluInfoModel *)model;
    
    [self setupFirstView];
    
    if ([type isEqualToString:@"1"]) {
        if ([self.jianluInfoModel.status_jianlu isEqualToString:@"2"]) {
            self.jianluInfoModel.isSendGift = YES;
            self.jianluInfoModel.isUploadPic = YES;
        }
        
        if ([self.jianluInfoModel.status_qiandao isEqualToString:@"2"]) {
            self.jianluInfoModel.isSendCer = YES;
            self.secondButtonImageView.userInteractionEnabled = NO;
        }else{
            self.jianluInfoModel.isSendCer = NO;
        }
        
        if (self.jianluInfoModel.mianze_img.length > 0) {
           self.jianluInfoModel.isUploadPic = YES;
        }else{
            self.jianluInfoModel.isUploadPic = NO;
        }
    }
    
    if ([self.jianluInfoModel.status_jianlu isEqualToString:@"2"]) {
        self.thirdButtonImageView.userInteractionEnabled = NO;
    }
    
    if ([self.jianluInfoModel.status_qiandao isEqualToString:@"2"]) {
        self.secondButtonImageView.userInteractionEnabled = NO;
    }
    
    if (self.jianluInfoModel.mianze_img.length > 0) {
        
        if ([self.jianluInfoModel.status_jianlu isEqualToString:@"2"]) {
            self.uploadLabel.hidden = YES;
        }else{
            self.uploadLabel.hidden = NO;
        }
        self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.photoImageView.clipsToBounds = YES;
        [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.jianluInfoModel.mianze_img] placeholderImage:[UIImage imageNamed:@"upload_mianze_placeholder"]];
        self.mianze_img = self.jianluInfoModel.mianze_img;
    }else{
        self.uploadLabel.hidden = YES;
        self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.photoImageView.clipsToBounds = YES;
        self.photoImageView.image = [UIImage imageNamed:@"upload_mianze_placeholder"];
    }
    
    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    if ([usvm.jianlu_check isEqualToString:@"1"] && [usvm.qiandao_check isEqualToString:@"1"]) {
        /* 既有签到权限 又有检录权限  */
        [self haveAllAuth];
    }else if (![usvm.jianlu_check isEqualToString:@"1"] && [usvm.qiandao_check isEqualToString:@"1"]) {
        /* 有签到权限 没有检录权限  */
        [self onlySignAuth];
    }else if ([usvm.jianlu_check isEqualToString:@"1"] && ![usvm.qiandao_check isEqualToString:@"1"]) {
        /* 没签到权限 、有检录权限  */
        [self onlyJianluAuth];
    }else{
        [self noAuth];
    }
    
    
    if ([usvm.jianlu_check isEqualToString:@"1"]) {
        if (self.jianluInfoModel.isSendGift) {
            /* 发放了车手礼包 */
            self.thirdButtonImageView.image = [UIImage imageNamed:@"driver_info_hascender"];
        }else{
            self.thirdButtonImageView.image = [UIImage imageNamed:@"driver_info_sender"];
        }
    }
    
    if ([usvm.qiandao_check isEqualToString:@"1"]) {
        if (self.jianluInfoModel.isSendCer) {
            /* 发放了车手输入证 */
            self.secondButtonImageView.image = [UIImage imageNamed:@"driver_info_hascender"];
        }else{
            self.secondButtonImageView.image = [UIImage imageNamed:@"driver_info_sender"];
        }
    }
}

- (void)setupFirstView{
    
    for (UIView * view in self.firstBgView.subviews) {
        [view removeFromSuperview];
    }
    
    NSMutableArray * leftArray = @[@"车号",@"真实姓名",@"证件号码",@"手机号码",@"车辆品牌",@"车辆型号",@"车架号码"].mutableCopy;
    NSMutableArray * rightArray = @[self.jianluInfoModel.car_no,self.jianluInfoModel.real_name,self.jianluInfoModel.idcard,self.jianluInfoModel.mobile,self.jianluInfoModel.brand,self.jianluInfoModel.car_style,self.jianluInfoModel.carframe_number].mutableCopy;
    
    for (int i = 0; i<leftArray.count; i++) {
        
        UILabel * leftLabel = [[UILabel alloc] init];
        leftLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        leftLabel.textColor = FWTextColor_B6BCC4;
        leftLabel.text = leftArray[i];
        leftLabel.textAlignment = NSTextAlignmentLeft;
        [self.firstBgView addSubview:leftLabel];
        leftLabel.frame = CGRectMake(17, 16+i*35, 55, 35);
        
        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
        rightLabel.textColor = FWTextColor_222222;
        rightLabel.text = rightArray[i];
        rightLabel.numberOfLines = 0;
        rightLabel.textAlignment = NSTextAlignmentLeft;
        [self.firstBgView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.firstBgView).mas_offset(-14);
            make.centerY.mas_equalTo(leftLabel);
            make.left.mas_equalTo(leftLabel.mas_right).mas_offset(5);
            make.width.mas_greaterThanOrEqualTo(10);
            make.height.mas_equalTo(28);
        }];
    }
}

#pragma mark - > 挂起
- (void)guaqiButtonClick{
    
//    NSMutableArray * tempArray = [[FWDriverInfoManager getDriverInfoManagerInfoJSONDataFromLocal] objectForKey:@"driver_list"];
//
//    for (NSMutableDictionary * dict in tempArray) {
//        if ([dict[@"baoming_user_id"] isEqualToString:self.jianluInfoModel.baoming_user_id]) {
//            [[FWHudManager sharedManager] showErrorMessage:@"已挂起，请不要重复挂起" toController:self.vc];
//            return;
//        }
//    }

    [[FWHudManager sharedManager] showErrorMessage:@"挂起成功" toController:self.vc];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSMutableDictionary * dict = [self.jianluInfoModel mj_keyValues];
          
        [FWDriverInfoManager saveDriverInfoManagerInfoJSONDataToLocalWithData:dict];
        
        for (UIViewController * item in self.vc.navigationController.viewControllers) {
          if ([item isKindOfClass:[FWCheckAndSignDetailViewController class]]) {
              [self.vc.navigationController popToViewController:item animated:YES];
              return ;
          }
        }
        
        for (UIViewController * item in self.vc.navigationController.viewControllers) {
            if ([item isKindOfClass:[FWLookDriverListViewController class]]) {
                [self.vc.navigationController popToViewController:item animated:YES];
                return ;
            }
        }
        [self.vc.navigationController popViewControllerAnimated:YES];
    });
}

#pragma mark - > 签到（发放车手出入证）
- (void)secondButtonImageViewClick{
    
    if (self.jianluInfoModel.isSendCer) {
        self.jianluInfoModel.isSendCer = NO;
        self.secondButtonImageView.image = [UIImage imageNamed:@"driver_info_sender"];
    }else{
        self.jianluInfoModel.isSendCer = YES;
        self.secondButtonImageView.image = [UIImage imageNamed:@"driver_info_hascender"];
    }
}

#pragma mark - > 检录之（发放车手礼包）
- (void)thirdButtonImageViewClick{

    if (self.jianluInfoModel.isSendGift) {
        self.jianluInfoModel.isSendGift = NO;
        self.thirdButtonImageView.image = [UIImage imageNamed:@"driver_info_sender"];
    }else{
        self.jianluInfoModel.isSendGift = YES;
        self.thirdButtonImageView.image = [UIImage imageNamed:@"driver_info_hascender"];
    }
}

#pragma mark - > 上传图片
- (void)uploadLabelWithType{
    
    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *_Nonnull action) {
        
    }];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"打开相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        [self.vc presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    
    UIAlertAction *picture = [UIAlertAction actionWithTitle:@"打开相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (@available(iOS 11, *)) {
            UIScrollView.appearance.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
        }
        
        UIImagePickerController *pickerImage = [[UIImagePickerController alloc] init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            pickerImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            pickerImage.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:pickerImage.sourceType];
        }
        pickerImage.delegate = self;
        pickerImage.allowsEditing = NO;
        pickerImage.navigationController.navigationBar.hidden = NO;
        pickerImage.fd_prefersNavigationBarHidden = NO;
        
        [self.vc presentViewController:pickerImage animated:YES completion:nil];
    }];
    [alertVc addAction:cancle];
    [alertVc addAction:camera];
    [alertVc addAction:picture];
    [self.vc presentViewController:alertVc animated:YES completion:nil];
}

#pragma mark - >  取消选取图片
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - > 查看大图
- (void)uploadPhotoClickWithType{
    
    if (self.mianze_img.length > 0) {
        FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
        browser.isFullWidthForLandScape = YES;
        browser.isNeedLandscape = YES;
        browser.currentImageIndex = 0;
        browser.imageArray = @[self.mianze_img].mutableCopy;
        browser.originalImageArray = @[self.mianze_img].copy;
        browser.vc = self.vc;
        browser.fromType = 1;
        [browser show];
    }else{
        [self uploadLabelWithType];
    }
}

#pragma mark - > 上传身份证
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"]){
        
        UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        [ALiOssUploadTool asyncUploadImage:image WithBucketName:[self.objectDict objectForKey:@"bucket"] WithObject_key:[self.objectDict objectForKey:@"object_key"] WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
            /* 回归主线程 */
            dispatch_async(dispatch_get_main_queue(), ^{
                if (UploadImageSuccess == state) {
                    self.jianluInfoModel.isUploadPic = YES;
                    self.jianluInfoModel.mianze_img = [self.objectDict objectForKey:@"object_key"];
                    
                    self.uploadLabel.hidden = NO;
                    self.photoImageView.image = image;
                    self.mianze_img = self.temp_mianze_img;
                    self.jianluInfoModel.mianze_img = self.temp_mianze_img;
                }
            });
        }];
    }
}

#pragma mark - > 提交
- (void)commitButtonClick{
    
    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    if ([usvm.qiandao_check isEqualToString:@"1"] && ![usvm.jianlu_check isEqualToString:@"1"]){
        if (!self.jianluInfoModel.isSendCer) {
            [[FWHudManager sharedManager] showErrorMessage:@"请发放车手出入证" toController:self.vc];
            return ;
        }
    }else if([usvm.jianlu_check isEqualToString:@"1"]){
        
        if ([usvm.qiandao_check isEqualToString:@"1"]) {
            /* 签到 */
            if (!self.jianluInfoModel.isSendCer) {
                [[FWHudManager sharedManager] showErrorMessage:@"请发放车手出入证" toController:self.vc];
                return ;
            }
        }
        
        if (!self.jianluInfoModel.isSendGift) {
            [[FWHudManager sharedManager] showErrorMessage:@"请发放车手礼包" toController:self.vc];
            return ;
        }
        
        if ( !self.jianluInfoModel.isUploadPic) {
            [[FWHudManager sharedManager] showErrorMessage:@"请上传免责声明" toController:self.vc];
            return ;
        }
    }
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"请确认车手信息后，进行签到或检录操作！" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

        NSDictionary * params ;
        NSString * actionString;
        if ([usvm.qiandao_check isEqualToString:@"1"] && ![usvm.jianlu_check isEqualToString:@"1"]){
            /* 只有签到权限 */
            params = @{
                        @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                        @"baoming_user_id":self.jianluInfoModel.baoming_user_id,
                      };
            actionString = Submit_qiandao;
        }else if([usvm.jianlu_check isEqualToString:@"1"]){
            /* 有检录权限 */
            params = @{
                       @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                       @"baoming_user_id":self.jianluInfoModel.baoming_user_id,
                       @"mianze_img":self.mianze_img,
                     };
            actionString = Submit_jianlu;
        }
        
        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
        [request startWithParameters:params WithAction:actionString WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([usvm.qiandao_check isEqualToString:@"1"] && ![usvm.jianlu_check isEqualToString:@"1"]){
                    /* 签到成功，改状态 */
                    self.jianluInfoModel.status_qiandao = @"2";
                    self.jianluInfoModel.status_jianlu = @"1";
                }else if([usvm.jianlu_check isEqualToString:@"1"]){
                    /* 签到+检录 成功，改状态 */
                    self.jianluInfoModel.status_qiandao = @"2";
                    self.jianluInfoModel.status_jianlu = @"2";
                }
                [[FWHudManager sharedManager] showErrorMessage:@"信息保存成功" toController:self.vc];

                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSMutableDictionary * dict = [self.jianluInfoModel mj_keyValues];

                    [FWDriverInfoManager saveDriverInfoManagerInfoJSONDataToLocalWithData:dict];

                    for (UIViewController * item in self.vc.navigationController.viewControllers) {
                      if ([item isKindOfClass:[FWCheckAndSignDetailViewController class]]) {
                          [self.vc.navigationController popToViewController:item animated:YES];
                          return ;
                      }
                    }
                    
                    for (UIViewController * item in self.vc.navigationController.viewControllers) {
                      if ([item isKindOfClass:[FWLookDriverListViewController class]]) {
                          [self.vc.navigationController popToViewController:item animated:YES];
                          return ;
                      }
                    }
                    [self.vc.navigationController popViewControllerAnimated:YES];
                });
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 拥有双权限
- (void)haveAllAuth{
    
    if ([self.jianluInfoModel.status_jianlu isEqualToString:@"2"] &&
        [self.jianluInfoModel.status_qiandao isEqualToString:@"2"] ) {
        /* 检录 签到成功，置灰不允许点击 */
        self.guaqiButton.enabled = NO;
        self.guaqiButton.backgroundColor = FWColor(@"#E5E5E5");
        
        self.commitButton.enabled = NO;
        self.commitButton.backgroundColor = FWColor(@"#E5E5E5");
    }else{
        self.guaqiButton.enabled = YES;
        self.guaqiButton.backgroundColor = FWColor(@"#3E68DC");
        
        self.commitButton.enabled = YES;
        self.commitButton.backgroundColor = FWColor(@"#3E68DC");
    }

    [self.secondShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.firtShadowView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.firtShadowView);
        make.right.mas_equalTo(self.firtShadowView);
        make.height.mas_equalTo(63);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];
    
    [self.thirdShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.secondShadowView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.secondShadowView);
        make.right.mas_equalTo(self.secondShadowView);
        make.height.mas_equalTo(63);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];
    
    [self.fourthShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.thirdShadowView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.thirdShadowView);
        make.right.mas_equalTo(self.thirdShadowView);
        make.height.mas_equalTo(170);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];
    
    [self.guaqiButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.photoImageView.mas_bottom).mas_offset(30);
        make.left.mas_equalTo(self.mainView).mas_offset(33);
        make.height.mas_equalTo(41);
        make.width.mas_equalTo((SCREEN_WIDTH-76)/2);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-45);
    }];
}

#pragma mark - > 仅有签到权限
- (void)onlySignAuth{
    
    if ([self.jianluInfoModel.status_qiandao isEqualToString:@"2"] ) {
        /* 检录 签到成功，置灰不允许点击 */
        self.guaqiButton.enabled = NO;
        self.guaqiButton.backgroundColor = FWColor(@"#E5E5E5");
        
        self.commitButton.enabled = NO;
        self.commitButton.backgroundColor = FWColor(@"#E5E5E5");
    }else{
        self.guaqiButton.enabled = YES;
        self.guaqiButton.backgroundColor = FWColor(@"#3E68DC");
        
        self.commitButton.enabled = YES;
        self.commitButton.backgroundColor = FWColor(@"#3E68DC");
    }
    
    
    [self.secondShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.firtShadowView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.firtShadowView);
        make.right.mas_equalTo(self.firtShadowView);
        make.height.mas_equalTo(63);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];
    
    self.thirdShadowView.hidden = YES;
    self.fourthShadowView.hidden = YES;
    
    [self.guaqiButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.secondShadowView.mas_bottom).mas_offset(30);
        make.left.mas_equalTo(self.mainView).mas_offset(33);
        make.height.mas_equalTo(41);
        make.width.mas_equalTo((SCREEN_WIDTH-76)/2);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-45);
    }];
}

#pragma mark - > 仅有检录权限
- (void)onlyJianluAuth{
    
    if ([self.jianluInfoModel.status_jianlu isEqualToString:@"2"] ) {
        /* 检录 签到成功，置灰不允许点击 */
        self.guaqiButton.enabled = NO;
        self.guaqiButton.backgroundColor = FWColor(@"#E5E5E5");
        
        self.commitButton.enabled = NO;
        self.commitButton.backgroundColor = FWColor(@"#E5E5E5");
    }else{
        self.guaqiButton.enabled = YES;
        self.guaqiButton.backgroundColor = FWColor(@"#3E68DC");
        
        self.commitButton.enabled = YES;
        self.commitButton.backgroundColor = FWColor(@"#3E68DC");
    }
    
    self.secondShadowView.hidden = YES;
    
    [self.thirdShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.firtShadowView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.firtShadowView);
        make.right.mas_equalTo(self.firtShadowView);
        make.height.mas_equalTo(63);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];
    
    [self.fourthShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.thirdShadowView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.thirdShadowView);
        make.right.mas_equalTo(self.thirdShadowView);
        make.height.mas_equalTo(170);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];
    
    [self.guaqiButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.photoImageView.mas_bottom).mas_offset(30);
        make.left.mas_equalTo(self.mainView).mas_offset(33);
        make.height.mas_equalTo(41);
        make.width.mas_equalTo((SCREEN_WIDTH-76)/2);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-45);
    }];
}

#pragma mark - > 啥权限都没有
- (void)noAuth{
    
    self.secondShadowView.hidden = YES;
    self.thirdShadowView.hidden = YES;
    self.fourthShadowView.hidden = YES;
    self.guaqiButton.hidden = YES;
    self.commitButton.hidden = YES;
}
@end
