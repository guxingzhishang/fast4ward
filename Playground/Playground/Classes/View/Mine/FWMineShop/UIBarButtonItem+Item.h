//
//  UIBarButtonItem+Item.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIBarButtonItem (Item)
+ (instancetype)barButtonItemWithTitle:(NSString *)title target:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;

@end

NS_ASSUME_NONNULL_END
