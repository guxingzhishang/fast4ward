//
//  FWGoodsDetailView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoodsDetailView.h"

@implementation FWGoodsDetailView

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWTextColor_F6F8FA;
        
        [self setupTopView];
        [self setupShopView];
        [self setupDetailView];
    }
    
    return self;
}

#pragma mark - > 初始化顶部视图
- (void)setupTopView{
    
    self.topView = [UIView new];
    self.topView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.topView];
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(SCREEN_WIDTH+109);
    }];
    
    
    self.bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.bannerView.autoScroll = NO;
    self.bannerView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    [self.topView addSubview:self.bannerView];
    
    self.backButton = [[UIButton alloc] initWithFrame: CGRectMake(0,25+FWCustomeSafeTop,45,30)];
    [self.backButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bannerView addSubview:self.backButton];
    
    self.countLabel = [UILabel new];
    self.countLabel.layer.cornerRadius = 2;
    self.countLabel.layer.masksToBounds = YES;
    self.countLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.countLabel.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.4);
    self.countLabel.font = DHSystemFontOfSize_15;
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    [self.bannerView addSubview:self.countLabel];
    [self.countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.bannerView);
        make.size.mas_equalTo(CGSizeMake(65, 23));
        make.bottom.mas_equalTo(self.bannerView).mas_offset(-10);
    }];
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 24.3];
    self.priceLabel.textColor = DHRedColorff6f00;
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topView).mas_offset(20);
        make.height.mas_equalTo(47);
        make.top.mas_equalTo(self.bannerView.mas_bottom).mas_offset(0);
        make.width.mas_equalTo(150);
    }];
    
    self.scanLabel = [[UILabel alloc] init];
    self.scanLabel.font = DHSystemFontOfSize_13;
    self.scanLabel.textColor = FWTextColor_B6BCC4;
    self.scanLabel.textAlignment = NSTextAlignmentRight;
    [self.topView addSubview:self.scanLabel];
    [self.scanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.topView).mas_offset(-20);
        make.centerY.mas_equalTo(self.priceLabel);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.scanImageView = [[UIImageView alloc] init];
    self.scanImageView.image = [UIImage imageNamed:@"card_eye"];
    [self.topView addSubview:self.scanImageView];
    [self.scanImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(16, 11));
        make.centerY.mas_equalTo(self.scanLabel);
        make.right.mas_equalTo(self.scanLabel.mas_left).mas_offset(-5);
    }];
    
    self.lineOneView = [UIView new];
    self.lineOneView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.topView addSubview:self.lineOneView];
    [self.lineOneView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.priceLabel);
        make.height.mas_equalTo(1);
        make.right.mas_equalTo(self.scanLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(1);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = DHSystemFontOfSize_15;
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.priceLabel);
        make.top.mas_equalTo(self.lineOneView.mas_bottom).mas_offset(10);
        make.height.mas_greaterThanOrEqualTo(5);
        make.width.mas_greaterThanOrEqualTo(100);
        make.right.mas_equalTo(self.topView).mas_offset(-20);
    }];
    
}

- (void)setupShopView{
    
    self.shopView = [UIView new];
    self.shopView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.shopView];
    [self.shopView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.topView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(74);
    }];

    CGFloat photoW = 55;
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.layer.cornerRadius = photoW/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder"];
    [self.shopView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopView).mas_offset(21);
        make.centerY.mas_equalTo(self.shopView);
        make.size.mas_equalTo(CGSizeMake(photoW, photoW));
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoClick)];
    [self.photoImageView addGestureRecognizer:tap];
    
    
    self.dongjiaImageView = [[UIImageView alloc] init];
    self.dongjiaImageView.image = [UIImage imageNamed:@"bussiness_renzheng"];
    [self.shopView addSubview:self.dongjiaImageView];
    [self.dongjiaImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.photoImageView);
        make.bottom.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(53, 15));
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = DHBoldSystemFontOfSize_14;
    self.nameLabel.textColor = FWTextColor_121619;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.shopView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.photoImageView).mas_offset(5);
        make.height.mas_equalTo(22);
        make.width.mas_greaterThanOrEqualTo(100);
        make.right.mas_equalTo(self.shopView).mas_offset(-115);
    }];
    
    self.goodsLabel = [[UILabel alloc] init];
    self.goodsLabel.font = DHSystemFontOfSize_12;
    self.goodsLabel.textColor = FWTextColor_B6BCC4;
    self.goodsLabel.textAlignment = NSTextAlignmentLeft;
    [self.shopView addSubview:self.goodsLabel];
    [self.goodsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(self.nameLabel);
    }];
    
    self.goShopButton = [[UIButton alloc] init];
    self.goShopButton.layer.cornerRadius = 2;
    self.goShopButton.layer.masksToBounds = YES;
    self.goShopButton.titleLabel.font = DHSystemFontOfSize_13;
    self.goShopButton.backgroundColor = FWTextColor_222222;
    [self.goShopButton setTitle:@"进入店铺" forState:UIControlStateNormal];
    [self.goShopButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.goShopButton addTarget:self action:@selector(goShopButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.shopView addSubview:self.goShopButton];
    [self.goShopButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.shopView).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(91, 31));
    }];
}

- (void)setupDetailView{
    
    self.detailView = [UIView new];
    self.detailView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.detailView];
    [self.detailView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.shopView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(50);
        make.bottom.mas_equalTo(self);
    }];
    
    self.shopDetailLabel = [[UILabel alloc] init];
    self.shopDetailLabel.text = @"商品详情";
    self.shopDetailLabel.font = DHSystemFontOfSize_15;
    self.shopDetailLabel.textColor = FWTextColor_000000;
    self.shopDetailLabel.textAlignment = NSTextAlignmentLeft;
    [self.detailView addSubview:self.shopDetailLabel];
    [self.shopDetailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.detailView).mas_offset(15);
        make.top.mas_equalTo(self.detailView).mas_offset(0);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(100);
    }];
    
}

- (void)configForView:(id)model{
    
    self.listModel = (FWBussinessShopGoodsListModel *)model;
    
    self.bannerImageCount = self.listModel.imgs.count;
    
    // 轮播图
    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (FWBussinessImageModel * imageModel in self.listModel.imgs) {
        [tempArr addObject:imageModel.img_url];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.bannerView.imageURLStringsGroup = tempArr;
    });
    self.countLabel.text = [NSString stringWithFormat:@"1/%@",@(self.bannerImageCount).stringValue];

    self.priceLabel.text = [NSString stringWithFormat:@"￥%@",self.listModel.price];
    self.scanLabel.text = self.listModel.click_count;
    self.titleLabel.text = self.listModel.title;
    
    self.nameLabel.text = self.listModel.user_info.nickname;
    self.goodsLabel.text = [NSString stringWithFormat:@"全部商品：%@",self.listModel.shop_goods_count];
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    CGFloat partImageY = 0;
    for (int i = 0; i < self.listModel.detail.count; i++) {
        FWBussinessImageModel * imageModel = self.listModel.detail[i];
        
        UIImageView * partImageView = [[UIImageView alloc] init];
        partImageView.tag = 1090 +i;
        partImageView.contentMode = UIViewContentModeScaleAspectFill;
        partImageView.clipsToBounds = YES;
        partImageView.userInteractionEnabled = YES;
        [partImageView sd_setImageWithURL:[NSURL URLWithString:imageModel.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        if (!imageModel.img_width || [imageModel.img_width floatValue]<= 0 ||
            !imageModel.img_height || [imageModel.img_height floatValue]<= 0) {
            // 兼容没有宽高的情况
            imageModel.img_width = @(SCREEN_WIDTH).stringValue;
            imageModel.img_height = @(SCREEN_WIDTH).stringValue;
        }
        [self.detailView addSubview:partImageView];
        [partImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.height.mas_equalTo((SCREEN_WIDTH*[imageModel.img_height floatValue]/[imageModel.img_width floatValue]));
            make.left.right.mas_equalTo(self.detailView);
            make.top.mas_equalTo(self.detailView).mas_offset(50+partImageY);
            if (i == self.listModel.detail.count-1) {
                make.bottom.mas_equalTo(self.detailView);
            }
        }];
        
//        NSLog(@"---%.2f",(SCREEN_WIDTH*[imageModel.img_height floatValue]/[imageModel.img_width floatValue]));
        partImageY = partImageY +(SCREEN_WIDTH*[imageModel.img_height floatValue]/[imageModel.img_width floatValue]);
//        NSLog(@"---%.2f",partImageY);
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        [partImageView addGestureRecognizer:tap];
    }
}

#pragma mark - > 点击头像
- (void)photoClick{
    
}

#pragma mark - > 进入店铺
- (void)goShopButtonOnClick{
    
    if (!self.listModel.user_info.uid) {
        return;
    }
    FWCustomerShopViewController * UIVC = [[FWCustomerShopViewController alloc] init];
    UIVC.user_id = self.listModel.user_info.uid;
    UIVC.isUserPage = NO;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 返回
- (void)backBtnClick{
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 点击图片
- (void)tapClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 1090;
    
    NSMutableArray * tempArr = @[].mutableCopy;

    for (FWBussinessImageModel * imageModel in self.listModel.detail) {
        [tempArr addObject:imageModel.img_url];
    }
    
    if (tempArr.count > 0 && index < tempArr.count) {
        
        FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
        browser.isFullWidthForLandScape = YES;
        browser.isNeedLandscape = YES;
        browser.currentImageIndex = (int)index;
        browser.imageArray = [tempArr mutableCopy];
        browser.originalImageArray = [tempArr copy];
        browser.vc = self.vc;
        browser.fromType = 1;
        [browser show];
    }
}

#pragma mark - > banner代理方法
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{
    
    self.countLabel.text = [NSString stringWithFormat:@"%@/%@",@(index+1).stringValue,@(self.bannerImageCount).stringValue];
}

-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (FWBussinessImageModel * imageModel in self.listModel.imgs) {
        [tempArr addObject:imageModel.img_url];
    }
    
    if (tempArr.count > 0 && index < tempArr.count) {
        
        FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
        browser.isFullWidthForLandScape = YES;
        browser.isNeedLandscape = YES;
        browser.currentImageIndex = (int)index;
        browser.imageArray = [tempArr mutableCopy];
        browser.originalImageArray = [tempArr copy];
        browser.vc = self.vc;
        browser.fromType = 1;
        [browser show];
    }
}
@end
