//
//  FWShopInfoDetailView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWShopInfoDetailView.h"

@implementation FWShopInfoDetailView
@synthesize shopInfo;

- (id)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = FWTextColor_F6F8FA;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.mainView = [[UIView alloc] init];
    self.mainView.backgroundColor = FWTextColor_F6F8FA;
    [self addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    self.firstView = [[UIView alloc] init];
    self.firstView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.firstView.layer.cornerRadius = 5;
    self.firstView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.firstView];
    [self.firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mainView).mas_offset(14);
        make.right.mas_equalTo(self.mainView).mas_offset(-14);
        make.top.mas_equalTo(self.mainView).mas_offset(20);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_greaterThanOrEqualTo(100);
    }];
    
    self.bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-28, 200) delegate:self placeholderImage:[UIImage imageNamed:@"shopinfo_placeholder"]];
    self.bannerView.autoScrollTimeInterval = 4;
    self.bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.bannerView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    [self.firstView addSubview:self.bannerView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.titleLabel.textColor = FWTextColor_272727;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.firstView).mas_offset(11);
        make.right.mas_equalTo(self.firstView).mas_offset(-11);
        make.top.mas_equalTo(self.bannerView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH-28-22);
        make.height.mas_greaterThanOrEqualTo(22);
    }];
    
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.numberOfLines = 0;
    self.descLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.descLabel.textColor = FWTextColor_272727;
    self.descLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:self.descLabel];
    [self.descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(8);
        make.width.mas_equalTo(self.titleLabel);
        make.height.mas_greaterThanOrEqualTo(19);
        make.bottom.mas_equalTo(self.firstView).mas_offset(-14);
    }];
    
    self.secondView = [[UIView alloc] init];
    self.secondView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.secondView.layer.cornerRadius = 5;
    self.secondView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.secondView];
    [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.firstView);
        make.right.mas_equalTo(self.firstView);
        make.top.mas_equalTo(self.firstView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(self.firstView);
        make.height.mas_greaterThanOrEqualTo(48);
    }];
    
    self.lianxirenLabel = [[UILabel alloc] init];
    self.lianxirenLabel.text = @"联系人";
    self.lianxirenLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.lianxirenLabel.textColor = FWTextColor_272727;
    self.lianxirenLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:self.lianxirenLabel];
    [self.lianxirenLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.secondView);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(48);
    }];
    
    self.rightOneImageView = [[UIImageView alloc] init];
    self.rightOneImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.secondView addSubview:self.rightOneImageView];
    [self.rightOneImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(5, 9));
        make.centerY.mas_equalTo(self.lianxirenLabel);
        make.right.mas_equalTo(self.descLabel);
    }];
    
    self.memberButton = [[UIButton alloc] init];
//    [self.memberButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.secondView addSubview:self.memberButton];
    [self.memberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.rightOneImageView.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(self.rightOneImageView);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
    
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.nameLabel.textColor = FWTextColor_272727;
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.secondView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.memberButton);
        make.right.mas_equalTo(self.memberButton.mas_left).mas_offset(-5);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-170);
        make.height.mas_equalTo(20);
    }];
    
    self.photoImageView = [UIImageView new];
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    self.photoImageView.layer.cornerRadius = 33/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.secondView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.nameLabel.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(self.nameLabel);
        make.width.mas_equalTo(33);
        make.height.mas_equalTo(33);
    }];
//    UITapGestureRecognizer * photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoTapClick)];
//    [self.photoImageView addGestureRecognizer:photoTap];
    
    self.authenticationView = [UIImageView new];
    [self.secondView addSubview:self.authenticationView];
    [self.authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.photoImageView).mas_offset(2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(2);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
    }];
    
    self.secondLineView = [[UIView alloc] init];
    self.secondLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.secondView addSubview:self.secondLineView];
    [self.secondLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.titleLabel);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.lianxirenLabel.mas_bottom);
    }];
    
    self.lianxirenButton = [[UIButton alloc] init];
    [self.lianxirenButton addTarget:self action:@selector(photoTapClick) forControlEvents:UIControlEventTouchUpInside];
    [self.secondView addSubview:self.lianxirenButton];
    [self.lianxirenButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(self.lianxirenLabel);
        make.right.mas_equalTo(self.secondView);
    }];
    
    self.lianxidianhuaLabel = [[UILabel alloc] init];
    self.lianxidianhuaLabel.text = @"联系电话";
    self.lianxidianhuaLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.lianxidianhuaLabel.textColor = FWTextColor_272727;
    self.lianxidianhuaLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:self.lianxidianhuaLabel];
    [self.lianxidianhuaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.self.lianxirenLabel.mas_bottom);
        make.width.mas_equalTo(300);
        make.height.mas_equalTo(48);
    }];
    
    self.rightTwoImageView = [[UIImageView alloc] init];
    self.rightTwoImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.secondView addSubview:self.rightTwoImageView];
    [self.rightTwoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(5, 9));
        make.centerY.mas_equalTo(self.lianxidianhuaLabel);
        make.right.mas_equalTo(self.descLabel);
    }];
    
    self.phoneLabel = [[UILabel alloc] init];
    self.phoneLabel.userInteractionEnabled = YES;
    self.phoneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.phoneLabel.textColor = FWTextColor_272727;
    self.phoneLabel.textAlignment = NSTextAlignmentRight;
    [self.secondView addSubview:self.phoneLabel];
    [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.lianxidianhuaLabel);
        make.right.mas_equalTo(self.rightTwoImageView.mas_left).mas_offset(-5);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(20);
    }];
    UITapGestureRecognizer * phoneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneTapClick)];
    [self.phoneLabel addGestureRecognizer:phoneTap];
    
    self.secondLineView.hidden = YES;
    self.lianxidianhuaLabel.hidden = YES;
    self.rightTwoImageView.hidden = YES;
    self.phoneLabel.hidden = YES;
    
    self.thirdView = [[UIView alloc] init];
    self.thirdView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.thirdView.layer.cornerRadius = 5;
    self.thirdView.layer.masksToBounds = YES;
    [self.mainView addSubview:self.thirdView];
    [self.thirdView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.secondView);
        make.right.mas_equalTo(self.secondView);
        make.top.mas_equalTo(self.secondView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(self.secondView);
        make.height.mas_greaterThanOrEqualTo(184);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-30);
    }];
}

- (void)configForView:(id)model{
    
    shopInfo = (FWHomeShopListModel *)model;
    
    if (!shopInfo) return;
    
    if(shopInfo.shop_desc.length<=0) shopInfo.shop_desc = @"这个店家很懒，还没有写店铺介绍哦";
    
    self.titleLabel.text = shopInfo.user_info.title;
    self.descLabel.text = shopInfo.shop_desc;
    self.nameLabel.text = shopInfo.user_info.nickname;

    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:shopInfo.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    // 轮播图
    NSMutableArray * tempArr = @[].mutableCopy;
    for (FWBussinessImageModel * imageModel in shopInfo.imgs) {
        [tempArr addObject:imageModel.img_url];
    }
    
    // vip图标
    [self.memberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.rightOneImageView.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(self.rightOneImageView);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
    
    self.memberButton.enabled = YES;
    if ([shopInfo.user_info.user_level isEqualToString:@"1"]) {
        [self.memberButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([shopInfo.user_info.user_level isEqualToString:@"2"]) {
        [self.memberButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([shopInfo.user_info.user_level isEqualToString:@"3"]) {
        [self.memberButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.memberButton.enabled = NO;
        [self.memberButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [self.memberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.rightOneImageView.mas_left).mas_offset(-5);
            make.centerY.mas_equalTo(self.rightOneImageView);
            make.size.mas_equalTo(CGSizeMake(0.5, 15));
        }];
    }
    
    // 认证身份
    if([shopInfo.user_info.cert_status isEqualToString:@"2"]) {
        self.authenticationView.hidden = NO;
        self.authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        self.authenticationView.hidden = YES;
    }
    
    
    //如果有手机号，就显示该模块
    if (shopInfo.mobile.length > 0) {
        self.phoneLabel.text = shopInfo.mobile;
    
        self.secondLineView.hidden = NO;
        self.lianxidianhuaLabel.hidden = NO;
        self.rightTwoImageView.hidden = NO;
        self.phoneLabel.hidden = NO;
        [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.firstView);
            make.right.mas_equalTo(self.firstView);
            make.top.mas_equalTo(self.firstView.mas_bottom).mas_offset(10);
            make.width.mas_equalTo(self.firstView);
            make.height.mas_equalTo(96);
        }];
    }else{
        self.secondLineView.hidden = YES;
        self.lianxidianhuaLabel.hidden = YES;
        self.rightTwoImageView.hidden = YES;
        self.phoneLabel.hidden = YES;
        [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.firstView);
            make.right.mas_equalTo(self.firstView);
            make.top.mas_equalTo(self.firstView.mas_bottom).mas_offset(10);
            make.width.mas_equalTo(self.firstView);
            make.height.mas_equalTo(48);
        }];
    }
    
    NSMutableArray * detailArr = @[].mutableCopy;
    NSMutableArray * titleArr = @[].mutableCopy;

    if (shopInfo.show_address.length <= 0) {
        shopInfo.show_address = @"这个店家很懒，还没有公布地址哦";
    }
    [detailArr addObject:shopInfo.show_address];
    [titleArr addObject:@"店铺地址"];
    
    if (shopInfo.main_business_string.length > 0) {
        [titleArr addObject:@"主营业务"];
        [detailArr addObject:shopInfo.main_business_string];
    }else if(shopInfo.main_business.count > 0){
        [titleArr addObject:@"主营业务"];
        shopInfo.main_business_string = [shopInfo.main_business componentsJoinedByString:@"，"];
        [detailArr addObject:shopInfo.main_business_string];
    }
    
    
    
    if (shopInfo.main_cars.length > 0) {
        [titleArr addObject:@"擅长车型"];
        [detailArr addObject:shopInfo.main_cars];
    }
    
    CGFloat thirdHeight = 0;
    for (int i = 0; i<titleArr.count; i++) {

        UIView * thirdPartView = [[UIView alloc] init];
        thirdPartView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self.thirdView addSubview:thirdPartView];
        [thirdPartView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.thirdView);
            make.width.mas_equalTo(thirdPartView);
            make.height.mas_greaterThanOrEqualTo(61);
            make.top.mas_equalTo(self.thirdView).mas_offset(thirdHeight);
            if (i == titleArr.count-1) {
                make.bottom.mas_equalTo(self.thirdView);
            }
        }];
        
        UILabel * leftLabel = [[UILabel alloc] init];
        leftLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];;
        leftLabel.textColor = FWTextColor_272727;
        leftLabel.textAlignment = NSTextAlignmentLeft;
        leftLabel.text = titleArr[i];
        [thirdPartView addSubview:leftLabel];
        [leftLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(65, 20));
            make.top.mas_equalTo(thirdPartView).mas_offset(14);
            make.left.mas_equalTo(self.titleLabel);
        }];
        
        UITextView * rightTextView = [[UITextView alloc] init];
        rightTextView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        rightTextView.contentInset = UIEdgeInsetsMake(-7, -5,0 , 0);
        rightTextView.editable = NO;
        rightTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];;
        rightTextView.textColor = FWTextColor_A7ADB4;
        rightTextView.textAlignment = NSTextAlignmentLeft;
        rightTextView.text = detailArr[i];
        [self.thirdView addSubview:rightTextView];
     
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:rightTextView.attributedText];
        CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 50].height;
       
        if (textHeight > 28) {
            textHeight += 40;
        }else{
            textHeight = 61;
        }
        
        [rightTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(textHeight);
            make.right.mas_equalTo(self.thirdView).mas_offset(-10);
            make.top.mas_equalTo(leftLabel).mas_offset(-1);
            make.left.mas_equalTo(leftLabel.mas_right).mas_offset(12);
            make.bottom.mas_equalTo(thirdPartView);
        }];
        
        
        if (i <titleArr.count-1 && titleArr.count!= 1) {
            UIView * lineView = [[UIView alloc] init];
            lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
            [self.thirdView addSubview:lineView];
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(self.titleLabel);
                make.height.mas_equalTo(1);
                make.top.mas_equalTo(rightTextView.mas_bottom);
            }];
        }
        
        thirdHeight = thirdHeight+textHeight+14;
        
        if (i == titleArr.count - 1) {
            [thirdPartView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(self.thirdView);
                make.width.mas_equalTo(thirdPartView);
                make.height.mas_greaterThanOrEqualTo(61);
                make.top.mas_equalTo(self.thirdView).mas_offset(thirdHeight);
                make.bottom.mas_equalTo(self.thirdView);
            }];
        }
    }
    
    [self.thirdView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.secondView);
        make.right.mas_equalTo(self.secondView);
        make.top.mas_equalTo(self.secondView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(self.secondView);
        make.height.mas_equalTo(thirdHeight);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-30);
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.bannerView.imageURLStringsGroup = tempArr;
    });
}

#pragma mark - > 会员跳转
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 点击图片
- (void)photoTapClick{
    
    if (self.isUserPage) {
        [self.vc.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.shopInfo.user_info.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 手机号
- (void)phoneTapClick{
    
    NSString *tel = [NSString stringWithFormat:@"tel:%@", shopInfo.mobile];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel] options:@{} completionHandler:nil];
    });
}

#pragma mark - > 点击banner
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (FWHomeCoverListModel * model in self.shopInfo.imgs) {
        [tempArr addObject:model.img_url];
    }
    
    if (tempArr.count > 0 && index < tempArr.count) {
        
        FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
        browser.isFullWidthForLandScape = YES;
        browser.isNeedLandscape = YES;
        browser.currentImageIndex = (int)index;
        browser.imageArray = [tempArr mutableCopy];
        browser.originalImageArray = [tempArr copy];
        browser.vc = self.vc;
        browser.fromType = 1;
        [browser show];
    }
}
@end
