//
//  FWBussinessShopHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWBussinessShopHeaderViewDelegate <NSObject>

#pragma mark - > 操作商品 ： 0 全部商品  1：已上架  2：已下架
- (void)bussinessShopGoodsDeal:(NSString *)index;

@end

@interface FWBussinessShopHeaderView : UIView

@property (nonatomic, strong) UIImageView * topImageView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UILabel * titleLabel;

@property (nonatomic, strong) UIImageView * photoBgImageView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * dongjiaImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * goodsNumLabel;

@property (nonatomic, strong) UIButton * addGoodsButton;
@property (nonatomic, strong) UIButton * uploadButton;
@property (nonatomic, strong) UIButton * downButton;

@property (nonatomic, assign) BOOL  isBussiness;//是否是商家
@property (nonatomic, assign) BOOL  isUserPage;

@property (nonatomic, strong) FWBussinessShopModel * shopModel;
@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, weak) id<FWBussinessShopHeaderViewDelegate>delegate;


- (void)configForHeader:(id)model;

@end

NS_ASSUME_NONNULL_END
