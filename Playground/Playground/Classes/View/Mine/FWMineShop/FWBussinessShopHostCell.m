//
//  FWBussinessShopHostCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessShopHostCell.h"

@interface FWBussinessShopHostCell ()

@property (nonatomic, strong) FWBussinessShopGoodsListModel * shopModel;

@end

@implementation FWBussinessShopHostCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.contentView.layer.cornerRadius = 4;
        self.contentView.layer.masksToBounds = YES;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.cardImageView = [[UIImageView alloc] init];
    self.cardImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.cardImageView.image = [UIImage imageNamed:@"placeholder"];
    self.cardImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.cardImageView];
    [self.cardImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(91);
        make.top.left.bottom.mas_equalTo(self.contentView);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = DHSystemFontOfSize_13;
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cardImageView.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.cardImageView).mas_offset(5);
        make.height.mas_greaterThanOrEqualTo(5);
        make.width.mas_greaterThanOrEqualTo(100);
        make.right.mas_equalTo(self.contentView).mas_offset(-11);
    }];
    
    self.editButton = [[UIButton alloc] init];
    self.editButton.layer.cornerRadius = 4;
    self.editButton.layer.masksToBounds = YES;
    self.editButton.layer.borderColor = FWTextColor_B6BCC4.CGColor;
    self.editButton.layer.borderWidth = 1;
    self.editButton.titleLabel.font = DHSystemFontOfSize_12;
    [self.editButton setTitle:@"编辑" forState:UIControlStateNormal];
    [self.editButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
    [self.editButton addTarget:self action:@selector(editButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.editButton];
    [self.editButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.mas_equalTo(self.contentView).mas_offset(-8);
        make.size.mas_equalTo(CGSizeMake(48, 22));
    }];
    
    self.scanLabel = [[UILabel alloc] init];
    self.scanLabel.font = DHSystemFontOfSize_12;
    self.scanLabel.textColor = DHTitleColor_666666;
    self.scanLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.scanLabel];
    [self.scanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.editButton).mas_offset(0);
        make.bottom.mas_equalTo(self.editButton.mas_top).mas_offset(0);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.scanImageView = [[UIImageView alloc] init];
    self.scanImageView.image = [UIImage imageNamed:@"card_eye"];
    [self.contentView addSubview:self.scanImageView];
    [self.scanImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(16, 11));
        make.centerY.mas_equalTo(self.scanLabel);
        make.right.mas_equalTo(self.scanLabel.mas_left).mas_offset(-5);
    }];
    
    self.deleteButton = [[UIButton alloc] init];
    self.deleteButton.layer.cornerRadius = 4;
    self.deleteButton.layer.masksToBounds = YES;
    self.deleteButton.layer.borderColor = FWTextColor_B6BCC4.CGColor;
    self.deleteButton.layer.borderWidth = 1;
    self.deleteButton.titleLabel.font = DHSystemFontOfSize_12;
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(deleteButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.deleteButton];
    [self.deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.size.mas_equalTo(self.editButton);
        make.centerY.mas_equalTo(self.editButton);
    }];
    
    self.downButton = [[UIButton alloc] init];
    self.downButton.layer.cornerRadius = 4;
    self.downButton.layer.masksToBounds = YES;
    self.downButton.layer.borderColor = FWTextColor_B6BCC4.CGColor;
    self.downButton.layer.borderWidth = 1;
    self.downButton.titleLabel.font = DHSystemFontOfSize_12;
    [self.downButton setTitle:@"下架" forState:UIControlStateNormal];
    [self.downButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
    [self.downButton addTarget:self action:@selector(downButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.downButton];
    [self.downButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.deleteButton.mas_right).mas_offset(10);
        make.size.mas_equalTo(self.deleteButton);
        make.centerY.mas_equalTo(self.deleteButton);
    }];
   
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = DHSystemFontOfSize_15;
    self.priceLabel.textColor = DHRedColorff6f00;
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.centerY.mas_equalTo(self.scanLabel);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(150);
    }];
    
    
}

- (void)configForShopCell:(id)model{
 
    self.shopModel = (FWBussinessShopGoodsListModel *)model;
    
    self.scanLabel.text = self.shopModel.click_count;
    self.titleLabel.text = self.shopModel.title;
    self.priceLabel.text = [NSString stringWithFormat:@"￥%@",self.shopModel.price];

    [self.cardImageView sd_setImageWithURL:[NSURL URLWithString:self.shopModel.cover.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    if ([self.shopModel.status_show isEqualToString:@"1"]) {
        [self.downButton setTitle:@"下架" forState:UIControlStateNormal];
    }else{
        [self.downButton setTitle:@"上架" forState:UIControlStateNormal];
    }
}

#pragma mark - > 编辑
- (void)editButtonOnClick:(UIButton *)sender{
    
    FWUploadGoodsViewController * UGVC = [[FWUploadGoodsViewController alloc] init];
    UGVC.oprationType = EditGoods;
    UGVC.listModel = self.shopModel;
    [self.vc.navigationController pushViewController:UGVC animated:YES];
}

#pragma mark - > 下架
- (void)downButtonOnClick:(UIButton *)sender{
    
    NSInteger index = sender.tag-6000;
    
    NSString * statusString ;
    if ([self.shopModel.status_show isEqualToString:@"1"]) {
        self.shopModel.status_show = @"2";
        statusString = @"下架成功";
    }else{
        self.shopModel.status_show = @"1";
        statusString = @"上架成功";
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"goods_id":self.shopModel.goods_id,
                              @"status_show":self.shopModel.status_show,
                              };
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    [request startWithParameters:params WithAction:Update_goods_status WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [[FWHudManager sharedManager] showErrorMessage:statusString toController:self.vc];
            
            if ([self.delegate respondsToSelector:@selector(downButtonClick:)]) {
                [self.delegate downButtonClick:index];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

#pragma mark - > 删除
- (void)deleteButtonOnClick:(UIButton *)sender{
 
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"商品删除后不可恢复，确认删除该商品?" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确认删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        NSInteger index = sender.tag - 4000;
        if ([self.delegate respondsToSelector:@selector(deleteButtonClick:)]) {
            [self.delegate deleteButtonClick:index];
        }
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

@end
