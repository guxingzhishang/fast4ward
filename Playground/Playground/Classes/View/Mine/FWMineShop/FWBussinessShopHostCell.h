//
//  FWBussinessShopHostCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWBussinessShopHostCellDelegate <NSObject>

- (void)deleteButtonClick:(NSInteger)index;
- (void)downButtonClick:(NSInteger)index;

@end

@interface FWBussinessShopHostCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView * cardImageView;
@property (nonatomic, strong) UIImageView * scanImageView;
@property (nonatomic, strong) UILabel * scanLabel;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * priceLabel;

@property (nonatomic, strong) UIButton * deleteButton;
@property (nonatomic, strong) UIButton * editButton;
@property (nonatomic, strong) UIButton * downButton;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, weak) id<FWBussinessShopHostCellDelegate>delegate;

- (void)configForShopCell:(id)model;

@end

NS_ASSUME_NONNULL_END
