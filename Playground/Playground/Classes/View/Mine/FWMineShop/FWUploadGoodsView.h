//
//  FWUploadGoodsView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZAddImage.h"
#import "FWGoodsChannelSegementView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWUploadGoodsViewDelegate <NSObject>

- (void)uploadButtonClick;

@end

@interface FWUploadGoodsView : UIScrollView<UITextFieldDelegate,SZAddImageDelegate,FWGoodsChannelSegementViewDelegate,UITextViewDelegate>

@property (nonatomic, strong) UILabel * showLabel;
@property (nonatomic, strong) UIView * showView;
@property (nonatomic, strong) SZAddImage * addShowImageView;

@property (nonatomic, strong) UIView * lineOneView;

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UITextField * titleTF;
@property (nonatomic, strong) UIView * lineTitleView;

@property (nonatomic, strong) UILabel * shortTitleLabel;
@property (nonatomic, strong) UITextField * shortTitleTF;
@property (nonatomic, strong) UIView * lineShortTitleView;

@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UITextField * priceTF;
@property (nonatomic, strong) UIView * linePriceView;

@property (nonatomic, strong) UIView * lineTwoView;

@property (nonatomic, strong) UILabel * goodsDetailLabel;
@property (nonatomic, strong) UIView * goodsDetailView;
@property (nonatomic, strong) SZAddImage * addDetailImageView;

@property (nonatomic, strong) UIView * lineThreeView;

@property (nonatomic, strong) UILabel * channelLabel;

@property (nonatomic, strong) FWGoodsChannelSegementView * segement;

@property (nonatomic, strong) UIButton * paramButton;

@property (nonatomic, strong) UILabel * paramLabel;
@property (nonatomic, strong) IQTextView * paramTF;
@property (nonatomic, strong) UIView * lineParamView;

@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) IQTextView * addressTF;
@property (nonatomic, strong) UIView * lineAddressView;

@property (nonatomic, strong) UILabel * webLabel;
@property (nonatomic, strong) IQTextView * webTF;
@property (nonatomic, strong) UIView * lineWebView;

@property (nonatomic, strong) UILabel * tipLabel;

@property (nonatomic, strong) UIButton * uploadButton;

/**
 *  存储所有的照片(UIImage)
 */
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWGoodsChannelModel * channelModel;
@property (nonatomic, strong) FWGoodsChannelListModel * channelListModel;


@property (nonatomic, weak) id<FWUploadGoodsViewDelegate>uplLoadDelegate;

- (void)configForModel:(id)model;

- (void)setChannelMoudle:(FWGoodsChannelModel *)model;
@end

NS_ASSUME_NONNULL_END
