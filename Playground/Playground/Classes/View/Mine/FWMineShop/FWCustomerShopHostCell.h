//
//  FWCustomerShopHostCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWCustomerShopHostCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView * cardImageView;
@property (nonatomic, strong) UIImageView * scanImageView;
@property (nonatomic, strong) UILabel * scanLabel;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * priceLabel;

@property (nonatomic, strong) FWBussinessShopGoodsListModel * shopModel;


- (void)configForShopCell:(id)model;
@end

NS_ASSUME_NONNULL_END
