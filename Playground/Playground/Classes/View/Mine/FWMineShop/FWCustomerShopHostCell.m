//
//  FWCustomerShopHostCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCustomerShopHostCell.h"

@implementation FWCustomerShopHostCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.contentView.layer.cornerRadius = 4;
        self.contentView.layer.masksToBounds = YES;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.cardImageView = [[UIImageView alloc] init];
    self.cardImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.cardImageView.image = [UIImage imageNamed:@"placeholder"];
    self.cardImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.cardImageView];
    [self.cardImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo((SCREEN_WIDTH-30)/2);
        make.height.mas_equalTo(173);
        make.top.left.right.mas_equalTo(self.contentView);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = DHSystemFontOfSize_13;
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cardImageView).mas_offset(7);
        make.top.mas_equalTo(self.cardImageView.mas_bottom).mas_offset(9);
        make.height.mas_greaterThanOrEqualTo(5);
        make.width.mas_equalTo((SCREEN_WIDTH-30)/2-14);
        make.right.mas_equalTo(self.contentView).mas_offset(-7);
    }];
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = DHSystemFontOfSize_15;
    self.priceLabel.textColor = DHRedColorff6f00;
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-3);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(150);
    }];
    
    self.scanLabel = [[UILabel alloc] init];
    self.scanLabel.font = DHSystemFontOfSize_11;
    self.scanLabel.textColor = FWTextColor_B6BCC4;
    self.scanLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.scanLabel];
    [self.scanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-9);
        make.centerY.mas_equalTo(self.priceLabel);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(5);
    }];
    
    self.scanImageView = [[UIImageView alloc] init];
    self.scanImageView.image = [UIImage imageNamed:@"card_eye"];
    [self.contentView addSubview:self.scanImageView];
    [self.scanImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(16, 11));
        make.centerY.mas_equalTo(self.scanLabel);
        make.right.mas_equalTo(self.scanLabel.mas_left).mas_offset(-5);
    }];
}

- (void)configForShopCell:(id)model{
    
    self.shopModel = (FWBussinessShopGoodsListModel *)model;
    
    self.scanLabel.text = self.shopModel.click_count;
    self.titleLabel.text = self.shopModel.title;
    self.priceLabel.text = [NSString stringWithFormat:@"￥%@",self.shopModel.price];
    
    [self.cardImageView sd_setImageWithURL:[NSURL URLWithString:self.shopModel.cover.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

@end
