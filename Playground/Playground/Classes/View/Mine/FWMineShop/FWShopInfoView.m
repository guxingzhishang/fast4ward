//
//  FWShopInfoView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWShopInfoView.h"
#import "UIBarButtonItem+Item.h"
#import "FWArearModel.h"

@interface FWShopInfoView ()

@property (nonatomic, strong) FWBussinessCertificationModel * cerModel ;
@property (nonatomic, assign) NSInteger  currentCom;
@property (nonatomic, strong) NSString *  select_id;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSMutableArray * showImageArray;
@end

@implementation FWShopInfoView
- (NSMutableArray *)showImageArray{
    if (!_showImageArray) {
        _showImageArray = [[NSMutableArray alloc] init];
    }
    return _showImageArray;
}

- (void)setVc:(UIViewController *)vc{
    _vc = vc;
    self.addPicsImageView.vc = self.vc;
}

- (void)setImages:(NSMutableArray *)images{
    _images = images;
    self.addPicsImageView.showImage = self.images;
}

- (id)init{
    self = [super init];
    if (self) {

        self.backgroundColor = FWTextColor_F6F8FA;
        self.select_id = @"0";
        self.address = @"";
        
        self.idArray = @[].mutableCopy;
        self.ProvinceArr = @[].mutableCopy;
        self.CityArr = @[].mutableCopy;
        self.AreaArr = @[].mutableCopy;
        
        self.arr = @[];
        self.currentCom = -1;
        
        self.mainView = [[UIView alloc] init];
        self.mainView.backgroundColor = FWTextColor_F6F8FA;
        [self addSubview:self.mainView];
        [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self);
        }];

        self.pickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
        self.pickerView.delegate=self;
        self.pickerView.dataSource=self;
        self.pickerView.backgroundColor=[UIColor clearColor];
        
        
        [self setupFirstView];
        [self setupSecondView];
        [self setupThirdView];
        [self setupFourthView];
        [self setupFifthView];
        [self setupSixthView];
        
        [self requestAreaData];
    }
    
    return self;
}

#pragma mark - > UI
- (void)setupFirstView{
    
    self.firstView = [[UIView alloc] init];
    self.firstView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.firstView];
    [self.firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.mainView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 180));
    }];
    
    self.shopDescLabel = [[UILabel alloc] init];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"店铺描述(*必填)" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:18/255.0 green:16/255.0 blue:29/255.0 alpha:1.0]}];
    [string addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:204/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]} range:NSMakeRange(4, 5)];
    self.shopDescLabel.attributedText = string;
    self.shopDescLabel.textAlignment = NSTextAlignmentLeft;
    [self.firstView addSubview:self.shopDescLabel];
    [self.shopDescLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.firstView).mas_offset(19);
        make.size.mas_equalTo(CGSizeMake(200, 15));
        make.top.mas_equalTo(self.firstView).mas_offset(10);
    }];
    
    self.descTextView = [[IQTextView alloc] init];
    self.descTextView.delegate = self;
    self.descTextView.placeholder = @"介绍一下自己的店铺吧~";
    self.descTextView.layer.borderColor = FWTextColor_272727.CGColor;
    self.descTextView.layer.borderWidth = 0.5;
    self.descTextView.layer.cornerRadius = 4;
    self.descTextView.layer.masksToBounds = YES;
    self.descTextView.textColor = FWTextColor_272727;
    self.descTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [self.firstView addSubview:self.descTextView];
    [self.descTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopDescLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-38, 120));
        make.top.mas_equalTo(self.shopDescLabel.mas_bottom).mas_offset(20);
    }];
}

- (void)setupSecondView{
    
    self.secondView = [[UIView alloc] init];
    self.secondView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.secondView];
    [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.top.mas_equalTo(self.firstView.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 150));
    }];
    
    self.shopAddressLabel = [[UILabel alloc] init];
    self.shopAddressLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.shopAddressLabel.text = @"店铺地址(*选填）";
    self.shopAddressLabel.textColor = FWTextColor_12101D;
    self.shopAddressLabel.textAlignment = NSTextAlignmentLeft;
    [self.secondView addSubview:self.shopAddressLabel];
    [self.shopAddressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopDescLabel);
        make.size.mas_equalTo(CGSizeMake(200, 15));
        make.top.mas_equalTo(self.secondView).mas_offset(10);
    }];
    
    self.arearField = [[IQTextView alloc] init];
    self.arearField.delegate = self;
    self.arearField.placeholder = @"请选择地址";
    self.arearField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.arearField.textColor = FWTextColor_12101D;
    self.arearField.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.secondView addSubview:self.arearField];
    [self.arearField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopAddressLabel);
        make.top.mas_equalTo(self.shopAddressLabel.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-38, 45));
    }];


    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];

    UIBarButtonItem *doneButton = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
   
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar.items = @[cancelButton,flexItem, fixItem, doneButton];
    
    self.arearField.inputView = self.pickerView;
    self.arearField.inputAccessoryView = bar;
    
    self.arearLineView = [[UIView alloc] init];
    self.arearLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.secondView addSubview:self.arearLineView];
    [self.arearLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.arearField);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.arearField.mas_bottom);
    }];
    
    self.addressField = [[IQTextView alloc] init];
    self.addressField.delegate = self;
    self.addressField.placeholder = @"详细地址：如道路，门牌号";
    self.addressField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.addressField.textColor = FWTextColor_12101D;
    [self.secondView addSubview:self.addressField];
    [self.addressField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopAddressLabel);
        make.bottom.mas_equalTo(self.secondView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-38, 45));
    }];
    
    self.addressLineView = [[UIView alloc] init];
    self.addressLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.secondView addSubview:self.addressLineView];
    [self.addressLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.addressField);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.addressField.mas_bottom);
    }];
}

- (void)setupThirdView{
    
    self.thirdView = [[UIView alloc] init];
    self.thirdView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.thirdView];
    [self.thirdView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.top.mas_equalTo(self.secondView.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 93));
    }];
    
    self.phoneLabel = [[UILabel alloc] init];
    self.phoneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.phoneLabel.text = @"联系方式(*选填）";
    self.phoneLabel.textColor = FWTextColor_12101D;
    self.phoneLabel.textAlignment = NSTextAlignmentLeft;
    [self.thirdView addSubview:self.phoneLabel];
    [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopDescLabel);
        make.size.mas_equalTo(CGSizeMake(200, 15));
        make.top.mas_equalTo(self.thirdView).mas_offset(10);
    }];
    
    self.phoneField = [[IQTextView alloc] init];
    self.phoneField.delegate = self;
    self.phoneField.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneField.placeholder = @"请输入11位手机号，方便客户联系您";
    self.phoneField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.phoneField.textColor = FWTextColor_12101D;
    [self.thirdView addSubview:self.phoneField];
    [self.phoneField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.phoneLabel);
        make.top.mas_equalTo(self.phoneLabel.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-38, 45));
    }];
    
    self.phoneLineView = [[UIView alloc] init];
    self.phoneLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.thirdView addSubview:self.phoneLineView];
    [self.phoneLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.phoneField);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.phoneField.mas_bottom);
    }];
}

- (void)setupFourthView{
    
    self.fourthView = [[UIView alloc] init];
    self.fourthView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.fourthView];
    [self.fourthView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.top.mas_equalTo(self.thirdView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(50);
    }];
    
    self.bussinessLabel = [[UILabel alloc] init];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"主营业务(*必填）" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:18/255.0 green:16/255.0 blue:29/255.0 alpha:1.0]}];
    [string addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:204/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]} range:NSMakeRange(4, 5)];
    self.bussinessLabel.attributedText = string;
    self.bussinessLabel.textAlignment = NSTextAlignmentLeft;
    [self.fourthView addSubview:self.bussinessLabel];
    [self.bussinessLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopDescLabel);
        make.size.mas_equalTo(CGSizeMake(200, 15));
        make.top.mas_equalTo(self.fourthView).mas_offset(10);
    }];
    
    self.containerView = [[UIView alloc] init];
    [self.fourthView addSubview:self.containerView];
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bussinessLabel).mas_offset(11);
        make.height.mas_greaterThanOrEqualTo(1);
        make.width.mas_equalTo(SCREEN_WIDTH-50);
        make.top.mas_equalTo(self.bussinessLabel.mas_bottom).mas_offset(5);
        make.bottom.mas_equalTo(self.fourthView.mas_bottom).mas_offset(-0);
    }];
}

- (void)setupFifthView{
    
    self.fivethView = [[UIView alloc] init];
    self.fivethView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.fivethView];
    [self.fivethView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.top.mas_equalTo(self.fourthView.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 93));
    }];
    
    self.carStyleLabel = [[UILabel alloc] init];
    self.carStyleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.carStyleLabel.text = @"擅长车型(*选填）";
    self.carStyleLabel.textColor = FWTextColor_12101D;
    self.carStyleLabel.textAlignment = NSTextAlignmentLeft;
    [self.fivethView addSubview:self.carStyleLabel];
    [self.carStyleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopDescLabel);
        make.size.mas_equalTo(CGSizeMake(200, 15));
        make.top.mas_equalTo(self.fivethView).mas_offset(10);
    }];
    
    self.carStyleField = [[IQTextView alloc] init];
    self.carStyleField.delegate = self;
    self.carStyleField.placeholder = @"车型之间以，隔开，如奔驰AMG，奥迪";
    self.carStyleField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.carStyleField.textColor = FWTextColor_12101D;
    [self.fivethView addSubview:self.carStyleField];
    [self.carStyleField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carStyleLabel);
        make.top.mas_equalTo(self.carStyleLabel.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-38, 45));
    }];
    
    self.carStyleLineView = [[UIView alloc] init];
    self.carStyleLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.fivethView addSubview:self.carStyleLineView];
    [self.carStyleLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.carStyleField);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.carStyleField.mas_bottom);
    }];
}

- (void)setupSixthView{
    
    self.sixthView = [[UIView alloc] init];
    self.sixthView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.sixthView];
    [self.sixthView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.mainView);
        make.top.mas_equalTo(self.fivethView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(160);
    }];
    
    self.picsLabel = [[UILabel alloc] init];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"店铺照片(*必填）" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:18/255.0 green:16/255.0 blue:29/255.0 alpha:1.0]}];
    [string addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:204/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]} range:NSMakeRange(4, 5)];
    self.picsLabel.attributedText = string;
    self.picsLabel.textAlignment = NSTextAlignmentLeft;
    [self.sixthView addSubview:self.picsLabel];
    [self.picsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopDescLabel);
        make.size.mas_equalTo(CGSizeMake(200, 15));
        make.top.mas_equalTo(self.sixthView).mas_offset(10);
    }];
    
    self.addPicsImageView = [[SZAddImage alloc] init];
    self.addPicsImageView.delegate = self;
    self.addPicsImageView.comeFrom = 4;
    self.addPicsImageView.clipsToBounds = YES;
    self.addPicsImageView.frame = CGRectMake(0, CGRectGetMaxY(self.picsLabel.frame)+10, SCREEN_WIDTH, 60);
    [self.sixthView addSubview:self.addPicsImageView];
    [self.addPicsImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.picsLabel.mas_bottom).mas_offset(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(110);
        make.left.right.mas_equalTo(self.sixthView);
        make.bottom.mas_equalTo(self.sixthView).mas_offset(-20);
    }];
    
    self.saveButton = [[UIButton alloc] init];
    self.saveButton.layer.cornerRadius = 2;
    self.saveButton.layer.masksToBounds = YES;
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.saveButton.backgroundColor = FWTextColor_222222;
    [self.saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [self.saveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.saveButton addTarget:self action:@selector(saveButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.saveButton];
    [self.saveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mainView).mas_offset(30);
        make.centerX.mas_equalTo(self.mainView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-60, 48));
        make.top.mas_equalTo(self.sixthView.mas_bottom).mas_offset(14);
        make.bottom.mas_equalTo(self.mainView).mas_offset(-90);
    }];
}

#pragma mark - > 配置参数
- (void)configViewForModel:(id)model{
    
    FWHomeShopListModel * shopInfo = (FWHomeShopListModel *)model;
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    //异步并列执行
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        for (FWBussinessImageModel * iamgeModel in shopInfo.imgs) {
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:iamgeModel.img_url]];
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                [self.showImageArray addObject:image];
            }
        }
        
        //跳回主队列执行
        dispatch_async(dispatch_get_main_queue(), ^{
            //在主队列中进行ui操作
            if (self.showImageArray.count > 0) {
                self.addPicsImageView.showImage = self.showImageArray;
            }
            [MBProgressHUD hideHUDForView:self animated:YES];
        });
    });
    
    if (!shopInfo.province_name) shopInfo.province_name = @"";
    
    if (!shopInfo.city_name) shopInfo.city_name = @"";
    
    if (!shopInfo.area_name) shopInfo.area_name = @"";
    
    self.descTextView.text = shopInfo.shop_desc?shopInfo.shop_desc:@"";
    self.arearField.text = [NSString stringWithFormat:@"%@%@%@",shopInfo.province_name,shopInfo.city_name,shopInfo.area_name];
    self.addressField.text = shopInfo.address?shopInfo.address:@"";
    self.phoneField.text = shopInfo.mobile?shopInfo.mobile:@"";
    self.carStyleField.text = shopInfo.main_cars?shopInfo.main_cars:@"";
    
    self.provice_id = shopInfo.province_id;
    self.city_id = shopInfo.city_id;
    self.arear_id = shopInfo.area_id;
    

    if (shopInfo.main_business_ids.count > 0) {
        
        NSInteger count = self.cerModel.list.count;
        if (count > 0 ) {
            for (int i = 0; i < count; i++) {
                FWBussinessCertificationListModel * listModel = self.cerModel.list[i];

                UIButton * selectButton = (UIButton *)[self viewWithTag:27777+i];
                for (int j = 0; j< shopInfo.main_business_ids.count ; j++) {
                    if ([listModel.main_business_id isEqualToString:shopInfo.main_business_ids[j]]) {
                        [self selectButtonOnClick:selectButton];
                    }
                }
            }
        }
    }
}
#pragma mark - > 主营业务
- (void)setupBussinessInfo:(id)model{
    
    self.cerModel = (FWBussinessCertificationModel *)model;
    
    NSInteger count = self.cerModel.list.count;
    if (count > 0 ) {
        for (UIView * view in self.containerView.subviews) {
            [view removeFromSuperview];
        }
        
        for (int i = 0; i < count; i++) {
            FWBussinessCertificationListModel * listModel = self.cerModel.list[i];
            listModel.isSelect = NO;
            
            int row = i/2;
            
            UIButton * selectButton = [[UIButton alloc] init];
            selectButton.selected = NO;
            selectButton.tag = 27777+i;
            selectButton.backgroundColor = [UIColor clearColor];
            [selectButton addTarget:self action:@selector(selectButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.containerView addSubview:selectButton];
            if (i % 2 == 0) {
                [selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(self.containerView);
                    make.top.mas_equalTo(10+35*row);
                    make.width.mas_equalTo((SCREEN_WIDTH-80)/2-10);
                    make.height.mas_equalTo(22);
                    if (i == count-1) {
                        make.bottom.mas_equalTo(self.containerView).mas_offset(-20);
                    }
                }];
            }else{
                [selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo((SCREEN_WIDTH-80)/2+10);
                    make.right.mas_equalTo(self.containerView);
                    make.top.mas_equalTo(10+35*row);
                    make.width.mas_greaterThanOrEqualTo(20);
                    make.height.mas_equalTo(22);
                    if (i == count-1) {
                        make.bottom.mas_equalTo(self.containerView).mas_offset(-20);
                    }
                }];
            }
            
            UIImageView * selectImageView = [[UIImageView alloc] init];
            selectImageView.image = [UIImage imageNamed:@"bussiness_unselect"];
            selectImageView.tag = 28888+i;
            [selectButton addSubview:selectImageView];
            [selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(20, 20));
                make.left.mas_equalTo(selectButton);
                make.centerY.mas_equalTo(selectButton);
            }];
            
            UILabel * bussinessLabel = [[UILabel alloc] init];
            bussinessLabel.text = listModel.name;
            bussinessLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
            bussinessLabel.textColor = FWTextColor_000000;
            bussinessLabel.textAlignment = NSTextAlignmentLeft;
            [selectButton addSubview:bussinessLabel];
            [bussinessLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(selectButton);
                make.height.mas_equalTo(20);
                make.width.mas_greaterThanOrEqualTo(30);
                make.right.mas_equalTo(selectButton);
                make.left.mas_equalTo(selectImageView.mas_right).mas_offset(10);
            }];
        }
    }
}

#pragma mark - > 主营业务点击事件
- (void)selectButtonOnClick:(UIButton *)sender{
 
    if (self.idArray.count >= 10){
        [[FWHudManager sharedManager] showErrorMessage:@"最多可选择10项业务哦" toController:self.vc];
        return;
    }
    
    /* 变换状态*/
    sender.selected = !sender.selected;
    
    NSInteger index = sender.tag - 27777;
    
    /* 找到对应imageview */
    NSInteger imageTag = 28888+index;
    UIImageView * imageView = (UIImageView *)[self viewWithTag:imageTag];
    
    FWBussinessCertificationListModel * listModel = self.cerModel.list[index];
    listModel.isSelect = sender.isSelected;
    
    if (sender.isSelected) {
        
        imageView.image = [UIImage imageNamed:@"bussiness_select"];
        
        if (![self.idArray containsObject:listModel.main_business_id]) {
            
            [self.idArray addObject:listModel.main_business_id];
        }
    }else{
        
        imageView.image = [UIImage imageNamed:@"bussiness_unselect"];
        
        if ([self.idArray containsObject:listModel.main_business_id]) {
            [self.idArray removeObject:listModel.main_business_id];
        }
    }
}

#pragma mark - > 保存
- (void)saveButtonOnClick{
    
    if ([self.shopInfoDelegate respondsToSelector:@selector(saveButtonClick)]) {
        [self.shopInfoDelegate saveButtonClick];
    }
}

#pragma mark - > pickerview数据源及代理方法
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}


#pragma mark 数据源  numberOfRowsInComponent
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (component==0){
        return self.ProvinceArr.count;
    }else if (component==1){
        return self.CityArr.count;
    }else{
        return self.AreaArr.count;
    }
}

#pragma delegate 显示信息方法
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component==0){
        if (self.ProvinceArr.count > row) {
            return ((FWArearListModel *)self.ProvinceArr[row]).name;
        }
    }else if (component==1){
        if (self.CityArr.count > row) {
            return ((FWArearListModel *)self.CityArr[row]).name;
        }
    }else{
        if (self.AreaArr.count > row) {
            return ((FWArearListModel *)self.AreaArr[row]).name;
        }
    }
    
    return @"";
}

#pragma mark 选中行信息
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component==0){
        
        self.currentCom = 0;
        
        if (self.ProvinceArr.count > row) {
            self.select_id = ((FWArearListModel *)self.ProvinceArr[row]).area_id;
            self.provice_id = self.select_id;
        }
        [self requestAreaData];

        if (self.AreaArr.count > 0) {
            [self.AreaArr removeAllObjects];
        }
        
        [pickerView reloadComponent:1];
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:1 animated:YES];
    }else if (component == 1){
       
        self.currentCom = 1;
        if (self.CityArr.count > row) {
            self.select_id = ((FWArearListModel *)self.CityArr[row]).area_id;
            self.city_id = self.select_id;
        }
        [self requestAreaData];
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }else{
        // 北京---1 天津---18 上海---791 重庆---2244
        if (self.AreaArr.count > row) {
            self.arear_id = ((FWArearListModel *)self.AreaArr[row]).area_id;
        }
    }
}

#pragma mark 显示行高
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.00;
}

#pragma mark - pickerView 每列宽度
- (CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    return SCREEN_WIDTH/3;
}

#pragma mark - > 选中
- (void)doneButtonClick:(UIBarButtonItem *)button{
    [self endEditing:YES];
    
    NSInteger onerow=[self.pickerView selectedRowInComponent:0];
    NSInteger tworow=[self.pickerView selectedRowInComponent:1];
    NSInteger threerow=[self.pickerView selectedRowInComponent:2];
    
    if (self.ProvinceArr.count > onerow) {
        self.address = ((FWArearListModel *)self.ProvinceArr[onerow]).name;
        self.provice_id = ((FWArearListModel *)self.ProvinceArr[onerow]).area_id;
    }
    
    if (self.CityArr.count > tworow) {
        self.address = [self.address stringByAppendingString:((FWArearListModel *)self.CityArr[tworow]).name];
        self.city_id = ((FWArearListModel *)self.CityArr[tworow]).area_id;
    }
    
    if (self.AreaArr.count > threerow) {
        self.address = [self.address stringByAppendingString:((FWArearListModel *)self.AreaArr[threerow]).name];
        self.arear_id = ((FWArearListModel *)self.AreaArr[threerow]).area_id;
    }
    
    self.arearField.text = self.address;

}

#pragma mark - > 取消
- (void)cancelButtonClick:(UIBarButtonItem *)button{
    [self endEditing:YES];
}

#pragma mark - > 获取地区
-(void)requestAreaData{
    
    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"pid":self.select_id,
                              };
    
//    if (MBProgressHUD) {
//        <#statements#>
//    }
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_area WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWArearModel * areaModel = [FWArearModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            if (self.currentCom == -1) {
                if (self.ProvinceArr.count > 0) {
                    [self.ProvinceArr removeAllObjects];
                }
             
                if (self.CityArr.count > 0) {
                    [self.CityArr removeAllObjects];
                }
                
                [self.ProvinceArr addObjectsFromArray:areaModel.list];
                [self.CityArr addObjectsFromArray:areaModel.list_beijing];
                
                [self.pickerView reloadComponent:0];
                [self.pickerView reloadComponent:1];
            }else if(self.currentCom == 0){
                
                if (self.CityArr.count > 0) {
                    [self.CityArr removeAllObjects];
                }
                
                [self.CityArr addObjectsFromArray:areaModel.list];
                [self.pickerView reloadComponent:1];
                
                [self requestThirdComponent];
            }else if(self.currentCom == 1){
                if (self.AreaArr.count > 0) {
                    [self.AreaArr removeAllObjects];
                }
                
                [self.AreaArr addObjectsFromArray:areaModel.list];
                [self.pickerView reloadComponent:2];
            }
        }
    }];
}

#pragma mark - > 每次省滚动后，自动取出市的第一行id，去请求第三列
- (void)requestThirdComponent{
    
    if (((FWArearListModel *)self.CityArr[0]).area_id <= 0) {
        return;
    }
    
    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"pid": ((FWArearListModel *)self.CityArr[0]).area_id,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_area WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWArearModel * areaModel = [FWArearModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (self.AreaArr.count > 0) {
                [self.AreaArr removeAllObjects];
            }
            
            [self.AreaArr addObjectsFromArray:areaModel.list];
            [self.pickerView reloadComponent:2];
        }
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@""]){
        // 允许删除
        return YES;
    }
    if (textView == self.phoneField) {
        if (textView.text.length >=11) {
            return NO;
        }
    }else if (textView == self.addressField) {
        if (textView.text.length >=30) {
            return NO;
        }
    }
    
    return YES;
}

@end
