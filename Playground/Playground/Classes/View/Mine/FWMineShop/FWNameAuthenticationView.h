//
//  FWNameAuthenticationView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWNameAuthenticationViewDelegate <NSObject>

- (void)uploadCardPhotoClick:(UIImage *)image;
- (void)commitButtonClick;

@end

@interface FWNameAuthenticationView : UIView<UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIView *upView;

@property (strong, nonatomic) UITextField *realNameTextField;
@property (strong, nonatomic) UIView *realNameLineView;
@property (strong, nonatomic) UILabel *realNameLabel;

@property (strong, nonatomic) UITextField *cardIDTextField;
@property (strong, nonatomic) UIView *cardIDLineView;
@property (strong, nonatomic) UILabel *cardIDLabel;

@property (strong, nonatomic) UITextField *phoneTextField;
@property (strong, nonatomic) UIView *phoneLineTextField;
@property (strong, nonatomic) UILabel *phoneLabel;


@property (strong, nonatomic) UIView *downView;
@property (strong, nonatomic) UILabel *tipLabel;
@property (strong, nonatomic) UILabel *tip1Label;
@property (strong, nonatomic) UILabel *tip2Label;
@property (strong, nonatomic) UILabel *tip3Label;

@property (strong, nonatomic) UIImageView *cankaoImageView;
@property (strong, nonatomic) UIImageView *uploadImageView;

@property (strong, nonatomic) UIButton *commitButton;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, weak) id<FWNameAuthenticationViewDelegate>delegate;


- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
