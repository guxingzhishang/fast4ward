//
//  FWUploadGoodsView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUploadGoodsView.h"

@implementation FWUploadGoodsView

- (void)setVc:(UIViewController *)vc{
    _vc = vc;
    
    self.addShowImageView.vc = self.vc;
    self.addDetailImageView.vc = self.vc;
}

- (void)setImages:(NSMutableArray *)images{
    _images = images;
    
    self.addShowImageView.showImage = self.images;
    self.addDetailImageView.showImage = self.images;
}

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.showLabel = [UILabel new];
    self.showLabel.text = @"商品展示图";
    self.showLabel.frame = CGRectMake(20, 20, 100, 20);
    self.showLabel.font = DHSystemFontOfSize_12;
    self.showLabel.textColor = FWTextColor_12101D;
    self.showLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.showLabel];
  
    
    self.showView = [[UIView alloc] init];
    self.showView.clipsToBounds = YES;
    self.showView.frame = CGRectMake(0, CGRectGetMaxY(self.showLabel.frame), SCREEN_WIDTH, 50);
    [self addSubview:self.showView];
    
    self.addShowImageView = [[SZAddImage alloc] init];
    self.addShowImageView.delegate = self;
    self.addShowImageView.comeFrom = 2;
    self.addShowImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetHeight(self.showView.frame));
    [self.showView addSubview:self.addShowImageView];
    
    self.lineOneView = [UIView new];
    self.lineOneView.frame = CGRectMake(CGRectGetMinX(self.showLabel.frame), CGRectGetMaxY(self.showView.frame)+31, SCREEN_WIDTH-CGRectGetMinX(self.showLabel.frame)*2, 1);
    self.lineOneView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:self.lineOneView];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.text = @"商品标题";
    self.titleLabel.frame = CGRectMake(CGRectGetMinX(self.lineOneView.frame), CGRectGetMaxY(self.lineOneView.frame)+10, 100, 20);
    self.titleLabel.font = DHSystemFontOfSize_12;
    self.titleLabel.textColor = FWTextColor_12101D;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.titleLabel];
    
    self.titleTF = [[UITextField alloc] init];
    self.titleTF.delegate = self;
    self.titleTF.placeholder = @"请输入商品标题，8到32字符";
    self.titleTF.textColor = FWTextColor_12101D;
    self.titleTF.frame = CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.titleLabel.frame), 44);
    [self addSubview:self.titleTF];
    
    self.lineTitleView =  [[UIView alloc] init];
    self.lineTitleView.frame = CGRectMake(CGRectGetMinX(self.showLabel.frame), CGRectGetMaxY(self.titleTF.frame), CGRectGetWidth(self.titleTF.frame), 1);
    self.lineTitleView.backgroundColor = FWTextColor_141414;
    [self addSubview:self.lineTitleView];
    
    self.shortTitleLabel = [UILabel new];
    self.shortTitleLabel.text = @"商品短标题";
    self.shortTitleLabel.frame = CGRectMake(CGRectGetMinX(self.lineTitleView.frame), CGRectGetMaxY(self.lineTitleView.frame)+5, 100, 20);
    self.shortTitleLabel.font = DHSystemFontOfSize_12;
    self.shortTitleLabel.textColor = FWTextColor_12101D;
    self.shortTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.shortTitleLabel];
    
    self.shortTitleTF = [[UITextField alloc] init];
    self.shortTitleTF.delegate = self;
    self.shortTitleTF.placeholder = @"请输入商品短标题，6到12字符";
    self.shortTitleTF.textColor = FWTextColor_12101D;
    self.shortTitleTF.frame = CGRectMake(CGRectGetMinX(self.shortTitleLabel.frame), CGRectGetMaxY(self.shortTitleLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.shortTitleLabel.frame), 44);
    [self addSubview:self.shortTitleTF];
    
    self.lineShortTitleView =  [[UIView alloc] init];
    self.lineShortTitleView.frame = CGRectMake(CGRectGetMinX(self.shortTitleTF.frame), CGRectGetMaxY(self.shortTitleTF.frame), CGRectGetWidth(self.shortTitleTF.frame), 1);
    self.lineShortTitleView.backgroundColor = FWTextColor_141414;
    [self addSubview:self.lineShortTitleView];
    
    self.priceLabel = [UILabel new];
    self.priceLabel.text = @"价格";
    self.priceLabel.frame = CGRectMake(CGRectGetMinX(self.lineShortTitleView.frame), CGRectGetMaxY(self.lineShortTitleView.frame)+5, 100, 20);
    self.priceLabel.font = DHSystemFontOfSize_12;
    self.priceLabel.textColor = FWTextColor_12101D;
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.priceLabel];
    
    self.priceTF = [[UITextField alloc] init];
    self.priceTF.delegate = self;
    self.priceTF.placeholder = @"请输入商品价格";
    self.priceTF.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.priceTF.textColor = FWTextColor_12101D;
    self.priceTF.frame = CGRectMake(CGRectGetMinX(self.priceLabel.frame), CGRectGetMaxY(self.priceLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.priceLabel.frame), 44);
    [self addSubview:self.priceTF];
    
    self.linePriceView =  [[UIView alloc] init];
    self.linePriceView.frame = CGRectMake(CGRectGetMinX(self.priceTF.frame), CGRectGetMaxY(self.priceTF.frame), CGRectGetWidth(self.priceTF.frame), 1);
    self.linePriceView.backgroundColor = FWTextColor_141414;
    [self addSubview:self.linePriceView];
    
    
    self.lineTwoView = [UIView new];
    self.lineTwoView.frame = CGRectMake(CGRectGetMinX(self.linePriceView.frame), CGRectGetMaxY(self.linePriceView.frame)+30,CGRectGetWidth(self.lineOneView.frame), 1);
    self.lineTwoView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:self.lineTwoView];
    
    self.goodsDetailLabel = [UILabel new];
    self.goodsDetailLabel.text = @"商品详情";
    self.goodsDetailLabel.frame = CGRectMake(CGRectGetMinX(self.lineTwoView.frame), CGRectGetMaxY(self.lineTwoView.frame)+20, 100, 20);
    self.goodsDetailLabel.font = DHSystemFontOfSize_12;
    self.goodsDetailLabel.textColor = FWTextColor_12101D;
    self.goodsDetailLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.goodsDetailLabel];
    
    self.goodsDetailView = [[UIView alloc] init];
    self.goodsDetailView.clipsToBounds = YES;
    self.goodsDetailView.frame = CGRectMake(0, CGRectGetMaxY(self.goodsDetailLabel.frame), SCREEN_WIDTH, 50);
    [self addSubview:self.goodsDetailView];
    
    self.addDetailImageView = [[SZAddImage alloc] init];
    self.addDetailImageView.delegate = self;
    self.addDetailImageView.comeFrom = 3;
    self.addDetailImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetHeight(self.showView.frame));
    [self.goodsDetailView addSubview:self.addDetailImageView];
    
    self.lineThreeView = [UIView new];
    self.lineThreeView.frame = CGRectMake(CGRectGetMinX(self.lineTwoView.frame), CGRectGetMaxY(self.goodsDetailView.frame)+30, CGRectGetWidth(self.lineTwoView.frame), 1);
    self.lineThreeView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:self.lineThreeView];
 
    self.channelLabel = [UILabel new];
    self.channelLabel.text = @"请选择商品购买渠道";
    self.channelLabel.frame = CGRectMake(CGRectGetMinX(self.lineThreeView.frame), CGRectGetMaxY(self.lineThreeView.frame)+20, 200, 20);
    self.channelLabel.font = DHSystemFontOfSize_12;
    self.channelLabel.textColor = FWTextColor_12101D;
    self.channelLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.channelLabel];
    
    self.segement = [[FWGoodsChannelSegementView alloc]init];
    self.segement.itemDelegate = self;
    self.segement.frame = CGRectMake(0, CGRectGetMaxY(self.channelLabel.frame)+10, SCREEN_WIDTH, 60);
    [self addSubview:self.segement];
    
    
    self.paramLabel = [UILabel new];
    self.paramLabel.text = @"商品参数";
    self.paramLabel.frame = CGRectMake(CGRectGetMinX(self.channelLabel.frame), CGRectGetMaxY(self.channelLabel.frame)+80, 100, 20);
    self.paramLabel.font = DHSystemFontOfSize_12;
    self.paramLabel.textColor = FWTextColor_12101D;
    self.paramLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.paramLabel];
    
    self.paramButton = [[UIButton alloc] init];
    self.paramButton.titleLabel.font = DHSystemFontOfSize_12;
    [self.paramButton setTitle:@"如何获取ID?" forState:UIControlStateNormal];
    [self.paramButton setTitleColor:FWTextColor_67769E forState:UIControlStateNormal];
    [self.paramButton addTarget:self action:@selector(paramButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.paramButton];
    self.paramButton.frame = CGRectMake(SCREEN_WIDTH-100,  CGRectGetMinY(self.paramLabel.frame)-10, 100, 30);
    
    
    self.paramTF = [[IQTextView alloc] init];
    self.paramTF.delegate = self;
    self.paramTF.placeholder = @"请输入商品ID";
    self.paramTF.keyboardType = UIKeyboardTypeNumberPad;
    self.paramTF.textColor = FWTextColor_12101D;
    self.paramTF.font = DHSystemFontOfSize_14;
    self.paramTF.contentInset = UIEdgeInsetsMake(5, 0, 0, 0);
    self.paramTF.frame = CGRectMake(CGRectGetMinX(self.paramLabel.frame), CGRectGetMaxY(self.paramLabel.frame)+5, SCREEN_WIDTH-2*CGRectGetMinX(self.paramLabel.frame), 44);
    [self addSubview:self.paramTF];
    
    self.lineParamView =  [[UIView alloc] init];
    self.lineParamView.frame = CGRectMake(CGRectGetMinX(self.paramTF.frame), CGRectGetMaxY(self.paramTF.frame), CGRectGetWidth(self.paramTF.frame), 1);
    self.lineParamView.backgroundColor = FWTextColor_141414;
    [self addSubview:self.lineParamView];
    
    
    self.addressLabel = [UILabel new];
    self.addressLabel.text = @"店铺地址";
    self.addressLabel.frame = CGRectMake(CGRectGetMinX(self.channelLabel.frame), CGRectGetMaxY(self.channelLabel.frame)+80, 100, 20);
    self.addressLabel.font = DHSystemFontOfSize_12;
    self.addressLabel.textColor = FWTextColor_12101D;
    self.addressLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.addressLabel];
    
    self.addressTF = [[IQTextView alloc] init];
    self.addressTF.delegate = self;
    self.addressTF.placeholder = @"请填写50字以内店铺地址，用户可到店购买";
    self.addressTF.textColor = FWTextColor_12101D;
    self.addressTF.font = DHSystemFontOfSize_14;
    self.addressTF.contentInset = UIEdgeInsetsMake(5, 0, 0, 0);
    self.addressTF.frame = CGRectMake(CGRectGetMinX(self.addressLabel.frame), CGRectGetMaxY(self.addressLabel.frame)+5, SCREEN_WIDTH-2*CGRectGetMinX(self.addressLabel.frame), 44);
    [self addSubview:self.addressTF];
    
    self.lineAddressView =  [[UIView alloc] init];
    self.lineAddressView.frame = CGRectMake(CGRectGetMinX(self.addressTF.frame), CGRectGetMaxY(self.addressTF.frame), CGRectGetWidth(self.addressTF.frame), 1);
    self.lineAddressView.backgroundColor = FWTextColor_141414;
    [self addSubview:self.lineAddressView];
    
    self.tipLabel = [UILabel new];
    self.tipLabel.text = @"*您的所有实体店商品均显示此地址，请慎重填写。";
    self.tipLabel.numberOfLines = 0;
    self.tipLabel.frame = CGRectMake(CGRectGetMinX(self.lineAddressView.frame), CGRectGetMaxY(self.lineAddressView.frame)+5, SCREEN_WIDTH-CGRectGetMinX(self.showLabel.frame)*2, 30);
    self.tipLabel.font = DHSystemFontOfSize_12;
    self.tipLabel.textColor = FWTextColor_12101D;
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.tipLabel];
    
    self.addressLabel.hidden = YES;
    self.addressTF.hidden = YES;
    self.lineAddressView.hidden = YES;
    self.tipLabel.hidden = YES;
    
    self.webLabel = [UILabel new];
    self.webLabel.text = @"网站地址";
    self.webLabel.frame = CGRectMake(CGRectGetMinX(self.channelLabel.frame), CGRectGetMaxY(self.channelLabel.frame)+80, 100, 20);
    self.webLabel.font = DHSystemFontOfSize_12;
    self.webLabel.textColor = FWTextColor_12101D;
    self.webLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.webLabel];
    
    self.webTF = [[IQTextView alloc] init];
    self.webTF.delegate = self;
    self.webTF.placeholder = @"请输入商品购买网址";
    self.webTF.textColor = FWTextColor_12101D;
    self.webTF.font = DHSystemFontOfSize_14;
    self.webTF.keyboardType = UIKeyboardTypeURL;
    self.webTF.contentInset = UIEdgeInsetsMake(5, 0, 0, 0);
    self.webTF.frame = CGRectMake(CGRectGetMinX(self.webLabel.frame), CGRectGetMaxY(self.webLabel.frame)+5, SCREEN_WIDTH-2*CGRectGetMinX(self.webLabel.frame), 44);
    [self addSubview:self.webTF];
    
    self.lineWebView =  [[UIView alloc] init];
    self.lineWebView.frame = CGRectMake(CGRectGetMinX(self.webTF.frame), CGRectGetMaxY(self.webTF.frame), CGRectGetWidth(self.webTF.frame), 1);
    self.lineWebView.backgroundColor = FWTextColor_141414;
    [self addSubview:self.lineWebView];
    
    self.webLabel.hidden = YES;
    self.webTF.hidden = YES;
    self.lineWebView.hidden = YES;
    
    self.uploadButton = [[UIButton alloc] init];
    self.uploadButton.layer.cornerRadius = 2;
    self.uploadButton.layer.masksToBounds = YES;
    self.uploadButton.titleLabel.font = DHSystemFontOfSize_13;
    self.uploadButton.backgroundColor = FWTextColor_222222;
    [self.uploadButton setTitle:@"上架" forState:UIControlStateNormal];
    [self.uploadButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.uploadButton addTarget:self action:@selector(uploadButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.uploadButton];
    self.uploadButton.frame = CGRectMake(28, CGRectGetMaxY(self.lineParamView.frame)+57, SCREEN_WIDTH-56, 50);
    
    [self.uploadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.lineParamView.mas_bottom).mas_offset(57);
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-56, 50));
        make.bottom.mas_equalTo(self).mas_offset(-50-FWSafeBottom);
    }];
    
    [self refreshFrame];
}

#pragma mark - > 设置渠道
- (void)setChannelMoudle:(FWGoodsChannelModel *)model{
    
    self.channelModel = model;
    [self.segement deliverTitles:self.channelModel.list_channel];
    /* 先给一个默认值 */
    self.channelListModel = self.channelModel.list_channel[0];
}

- (void)configForModel:(id)model{
    
    FWBussinessShopGoodsListModel * listModel = (FWBussinessShopGoodsListModel *)model;
    
    self.titleTF.text = listModel.title;
    self.shortTitleTF.text = listModel.title_short;
    self.priceTF.text = listModel.price;
    
    if ([listModel.channel isEqualToString:@"guanwang"]) {
        self.webTF.text = listModel.channel_val;
    }else{
        self.paramTF.text = listModel.channel_val;
    }

    self.addressTF.text = self.channelModel.shop_address;

    NSInteger index = 0;
    for (int i = 0; i < self.channelModel.list_channel.count; i ++) {
        
        FWGoodsChannelListModel * channel = self.channelModel.list_channel[i];
        
        if ([listModel.channel isEqualToString:channel.channel]) {
            index = i;
            break;
        }
    }
    
    [self.segement menuUpdateStyle:index];
    [self segementItemClickTap:index];
    self.segement.lastIndex = index;

    NSMutableArray * showImageArray = @[].mutableCopy;
    NSMutableArray * detailImageArray = @[].mutableCopy;

    //异步并列执行
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        for (FWBussinessImageModel * iamgeModel in listModel.imgs) {
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:iamgeModel.img_url]];
            UIImage *image = [UIImage imageWithData:data];
            [showImageArray addObject:image];
        }
        
        //跳回主队列执行
        dispatch_async(dispatch_get_main_queue(), ^{
            //在主队列中进行ui操作
            if (showImageArray.count > 0) {
                self.addShowImageView.showImage = showImageArray;
                [self refreshFrame];
            }
        });
    });
    
    //异步并列执行
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        for (FWBussinessImageModel * iamgeModel in listModel.detail) {
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:iamgeModel.img_url]];
            UIImage *image = [UIImage imageWithData:data];
            [detailImageArray addObject:image];
        }
        
        //跳回主队列执行
        dispatch_async(dispatch_get_main_queue(), ^{
            //在主队列中进行ui操作
            if (detailImageArray.count > 0) {
                self.addDetailImageView.showImage = detailImageArray;
                [self refreshFrame];
            }
        });
    });
    [self refreshFrame];
}


#pragma mark - > 点击推荐item
- (void)segementItemClickTap:(NSInteger)index{
    
    self.channelListModel = self.channelModel.list_channel[index];
    
    if ([self.channelListModel.channel_desc isEqualToString:@"实体店"]) {
        
        self.paramLabel.hidden = YES;
        self.paramButton.hidden = YES;
        self.paramTF.hidden = YES;
        self.lineParamView.hidden = YES;

        self.webLabel.hidden = YES;
        self.webTF.hidden = YES;
        self.lineWebView.hidden = YES;
        
        self.addressLabel.hidden = NO;
        self.addressTF.hidden = NO;
        self.lineAddressView.hidden = NO;
        self.tipLabel.hidden = NO;
        
        self.uploadButton.frame = CGRectMake(28, CGRectGetMaxY(self.tipLabel.frame)+57, SCREEN_WIDTH-56, 50);
        
        [self.uploadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(57);
            make.centerX.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-56, 50));
            make.bottom.mas_equalTo(self).mas_offset(-50-FWSafeBottom);
        }];
    }else if ([self.channelListModel.channel_desc isEqualToString:@"官网"]) {
        
        self.paramLabel.hidden = YES;
        self.paramButton.hidden = YES;
        self.paramTF.hidden = YES;
        self.lineParamView.hidden = YES;
        
        self.webLabel.hidden = NO;
        self.webTF.hidden = NO;
        self.lineWebView.hidden = NO;
        
        self.addressLabel.hidden = YES;
        self.addressTF.hidden = YES;
        self.lineAddressView.hidden = YES;
        self.tipLabel.hidden = YES;
        
        self.uploadButton.frame = CGRectMake(28, CGRectGetMaxY(self.lineWebView.frame)+57, SCREEN_WIDTH-56, 50);
        
        [self.uploadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.lineWebView.mas_bottom).mas_offset(57);
            make.centerX.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-56, 50));
            make.bottom.mas_equalTo(self).mas_offset(-50-FWSafeBottom);
        }];
    }else{
        
        self.paramLabel.hidden = NO;
        self.paramButton.hidden = NO;
        self.paramTF.hidden = NO;
        self.lineParamView.hidden = NO;
        
        self.webLabel.hidden = YES;
        self.webTF.hidden = YES;
        self.lineWebView.hidden = YES;
        
        self.addressLabel.hidden = YES;
        self.addressTF.hidden = YES;
        self.lineAddressView.hidden = YES;
        self.tipLabel.hidden = YES;
        
        self.uploadButton.frame = CGRectMake(28, CGRectGetMaxY(self.lineParamView.frame)+57, SCREEN_WIDTH-56, 50);
        
        [self.uploadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.lineParamView.mas_bottom).mas_offset(57);
            make.centerX.mas_equalTo(self);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-56, 50));
            make.bottom.mas_equalTo(self).mas_offset(-50-FWSafeBottom);
        }];
    }
}

#pragma mark - > 上架
- (void)uploadButtonOnClick{
    
    self.uploadButton.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       self.uploadButton.enabled = YES;
    });
    
    self.titleTF.text = [self checkString:self.titleTF.text];
    self.priceTF.text = [self checkString:self.priceTF.text];
    self.shortTitleTF.text = [self checkString:self.shortTitleTF.text];
    self.paramTF.text = [self checkString:self.paramTF.text];
    self.addressTF.text = [self checkString:self.addressTF.text];
    self.webTF.text = [self checkString:self.webTF.text];

    NSString * channel = self.channelListModel.channel;

    if (self.titleTF.text.length < 8 || self.titleTF.text.length > 32) {
        [self showHudWithTitle:@"请输入8到32位商品标题"];
    }else if (self.shortTitleTF.text.length < 6 ||self.shortTitleTF.text.length > 12) {
        [self showHudWithTitle:@"请输入6到12位商品短标题"];
    }else if (self.priceTF.text.length <= 0 ) {
        [self showHudWithTitle:@"请输入商品价格"];
    }else if ([self.priceTF.text floatValue] < 0 || [self.priceTF.text floatValue] > 100000000) {
        [self showHudWithTitle:@"商品必须在0元与1亿元之间"];
    }else if (self.channelListModel.channel.length <= 0 ) {
        [self showHudWithTitle:@"请选择商品购买渠道"];
    }else if ([self.addShowImageView getImagesArray].count <= 0) {
        [self showHudWithTitle:@"请上传至少一张商品图片"];
    }else if ([self.addDetailImageView getImagesArray].count <= 0) {
        [self showHudWithTitle:@"请上传至少一张商品详情图"];
    }else if([channel isEqualToString:@"shitidian"]){
        if (self.addressTF.text.length <= 0 || self.addressTF.text.length > 50) {
            [self showHudWithTitle:@"请填写50字以内店铺地址，用户可到店购买"];
        }else{
            if ([self.uplLoadDelegate respondsToSelector:@selector(uploadButtonClick)]) {
                [self.uplLoadDelegate uploadButtonClick];
            }
        }
    }else if([channel isEqualToString:@"guanwang"]){
        if (self.webTF.text.length <= 0) {
            [self showHudWithTitle:@"请填写商品购买网址"];
        }else if ([self.webTF.text hasPrefix:@"http://"] || [self.webTF.text hasPrefix:@"https://"]){
            if ([self.uplLoadDelegate respondsToSelector:@selector(uploadButtonClick)]) {
                [self.uplLoadDelegate uploadButtonClick];
            }
        }else{
            [self showHudWithTitle:@"网址格式有误，请认真核对哦"];
        }
    }else if(![channel isEqualToString:@"shitidian"] &&![channel isEqualToString:@"guanwang"]){
        if (self.paramTF.text.length <= 0) {
            [self showHudWithTitle:@"请填写商品参数，否则不可跳转购买"];
        }else if ([self.paramTF.text floatValue] < 0){
            [self showHudWithTitle:@"请填写商品参数，否则不可跳转购买"];
        }else{
            if ([self.uplLoadDelegate respondsToSelector:@selector(uploadButtonClick)]) {
                [self.uplLoadDelegate uploadButtonClick];
            }
        }
    }
}

- (void)showHudWithTitle:(NSString *)title{
    
    [[FWHudManager sharedManager] showErrorMessage:title toController:self.vc];
    return;
}

#pragma mark - > 去掉前后空格
- (NSString *)checkString:(NSString *)string{
    
    NSCharacterSet  *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    string = [string stringByTrimmingCharactersInSet:set];
    return string;
}

#pragma mark - > 如何获取ID
- (void)paramButtonOnClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.htmlStr = self.channelModel.h5_get_id;
    WVC.pageType = WebViewTypeURL;
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 增加或删除了图片
-(void)operationPicture{
    
    [self refreshFrame];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.priceTF]) {
        
        NSMutableString *futureString = [NSMutableString stringWithString:textField.text];
        [futureString insertString:string atIndex:range.location];
        
        NSInteger flag = 0;
        // 这个可以自定义,保留到小数点后两位,后几位都可以
        const NSInteger limited = 2;
        
        for (NSInteger i = futureString.length - 1; i >= 0; i--) {
            
            if ([futureString characterAtIndex:i] == '.') {
                // 如果大于了限制的就提示
                if (flag > limited) {
                    
                    return NO;
                }
                break;
            }
            flag++;
        }
    }
    
    return YES;
}

#pragma mark - > 重新计算布局
- (void)refreshFrame{
    
    [self layoutIfNeeded];
    
    NSInteger showImageCount = [self.addShowImageView getImagesArray].count<9?[self.addShowImageView getImagesArray].count+1:9;
    NSInteger detailImageCount = [self.addDetailImageView getImagesArray].count<6?[self.addDetailImageView getImagesArray].count+1:6;
    
    CGFloat imageW = (SCREEN_WIDTH-60)/3;
    CGFloat marginY = 20;
    
    NSInteger showY = (showImageCount-1)/3+1;
    NSInteger detailY = (detailImageCount-1)/3+1;

    self.addShowImageView.frame =  CGRectMake(0, 0, SCREEN_WIDTH, showY*(imageW  + marginY));
    self.showView.frame = CGRectMake(0, CGRectGetMaxY(self.showLabel.frame), SCREEN_WIDTH, showY*(imageW  + marginY));
    
    self.lineOneView.frame = CGRectMake(CGRectGetMinX(self.showLabel.frame), CGRectGetMaxY(self.showView.frame)+31, SCREEN_WIDTH-CGRectGetMinX(self.showLabel.frame)*2, 1);

    self.titleLabel.frame = CGRectMake(CGRectGetMinX(self.lineOneView.frame), CGRectGetMaxY(self.lineOneView.frame)+10, 100, 20);
    
    self.titleTF.frame = CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.titleLabel.frame), 44);
    
    self.lineTitleView.frame = CGRectMake(CGRectGetMinX(self.showLabel.frame), CGRectGetMaxY(self.titleTF.frame), CGRectGetWidth(self.titleTF.frame), 1);
    
    self.shortTitleLabel.frame = CGRectMake(CGRectGetMinX(self.lineTitleView.frame), CGRectGetMaxY(self.lineTitleView.frame)+5, 100, 20);
    
    self.shortTitleTF.frame = CGRectMake(CGRectGetMinX(self.shortTitleLabel.frame), CGRectGetMaxY(self.shortTitleLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.shortTitleLabel.frame), 44);
    
    self.lineShortTitleView.frame = CGRectMake(CGRectGetMinX(self.shortTitleTF.frame), CGRectGetMaxY(self.shortTitleTF.frame), CGRectGetWidth(self.shortTitleTF.frame), 1);
    
    self.priceLabel.frame = CGRectMake(CGRectGetMinX(self.lineShortTitleView.frame), CGRectGetMaxY(self.lineShortTitleView.frame)+5, 100, 20);
    
    self.priceTF.frame = CGRectMake(CGRectGetMinX(self.priceLabel.frame), CGRectGetMaxY(self.priceLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.priceLabel.frame), 44);
    
    self.linePriceView.frame = CGRectMake(CGRectGetMinX(self.priceTF.frame), CGRectGetMaxY(self.priceTF.frame), CGRectGetWidth(self.priceTF.frame), 1);
    
    self.lineTwoView.frame = CGRectMake(CGRectGetMinX(self.linePriceView.frame), CGRectGetMaxY(self.linePriceView.frame)+30,CGRectGetWidth(self.lineOneView.frame), 1);
    
    self.goodsDetailLabel.frame = CGRectMake(CGRectGetMinX(self.lineTwoView.frame), CGRectGetMaxY(self.lineTwoView.frame)+20, 100, 20);
    
    self.addDetailImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, detailY*(imageW  + marginY));
    self.goodsDetailView.frame = CGRectMake(0, CGRectGetMaxY(self.goodsDetailLabel.frame), SCREEN_WIDTH, detailY*(imageW  + marginY));

    self.lineThreeView.frame = CGRectMake(CGRectGetMinX(self.lineTwoView.frame), CGRectGetMaxY(self.goodsDetailView.frame)+30, CGRectGetWidth(self.lineTwoView.frame), 1);
    
    self.channelLabel.frame = CGRectMake(CGRectGetMinX(self.lineThreeView.frame), CGRectGetMaxY(self.lineThreeView.frame)+20, 200, 20);
    
    self.segement.frame = CGRectMake(0, CGRectGetMaxY(self.channelLabel.frame)+10, SCREEN_WIDTH, 60);
    
    self.paramLabel.frame = CGRectMake(CGRectGetMinX(self.channelLabel.frame), CGRectGetMaxY(self.channelLabel.frame)+80, 100, 20);
    
    self.paramButton.frame = CGRectMake(SCREEN_WIDTH-100,  CGRectGetMinY(self.paramLabel.frame)-10, 100, 30);
    
    self.paramTF.frame = CGRectMake(CGRectGetMinX(self.paramLabel.frame), CGRectGetMaxY(self.paramLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.paramLabel.frame), 44);
    
    self.lineParamView.frame = CGRectMake(CGRectGetMinX(self.paramTF.frame), CGRectGetMaxY(self.paramTF.frame), CGRectGetWidth(self.paramTF.frame), 1);
    
    self.addressLabel.frame = CGRectMake(CGRectGetMinX(self.channelLabel.frame), CGRectGetMaxY(self.channelLabel.frame)+80, 100, 20);
    
    self.addressTF.frame = CGRectMake(CGRectGetMinX(self.addressLabel.frame), CGRectGetMaxY(self.addressLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.addressLabel.frame), 44);
    
    self.lineAddressView.frame = CGRectMake(CGRectGetMinX(self.addressTF.frame), CGRectGetMaxY(self.addressTF.frame), CGRectGetWidth(self.addressTF.frame), 1);
    
    self.tipLabel.frame = CGRectMake(CGRectGetMinX(self.lineAddressView.frame), CGRectGetMaxY(self.lineAddressView.frame)+5, SCREEN_WIDTH-CGRectGetMinX(self.showLabel.frame)*2, 30);
    
    self.webLabel.frame = CGRectMake(CGRectGetMinX(self.channelLabel.frame), CGRectGetMaxY(self.channelLabel.frame)+80, 100, 20);
    
    self.webTF.frame = CGRectMake(CGRectGetMinX(self.webLabel.frame), CGRectGetMaxY(self.webLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.webLabel.frame), 44);
    
    self.lineWebView.frame = CGRectMake(CGRectGetMinX(self.webTF.frame), CGRectGetMaxY(self.webTF.frame), CGRectGetWidth(self.webTF.frame), 1);
    
    self.uploadButton.frame = CGRectMake(28, CGRectGetMaxY(self.tipLabel.frame)+57, SCREEN_WIDTH-56, 50);
    
    [self.uploadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(57);
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-56, 50));
        make.bottom.mas_equalTo(self).mas_offset(-50-FWSafeBottom);
    }];
}

@end
