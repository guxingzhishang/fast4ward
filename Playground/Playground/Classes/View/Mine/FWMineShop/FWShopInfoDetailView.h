//
//  FWShopInfoDetailView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWShopInfoDetailView : UIScrollView<SDCycleScrollViewDelegate>

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIView * firstView;
/* 轮播图 */
@property (nonatomic, strong) SDCycleScrollView * bannerView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * descLabel;

@property (nonatomic, strong) UIView * secondView;
@property (nonatomic, strong) UILabel * lianxirenLabel;
@property (nonatomic, strong) UILabel * lianxidianhuaLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * authenticationView;
@property (nonatomic, strong) UIImageView * rightOneImageView ;
@property (nonatomic, strong) UIButton * memberButton;
@property (nonatomic, strong) UIButton * lianxirenButton;
@property (nonatomic, strong) UILabel * phoneLabel;
@property (nonatomic, strong) UIImageView * rightTwoImageView;
@property (nonatomic, strong) UIView * secondLineView;

@property (nonatomic, strong) UIView * thirdView;
@property (nonatomic, strong) FWHomeShopListModel * shopInfo;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, assign) BOOL  isUserPage;


- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
