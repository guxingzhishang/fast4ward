//
//  FWGoodsDetailView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWGoodsDetailView : UIScrollView<SDCycleScrollViewDelegate>

@property (nonatomic, strong) UIView * topView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) SDCycleScrollView * bannerView;
@property (nonatomic, strong) UILabel * countLabel;
@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UIImageView * scanImageView;
@property (nonatomic, strong) UILabel * scanLabel;
@property (nonatomic, strong) UIView * lineOneView;
@property (nonatomic, strong) UILabel * titleLabel;

@property (nonatomic, strong) UIView * shopView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * dongjiaImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * goodsLabel;
@property (nonatomic, strong) UIButton * goShopButton;

@property (nonatomic, strong) UIView * detailView;
@property (nonatomic, strong) UILabel * shopDetailLabel;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWBussinessShopGoodsListModel * listModel;
@property (nonatomic, assign) NSInteger  bannerImageCount;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
