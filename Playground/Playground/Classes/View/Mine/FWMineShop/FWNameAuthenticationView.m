//
//  FWNameAuthenticationView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWNameAuthenticationView.h"
#import "FWBussinessHomePageModel.h"

@implementation FWNameAuthenticationView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_F1F1F1;
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews{

    self.upView = [[UIView alloc] init];
    self.upView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.upView];
    [self.upView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(240);
        make.top.left.right.mas_equalTo(self);
    }];
    
    self.realNameLabel = [[UILabel alloc] init];
    self.realNameLabel.text = @"真实姓名";
    self.realNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.realNameLabel.textColor = FWTextColor_12101D;
    self.realNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.upView addSubview:self.realNameLabel];
    [self.realNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.upView).mas_offset(15);
        make.left.mas_equalTo(self.upView).mas_offset(21);
        make.right.mas_equalTo(self.upView).mas_offset(-21);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(SCREEN_WIDTH-42);
    }];
  
    self.realNameTextField = [[UITextField alloc] init];
    self.realNameTextField.delegate = self;
    self.realNameTextField.placeholder = @"请输入姓名";
    self.realNameTextField.textColor = FWTextColor_12101D;
    self.realNameTextField.borderStyle = UITextBorderStyleNone;
    self.realNameTextField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.upView addSubview:self.realNameTextField];
    [self.realNameTextField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.realNameLabel);
        make.height.mas_equalTo(40);
        make.top.mas_equalTo(self.realNameLabel.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(self.realNameLabel);
    }];
    
    self.realNameLineView = [[UIView alloc] init];
    self.realNameLineView.backgroundColor = FWTextColor_141414;
    [self.upView addSubview:self.realNameLineView];
    [self.realNameLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.realNameLabel);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.realNameTextField.mas_bottom).mas_offset(0.5);
        make.width.mas_equalTo(self.realNameLabel);
    }];
    
    
    self.cardIDLabel = [[UILabel alloc] init];
    self.cardIDLabel.text = @"身份证号";
    self.cardIDLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.cardIDLabel.textColor = FWTextColor_12101D;
    self.cardIDLabel.textAlignment = NSTextAlignmentLeft;
    [self.upView addSubview:self.cardIDLabel];
    [self.cardIDLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.realNameLineView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.realNameLabel);
        make.right.mas_equalTo(self.realNameLabel);
        make.height.mas_equalTo(self.realNameLabel);
        make.width.mas_equalTo(self.realNameLabel);
    }];
    
    self.cardIDTextField = [[UITextField alloc] init];
    self.cardIDTextField.delegate = self;
    self.cardIDTextField.placeholder = @"请输入身份证号码";
    self.cardIDTextField.textColor = FWTextColor_12101D;
    self.cardIDTextField.borderStyle = UITextBorderStyleNone;
    self.cardIDTextField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.upView addSubview:self.cardIDTextField];
    [self.cardIDTextField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.realNameTextField);
        make.height.mas_equalTo(self.realNameTextField);
        make.top.mas_equalTo(self.cardIDLabel.mas_bottom).mas_offset(5);
        make.width.mas_equalTo(self.realNameTextField);
    }];
    
    self.cardIDLineView = [[UIView alloc] init];
    self.cardIDLineView.backgroundColor = FWTextColor_141414;
    [self.upView addSubview:self.cardIDLineView];
    [self.cardIDLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.realNameLabel);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.cardIDTextField.mas_bottom).mas_offset(0.5);
        make.width.mas_equalTo(self.realNameLabel);
    }];
    
    self.phoneLabel = [[UILabel alloc] init];
    self.phoneLabel.text = @"手机号码";
    self.phoneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.phoneLabel.textColor = FWTextColor_12101D;
    self.phoneLabel.textAlignment = NSTextAlignmentLeft;
    [self.upView addSubview:self.phoneLabel];
    [self.phoneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.cardIDLineView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.realNameLabel);
        make.right.mas_equalTo(self.realNameLabel);
        make.height.mas_equalTo(self.realNameLabel);
        make.width.mas_equalTo(self.realNameLabel);
    }];
    
    self.phoneTextField = [[UITextField alloc] init];
    self.phoneTextField.delegate = self;
    self.phoneTextField.placeholder = @"请输入手机号码";
    self.phoneTextField.textColor = FWTextColor_12101D;
    self.phoneTextField.borderStyle = UITextBorderStyleNone;
    self.phoneTextField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.upView addSubview:self.phoneTextField];
    [self.phoneTextField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.realNameTextField);
        make.height.mas_equalTo(self.realNameTextField);
        make.top.mas_equalTo(self.phoneLabel.mas_bottom).mas_offset(5);
        make.width.mas_equalTo(self.realNameTextField);
    }];
    
    self.cardIDLineView = [[UIView alloc] init];
    self.cardIDLineView.backgroundColor = FWTextColor_141414;
    [self.upView addSubview:self.cardIDLineView];
    [self.cardIDLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.realNameLabel);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.phoneTextField.mas_bottom).mas_offset(0.5);
        make.width.mas_equalTo(self.realNameLabel);
        make.bottom.mas_equalTo(self.upView).mas_offset(-18);
    }];
    
    self.downView = [[UIView alloc] init];
    self.downView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.downView];
    [self.downView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(234);
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.upView.mas_bottom).mas_offset(10);
    }];
    
    self.tip1Label = [[UILabel alloc] init];
    self.tip1Label.text = @"身份证照片";
    self.tip1Label.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.tip1Label.textColor = FWTextColor_12101D;
    self.tip1Label.textAlignment = NSTextAlignmentLeft;
    [self.downView addSubview:self.tip1Label];
    [self.tip1Label mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.downView).mas_offset(13);
        make.left.mas_equalTo(self.downView).mas_offset(21);
        make.right.mas_equalTo(self.downView).mas_offset(-21);
        make.height.mas_equalTo(self.realNameLabel);
        make.width.mas_equalTo(self.realNameLabel);
    }];
    
    self.tip2Label = [[UILabel alloc] init];
    self.tip2Label.text = @"参考图片";
    self.tip2Label.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.tip2Label.textColor = FWTextColor_C4C4C4;
    self.tip2Label.textAlignment = NSTextAlignmentLeft;
    [self.downView addSubview:self.tip2Label];
    [self.tip2Label mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tip1Label.mas_bottom).mas_offset(15);
        make.left.mas_equalTo(self.tip1Label);
        make.height.mas_equalTo(self.realNameLabel);
        make.width.mas_equalTo(100);
    }];
    
    self.cankaoImageView = [[UIImageView alloc] init];
    self.cankaoImageView.image = [UIImage imageNamed:@"cankao_card"];
    self.cankaoImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.downView addSubview:self.cankaoImageView];
    [self.cankaoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tip2Label.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.downView).mas_offset(21);
        make.width.mas_equalTo((SCREEN_WIDTH-42-10)/2);
        make.height.mas_equalTo(120*((SCREEN_WIDTH-42-10)/2)/163);
    }];
    
    self.uploadImageView = [[UIImageView alloc] init];
    self.uploadImageView.image = [UIImage imageNamed:@"upload_card"];
    self.uploadImageView.userInteractionEnabled = YES;
    self.uploadImageView.contentMode = UIViewContentModeScaleToFill;
    [self.downView addSubview:self.uploadImageView];
    [self.uploadImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.cankaoImageView);
        make.left.mas_equalTo(self.cankaoImageView.mas_right).mas_offset(10);
        make.width.mas_equalTo(self.cankaoImageView);
        make.height.mas_equalTo(self.cankaoImageView);
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadCardPhoto)];
    [self.uploadImageView addGestureRecognizer:tap];
    
    self.tip3Label = [[UILabel alloc] init];
    self.tip3Label.text = @"你的身份证";
    self.tip3Label.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.tip3Label.textColor = FWTextColor_C4C4C4;
    self.tip3Label.textAlignment = NSTextAlignmentLeft;
    [self.downView addSubview:self.tip3Label];
    [self.tip3Label mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.tip2Label);
        make.left.mas_equalTo(self.uploadImageView);
        make.height.mas_equalTo(self.tip2Label);
        make.width.mas_equalTo(100);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"*请本人手持身份证，保证身份证正面信息清晰可见";
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.tipLabel.textColor = FWTextColor_C4C4C4;
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.downView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.cankaoImageView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.cankaoImageView);
        make.height.mas_equalTo(self.tip2Label);
        make.width.mas_equalTo(SCREEN_WIDTH-42);
    }];
    
    self.commitButton = [[UIButton alloc] init];
    self.commitButton.layer.cornerRadius = 2;
    self.commitButton.layer.masksToBounds = YES;
    self.commitButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.commitButton.backgroundColor = FWTextColor_222222;
    [self.commitButton setTitle:@"提交审核" forState:UIControlStateNormal];
    [self.commitButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.commitButton addTarget:self action:@selector(commitButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.commitButton];
    [self.commitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.downView.mas_bottom).mas_offset(14);
        make.left.mas_equalTo(self).mas_offset(30);
        make.height.mas_equalTo(48);
        make.width.mas_equalTo(SCREEN_WIDTH-60);
    }];
}

- (void)configForView:(id)model{
    
    FWBussinessHomePageModel * shopModel = (FWBussinessHomePageModel *)model;
    self.realNameTextField.text = shopModel.real_name;
    self.cardIDTextField.text = shopModel.idcard;
    self.phoneTextField.text = shopModel.mobile;
}

#pragma mark - > 上传身份证照片
- (void)uploadCardPhoto{

    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *_Nonnull action) {
        
    }];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"打开相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        [self.vc presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    
    UIAlertAction *picture = [UIAlertAction actionWithTitle:@"打开相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if (@available(iOS 11, *)) {
            UIScrollView.appearance.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
        }
        
        UIImagePickerController *pickerImage = [[UIImagePickerController alloc] init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            pickerImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            pickerImage.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:pickerImage.sourceType];
        }
        pickerImage.delegate = self;
        pickerImage.allowsEditing = NO;
        pickerImage.navigationController.navigationBar.hidden = NO;
        pickerImage.fd_prefersNavigationBarHidden = NO;
        
        [self.vc presentViewController:pickerImage animated:YES completion:nil];
    }];
    [alertVc addAction:cancle];
    [alertVc addAction:camera];
    [alertVc addAction:picture];
    [self.vc presentViewController:alertVc animated:YES completion:nil];
}

#pragma mark - >  选择图片
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"]){
        
        UIImage* image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        //加在视图中
        self.uploadImageView.image = image;
        
        if ([self.delegate respondsToSelector:@selector(uploadCardPhotoClick:)]) {
            [self.delegate uploadCardPhotoClick:image];
        }
    }
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([UIDevice currentDevice].systemVersion.floatValue < 11) {
        return;
    }
    if ([viewController isKindOfClass:NSClassFromString(@"PUPhotoPickerHostViewController")]) {
        [viewController.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.frame.size.width < 42) {
                [viewController.view sendSubviewToBack:obj];
                *stop = YES;
            }
        }];
    }
}

#pragma mark - >  取消选取图片
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - > 提交审核
- (void)commitButtonOnClick{
    
    self.commitButton.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       self.commitButton.enabled = YES;
    });
    
    if ([self checkString:self.realNameTextField.text].length <= 0 ) {
        [[FWHudManager sharedManager] showErrorMessage:@"请确保真实姓名无误" toController:self.vc];
        return;
    }
    
    if ([self checkString:self.cardIDTextField.text].length != 18 ) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入18位身份证号" toController:self.vc];
        return;
    }
    
    if ([self checkString:self.phoneTextField.text].length != 11 ) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入11位手机号" toController:self.vc];
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(commitButtonClick)]) {
        [self.delegate commitButtonClick];
    }
}

#pragma mark - > 去掉前后空格
- (NSString *)checkString:(NSString *)string{
    
    NSCharacterSet  *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    string = [string stringByTrimmingCharactersInSet:set];
    return string;
}

@end
