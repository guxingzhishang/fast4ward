//
//  FWBussinessShopHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessShopHeaderView.h"
#import "FWShopInfoViewController.h"
#import "FWShopInfoDetailViewController.h"

@implementation FWBussinessShopHeaderView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_EEEEEE;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.topImageView = [[UIImageView alloc] init];
    self.topImageView.userInteractionEnabled = YES;
    self.topImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topImageView.clipsToBounds = YES;
    self.topImageView.backgroundColor = FWTextColor_646464;
    [self addSubview:self.topImageView];
    [self.topImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(177);
    }];
    
    self.backButton = [[UIButton alloc] initWithFrame: CGRectMake(0,25+FWCustomeSafeTop,45,30)];
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.backButton];
    
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.topImageView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.topImageView);
        make.centerY.mas_equalTo(self.backButton);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(100);
    }];
    
    CGFloat photoBGW = 65;
    self.photoBgImageView = [[UIImageView alloc] init];
    self.photoBgImageView.userInteractionEnabled = YES;
    self.photoBgImageView.image = [UIImage imageNamed:@"photo_quan"];
    [self.topImageView addSubview:self.photoBgImageView];
    [self.photoBgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topImageView).mas_offset(10);
        make.bottom.mas_equalTo(self.topImageView).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(photoBGW, photoBGW));
    }];
    
    CGFloat photoW = 57;
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.layer.cornerRadius = photoW/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder"];
    [self.photoBgImageView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.photoBgImageView);
        make.centerY.mas_equalTo(self.photoBgImageView);
        make.size.mas_equalTo(CGSizeMake(photoW, photoW));
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoClick)];
    [self.photoImageView addGestureRecognizer:tap];
    
    
    self.dongjiaImageView = [[UIImageView alloc] init];
    self.dongjiaImageView.image = [UIImage imageNamed:@"bussiness_renzheng"];
    [self.topImageView addSubview:self.dongjiaImageView];
    [self.dongjiaImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.photoImageView);
        make.bottom.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(55, 15));
    }];
    
   
    self.addGoodsButton = [[UIButton alloc] init];
    self.addGoodsButton.layer.cornerRadius = 2;
    self.addGoodsButton.layer.masksToBounds = YES;
    self.addGoodsButton.titleLabel.font = DHSystemFontOfSize_13;
    self.addGoodsButton.backgroundColor = FWTextColor_222222;
    [self.addGoodsButton setTitle:@"添加商品" forState:UIControlStateNormal];
    [self.addGoodsButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.addGoodsButton addTarget:self action:@selector(addGoodsButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.addGoodsButton];
    [self.addGoodsButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.topImageView).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(91, 31));
    }];

    self.downButton = [[UIButton alloc] init];
    self.downButton.selected = NO;
    self.downButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.downButton setTitle:@"已下架" forState:UIControlStateNormal];
    [self.downButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
    [self.downButton setTitleColor:FWTextColor_67769E forState:UIControlStateSelected];
    [self.downButton addTarget:self action:@selector(downButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.downButton];
    [self.downButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topImageView.mas_bottom);
        make.right.mas_equalTo(self.topImageView).mas_offset(-20);
        make.height.mas_equalTo(30);
        make.width.mas_greaterThanOrEqualTo(50);
    }];
    self.downButton.hidden = YES;
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = DHBoldSystemFontOfSize_16;
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.topImageView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(15);
        make.top.mas_equalTo(self.photoImageView).mas_offset(10);
        make.height.mas_equalTo(22);
        make.width.mas_greaterThanOrEqualTo(100);
        make.right.mas_equalTo(self.downButton.mas_left).mas_offset(-15);
    }];
    
    self.goodsNumLabel = [[UILabel alloc] init];
    self.goodsNumLabel.font = DHSystemFontOfSize_12;
    self.goodsNumLabel.textColor = FWTextColor_B6BCC4;
    self.goodsNumLabel.userInteractionEnabled = YES;
    self.goodsNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.topImageView addSubview:self.goodsNumLabel];
    [self.goodsNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(self.nameLabel);
    }];
    UITapGestureRecognizer * tapNumber = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goodsNumLabelClick)];
    [self.goodsNumLabel addGestureRecognizer:tapNumber];
    
    self.uploadButton = [[UIButton alloc] init];
    self.uploadButton.selected = YES;
    self.uploadButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.uploadButton setTitle:@"上架中" forState:UIControlStateNormal];
    [self.uploadButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
    [self.uploadButton setTitleColor:FWTextColor_67769E forState:UIControlStateSelected];
    [self.uploadButton addTarget:self action:@selector(uploadButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.uploadButton];
    [self.uploadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.downButton);
        make.right.mas_equalTo(self.downButton.mas_left).mas_offset(-5);
        make.height.mas_equalTo(self.downButton);
        make.width.mas_greaterThanOrEqualTo(50);
    }];
    self.uploadButton.hidden = YES;
}

- (void)setIsBussiness:(BOOL)isBussiness{
    _isBussiness = isBussiness;
    if (_isBussiness) {

        self.uploadButton.hidden = NO;
        self.downButton.hidden = NO;
        
        self.titleLabel.text = @"我的店铺";
        [self.addGoodsButton setTitle:@"添加商品" forState:UIControlStateNormal];
    }else{
        self.titleLabel.text = @"";
        [self.addGoodsButton setTitle:@"店铺信息" forState:UIControlStateNormal];
    }
}

- (void)setIsUserPage:(BOOL)isUserPage{
    _isUserPage = isUserPage;
}

- (void)configForHeader:(id)model{
    
    self.shopModel = (FWBussinessShopModel *)model;
    
    self.nameLabel.text = self.shopModel.user_info.nickname;
    
    if (_isBussiness) {
        self.goodsNumLabel.text = @"编辑商家信息 >";
        self.goodsNumLabel.font = DHSystemFontOfSize_12;
        self.goodsNumLabel.textColor = FWViewBackgroundColor_FFFFFF;
    }else{
        self.goodsNumLabel.font = DHSystemFontOfSize_12;
        self.goodsNumLabel.textColor = FWTextColor_B6BCC4;
        self.goodsNumLabel.text = [NSString stringWithFormat:@"全部商品：%@",self.shopModel.total_count_all];
    }
    
    [self.downButton setTitle:[NSString stringWithFormat:@"已下架(%@)",self.shopModel.total_count_off] forState:UIControlStateNormal];
    [self.uploadButton setTitle:[NSString stringWithFormat:@"上架中(%@)",self.shopModel.total_count_on] forState:UIControlStateNormal];

    
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:self.shopModel.user_info.shop_bg]  placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.shopModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

#pragma mark - > 点击头像(商家页面不可点击，客户页面可点击)
- (void)photoClick{

    if (!self.isBussiness) {
        
        if (self.isUserPage) {
            /* 好友页 */
            [self.vc.navigationController popViewControllerAnimated:YES];
        }else{
            /* 其他页 */
            FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
            UIVC.user_id = self.shopModel.user_info.uid;
            [self.vc.navigationController pushViewController:UIVC animated:YES];
        }
    }
}

#pragma mark - > 编辑信息
- (void)goodsNumLabelClick{
    
    if (_isBussiness) {
        FWShopInfoViewController * SIVC = [[FWShopInfoViewController alloc] init];
        SIVC.shopInfoModel = self.shopModel.shop_info;
        SIVC.oprationType = EditGoods;
        [self.vc.navigationController pushViewController:SIVC animated:YES];
    }
}

#pragma mark - > 添加商品
- (void)addGoodsButtonOnClick{
    
    if (_isBussiness) {
        FWUploadGoodsViewController * UGVC = [[FWUploadGoodsViewController alloc] init];
        UGVC.oprationType = AddGoods;
        [self.vc.navigationController pushViewController:UGVC animated:YES];
    }else{
        // 店铺信息
        FWShopInfoDetailViewController * SIDVC = [[FWShopInfoDetailViewController alloc] init];
        SIDVC.user_id = self.shopModel.user_info.uid;
        [self.vc.navigationController pushViewController:SIDVC animated:YES];
    }
    
}

#pragma mark - > 已上架
- (void)uploadButtonOnClick{
    
    self.uploadButton.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.uploadButton.enabled = YES;
    });
    
    self.uploadButton.selected = YES;
    self.downButton.selected = NO;
    
    if ([self.delegate respondsToSelector:@selector(bussinessShopGoodsDeal:)]) {
        [self.delegate bussinessShopGoodsDeal:@"1"];
    }
}

#pragma mark - > 已下架
- (void)downButtonOnClick{
    
    self.downButton.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       self.downButton.enabled = YES;
    });
    
    self.uploadButton.selected = NO;
    self.downButton.selected = YES;
    
    if ([self.delegate respondsToSelector:@selector(bussinessShopGoodsDeal:)]) {
        [self.delegate bussinessShopGoodsDeal:@"2"];
    }
}

#pragma mark - > 返回
- (void)backBtnClick{
    
    [self.vc.navigationController popViewControllerAnimated:YES];
}
@end
