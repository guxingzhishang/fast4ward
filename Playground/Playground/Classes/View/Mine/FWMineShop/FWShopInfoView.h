//
//  FWShopInfoView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZAddImage.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWShopInfoViewDelegate <NSObject>

- (void)saveButtonClick;

@end

@interface FWShopInfoView : UIScrollView<UITextFieldDelegate,UITextViewDelegate,SZAddImageDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIView * firstView;// 店铺描述
@property (nonatomic, strong) UILabel * shopDescLabel;
@property (nonatomic, strong) IQTextView * descTextView;

@property (nonatomic, strong) UIView * secondView;//店铺地址
@property (nonatomic, strong) UILabel * shopAddressLabel;
@property (nonatomic, strong) IQTextView * arearField;
@property (nonatomic, strong) UIView * arearLineView;
@property (nonatomic, strong) IQTextView * addressField;
@property (nonatomic, strong) UIView * addressLineView;

@property (nonatomic, strong) UIView * thirdView;//联系方式
@property (nonatomic, strong) UILabel * phoneLabel;
@property (nonatomic, strong) IQTextView * phoneField;
@property (nonatomic, strong) UIView * phoneLineView;

@property (nonatomic, strong) UIView * fourthView;//主营业务
@property (nonatomic, strong) UILabel * bussinessLabel;
@property (nonatomic, strong) UIView * containerView;

@property (nonatomic, strong) UIView * fivethView;//擅长车型
@property (nonatomic, strong) UILabel * carStyleLabel;
@property (nonatomic, strong) IQTextView * carStyleField;
@property (nonatomic, strong) UIView * carStyleLineView;

@property (nonatomic, strong) UIView * sixthView; //店铺照片
@property (nonatomic, strong) UILabel * picsLabel;
@property (nonatomic, strong) SZAddImage * addPicsImageView;

@property (nonatomic, strong) UIButton * saveButton;

@property (strong, nonatomic) NSMutableArray * idArray;

@property(strong,nonatomic) UIPickerView * pickerView;

@property(strong,nonatomic) NSMutableArray *ProvinceArr;
@property(strong,nonatomic) NSMutableArray *CityArr;
@property(strong,nonatomic) NSMutableArray *AreaArr;
@property(strong,nonatomic) NSArray *arr;

@property (nonatomic, weak) id<FWShopInfoViewDelegate> shopInfoDelegate;

@property (nonatomic, strong) NSString * provice_id;
@property (nonatomic, strong) NSString * city_id;
@property (nonatomic, strong) NSString * arear_id;


/**
 *  存储所有的照片(UIImage)
 */
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, weak) UIViewController * vc;

- (void)configViewForModel:(id)model;

- (void)setupBussinessInfo:(id)model;

@end

NS_ASSUME_NONNULL_END
