//
//  FWMineView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWBaseCellView.h"
#import "MagicImageView.h"
#import "FWThumbUpView.h"
#import "FWMineModel.h"

@protocol FWMineViewDelegate <NSObject>

- (void)headerBtnClick:(UIButton *)sender;

@end


@interface FWMineView : UIView


/* 顶部背景色 */
@property (nonatomic, strong) UIImageView * topBgImageView;

/* 消息 */
@property (nonatomic, strong) UIButton * messageButton;
@property (nonatomic, strong) UILabel * unReadLabel;

/* 头像 */
@property (nonatomic, strong) UIImageView * photoImageView;

/* 肆放东家图标 */
@property (nonatomic, strong) UIImageView * renzhengImageView;

/* 查看大图按钮 */
@property (nonatomic, strong) UIButton * changePhotoButton;

/* 查看大图的view */
@property (nonatomic, strong) UIImageView * browseImgView;

/* 姓名 */
@property (nonatomic, strong) UILabel * nameLabel;

/* vip图标 */
@property (nonatomic, strong) UIButton * vipImageButton;

/* 签名 */
@property (nonatomic, strong) UILabel * signLabel;

/* id */
@property (nonatomic, strong) UILabel * f4wIDLabel;

/* 认证 */
@property (nonatomic, strong) UIImageView * authenticationView;

/* 认证 */
@property (nonatomic, strong) UILabel * authenticationLabel;

/* 影响力按钮 */
@property (nonatomic, strong) UIButton * effectButton;

/* 影响力值 */
@property (nonatomic, strong) UILabel * effectLabel;

/* 影响力图标 */
@property (nonatomic, strong) UIImageView * effectImageView;

/* 承载粉丝、获赞、关注 */
@property (nonatomic, strong) UIView * lightView;

/* 获赞按钮 */
@property (nonatomic, strong) UIButton * thumbUpButton;

/* 获赞文案 */
@property (nonatomic, strong) UILabel * thumbUpLabel;

/* 关注按钮 */
@property (nonatomic, strong) UIButton * followButton;

/* 关注文案 */
@property (nonatomic, strong) UILabel * followLabel;

/* 粉丝按钮 */
@property (nonatomic, strong) UIButton * funsButton;

/* 粉丝文案 */
@property (nonatomic, strong) UILabel * funsLabel;

/* 点赞弹窗 */
@property (nonatomic, strong) FWThumbUpView * thumbUpView;

/* 活动 */
@property (nonatomic, strong) UIImageView * activityImageView;

/* 个人主页 */
@property (nonatomic, strong) UIButton * mineButton;

/* 承载该视图的控制器 */
@property (nonatomic, weak) UIViewController * viewcontroller;

/* 代理 */
@property (nonatomic, weak) id<FWMineViewDelegate> delegate;

/* 之前头像地址 */
@property (nonatomic, strong) NSString * preHeader_URL;

/* 全局模型 */
@property (nonatomic, strong) FWMineModel * mineModel;

/* 用户页状态 1：个人（登录的用户） 2：其他用户的信息 */
//@property (nonatomic, assign) FWMineViewType  mineViewType;

@property (nonatomic, assign) CGFloat  currentHeight;

- (void)initClickMethodWithController:(UIViewController *)delegate;

- (void)configForView:(id)model;
@end
