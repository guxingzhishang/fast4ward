//
//  FWMyworksCollectionCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/2.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWCollecitonBaseCell.h"

@interface FWMyworksCollectionCell : FWCollecitonBaseCell

+ (NSString *)cellIdentifier;

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath;


@end
