//
//  FWMineCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMineCell.h"

@implementation FWMineCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
    }
    return self;
}


- (void)setupSubviews{
    
    self.photoImageView.layer.cornerRadius = 20/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(24);
    }];
    
    self.nameLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 16];
    self.nameLabel.textColor = FWTextColor_000000;
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(12);
        make.height.mas_equalTo(20);
        make.width.mas_lessThanOrEqualTo(150);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    self.selectImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(6, 11));
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    
    self.signLabel.textAlignment = NSTextAlignmentRight;
    self.signLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 14];
    self.signLabel.textColor = FWTextColor_C4C4C4;
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.selectImageView.mas_left).mas_offset(-15);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(150);
        make.centerY.mas_equalTo(self.contentView);
    }];
    self.signLabel.hidden = YES;
    
    
    self.lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.selectImageView);
        make.height.mas_equalTo(0.5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-0.5);
    }];
}

-(void)cellConfigureFor:(id)model{
    
    
}

@end
