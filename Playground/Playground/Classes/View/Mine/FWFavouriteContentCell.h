//
//  FWFavouriteContentCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWFavouriteContentTagModel.h"

@class FWFavouriteContentCell;

@protocol FWFavouriteContentCellProtocol<NSObject>

- (void)cell:(FWFavouriteContentCell *)cell didSelect:(UIButton *)button;

@end

@interface FWFavouriteContentCell : UITableViewCell

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, weak) UIViewController<FWFavouriteContentCellProtocol> *delegate;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier delegate:(UIViewController *)delegate;

- (void)cellConfigureFor:(id)model;

@end
