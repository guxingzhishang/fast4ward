//
//  FWCarDetailPicsCell.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/6.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWCarListModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^LeftButtonBlock)(void);
typedef void(^RightButtonBlock)(void);

@interface FWCarDetailPicsCell : UITableViewCell

@property (nonatomic, strong) UIImageView * leftCarPicView;
@property (nonatomic, strong) UIImageView * rightCarPicView;

@property (nonatomic, copy  ) LeftButtonBlock  leftButtonBlock;
@property (nonatomic, copy  ) RightButtonBlock rightButtonBlock;

@property (nonatomic, strong) FWCarImgsModel * leftListModel;
@property (nonatomic, strong) FWCarImgsModel * rightListModel;

- (void)configForLeftView:(id)model;
- (void)configForRightView:(id)model;
@end

NS_ASSUME_NONNULL_END
