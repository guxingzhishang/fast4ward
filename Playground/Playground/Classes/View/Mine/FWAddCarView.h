//
//  FWAddCarView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWAddCarViewDelegate <NSObject>

- (void)saveFinished;

@end

@interface FWAddCarView : UIScrollView

@property (nonatomic, strong) UIView * topView;

@property (nonatomic, strong) UIImageView * carImageView;
@property (nonatomic, strong) UILabel * carReUploadLabel;

@property (nonatomic, strong) UILabel * nicknameLabel;
@property (nonatomic, strong) IQTextView * nicknameTF;
@property (nonatomic, strong) UIView * nicknameLineView;

@property (nonatomic, strong) UILabel * buyTimeLabel;
@property (nonatomic, strong) UIView * buyTimeLineView;

@property (nonatomic, strong) UILabel * carStyleLabel;
@property (nonatomic, strong) UILabel * selectLabel;
@property (nonatomic, strong) UIView * carStyleLineView;

@property (nonatomic, strong) UILabel * maliLabel;
@property (nonatomic, strong) IQTextView * maliTF;
@property (nonatomic, strong) UIView * maliLineView;

@property (nonatomic, strong) UILabel * descriptionLabel;
@property (nonatomic, strong) UILabel * descriptionTV;
@property (nonatomic, strong) UIView * descriptionLineView;

@property (nonatomic, strong) UIButton * saveButton;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, assign) BOOL isSelectCar;
@property (nonatomic, strong) NSString * car_style_id;

@property (nonatomic, weak) id <FWAddCarViewDelegate>viewDelegate;


- (void)configForView:(id)model;
- (NSDictionary *)getParams;
- (void)saveButtonClick;

@end

NS_ASSUME_NONNULL_END
