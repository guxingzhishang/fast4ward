//
//  FWMyworksCollectionCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/2.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMyworksCollectionCell.h"

@implementation FWMyworksCollectionCell


+ (NSString *)cellIdentifier {
    return @"FWMyworksCollectionCell";
}

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath {
    FWMyworksCollectionCell *cell = (FWMyworksCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:[FWMyworksCollectionCell cellIdentifier] forIndexPath:indexPath];
    return cell;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}


@end
