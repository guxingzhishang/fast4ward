//
//  FWSettingCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWSettingCell : UITableViewCell

/**
 * 右侧箭头
 */
@property (nonatomic, strong) UIImageView * arrowImageView;

/**
 * 标题
 */
@property (nonatomic, strong) UILabel *titleLabel;

/**
 * 右侧副标题
 */
@property (nonatomic, strong) UILabel *rightLabel;

/**
 * 底线
 */
@property (nonatomic, strong) UIView * lineView;


@end

NS_ASSUME_NONNULL_END
