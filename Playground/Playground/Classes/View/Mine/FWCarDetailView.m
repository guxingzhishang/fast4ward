//
//  FWCarDetailView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarDetailView.h"
#import "UIImage+RoundedCorner.h"
#import "FWCarDetailShareViewController.h"

@interface FWCarDetailView ()
@property (nonatomic, strong) FWCarListModel * listModel;

@end

@implementation FWCarDetailView
@synthesize listModel;
@synthesize barView;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWTextColor_F8F8F8;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.topImageView = [[UIImageView alloc] init];
    self.topImageView.clipsToBounds = YES;
    self.topImageView.userInteractionEnabled = YES;
    self.topImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topImageView.image = [UIImage imageNamed:@"placeholder"];
    [self addSubview:self.topImageView];
    self.topImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 215);

    [self.topImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topImageTap)]];

    self.backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0+FWCustomeSafeTop,45,40)];
    self.backButton.titleLabel.font = DHSystemFontOfSize_16;
    self.backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(55, 30));
        make.left.mas_equalTo(self.topImageView).mas_offset(10);
        make.top.mas_equalTo(self.topImageView).mas_offset(15+FWCustomeSafeTop);
    }];
    
    self.shareButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    [self.shareButton setImage:[UIImage imageNamed:@"new_white_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.shareButton];
    [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(45, 30));
        make.right.mas_equalTo(self.topImageView).mas_offset(-5);
        make.centerY.mas_equalTo(self.backButton);
    }];
    
    
    self.userInfoView = [[UIView alloc] init];
    self.userInfoView.layer.cornerRadius = 2;
    self.userInfoView.layer.masksToBounds = YES;
    self.userInfoView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.userInfoView];
    self.userInfoView.frame = CGRectMake(0, CGRectGetMaxY(self.topImageView.frame), SCREEN_WIDTH, 61);

    [self.userInfoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userInfoClick)]];

    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 33/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.userInfoView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.userInfoView);
        make.left.mas_equalTo(self.userInfoView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(33, 33));
    }];

    self.editButton = [[UIButton alloc] init];
    self.editButton.layer.borderColor = FWColor(@"222222").CGColor;
    self.editButton.layer.borderWidth = 1;
    self.editButton.layer.cornerRadius = 2;
    self.editButton.layer.masksToBounds = YES;
    self.editButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.editButton setTitle:@"设置" forState:UIControlStateNormal];
    [self.editButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.editButton addTarget:self action:@selector(editButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.userInfoView addSubview:self.editButton];
    [self.editButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.userInfoView).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(70, 23));
        make.centerY.mas_equalTo(self.userInfoView);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nameLabel.textColor = FWTextColor_272727;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.userInfoView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(14);
        make.centerY.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.editButton.mas_left).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    self.carInfoView = [[UIView alloc] init];
    self.carInfoView.layer.cornerRadius = 2;
    self.carInfoView.layer.masksToBounds = YES;
    self.carInfoView.userInteractionEnabled = YES;
    self.carInfoView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.carInfoView];
    self.carInfoView.frame = CGRectMake(CGRectGetMinX(self.userInfoView.frame), CGRectGetMaxY(self.userInfoView.frame), CGRectGetWidth(self.userInfoView.frame), 238);
    
    self.carNameLabel = [[UILabel alloc] init];
    self.carNameLabel.numberOfLines = 0;
    self.carNameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 18];
    self.carNameLabel.textColor = FWTextColor_222222;
    self.carNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.carInfoView addSubview:self.carNameLabel];
    [self.carNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carInfoView).mas_offset(14);
        make.width.mas_equalTo(SCREEN_WIDTH-14*4);
        make.top.mas_equalTo(self.carInfoView);
        make.height.mas_greaterThanOrEqualTo(22);
    }];
//    self.carNameLabel.frame = CGRectMake(14, 0, SCREEN_WIDTH-14*4, 22);
    [self.carNameLabel layoutIfNeeded];
    
    self.maliLabel = [[UILabel alloc] init];
    self.maliLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.maliLabel.textColor = DHLineHexColor_LightGray_999999;
    self.maliLabel.textAlignment = NSTextAlignmentLeft;
    [self.carInfoView addSubview:self.maliLabel];
    self.maliLabel.frame = CGRectMake(CGRectGetMinX(self.carNameLabel.frame), CGRectGetMaxY(self.carNameLabel.frame)+5, CGRectGetWidth(self.carNameLabel.frame), 16);
    
    self.chuohaoLabel = [[UILabel alloc] init];
    self.chuohaoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.chuohaoLabel.textColor = DHLineHexColor_LightGray_999999;
    self.chuohaoLabel.textAlignment = NSTextAlignmentLeft;
    [self.carInfoView addSubview:self.chuohaoLabel];
    self.chuohaoLabel.frame = CGRectMake(CGRectGetMinX(self.carNameLabel.frame), CGRectGetMaxY(self.maliLabel.frame), CGRectGetWidth(self.carNameLabel.frame), 16);

    
    self.ageLabel = [[UILabel alloc] init];
    self.ageLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.ageLabel.textColor = DHLineHexColor_LightGray_999999;
    self.ageLabel.textAlignment = NSTextAlignmentLeft;
    [self.carInfoView addSubview:self.ageLabel];
    self.ageLabel.frame = CGRectMake(CGRectGetMinX(self.carNameLabel.frame), CGRectGetMaxY(self.chuohaoLabel.frame), CGRectGetWidth(self.carNameLabel.frame), 16);
    
    
    self.likeButton = [[UIButton alloc] init];
    [self.likeButton addTarget:self action:@selector(likeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.carInfoView addSubview:self.likeButton];
    [self.likeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.ageLabel);
        make.top.mas_equalTo(self.ageLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(30);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.likeLabel = [[UILabel alloc] init];
    self.likeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 18];
    self.likeLabel.textColor = FWColor(@"#39393F");
    self.likeLabel.textAlignment = NSTextAlignmentLeft;
    [self.likeButton addSubview:self.likeLabel];
    [self.likeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.likeButton).mas_offset(0);
        make.bottom.mas_equalTo(self.likeButton);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.likeDescLabel = [[UILabel alloc] init];
    self.likeDescLabel.text = @"喜欢";
    self.likeDescLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.likeDescLabel.textColor = FWColor(@"#969AA1");
    self.likeDescLabel.textAlignment = NSTextAlignmentLeft;
    [self.likeButton addSubview:self.likeDescLabel];
    [self.likeDescLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.likeLabel.mas_right).mas_offset(5);
        make.bottom.mas_equalTo(self.likeButton).mas_offset(-1);;
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.likeButton);
        make.width.mas_greaterThanOrEqualTo(10);
    }];

    self.commentButton = [[UIButton alloc] init];
    [self.commentButton addTarget:self action:@selector(commentButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.carInfoView addSubview:self.commentButton];
    [self.commentButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.likeButton.mas_right).mas_offset(20);
        make.centerY.mas_equalTo(self.likeButton);
        make.height.mas_equalTo(30);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.commentLabel = [[UILabel alloc] init];
    self.commentLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 18];
    self.commentLabel.textColor = FWColor(@"#39393F");
    self.commentLabel.textAlignment = NSTextAlignmentLeft;
    [self.commentButton addSubview:self.commentLabel];
    [self.commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.commentButton).mas_offset(0);
        make.bottom.mas_equalTo(self.commentButton);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.commentDescLabel = [[UILabel alloc] init];
    self.commentDescLabel.text = @"评论";
    self.commentDescLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.commentDescLabel.textColor = FWColor(@"#969AA1");
    self.commentDescLabel.textAlignment = NSTextAlignmentLeft;
    [self.commentButton addSubview:self.commentDescLabel];
    [self.commentDescLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.commentLabel.mas_right).mas_offset(5);
        make.bottom.mas_equalTo(self.commentButton).mas_offset(-1);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.commentButton);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.numberOfLines = 0;
    self.descLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.descLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.descLabel.textColor = FWTextColor_272727;
    self.descLabel.textAlignment = NSTextAlignmentLeft;
    [self.carInfoView addSubview:self.descLabel];
    self.descLabel.frame = CGRectMake(CGRectGetMinX(self.ageLabel.frame), CGRectGetMaxY(self.ageLabel.frame)+40, SCREEN_WIDTH-54, 10);
    
    barView = [[FWCarDetailSelectBarView alloc] init];
    barView.titleArray = @[@"爱车相册",@"改装清单",@"同系车友"];
    barView.numType = 3;
    [self addSubview:barView];
    barView.frame = CGRectMake(0, CGRectGetMaxY(self.carInfoView.frame)+10, SCREEN_WIDTH, 50);

    [barView.firstButton addTarget:self action:@selector(picButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [barView.secondButton addTarget:self action:@selector(gaizhuangButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [barView.thirdButton addTarget:self action:@selector(tongxiButtonClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)picButtonClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

- (void)gaizhuangButtonClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

- (void)tongxiButtonClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > 配置信息
- (void)configForView:(id)model{
    
    listModel = (FWCarListModel *)model;
    
    self.nameLabel.text = listModel.user_info.nickname;
    self.carNameLabel.text = listModel.car_style_name;
    self.ageLabel.text = [NSString stringWithFormat:@"车龄：%@",listModel.car_age];
    self.likeLabel.text = listModel.count_realtime.like_count_format;
    self.commentLabel.text = listModel.count_realtime.comment_count_format;
    self.descLabel.text = listModel.car_desc;
    
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:listModel.car_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    /* 没有马力 */
    if (!listModel.mali_show || listModel.mali_show.length <= 0) {
        self.maliLabel.frame = CGRectMake(CGRectGetMinX(self.carNameLabel.frame), CGRectGetMaxY(self.carNameLabel.frame)+5, CGRectGetWidth(self.carNameLabel.frame), 0.01);
    }else{
        self.maliLabel.frame = CGRectMake(CGRectGetMinX(self.carNameLabel.frame), CGRectGetMaxY(self.carNameLabel.frame)+5, CGRectGetWidth(self.carNameLabel.frame), 20);
        self.maliLabel.text = listModel.mali_show;
    }
    
    /* 没有昵称 */
    if (!listModel.car_nickname || listModel.car_nickname.length <= 0) {
        self.chuohaoLabel.frame = CGRectMake(CGRectGetMinX(self.carNameLabel.frame), CGRectGetMaxY(self.maliLabel.frame), CGRectGetWidth(self.carNameLabel.frame), 0.01);
    }else{
        self.chuohaoLabel.frame = CGRectMake(CGRectGetMinX(self.carNameLabel.frame), CGRectGetMaxY(self.maliLabel.frame), CGRectGetWidth(self.carNameLabel.frame), 20);
        self.chuohaoLabel.text = [NSString stringWithFormat:@"绰号：%@",listModel.car_nickname];
    }
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:self.descLabel.attributedText];
    CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 54].height;
    
    self.ageLabel.frame = CGRectMake(CGRectGetMinX(self.carNameLabel.frame), CGRectGetMaxY(self.chuohaoLabel.frame), CGRectGetWidth(self.carNameLabel.frame), 20);
    
     [self.likeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(self.ageLabel);
         make.top.mas_equalTo(self.ageLabel.mas_bottom).mas_offset(0);
         make.height.mas_equalTo(30);
         make.width.mas_greaterThanOrEqualTo(10);
     }];
    
    [self.commentButton mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(self.likeButton.mas_right).mas_offset(20);
         make.centerY.mas_equalTo(self.likeButton);
         make.height.mas_equalTo(30);
         make.width.mas_greaterThanOrEqualTo(10);
     }];
    
    self.descLabel.frame = CGRectMake(CGRectGetMinX(self.ageLabel.frame), CGRectGetMaxY(self.ageLabel.frame)+40, SCREEN_WIDTH-2*(CGRectGetMinX(self.ageLabel.frame)), textHeight);
    
    self.carInfoView.frame = CGRectMake(CGRectGetMinX(self.userInfoView.frame), CGRectGetMaxY(self.userInfoView.frame), CGRectGetWidth(self.userInfoView.frame),CGRectGetMaxY(self.descLabel.frame)+10);
    
    barView.frame = CGRectMake(0, CGRectGetMaxY(self.carInfoView.frame)+10, SCREEN_WIDTH, 50);
}

#pragma mark - > 喜欢
- (void)likeButtonClick{
    
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:self.listModel.user_info.uid]) {
        /* 自己不能给自己点赞 */
        return;
    }
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.listModel.feed_id,
                                  @"like_type":self.likeType,
                                  };
        
        [request startWithParameters:params WithAction:Submit_like_feed WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([self.likeType isEqualToString: @"add"]) {
                    self.listModel.is_liked = @"1";
                    
                    if ([self.listModel.count_realtime.like_count_format integerValue] ||
                        [self.listModel.count_realtime.like_count_format integerValue] == 0) {
                        
                        self.listModel.count_realtime.like_count_format = @([self.listModel.count_realtime.like_count_format integerValue] +1).stringValue;
                    }
                }else if ([self.likeType isEqualToString: @"cancel"]){
                    self.listModel.is_liked = @"2";
                    
                    if ([self.listModel.count_realtime.like_count_format integerValue] ||
                        [self.listModel.count_realtime.like_count_format integerValue] == 0) {
                        
                        self.listModel.count_realtime.like_count_format = @([self.listModel.count_realtime.like_count_format integerValue] -1).stringValue;
                    }
                }
                
                [self configForView:self.listModel];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}

#pragma mark - > 评论
- (void)commentButtonClick{
    
    FWCommentViewController * VC = [[FWCommentViewController alloc] init];
    VC.feed_id = self.listModel.feed_id;
    [self.vc.navigationController pushViewController:VC animated:YES];
}

#pragma mark - > 编辑座驾
- (void)editButtonClick{
  
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *editAction = [UIAlertAction actionWithTitle:@"编辑" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([self.delegate respondsToSelector:@selector(editButtonOnClick)]) {
            [self.delegate editButtonOnClick];
        }
    }];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deleteCar];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:editAction];
    [alertController addAction:deleteAction];
    [alertController addAction:cancelAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

- (void)deleteCar{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"座驾信息一旦删除不可复原，确认删除吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"删除座驾" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];

    NSDictionary * params = @{
                           @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                           @"user_car_id":listModel.user_car_id,
                           };

    [request startWithParameters:params WithAction:Submit_delete_car WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
     
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"删除成功" toController:self.vc];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.vc.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"暂不" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - > 个人中心模块
- (void)userInfoClick{
    
    FWNewUserInfoViewController * UVC = [[FWNewUserInfoViewController alloc] init];
    UVC.user_id = self.listModel.user_info.uid;
    [self.vc.navigationController pushViewController:UVC animated:YES];
}

#pragma mark - > 点击顶部大图
- (void)topImageTap{
    
    NSMutableArray * imageArray = @[self.listModel.car_cover_original].mutableCopy;
    
    FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
    browser.isFullWidthForLandScape = YES;
    browser.isNeedLandscape = YES;
    browser.imageArray = [imageArray mutableCopy];
    browser.smallArray = [imageArray copy];
    browser.originalImageArray = [imageArray copy];
    browser.currentImageIndex = 0;
    browser.vc = self.vc;
    browser.fromType = 2;
    [browser show];
}

#pragma mark - > 返回
- (void)backBtnClick{
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 分享
- (void)shareBtnClick{
    
    FWCarDetailShareViewController * vc = [[FWCarDetailShareViewController alloc] init];
    vc.listModel = self.listModel;
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.vc.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.vc presentViewController:vc animated:YES completion:nil];
}

#pragma mark - > 返回当前顶部视图的高度
- (CGFloat)getCurrentViewHeight{
    
    CGFloat height = 0;
    height = CGRectGetMaxY(barView.frame);
    return height;
}

#pragma mark - > 返回切换标签上部的高度
- (CGFloat)getBarViewTopHeight{
    
    CGFloat height = 0;
    height = CGRectGetMinY(barView.frame);
    return height;
}

@end
