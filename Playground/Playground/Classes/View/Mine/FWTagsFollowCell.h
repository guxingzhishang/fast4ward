//
//  FWTagsFollowCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/29.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWSearchTagsListModel.h"
#import "FWTagsRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWTagsFollowCell : FWListCell


@property (nonatomic, strong) UIButton * followButton;

@property (nonatomic, strong) FWSearchTagsSubListModel * tagsFollowModel;

@end

NS_ASSUME_NONNULL_END
