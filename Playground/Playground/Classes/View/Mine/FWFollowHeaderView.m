//
//  FWFollowHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/29.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWFollowHeaderView.h"

@implementation FWFollowHeaderView
@synthesize backButton;
@synthesize userButton;
@synthesize tagsButton;
@synthesize gunView;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    backButton= [[UIButton alloc] init];
    [backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backButton];
    [backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(self).mas_offset(30+FWCustomeSafeTop);
    }];
    
    userButton = [[UIButton alloc] init];
    userButton.titleLabel.font = DHSystemFontOfSize_16;
    [userButton setTitle:@"用户" forState:UIControlStateNormal];
    [userButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    [userButton addTarget:self action:@selector(userButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:userButton];
    [userButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(backButton);
        make.size.mas_equalTo(CGSizeMake(50, 25));
        make.right.mas_equalTo(self).mas_offset(-SCREEN_WIDTH/2-10);
    }];
    
    
    tagsButton = [[UIButton alloc] init];
    tagsButton.titleLabel.font = DHSystemFontOfSize_16;
    [tagsButton setTitle:@"标签" forState:UIControlStateNormal];
    [tagsButton setTitleColor:FWTextColor_D8D8D8 forState:UIControlStateNormal];
    [tagsButton addTarget:self action:@selector(tagsButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:tagsButton];
    [tagsButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(userButton);
        make.size.mas_equalTo(userButton);
        make.left.mas_equalTo(self).mas_offset(SCREEN_WIDTH/2+10);
    }];
    
    gunView = [[UIView alloc] init];
    gunView.layer.cornerRadius = 2;
    gunView.layer.masksToBounds = YES;
    gunView.backgroundColor = FWTextColor_222222;
    [self addSubview:gunView];
    [gunView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(userButton);
        make.size.mas_equalTo(CGSizeMake(32, 1.5));
        make.top.mas_equalTo(userButton.mas_bottom);
    }];
}

#pragma mark - > 返回
- (void)backBtnClick{
    
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 用户
- (void)userButtonOnClick:(UIButton *)sender{
 
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > 标签
- (void)tagsButtonOnClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}


@end
