//
//  FWVisitorNameListHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVisitorNameListHeaderView.h"

@implementation FWVisitorNameListHeaderView
@synthesize todayVisivorLabel;
@synthesize totalVisivorLabel;
@synthesize tipButton;

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    todayVisivorLabel = [[UILabel alloc] init];
    todayVisivorLabel.text = @"今日访客：0";
    todayVisivorLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];;
    todayVisivorLabel.textColor = FWTextColor_71768C;
    todayVisivorLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:todayVisivorLabel];
    [todayVisivorLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(24);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(80);
    }];
    
    tipButton = [[UIButton alloc] init];
    [tipButton setImage:[UIImage imageNamed:@"visitor_tip"] forState:UIControlStateNormal];
    [tipButton addTarget:self action:@selector(tipButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:tipButton];
    [tipButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.top.mas_equalTo(self);
        make.left.mas_equalTo(todayVisivorLabel.mas_right).mas_offset(-2);
    }];
    
    totalVisivorLabel = [[UILabel alloc] init];
    totalVisivorLabel.text = @"累计访客：0";
    totalVisivorLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];;
    totalVisivorLabel.textColor = FWTextColor_71768C;
    totalVisivorLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:totalVisivorLabel];
    [totalVisivorLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(todayVisivorLabel);
        make.left.mas_equalTo(todayVisivorLabel.mas_right).mas_offset(44);
        make.height.mas_equalTo(todayVisivorLabel);
        make.width.mas_greaterThanOrEqualTo(70);
    }];
}

- (void)tipButtonOnClick{
    
    NSString * message = @"\n1、上传帖子，参加热门活动，让更多人看到你。\n2、将自己的帖子分享到微信，朋友圈可以获得更多关注。";
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"如何增加账户的曝光量" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"去投稿" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
            NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
            
            if ([params[@"is_draft"] isEqualToString:@"1"]) {
                // 草稿
                [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self.vc];
            }else{
                // 发布
                [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self.vc];
            }
        }else {
            if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
                
                FWPublishViewController * vc = [[FWPublishViewController alloc] init];
                vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
                vc.fd_prefersNavigationBarHidden = YES;
                vc.navigationController.navigationBar.hidden = YES;
                
                [GFStaticData saveObject:@"home" forKey:Frome_ViewController];

                CATransition *animation = [CATransition animation];
                [animation setDuration:0.4];
                [animation setType: kCATransitionMoveIn];
                [animation setSubtype: kCATransitionFromTop];
                [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
                [self.vc.navigationController pushViewController:vc animated:NO];
                [self.vc.navigationController.view.layer addAnimation:animation forKey:nil];
            }
        }
    }];
    UIView *subView1 = alertController.view.subviews[0];
    UIView *subView2 = subView1.subviews[0];
    UIView *subView3 = subView2.subviews[0];
    UIView *subView4 = subView3.subviews[0];
    UIView *subView5 = subView4.subviews[0];
    
    for (UILabel * label in subView5.subviews) {
        if ([label isKindOfClass:[UILabel class]]&&
            [label.text isEqualToString:message]) {
            label.textAlignment = NSTextAlignmentLeft;
        }
    }
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}
@end
