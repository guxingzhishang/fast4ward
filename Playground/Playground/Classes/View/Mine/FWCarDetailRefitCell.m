//
//  FWCarDetailRefitCell.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWCarDetailRefitCell.h"
#import "FWCarDetailRefitListViewController.h"

@implementation FWCarDetailRefitDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.detailLabel = [[UILabel alloc] init];
    self.detailLabel.numberOfLines = 0;
    self.detailLabel.font = DHBoldFont(14);
    self.detailLabel.textColor = FWTextColor_222222;
    self.detailLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.detailLabel];
    [self.detailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(100);
    }];
}

@end


@implementation FWCarDetailRefitCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"改装清单";
    self.titleLabel.font = DHBoldFont(14);
    self.titleLabel.textColor = FWTextColor_222222;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(100, 260));
    }];
    
    self.editButton = [[UIButton alloc] init];
    self.editButton.layer.borderColor = FWTextColor_222222.CGColor;
    self.editButton.layer.borderWidth = 1;
    self.editButton.layer.cornerRadius = 2;
    self.editButton.layer.masksToBounds = YES;
    self.editButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.editButton setTitle:@"编辑" forState:UIControlStateNormal];
    [self.editButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.editButton addTarget:self action:@selector(editButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.editButton];
    [self.editButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(50, 24));
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.editButton);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(self.contentView);
    }];
}

#pragma mark - > 编辑
- (void)editButtonClick{
    
    FWCarDetailRefitListViewController *VC = [[FWCarDetailRefitListViewController alloc] init];
    VC.listModel = self.listModel;
    [self.vc.navigationController pushViewController:VC animated:YES];
}
@end
