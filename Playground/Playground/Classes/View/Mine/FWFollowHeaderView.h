//
//  FWFollowHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/29.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWFollowHeaderViewDelegate <NSObject>

- (void)headerBtnClick:(UIButton *)sender;

@end

@interface FWFollowHeaderView : UIView

@property (nonatomic, strong) UIButton * backButton;

@property (nonatomic, strong) UIButton * userButton;

@property (nonatomic, strong) UIButton * tagsButton;

@property (nonatomic, strong) UIView * gunView;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) id<FWFollowHeaderViewDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
