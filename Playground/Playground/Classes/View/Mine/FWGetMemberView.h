//
//  FWGetMemberView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWGetMemberViewDelegate <NSObject>

- (void)payButtonClick:(NSString *)payMethod;

@end

@interface FWGetMemberView : UIView<UITextFieldDelegate>

@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UIButton * payButton;
@property (nonatomic, strong) UITextField * nameField;
@property (nonatomic, strong) UITextField * phoneField;
@property (nonatomic, strong) UIView * payView;

@property (nonatomic, assign) NSInteger  currentIndex;

@property (nonatomic, strong) FWMemberListModel * memberListModel;
@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) id<FWGetMemberViewDelegate>delegate;

-(void)settingInfo:(FWMemberInfoModel *)infoModel;

@end

NS_ASSUME_NONNULL_END
