//
//  FWVisitorNameListCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVisitorNameListCell.h"

@interface FWVisitorNameListCell()

@property (nonatomic, strong) FWVisitorNameListModel * listModel;

@end

@implementation FWVisitorNameListCell
@synthesize listModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    self.photoImageView.layer.cornerRadius = 37/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(37, 37));
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(24);
    }];
    
    self.authenticationImageView.hidden = NO;
    [self.authenticationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.right.mas_equalTo(self.photoImageView).mas_offset(-2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(0);
    }];
    
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    self.nameLabel.textColor = FWTextColor_12101D;
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(24);
        make.height.mas_equalTo(19);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-184);
        make.top.mas_equalTo(self.photoImageView);
    }];
    
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    self.signLabel.textColor = FWTextColor_969696;
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(self.nameLabel);
        make.width.mas_lessThanOrEqualTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
    }];
    
    
    self.selectImageView.hidden = NO;
    self.selectImageView.layer.cornerRadius = 4;
    self.selectImageView.layer.masksToBounds = YES;
    [self.selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-24);
        make.height.mas_equalTo(24);
        make.width.mas_equalTo(24);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    self.lineView.hidden = NO;
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.contentView).mas_offset(-24);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH-48);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-0.5);
    }];
}

- (void)cellConfigureFor:(id)model{
    
    listModel = (FWVisitorNameListModel *)model;
    
    if([listModel.cert_status isEqualToString:@"2"]) {
        self.nameLabel.textColor = FWTextColor_FFAF3C;
        self.authenticationImageView.hidden = NO;
        self.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        if ([listModel.merchant_cert_status isEqualToString:@"3"] &&
            ![listModel.cert_status isEqualToString:@"2"]){
            self.nameLabel.textColor = FWTextColor_2B98FA;
            self.authenticationImageView.hidden = YES;
        }else{
            self.authenticationImageView.hidden = YES;
            self.nameLabel.textColor = FWTextColor_12101D;
        }
    }
    
    
    self.nameLabel.text = listModel.nickname;
    self.signLabel.text = listModel.desc;

    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:listModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    if ([listModel.type isEqualToString:@"1"]) {
        
        self.selectImageView.hidden = YES;
    }else{
        self.selectImageView.hidden = NO;
        [self.selectImageView sd_setImageWithURL:[NSURL URLWithString:listModel.feed_cover] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    }
}

- (void)photoImageViewTapClick{
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = listModel.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}


@end
