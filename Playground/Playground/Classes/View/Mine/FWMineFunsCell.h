//
//  FWMineFunsCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/8.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWListCell.h"
#import "FWFollowModel.h"

@class FWMineFunsCell;

@protocol FWMineFunsCellDelegate<NSObject>

- (void)followButtonForIndex:(NSInteger)index  withCell:(FWMineFunsCell *)cell;

@end

@interface FWMineFunsCell : FWListCell

@property (nonatomic, strong) UIButton * followButton;

@property (nonatomic, strong) UIButton * vipImageButton;

@property (nonatomic, strong) NSMutableArray * preArray;

@property (nonatomic, strong) FWFollowUserListModel * listModel;

@property (nonatomic, weak) id<FWMineFunsCellDelegate> delegate;
- (void)cellConfigureFor:(id)model;

@end
