//
//  FWCarDetailSelectBarView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/5.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWCarDetailSelectBarView : UIView

/**
 * 灰色横线
 */
@property (nonatomic, strong) UIView * honOneView;

/**
 * 灰色横线
 */
@property (nonatomic, strong) UIView * honTwoView;


@property (nonatomic, strong) UIButton * firstButton;

@property (nonatomic, strong) UIButton * thirdButton;

@property (nonatomic, strong) UIButton * fourthButton;

@property (nonatomic, strong) UIButton * secondButton;

/**
 * 光标
 */
@property (nonatomic, strong) UIView * shadowView;

/**
 * 有几个图标
 */
@property (nonatomic, assign) NSInteger numType;

/* 标题 */
@property (nonatomic, strong) NSArray * titleArray;

@end

NS_ASSUME_NONNULL_END
