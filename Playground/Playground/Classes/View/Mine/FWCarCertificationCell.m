//
//  FWCarCertificationCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarCertificationCell.h"

@implementation FWCarCertificationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
   
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [self addSubviews];

    return self;
}

- (void)addSubviews{
    
    self.carImageView.userInteractionEnabled = YES;
    
    self.mengcengView = [[UIView alloc] init];
    self.mengcengView.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    [self.carImageView addSubview:self.mengcengView];
    [self.mengcengView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.carImageView);
    }];
    self.mengcengView.hidden = YES;
    
    self.deleteButton = [[UIButton alloc] init];
    self.deleteButton.layer.borderColor = FWViewBackgroundColor_FFFFFF.CGColor;
    self.deleteButton.layer.borderWidth = 1;
    self.deleteButton.layer.cornerRadius = 5;
    self.deleteButton.layer.masksToBounds = YES;
    self.deleteButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(deleteButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.mengcengView addSubview:self.deleteButton];
    [self.deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.carImageView).mas_offset(10);
        make.right.mas_equalTo(self.carImageView).mas_offset(-13);
        make.width.mas_equalTo(CGSizeMake(55, 24));
    }];
    self.deleteButton.hidden = YES;
}

- (void)configForCell:(id)model{
    
    FWCarListModel * listModel = (FWCarListModel *)model;
    
    self.titleLabel.text = listModel.car_style_name;
    [self.carImageView sd_setImageWithURL:[NSURL URLWithString:listModel.car_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

- (void)deleteButtonClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 7777;

    if ([self.delegate respondsToSelector:@selector(deleteButtonOnClick:WithCell:)]) {
        [self.delegate deleteButtonOnClick:index WithCell:self];
    }
}

@end
