//
//  FWPersonalCertificationView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWPersonalCertificationView : UIView<UITextFieldDelegate>

@property (nonatomic, strong) UILabel * tipLabel;

@property (nonatomic, strong) UIButton * idCardButton;
@property (nonatomic, strong) UIButton * postButton;

@property (nonatomic, strong) UILabel * cardLabel;
@property (nonatomic, strong) UITextField * cardTextField;
@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, strong) UIButton * sendButton;

@property (nonatomic, weak) UIViewController * vc;


@end

NS_ASSUME_NONNULL_END
