//
//  FWVisitorAnalysisCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWVisitorAnalysisCell : UICollectionViewCell

@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * numberLabel;
@property (nonatomic, strong) UIImageView * arrowImageView;

- (void)configForAnalysisCell:(id)model;

@end

NS_ASSUME_NONNULL_END
