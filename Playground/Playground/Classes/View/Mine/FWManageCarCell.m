//
//  FWManageCarCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWManageCarCell.h"

@implementation FWManageCarCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{

    self.bgImageView = [[UIImageView alloc] init];
    self.bgImageView.layer.cornerRadius = 2;
    self.bgImageView.layer.masksToBounds = YES;
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgImageView.clipsToBounds = YES;
    self.bgImageView.image = [UIImage imageNamed:@"placeholder"];
    [self.contentView addSubview:self.bgImageView];
    [self.bgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-5);
    }];

    self.bgView = [[UIView alloc] init];
    self.bgView.frame = CGRectMake(0, 0, self.contentView.size.width, self.contentView.size.height-5);
    [self.bgImageView addSubview:self.bgView];
    
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bgView.bounds;
    [self.bgView.layer addSublayer:gradientLayer];
    
    gradientLayer.colors = @[
                             (__bridge id)[UIColor colorWithHexString:@"ffffff" alpha:0.3].CGColor,
                             (__bridge id)[UIColor colorWithHexString:@"000000" alpha:0.3].CGColor,
                             ];
    
    gradientLayer.startPoint = CGPointMake(0.0, 0.0);
    gradientLayer.endPoint = CGPointMake(0.0, 1.0);
    
    self.selectImageView = [[UIImageView alloc] init];
    self.selectImageView.image = [UIImage imageNamed:@"car_pic_unselect"];
    [self.bgImageView addSubview:self.selectImageView];
    [self.selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgImageView).mas_offset(10);
        make.right.mas_equalTo(self.bgImageView).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
}

- (void)configForCell:(id)model{
    
    FWCarImgsModel * imageModel = (FWCarImgsModel *)model;
    
    if (imageModel.isSelect) {
        self.selectImageView.image = [UIImage imageNamed:@"car_pic_select"];
    }else{
        self.selectImageView.image = [UIImage imageNamed:@"car_pic_unselect"];
    }
    
    /* 展示图片 */
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:imageModel.car_img]];
        UIImage *image = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (image) {
                self.bgImageView.image = image;
            }else{
                self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
                self.bgImageView.clipsToBounds = YES;
                self.bgImageView.image = [UIImage imageNamed:@"placeholder"];
            }
        });
    });
}

@end
