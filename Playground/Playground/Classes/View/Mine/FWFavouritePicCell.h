//
//  FWFavouritePicCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 收藏-> 图片cell
 */

#import "FWCollecitonBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWFavouritePicCell : FWCollecitonBaseCell

@end

NS_ASSUME_NONNULL_END
