//
//  FWCarDetailSelectBarView.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/5.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWCarDetailSelectBarView.h"

@implementation FWCarDetailSelectBarView
@synthesize secondButton;
@synthesize firstButton;
@synthesize thirdButton;
@synthesize honOneView;
@synthesize honTwoView;
@synthesize numType;
@synthesize shadowView;
@synthesize fourthButton;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    honOneView = [[UIView alloc] init];
    honOneView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:honOneView];

    firstButton = [[UIButton alloc] init];
    firstButton.selected = YES;
    firstButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [firstButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [firstButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:firstButton];
    
    secondButton = [[UIButton alloc] init];
    secondButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [secondButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [secondButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:secondButton];
   
    thirdButton = [[UIButton alloc] init];
    thirdButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [thirdButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [thirdButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:thirdButton];
    
    fourthButton = [[UIButton alloc] init];
    fourthButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [fourthButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [fourthButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:fourthButton];
    
    honTwoView = [[UIView alloc] init];
    honTwoView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:honTwoView];
    honTwoView.hidden = YES;
    
    shadowView= [[UIView alloc]init];
    shadowView.backgroundColor = FWTextColor_222222;
    shadowView.layer.masksToBounds = YES;
    shadowView.tag = 100000;
    [self addSubview:shadowView];
}

- (void)setNumType:(NSInteger)numType{
    
    [honOneView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 0.5));
        make.top.mas_equalTo(self).mas_offset(0);
    }];
    
    [honTwoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 0.5));
        make.top.mas_equalTo(honOneView.mas_bottom).mas_offset(49);
    }];
    
    if (numType == 2) {
        
        firstButton.frame = CGRectMake((SCREEN_WIDTH-200-30)/2, CGRectGetMaxY(honOneView.frame)+10, 100, 30);
        
        [firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(100);
            make.left.mas_equalTo((SCREEN_WIDTH-200-30)/2);
            make.top.mas_equalTo(honOneView.mas_bottom).mas_offset(10);
        }];
        [secondButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(firstButton);
            make.width.mas_equalTo(firstButton);
            make.left.mas_equalTo(firstButton.mas_right).mas_offset(30);
            make.top.mas_equalTo(firstButton);
        }];
        
        thirdButton.hidden = YES;
        fourthButton.hidden = YES;
    }else if (numType == 3){
        
        firstButton.frame = CGRectMake((SCREEN_WIDTH-270-60)/2, CGRectGetMaxY(honOneView.frame)+10, 90, 30);

        [firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(90);
            make.left.mas_equalTo((SCREEN_WIDTH-270-60)/2);
            make.top.mas_equalTo(honOneView.mas_bottom).mas_offset(10);
        }];
        [secondButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(firstButton);
            make.width.mas_equalTo(firstButton);
            make.left.mas_equalTo(firstButton.mas_right).mas_offset(30);
            make.top.mas_equalTo(firstButton);
        }];
       
        [thirdButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(secondButton);
            make.left.mas_equalTo(secondButton.mas_right).mas_offset(30);
            make.top.mas_equalTo(secondButton);
        }];
        
        thirdButton.hidden = NO;
        fourthButton.hidden = YES;
    }else if (numType == 4){
        
        firstButton.frame = CGRectMake(10, CGRectGetMaxY(honOneView.frame)+10, (SCREEN_WIDTH-35)/4, 30);
        
        [firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.width.mas_equalTo((SCREEN_WIDTH-35)/4);
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(honOneView.mas_bottom).mas_offset(10);
        }];
        [secondButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(firstButton);
            make.width.mas_equalTo(firstButton);
            make.left.mas_equalTo(firstButton.mas_right).mas_offset(5);
            make.top.mas_equalTo(firstButton);
        }];
        
        [thirdButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(secondButton);
            make.left.mas_equalTo(secondButton.mas_right).mas_offset(5);
            make.top.mas_equalTo(secondButton);
        }];
        
        [fourthButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(thirdButton);
            make.left.mas_equalTo(thirdButton.mas_right).mas_offset(5);
            make.top.mas_equalTo(thirdButton);
        }];
        thirdButton.hidden = NO;
        fourthButton.hidden = NO;
    }
    
    if (firstButton.isSelected) {
        shadowView.frame = CGRectMake(CGRectGetMinX(firstButton.frame)+(CGRectGetWidth(firstButton.frame)-32)/2, CGRectGetMaxY(firstButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if(secondButton.isSelected){
        shadowView.frame = CGRectMake(CGRectGetMinX(secondButton.frame)+(CGRectGetWidth(secondButton.frame)-32)/2, CGRectGetMaxY(secondButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if (thirdButton.isSelected){
        shadowView.frame = CGRectMake(CGRectGetMinX(thirdButton.frame)+(CGRectGetWidth(thirdButton.frame)-32)/2, CGRectGetMaxY(thirdButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if (fourthButton.isSelected){
        shadowView.frame = CGRectMake(CGRectGetMinX(fourthButton.frame)+(CGRectGetWidth(fourthButton.frame)-32)/2, CGRectGetMaxY(fourthButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }
    
    [self setTabNmuber];
}

- (void)setTabNmuber{
    
    NSString * firstTitle = self.titleArray[0];
    NSString * secondTitle = self.titleArray[1];
    NSString * thirdTitle = self.titleArray[2];

 
    [firstButton setTitle:firstTitle forState:UIControlStateNormal];
    [firstButton setTitle:[NSString stringWithFormat:@" %@",firstTitle] forState:UIControlStateSelected];
    
    [secondButton setTitle:secondTitle forState:UIControlStateNormal];
    [secondButton setTitle:[NSString stringWithFormat:@" %@",secondTitle] forState:UIControlStateSelected];
    
    [thirdButton setTitle:thirdTitle forState:UIControlStateNormal];
    [thirdButton setTitle:[NSString stringWithFormat:@" %@",thirdTitle] forState:UIControlStateSelected];

}

@end
