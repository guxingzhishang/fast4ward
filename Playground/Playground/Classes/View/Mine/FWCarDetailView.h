//
//  FWCarDetailView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWCarDetailSelectBarView.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^arrayBlock)(NSArray * imageArray);

@protocol FWCarDetailViewDelegate <NSObject>

- (void)headerBtnClick:(UIButton *)sender;
- (void)manageButtonOnClick;
- (void)editButtonOnClick;

@end

@interface FWCarDetailView : UIView

@property (nonatomic, copy) arrayBlock myBlock;

@property (nonatomic, strong) UIImageView * topImageView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * shareButton;

@property (nonatomic, strong) UIView * userInfoView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIButton * editButton;

@property (nonatomic, strong) UIView * carInfoView;
@property (nonatomic, strong) UILabel * carNameLabel;
@property (nonatomic, strong) UILabel * maliLabel;
@property (nonatomic, strong) UILabel * chuohaoLabel;
@property (nonatomic, strong) UILabel * ageLabel;

@property (nonatomic, strong) UIButton * likeButton;
@property (nonatomic, strong) UILabel * likeDescLabel;
@property (nonatomic, strong) UILabel * likeLabel;
@property (nonatomic, strong) UIButton * commentButton;
@property (nonatomic, strong) UILabel * commentDescLabel;
@property (nonatomic, strong) UILabel * commentLabel;
@property (nonatomic, strong) UILabel * descLabel;

@property (nonatomic, strong) FWCarDetailSelectBarView * barView;

@property (nonatomic, weak) UIViewController * vc;

/* 点赞的状态 */
@property (nonatomic, strong) NSString * likeType;

@property (nonatomic, weak) id <FWCarDetailViewDelegate>delegate;

- (CGFloat)getCurrentViewHeight;

- (CGFloat)getBarViewTopHeight;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
