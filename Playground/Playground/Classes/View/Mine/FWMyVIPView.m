//
//  FWMyVIPView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyVIPView.h"
#import "FWMyTicketsViewController.h"
#import "FWVIPWebViewController.h"
#import "FWMyCouponViewController.h"

@implementation FWMyVIPView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.topImageView = [[UIImageView alloc] init];
    self.topImageView.userInteractionEnabled = YES;
    self.topImageView.image = [UIImage imageNamed:@"member_bg"];
    [self addSubview:self.topImageView];
    [self.topImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(210);
    }];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = DHSystemFontOfSize_19;
    self.titleLabel.text = @"会员中心";
    self.titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.topImageView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.topImageView);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(100);
        make.top.mas_equalTo(self.topImageView).mas_offset(27+FWCustomeSafeTop);
    }];
    
    self.backButton = [UIButton new];
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.titleLabel);
        make.left.mas_equalTo(self.topImageView).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.layer.cornerRadius = 111/2;
    self.iconImageView.layer.masksToBounds = YES;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    [self.topImageView addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topImageView).mas_offset(30);
        make.bottom.mas_equalTo(self.topImageView).mas_offset(-21);
        make.size.mas_equalTo(CGSizeMake(111, 111));
    }];
    
    self.vipImageButton = [[UIButton alloc] init];
    [self.vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(27);
        make.top.mas_equalTo(self.iconImageView).mas_offset(12);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    self.nameLabel = [UILabel new];
    self.nameLabel.font = DHSystemFontOfSize_16;
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.topImageView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.vipImageButton).mas_offset(0);
        make.height.mas_equalTo(30);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.vipImageButton.mas_right).mas_offset(8);
    }];
    
    self.verImageView = [[UIImageView alloc] init];
    self.verImageView.image = [UIImage imageNamed:@""];
    [self.topImageView addSubview:self.verImageView];
    [self.verImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.iconImageView);
        make.height.mas_equalTo(13);
        make.width.mas_equalTo(3);
        make.left.mas_equalTo(self.vipImageButton);
    }];
    
    self.levelLabel = [UILabel new];
    self.levelLabel.font = DHSystemFontOfSize_14;
    self.levelLabel.textColor = FWViewBackgroundColor_EEEEEE;
    self.levelLabel.textAlignment = NSTextAlignmentLeft;
    [self.topImageView addSubview:self.levelLabel];
    [self.levelLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.verImageView);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(120);
        make.left.mas_equalTo(self.verImageView.mas_right).mas_offset(2);
    }];
    
    self.equityButton = [[UIButton alloc] init];
    self.equityButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    self.equityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.equityButton setTitle:@"   会员权益" forState:UIControlStateNormal];
    [self.equityButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.equityButton addTarget:self action:@selector(equityButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.equityButton];
    [self.equityButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(101, 25));
        make.right.mas_equalTo(self.topImageView).mas_offset(32);
        make.bottom.mas_equalTo(self.iconImageView).mas_offset(-16);
    }];
    
    UIImageView * arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"white_right"];
    [self.equityButton addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.equityButton);
        make.size.mas_equalTo(CGSizeMake(5, 10));
        make.centerX.mas_equalTo(self.equityButton).mas_offset(10);
    }];
    
    self.ticketButton = [[UIButton alloc] init];
    self.ticketButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.ticketButton.titleLabel.font = DHSystemFontOfSize_16;
    [self.ticketButton setTitle:@"我的门票" forState:UIControlStateNormal];
    [self.ticketButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.ticketButton addTarget:self action:@selector(ticketButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.ticketButton];
    [self.ticketButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH - 40, 30));
        make.left.mas_equalTo(self).mas_offset(50);
        make.top.mas_equalTo(self.topImageView.mas_bottom).mas_offset(16);
    }];
    
    UIView * lineView1 = [[UIView alloc] init];
    lineView1.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.ticketButton);
        make.left.mas_equalTo(self).mas_offset(24);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.ticketButton.mas_bottom).mas_offset(10);
    }];
    
    self.couponButton = [[UIButton alloc] init];
    self.couponButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.couponButton.titleLabel.font = DHSystemFontOfSize_16;
    [self.couponButton setTitle:@"我的优惠券" forState:UIControlStateNormal];
    [self.couponButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.couponButton addTarget:self action:@selector(couponButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.couponButton];
    [self.couponButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.ticketButton);
        make.left.mas_equalTo(self.ticketButton);
        make.top.mas_equalTo(self.ticketButton.mas_bottom).mas_offset(20);
    }];
    
    UIView * lineView2 = [[UIView alloc] init];
    lineView2.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:lineView2];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.couponButton);
        make.left.mas_equalTo(lineView1);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.couponButton.mas_bottom).mas_offset(10);
    }];
    
    NSArray * iconArray = @[@"member_ticket",@"member_coupon"];
    for (int i = 0; i < 2; i++) {
        
        UIImageView * iconView = [[UIImageView alloc] init];
        iconView.image = [UIImage imageNamed:iconArray[i]];
        [self addSubview:iconView];
        [iconView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(20, 20));
            make.left.mas_equalTo(self).mas_offset(24);
            if (i == 0) {
                make.centerY.mas_equalTo(self.ticketButton);
            }else if (i == 1){
                make.centerY.mas_equalTo(self.couponButton);
            }
        }];
        
        UIImageView * rightArrowView = [[UIImageView alloc] init];
        rightArrowView.image = [UIImage imageNamed:@"visitor_arrow"];
        [self addSubview:rightArrowView];
        [rightArrowView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(8, 12));
            make.right.mas_equalTo(self).mas_offset(-20);
            if (i == 0) {
                make.centerY.mas_equalTo(self.ticketButton);
            }else if (i == 1){
                make.centerY.mas_equalTo(self.couponButton);
            }
        }];
    }
    
    self.qrcodeImageView = [[UIImageView alloc] init];
//    self.qrcodeImageView.image = [UIImage imageNamed:@"vip_qrcode"];
    [self addSubview:self.qrcodeImageView];
    [self.qrcodeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 100));
        make.bottom.mas_equalTo(self).mas_offset(-50-FWSafeBottom);
        make.centerX.mas_equalTo(self);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"";
    self.tipLabel.font = DHSystemFontOfSize_14;
    self.tipLabel.textColor = FWTextColor_12101D;
    self.tipLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.centerX.mas_equalTo(self);
        make.bottom.mas_equalTo(self.qrcodeImageView.mas_top).mas_offset(-10);
    }];
}

- (void)configViewWithModel:(id)model{
    
    FWMineInfoModel * infoModel = (FWMineInfoModel *)model;
    
    self.nameLabel.text = infoModel.nickname;
    self.levelLabel.text = infoModel.level_name;
    
    NSString * vipImage = @"";
    NSString * verImage = @"";
    NSString * equityImage = @"";

    //vip图标
    if ([infoModel.user_level isEqualToString:@"1"]) {
        
        vipImage = @"vip_primary";
        verImage = @"member_ver_primary";
        equityImage = @"member_equity_primary";
    }else if ([infoModel.user_level isEqualToString:@"2"]) {
        
        vipImage = @"vip_middle";
        verImage = @"member_ver_middle";
        equityImage = @"member_equity_middle";
    }else if ([infoModel.user_level isEqualToString:@"3"]) {
        
        vipImage = @"vip_senior";
        verImage = @"member_ver_senior";
        equityImage = @"member_equity_senior";
    }
    
    self.verImageView.image = [UIImage imageNamed:verImage];
    
    [self.vipImageButton setImage:[UIImage imageNamed:vipImage] forState:UIControlStateNormal];
    [self.vipImageButton setImage:[UIImage imageNamed:vipImage] forState:UIControlStateHighlighted];

    [self.equityButton setBackgroundImage:[UIImage imageNamed:equityImage] forState:UIControlStateNormal];
    [self.equityButton setBackgroundImage:[UIImage imageNamed:equityImage] forState:UIControlStateHighlighted];
}

#pragma mark - > 会员权益
- (void)equityButtonClick{
    
    FWVIPWebViewController * WVC = [[FWVIPWebViewController alloc] init];
    WVC.webTitle = @"会员权益";
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 我的门票
- (void)ticketButtonClick{
    
    FWMyTicketsViewController * TVC = [[FWMyTicketsViewController alloc] init];
    [self.vc.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 我的优惠券
- (void)couponButtonClick{
    
    FWMyCouponViewController * CVC = [[FWMyCouponViewController alloc] init];
    [self.vc.navigationController pushViewController:CVC animated:YES];
}

#pragma mark - > 返回
- (void)backButtonOnClick{

    for (UIViewController * item in self.vc.navigationController.viewControllers) {
        if ([item isKindOfClass:[FWNewMineViewController class]]) {
            [self.vc.navigationController popToViewController:item animated:YES];
            return ;
        }
    }
    
    [self.vc.navigationController popViewControllerAnimated:YES];
}
@end
