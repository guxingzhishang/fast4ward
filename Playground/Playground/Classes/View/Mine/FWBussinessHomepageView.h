//
//  FWBussinessHomepageView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWBussinessHomepageView : UIView

/* 标题 */
@property (nonatomic, strong) UILabel * titleLabel;

/* 顶部背景色 */
@property (nonatomic, strong) UIImageView * topBgImageView;

/* 返回 */
@property (nonatomic, strong) UIButton * blackBackButton;

/* 头像 */
@property (nonatomic, strong) UIImageView * photoImageView;

/* 会员按钮 */
@property (nonatomic, strong) UIButton * vipImageButton;

/* 姓名 */
@property (nonatomic, strong) UILabel * nameLabel;

/* 商家认证图标 */
@property (nonatomic, strong) UIImageView * renzhengImageView;

/* 认证 */
@property (nonatomic, strong) UIImageView * authenticationView;

/* 认证 */
@property (nonatomic, strong) UILabel * authenticationLabel;

@property (nonatomic, weak) UIViewController * vc;


- (void)configViewForModel:(id)model;

@end

NS_ASSUME_NONNULL_END
