//
//  FWMineCarListCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 个人中心 - 座驾
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMineCarListCell : UITableViewCell

@property (nonatomic, strong) UIImageView * carImageView;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * arrowView;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
