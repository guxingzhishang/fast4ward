//
//  FWRefitListView.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWRefitListView.h"
#import "FWCarDetailRefitListViewController.h"

@implementation FWRefitListView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
    
    UILabel * qingdanTitleLabel = [[UILabel alloc] init];
    qingdanTitleLabel.font = DHBoldFont(14);
    qingdanTitleLabel.text = @"完成你的改装清单（必填）";
    qingdanTitleLabel.textColor = FWTextColor_222222;
    qingdanTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:qingdanTitleLabel];
    qingdanTitleLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH-30, 20);

    CGFloat currentY = 25;

    for(int i = 0; i< self.categaryModel.list_gaizhuang_category.count;i++){
       
       /* 第一层数据 */
       FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[i];
       
       UIView * firstView = [[UIView alloc] init];
       [self addSubview:firstView];
       [firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.top.mas_equalTo(self).mas_offset(currentY);
           make.left.right.mas_equalTo(qingdanTitleLabel);
           make.height.mas_equalTo(53);
       }];
       
       UIButton * firstOneKeyButton = [[UIButton alloc] init];
       firstOneKeyButton.tag = 12306+i;
       firstOneKeyButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
       firstOneKeyButton.layer.borderWidth = 1;
       firstOneKeyButton.layer.cornerRadius = 2;
       firstOneKeyButton.layer.masksToBounds = YES;
       firstOneKeyButton.titleLabel.font = DHFont(12);
       [firstOneKeyButton setTitle:@"一键原厂" forState:UIControlStateNormal];
       [firstOneKeyButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
       [firstOneKeyButton addTarget:self action:@selector(onKeyToOriginal:) forControlEvents:UIControlEventTouchUpInside];
       [firstView addSubview:firstOneKeyButton];
       [firstOneKeyButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.right.mas_equalTo(firstView);
           make.size.mas_equalTo(CGSizeMake(108, 24));
           make.centerY.mas_equalTo(firstView);
       }];

       
       UIButton * firstEditButton = [[UIButton alloc] init];
       firstEditButton.tag = 12406+i;
       firstEditButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
       firstEditButton.layer.borderWidth = 1;
       firstEditButton.layer.cornerRadius = 2;
       firstEditButton.layer.masksToBounds = YES;
       firstEditButton.titleLabel.font = DHFont(12);
       [firstEditButton setTitle:@"编辑" forState:UIControlStateNormal];
       [firstEditButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
       [firstEditButton addTarget:self action:@selector(firstEditClick:) forControlEvents:UIControlEventTouchUpInside];
       [firstView addSubview:firstEditButton];
       [firstEditButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.right.mas_equalTo(firstView);
           make.size.mas_equalTo(CGSizeMake(50, 24));
           make.centerY.mas_equalTo(firstView);
       }];
       
       UIButton * firstOrigianlButton = [[UIButton alloc] init];
       firstOrigianlButton.tag = 12506+i;
       firstOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
       firstOrigianlButton.layer.borderWidth = 1;
       firstOrigianlButton.layer.cornerRadius = 2;
       firstOrigianlButton.layer.masksToBounds = YES;
       firstOrigianlButton.titleLabel.font = DHFont(12);
       [firstOrigianlButton setTitle:@"原厂" forState:UIControlStateNormal];
       [firstOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
       [firstOrigianlButton addTarget:self action:@selector(firstOriginalClick:) forControlEvents:UIControlEventTouchUpInside];
       [firstView addSubview:firstOrigianlButton];
       [firstOrigianlButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.right.mas_equalTo(firstEditButton.mas_left).mas_offset(-8);
           make.size.mas_equalTo(CGSizeMake(50, 24));
           make.centerY.mas_equalTo(firstView);
       }];

       UILabel * firstLabel = [[UILabel alloc] init];
       firstLabel.tag = 12606+i;
       firstLabel.userInteractionEnabled = YES;
       firstLabel.text = [NSString stringWithFormat:@"· %@",firstModel.name];
       firstLabel.font = DHSystemFontOfSize_16;
       firstLabel.textColor = FWTextColor_222222;
       firstLabel.textAlignment = NSTextAlignmentLeft;
       [firstView addSubview:firstLabel];
       [firstLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.top.bottom.left.mas_equalTo(firstView);
           make.right.mas_equalTo(firstView).mas_offset(-108);
           make.width.mas_greaterThanOrEqualTo(10);
       }];
       [firstLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(firstQingdanLabelClick:)]];

       
       UIView * firstlineView = [[UIView alloc] init];
       firstlineView.backgroundColor = FWColor(@"F4F4F4");
       [firstView addSubview:firstlineView];
       [firstlineView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.left.right.mas_equalTo(firstView);
           make.height.mas_equalTo(0.5);
           make.top.mas_equalTo(firstView.mas_bottom);
       }];
       
       if (firstModel.firstLevelType == 1) {
           /* 展开，显示一键原厂 */
           firstOneKeyButton.hidden = NO;
           firstEditButton.hidden = YES;
           firstOrigianlButton.hidden = YES;
       }else if (firstModel.firstLevelType == 2){
           /* 编辑完成，全隐藏 */
           firstOneKeyButton.hidden = YES;
           firstEditButton.hidden = YES;
           firstOrigianlButton.hidden = YES;
           firstLabel.userInteractionEnabled = NO;
       }else{
           /* 显示编辑和原厂 */
           firstOneKeyButton.hidden = YES;
           firstEditButton.hidden = NO;
           firstOrigianlButton.hidden = NO;
       }

       
       CGFloat secondCurrentY = 0;
       
       if (firstModel.firstLevelType != 0) {
           
           for (int j = 0; j <firstModel.sub.count; j++) {
               
               /* 第二层数据 */
               FWRefitCategarySubListModel * secondModel = firstModel.sub[j];
               
               UIView * secondView = [[UIView alloc] init];
               [self addSubview:secondView];
               [secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
                   make.top.mas_equalTo(firstView.mas_bottom).mas_offset(secondCurrentY);
                   make.left.right.mas_equalTo(qingdanTitleLabel);
                   make.height.mas_equalTo(53);
                   if (j == firstModel.sub.count-1 &&
                       i == self.categaryModel.list_gaizhuang_category.count - 1) {
                       make.bottom.mas_equalTo(self).mas_offset(-117);
                   }
               }];
               
               UILabel * secondLabel = [[UILabel alloc] init];
               secondLabel.numberOfLines = 2;
               secondLabel.font = DHSystemFontOfSize_14;
               secondLabel.textColor = FWColor(@"999999");
               secondLabel.textAlignment = NSTextAlignmentLeft;
               [secondView addSubview:secondLabel];
               [secondLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                   make.top.bottom.left.mas_equalTo(secondView);
                   make.right.mas_equalTo(secondView).mas_offset(-108);
                   make.width.mas_greaterThanOrEqualTo(10);
               }];
               
               UIView * secondlineView = [[UIView alloc] init];
               secondlineView.backgroundColor = FWColor(@"F4F4F4");
               [secondView addSubview:secondlineView];
               [secondlineView mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.right.mas_equalTo(secondView);
                   make.height.mas_equalTo(0.5);
                   make.top.mas_equalTo(secondView.mas_bottom);
               }];
               
               FWBlockButton * secondEditButton = [[FWBlockButton alloc] init];
               secondEditButton.tag = 124060+i;
               secondEditButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
               secondEditButton.layer.borderWidth = 1;
               secondEditButton.layer.cornerRadius = 2;
               secondEditButton.layer.masksToBounds = YES;
               secondEditButton.titleLabel.font = DHFont(12);
               [secondEditButton setTitle:@"编辑" forState:UIControlStateNormal];
               [secondEditButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
               [secondView addSubview:secondEditButton];
               [secondEditButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                   make.right.mas_equalTo(secondView);
                   make.size.mas_equalTo(CGSizeMake(50, 24));
                   make.centerY.mas_equalTo(secondView);
               }];
               
               FWBlockButton * secondOrigianlButton = [[FWBlockButton alloc] init];
               secondOrigianlButton.tag = 125060+i;
               secondOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
               secondOrigianlButton.layer.borderWidth = 1;
               secondOrigianlButton.layer.cornerRadius = 2;
               secondOrigianlButton.layer.masksToBounds = YES;
               secondOrigianlButton.titleLabel.font = DHFont(12);
               [secondOrigianlButton setTitle:@"原厂" forState:UIControlStateNormal];
               [secondOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
               [secondView addSubview:secondOrigianlButton];
               [secondOrigianlButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                   make.right.mas_equalTo(secondEditButton.mas_left).mas_offset(-8);
                   make.size.mas_equalTo(CGSizeMake(50, 24));
                   make.centerY.mas_equalTo(secondView);
               }];
               
               __weak typeof(self) weakSelf = self;
               [secondEditButton addButtonTapBlock:^(UIButton * _Nonnull sender) {
                   /* 二级编辑 */
                   FWEditRefitModuleViewController * RMVC = [[FWEditRefitModuleViewController alloc] init];
                   RMVC.type = secondModel.name;
                   RMVC.dict = secondModel.secondDict;
                   RMVC.moduleBlock = ^(NSDictionary * _Nonnull moduleDict) {

                       for (int k = 0;k < secondModel.list.count; k++) {
                           if ([secondModel.list[k].name isEqualToString:@"品牌"]) {
                                secondModel.list[k].val = moduleDict[@"品牌"];
                            }else  if ([secondModel.list[k].name isEqualToString:@"型号"]) {
                                secondModel.list[k].val = moduleDict[@"型号"];
                            }else  if ([secondModel.list[k].name isEqualToString:@"规格"]) {
                                secondModel.list[k].val = moduleDict[@"规格"];
                            }else  if ([secondModel.list[k].name isEqualToString:@"费用"]) {
                                secondModel.list[k].val = moduleDict[@"费用"];
                            }
                            secondModel.secondDict = moduleDict;
                       }

                       secondModel.tempName = [NSString stringWithFormat:@"%@\n%@",secondModel.name,moduleDict[@"moduleString"]];
                       secondLabel.text = secondModel.tempName;
                       secondLabel.textColor = FWTextColor_222222;
                       
                       secondOrigianlButton.enabled = YES;
                       secondOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
                       [secondOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];

                       
                       secondModel.secondLevelType = 2;
                       firstModel.isFirstOriginalType = 2;
                   };
                   [weakSelf.vc.navigationController pushViewController:RMVC animated:YES];
               }];
               
               
               /* 非一键原厂才可以点击设置原厂 */
               [secondOrigianlButton addButtonTapBlock:^(UIButton * _Nonnull sender) {
                   if (firstModel.isFirstOriginalType != 1) {
                       /* 二级设置原厂 */
                       secondModel.secondLevelType = 1;
                       
                       secondModel.tempName = [NSString stringWithFormat:@"%@-原厂",secondModel.name];
                       secondLabel.text = secondModel.tempName;
                       secondLabel.textColor = FWColor(@"222222");
                       
                       secondOrigianlButton.enabled = NO;
                       secondOrigianlButton.layer.borderColor = FWColor(@"EEEEEE").CGColor;
                       [secondOrigianlButton setTitleColor:FWViewBackgroundColor_EEEEEE forState:UIControlStateNormal];
                   }
               }];

               if (secondModel.tempName.length > 0) {
                   secondLabel.text = secondModel.tempName;
               }else{
                   secondLabel.text = secondModel.name;
               }

               if (secondModel.secondLevelType == 0) {
                   /* 初始状态 */
                   secondLabel.textColor = FWColor(@"999999");
                   
                   secondOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
                   [secondOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
               }else{
                   secondLabel.textColor = FWColor(@"666666");
                   if (secondModel.secondLevelType == 1) {
                       /* 原厂 */
                       secondOrigianlButton.enabled = NO;
                       secondOrigianlButton.layer.borderColor = FWColor(@"EEEEEE").CGColor;
                       [secondOrigianlButton setTitleColor:FWViewBackgroundColor_EEEEEE forState:UIControlStateNormal];
                   }else{
                       /* 非原厂 */
                       secondOrigianlButton.enabled = YES;
                       secondOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
                       [secondOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
                   }
               }
               
               [self layoutIfNeeded];
               secondCurrentY = secondCurrentY+CGRectGetHeight(secondView.frame);
           }

       }else{
           [firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
               make.top.mas_equalTo(self).mas_offset(currentY);
               make.left.right.mas_equalTo(qingdanTitleLabel);
               make.height.mas_equalTo(53);
               if (i == self.categaryModel.list_gaizhuang_category.count - 1) {
                   make.bottom.mas_equalTo(self).mas_offset(-117);
               }
           }];
       }
       
       [self layoutIfNeeded];
       currentY = currentY + CGRectGetHeight(firstView.frame) +secondCurrentY;
    }

    UIButton * saveButton = [[UIButton alloc] init];
    saveButton.layer.cornerRadius = 2;
    saveButton.layer.masksToBounds = YES;
    saveButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    saveButton.backgroundColor = FWTextColor_222222;
    [saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [saveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:saveButton];
    [saveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(300, 36));
        make.bottom.mas_equalTo(self).mas_offset(-38-FWSafeBottom);
    }];
    
    if (self.heightBlock) {
        self.heightBlock(currentY);
    }
}

#pragma mark - > 保存
- (void)saveButtonClick{
    
    /* 改装清单 */
    NSMutableArray * qingdanArray = @[].mutableCopy;
    for(int i = 0; i< self.categaryModel.list_gaizhuang_category.count;i++){
        
        /* 第一层数据 */
        NSMutableArray * subArray = @[].mutableCopy;
        FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[i];
        
        for (int j = 0; j <firstModel.sub.count; j++) {
        
            /* 第二层数据 */
            NSMutableArray * listArray = @[].mutableCopy;
            FWRefitCategarySubListModel * secondModel = firstModel.sub[j];
            
            if (secondModel.tempName.length > 0) {
                if ([secondModel.tempName containsString:@"原厂"]) {
                    /* 原厂 */
                    secondModel.val = @"原厂";
                    
                    for (int k = 0; k<secondModel.list.count; k++) {
                         
                        NSDictionary * thirdDict = @{
                            @"name":secondModel.list[k].name,
                            @"val":@"",
                            @"type":secondModel.list[k].type,
                         };
                        
                        [listArray addObject:thirdDict];
                    }
                }else {
                    /* 编辑了 */
                    secondModel.val = @"";
                    
                    if (secondModel.list.count > 0) {
                        for (int k = 0; k<secondModel.list.count; k++) {
                           
                            NSDictionary * thirdDict = @{
                                @"name":secondModel.list[k].name,
                                @"val":secondModel.list[k].val,
                                @"type":secondModel.list[k].type,
                             };
                            
                            [listArray addObject:thirdDict];
                        }
                    }else{
                        
                    }
                }
            }else{
                //  改装清单没填写完
//                [[FWHudManager sharedManager] showErrorMessage:@"请完善改装清单" toController:self.vc];
//                return;

                for (int k = 0; k<secondModel.list.count; k++) {

                    NSDictionary * thirdDict = @{
                        @"name":secondModel.list[k].name,
                        @"val":secondModel.list[k].val,
                        @"type":secondModel.list[k].type,
                     };

                    [listArray addObject:thirdDict];
                }
                NSLog(@"listArray===%@",listArray);
            }
            
            NSDictionary * secondDict = @{
                @"name":secondModel.name,
                @"type":secondModel.type,
                @"val":secondModel.val,
                @"list":listArray,
            };
            
            [subArray addObject:secondDict];
        }
        
        NSDictionary * firstDict = @{
            @"name":firstModel.name,
            @"sub":subArray,
        };
        
        [qingdanArray addObject:firstDict];
    }

    NSString* qingdanString = [qingdanArray mj_JSONString];

    if ([self.type isEqualToString:@"1"]) {
        

        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];

        NSDictionary * params = @{
                               @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                               @"gaizhuang_list":qingdanString,
        };

        [request startWithParameters:params WithAction:Format_car_gaizhuang_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
           
           NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
           
           if ([code isEqualToString:NetRespondCodeSuccess]) {
               
               NSArray * listArray = [[back objectForKey:@"data"] objectForKey:@"gaizhuang_list_show"];
         
               FWCarDetailRefitListViewController * carVC = (FWCarDetailRefitListViewController *)self.vc;
               if (carVC.showBlock) {
                   NSDictionary * dict = @{
                       @"commit":qingdanString,
                       @"show":listArray,
                   };
                   carVC.showBlock(dict);
               }
               
               [self.vc.navigationController popViewControllerAnimated:YES];
           }else{
               [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
           }
        }];
    }else{
        
        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
        NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"gaizhuang_list":qingdanString,
                                @"user_car_id":self.user_car_id,
        };
        
        [request startWithParameters:params WithAction:Submit_update_car WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                [self.vc.navigationController popViewControllerAnimated:YES];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}

#pragma mark - > 一键原厂
- (void)onKeyToOriginal:(UIButton *)sender{
    NSInteger index = sender.tag - 12306;
    
    FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[index];
    
    if (firstModel.isFirstOriginalType != 1) {
        /* 非一键原厂才可以设置 */
        firstModel.isFirstOriginalType = 1;
        firstModel.firstLevelType = 2;

        for (int i =0; i<firstModel.sub.count; i++) {
            FWRefitCategarySubListModel * model = firstModel.sub[i];
            model.secondLevelType = 1;
            model.tempName = [NSString stringWithFormat:@"%@-原厂",model.name];
        }
        
        [self setupSubviews];
    }
}


#pragma mark - > 第一级原厂
- (void)firstOriginalClick:(UIButton *)sender{
    NSInteger index = sender.tag - 12506;
    
    FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[index];

    if (firstModel.isFirstOriginalType != 1) {
        
        [self showFirstSubView:index];
        
        /* 非一键原厂才可以设置 */
        firstModel.isFirstOriginalType = 1;
        firstModel.firstLevelType = 2;
        
         for (int i =0; i<firstModel.sub.count; i++) {
             FWRefitCategarySubListModel * model = firstModel.sub[i];
             model.secondLevelType = 1;
             model.tempName = [NSString stringWithFormat:@"%@-原厂",model.name];
         }
        [self setupSubviews];
    }
}

#pragma mark - > 第一级点击item展示第二级
- (void)firstEditClick:(UIButton *)sender{
    NSInteger index = sender.tag - 12406;
    [self showFirstSubView:index];
}

- (void)firstQingdanLabelClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 12606;
    [self showFirstSubView:index];
}

- (void)showFirstSubView:(NSInteger)index{
    
    FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[index];

    if (firstModel.isFirstOriginalType != 0) {
        /* 一旦设置后就禁止在收缩起来 */
        return;
    }
    
    for (int i =0; i<firstModel.sub.count; i++) {
        FWRefitCategarySubListModel * model = firstModel.sub[i];
        if (model.secondLevelType != 0) {
            /* 只要有一个修改过就不允许缩起来 */
            return;
        }
    }
    
    if (self.categaryModel.list_gaizhuang_category[index].firstLevelType == 1) {
        self.categaryModel.list_gaizhuang_category[index].firstLevelType = 0;
    }else{
        self.categaryModel.list_gaizhuang_category[index].firstLevelType = 1;
    }
    [self setupSubviews];
}

- (void)setCategaryModel:(FWRefitCategaryModel *)categaryModel{
    _categaryModel = categaryModel;
    [self setupSubviews];
}

@end
