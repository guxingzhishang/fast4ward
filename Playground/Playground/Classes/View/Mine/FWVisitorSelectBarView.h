//
//  FWVisitorSelectBarView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWVisitorSelectBarViewDelegate <NSObject>

- (void)didClickHeadButton:(NSInteger)index;

@end

@interface FWVisitorSelectBarView : UIView

/**
 * 灰色横线
 */
@property (nonatomic, strong) UIView * honOneView;

/**
 * 访客名单按钮
 */
@property (nonatomic, strong) UIButton * nameListButton;

/**
 * 访客分析按钮
 */
@property (nonatomic, strong) UIButton * analysisButton;

@property (nonatomic, strong) UIView * shadowView;

@property (nonatomic, weak) id<FWVisitorSelectBarViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
