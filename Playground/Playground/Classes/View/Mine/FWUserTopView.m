//
//  FWUserTopView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUserTopView.h"
#import "FWFollowViewController.h"
#import "FWFunsViewController.h"
#import "FWShopInfoDetailViewController.h"
#import "FWPlayerArchivesViewController.h"

#define PHOTOWIDTH 111

@implementation FWUserTopView
@synthesize blackShareButton;
@synthesize blackBackButton;
@synthesize blackMoreButton;
@synthesize topBgImageView;
@synthesize photoImageView;
@synthesize nameLabel;
@synthesize vipImageButton;
@synthesize thumbUpButton;
@synthesize followButton;
@synthesize funsButton;
@synthesize background;
@synthesize signLabel;
@synthesize mineModel;
@synthesize preHeader_URL;
@synthesize authenticationView;
@synthesize authenticationLabel;
@synthesize effectButton;
@synthesize effectImageView;
@synthesize effectLabel;
@synthesize f4wIDLabel;
@synthesize attentionButton;
@synthesize signImageView;
@synthesize selectBar;
@synthesize shopButton;
@synthesize carImageView;
@synthesize carLabel;
@synthesize carPlayerView;
@synthesize carPlayerLabel;
@synthesize playerArrowImageView;
@synthesize flagLineView;

- (instancetype)init{
    
    self = [super init];
    
    if (self) {
        
        preHeader_URL = @"";
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.userInteractionEnabled = YES;
        [self setupTopSubviews];
    }
    
    return self;
}

#pragma mark - > 初始化头部视图
- (void)setupTopSubviews{
    
    topBgImageView = [[UIImageView alloc] init];
    topBgImageView.userInteractionEnabled = YES;
    topBgImageView.image = [UIImage imageNamed:@"user_bg"];
    [self addSubview:topBgImageView];
    topBgImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 297+FWCustomeSafeTop);
   
    
    blackBackButton = [[UIButton alloc] init];
    [blackBackButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [blackBackButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:blackBackButton];
    [blackBackButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topBgImageView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(topBgImageView).mas_offset(30+FWCustomeSafeTop);
    }];
    
    blackMoreButton = [[UIButton alloc] init];
    [blackMoreButton setImage:[UIImage imageNamed:@"white_more"] forState:UIControlStateNormal];
    [blackMoreButton addTarget:self action:@selector(moreButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:blackMoreButton];
    [blackMoreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(topBgImageView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(0.1, 0.1));
        make.centerY.mas_equalTo(blackBackButton);
    }];
    
    
    blackShareButton = [[UIButton alloc] init];
    [blackShareButton setImage:[UIImage imageNamed:@"new_mine_share_white"] forState:UIControlStateNormal];
    [blackShareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:blackShareButton];
    [blackShareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(blackMoreButton.mas_left).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(36, 20));
        make.centerY.mas_equalTo(blackBackButton);
    }];
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        blackShareButton.hidden = NO;
    }else{
        blackShareButton.hidden = YES;
    }
    
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.userInteractionEnabled = YES;
    photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    photoImageView.layer.cornerRadius = PHOTOWIDTH/2;
    photoImageView.layer.masksToBounds = YES;
    [topBgImageView addSubview:photoImageView];
    [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topBgImageView).mas_offset(30);
        make.top.mas_equalTo(topBgImageView).mas_offset(70+FWCustomeSafeTop);
        make.size.mas_equalTo(CGSizeMake(PHOTOWIDTH, PHOTOWIDTH));
    }];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cleckImageViewAction)];
    [photoImageView addGestureRecognizer:tapGesture];
    
    self.renzhengImageView = [[UIImageView alloc] init];
    self.renzhengImageView.image = [UIImage imageNamed:@"bussiness_renzheng"];
    [topBgImageView addSubview:self.renzhengImageView];
    [self.renzhengImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(photoImageView);
        make.bottom.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(75, 21));
    }];
    self.renzhengImageView.hidden = YES;
    
    self.vipImageButton = [[UIButton alloc] init];
    [topBgImageView addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(photoImageView.mas_right).mas_offset(15);
        make.top.mas_equalTo(self.photoImageView).mas_offset(3);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 22];
    nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [topBgImageView addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.vipImageButton);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(self.vipImageButton.mas_right).mas_offset(2);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-10);
    }];
    
    f4wIDLabel = [[UILabel alloc] init];
    f4wIDLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    f4wIDLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.3);
    f4wIDLabel.textAlignment = NSTextAlignmentLeft;
    [topBgImageView addSubview:f4wIDLabel];
    [f4wIDLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(40);
        make.top.mas_equalTo(nameLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(15);
    }];
    
    shopButton = [[UIButton alloc] init];
    shopButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [shopButton setImage:[UIImage imageNamed:@"Ta_shop"] forState:UIControlStateNormal];
    [shopButton addTarget:self action:@selector(shopButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:shopButton];
    [shopButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(topBgImageView);
        make.size.mas_equalTo(CGSizeMake(101, 28));
        make.centerY.mas_equalTo(f4wIDLabel);
    }];
    shopButton.hidden = YES;

    
    authenticationView = [[UIImageView alloc] init];
    authenticationView.image = [UIImage imageNamed:@"primary_bussiness"];
    [topBgImageView addSubview:authenticationView];
    [authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.left.mas_equalTo(f4wIDLabel).mas_offset(0);
        make.top.mas_equalTo(f4wIDLabel.mas_bottom).mas_offset(8);
    }];
    authenticationView.hidden = YES;
    
    authenticationLabel = [[UILabel alloc] init];
    authenticationLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    authenticationLabel.textAlignment = NSTextAlignmentLeft;
    authenticationLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.3);
    [topBgImageView addSubview:authenticationLabel];
    [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(15);
        make.centerY.mas_equalTo(authenticationView);
        make.left.mas_equalTo(authenticationView.mas_right).mas_offset(5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-5);
    }];
    authenticationLabel.hidden = YES;
    
    effectButton = [[UIButton alloc] init];
    effectButton.layer.cornerRadius = 25/2;
    effectButton.layer.masksToBounds = YES;
    effectButton.backgroundColor = FWTextColor_68769F;
    [topBgImageView addSubview:effectButton];
    [effectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(f4wIDLabel);
        make.height.mas_equalTo( 25);
        make.bottom.mas_equalTo(photoImageView);
        make.width.mas_greaterThanOrEqualTo (20);
    }];
    
    effectLabel = [[UILabel alloc] init];
    effectLabel.text = @"影响力";
    effectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:10];
    effectLabel.textColor = FWViewBackgroundColor_FFFFFF;
    effectLabel.textAlignment = NSTextAlignmentCenter;
    [effectButton addSubview:effectLabel];
    [effectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(effectButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(effectButton).mas_offset(30);
        make.right.mas_equalTo(effectButton).mas_offset(-15);
        make.centerY.mas_equalTo(effectButton);
    }];
    
    effectImageView = [[UIImageView alloc] init];
    effectImageView.image = [UIImage imageNamed:@"mine_effect"];
    [effectButton addSubview:effectImageView];
    [effectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(14, 14));
        make.centerY.mas_equalTo(effectButton);
        make.right.mas_equalTo(effectLabel.mas_left).mas_offset(-5);
    }];
    
    attentionButton = [[UIButton alloc] init];
    attentionButton.layer.cornerRadius = 2;
    attentionButton.layer.masksToBounds = YES;
    attentionButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10.2];
    [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [attentionButton setTitle:@"已关注" forState:UIControlStateSelected];
    [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [attentionButton setTitleColor:FWTextColor_515151 forState:UIControlStateSelected];
    [attentionButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateNormal];
    [attentionButton setBackgroundImage:[FWViewBackgroundColor_EEEEEE image] forState:UIControlStateSelected];
    [topBgImageView addSubview:attentionButton];
    [attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(54, 25));
        make.centerY.mas_equalTo(effectButton);
        make.left.mas_equalTo(effectButton.mas_right).mas_offset(10);
    }];

    thumbUpButton = [[UIButton alloc] init];
    thumbUpButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    thumbUpButton.backgroundColor = [UIColor clearColor];
    thumbUpButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    thumbUpButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [thumbUpButton setTitle:@"获赞" forState:UIControlStateNormal];
    [thumbUpButton setTitleColor:FWTextColor_919191 forState:UIControlStateNormal];
    [topBgImageView addSubview:thumbUpButton];
    [thumbUpButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.width.mas_equalTo((SCREEN_WIDTH-84)/3);
        make.left.mas_equalTo(topBgImageView).mas_offset(42);
        make.top.mas_equalTo(photoImageView.mas_bottom).mas_offset(20);
    }];
    
    followButton = [[UIButton alloc] init];
    followButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    followButton.backgroundColor = [UIColor clearColor];
    followButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [followButton setTitle:@"关注" forState:UIControlStateNormal];
    [followButton setTitleColor:FWTextColor_919191 forState:UIControlStateNormal];
    [topBgImageView addSubview:followButton];
    [followButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(thumbUpButton);
        make.width.mas_equalTo(thumbUpButton);
        make.left.mas_equalTo(thumbUpButton.mas_right);
        make.top.mas_equalTo(thumbUpButton);
    }];
    
    funsButton = [[UIButton alloc] init];
    funsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    funsButton.backgroundColor = [UIColor clearColor];
    funsButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [funsButton setTitle:@"粉丝" forState:UIControlStateNormal];
    [funsButton setTitleColor:FWTextColor_919191 forState:UIControlStateNormal];
    [topBgImageView addSubview:funsButton];
    [funsButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(followButton);
        make.width.mas_equalTo(followButton);
        make.left.mas_equalTo(followButton.mas_right);
        make.top.mas_equalTo(followButton);
    }];
    
    
    
    for ( int i = 0; i<2; i++) {
        UIView * lineView = [[UIView alloc] init];
        lineView.backgroundColor = FWColorWihtAlpha(@"E1E1E1", 1);
        [topBgImageView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.left.mas_equalTo(thumbUpButton.mas_right);
            }else{
                make.left.mas_equalTo(followButton.mas_right);
            }
            make.centerY.mas_equalTo(thumbUpButton);
            make.height.mas_equalTo(11);
            make.width.mas_equalTo(1);
        }];
    }
    
    flagLineView = [[UIView alloc] init];
    flagLineView.backgroundColor = FWColorWihtAlpha(@"FFFFFF", 0.3);
    [topBgImageView addSubview:flagLineView];
    [flagLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(thumbUpButton);
        make.right.mas_equalTo(funsButton);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(thumbUpButton.mas_bottom).mas_offset(5);
    }];
    
    carPlayerView = [[UIImageView alloc] init];
    carPlayerView.image = [UIImage imageNamed:@"player_certificate"];
    [topBgImageView addSubview:carPlayerView];
    
    
    carPlayerLabel = [[UILabel alloc] init];
    carPlayerLabel.userInteractionEnabled = YES;
    carPlayerLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    carPlayerLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.5);;
    carPlayerLabel.textAlignment = NSTextAlignmentLeft;
    [topBgImageView addSubview:carPlayerLabel];
    UITapGestureRecognizer * carPlayerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(carPlayerClick)];
    [carPlayerLabel addGestureRecognizer:carPlayerTap];
    
    
    playerArrowImageView = [[UIImageView alloc] init];
    playerArrowImageView.image = [UIImage imageNamed:@"mine_car_arrow"];
    [topBgImageView addSubview:playerArrowImageView];
    
    
    carImageView = [[UIImageView alloc] init];
    carImageView.image = [UIImage imageNamed:@"car_certificate"];
    [topBgImageView addSubview:carImageView];
    
    carLabel = [[UILabel alloc] init];
    carLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    carLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.5);;
    carLabel.textAlignment = NSTextAlignmentLeft;
    [topBgImageView addSubview:carLabel];
 
    
    self.carPlayerLabel.hidden = YES;
    self.carPlayerView.hidden = YES;
    
    self.carImageView.hidden = YES;
    self.carLabel.hidden = YES;
    
    signImageView = [[UIImageView alloc] init];
    signImageView.image = [UIImage imageNamed:@"mine_sign"];
    [topBgImageView addSubview:signImageView];
    
    signLabel = [[UILabel alloc] init];
    signLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    signLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.5);;
    signLabel.textAlignment = NSTextAlignmentLeft;
    [topBgImageView addSubview:signLabel];
    
    
    selectBar = [[FWSelectBarView alloc] init];
    selectBar.honOneView.hidden = YES;
    selectBar.honTwoView.hidden = YES;
    selectBar.titleArray = @[@"视频",@"图文",@"草稿"];
    selectBar.numType = 3;
    [self addSubview:selectBar];
    [selectBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 50));
        make.top.mas_equalTo(topBgImageView.mas_bottom).mas_offset(0);
    }];
}

- (void)setMineViewType:(FWMineViewType)mineViewType{
    
    _mineViewType = mineViewType;
    
    if (mineViewType == FWMineType) {
        selectBar.titleArray = @[@"视频",@"图文",@"座驾",@"草稿"];
        selectBar.numType = 4;
    }else if(mineViewType == FWUserType) {
        selectBar.titleArray = @[@"视频",@"图文",@"座驾"];
        selectBar.numType = 3;
    }
}

- (void)backButtonClick{
    if ([self.delegate respondsToSelector:@selector(backButtonOnClick)]) {
        [self.delegate backButtonOnClick];
    }
}

- (void)moreButtonClick{
    if ([self.delegate respondsToSelector:@selector(moreButtonOnClick)]) {
        [self.delegate moreButtonOnClick];
    }
}

- (void)shareButtonClick{
    if ([self.delegate respondsToSelector:@selector(shareButtonOnClick)]) {
        [self.delegate shareButtonOnClick];
    }
}

#pragma mark - > 初始化所有按钮的点击事件，并把控制器传进来
- (void)initClickMethodWithController:(UIViewController *)delegate{
    
    self.viewcontroller = delegate;
    
    [attentionButton addTarget:self action:@selector(attentionButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [effectButton addTarget:self action:@selector(effectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [thumbUpButton addTarget:self action:@selector(thumbUpButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [followButton addTarget:self action:@selector(followButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [funsButton addTarget:self action:@selector(funsButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [selectBar.firstButton addTarget:self action:@selector(firstButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [selectBar.secondButton addTarget:self action:@selector(secondButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [selectBar.thirdButton addTarget:self action:@selector(thirdButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [selectBar.fourthButton addTarget:self action:@selector(fourthButtonClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (NSMutableAttributedString *)buttonAttributeWithTitle1:(NSString *)title1 WithTitle2:(NSString *)title2{
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",title1,title2]];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Medium" size: 15.2] range:NSMakeRange(0,title1.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Regular" size: 12] range:NSMakeRange(title1.length,title2.length)];
    
    [str addAttribute:NSForegroundColorAttributeName value:FWViewBackgroundColor_FFFFFF range:NSMakeRange(0,title1.length)];
    [str addAttribute:NSForegroundColorAttributeName value:FWColorWihtAlpha(@"FFFFFF", 0.5) range:NSMakeRange(title1.length,title2.length)];
    
    return str;
}

- (void)configForView:(id)model{
    
    self.mineModel = (FWMineModel*)model;
    
    [selectBar setTabNmuber:model];

    FWUserDefaultsVariableModel * uvam = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    if([self.mineModel.user_info.uid isEqualToString:uvam.userModel.uid]){
        [blackMoreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(topBgImageView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(0.1, 0.1));
            make.centerY.mas_equalTo(blackBackButton);
        }];
    }else{
        [blackMoreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(topBgImageView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(blackBackButton);
        }];
    }
    [blackShareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(blackMoreButton.mas_left).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(36, 20));
        make.centerY.mas_equalTo(blackBackButton);
    }];
    
    [self dealwithBottomSTH];
    
    nameLabel.text = mineModel.user_info.nickname;
    signLabel.text = mineModel.user_info.autograph;
    f4wIDLabel.text = [NSString stringWithFormat:@"ID：%@",mineModel.user_info.f4w_id];
    effectLabel.text = [NSString stringWithFormat:@"影响力%@",mineModel.user_info.effect_value];
    
    if ([self.mineModel.user_info.cert_status isEqualToString:@"2"] || [self.mineModel.user_info.merchant_cert_status isEqualToString:@"3"]) {
        shopButton.hidden = NO;
    }else{
        shopButton.hidden = YES;
    }
    
    [thumbUpButton setAttributedTitle:[self buttonAttributeWithTitle1:mineModel.beliked_count WithTitle2:@" 获赞"] forState:UIControlStateNormal];
    [followButton setAttributedTitle:[self buttonAttributeWithTitle1:mineModel.follow_count WithTitle2:@" 关注"] forState:UIControlStateNormal];
    [funsButton setAttributedTitle:[self buttonAttributeWithTitle1:mineModel.fans_count WithTitle2:@" 粉丝"] forState:UIControlStateNormal];

    if ([mineModel.user_info.cert_status isEqualToString:@"2"]){
        self.renzhengImageView.hidden = NO;
    }else{
        self.renzhengImageView.hidden = YES;
    }
    
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(photoImageView.mas_right).mas_offset(15);
        make.top.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    CGFloat offset = 2;
    
    self.vipImageButton.enabled = YES;
    if ([mineModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([mineModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([mineModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self.vipImageButton.enabled = NO;

        [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(photoImageView.mas_right).mas_offset(14);
            make.top.mas_equalTo(self.photoImageView);
            make.size.mas_equalTo(CGSizeMake(0.5, 17));
        }];
        
        offset = -3;
    }
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.vipImageButton);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(self.vipImageButton.mas_right).mas_offset(offset);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-10);
    }];
    
    
    if (mineModel.user_info.autograph.length <= 0) {
        signLabel.text = @"这个家伙很懒 什么都没留下~";
    }
    
    photoImageView.layer.cornerRadius = PHOTOWIDTH/2;
    photoImageView.layer.masksToBounds = YES;
    [photoImageView sd_setImageWithURL:[NSURL URLWithString:mineModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    if ([mineModel.follow_status isEqualToString:@"2"]) {
        attentionButton.selected = NO;
    }else {
        attentionButton.selected = YES;
    }
    
    
    if (mineModel.user_info.title.length > 0) {
        
        if ([mineModel.user_info.cert_status isEqualToString:@"2"]){
            authenticationView.hidden = NO;
            authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
            
            [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(15);
                make.centerY.mas_equalTo(authenticationView);
                make.left.mas_equalTo(authenticationView.mas_right).mas_offset(5);
                make.width.mas_greaterThanOrEqualTo(10);
                make.right.mas_equalTo(topBgImageView).mas_offset(-5);
            }];
        }else{
            authenticationView.hidden = YES;
            [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(15);
                make.centerY.mas_equalTo(authenticationView);
                make.left.mas_equalTo(f4wIDLabel);
                make.width.mas_greaterThanOrEqualTo(10);
                make.right.mas_equalTo(topBgImageView).mas_offset(-5);
            }];
        }
        authenticationLabel.hidden = NO;
        authenticationLabel.text = mineModel.user_info.title;
        
    }else{
        authenticationView.hidden = YES;
        authenticationLabel.hidden = YES;
    }
}

#pragma mark - > 添加关注
- (void)attentionButtonClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        NSString * action ;
        
        if ([mineModel.follow_status isEqualToString:@"2"]) {
            action = Submit_follow_users;
        }else{
            action = Submit_cancel_follow_users;
        }
        
        FWFollowRequest * request = [[FWFollowRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"f_uid":mineModel.user_info.uid,
                                  };
        [request startWithParameters:params WithAction:action WithDelegate:self .viewController completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([action isEqualToString:Submit_follow_users]) {
                    mineModel.follow_status = [[back objectForKey:@"data"] objectForKey:@"follow_status"];
                    attentionButton.selected = YES;
                    [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self.viewcontroller];
                }else{
                    mineModel.follow_status = @"2";
                    attentionButton.selected = NO;
                }
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewcontroller];
            }
        }];
    }
}

#pragma mark - > 影响力
- (void)effectButtonClick{
    
//    if ([self.mineModel.user_info.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
//
//        FWWebViewController * WVC = [[FWWebViewController alloc] init];
//        WVC.pageType = WebViewTypeURL;
//        WVC.htmlStr = self.mineModel.h5_jifen;
//        [self.viewcontroller.navigationController pushViewController:WVC animated:YES];
//    }
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.viewcontroller.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 点赞
- (void)thumbUpButtonClick{
    
    NSString * userName = mineModel.user_info.nickname?mineModel.user_info.nickname:@"";
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"获赞数" message:[NSString stringWithFormat:@"%@一共获得 %@ 个赞",userName,mineModel.beliked_count] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alertController addAction:okAction];
    [self.viewcontroller presentViewController:alertController animated:YES completion:nil];
    
    //自定义弹窗样式
    //    thumbUpView =  [[FWThumbUpView defaultShareView] init];
    //    thumbUpView.contentLabel.text = [NSString stringWithFormat:@"一共获得 %@ 个赞",mineModel.beliked_count];
    //    [thumbUpView showView];
}

#pragma mark - > 关注
- (void)followButtonClick{
    
    FWFollowViewController * FVC = [[FWFollowViewController alloc] init];
    if (self.mineViewType == FWMineType) {
        FVC.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
    }else{
        FVC.base_uid = mineModel.user_info.uid;
    }
    [self.viewcontroller.navigationController pushViewController:FVC animated:YES];
}

#pragma mark - > 粉丝
- (void)funsButtonClick{
    
    FWFunsViewController * FVC = [[FWFunsViewController alloc] init];
    if (self.mineViewType == FWMineType) {
        FVC.funsType = @"2";
        FVC.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
    }else{
        FVC.funsType = @"3";
        FVC.base_uid = mineModel.user_info.uid;
    }
    [self.viewcontroller.navigationController pushViewController:FVC animated:YES];
}

#pragma mark - > 全部
- (void)firstButtonClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > 图片
- (void)secondButtonClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > 座驾/草稿箱
- (void)thirdButtonClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > 草稿箱
- (void)fourthButtonClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > TA的店铺
- (void)shopButtonClick{
    
    if (!mineModel.user_info.uid) {
        return;
    }
    
    if ([self.mineModel.user_info.cert_status isEqualToString:@"2"]){
        // 店铺页
        FWCustomerShopViewController * UIVC = [[FWCustomerShopViewController alloc] init];
        UIVC.user_id = mineModel.user_info.uid;
        UIVC.isUserPage = YES;
        [self.viewController.navigationController pushViewController:UIVC animated:YES];
    }else{
        // 非付费商家 进入介绍页
        FWShopInfoDetailViewController  * UIVC = [[FWShopInfoDetailViewController alloc] init];
        UIVC.user_id = mineModel.user_info.uid;
        UIVC.isUserPage = YES;
        [self.viewController.navigationController pushViewController:UIVC animated:YES];
    }
}

#pragma mark - > 查看大头像
- (void) cleckImageViewAction{
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    background = bgView;
    [bgView setBackgroundColor:[UIColor colorWithRed:0/250.0 green:0/250.0 blue:0/250.0 alpha:1.0]];
    
    [[UIApplication sharedApplication].keyWindow addSubview:bgView];
    UIImageView *browseImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    browseImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.browseImgView = browseImgView;
    
    [_browseImgView sd_setImageWithURL:[NSURL URLWithString:mineModel.user_info.header_url_original] placeholderImage:[UIImage imageNamed:@"icon"]];
    
    [bgView addSubview:browseImgView];
    
    browseImgView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeView)];
    [browseImgView addGestureRecognizer:tapGesture];
    
    [self shakeToShow:bgView];//放大过程中的动画
}
-(void)closeView{
    
    [background removeFromSuperview];
}

//放大过程中出现的缓慢动画
- (void) shakeToShow:(UIView*)aView{
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.3;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [aView.layer addAnimation:animation forKey:nil];
}


- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewcontroller presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

- (void)dealwithBottomSTH{
    
    if ([self.mineModel.user_info.car_cert_status isEqualToString:@"3"] && [self.mineModel.user_info.driver_cert_status isEqualToString:@"2"]) {
        /* 认证车手 并且 认证过座驾 */
        self.carPlayerLabel.hidden = NO;
        self.carPlayerView.hidden = NO;
        self.carImageView.hidden = NO;
        self.carLabel.hidden = NO;
        
        self.carPlayerLabel.text = @"FAST4WARD认证车手";
        self.carLabel.text = self.mineModel.user_info.car_style;

        topBgImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 297+40+FWCustomeSafeTop);
        self.currentHeight = 350+FWCustomeSafeTop+40;

        [carPlayerView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.mas_equalTo(flagLineView);
            make.top.mas_equalTo(flagLineView.mas_bottom).mas_offset(15);
        }];
        
        [carPlayerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(carPlayerView.mas_right).mas_offset(9);
            make.width.mas_equalTo(125);
            make.centerY.mas_equalTo(carPlayerView);
        }];
        
        
        [playerArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(10);
            make.left.mas_equalTo(carPlayerLabel.mas_right).mas_offset(0);
            make.width.mas_equalTo(5);
            make.centerY.mas_equalTo(carPlayerLabel);
        }];
        
        [carImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.mas_equalTo(flagLineView);
            make.top.mas_equalTo(carPlayerView.mas_bottom).mas_offset(5);
        }];
        
        [carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(carImageView.mas_right).mas_offset(9);
            make.width.mas_greaterThanOrEqualTo(100);
            make.right.mas_equalTo(flagLineView);
            make.centerY.mas_equalTo(carImageView);
        }];
        
        
        [signImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.mas_equalTo(flagLineView);
            make.top.mas_equalTo(carImageView.mas_bottom).mas_offset(5);
        }];
        
        [signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(signImageView.mas_right).mas_offset(9);
            make.width.mas_greaterThanOrEqualTo(100);
            make.right.mas_equalTo(flagLineView);
            make.centerY.mas_equalTo(signImageView);
        }];
    }else if([self.mineModel.user_info.car_cert_status isEqualToString:@"3"] && ![self.mineModel.user_info.driver_cert_status isEqualToString:@"2"]){
        /* 只认证过座驾 */
        self.carPlayerLabel.hidden = YES;
        self.carPlayerView.hidden = YES;
        
        self.carImageView.hidden = NO;
        self.carLabel.hidden = NO;
        
        self.carLabel.text = self.mineModel.user_info.car_style;
        topBgImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 297+20+FWCustomeSafeTop);
        self.currentHeight = 350+FWCustomeSafeTop+20;

        [carImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.mas_equalTo(flagLineView);
            make.top.mas_equalTo(flagLineView.mas_bottom).mas_offset(15);
        }];
        
        [carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(carImageView.mas_right).mas_offset(9);
            make.width.mas_greaterThanOrEqualTo(100);
            make.right.mas_equalTo(flagLineView);
            make.centerY.mas_equalTo(carImageView);
        }];
        
        [signImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.mas_equalTo(flagLineView);
            make.top.mas_equalTo(carImageView.mas_bottom).mas_offset(5);
        }];
        
        [signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(signImageView.mas_right).mas_offset(9);
            make.width.mas_greaterThanOrEqualTo(100);
            make.right.mas_equalTo(flagLineView);
            make.centerY.mas_equalTo(signImageView);
        }];
    }else if(![self.mineModel.user_info.car_cert_status isEqualToString:@"3"] && [self.mineModel.user_info.driver_cert_status isEqualToString:@"2"]){
        /* 只认证过车手 */
        self.carPlayerLabel.hidden = NO;
        self.carPlayerView.hidden = NO;
        
        self.carImageView.hidden = YES;
        self.carLabel.hidden = YES;
        
        self.carPlayerLabel.text = @"FAST4WARD认证车手>";
        
        topBgImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 297+20+FWCustomeSafeTop);
        self.currentHeight = 350+FWCustomeSafeTop+20;
        
        [carPlayerView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.mas_equalTo(flagLineView);
            make.top.mas_equalTo(flagLineView.mas_bottom).mas_offset(15);
        }];
        
        [carPlayerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(carPlayerView.mas_right).mas_offset(9);
            make.width.mas_greaterThanOrEqualTo(100);
            make.right.mas_equalTo(flagLineView);
            make.centerY.mas_equalTo(carPlayerView);
        }];
        
        [signImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.mas_equalTo(flagLineView);
            make.top.mas_equalTo(carPlayerView.mas_bottom).mas_offset(5);
        }];
        
        [signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(signImageView.mas_right).mas_offset(9);
            make.width.mas_greaterThanOrEqualTo(100);
            make.right.mas_equalTo(flagLineView);
            make.centerY.mas_equalTo(signImageView);
        }];


    }else if(![self.mineModel.user_info.car_cert_status isEqualToString:@"3"] && ![self.mineModel.user_info.driver_cert_status isEqualToString:@"2"]){
        /* 既没认证过车手， 也没认证过座驾 */
        self.carPlayerLabel.hidden = YES;
        self.carPlayerView.hidden = YES;
        
        self.carImageView.hidden = YES;
        self.carLabel.hidden = YES;
        
        topBgImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 297+FWCustomeSafeTop);
        self.currentHeight = 350+FWCustomeSafeTop;

        [signImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.mas_equalTo(flagLineView);
            make.top.mas_equalTo(flagLineView.mas_bottom).mas_offset(15);
        }];
        
        [signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.left.mas_equalTo(signImageView.mas_right).mas_offset(9);
            make.width.mas_greaterThanOrEqualTo(100);
            make.right.mas_equalTo(flagLineView);
            make.centerY.mas_equalTo(signImageView);
        }];
    }
}

- (void)carPlayerClick{
    
    FWPlayerArchivesViewController * AVC = [[FWPlayerArchivesViewController alloc] init];
    AVC.driver_id = self.mineModel.user_info.driver_id;
    [self.viewController.navigationController pushViewController:AVC animated:YES];
}

@end
