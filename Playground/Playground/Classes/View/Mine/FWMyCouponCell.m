//
//  FWMyCouponCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyCouponCell.h"

@interface FWMyCouponCell()

@property (nonatomic, strong) FWCouponListModel * listModel;

@end

@implementation FWMyCouponCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupUnReceiveView];
        [self setupReceiveView];
    }
    
    return self;
}

- (void)setupUnReceiveView{
    
    /* 未领取时的承载视图 */
    self.unReceiveView = [UIImageView new];
    self.unReceiveView.image = [UIImage imageNamed:@"coupon_lingqu"];
    self.unReceiveView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.unReceiveView];
    [self.unReceiveView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(00);
        make.left.mas_equalTo(self.contentView).mas_offset(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-10);
        make.bottom.mas_equalTo(self.contentView).mas_offset(0);
        make.height.mas_equalTo(125*(SCREEN_WIDTH-20)/355);
    }];
    
    self.unReceivePriceLabel = [[UILabel alloc] init];
    self.unReceivePriceLabel.font = DHSystemFontOfSize_22;
    self.unReceivePriceLabel.textColor = FWColor(@"FF5454");
    self.unReceivePriceLabel.textAlignment = NSTextAlignmentLeft;
    [self.unReceiveView addSubview:self.unReceivePriceLabel];
    [self.unReceivePriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.unReceiveView);
        make.left.mas_equalTo(self.unReceiveView).mas_offset(30);
        make.width.mas_greaterThanOrEqualTo(10);
        make.top.bottom.mas_equalTo(self.unReceiveView);
    }];
    
    self.unConditionLabel = [[UILabel alloc] init];
    self.unConditionLabel.font = DHSystemFontOfSize_13;
    self.unConditionLabel.textColor = FWTextColor_222222;
    self.unConditionLabel.textAlignment = NSTextAlignmentLeft;
    [self.unReceiveView addSubview:self.unConditionLabel];
    [self.unConditionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.unReceivePriceLabel).mas_offset(2);
        make.left.mas_equalTo(self.unReceivePriceLabel.mas_right).mas_offset(5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(25);
    }];
    
    self.reeceiveButton = [[UIButton alloc] init];
    [self.reeceiveButton addTarget:self action:@selector(reeceiveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.unReceiveView addSubview:self.reeceiveButton];
    [self.reeceiveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.unReceiveView);
        make.right.mas_equalTo(self.unReceiveView).mas_offset(-24);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(self.unReceiveView);
    }];
}

- (void)setupReceiveView{
    
    /* 领取时的承载视图 */
    self.receiveView = [UIImageView new];
    self.receiveView.image = [UIImage imageNamed:@"coupon_yilingqu"];
    self.receiveView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.receiveView];
    [self.receiveView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(0);
        make.left.mas_equalTo(self.contentView).mas_offset(10);
        make.right.mas_equalTo(self.contentView).mas_offset(-10);
        make.bottom.mas_equalTo(self.contentView).mas_offset(0);
        make.height.mas_equalTo(125*(SCREEN_WIDTH-20)/355);
    }];
    self.receiveView.hidden = YES;
    
    self.addressLabel = [[UILabel alloc] init];
    self.addressLabel.font = DHSystemFontOfSize_13;
    self.addressLabel.textColor = FWTextColor_222222;
    self.addressLabel.textAlignment = NSTextAlignmentLeft;
    [self.receiveView addSubview:self.addressLabel];
    [self.addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.receiveView).mas_offset(-23);
        make.left.mas_equalTo(self.receiveView).mas_offset(35);
        make.width.mas_equalTo(self.receiveView);
        make.height.mas_equalTo(25);
    }];
    
    self.receivePriceLabel = [[UILabel alloc] init];
    self.receivePriceLabel.font = DHSystemFontOfSize_25;
    self.receivePriceLabel.textColor = FWColor(@"FF5454");
    self.receivePriceLabel.textAlignment = NSTextAlignmentLeft;
    [self.receiveView addSubview:self.receivePriceLabel];
    [self.receivePriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.addressLabel.mas_top).mas_offset(-5);
        make.left.mas_equalTo(self.receiveView).mas_offset(30);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(30);
    }];
    
    self.conditionLabel = [[UILabel alloc] init];
    self.conditionLabel.font = DHSystemFontOfSize_13;
    self.conditionLabel.textColor = FWTextColor_222222;
    self.conditionLabel.textAlignment = NSTextAlignmentLeft;
    [self.receiveView addSubview:self.conditionLabel];
    [self.conditionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.receivePriceLabel).mas_offset(2);
        make.left.mas_equalTo(self.receivePriceLabel.mas_right).mas_offset(5);
        make.width.mas_greaterThanOrEqualTo(25);
        make.height.mas_equalTo(25);
    }];
    
    
    self.cpButton = [[UIButton alloc] init];
    [self.cpButton addTarget:self action:@selector(cpButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.receiveView addSubview:self.cpButton];
    [self.cpButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(self.receiveView);
        make.right.mas_equalTo(self.receiveView).mas_offset(-10);
        make.centerY.mas_equalTo(self.receiveView);
    }];
}

- (void)cpButtonClick{
    
    [[FWHudManager sharedManager] showSuccessMessage:@"复制成功，请在浏览器打开" toController:self.vc];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [self.addressLabel.text substringFromIndex:5];
}

- (void)configForCellWithModel:(FWCouponListModel *)model{
    
    self.listModel = (FWCouponListModel *)model;
    
    if ([self.listModel.get_status isEqualToString:@"1"]) {
        /* 已领取 */
        self.receiveView.hidden = NO;
        self.unReceiveView.hidden = YES;
        
        self.receivePriceLabel.text = [NSString stringWithFormat:@"￥%@",self.listModel.price];
        self.conditionLabel.text = self.listModel.name;
        self.addressLabel.text = self.listModel.code;
    }else if ([self.listModel.get_status isEqualToString:@"3"]) {
        /* 未领取 */
        self.receiveView.hidden = YES;
        self.unReceiveView.hidden = NO;
        
        self.unReceivePriceLabel.text = [NSString stringWithFormat:@"￥%@",self.listModel.price];
        self.unConditionLabel.text = self.listModel.name;
    }
}

#pragma mark - > 领取门票
- (void)reeceiveButtonClick{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"gift_type":self.listModel.gift_type,
                              };
    
    [request startWithParameters:params WithAction:Submit_get_coupon WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWTicketsInfoModel * infoModel = [FWTicketsInfoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            self.receiveView.hidden = NO;
            self.unReceiveView.hidden = YES;
            
            self.receivePriceLabel.text = [NSString stringWithFormat:@"￥%@",self.listModel.price];;
            self.conditionLabel.text = self.listModel.name;
            
            self.addressLabel.text = infoModel.code;
            [[FWHudManager sharedManager] showErrorMessage:@"领取成功" toController:self.vc];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"领取失败，请稍后重试" toController:self.vc];
        }
    }];
    
}

@end


////
////  FWMyCouponCell.m
////  Playground
////
////  Created by 孤星之殇 on 2019/2/18.
////  Copyright © 2019 孤星之殇. All rights reserved.
////
//
//#import "FWMyCouponCell.h"
//
//@interface FWMyCouponCell()
//
//@property (nonatomic, strong) FWCouponListModel * listModel;
//
//@end
//
//@implementation FWMyCouponCell
//
//- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
//
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//
//    if (self) {
//        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
//        self.selectionStyle = UITableViewCellSelectionStyleNone;
//
//        [self setupUnReceiveView];
//        [self setupReceiveView];
//    }
//
//    return self;
//}
//
//- (void)setupUnReceiveView{
//
//    /* 未领取时的承载视图 */
//    self.unReceiveView = [UIView new];
//    self.unReceiveView.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
//    self.unReceiveView.layer.borderWidth = 1;
//    self.unReceiveView.layer.cornerRadius = 4;
//    self.unReceiveView.layer.masksToBounds = YES;
//    [self.contentView addSubview:self.unReceiveView];
//    [self.unReceiveView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.contentView).mas_offset(10);
//        make.left.mas_equalTo(self.contentView).mas_offset(20);
//        make.right.mas_equalTo(self.contentView).mas_offset(-20);
//        make.bottom.mas_equalTo(self.contentView).mas_offset(-10);
//        make.width.mas_equalTo(SCREEN_WIDTH-40);
//        make.height.mas_equalTo(83);
//    }];
//
//    self.unReceivePriceLabel = [[UILabel alloc] init];
//    self.unReceivePriceLabel.font = DHSystemFontOfSize_22;
//    self.unReceivePriceLabel.textColor = FWViewBackgroundColor_FFFFFF;
//    self.unReceivePriceLabel.textAlignment = NSTextAlignmentLeft;
//    [self.unReceiveView addSubview:self.unReceivePriceLabel];
//    [self.unReceivePriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.unReceiveView);
//        make.left.mas_equalTo(self.unReceiveView).mas_offset(20);
//        make.width.mas_greaterThanOrEqualTo(10);
//        make.top.bottom.mas_equalTo(self.unReceiveView);
//    }];
//
//    self.unConditionLabel = [[UILabel alloc] init];
//    self.unConditionLabel.font = DHSystemFontOfSize_13;
//    self.unConditionLabel.textColor = FWViewBackgroundColor_FFFFFF;
//    self.unConditionLabel.textAlignment = NSTextAlignmentLeft;
//    [self.unReceiveView addSubview:self.unConditionLabel];
//    [self.unConditionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.unReceivePriceLabel).mas_offset(2);
//        make.left.mas_equalTo(self.unReceivePriceLabel.mas_right).mas_offset(5);
//        make.width.mas_greaterThanOrEqualTo(10);
//        make.height.mas_equalTo(25);
//    }];
//
//    self.reeceiveButton = [[UIButton alloc] init];
//    self.reeceiveButton.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
//    self.reeceiveButton.layer.borderWidth = 1;
//    self.reeceiveButton.layer.cornerRadius = 4;
//    self.reeceiveButton.layer.masksToBounds = YES;
//    self.reeceiveButton.titleLabel.font = DHSystemFontOfSize_14;
//    [self.reeceiveButton setTitle:@"领取" forState:UIControlStateNormal];
//    [self.reeceiveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
//    [self.reeceiveButton addTarget:self action:@selector(reeceiveButtonClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.unReceiveView addSubview:self.reeceiveButton];
//    [self.reeceiveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.unReceiveView);
//        make.right.mas_equalTo(self.unReceiveView).mas_offset(-24);
//        make.width.mas_equalTo(60);
//        make.height.mas_equalTo(30);
//    }];
//}
//
//- (void)setupReceiveView{
//
//    /* 领取时的承载视图 */
//    self.receiveView = [UIView new];
//    self.receiveView.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
//    self.receiveView.layer.borderWidth = 1;
//    self.receiveView.layer.cornerRadius = 4;
//    self.receiveView.layer.masksToBounds = YES;
//    [self.contentView addSubview:self.receiveView];
//    [self.receiveView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.contentView).mas_offset(10);
//        make.left.mas_equalTo(self.contentView).mas_offset(20);
//        make.right.mas_equalTo(self.contentView).mas_offset(-20);
//        make.bottom.mas_equalTo(self.contentView).mas_offset(-10);
//        make.width.mas_equalTo(SCREEN_WIDTH-40);
//        make.height.mas_equalTo(83);
//    }];
//    self.receiveView.hidden = YES;
//
//    self.receivePriceLabel = [[UILabel alloc] init];
//    self.receivePriceLabel.font = DHSystemFontOfSize_25;
//    self.receivePriceLabel.textColor = FWViewBackgroundColor_FFFFFF;
//    self.receivePriceLabel.textAlignment = NSTextAlignmentLeft;
//    [self.receiveView addSubview:self.receivePriceLabel];
//    [self.receivePriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.receiveView).mas_offset(15);
//        make.left.mas_equalTo(self.receiveView).mas_offset(20);
//        make.width.mas_greaterThanOrEqualTo(10);
//        make.height.mas_equalTo(30);
//    }];
//
//    self.conditionLabel = [[UILabel alloc] init];
//    self.conditionLabel.font = DHSystemFontOfSize_13;
//    self.conditionLabel.textColor = FWTextColor_9C9C9C;
//    self.conditionLabel.textAlignment = NSTextAlignmentLeft;
//    [self.receiveView addSubview:self.conditionLabel];
//    [self.conditionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.receivePriceLabel).mas_offset(2);
//        make.left.mas_equalTo(self.receivePriceLabel.mas_right).mas_offset(5);
//        make.width.mas_greaterThanOrEqualTo(25);
//        make.height.mas_equalTo(25);
//    }];
//
//    self.addressLabel = [[UILabel alloc] init];
//    self.addressLabel.font = DHSystemFontOfSize_13;
//    self.addressLabel.textColor = FWTextColor_9C9C9C;
//    self.addressLabel.textAlignment = NSTextAlignmentLeft;
//    [self.receiveView addSubview:self.addressLabel];
//    [self.addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(self.receiveView.mas_bottom).mas_offset(-15);
//        make.left.mas_equalTo(self.receivePriceLabel);
//        make.width.mas_equalTo(self.receiveView);
//        make.height.mas_equalTo(25);
//    }];
//
//
//    self.cpButton = [[UIButton alloc] init];
//    self.cpButton.titleLabel.font = DHSystemFontOfSize_13;
//    [self.cpButton setTitle:@"复制链接" forState:UIControlStateNormal];
//    [self.cpButton setTitleColor:FWTextColor_2B98FA forState:UIControlStateNormal];
//    [self.cpButton addTarget:self action:@selector(cpButtonClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.receiveView addSubview:self.cpButton];
//    [self.cpButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(70);
//        make.height.mas_equalTo(30);
//        make.right.mas_equalTo(self.receiveView).mas_offset(-10);
//        make.centerY.mas_equalTo(self.receiveView);
//    }];
//}
//
//- (void)cpButtonClick{
//
//    [[FWHudManager sharedManager] showSuccessMessage:@"复制成功，请在浏览器打开" toController:self.vc];
//
//    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
//    pasteboard.string = [self.addressLabel.text substringFromIndex:5];
//}
//
//- (void)configForCellWithModel:(FWCouponListModel *)model{
//
//    self.listModel = (FWCouponListModel *)model;
//
//    if ([self.listModel.get_status isEqualToString:@"1"]) {
//        /* 已领取 */
//        self.receiveView.hidden = NO;
//        self.unReceiveView.hidden = YES;
//
//        self.receivePriceLabel.text = [NSString stringWithFormat:@"￥%@",self.listModel.price];
//        self.conditionLabel.text = self.listModel.name;
//        self.addressLabel.text = [NSString stringWithFormat:@"领取地址:%@",self.listModel.code];
//    }else if ([self.listModel.get_status isEqualToString:@"3"]) {
//        /* 未领取 */
//        self.receiveView.hidden = YES;
//        self.unReceiveView.hidden = NO;
//
//        self.unReceivePriceLabel.text = [NSString stringWithFormat:@"￥%@",self.listModel.price];
//        self.unConditionLabel.text = self.listModel.name;
//    }
//}
//
//#pragma mark - > 领取门票
//- (void)reeceiveButtonClick{
//
//    FWMemberRequest * request = [[FWMemberRequest alloc] init];
//
//    NSDictionary * params = @{
//                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
//                              @"gift_type":self.listModel.gift_type,
//                              };
//
//    [request startWithParameters:params WithAction:Submit_get_coupon WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
//
//        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
//
//        if ([code isEqualToString:NetRespondCodeSuccess]) {
//
//            FWTicketsInfoModel * infoModel = [FWTicketsInfoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
//
//            self.receiveView.hidden = NO;
//            self.unReceiveView.hidden = YES;
//
//            self.receivePriceLabel.text = [NSString stringWithFormat:@"￥%@",self.listModel.price];;
//            self.conditionLabel.text = self.listModel.name;
//
//            self.addressLabel.text = [NSString stringWithFormat:@"领取地址:%@",infoModel.code];
//            [[FWHudManager sharedManager] showErrorMessage:@"领取成功" toController:self.vc];
//        }else{
//            [[FWHudManager sharedManager] showErrorMessage:@"领取失败，请稍后重试" toController:self.vc];
//        }
//    }];
//
//}
//
//@end
