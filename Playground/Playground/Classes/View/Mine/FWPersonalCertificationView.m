//
//  FWPersonalCertificationView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPersonalCertificationView.h"

@interface FWPersonalCertificationView()
@property (nonatomic, strong) NSString * type;
@end

@implementation FWPersonalCertificationView

- (id)init{
    self = [super init];
    if (self) {
        self.type = @"1";
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.numberOfLines = 3;
    self.tipLabel.text = @"*若您参加过FAST4WARD直线竞速赛，可申请认证车手身份，享受车手勋章，同事可在线查看您的全部参赛成绩。";
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.tipLabel.textColor = FWTextColor_B6BCC4;
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(20);
        make.right.mas_equalTo(self).mas_offset(-20);
        make.top.mas_equalTo(self).mas_offset(10);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
    }];
    
    self.idCardButton = [[UIButton alloc] init];
    self.idCardButton.selected = YES;
    [self.idCardButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.idCardButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    self.idCardButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.idCardButton setTitle:@"身份证" forState:UIControlStateNormal];
    [self.idCardButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.idCardButton addTarget:self action:@selector(idCardButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.idCardButton];
    [self.idCardButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.tipLabel);
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(30);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(80);
    }];
    
    self.postButton = [[UIButton alloc] init];
    [self.postButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.postButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    self.postButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.postButton setTitle:@"护照" forState:UIControlStateNormal];
    [self.postButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.postButton addTarget:self action:@selector(postButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.postButton];
    [self.postButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.idCardButton.mas_right).mas_offset(30);
        make.centerY.mas_equalTo(self.idCardButton);
        make.height.mas_equalTo(self.idCardButton);
        make.width.mas_equalTo(self.idCardButton);
    }];
    
    self.cardLabel = [[UILabel alloc] init];
    self.cardLabel.text = @"证件号";
    self.cardLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.cardLabel.textColor = FWTextColor_12101D;
    self.cardLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.cardLabel];
    [self.cardLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.tipLabel).mas_offset(0);
        make.top.mas_equalTo(self.idCardButton.mas_bottom).mas_offset(30);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
    }];
    
    self.cardTextField = [[UITextField alloc] init];
    self.cardTextField.delegate = self;
    self.cardTextField.placeholder = @"请输入参赛证件号码";
    self.cardTextField.textColor = FWTextColor_12101D;
    [self addSubview:self.cardTextField];
    [self.cardTextField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cardLabel);
        make.right.mas_equalTo(self.cardLabel);
        make.top.mas_equalTo(self.cardLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(44);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
    }];
    
    self.lineView =  [[UIView alloc] init];
    self.lineView.backgroundColor = FWTextColor_141414;
    [self addSubview:self.lineView];
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.cardTextField);
        make.height.mas_equalTo(1);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
        make.top.mas_equalTo(self.cardTextField.mas_bottom);
    }];
    
    self.sendButton = [[UIButton alloc] init];
    self.sendButton.backgroundColor = FWTextColor_222222;
    self.sendButton.layer.cornerRadius = 2;
    self.sendButton.layer.masksToBounds = YES;
    self.sendButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.sendButton setTitle:@"申请认证" forState:UIControlStateNormal];
    [self.sendButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.sendButton addTarget:self action:@selector(sendButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.sendButton];
    [self.sendButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(29);
        make.right.mas_equalTo(self).mas_offset(-29);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(SCREEN_WIDTH-58);
        make.bottom.mas_equalTo(self).mas_offset(-FWSafeBottom-86);
    }];
}

#pragma mark - > 选择身份证
- (void)idCardButtonClick{
    self.type = @"1";
    self.idCardButton.selected = YES;
    self.postButton.selected = NO;
}

#pragma mark - > 选择护照
- (void)postButtonClick{
    self.type = @"2";
    self.idCardButton.selected = NO;
    self.postButton.selected = YES;
}

#pragma mark - > 申请
- (void)sendButtonClick{
    
    if (self.cardTextField.text.length != 18 && [self.type isEqualToString:@"1"]) {
        [[FWHudManager sharedManager] showErrorMessage:@"证件号格式错误，请仔细核对" toController:self.vc];
        return;
    }
    
    if (self.cardTextField.text.length <= 0 && [self.type isEqualToString:@"2"]) {
        [[FWHudManager sharedManager] showErrorMessage:@"证件号格式错误，请仔细核对" toController:self.vc];
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"type":self.type,
                              @"idcard":self.cardTextField.text,
                              };
    
    [request startWithParameters:params WithAction:Submit_identify_driver WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"认证成功" message:@"参赛成绩已同步，您可在个人主页查看全部成绩。" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.vc.navigationController popToRootViewControllerAnimated:YES];
            }];
            [alertController addAction:okAction];
            [self.vc presentViewController:alertController animated:YES completion:nil];
        }else{
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"认证失败" message:[back objectForKey:@"errmsg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            [alertController addAction:okAction];
            [self.vc presentViewController:alertController animated:YES completion:nil];
        }
    }];
}
@end
