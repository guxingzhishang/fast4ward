//
//  FWCarPlayerCenterView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/6.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarPlayerCenterView.h"

@implementation FWCarPlayerCenterView

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.userInfo= [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];

        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.currentHeight = 150 *SCREEN_WIDTH/375-64+15;
    
    
    self.topBgImageView = [[UIImageView alloc] init];
    self.topBgImageView.clipsToBounds = YES;
    self.topBgImageView.userInteractionEnabled = YES;
    self.topBgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topBgImageView.image = [UIImage imageNamed:@"match_detail_bg"];
    [self addSubview:self.topBgImageView];
    [self.topBgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).mas_offset(0);
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, self.currentHeight-15));
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 51/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.userInfo.userModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    [self.topBgImageView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topBgImageView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(51, 51));
        make.bottom.mas_equalTo(self.topBgImageView).mas_offset(-20);
    }];
    UITapGestureRecognizer * photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoTapClick)];
    [self.photoImageView addGestureRecognizer:photoTap];
    
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.text = self.userInfo.userModel.nickname;
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.topBgImageView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.centerY.mas_equalTo(self.photoImageView);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        make.right.mas_equalTo(self.topBgImageView).mas_offset(-5);
        make.width.mas_greaterThanOrEqualTo(200);
    }];
}

- (void)photoTapClick{
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.userInfo.userModel.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

@end
