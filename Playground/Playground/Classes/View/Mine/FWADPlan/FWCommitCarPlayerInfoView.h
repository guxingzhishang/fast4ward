//
//  FWCommitCarPlayerInfoView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/7.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWPlayerRegistrationViewDelegate <NSObject>

- (void)commitButtonClick;

@end

@interface FWCommitCarPlayerInfoView : UIScrollView

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIView * topView;

@property (nonatomic, strong) UILabel * realNameLabel;
@property (nonatomic, strong) UITextField * realNameTF;
@property (nonatomic, strong) UIView * realNameLineView;

@property (nonatomic, strong) UILabel * selectLabel;
@property (nonatomic, strong) UIButton * idCardButton;
@property (nonatomic, strong) UIButton * postButton;

@property (nonatomic, strong) UILabel * cardNumLabel;
@property (nonatomic, strong) UITextField * cardNumTF;
@property (nonatomic, strong) UIView * cardNumLineView;

@property (nonatomic, strong) UILabel * cardPhotoLabel;
@property (nonatomic, strong) UIImageView * cardPhotoImageView;
@property (nonatomic, strong) UILabel * reuploadLabel;
@property (nonatomic, strong) UILabel * tipLabel;

@property (nonatomic, strong) UIView * middleView;
@property (nonatomic, strong) UIButton * selectButton;
@property (nonatomic, strong) UILabel  * protocolLabel;

@property (nonatomic, strong) UIButton * commitButton;

@property (nonatomic, weak) id<FWPlayerRegistrationViewDelegate>registrationDelegate;

@property (nonatomic, strong) NSMutableArray * firstArray;

/* 1:身份证   2:护照 */
@property (nonatomic, strong) NSString * type;

@property (nonatomic, strong) NSString * h5_url;

@property (nonatomic, weak) UIViewController * vc;
@end

NS_ASSUME_NONNULL_END
