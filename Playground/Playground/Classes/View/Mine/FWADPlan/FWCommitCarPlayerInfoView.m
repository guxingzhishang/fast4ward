//
//  FWCommitCarPlayerInfoView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/7.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCommitCarPlayerInfoView.h"
#import "FWCarPlayerADPlanViewController.h"
#import "UILabel+YBAttributeTextTapAction.h"

@interface FWCommitCarPlayerInfoView()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UIImagePickerController * picker;
@property (nonatomic, strong) NSDictionary * dataDict;
@property (nonatomic, strong) NSString * card_cover;


@end

@implementation FWCommitCarPlayerInfoView


#pragma mark - > 请求阿里临时验证
- (void)requestStsData{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    [request requestStsTokenWithType:@"1"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestUploadName];
    });
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"driver_plan_idcard",
                              @"number":@"1",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.dataDict = [back objectForKey:@"data"];
        }
    }];
}

- (id)init{
    self = [super init];
    if (self) {
        self.type = @"1";
        self.firstArray = @[].mutableCopy;
        
        self.backgroundColor = FWTextColor_F6F8FA;
        [self setupSubviews];
        [self requestStsData];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.topView = [[UIView alloc] init];
    self.topView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.topView];
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(100);
    }];
    
    self.realNameLabel = [UILabel new];
    self.realNameLabel.text = @"真实姓名";
    self.realNameLabel.frame = CGRectMake(15, 18, 100, 16);
    self.realNameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.realNameLabel.textColor = FWTextColor_12101D;
    self.realNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.realNameLabel];
    
    self.realNameTF = [[UITextField alloc] init];
    self.realNameTF.delegate = self;
    self.realNameTF.placeholder = @"请填写真实姓名";
    self.realNameTF.textColor = FWTextColor_12101D;
    self.realNameTF.frame = CGRectMake(CGRectGetMinX(self.realNameLabel.frame), CGRectGetMaxY(self.realNameLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.realNameLabel.frame), 38);
    [self.topView addSubview:self.realNameTF];
    
    self.realNameLineView =  [[UIView alloc] init];
    self.realNameLineView.frame = CGRectMake(CGRectGetMinX(self.realNameLabel.frame), CGRectGetMaxY(self.realNameTF.frame), CGRectGetWidth(self.realNameTF.frame), 0.5);
    self.realNameLineView.backgroundColor = FWTextColor_141414;
    [self.topView addSubview:self.realNameLineView];
    
    self.selectLabel = [UILabel new];
    self.selectLabel.text = @"选择证件";
    self.selectLabel.frame = CGRectMake(CGRectGetMinX(self.realNameLabel.frame), CGRectGetMaxY(self.realNameLineView.frame)+20, 100, 16);
    self.selectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.selectLabel.textColor = FWTextColor_12101D;
    self.selectLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.selectLabel];
    
    self.idCardButton = [[UIButton alloc] init];
    self.idCardButton.selected = YES;
    [self.idCardButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.idCardButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    self.idCardButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.idCardButton setTitle:@" 身份证" forState:UIControlStateNormal];
    [self.idCardButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.idCardButton addTarget:self action:@selector(idCardButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.idCardButton];
    self.idCardButton.frame = CGRectMake(CGRectGetMinX(self.realNameLabel.frame), CGRectGetMaxY(self.selectLabel.frame)+20, 80, 30);
    [self.idCardButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.selectLabel);
        make.top.mas_equalTo(self.selectLabel.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(80);
    }];
    
    self.postButton = [[UIButton alloc] init];
    [self.postButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.postButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    self.postButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.postButton setTitle:@" 护照/港澳证" forState:UIControlStateNormal];
    [self.postButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.postButton addTarget:self action:@selector(postButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.postButton];
    [self.postButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.idCardButton.mas_right).mas_offset(30);
        make.centerY.mas_equalTo(self.idCardButton);
        make.height.mas_equalTo(self.idCardButton);
        make.width.mas_equalTo(110);
    }];
    
    self.cardNumLabel = [UILabel new];
    self.cardNumLabel.text = @"证件号码";
    self.cardNumLabel.frame = CGRectMake(CGRectGetMinX(self.selectLabel.frame), CGRectGetMaxY(self.idCardButton.frame)+5, 100, 20);
    self.cardNumLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.cardNumLabel.textColor = FWTextColor_12101D;
    self.cardNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.cardNumLabel];
    
    self.cardNumTF = [[UITextField alloc] init];
    self.cardNumTF.delegate = self;
    self.cardNumTF.placeholder = @"请填写证件号码";
    self.cardNumTF.textColor = FWTextColor_12101D;
    self.cardNumTF.frame = CGRectMake(CGRectGetMinX(self.cardNumLabel.frame), CGRectGetMaxY(self.cardNumLabel.frame), SCREEN_WIDTH-2*CGRectGetMinX(self.cardNumLabel.frame), 38);
    [self.topView addSubview:self.cardNumTF];
    
    self.cardNumLineView =  [[UIView alloc] init];
    self.cardNumLineView.frame = CGRectMake(CGRectGetMinX(self.cardNumTF.frame), CGRectGetMaxY(self.cardNumTF.frame), CGRectGetWidth(self.cardNumTF.frame), 0.5);
    self.cardNumLineView.backgroundColor = FWTextColor_141414;
    [self.topView addSubview:self.cardNumLineView];
    
    self.cardPhotoLabel = [UILabel new];
    self.cardPhotoLabel.text = @"证件照片";
    self.cardPhotoLabel.frame = CGRectMake(CGRectGetMinX(self.cardNumLabel.frame), CGRectGetMaxY(self.cardNumLineView.frame)+15, 100, 20);
    self.cardPhotoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.cardPhotoLabel.textColor = FWTextColor_12101D;
    self.cardPhotoLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.cardPhotoLabel];
    
    self.cardPhotoImageView = [[UIImageView alloc] init];
    self.cardPhotoImageView.userInteractionEnabled = YES;
    self.cardPhotoImageView.image = [UIImage imageNamed:@"upload_card"];
    self.cardPhotoImageView.layer.cornerRadius= 5;
    self.cardPhotoImageView.layer.masksToBounds = YES;
    self.cardPhotoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.cardPhotoImageView.clipsToBounds = YES;
    [self.topView addSubview:self.cardPhotoImageView];
    [self.cardPhotoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cardPhotoLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 188*(SCREEN_WIDTH-28)/347));
        make.top.mas_equalTo(self.cardPhotoLabel.mas_bottom).mas_offset(15);
    }];
    [self.cardPhotoImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType)]];

    
    self.reuploadLabel = [UILabel new];
    self.reuploadLabel.text = @"重新上传";
    self.reuploadLabel.layer.cornerRadius= 5;
    self.reuploadLabel.layer.masksToBounds = YES;
    self.reuploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.reuploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.reuploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.reuploadLabel.textAlignment = NSTextAlignmentCenter;
    [self.cardPhotoImageView addSubview:self.reuploadLabel];
    self.reuploadLabel.hidden = YES;
    [self.reuploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.cardPhotoImageView);
        make.height.mas_equalTo(35);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    
    self.tipLabel = [UILabel new];
    self.tipLabel.text = @"*请上传带有头像页的证件照片";
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.tipLabel.textColor = FWTextColor_9EA3AB;
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cardPhotoLabel);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.top.mas_equalTo(self.cardPhotoImageView.mas_bottom).mas_offset(10);
        make.bottom.mas_equalTo(self.topView).mas_offset(-15);
    }];
    
    self.middleView = [[UIView alloc] init];
    self.middleView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.middleView];
    [self.middleView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.topView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(52);
    }];
    
    self.selectButton = [[UIButton alloc] init];
    self.selectButton.selected = NO;
    [self.selectButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
    [self.selectButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateSelected];
    [self.selectButton addTarget:self action:@selector(selectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.middleView addSubview:self.selectButton];
    [self.selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self.middleView);
        make.height.mas_equalTo(52);
        make.width.mas_equalTo(50);
    }];
    
    NSString *protocolText = @"我已认真阅读并同意广告车招募协议。";
    NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc]initWithString:protocolText];
    [attributedString2 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, protocolText.length)];
    [attributedString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"9EA3AB" alpha:1] range:NSMakeRange(0, 9)];
    [attributedString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"ff6f00" alpha:1] range:NSMakeRange(9, 8)];
    
    self.protocolLabel = [[UILabel alloc] init];
    self.protocolLabel.backgroundColor = FWClearColor;
    self.protocolLabel.numberOfLines = 1;
    self.protocolLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];;
    self.protocolLabel.attributedText = attributedString2;
    //设置是否有点击效果，默认是YES
    self.protocolLabel.enabledTapEffect = NO;
    [self.middleView addSubview:self.protocolLabel];
    [self.protocolLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.selectButton);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(250);
        make.left.mas_equalTo(self.selectButton.mas_right).mas_offset(-10);
    }];
    
    [self.protocolLabel yb_addAttributeTapActionWithStrings:@[@"广告车招募协议。"] tapClicked:^(NSString *string, NSRange range, NSInteger index) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = self.h5_url;
        [self.vc.navigationController pushViewController:WVC animated:YES];
    }];
    
    self.commitButton = [[UIButton alloc] init];
    self.commitButton.layer.cornerRadius = 2;
    self.commitButton.layer.masksToBounds = YES;
    self.commitButton.backgroundColor = FWTextColor_222222;
    self.commitButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    [self.commitButton setTitle:@"提交申请" forState:UIControlStateNormal];
    [self.commitButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.commitButton addTarget:self action:@selector(commitButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.commitButton];
    [self.commitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.middleView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self).mas_offset(30);
        make.right.mas_equalTo(self).mas_offset(-30);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(SCREEN_WIDTH-60);
        make.bottom.mas_greaterThanOrEqualTo(self).mas_offset(-125);
    }];
}

#pragma mark - > 上传图片
- (void)uploadPhotoClickWithType{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"上传证件照" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:1];
    }];
    UIAlertAction *selectAction = [UIAlertAction actionWithTitle:@"选择相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:2];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:takePhotoAction];
    [alertController addAction:selectAction];
    [alertController addAction:cancelAction];
    
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 选择相册
- (void)selectLibraryWithType:(NSInteger)type{
    
    if (@available(iOS 11, *)) {
        UIScrollView.appearance.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
    }
    
    NSUInteger sourceType = 0;
    
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeCamera;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
    }else{
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
    }
    
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.delegate                 = self;
    self.picker.sourceType               = sourceType;
    self.picker.allowsEditing            = NO;
    self.picker.navigationController.navigationBar.hidden = NO;
    self.picker.fd_prefersNavigationBarHidden = NO;
    self.picker.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.vc.navigationController presentViewController:self.picker animated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *avatar = info[UIImagePickerControllerOriginalImage];
    //处理完毕，回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    self.cardPhotoImageView.image = avatar;
    
    NSDate * startDare = [NSDate date];

    [ALiOssUploadTool asyncUploadImage:avatar WithBucketName:[self.dataDict objectForKey:@"bucket"] WithObject_key:[self.dataDict objectForKey:@"object_key"] WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
        if (UploadImageSuccess == state) {
            NSLog(@"成功了");
            double durationDate = [[NSDate date] timeIntervalSinceDate:startDare];
            NSLog(@"耗时：%f 秒",durationDate);
            
            
            //跳回主队列执行
            dispatch_async(dispatch_get_main_queue(), ^{
                //在主队列中进行ui操作
                self.card_cover = [self.dataDict objectForKey:@"object_key"];
                self.reuploadLabel.hidden = NO;
            });
        }
    }];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([UIDevice currentDevice].systemVersion.floatValue < 11) {
        return;
    }
    if ([viewController isKindOfClass:NSClassFromString(@"PUPhotoPickerHostViewController")]) {
        [viewController.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.frame.size.width < 42) {
                [viewController.view sendSubviewToBack:obj];
                *stop = YES;
            }
        }];
    }
}


#pragma mark - > 选择身份证
- (void)idCardButtonClick{
    self.type = @"1";
    self.idCardButton.selected = YES;
    self.postButton.selected = NO;
}

#pragma mark - > 选择护照
- (void)postButtonClick{
    self.type = @"2";
    self.idCardButton.selected = NO;
    self.postButton.selected = YES;
}

#pragma mark - > 同意协议
- (void)selectButtonClick{
    self.selectButton.selected = !self.selectButton.selected;
}

#pragma mark - > 提交信息
- (void)commitButtonOnClick{
    
    self.commitButton.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.commitButton.enabled = YES;
    });
    
    /* 图片 */
    if (self.card_cover.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请上传证件照片" toController:self.vc];
        return;
    }
    
    /* 真实姓名 */
    NSString * realName = [self checkString:self.realNameTF.text];
    if (realName.length == 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写真实姓名" toController:self.vc];
        return;
    }
    
    /* 身份证 */
    NSString * idcardString = [self checkString:self.cardNumTF.text];
    if (idcardString.length == 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写证件号码" toController:self.vc];
        return;
    }else if ([self.type isEqualToString:@"1"] && idcardString.length != 18){
        [[FWHudManager sharedManager] showErrorMessage:@"证件号格式错误" toController:self.vc];
        return;
    }
    
    
    /* 阅读协议 */
    if (!self.selectButton.isSelected) {
        [[FWHudManager sharedManager] showErrorMessage:@"您尚未同意车手参赛协议" toController:self.vc];
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"real_name":realName,
                              @"idcard":idcardString,
                              @"idcard_type":self.type,
                              @"idcard_img":self.card_cover,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Submit_join_driver_plan WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"提交成功" toController:self.vc];
           
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                FWCarPlayerADPlanViewController * CPAPVC = [[FWCarPlayerADPlanViewController alloc] init];
                [self.vc.navigationController pushViewController:CPAPVC animated:YES];
            });
           
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

#pragma mark - > 去掉前后空格
- (NSString *)checkString:(NSString *)string{
    
    NSCharacterSet  *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    string = [string stringByTrimmingCharactersInSet:set];
    return string;
}


@end
