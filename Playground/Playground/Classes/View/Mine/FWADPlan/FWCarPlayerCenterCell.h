//
//  FWCarPlayerCenterCell.h
//  
//
//  Created by 孤星之殇 on 2019/8/6.
//

#import "FWListCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCarPlayerCenterCell : FWListCell

-(void)cellConfigureFor:(id)model;

@end

NS_ASSUME_NONNULL_END
