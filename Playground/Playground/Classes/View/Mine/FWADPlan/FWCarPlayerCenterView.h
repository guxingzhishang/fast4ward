//
//  FWCarPlayerCenterView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/6.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWCarPlayerCenterView : UIView

@property (nonatomic, strong) UIImageView * topBgImageView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, assign) CGFloat currentHeight;
@property (nonatomic, strong) FWUserDefaultsVariableModel * userInfo;
@property (nonatomic, weak) UIViewController * vc;

@end

NS_ASSUME_NONNULL_END
