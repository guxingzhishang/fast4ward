//
//  FWBussinessCertificationView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessCertificationView.h"
#import "FWBussinessRankViewController.h"

@interface FWBussinessCertificationView()

@property (strong, nonatomic) FWBussinessCertificationModel * cerModel;
@property (strong, nonatomic) NSMutableArray * idArray;

@end

@implementation FWBussinessCertificationView
@synthesize mainView;
@synthesize backButton;
@synthesize tipNameLabel;
@synthesize certificationTF;
@synthesize tipLabel;
@synthesize containerView;
@synthesize certificationButton;
@synthesize cerModel;
@synthesize idArray;

- (id)init{
    self = [super init];
    if (self) {
        
        idArray = @[].mutableCopy;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    CGFloat bottomMargin = 100+FWSafeBottom;
    
    mainView = [[UIScrollView alloc] init];
    mainView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    mainView.showsVerticalScrollIndicator = NO;
    mainView.delegate = self;
    [self addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.height.mas_equalTo(SCREEN_HEIGHT -bottomMargin);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    

    tipNameLabel = [[UILabel alloc] init];
    tipNameLabel.text = @"认证名称（提交后不可修改）";
    tipNameLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    tipNameLabel.textColor = FWTextColor_000000;
    tipNameLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:tipNameLabel];
    [tipNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(mainView).mas_offset(25);
        make.size.mas_equalTo(CGSizeMake(250, 20));
        make.left.mas_equalTo(mainView).mas_offset(31);
    }];
    
    certificationTF = [[UITextField alloc] init];
    certificationTF.delegate = self;
    certificationTF.placeholder = @"请输入认证名称";
    certificationTF.textColor = FWTextColor_000000;
    certificationTF.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:18];
    certificationTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [mainView addSubview:certificationTF];
    [certificationTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(tipNameLabel).mas_offset(19);
        make.right.mas_equalTo(mainView).mas_offset(-40);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-80, 22));
        make.top.mas_equalTo(tipNameLabel.mas_bottom).mas_offset(25);
    }];
    
    // 获取 TextField 并设置
    UIButton *ClearButton = [certificationTF valueForKey:@"clearButton"];
    [ClearButton setImage:[UIImage imageNamed:@"publish_delete_tags"] forState:UIControlStateNormal];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [certificationTF becomeFirstResponder];
    });
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWTextColor_363445;
    [mainView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(certificationTF);
        make.right.mas_equalTo(certificationTF);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(certificationTF.mas_bottom).mas_offset(10);
    }];
    
    tipLabel = [[UILabel alloc] init];
    tipLabel.text = @"主营业务";
    tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    tipLabel.textColor = FWTextColor_000000;
    tipLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:tipLabel];
    [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(30);
        make.size.mas_equalTo(CGSizeMake(150, 20));
        make.left.mas_equalTo(tipNameLabel);
    }];
    
    containerView = [[UIView alloc] init];
    [mainView addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView);
        make.right.mas_equalTo(lineView);
        make.top.mas_equalTo(tipLabel.mas_bottom).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_greaterThanOrEqualTo(20);
        make.bottom.mas_equalTo(mainView).mas_offset(-20);
    }];
    
    certificationButton = [[UIButton alloc] init];
    certificationButton.backgroundColor = FWTextColor_222222;
    [certificationButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    certificationButton.layer.cornerRadius = 2;
    certificationButton.layer.masksToBounds = YES;
    [certificationButton setTitle:@"认证" forState:UIControlStateNormal];
    certificationButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:16];
    [certificationButton addTarget:self action:@selector(certificationButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:certificationButton];
    [certificationButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(44);
        make.centerX.mas_equalTo(self);
        make.size.mas_offset(CGSizeMake(295, 48));
        make.top.mas_equalTo(self).mas_offset(-bottomMargin+44);
    }];
}

#pragma mark - > 配置
- (void)configViewForModel:(id)model{
    
    cerModel = (FWBussinessCertificationModel *)model;
    NSInteger count = cerModel.list.count;
    if (count > 0 ) {
        for (UIView * view in containerView.subviews) {
            [view removeFromSuperview];
        }
        
        for (int i = 0; i < count; i++) {
            FWBussinessCertificationListModel * listModel = cerModel.list[i];
            listModel.isSelect = NO;
            
            int row = i/2;
            
            UIButton * selectButton = [[UIButton alloc] init];
            selectButton.selected = NO;
            selectButton.tag = 27777+i;
            selectButton.backgroundColor = [UIColor clearColor];
            [selectButton addTarget:self action:@selector(selectButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
            [containerView addSubview:selectButton];
            if (i % 2 == 0) {
                [selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(containerView);
                    make.top.mas_equalTo(10+35*row);
                    make.width.mas_equalTo((SCREEN_WIDTH-80)/2-10);
                    make.height.mas_equalTo(22);
                    if (i == count-1) {
                        make.bottom.mas_equalTo(containerView).mas_offset(-20);
                    }
                }];
            }else{
                [selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo((SCREEN_WIDTH-80)/2+10);
                    make.right.mas_equalTo(containerView);
                    make.top.mas_equalTo(10+35*row);
                    make.width.mas_greaterThanOrEqualTo(20);
                    make.height.mas_equalTo(22);
                    if (i == count-1) {
                        make.bottom.mas_equalTo(containerView).mas_offset(-20);
                    }
                }];
            }
            
            UIImageView * selectImageView = [[UIImageView alloc] init];
            selectImageView.image = [UIImage imageNamed:@"bussiness_unselect"];
            selectImageView.tag = 28888+i;
            [selectButton addSubview:selectImageView];
            [selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(20, 20));
                make.left.mas_equalTo(selectButton);
                make.centerY.mas_equalTo(selectButton);
            }];
            
            UILabel * bussinessLabel = [[UILabel alloc] init];
            bussinessLabel.text = listModel.name;
            bussinessLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
            bussinessLabel.textColor = FWTextColor_000000;
            bussinessLabel.textAlignment = NSTextAlignmentLeft;
            [selectButton addSubview:bussinessLabel];
            [bussinessLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(selectButton);
                make.height.mas_equalTo(20);
                make.width.mas_greaterThanOrEqualTo(30);
                make.right.mas_equalTo(selectButton);
                make.left.mas_equalTo(selectImageView.mas_right).mas_offset(10);
            }];
        }
    }
    
    [certificationButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(44);
        make.left.mas_equalTo(self).mas_offset(40);
        make.right.mas_equalTo(self).mas_offset(-40);
        make.bottom.mas_equalTo(self).mas_offset(-40-FWSafeBottom);
    }];
}

- (void)selectButtonOnClick:(UIButton *)sender{
    
    /* 变换状态*/
    sender.selected = !sender.selected;

    NSInteger index = sender.tag - 27777;
    
    /* 找到对应imageview */
    NSInteger imageTag = 28888+index;
    UIImageView * imageView = (UIImageView *)[self viewWithTag:imageTag];
    
    FWBussinessCertificationListModel * listModel = cerModel.list[index];
    listModel.isSelect = sender.isSelected;

    if (sender.isSelected) {

        imageView.image = [UIImage imageNamed:@"bussiness_select"];

        if (![idArray containsObject:listModel.main_business_id]) {
            [idArray addObject:listModel.main_business_id];
        }
    }else{
        
        imageView.image = [UIImage imageNamed:@"bussiness_unselect"];

        if ([idArray containsObject:listModel.main_business_id]) {
            [idArray removeObject:listModel.main_business_id];
        }
    }
}


#pragma mark - > 认证
- (void)certificationButtonClick{
    
    NSString * idString = [idArray mj_JSONString];
    
     //去掉空格
    NSString *formatString = [certificationTF.text
                              stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (formatString.length <= 0) {
        
        [[FWHudManager sharedManager] showErrorMessage:@"请填写您的认证名称" toController:self.viewcontroller];
        return;
    }
    
    if (idArray.count <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择您的主营业务" toController:self.viewcontroller];
        return;
    }
    
    [self commitCertificationWithid:idString];
}

#pragma mark - > 返回
- (void)backButtonClick{
    
    [self.viewcontroller.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 商家认证
- (void)commitCertificationWithid:(NSString *)idString{
    
    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"main_business_ids":idString,
                              @"title":certificationTF.text,
                              };
    
    [request startWithParameters:params WithAction:Submit_identify_business WithDelegate:self.viewcontroller completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        NSDictionary * data = [back objectForKey:@"data"];
        
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:[data objectForKey:@"title"] message:[data objectForKey:@"desc"] preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"去看看" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                FWBussinessRankViewController * WVC = [[FWBussinessRankViewController alloc] init];
                [self.viewcontroller.navigationController pushViewController:WVC animated:YES];
            }];
            [alertController addAction:okAction];
            [self.viewcontroller presentViewController:alertController animated:YES completion:nil];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewcontroller];
        }
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([string isEqualToString:@"<"] ||
       [string isEqualToString:@">"] ||
       [string isEqualToString:@"/"] ||
       [string isEqualToString:@"“"] ||
       [string isEqualToString:@"•"] ||
       [string isEqualToString:@"’"] ||
       [string isEqualToString:@"…"] ||
       [string isEqualToString:@"……"] ){
        return NO;
    }
    
    UITextRange *selectedRange = textField.markedTextRange;
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    
    if (!position) {
        // 没有高亮选择的字
        // 1. 过滤非汉字、字母、数字字符
//        certificationTF.text = [self filterCharactor:textField.text withRegex:@"[^a-zA-Z0-9\u4e00-\u9fa5]"];
        // 2. 截取
        if (certificationTF.text.length > 16) {
            certificationTF.text = [certificationTF.text substringToIndex:16];
        }
    } else {
        // 有高亮选择的字 不做任何操作
    }
    
    return YES;
}

// 过滤字符串中的非汉字、字母、数字
- (NSString *)filterCharactor:(NSString *)string withRegex:(NSString *)regexStr{
    NSString *filterText = string;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexStr options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *result = [regex stringByReplacingMatchesInString:filterText options:NSMatchingReportCompletion range:NSMakeRange(0, filterText.length) withTemplate:@""];
    return result;
}

@end
