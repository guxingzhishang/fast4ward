//
//  FWUserTopView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, FWMineViewType) {
    FWMineType = 1,// 个人中心
    FWUserType,    // 用户个人信息
};

@protocol FWUserTopViewDelegate <NSObject>

- (void)headerBtnClick:(UIButton *)sender;
- (void)backButtonOnClick;
- (void)moreButtonOnClick;
- (void)shareButtonOnClick;

@end


@interface FWUserTopView : UIView


/* 顶部背景色 */
@property (nonatomic, strong) UIImageView * topBgImageView;

/* 返回 */
@property (nonatomic, strong) UIButton * blackBackButton;

/* 分享 */
@property (nonatomic, strong) UIButton * blackShareButton;

/* 更多 */
@property (nonatomic, strong) UIButton * blackMoreButton;

/* 头像 */
@property (nonatomic, strong) UIImageView * photoImageView;

/* 肆放东家图标 */
@property (nonatomic, strong) UIImageView * renzhengImageView;

@property (nonatomic, strong) UIView *background;//图片放大

/* 查看大图的view */
@property (nonatomic, strong) UIImageView * browseImgView;

/* 姓名 */
@property (nonatomic, strong) UILabel * nameLabel;

/* vip图标 */
@property (nonatomic, strong) UIButton * vipImageButton;

/* id */
@property (nonatomic, strong) UILabel * f4wIDLabel;

/* 认证 */
@property (nonatomic, strong) UIImageView * authenticationView;

/* 认证 */
@property (nonatomic, strong) UILabel * authenticationLabel;

/* 影响力按钮 */
@property (nonatomic, strong) UIButton * effectButton;

/* 影响力值 */
@property (nonatomic, strong) UILabel * effectLabel;

/* 关注 */
@property (nonatomic, strong) UIButton * attentionButton;

/* 影响力图标 */
@property (nonatomic, strong) UIImageView * effectImageView;

/* TA的店铺 */
@property (nonatomic, strong) UIButton * shopButton;

/* 获赞按钮 */
@property (nonatomic, strong) UIButton * thumbUpButton;

/* 关注按钮 */
@property (nonatomic, strong) UIButton * followButton;

/* 粉丝按钮 */
@property (nonatomic, strong) UIButton * funsButton;

@property (nonatomic, strong) UIView * flagLineView ;
/* 标签图标 */
@property (nonatomic, strong) UIImageView * signImageView;

/* 签名 */
@property (nonatomic, strong) UILabel * signLabel;

/* 认证座驾图标 */
@property (nonatomic, strong) UIImageView * carImageView;

/* 认证座驾 */
@property (nonatomic, strong) UILabel * carLabel;

/* 认证车手图标 */
@property (nonatomic, strong) UIImageView * carPlayerView;

/* 认证车手 */
@property (nonatomic, strong) UILabel * carPlayerLabel;


/* 认证车手右箭头 */
@property (nonatomic, strong) UIImageView * playerArrowImageView;


/* 承载着全部、视频、草稿箱 */
@property (nonatomic, strong) FWSelectBarView * selectBar;

/* 承载该视图的控制器 */
@property (nonatomic, weak) UIViewController * viewcontroller;

/* 代理 */
@property (nonatomic, weak) id<FWUserTopViewDelegate> delegate;

/* 之前头像地址 */
@property (nonatomic, strong) NSString * preHeader_URL;

/* 全局模型 */
@property (nonatomic, strong) FWMineModel * mineModel;

/* 用户页状态 1：个人（登录的用户） 2：其他用户的信息 */
@property (nonatomic, assign) FWMineViewType  mineViewType;

@property (nonatomic, assign) CGFloat  currentHeight;

- (void)initClickMethodWithController:(UIViewController *)delegate;

- (void)configForView:(id)model;
@end

NS_ASSUME_NONNULL_END
