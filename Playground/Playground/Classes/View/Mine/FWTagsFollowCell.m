//
//  FWTagsFollowCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/29.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWTagsFollowCell.h"

@implementation FWTagsFollowCell
@synthesize followButton;
@synthesize tagsFollowModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self changeSubviews];
    
    return self;
}

- (void)changeSubviews{

    self.photoImageView.hidden = YES;
    
    followButton = [[UIButton alloc] init];
    followButton.layer.cornerRadius = 2;
    followButton.layer.masksToBounds =YES;
    followButton.titleLabel.font = DHSystemFontOfSize_14;
    [followButton setTitle:@"互相关注" forState:UIControlStateNormal];
    [followButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
    [followButton addTarget:self action:@selector(followButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:followButton];
    [followButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(69,30));
    }];

    self.nameLabel.font = DHBoldSystemFontOfSize_16;
    self.signLabel.textColor = FWTextColor_969696;
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.contentView).mas_offset(15);
        make.top.mas_equalTo(self.contentView).mas_equalTo(15);
        make.right.mas_equalTo(self.followButton.mas_left).mas_offset(-5);
    }];
    
    [self.signLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.right.mas_equalTo(self.followButton.mas_left).mas_offset(-5);
    }];
    
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.followButton);
        make.left.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-1);
    }];
}

#pragma mark - > 关注标签
- (void)followButtonClick:(UIButton *)sender{
   
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSString * action ;
        
        if ([tagsFollowModel.follow_status isEqualToString:@"1"]) {
            //已关注
            action = Submit_cancel_follow_tags;
        }else if ([tagsFollowModel.follow_status isEqualToString:@"2"]){
            //未关注
            action = Submit_follow_tags;
        }
        
        FWTagsRequest * request = [[FWTagsRequest alloc] init];
        
        NSDictionary * param = @{
                                 @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                 @"tag_id":tagsFollowModel.tag_id
                                 };
        [request startWithParameters:param WithAction:action WithDelegate:self.viewController  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([action isEqualToString:Submit_cancel_follow_tags]) {
                    //取消关注成功
                    tagsFollowModel.follow_status = @"2";
                }else{
                    //关注成功
                    tagsFollowModel.follow_status = @"1";
                    
                    [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self.viewController];
                }
                [self cellConfigureFor:tagsFollowModel];
            }
        }];
    }
}

- (void)cellConfigureFor:(id)model{
    
    tagsFollowModel = (FWSearchTagsSubListModel *)model;
    
    if ([tagsFollowModel.ft_id isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        // 自己不显示关注
        followButton.hidden = YES;
    }else{
        followButton.hidden = NO;
    }
    
    self.selectImageView.hidden = YES;
    
    self.nameLabel.text = [NSString stringWithFormat:@"#%@",tagsFollowModel.tag_name];
    self.signLabel.text = [NSString stringWithFormat:@"%@篇帖子",tagsFollowModel.feed_count];
    
    NSString * buttonTitle;
    NSInteger state = [tagsFollowModel.follow_status integerValue];
    UIColor * titleColor = nil;
    UIColor * buttonBackgroundColor = nil;
    switch (state) {
        case 1:
        {
            buttonTitle = @"已关注";
            titleColor = FWTextColor_515151;
            buttonBackgroundColor = FWViewBackgroundColor_EEEEEE;
        }
            break;
        case 2:
        {
            buttonTitle = @"关注";
            titleColor = DHTitleColor_FFFFFF;
            buttonBackgroundColor = FWTextColor_222222;
        }
            break;
        default:
            break;
    }
    
    [followButton setBackgroundColor:buttonBackgroundColor];
    [followButton setTitle:buttonTitle forState:UIControlStateNormal];
    [followButton setTitleColor:titleColor forState:UIControlStateNormal];
}


- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
