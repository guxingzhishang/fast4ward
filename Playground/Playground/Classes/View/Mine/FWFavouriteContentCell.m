//
//  FWFavouriteContentCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWFavouriteContentCell.h"

@interface FWFavouriteContentCell ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSMutableArray<UIButton *> *buttons;
@property (nonatomic, strong) FWFavouriteContentTagModel *cellModel;
//@property (nonatomic, strong) NSDictionary *cellModel;

@end

@implementation FWFavouriteContentCell

- (NSMutableArray *)buttons {
    if (!_buttons) {
        _buttons = @[].mutableCopy;
    }
    return _buttons;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier delegate:(UIViewController *)delegate {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.delegate = delegate;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.titleLabel = [UILabel new];
        self.titleLabel.numberOfLines = 1;
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = FWTextColor_9EA3AB;
        self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
        [self.contentView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(70);
            make.height.mas_equalTo(20);
            make.left.mas_equalTo(self.contentView).mas_offset(40);
            make.top.mas_equalTo(self.contentView).mas_offset(10);
        }];
        
    }
    return self;
}

- (void)cellConfigureFor:(id)model {
    
    self.cellModel = (FWFavouriteContentTagModel *)model;
    self.titleLabel.text = self.cellModel.tag_name;
    
    [self.buttons enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    [self.buttons removeAllObjects];
    
    [self.cellModel.sub_list enumerateObjectsUsingBlock:^(FWFavouriteContentTagSubModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *buttonTitle = [NSString stringWithFormat:@"%@", obj.tag_name];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:buttonTitle forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
        button.tag = 2000+idx;
        button.layer.borderWidth = 1.0f;
        button.layer.cornerRadius = 15;
        [button addTarget:self action:@selector(buttonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttons addObject:button];
        [self.contentView addSubview:button];
        
        if ([obj.is_selected isEqualToString:@"1"]) {
            
            button.selected = YES;
        }else if ([obj.is_selected isEqualToString:@"2"]){
            
            button.selected = NO;
        }
        [self setButtonColorFor:button WithModel:obj];
        
        NSInteger adapter = 3;
        
        NSInteger line = idx / adapter;
        NSInteger row = idx % adapter;
//        NSInteger buttonWidth = (SCREEN_WIDTH - 20 * (adapter + 1)) / adapter;
        NSInteger buttonWidth = (SCREEN_WIDTH - 80 -15)/3;
        NSInteger buttonHeight = 30;
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(line * 40 + 10);
            make.left.mas_equalTo(self.contentView).mas_offset((buttonWidth + 5) * row +40);
            make.height.mas_offset(buttonHeight);
            make.width.mas_equalTo(buttonWidth);
        }];
    }];
    
    UIButton *button = [self.buttons lastObject];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-10);
    }];
}

#pragma mark - > 按钮点击事件

- (void)buttonClickEvent:(UIButton *)sender {
    
    
    sender.selected = !sender.selected;
    NSInteger index = [self.buttons indexOfObject:sender];
    
    FWFavouriteContentTagSubModel *tag = self.cellModel.sub_list[index];
    
    [self setButtonColorFor:sender WithModel:tag];
    
    if ([self.delegate respondsToSelector:@selector(cell:didSelect:)]) {
        
        [self.delegate cell:self didSelect:sender];
    }
}

- (void)setButtonColorFor:(UIButton *)button WithModel:(FWFavouriteContentTagSubModel *)model{
    
    UIColor *buttonTitleColor;
    UIColor *buttonLayerBorderColor;
    UIColor *buttonBackgroundColor;

    if (button.selected) {
        buttonTitleColor = FWViewBackgroundColor_FFFFFF ;
        buttonLayerBorderColor = [UIColor clearColor];
        buttonBackgroundColor = FWTextColor_68769F;
    } else {
        buttonTitleColor = FWTextColor_9EA3AB;
        buttonLayerBorderColor = FWTextColor_9EA3AB;
        buttonBackgroundColor = [UIColor clearColor];
    }
    
    [button setTitleColor:buttonTitleColor forState:UIControlStateNormal];
    [button setBackgroundColor:buttonBackgroundColor];
    button.layer.borderColor = buttonLayerBorderColor.CGColor;
}

@end
