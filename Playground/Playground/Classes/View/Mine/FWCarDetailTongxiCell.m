//
//  FWCarDetailTongxiCell.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/6.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWCarDetailTongxiCell.h"

@implementation FWCarDetailTongxiCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(5);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_equalTo(205);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-5);
    }];
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.picImageView = [[UIImageView alloc] init];
    [self.bgView addSubview:self.picImageView];
    [self.picImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.bgView);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(137);
    }];
    
    self.carNameLabel = [[UILabel alloc] init];
    self.carNameLabel.font = DHBoldFont(15);
    self.carNameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.carNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.picImageView addSubview:self.carNameLabel];
    [self.carNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(14);
        make.bottom.mas_equalTo(self.picImageView).mas_offset(-12);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.arrowImageView = [[UIImageView alloc] init];
    self.arrowImageView.image = [UIImage imageNamed:@"white_arrow"];
    [self.picImageView addSubview:self.arrowImageView];
    [self.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.picImageView).mas_offset(-13);
        make.centerY.mas_equalTo(self.carNameLabel);
        make.size.mas_equalTo(CGSizeMake(7, 12));
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.layer.cornerRadius = 36/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.bgView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(36, 36));
        make.top.mas_equalTo(self.picImageView.mas_bottom).mas_offset(15);
    }];
    [self.photoImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewClick)]];

    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = DHFont(15);
    self.nameLabel.textColor = FWTextColor_222222;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(8);
        make.top.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(21);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.bgView).mas_offset(93);
    }];
    
    self.signLabel = [[UILabel alloc] init];
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.signLabel.textColor = DHLineHexColor_LightGray_999999;
    self.signLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.signLabel];
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(8);
        make.top.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(21);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.bgView).mas_offset(93);
    }];
    
    self.attentionButton = [[UIButton alloc] init];
    self.attentionButton.layer.cornerRadius = 2;
    self.attentionButton.layer.masksToBounds = YES;
    self.attentionButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.attentionButton.backgroundColor = FWTextColor_222222;
    [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [self.attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.attentionButton addTarget:self action:@selector(attentionButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.attentionButton];
    [self.attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(60, 24));
        make.centerY.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
    }];
}

- (void)configForCell:(id)model{
 
    self.carModel = (FWCarListModel *)model;
    
    self.carNameLabel.text = self.carModel.car_nickname;
    self.nameLabel.text = self.carModel.user_info.nickname;
    if (self.carModel.gaizhuang_count.length <= 0) {
        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(8);
            make.centerY.mas_equalTo(self.photoImageView);
            make.height.mas_equalTo(21);
            make.width.mas_greaterThanOrEqualTo(10);
            make.right.mas_equalTo(self.bgView).mas_offset(93);
        }];
    }else{
        self.signLabel.text = self.carModel.gaizhuang_count;

        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(8);
            make.top.mas_equalTo(self.photoImageView);
            make.height.mas_equalTo(21);
            make.width.mas_greaterThanOrEqualTo(10);
            make.right.mas_equalTo(self.bgView).mas_offset(93);
        }];
        
        [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(8);
            make.top.mas_equalTo(self.nameLabel.mas_bottom);
            make.height.mas_equalTo(21);
            make.width.mas_greaterThanOrEqualTo(10);
            make.right.mas_equalTo(self.bgView).mas_offset(93);
        }];
    }
    
    self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.picImageView.clipsToBounds = YES;
    
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.clipsToBounds = YES;
    
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:self.carModel.car_cover] placeholderImage:[UIImage imageNamed:@""]];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.carModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    if ([self.carModel.is_followed isEqualToString:@"1"]) {
        /* 已关注 */
        self.attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
        [self.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
        [self.attentionButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
    }else{
        /* 未关注 */
        self.attentionButton.backgroundColor = FWTextColor_222222;
        [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
        [self.attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];

    }
}

- (void)attentionButtonClick{
    
    NSString * action ;
    
    if ([self.carModel.is_followed isEqualToString:@"2"]) {
        action = Submit_follow_users;
    }else{
        action = Submit_cancel_follow_users;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":self.carModel.user_info.uid,
                              };
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if ([action isEqualToString:Submit_follow_users]) {
                self.carModel.is_followed = @"1";
                self.attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
                [self.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
                [self.attentionButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];

                [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self.vc];
            }else{
                self.carModel.is_followed = @"2";
                self.attentionButton.backgroundColor = FWTextColor_222222;
                [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
                [self.attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
    
//    if ([self.carModel.is_followed isEqualToString:@"2"]) {
//        /* 未关注点击后变成已关注状态 */
//        self.attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
//        [self.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
//        [self.attentionButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
//
//    }else{
//        /* 已关注点击后变成未关注状态 */
//        self.attentionButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
//        self.attentionButton.layer.borderColor = FWTextColor_222222.CGColor;
//        self.attentionButton.layer.borderWidth = 1;
//        [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
//        [self.attentionButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
//    }
}

- (void)photoImageViewClick{
    FWNewUserInfoViewController * UVC = [[FWNewUserInfoViewController alloc] init];
    UVC.user_id = self.carModel.user_info.uid;
    [self.vc.navigationController pushViewController:UVC animated:YES];
}

@end
