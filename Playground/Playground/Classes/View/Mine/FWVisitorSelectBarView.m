//
//  FWVisitorSelectBarView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVisitorSelectBarView.h"

@implementation FWVisitorSelectBarView
@synthesize nameListButton;
@synthesize analysisButton;
@synthesize shadowView;
@synthesize honOneView;

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{

    nameListButton = [[UIButton alloc] init];
    nameListButton.selected = YES;
    nameListButton.tag = 44444;
    nameListButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [nameListButton setTitle:@"访客名单" forState:UIControlStateNormal];
    [nameListButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [nameListButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:nameListButton];
    
    analysisButton = [[UIButton alloc] init];
    analysisButton.tag = 44445;
    analysisButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [analysisButton setTitle:@"访客分析" forState:UIControlStateNormal];
    [analysisButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [analysisButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:analysisButton];
    
    shadowView= [[UIView alloc]init];
    shadowView.backgroundColor = FWTextColor_222222;
    shadowView.layer.masksToBounds = YES;
    shadowView.tag = 100000;
    [self addSubview:shadowView];
    
    [analysisButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(nameListButton);
        make.width.mas_equalTo(nameListButton);
        make.left.mas_equalTo(nameListButton.mas_right).mas_offset(20);
        make.top.mas_equalTo(nameListButton);
    }];
    
    nameListButton.frame = CGRectMake((SCREEN_WIDTH-200-30)/2, CGRectGetMaxY(honOneView.frame)+10, 100, 30);
    analysisButton.frame = CGRectMake(CGRectGetMaxX(nameListButton.frame)+20, CGRectGetMinY(nameListButton.frame), CGRectGetWidth(nameListButton.frame), CGRectGetHeight(nameListButton.frame));
    shadowView.frame = CGRectMake(CGRectGetMinX(nameListButton.frame)+(CGRectGetWidth(nameListButton.frame)-32)/2, CGRectGetMaxY(nameListButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);

    [nameListButton addTarget:self action:@selector(didClickHeadButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [analysisButton addTarget:self action:@selector(didClickHeadButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didClickHeadButtonAction:(UIButton *)sender{
    
    NSInteger index = sender.tag - 44444;
    
    if ([self.delegate respondsToSelector:@selector(didClickHeadButton:)]) {
        [self.delegate didClickHeadButton:index];
    }
}

@end
