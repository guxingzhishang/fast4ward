//
//  FWCarDetailPicHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/6.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWCarDetailPicHeaderView.h"

@implementation FWCarDetailPicHeaderView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.manageButton = [[UIButton alloc] init];
    [self.manageButton addTarget:self action:@selector(manageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.manageButton];
    self.manageButton.frame = CGRectMake(SCREEN_WIDTH-70, 0, 70, 20);
    
    
    self.manageImage = [[UIImageView alloc] init];
    self.manageImage.image = [UIImage imageNamed:@"wenda_qiehuan"];
    [self.manageButton addSubview:self.manageImage];
    [self.manageImage mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.manageButton);
        make.size.mas_equalTo(CGSizeMake(12, 11));
        make.right.mas_equalTo(self.manageButton).mas_offset(-14);
    }];
    
    
    self.manageLabel = [[UILabel alloc] init];
    self.manageLabel.text = @"管理";
    self.manageLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.manageLabel.textColor = FWColor(@"737DA0");
    self.manageLabel.textAlignment = NSTextAlignmentRight;
    [self.manageButton addSubview:self.manageLabel];
    [self.manageLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.manageButton);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.manageImage.mas_left).mas_offset(-5);
    }];
}

#pragma mark - > 图片管理
- (void)manageButtonClick{
    
    if ([self.delegate respondsToSelector:@selector(manageButtonOnClick)]) {
        [self.delegate manageButtonOnClick];
    }
}
@end
