//
//  FWCarDetailPicsCell.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/6.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWCarDetailPicsCell.h"

@implementation FWCarDetailPicsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
 
    self.leftCarPicView = [[UIImageView alloc] init];
    self.leftCarPicView.layer.cornerRadius = 2;
    self.leftCarPicView.layer.masksToBounds = YES;
    self.leftCarPicView.userInteractionEnabled = YES;
    self.leftCarPicView.contentMode = UIViewContentModeScaleAspectFill;
    self.leftCarPicView.clipsToBounds = YES;
    [self.contentView addSubview:self.leftCarPicView];
    [self.leftCarPicView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-38)/2, (127*(SCREEN_WIDTH-38)/2)/169));
        make.top.mas_equalTo(self.contentView).mas_offset(5);
    }];
    
    UITapGestureRecognizer * leftTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftButtonOnClick)];
    [self.leftCarPicView addGestureRecognizer:leftTap];
    
 
    self.rightCarPicView = [[UIImageView alloc] init];
    self.rightCarPicView.layer.cornerRadius = 2;
    self.rightCarPicView.layer.masksToBounds = YES;
    self.rightCarPicView.userInteractionEnabled = YES;
    self.rightCarPicView.contentMode = UIViewContentModeScaleAspectFill;
    self.rightCarPicView.clipsToBounds = YES;
    [self.contentView addSubview:self.rightCarPicView];
    [self.rightCarPicView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftCarPicView.mas_right).mas_offset(10);
        make.size.mas_equalTo(self.leftCarPicView);
        make.top.mas_equalTo(self.leftCarPicView);
    }];
    
    UITapGestureRecognizer * rightTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightButtonOnClick)];
    [self.rightCarPicView addGestureRecognizer:rightTap];
    
}

- (void)configForLeftView:(id)model{
    
    self.leftListModel = (FWCarImgsModel *)model;

    [self.leftCarPicView sd_setImageWithURL:[NSURL URLWithString:self.leftListModel.car_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

- (void)configForRightView:(id)model{
    
    self.rightListModel = (FWCarImgsModel *)model;

    [self.rightCarPicView sd_setImageWithURL:[NSURL URLWithString:self.rightListModel.car_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

//按钮点击事件
- (void)leftButtonOnClick{
    
    if (self.leftButtonBlock) {
        self.leftButtonBlock();
    }
}

- (void)rightButtonOnClick{
    
    if (self.rightButtonBlock) {
        self.rightButtonBlock();
    }
}
@end
