//
//  FWChangePhoneView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  FWChangePhoneView;

@protocol FWChangePhoneViewDelegate <NSObject>

- (void)backButtonClick;

@required
- (void)nextButtonClick;
- (void)resendButtonClick;

@optional

-(void)checkPassword:(NSString*)inputPwd;

- (void)codeInputView:(FWChangePhoneView *)codeInputView finished:(BOOL)finished;


@end

@interface FWChangePhoneView : UIView<UITextViewDelegate,UITextFieldDelegate>


@property (nonatomic,strong) UILabel * tipLabel;
@property (nonatomic,strong) UILabel * subLabel;
@property (nonatomic,strong) UIView * customView;
//@property (nonatomic,strong) UITextField * codeTextView;
@property (nonatomic,strong) IQTextView * codeTextView;
@property (nonatomic,strong) UIView * containerView;
@property (nonatomic,strong) UIButton * nextButton;
@property (nonatomic,strong) UIButton * backButton;

@property (nonatomic,copy) NSString * phoneNumber;

@property (nonatomic, weak) id<FWChangePhoneViewDelegate> delegate;

- (void)beginEdit;

- (void)endEdit;

- (NSString *)getCode;

- (void)encryptWithPhoneNumber:(NSString*)phoneNumber;

@end
