//
//  FWBussinessHomepageView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessHomepageView.h"
#import "FWBussinessHomePageModel.h"

#define PHOTOWIDTH 111

@interface FWBussinessHomepageView()

@end

@implementation FWBussinessHomepageView
@synthesize titleLabel;
@synthesize topBgImageView;
@synthesize blackBackButton;
@synthesize photoImageView;
@synthesize nameLabel;
@synthesize renzhengImageView;
@synthesize authenticationView;
@synthesize authenticationLabel;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, 211+FWCustomeSafeTop +16);
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    topBgImageView = [[UIImageView alloc] init];
    topBgImageView.userInteractionEnabled = YES;
    topBgImageView.image = [UIImage imageNamed:@"user_bg"];
    [self addSubview:topBgImageView];
    [topBgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 211+FWCustomeSafeTop));
    }];
    
    blackBackButton = [[UIButton alloc] init];
    [blackBackButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [blackBackButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:blackBackButton];
    [blackBackButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topBgImageView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(topBgImageView).mas_offset(30+FWCustomeSafeTop);
    }];
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"商家主页";
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [topBgImageView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(blackBackButton);
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(topBgImageView).mas_offset(60);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-60);
    }];
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.userInteractionEnabled = YES;
    photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    photoImageView.layer.cornerRadius = PHOTOWIDTH/2;
    photoImageView.layer.masksToBounds = YES;
    [topBgImageView addSubview:photoImageView];
    [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topBgImageView).mas_offset(30);
        make.top.mas_equalTo(topBgImageView).mas_offset(70+FWCustomeSafeTop);
        make.size.mas_equalTo(CGSizeMake(PHOTOWIDTH, PHOTOWIDTH));
    }];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cleckImageViewAction)];
    [photoImageView addGestureRecognizer:tapGesture];
    
    self.renzhengImageView = [[UIImageView alloc] init];
    self.renzhengImageView.image = [UIImage imageNamed:@"bussiness_renzheng"];
    [topBgImageView addSubview:self.renzhengImageView];
    [self.renzhengImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(photoImageView);
        make.bottom.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(75, 21));
    }];
    
    self.vipImageButton = [[UIButton alloc] init];
    [topBgImageView addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(photoImageView.mas_right).mas_offset(15);
        make.top.mas_equalTo(self.photoImageView).mas_offset(13);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 22];
    nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [topBgImageView addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.vipImageButton);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(self.vipImageButton.mas_right).mas_offset(2);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-10);
    }];
    
    authenticationView = [[UIImageView alloc] init];
    authenticationView.image = [UIImage imageNamed:@"primary_bussiness"];
    [topBgImageView addSubview:authenticationView];
    [authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.left.mas_equalTo(self.vipImageButton).mas_offset(0);
        make.top.mas_equalTo(nameLabel.mas_bottom).mas_offset(13);
    }];
    authenticationView.hidden = YES;
    
    authenticationLabel = [[UILabel alloc] init];
    authenticationLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    authenticationLabel.textAlignment = NSTextAlignmentLeft;
    authenticationLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.3);
    [topBgImageView addSubview:authenticationLabel];
    [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(15);
        make.centerY.mas_equalTo(authenticationView);
        make.left.mas_equalTo(authenticationView.mas_right).mas_offset(5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-5);
    }];
    authenticationLabel.hidden = YES;
}

- (void)cleckImageViewAction{
    
}

- (void)configViewForModel:(id)model{
    
    FWBussinessHomePageModel * bussinessModel = (FWBussinessHomePageModel *)model;
    
    [photoImageView sd_setImageWithURL:[NSURL URLWithString:bussinessModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    nameLabel.text = bussinessModel.user_info.nickname;
    
    CGFloat offset = 2;
    
    self.vipImageButton.enabled = YES;
    if ([bussinessModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([bussinessModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([bussinessModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(photoImageView.mas_right).mas_offset(14);
            make.top.mas_equalTo(self.photoImageView).mas_offset(13);
            make.size.mas_equalTo(CGSizeMake(0.5, 17));
        }];
        
        offset = -3;
    }
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.vipImageButton);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(self.vipImageButton.mas_right).mas_offset(offset);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-10);
    }];
    
    if (bussinessModel.user_info.title.length > 0) {
        
        if ([bussinessModel.user_info.cert_status isEqualToString:@"2"]){
            
            authenticationView.hidden = NO;
            authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
            
            [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(15);
                make.centerY.mas_equalTo(authenticationView);
                make.left.mas_equalTo(authenticationView.mas_right).mas_offset(5);
                make.width.mas_greaterThanOrEqualTo(10);
                make.right.mas_equalTo(topBgImageView).mas_offset(-5);
            }];
        }else{
            authenticationView.hidden = YES;
            [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(15);
                make.centerY.mas_equalTo(authenticationView);
                make.left.mas_equalTo(self.vipImageButton);
                make.width.mas_greaterThanOrEqualTo(10);
                make.right.mas_equalTo(topBgImageView).mas_offset(-5);
            }];
        }
        authenticationLabel.hidden = NO;
        authenticationLabel.text = bussinessModel.user_info.title;
        
    }else{
        authenticationView.hidden = YES;
        authenticationLabel.hidden = YES;
    }
}

- (void)backButtonClick{
    [self.vc.navigationController popViewControllerAnimated:YES];
}
@end
