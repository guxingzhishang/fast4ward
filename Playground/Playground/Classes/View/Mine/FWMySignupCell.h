//
//  FWMySignupCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/8.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWPerfectInfoModel.h"
#import "FWPlayerSignupModel.h"
#import "FWRegistrationHomeModel.h"
#import "FWPerfectInfomationViewController.h"
#import "FWPlayerShareViewController.h"
#import "FWPlayerSignupViewController.h"
#import "FWRegistrationHomeViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMySignupCell : UITableViewCell

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UILabel * statusLabel; // 报名成功、未缴费
@property (nonatomic, strong) UILabel * perfectLabel;

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * qrcodeImageView;
@property (nonatomic, strong) UIImageView * timeImageView;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UIImageView * areaImageView;
@property (nonatomic, strong) UILabel * areaLabel;

@property (nonatomic, strong) UIButton * continueButton;
@property (nonatomic, strong) UIButton * myCardButton;
@property (nonatomic, strong) UIButton * perfectInfoButton;

@property (nonatomic, strong) FWRegistrationHomeModel * homeModel;

@property (nonatomic, weak) UIViewController * vc;

- (void)configForCell:(id)model;
@end

NS_ASSUME_NONNULL_END
