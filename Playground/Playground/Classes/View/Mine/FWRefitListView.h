//
//  FWRefitListView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//
/**
 * 改装清单模块
 */
#import <UIKit/UIKit.h>
#import "FWRefitCategaryModel.h"
#import "FWEditRefitModuleViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^refitListHeightBlock)(CGFloat currentHeight);

@interface FWRefitListView : UIScrollView

@property (nonatomic, strong) FWRefitCategaryModel * categaryModel;
@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, strong) NSString * user_car_id;

@property (nonatomic, strong) NSString * type; // 1:添加座驾  2:座驾乡情

@property (nonatomic, copy) refitListHeightBlock heightBlock;

@end

NS_ASSUME_NONNULL_END
