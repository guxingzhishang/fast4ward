//
//  FWCarDetailPicHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/6.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWCarDetailPicHeaderViewDelegate <NSObject>

- (void)manageButtonOnClick;

@end

@interface FWCarDetailPicHeaderView : UIView

@property (nonatomic, strong) UIButton * manageButton;
@property (nonatomic, strong) UILabel * manageLabel;
@property (nonatomic, strong) UIImageView * manageImage;
@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) id<FWCarDetailPicHeaderViewDelegate>delegate;


@end

NS_ASSUME_NONNULL_END
