//
//  FWMineFunsCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/8.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMineFunsCell.h"
#import "FWUnionMessageModel.h"

@implementation FWMineFunsCell
@synthesize followButton;
@synthesize preArray;
@synthesize listModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    preArray = @[].mutableCopy;
    
    [self changeSubviews];
    
    return self;
}

- (void)changeSubviews{
    
    self.authenticationImageView.hidden = NO;
    [self.authenticationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.right.mas_equalTo(self.photoImageView).mas_offset(2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(0);
    }];
    
    self.vipImageButton = [[UIButton alloc] init];
    [self.vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.nameLabel);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
    
    followButton = [[UIButton alloc] init];
    followButton.layer.cornerRadius = 2;
    followButton.layer.masksToBounds = YES;
    followButton.titleLabel.font = DHSystemFontOfSize_14;
    [followButton setTitle:@"互相关注" forState:UIControlStateNormal];
    [followButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
    [followButton addTarget:self action:@selector(followButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:followButton];
    [followButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(69,30));
    }];
    
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-20);
        make.left.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-1);
    }];
    
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-175);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        make.top.mas_equalTo(self.photoImageView);
    }];
    
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.right.mas_equalTo(self.followButton.mas_left).mas_offset(-5);
    }];
}

- (void)cellConfigureFor:(id)model{

    listModel = (FWFollowUserListModel *)model;

    if ([listModel.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        // 自己不显示关注
        followButton.hidden = YES;
    }else{
        followButton.hidden = NO;
    }
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];

    self.selectImageView.hidden = YES;
    
    self.nameLabel.text = listModel.user_info.nickname;
    self.signLabel.text = listModel.user_info.autograph;
    
  
    if([listModel.user_info.cert_status isEqualToString:@"2"]) {
        self.nameLabel.textColor = FWTextColor_FFAF3C;
        self.authenticationImageView.hidden = NO;
        self.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        if ([listModel.user_info.merchant_cert_status isEqualToString:@"3"] &&
            ![listModel.user_info.cert_status isEqualToString:@"2"]){
            self.nameLabel.textColor = FWTextColor_2B98FA;
            self.authenticationImageView.hidden = YES;
        }else{
            self.authenticationImageView.hidden = YES;
            self.nameLabel.textColor = FWTextColor_000000;
        }
    }
    
    self.vipImageButton.enabled = YES;
    if ([listModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([listModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([listModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    NSString * buttonTitle;
    NSInteger state = [listModel.follow_status integerValue];
    UIColor * titleColor = nil;
    UIColor * buttonBackgroundColor = nil;
    switch (state) {
            
        case 1:
        {
            buttonTitle = @"已关注";
            titleColor = FWTextColor_515151;
            buttonBackgroundColor = FWViewBackgroundColor_EEEEEE;
        }
            break;
        case 2:
        {
            buttonTitle = @"关注";
            titleColor = FWViewBackgroundColor_FFFFFF;
            buttonBackgroundColor = FWTextColor_222222;
        }
            break;
        case 3:
        {
            buttonTitle = @"互相关注";
            titleColor = FWTextColor_515151;
            buttonBackgroundColor = FWViewBackgroundColor_EEEEEE;
        }
            break;
        default:
            break;
    }
    
    [followButton setBackgroundColor:buttonBackgroundColor];
    [followButton setTitle:buttonTitle forState:UIControlStateNormal];
    [followButton setTitleColor:titleColor forState:UIControlStateNormal];
}

#pragma mark - > 关注
- (void)followButtonClick:(UIButton *)sender{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([self.delegate respondsToSelector:@selector(followButtonForIndex:withCell:)]) {
            [self.delegate followButtonForIndex:sender.tag - 5000 withCell:self];
        }
    }
}

#pragma mark - > 点击用户头像
- (void)photoImageViewTapClick{
    
    if (nil == self.listModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.uid;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.viewController.navigationController pushViewController:VVC animated:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
