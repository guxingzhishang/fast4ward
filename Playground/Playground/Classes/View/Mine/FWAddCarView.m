//
//  FWAddCarView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWAddCarView.h"
#import "UIBarButtonItem+Item.h"
#import "MyDateTimeView.h"
#import "FWSelectCertificationCarViewController.h"
#import "FWChooseBradeViewController.h"
#import "FWCarDetailRefitListViewController.h"

@interface FWAddCarView ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) NSDictionary * dataDict;
@property (nonatomic, strong) UIImagePickerController * picker;
@property (nonatomic, strong) NSString * car_cover;
@property (nonatomic, strong) NSString * user_car_id;
@property (nonatomic, strong) MyDateTimeView * buyTimeTF;
@property (nonatomic, strong) FWCarListModel * listModel;
@property (nonatomic, strong) NSString * qingdanString;


@end

@implementation FWAddCarView

#pragma mark - > 请求阿里临时验证
- (void)requestStsData{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    [request requestStsTokenWithType:@"1"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestUploadName];
    });
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"car_cover",
                              @"number":@"1",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.dataDict = [back objectForKey:@"data"];
        }
    }];
}

- (id)init{
    self = [super init];
    if (self) {
       
        self.dataDict = @{};
        
        [self requestStsData];

        self.backgroundColor = FWColor(@"F6F8FA");
        
        
        self.scrollEnabled = YES;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.topView = [[UIView alloc] init];
    self.topView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.topView];
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(100);
    }];
    
    self.carImageView = [[UIImageView alloc] init];
    self.carImageView.layer.cornerRadius = 2;
    self.carImageView.layer.masksToBounds = YES;
    self.carImageView.userInteractionEnabled = YES;
    self.carImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.carImageView.clipsToBounds = YES;
    self.carImageView.image = [UIImage imageNamed:@"upload_card_placeholder"];
    [self.topView addSubview:self.carImageView];
    [self.carImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topView).mas_offset(14);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_equalTo(188*(SCREEN_WIDTH-28)/347);
        make.top.mas_equalTo(self.topView).mas_offset(15);
    }];
    [self.carImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoClickWithType)]];
    
    self.carReUploadLabel = [[UILabel alloc] init];
    self.carReUploadLabel.text = @"重新上传";
    self.carReUploadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.carReUploadLabel.backgroundColor = FWColorWihtAlpha(@"000000", 0.5);
    self.carReUploadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.carReUploadLabel.textAlignment = NSTextAlignmentCenter;
    [self.carImageView addSubview:self.carReUploadLabel];
    [self.carReUploadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.carImageView);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(self.carImageView);
        make.bottom.mas_equalTo(self.carImageView).mas_offset(0);
    }];
    self.carReUploadLabel.hidden = YES;
    
    self.nicknameLabel = [[UILabel alloc] init];
    self.nicknameLabel.text = @"昵称";
    self.nicknameLabel.frame = CGRectMake(15, 18, 250, 16);
    self.nicknameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.nicknameLabel.textColor = FWTextColor_12101D;
    self.nicknameLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.nicknameLabel];
    [self.nicknameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carImageView);
        make.size.mas_equalTo(CGSizeMake(150, 16));
        make.top.mas_equalTo(self.carImageView.mas_bottom).mas_offset(21);
    }];
    
    self.nicknameTF = [[IQTextView alloc] init];
    self.nicknameTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.nicknameTF.placeholder = @"你对爱车的专属称呼";
    self.nicknameTF.textContainerInset = UIEdgeInsetsMake(10,-5,0,0);
    self.nicknameTF.textColor = FWTextColor_12101D;
    [self.topView addSubview:self.nicknameTF];
    [self.nicknameTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nicknameLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 44));
        make.top.mas_equalTo(self.nicknameLabel.mas_bottom).mas_offset(0);
    }];
    
    self.nicknameLineView =  [[UIView alloc] init];
    self.nicknameLineView.backgroundColor = DHLineHexColor_LightGray_999999;
    [self.topView addSubview:self.nicknameLineView];
    [self.nicknameLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nicknameLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.nicknameTF);
        make.top.mas_equalTo(self.nicknameTF.mas_bottom).mas_offset(0);
    }];
    
    self.buyTimeLabel = [[UILabel alloc] init];
    self.buyTimeLabel.text = @"购车时间*";
    self.buyTimeLabel.frame = CGRectMake(15, 18, 250, 16);
    self.buyTimeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.buyTimeLabel.textColor = FWTextColor_12101D;
    self.buyTimeLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.buyTimeLabel];
    [self.buyTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nicknameLabel);
        make.size.mas_equalTo(CGSizeMake(150, 16));
        make.top.mas_equalTo(self.nicknameTF.mas_bottom).mas_offset(24);
    }];
    
    self.buyTimeTF = [[MyDateTimeView alloc]initWithFrame:CGRectMake(14, 320, SCREEN_WIDTH-28, 44)];
    self.buyTimeTF.pickerViewMode = MyDatePickerViewDateYearMonthMode;
    self.buyTimeTF.field.placeholder = @"还记得什么时候买的车吗";
    self.buyTimeTF.field.textColor = FWTextColor_12101D;
    [self.topView addSubview:self.buyTimeTF];
    [self.buyTimeTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.buyTimeLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 44));
        make.top.mas_equalTo(self.buyTimeLabel.mas_bottom).mas_offset(0);
    }];
    
    
    UIImageView * brandArrowImageView = [[UIImageView alloc] init];
    brandArrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.buyTimeTF addSubview:brandArrowImageView];
    [brandArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.centerY.mas_equalTo(self.buyTimeTF);
        make.right.mas_equalTo(self.buyTimeTF).mas_offset(-10);
    }];
    
   
    
    self.buyTimeLineView =  [[UIView alloc] init];
    self.buyTimeLineView.backgroundColor = DHLineHexColor_LightGray_999999;
    [self.topView addSubview:self.buyTimeLineView];
    [self.buyTimeLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.buyTimeLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.buyTimeTF);
        make.top.mas_equalTo(self.buyTimeTF.mas_bottom).mas_offset(0);
    }];
    
    self.carStyleLabel = [[UILabel alloc] init];
    self.carStyleLabel.text = @"车型*";
    self.carStyleLabel.frame = CGRectMake(15, 18, 250, 16);
    self.carStyleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.carStyleLabel.textColor = FWTextColor_12101D;
    self.carStyleLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.carStyleLabel];
    [self.carStyleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.buyTimeLabel);
        make.size.mas_equalTo(CGSizeMake(150, 16));
        make.top.mas_equalTo(self.buyTimeTF.mas_bottom).mas_offset(24);
    }];
    
    self.selectLabel = [[UILabel alloc] init];
    self.selectLabel.text = @"选择车型";
    self.selectLabel.userInteractionEnabled = YES;
    self.selectLabel.textColor = [UIColor colorWithHexString:@"c4c4c4" alpha:1];
    self.selectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:17];
    [self.topView addSubview:self.selectLabel];
    [self.selectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carStyleLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 44));
        make.top.mas_equalTo(self.carStyleLabel.mas_bottom).mas_offset(0);
    }];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCarClick)];
    [self.selectLabel addGestureRecognizer:tap];
    
    
    self.carStyleLineView =  [[UIView alloc] init];
    self.carStyleLineView.backgroundColor = DHLineHexColor_LightGray_999999;
    [self.topView addSubview:self.carStyleLineView];
    [self.carStyleLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carStyleLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.selectLabel);
        make.top.mas_equalTo(self.selectLabel.mas_bottom).mas_offset(0);
    }];
    
    self.maliLabel = [[UILabel alloc] init];
    self.maliLabel.text = @"马力";
    self.maliLabel.frame = CGRectMake(15, 18, 250, 16);
    self.maliLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.maliLabel.textColor = FWTextColor_12101D;
    self.maliLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.maliLabel];
    [self.maliLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.carStyleLabel);
        make.size.mas_equalTo(CGSizeMake(150, 16));
        make.top.mas_equalTo(self.selectLabel.mas_bottom).mas_offset(24);
    }];
    
    self.maliTF = [[IQTextView alloc] init];
    self.maliTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.maliTF.placeholder = @"ta的最大马力";
    self.maliTF.keyboardType = UIKeyboardTypeNumberPad;
    self.maliTF.textContainerInset = UIEdgeInsetsMake(10,-5,0,0);
    self.maliTF.textColor = FWTextColor_12101D;
    [self.topView addSubview:self.maliTF];
    [self.maliTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.maliLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 44));
        make.top.mas_equalTo(self.maliLabel.mas_bottom).mas_offset(0);
    }];
    
    self.maliLineView =  [[UIView alloc] init];
    self.maliLineView.backgroundColor = DHLineHexColor_LightGray_999999;
    [self.topView addSubview:self.maliLineView];
    [self.maliLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.maliLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(self.maliTF);
        make.top.mas_equalTo(self.maliTF.mas_bottom).mas_offset(0);
    }];
    
    self.descriptionLabel = [[UILabel alloc] init];
    self.descriptionLabel.text = @"改装清单*";
    self.descriptionLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.5];
    self.descriptionLabel.textColor = FWTextColor_12101D;
    self.descriptionLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.descriptionLabel];
    [self.descriptionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.maliLabel);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.top.mas_equalTo(self.maliTF.mas_bottom).mas_offset(24);
    }];
    
    self.descriptionTV = [[UILabel alloc] init];
    self.descriptionTV.numberOfLines = 0;
    self.descriptionTV.userInteractionEnabled = YES;
    self.descriptionTV.text = @"分享与TA的改装清单";
    self.descriptionTV.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
    self.descriptionTV.textColor = FWTextColor_BCBCBC;
    [self.topView addSubview:self.descriptionTV];
    [self.descriptionTV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.descriptionLabel);
        make.top.mas_equalTo(self.descriptionLabel.mas_bottom).mas_offset(20);
        make.right.mas_equalTo(self.topView).mas_offset(-14);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_greaterThanOrEqualTo(50);
        make.bottom.mas_equalTo(self.topView.mas_bottom).mas_offset(-20);
    }];
    [self.descriptionTV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gaizhuangListClick)]];

    
    self.descriptionLineView = [[UIView alloc] init];
    self.descriptionLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.topView addSubview:self.descriptionLineView];
    [self.descriptionLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.descriptionTV);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.descriptionTV.mas_bottom);
    }];
    
    self.saveButton = [[UIButton alloc] init];
    self.saveButton.layer.cornerRadius = 2;
    self.saveButton.layer.masksToBounds = YES;
    self.saveButton.backgroundColor = FWTextColor_222222;
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    [self.saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [self.saveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.saveButton addTarget:self action:@selector(saveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.saveButton];
    [self.saveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self).mas_offset(30);
        make.right.mas_equalTo(self).mas_offset(-30);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(SCREEN_WIDTH-60);
        make.bottom.mas_equalTo(self).mas_offset(-125);
    }];
}

#pragma mark - > 改装清单
- (void)gaizhuangListClick{
    
    DHWeakSelf;
    FWCarDetailRefitListViewController *VC = [[FWCarDetailRefitListViewController alloc] init];
    VC.showBlock = ^(NSDictionary * _Nonnull showDict) {
        if (showDict[@"commit"]) {
            weakSelf.qingdanString = showDict[@"commit"];
        }
        if (showDict[@"show"]) {
            if ([(NSArray *)showDict[@"show"] count] >0 ) {
                weakSelf.descriptionTV.textColor = FWTextColor_222222;
                weakSelf.descriptionTV.text = [showDict[@"show"] componentsJoinedByString:@"\n"];
            }else{
                weakSelf.descriptionTV.textColor = FWTextColor_BCBCBC;
                weakSelf.descriptionTV.text = @"分享与TA的改装清单";
            }
        }
    };
    [self.vc.navigationController pushViewController:VC animated:YES];
}

#pragma mark - > 配置数据
- (void)configForView:(id)model{
    
    if (model) {
        self.listModel = (FWCarListModel *)model;
        
        self.car_style_id = self.listModel.car_style_id;
        self.user_car_id = self.listModel.user_car_id;
        
        self.nicknameTF.text = self.listModel.car_nickname;
        self.buyTimeTF.field.text = self.listModel.car_buy_time;
        self.selectLabel.text = self.listModel.car_style_name;
        self.maliTF.text = self.listModel.car_mali;
        self.descriptionTV.text = self.listModel.car_desc;
        
        self.selectLabel.textColor = FWTextColor_141414;
        
        if (self.listModel.car_desc.length > 0) {
            
            [self.carImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.car_cover] placeholderImage:[UIImage imageNamed:@"upload_card"]];
            self.carImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.carImageView.clipsToBounds = YES;
            self.car_cover = self.listModel.car_cover;
        }
       
        if (self.listModel.car_cover.length > 0) {
            self.carReUploadLabel.hidden = NO;
        }else{
            self.carReUploadLabel.hidden = YES;
        }
    }
}

#pragma mark - > 上传图片
- (void)uploadPhotoClickWithType{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"上传证件照" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:1];
    }];
    UIAlertAction *selectAction = [UIAlertAction actionWithTitle:@"选择相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:2];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:takePhotoAction];
    [alertController addAction:selectAction];
    [alertController addAction:cancelAction];
    
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 选择相册
- (void)selectLibraryWithType:(NSInteger)type{
    
    if (@available(iOS 11, *)) {
        UIScrollView.appearance.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
    }
    
    NSUInteger sourceType = 0;
    
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeCamera;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
    }else{
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
    }
    
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.delegate                 = self;
    self.picker.sourceType               = sourceType;
    self.picker.allowsEditing            = NO;
    self.picker.navigationController.navigationBar.hidden = NO;
    self.picker.fd_prefersNavigationBarHidden = NO;
    self.picker.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.vc.navigationController presentViewController:self.picker animated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *avatar = info[UIImagePickerControllerOriginalImage];
    //处理完毕，回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    self.carImageView.image = avatar;
    
    [ALiOssUploadTool asyncUploadImage:avatar WithBucketName:[self.dataDict objectForKey:@"bucket"] WithObject_key:[self.dataDict objectForKey:@"object_key"] WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
        if (UploadImageSuccess == state) {
          
            dispatch_async(dispatch_get_main_queue(), ^{

                self.car_cover = [self.dataDict objectForKey:@"object_key"];
                self.carReUploadLabel.hidden = NO;
            });
        }
    }];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([UIDevice currentDevice].systemVersion.floatValue < 11) {
        return;
    }
    if ([viewController isKindOfClass:NSClassFromString(@"PUPhotoPickerHostViewController")]) {
        [viewController.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.frame.size.width < 42) {
                [viewController.view sendSubviewToBack:obj];
                *stop = YES;
            }
        }];
    }
}

#pragma mark - > 保存
- (void)saveButtonClick{
    
    self.saveButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.saveButton.enabled = YES;
    });
    /* 图片 */
    if (self.car_cover.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请上传座驾图片" toController:self.vc];
        return;
    }

    /* 昵称 */
    NSString * nickname = [self checkString:self.nicknameTF.text];
    if (nickname.length < 0 || nickname.length >16) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入16字以内的昵称哦" toController:self.vc];
        return;
    }

    /* 购车时间 */
    if (self.buyTimeTF.field.text.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择购车时间哦" toController:self.vc];
        return;
    }

    /* 车型* */
    if (self.car_style_id.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写车型信息" toController:self.vc];
        return;
    }

    /* 马力数据 */
    if ([self.maliTF.text integerValue] > 2000 || [self.maliTF.text integerValue] < 0 ){
        [[FWHudManager sharedManager] showErrorMessage:@"请如实填写马力值哦" toController:self.vc];
        return;
    }

    /* 改装清单 */
    NSString * descriptionString = [self checkString:self.descriptionTV.text];
    if (descriptionString.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写改装清单" toController:self.vc];
        return;
    }

    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];

    [request startWithParameters:[self getParams] WithAction:Submit_add_car WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"保存成功" toController:self.vc];
            
            if ([self.viewDelegate respondsToSelector:@selector(saveFinished)]) {
                [self.viewDelegate saveFinished];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

- (NSDictionary *)getParams{
    
    if (!self.car_cover) {
        self.car_cover = @"";
    }

    if (!self.car_style_id) {
        self.car_style_id = @"";
    }
    
    if (!self.user_car_id) {
        self.user_car_id = @"";
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"car_cover":self.car_cover,
                              @"car_nickname":self.nicknameTF.text,
                              @"car_buy_time":self.buyTimeTF.field.text,
                              @"car_style_id":self.car_style_id,
                              @"car_mali":self.maliTF.text,
                              @"gaizhuang_list":self.qingdanString,
                              @"user_car_id":self.user_car_id,
                              };
    
    return params;
}

#pragma mark - > 去掉前后空格
- (NSString *)checkString:(NSString *)string{
    
    NSCharacterSet  *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    string = [string stringByTrimmingCharactersInSet:set];
    return string;
}

#pragma mark - > 选择车型
- (void)selectCarClick{

    FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
    CBVC.chooseFrom = @"addCar";
    [self.vc.navigationController pushViewController:CBVC animated:YES];
}

@end
