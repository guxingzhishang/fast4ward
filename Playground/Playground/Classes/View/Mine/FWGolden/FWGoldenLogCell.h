//
//  FWGoldenLogCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWGoldenLogCell : UITableViewCell

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UILabel * leftOneLabel;
@property (nonatomic, strong) UILabel * leftTwoLabel;
@property (nonatomic, strong) UILabel * leftThreeLabel;
@property (nonatomic, strong) UILabel * leftFourLabel;
@property (nonatomic, strong) UILabel * leftFiveLabel;
@property (nonatomic, strong) UILabel * rightOneLabel;
@property (nonatomic, strong) UILabel * rightTwoLabel;
@property (nonatomic, strong) UILabel * rightThreeLabel;
@property (nonatomic, strong) UILabel * rightFourLabel;
@property (nonatomic, strong) UILabel * rightFiveLabel;



- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
