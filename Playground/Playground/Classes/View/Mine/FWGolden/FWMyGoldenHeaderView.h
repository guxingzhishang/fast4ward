//
//  FWMyGoldenHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWMyGoldenHeaderViewDelegate <NSObject>

- (void)signInClick;

@end

@interface FWMyGoldenHeaderView : UIView

@property (nonatomic, strong) UIImageView * topBgView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * ableTitleLabel;
@property (nonatomic, strong) UIButton * detailButton;
@property (nonatomic, strong) UILabel * goldenNumLabel;
@property (nonatomic, strong) UIButton * goldenDetailButton;

@property (nonatomic, strong) UIButton * qrcodeButton;

@property (nonatomic, strong) UIView * signBgView;
@property (nonatomic, strong) UILabel * signLabel;
@property (nonatomic, strong) UIView * dateView;

@property (nonatomic, strong) UIButton * signButton;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, assign) id<FWMyGoldenHeaderViewDelegate>delegate;


- (void)configForView:(id)model;
@end

NS_ASSUME_NONNULL_END
