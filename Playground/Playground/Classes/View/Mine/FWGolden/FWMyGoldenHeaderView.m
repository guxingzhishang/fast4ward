//
//  FWMyGoldenHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyGoldenHeaderView.h"
#import "FWGoldenDetailViewController.h"
#import "FWGoldenModel.h"
#import "FWBaomingQrcodeViewController.h"

@interface FWMyGoldenHeaderView ()
@property (nonatomic, strong) FWGoldenModel * goldenModel;
@end

@implementation FWMyGoldenHeaderView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.topBgView = [[UIImageView alloc] init];
    self.topBgView.userInteractionEnabled= YES;
    self.topBgView.image = [UIImage imageNamed:@"golden_top_bg"];
    [self addSubview:self.topBgView];
    [self.topBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 193+FWCustomeSafeTop));
    }];
    
    self.backButton = [[UIButton alloc] init];
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topBgView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topBgView).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.top.mas_equalTo(self.topBgView).mas_offset(20+FWCustomeSafeTop);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"我的金币";
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 18.9];
    self.titleLabel.textColor = FWColorWihtAlpha(@"ffffff", 0.8);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.topBgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.topBgView);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(100);
        make.centerY.mas_equalTo(self.backButton);
    }];
    
    self.goldenDetailButton = [[UIButton alloc] init];
    self.goldenDetailButton.layer.borderColor = FWViewBackgroundColor_FFFFFF.CGColor;
    self.goldenDetailButton.layer.borderWidth = 1;
    self.goldenDetailButton.layer.cornerRadius = 2;
    self.goldenDetailButton.layer.masksToBounds = YES;
    self.goldenDetailButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.goldenDetailButton setTitle:@"金币明细" forState:UIControlStateNormal];
    [self.goldenDetailButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.goldenDetailButton addTarget:self action:@selector(goldenDetailClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topBgView addSubview:self.goldenDetailButton];
    [self.goldenDetailButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.topBgView).mas_offset(-16);
        make.centerY.mas_equalTo(self.backButton);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(76);
    }];
    
    self.ableTitleLabel = [[UILabel alloc] init];
    self.ableTitleLabel.text = @"可用金币";
    self.ableTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.ableTitleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.ableTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.topBgView addSubview:self.ableTitleLabel];
    [self.ableTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topBgView).mas_offset(20);
        make.top.mas_equalTo(self.topBgView).mas_offset(75+FWCustomeSafeTop);
        make.height.mas_equalTo(18);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.detailButton = [[UIButton alloc] init];
    [self.detailButton setImage:[UIImage imageNamed:@"golden_top_ask"] forState:UIControlStateNormal];
    [self.detailButton addTarget:self action:@selector(detailButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topBgView addSubview:self.detailButton];
    [self.detailButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.ableTitleLabel.mas_right).mas_offset(0);
        make.centerY.mas_equalTo(self.ableTitleLabel);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(40);
    }];
    
    self.goldenNumLabel = [[UILabel alloc] init];
    self.goldenNumLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 45];
    self.goldenNumLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.goldenNumLabel.textAlignment = NSTextAlignmentLeft;
    [self.topBgView addSubview:self.goldenNumLabel];
    [self.goldenNumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.ableTitleLabel);
        make.top.mas_equalTo(self.ableTitleLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(40);
        make.width.mas_greaterThanOrEqualTo(10);
    }];

    self.qrcodeButton = [[UIButton alloc] init];
    self.qrcodeButton.layer.cornerRadius = 2;
    self.qrcodeButton.layer.masksToBounds = YES;
    self.qrcodeButton.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.4);
    self.qrcodeButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.qrcodeButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.qrcodeButton setTitle:@" 使用" forState:UIControlStateNormal];
    [self.qrcodeButton setImage:[UIImage imageNamed:@"golden_qrcode"] forState:UIControlStateNormal];
    [self.qrcodeButton addTarget:self action:@selector(qrcodeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topBgView addSubview:self.qrcodeButton];
    [self.qrcodeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.goldenNumLabel);
        make.size.mas_equalTo(CGSizeMake(76, 31));
        make.right.mas_equalTo(self.topBgView).mas_offset(-13);
    }];
    
    self.signBgView = [[UIView alloc] init];
    self.signBgView.layer.cornerRadius = 2;
    self.signBgView.layer.masksToBounds = YES;
    self.signBgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.signBgView];
    [self.signBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(14);
        make.right.mas_equalTo(self).mas_offset(-14);
        make.height.mas_equalTo(181);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.bottom.mas_equalTo(self).mas_offset(-5);
        make.top.mas_equalTo(self.topBgView.mas_bottom).mas_offset(-45+FWCustomeSafeTop);
    }];
    
    self.signLabel = [[UILabel alloc] init];
    self.signLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.signLabel.textColor = FWColor(@"#FF6F00");
    self.signLabel.textAlignment = NSTextAlignmentCenter;
    [self.signBgView addSubview:self.signLabel];
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.signBgView);
        make.top.mas_equalTo(self.signBgView).mas_offset(20);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(self.signBgView);
    }];
    
    
    self.dateView = [[UIView alloc] init];
    [self.signBgView addSubview:self.dateView];
    [self.dateView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.signBgView);
        make.top.mas_equalTo(self.signLabel.mas_bottom).mas_offset(15);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(46);
    }];
    
    self.signButton = [[UIButton alloc] init];
    self.signButton.layer.borderColor = FWViewBackgroundColor_FFFFFF.CGColor;
    self.signButton.layer.borderWidth = 1;
    self.signButton.layer.cornerRadius = 4;
    self.signButton.layer.masksToBounds = YES;
    self.signButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.signButton setTitle:@"签到赚金币" forState:UIControlStateNormal];
    self.signButton.backgroundColor = FWColor(@"#FF6F00");
    [self.signButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.signButton addTarget:self action:@selector(signButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.signBgView addSubview:self.signButton];
    [self.signButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.signBgView);
        make.bottom.mas_equalTo(self.signBgView).mas_offset(-20);
        make.height.mas_equalTo(36);
        make.width.mas_equalTo(106);
    }];

}

#pragma mark - > 二维码
- (void)qrcodeButtonClick{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_gold_writeoff_code WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWBaomingQrcodeViewController * BQVC = [[FWBaomingQrcodeViewController alloc] init];
            BQVC.qrcode = [[back objectForKey:@"data"] objectForKey:@"url"];
            BQVC.name = @"*向工作人员出示二维码，核销金币兑换精美礼品。";
            BQVC.exp_time =  [[[back objectForKey:@"data"] objectForKey:@"exp_time"] integerValue];
            BQVC.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
            BQVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            self.vc.modalPresentationStyle = UIModalPresentationCurrentContext;
            [self.vc presentViewController:BQVC animated:YES completion:nil];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

#pragma mark - > 赋值
- (void)configForView:(id)model{
    self.goldenModel = (FWGoldenModel *)model;
    
    self.goldenNumLabel.text = self.goldenModel.balance_value;
    self.signLabel.text = [NSString stringWithFormat:@"已经连续签到%@天",self.goldenModel.signin_continue_days];
    
    [self setupDateView];
}

- (void)setupDateView{
    
    for (UIView * view in self.dateView.subviews) {
        [view removeFromSuperview];
    }

    NSInteger count = self.goldenModel.signin_list.count;
    
    for (int i = 0; i <count; i++) {
        FWSignListModel * listModel = self.goldenModel.signin_list[i];
        
        UIView * bgView =[[UIView alloc] init];
        bgView.layer.cornerRadius = 2;
        bgView.layer.masksToBounds = YES;
        [self.dateView addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(self.dateView);
            make.height.mas_equalTo(46);
            make.width.mas_equalTo(41);
            make.left.mas_equalTo(self.dateView).mas_offset(12.5+((41+(SCREEN_WIDTH-32-25-41*7)/6)*i));
        }];
        
        UILabel * dayLabel = [[UILabel alloc] init];
        dayLabel.textAlignment = NSTextAlignmentCenter;
        dayLabel.textColor = FWViewBackgroundColor_FFFFFF;
        [bgView addSubview:dayLabel];
        [dayLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(bgView);
            make.height.mas_equalTo(22);
            make.width.mas_equalTo(bgView);
        }];
        
        UIView * dayLineView = [[UIView alloc] init];
        dayLineView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [bgView addSubview:dayLineView];
        [dayLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(bgView).mas_offset(2);
            make.right.mas_equalTo(bgView).mas_offset(-2);
            make.height.mas_equalTo(1);
            make.centerY.mas_equalTo(bgView);
        }];
        
        UILabel * pointLabel = [[UILabel alloc] init];
        pointLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        pointLabel.textColor = FWViewBackgroundColor_FFFFFF;
        pointLabel.textAlignment = NSTextAlignmentCenter;
        [bgView addSubview:pointLabel];
        [pointLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.mas_equalTo(bgView);
            make.height.mas_equalTo(22);
            make.width.mas_equalTo(bgView);
        }];
        
        NSString * textString = [NSString stringWithFormat:@"%@天",listModel.index];
        NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:textString];
        [str2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Semibold" size: 20] range:NSMakeRange(0, 1)];
        [str2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Regular" size: 12] range:NSMakeRange(1, 1)];
        dayLabel.attributedText = str2;
        
        pointLabel.text = [NSString stringWithFormat:@"+%@",listModel.value];
        
        /*
         available_status 1 可签， 2 不可签
         sign_status 1 已签  2未签
         */
        
        if ([listModel.sign_status isEqualToString:@"1"]) {
            /* 已签 */
            bgView.backgroundColor = FWColorWihtAlpha(@"ff6f00", 0.5);
            self.signButton.backgroundColor = FWColorWihtAlpha(@"ff6f00", 0.5);
        }else if([listModel.sign_status isEqualToString:@"2"] &&
                 [listModel.available_status isEqualToString:@"1"]){
            /* 当日可签，并且未签到 */
            bgView.backgroundColor = FWColor(@"ff6f00");
            self.signButton.backgroundColor = FWColor(@"#FF6F00");
        }else if([listModel.sign_status isEqualToString:@"2"] &&
        [listModel.available_status isEqualToString:@"2"]){
            /* 未签到 */
            bgView.backgroundColor = FWColor(@"cdcdcd");
        }
    }
}

#pragma mark - > 返回
- (void)backButtonClick{
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 详情
- (void)detailButtonClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.goldenModel.help_url;
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 签到
- (void)signButtonClick{
    if ([self.delegate respondsToSelector:@selector(signInClick)]) {
        [self.delegate signInClick];
    }
}

#pragma mark - > 金币明细
- (void)goldenDetailClick{
    [self.vc.navigationController pushViewController:[FWGoldenDetailViewController new] animated:YES];
}
@end
