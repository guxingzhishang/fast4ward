//
//  FWGoldenLogCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoldenLogCell.h"
#import "FWGoldenLogModel.h"

@implementation FWGoldenLogCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{

    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.left.mas_equalTo(self.contentView).mas_offset(32);
        make.right.mas_equalTo(self.contentView).mas_offset(-32);
        make.height.mas_equalTo(150);
        make.width.mas_equalTo(SCREEN_WIDTH-64);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.leftOneLabel = [[UILabel alloc] init];
    self.leftOneLabel.text = @"用户 I D";
    self.leftOneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.leftOneLabel.textColor = FWTextColor_B6BCC4;
    self.leftOneLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftOneLabel];
    [self.leftOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(15);
        make.left.mas_equalTo(self.bgView).mas_offset(17);
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(16);
    }];
    
    self.leftTwoLabel = [[UILabel alloc] init];
    self.leftTwoLabel.text = @"用户昵称";
    self.leftTwoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.leftTwoLabel.textColor = FWTextColor_B6BCC4;
    self.leftTwoLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftTwoLabel];
    [self.leftTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leftOneLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.leftOneLabel);
        make.width.mas_equalTo(self.leftOneLabel);
        make.height.mas_equalTo(self.leftOneLabel);
    }];
    
    self.leftThreeLabel = [[UILabel alloc] init];
    self.leftThreeLabel.text = @"核销单号";
    self.leftThreeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.leftThreeLabel.textColor = FWTextColor_B6BCC4;
    self.leftThreeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftThreeLabel];
    [self.leftThreeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leftTwoLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.leftTwoLabel);
        make.width.mas_equalTo(self.leftTwoLabel);
        make.height.mas_equalTo(self.leftTwoLabel);
    }];
    
    self.leftFourLabel = [[UILabel alloc] init];
    self.leftFourLabel.text = @"核销数量";
    self.leftFourLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.leftFourLabel.textColor = FWTextColor_B6BCC4;
    self.leftFourLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftFourLabel];
    [self.leftFourLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leftThreeLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.leftThreeLabel);
        make.width.mas_equalTo(self.leftThreeLabel);
        make.height.mas_equalTo(self.leftThreeLabel);
    }];
    
    self.leftFiveLabel = [[UILabel alloc] init];
    self.leftFiveLabel.text = @"核销时间";
    self.leftFiveLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.leftFiveLabel.textColor = FWTextColor_B6BCC4;
    self.leftFiveLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.leftFiveLabel];
    [self.leftFiveLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leftFourLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.leftFourLabel);
        make.width.mas_equalTo(self.leftFourLabel);
        make.height.mas_equalTo(self.leftFourLabel);
    }];
    
    self.rightOneLabel = [[UILabel alloc] init];
    self.rightOneLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.rightOneLabel.textColor = FWTextColor_222222;
    self.rightOneLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.rightOneLabel];
    [self.rightOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.leftOneLabel);
        make.left.mas_equalTo(self.leftOneLabel.mas_right).mas_offset(12);
        make.width.mas_greaterThanOrEqualTo(55);
        make.right.mas_equalTo(self.bgView).mas_offset(-5);
        make.height.mas_equalTo(16);
    }];
    
    self.rightTwoLabel = [[UILabel alloc] init];
    self.rightTwoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.rightTwoLabel.textColor = FWTextColor_222222;
    self.rightTwoLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.rightTwoLabel];
    [self.rightTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.leftTwoLabel);
        make.left.mas_equalTo(self.leftTwoLabel.mas_right).mas_offset(12);
        make.width.mas_greaterThanOrEqualTo(55);
        make.right.mas_equalTo(self.bgView).mas_offset(-5);
        make.height.mas_equalTo(16);
    }];
    
    self.rightThreeLabel = [[UILabel alloc] init];
    self.rightThreeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.rightThreeLabel.textColor = FWTextColor_222222;
    self.rightThreeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.rightThreeLabel];
    [self.rightThreeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.leftThreeLabel);
        make.left.mas_equalTo(self.leftThreeLabel.mas_right).mas_offset(12);
        make.width.mas_greaterThanOrEqualTo(55);
        make.right.mas_equalTo(self.bgView).mas_offset(-5);
        make.height.mas_equalTo(16);
    }];
    
    self.rightFourLabel = [[UILabel alloc] init];
    self.rightFourLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.rightFourLabel.textColor = FWTextColor_222222;
    self.rightFourLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.rightFourLabel];
    [self.rightFourLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.leftFourLabel);
        make.left.mas_equalTo(self.leftFourLabel.mas_right).mas_offset(12);
        make.width.mas_greaterThanOrEqualTo(55);
        make.right.mas_equalTo(self.bgView).mas_offset(-5);
        make.height.mas_equalTo(16);
    }];
    
    self.rightFiveLabel = [[UILabel alloc] init];
    self.rightFiveLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.rightFiveLabel.textColor = FWTextColor_222222;
    self.rightFiveLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.rightFiveLabel];
    [self.rightFiveLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.leftFiveLabel);
        make.left.mas_equalTo(self.leftFiveLabel.mas_right).mas_offset(12);
        make.width.mas_greaterThanOrEqualTo(55);
        make.right.mas_equalTo(self.bgView).mas_offset(-5);
        make.height.mas_equalTo(16);
    }];
}

- (void)configForCell:(id)model{
    
    FWGoldenLogListModel * listModel = (FWGoldenLogListModel *)model;
    
    self.rightOneLabel.text = listModel.user_info.f4w_id;
    self.rightTwoLabel.text = listModel.user_info.nickname;
    self.rightThreeLabel.text = listModel.op_item_id;
    self.rightFourLabel.text = [NSString stringWithFormat:@"%@金币",listModel.gold_value];
    self.rightFiveLabel.text = listModel.create_time;
}

@end
