//
//  FWGoldenDetailCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoldenDetailCell.h"
#import "FWGoldenModel.h"

@implementation FWGoldenDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = FWTextColor_F8F8F8;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.titleLabel.textColor = FWTextColor_222222;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.contentLabel.textColor = FWColor(@"BCBCBC");
    self.contentLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.contentLabel];
    [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(0);
        make.left.mas_equalTo(self.titleLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(17);
    }];
    
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.numLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.numLabel.backgroundColor = FWColor(@"FF6F00");
    self.numLabel.layer.cornerRadius = 2;
    self.numLabel.layer.masksToBounds = YES;
    self.numLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.numLabel];
    [self.numLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.width.mas_equalTo(51);
        make.height.mas_equalTo(23);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = FWColor(@"E9E9E9");
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.numLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-0.5);
    }];
}

- (void)configForCell:(id)model{
    
    FWGoldenListModel * listModel = (FWGoldenListModel *)model;
    
    self.titleLabel.text = listModel.op_type_str;
    self.contentLabel.text = listModel.create_time;
    
    if ([listModel.gold_value integerValue] < 0) {
        self.numLabel.text = [NSString stringWithFormat:@"%@",listModel.gold_value];
    }else{
        self.numLabel.text = [NSString stringWithFormat:@"+%@",listModel.gold_value];
    }
}
@end
