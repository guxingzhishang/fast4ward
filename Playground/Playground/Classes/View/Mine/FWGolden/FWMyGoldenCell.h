//
//  FWMyGoldenCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWGoldenModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWMyGoldenCellDelegate <NSObject>

- (void)statusButtonOnClick;

@end

@interface FWMyGoldenCell : UITableViewCell

@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIButton * iconImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * pointLabel;

@property (nonatomic, strong) UIButton * goAllButton;
@property (nonatomic, strong) UIButton * goButton;

@property (nonatomic, strong) UIButton * statusButton;

@property (nonatomic, strong) FWTaskSubListModel * listModel;
@property (nonatomic, strong) NSString * type;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, weak) id <FWMyGoldenCellDelegate>delegate;


- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
