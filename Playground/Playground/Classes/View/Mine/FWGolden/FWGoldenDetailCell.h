//
//  FWGoldenDetailCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWGoldenDetailCell : UITableViewCell

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * contentLabel;
@property (nonatomic, strong) UILabel * numLabel;
@property (nonatomic, strong) UIView * lineView;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
