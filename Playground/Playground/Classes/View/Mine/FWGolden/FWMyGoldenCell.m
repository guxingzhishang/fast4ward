
//
//  FWMyGoldenCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyGoldenCell.h"
#import "FWThirdBindingViewController.h"
#import "FWEditInformationViewController.h"
@implementation FWMyGoldenCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = FWTextColor_F8F8F8;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(16);
        make.top.mas_equalTo(self.contentView).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-32, 49));
        make.bottom.mas_equalTo(self.contentView).mas_offset(-10);
    }];
    
    self.iconImageView = [[UIButton alloc] init];
    [self.iconImageView setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.bgView addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bgView);
        make.left.mas_equalTo(self.bgView).mas_offset(18);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.titleLabel.textColor = FWTextColor_222222;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.iconImageView);
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(14);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];
    
    self.pointLabel = [[UILabel alloc] init];
    self.pointLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.pointLabel.textColor = FWColor(@"ff6f00");
    self.pointLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.pointLabel];
    [self.pointLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.titleLabel);
        make.left.mas_equalTo(self.titleLabel.mas_right).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(18);
    }];
    
    self.statusButton = [[UIButton alloc] init];
    self.statusButton.layer.borderColor = FWColor(@"#FF6F00").CGColor;
    self.statusButton.layer.borderWidth = 1;
    self.statusButton.layer.cornerRadius = 2;
    self.statusButton.layer.masksToBounds = YES;
    self.statusButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.statusButton setTitle:@"未完成" forState:UIControlStateNormal];
    [self.statusButton setTitleColor:FWColor(@"FF6F00") forState:UIControlStateNormal];
    [self.statusButton addTarget:self action:@selector(statusButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.statusButton.enabled = NO;
    [self.bgView addSubview:self.statusButton];
    [self.statusButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bgView);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.width.mas_equalTo(51);
        make.height.mas_equalTo(23);
    }];
    
    self.goButton = [[UIButton alloc] init];
    self.goButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    
    [self.goButton setImage:[UIImage imageNamed:@"go_right_arrow"] forState:UIControlStateNormal];
    [self.goButton setTitle:@"GO" forState:UIControlStateNormal];
    [self.goButton setTitleColor:FWTextColor_67769E forState:UIControlStateNormal];
    CGFloat leftlab = [@"GO" sizeWithAttributes:@{NSFontAttributeName : self.goButton.titleLabel.font}].width;
    [self.goButton setImageEdgeInsets:UIEdgeInsetsMake(0, leftlab+10, 0, 0)];
    [self.goButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    
    [self.goButton setTitleColor:FWColor(@"FF6F00") forState:UIControlStateNormal];
    [self.goButton addTarget:self action:@selector(goButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.goButton];
    [self.goButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bgView);
        make.right.mas_equalTo(self.statusButton.mas_left).mas_offset(0);
        make.width.mas_equalTo(51);
        make.height.mas_equalTo(self.bgView);
    }];
    self.goButton.hidden = YES;
    
    
    self.goAllButton = [[UIButton alloc] init];
    [self.goAllButton addTarget:self action:@selector(goButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.goAllButton];
    [self.goAllButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView);
        make.centerY.mas_equalTo(self.bgView);
        make.right.mas_equalTo(self.goButton.mas_left);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(51);
    }];
    self.goAllButton.hidden = YES;
}

- (void)configForCell:(id)model{
    
    self.listModel = (FWTaskSubListModel *)model;

    self.titleLabel.text = self.listModel.name;
    self.pointLabel.text = [NSString stringWithFormat:@"+%@",self.listModel.value];

    NSString * imageIndex ;
    if ([self.type isEqualToString:@"每日任务"]) {
        imageIndex = [NSString stringWithFormat:@"day_%@",self.listModel.icon_id];
        
        if ([self.listModel.icon_id isEqualToString:@"7"] &&
            [self.listModel.complete_status isEqualToString:@"2"]) {
            /* 发布小视频 && 未完成 带go */
            self.goButton.hidden = NO;
            self.goAllButton.hidden = NO;
        }else{
            self.goButton.hidden = YES;
            self.goAllButton.hidden = YES;
        }
    }else{
        imageIndex = [NSString stringWithFormat:@"new_%@",self.listModel.icon_id];
        
        if([self.listModel.complete_status isEqualToString:@"2"]){
            if ([self.listModel.icon_id isEqualToString:@"1"] ||
                [self.listModel.icon_id isEqualToString:@"2"] ||
                [self.listModel.icon_id isEqualToString:@"3"] ||
                [self.listModel.icon_id isEqualToString:@"11"] ) {
                /* 上传头像、修改昵称、绑定微信号、完善个性签名 带go */
                self.goButton.hidden = NO;
                self.goAllButton.hidden = NO;
            }else{
                self.goButton.hidden = YES;
                self.goAllButton.hidden = YES;
            }
        }else{
            /* 已完成 */
            self.goButton.hidden = YES;
            self.goAllButton.hidden = YES;
        }
    }
    
    if ([self.listModel.complete_status isEqualToString:@"2"]) {
        /* 未完成 */
        self.statusButton.enabled = NO;
        self.statusButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.statusButton.layer.borderColor = FWColor(@"#FF6F00").CGColor;
        self.statusButton.layer.borderWidth = 1;
        [self.statusButton setTitleColor:FWColor(@"FF6F00") forState:UIControlStateNormal];
    }else{
        /* 已完成 */
        if ([self.listModel.get_status isEqualToString:@"1"]) {
            /* 已领取 */
            self.statusButton.enabled = NO;
            self.statusButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
            self.statusButton.layer.borderColor = FWClearColor.CGColor;
            [self.statusButton setTitleColor:FWColor(@"BCBCBC") forState:UIControlStateNormal];
        }else{
            /* 未领取 */
            self.statusButton.enabled = YES;
            self.statusButton.backgroundColor = FWViewBackgroundColor_FF6F00;
            self.statusButton.layer.borderColor = FWClearColor.CGColor;
            [self.statusButton setTitleColor:FWColor(@"ffffff") forState:UIControlStateNormal];
        }
    }
    [self.statusButton setTitle:self.listModel.complete_status_str forState:UIControlStateNormal];
    
    [self.iconImageView setImage:[UIImage imageNamed:imageIndex] forState:UIControlStateNormal];
}

#pragma mark - > 领取
- (void)statusButtonClick{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"icon_id":self.listModel.icon_id,
    };
    
    [request startWithParameters:params WithAction:Submit_get_gold_by_task WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [[FWHudManager sharedManager] showErrorMessage:@"领取成功" toController:self.vc];

            
            if ([self.delegate respondsToSelector:@selector(statusButtonOnClick)]) {
                [self.delegate statusButtonOnClick];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

#pragma mark - > 去完成任务
- (void)goButtonClick{
    
    if ([self.listModel.icon_id isEqualToString:@"7"] && [self.listModel.complete_status isEqualToString:@"2"]) {
        /* 发布小视频 未完成 */
        [self participateButtonOnClick];
    }else{
        if([self.listModel.complete_status isEqualToString:@"2"]){
            if ([self.listModel.icon_id isEqualToString:@"1"]) {
                /* 上传头像 */
                [self.vc.navigationController pushViewController:[FWEditInformationViewController new] animated:YES];
            }else if([self.listModel.icon_id isEqualToString:@"2"]){
                /* 修改昵称 */
                [self.vc.navigationController pushViewController:[FWEditInformationViewController new] animated:YES];
            }else if([self.listModel.icon_id isEqualToString:@"11"]){
                /* 完善个性签名 */
                [self.vc.navigationController pushViewController:[FWEditInformationViewController new] animated:YES];
            }else if([self.listModel.icon_id isEqualToString:@"3"]){
                /* 绑定微信号 */
                [self.vc.navigationController pushViewController:[FWThirdBindingViewController new] animated:YES];
            }
        }
    }
}

#pragma mark - > 参与活动
- (void)participateButtonOnClick{
    
    [self checkLogin];
    
    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self.vc];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self.vc];
        }
    }else {
        if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
            
            [TalkingData trackEvent:@"每日任务-发布" label:@"" parameters:nil];
            
            FWPublishViewController * vc = [[FWPublishViewController alloc] init];
            vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
            vc.fd_prefersNavigationBarHidden = YES;
            vc.navigationController.navigationBar.hidden = YES;
            
            [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
            
            CATransition *animation = [CATransition animation];
            [animation setDuration:0.4];
            [animation setType: kCATransitionMoveIn];
            [animation setSubtype: kCATransitionFromTop];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            [self.vc.navigationController pushViewController:vc animated:NO];
            [self.vc.navigationController.view.layer addAnimation:animation forKey:nil];
        }
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}
@end
