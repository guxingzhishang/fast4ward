//
//  FWMineView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMineView.h"
#import "FWEditInformationViewController.h"
#import "FWFollowViewController.h"
#import "FWFunsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "FWSettingViewController.h"
#import "FWMineModel.h"
#import "FWInviteActivityViewController.h"
#import "FWChangePhotoViewController.h"

#define PHOTOWIDTH 111

@interface FWMineView ()

@end

@implementation FWMineView

@synthesize topBgImageView;
@synthesize photoImageView;
@synthesize nameLabel;
@synthesize vipImageButton;
@synthesize thumbUpButton;
@synthesize followButton;
@synthesize funsButton;
@synthesize thumbUpView;
@synthesize signLabel;
@synthesize mineModel;
@synthesize preHeader_URL;
@synthesize authenticationView;
@synthesize authenticationLabel;
@synthesize effectButton;
@synthesize effectImageView;
@synthesize effectLabel;
@synthesize f4wIDLabel;
@synthesize lightView;
@synthesize funsLabel;
@synthesize followLabel;
@synthesize thumbUpLabel;
@synthesize mineButton;
@synthesize activityImageView;

- (instancetype)init{
    
    self = [super init];
    
    if (self) {

        preHeader_URL = @"";
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.userInteractionEnabled = YES;
        [self setupTopSubviews];
    }
    
    return self;
}

#pragma mark - > 初始化头部视图
- (void)setupTopSubviews{
    
    topBgImageView = [[UIImageView alloc] init];
    topBgImageView.userInteractionEnabled = YES;
    topBgImageView.image = [UIImage imageNamed:@"mine_bg"];
    [self addSubview:topBgImageView];
    [topBgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 268));
    }];
    
    self.messageButton = [[UIButton alloc] init];
    [self.messageButton setImage:[UIImage imageNamed:@"mine_message"] forState:UIControlStateNormal];
    [self.messageButton addTarget:self action:@selector(messageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:self.messageButton];
    [self.messageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.right.mas_equalTo(topBgImageView).mas_offset(-10);
        if (FWCustomeSafeTop== 0) {
            make.top.mas_equalTo(topBgImageView).mas_offset(20);
        }else{
            make.top.mas_equalTo(topBgImageView).mas_offset(30);
        }
    }];
    
    self.unReadLabel = [[UILabel alloc] init];
    self.unReadLabel.layer.cornerRadius = 8;
    self.unReadLabel.layer.masksToBounds = YES;
    self.unReadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    self.unReadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.unReadLabel.textAlignment = NSTextAlignmentCenter;
    self.unReadLabel.backgroundColor = DHRedColorff6f00;
    [self.messageButton addSubview:self.unReadLabel];
    [self.unReadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.messageButton).mas_offset(-8);
        make.height.mas_equalTo(16);
        make.width.mas_greaterThanOrEqualTo(16);
        make.right.mas_equalTo(self.messageButton).mas_offset(-3);
    }];
    self.unReadLabel.hidden = YES;
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.userInteractionEnabled = YES;
    photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    photoImageView.layer.cornerRadius = PHOTOWIDTH/2;
    photoImageView.layer.masksToBounds = YES;
    [topBgImageView addSubview:photoImageView];
    [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topBgImageView).mas_offset(30);
        make.top.mas_equalTo(topBgImageView).mas_offset(64);
        make.size.mas_equalTo(CGSizeMake(PHOTOWIDTH, PHOTOWIDTH));
    }];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cleckImageViewAction)];
    [photoImageView addGestureRecognizer:tapGesture];
    
    self.renzhengImageView = [[UIImageView alloc] init];
    self.renzhengImageView.image = [UIImage imageNamed:@"bussiness_renzheng"];
    [topBgImageView addSubview:self.renzhengImageView];
    [self.renzhengImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(photoImageView);
        make.bottom.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(75, 21));
    }];
    self.renzhengImageView.hidden = YES;
    
    self.vipImageButton = [[UIButton alloc] init];
    [self.vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(photoImageView.mas_right).mas_offset(15);
        make.top.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 22];
    nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [topBgImageView addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.vipImageButton);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(self.vipImageButton.mas_right).mas_offset(2);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-10);
    }];

    f4wIDLabel = [[UILabel alloc] init];
    f4wIDLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    f4wIDLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.3);
    f4wIDLabel.textAlignment = NSTextAlignmentLeft;
    [topBgImageView addSubview:f4wIDLabel];
    [f4wIDLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(40);
        make.top.mas_equalTo(nameLabel.mas_bottom).mas_offset(8);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(15);
    }];
    
    mineButton = [[UIButton alloc] init];
    mineButton.layer.cornerRadius = 23/2;
    mineButton.layer.masksToBounds = YES;
    mineButton.backgroundColor = FWTextColor_68769F;
    mineButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    mineButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    [mineButton setTitle:@"   个人主页" forState:UIControlStateNormal];
    [mineButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [mineButton addTarget:self action:@selector(mineButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:mineButton];
    [mineButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(topBgImageView).mas_offset(32);
        make.size.mas_equalTo(CGSizeMake(101, 25));
        make.centerY.mas_equalTo(f4wIDLabel);
    }];
    
    UIImageView * arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"white_right"];
    [mineButton addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(mineButton);
        make.size.mas_equalTo(CGSizeMake(5, 10));
        make.centerX.mas_equalTo(mineButton).mas_offset(10);
    }];
    
    authenticationView = [[UIImageView alloc] init];
    authenticationView.image = [UIImage imageNamed:@"primary_bussiness"];
    [topBgImageView addSubview:authenticationView];
    [authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.left.mas_equalTo(f4wIDLabel).mas_offset(0);
        make.top.mas_equalTo(f4wIDLabel.mas_bottom).mas_offset(9);
    }];
    authenticationView.hidden = YES;
    
    authenticationLabel = [[UILabel alloc] init];
    authenticationLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    authenticationLabel.textAlignment = NSTextAlignmentLeft;
    authenticationLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.3);
    [topBgImageView addSubview:authenticationLabel];
    [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(15);
        make.centerY.mas_equalTo(authenticationView);
        make.left.mas_equalTo(authenticationView.mas_right).mas_offset(5);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-5);
    }];
    authenticationLabel.hidden = YES;
    
    effectButton = [[UIButton alloc] init];
    effectButton.layer.cornerRadius = 25/2;
    effectButton.layer.masksToBounds = YES;
    effectButton.backgroundColor = FWTextColor_68769F;
    [effectButton addTarget:self action:@selector(effectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topBgImageView addSubview:effectButton];
    [effectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(f4wIDLabel);
        make.height.mas_equalTo( 25);
        make.bottom.mas_equalTo(photoImageView);
        make.width.mas_greaterThanOrEqualTo (20);
    }];
    
    effectLabel = [[UILabel alloc] init];
    effectLabel.text = @"影响力";
    effectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:10];
    effectLabel.textColor = FWViewBackgroundColor_FFFFFF;
    effectLabel.textAlignment = NSTextAlignmentCenter;
    [effectButton addSubview:effectLabel];
    [effectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(effectButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(effectButton).mas_offset(30);
        make.right.mas_equalTo(effectButton).mas_offset(-15);
        make.centerY.mas_equalTo(effectButton);
    }];
    
    effectImageView = [[UIImageView alloc] init];
    effectImageView.image = [UIImage imageNamed:@"mine_effect"];
    [effectButton addSubview:effectImageView];
    [effectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(14, 14));
        make.centerY.mas_equalTo(effectButton);
        make.right.mas_equalTo(effectLabel.mas_left).mas_offset(-5);
    }];
    
    signLabel = [[UILabel alloc] init];
    signLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    signLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.3);;
    signLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:signLabel];
    [signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(15);
        make.left.mas_equalTo(photoImageView);
        make.width.mas_greaterThanOrEqualTo(100);
        make.right.mas_equalTo(self).mas_offset(-24);
        make.top.mas_equalTo(photoImageView.mas_bottom).mas_offset(26);
    }];
    
    
    lightView = [[UIView alloc] init];
    lightView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:lightView];
    [lightView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(24);
        make.width.mas_equalTo(SCREEN_WIDTH-48);
        make.height.mas_equalTo(73);
        make.top.mas_equalTo(self).mas_offset(231);
    }];
    lightView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    lightView.layer.borderColor = lightView.layer.shadowColor; // 边框颜色建议和阴影颜色一致
    lightView.layer.borderWidth = 0.000001; // 只要不为0就行
    lightView.layer.cornerRadius = 10;
    lightView.layer.shadowOpacity = 0.5;
    lightView.layer.shadowRadius = 10;
    lightView.layer.shadowOffset = CGSizeMake(0, 4);

    thumbUpButton = [[UIButton alloc] init];
    thumbUpButton.backgroundColor = [UIColor clearColor];
    thumbUpButton.contentEdgeInsets = UIEdgeInsetsMake(30,0, 0, 0);
    thumbUpButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    thumbUpButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [thumbUpButton setTitle:@"帖子" forState:UIControlStateNormal];
    [thumbUpButton setTitleColor:FWTextColor_919191 forState:UIControlStateNormal];
    [lightView addSubview:thumbUpButton];
    [thumbUpButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(lightView);
        make.width.mas_equalTo((SCREEN_WIDTH-48)/3);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(lightView);
    }];
    
    thumbUpLabel = [[UILabel alloc] init];
    thumbUpLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    thumbUpLabel.textColor = FWTextColor_272727;
    thumbUpLabel.textAlignment = NSTextAlignmentCenter;
    [thumbUpButton addSubview:thumbUpLabel];
    [thumbUpLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(thumbUpButton);
        make.top.mas_equalTo(thumbUpButton).mas_offset(21);
        make.left.right.mas_equalTo(thumbUpButton);
    }];
    
    followButton = [[UIButton alloc] init];
    followButton.backgroundColor = [UIColor clearColor];
    followButton.contentEdgeInsets = UIEdgeInsetsMake(30,0, 0, 0);
    followButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [followButton setTitle:@"关注" forState:UIControlStateNormal];
    [followButton setTitleColor:FWTextColor_919191 forState:UIControlStateNormal];
    [lightView addSubview:followButton];
    [followButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(thumbUpButton);
        make.width.mas_equalTo(thumbUpButton);
        make.left.mas_equalTo(thumbUpButton.mas_right);
        make.top.mas_equalTo(thumbUpButton);
    }];
    
    followLabel = [[UILabel alloc] init];
    followLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    followLabel.textColor = FWTextColor_272727;
    followLabel.textAlignment = NSTextAlignmentCenter;
    [followButton addSubview:followLabel];
    [followLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(followButton);
        make.top.mas_equalTo(followButton).mas_offset(21);
        make.left.right.mas_equalTo(followButton);
    }];
    
    
    funsButton = [[UIButton alloc] init];
    funsButton.backgroundColor = [UIColor clearColor];
    funsButton.contentEdgeInsets = UIEdgeInsetsMake(30,0, 0, 0);
    funsButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [funsButton setTitle:@"粉丝" forState:UIControlStateNormal];
    [funsButton setTitleColor:FWTextColor_919191 forState:UIControlStateNormal];
    [lightView addSubview:funsButton];
    [funsButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(followButton);
        make.width.mas_equalTo(followButton);
        make.left.mas_equalTo(followButton.mas_right);
        make.top.mas_equalTo(followButton);
    }];
    
    funsLabel = [[UILabel alloc] init];
    funsLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    funsLabel.textColor = FWTextColor_272727;
    funsLabel.textAlignment = NSTextAlignmentCenter;
    [funsButton addSubview:funsLabel];
    [funsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(funsButton);
        make.top.mas_equalTo(funsButton).mas_offset(21);
        make.left.right.mas_equalTo(funsButton);
    }];
    
    
    for ( int i = 0; i<2; i++) {
        UIView * lineView = [[UIView alloc] init];
        lineView.backgroundColor = FWColorWihtAlpha(@"E1E1E1", 1);
        [lightView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(lightView).mas_offset((i+1)*((SCREEN_WIDTH-40)/3));
            make.width.mas_equalTo(1);
            make.height.mas_equalTo(34);
            make.top.mas_equalTo(lightView).mas_offset(23);
        }];
    }
    
    activityImageView = [[UIImageView alloc] init];
    activityImageView.userInteractionEnabled = YES;
    activityImageView.layer.cornerRadius = 8;
    activityImageView.layer.masksToBounds = YES;
    activityImageView.contentMode = UIViewContentModeScaleAspectFill;
    activityImageView.frame = CGRectMake(24, 320, SCREEN_WIDTH-48, (SCREEN_WIDTH-48)*76/327);
    [self addSubview:activityImageView];
    activityImageView.hidden = YES;
    
    UITapGestureRecognizer * activityTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activityTapGesture:)];
    [activityImageView addGestureRecognizer:activityTap];
}

//- (void)setMineViewType:(FWMineViewType)mineViewType{
//    
//    _mineViewType = mineViewType;
//}

#pragma mark - > 初始化所有按钮的点击事件，并把控制器传进来
- (void)initClickMethodWithController:(UIViewController *)delegate{
    
    self.viewcontroller = delegate;
    
    [thumbUpButton addTarget:self action:@selector(thumbUpButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [followButton addTarget:self action:@selector(followButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [funsButton addTarget:self action:@selector(funsButtonClick) forControlEvents:UIControlEventTouchUpInside];
}

- (NSMutableAttributedString *)buttonAttributeWithTitle1:(NSString *)title1 WithTitle2:(NSString *)title2{
   
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",title1,title2]];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Semibold" size:12] range:NSMakeRange(0,title1.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Regular" size:11] range:NSMakeRange(title1.length,title2.length)];
    
    [str addAttribute:NSForegroundColorAttributeName value:FWViewBackgroundColor_FFFFFF range:NSMakeRange(0,title1.length)];
    [str addAttribute:NSForegroundColorAttributeName value:FWTextColor_969696 range:NSMakeRange(title1.length,title2.length)];
    
    return str;
}

- (void)configForView:(id)model{
    
    self.mineModel = (FWMineModel*)model;
    
    nameLabel.text = mineModel.user_info.nickname;
    signLabel.text = mineModel.user_info.autograph;
    f4wIDLabel.text = [NSString stringWithFormat:@"ID：%@",mineModel.user_info.f4w_id];
    effectLabel.text = [NSString stringWithFormat:@"影响力%@",mineModel.user_info.effect_value];
    thumbUpLabel.text = mineModel.feed_count;
    funsLabel.text = mineModel.fans_count;
    followLabel.text = mineModel.follow_count;
    
    if ([mineModel.user_info.cert_status isEqualToString:@"2"]){
        self.renzhengImageView.hidden = NO;
    }else{
        self.renzhengImageView.hidden = YES;
    }
    
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(photoImageView.mas_right).mas_offset(15);
        make.top.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(17, 17));
    }];
    
    CGFloat offset = 2;
    
    self.vipImageButton.enabled = YES;
    if ([mineModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([mineModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([mineModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];

        [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(photoImageView.mas_right).mas_offset(14);
            make.top.mas_equalTo(self.photoImageView);
            make.size.mas_equalTo(CGSizeMake(0.5, 17));
        }];
        offset = -3;
    }
    
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.vipImageButton);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(self.vipImageButton.mas_right).mas_offset(offset);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(topBgImageView).mas_offset(-10);
    }];
    
    if (mineModel.user_info.autograph.length <= 0) {
        signLabel.text = @"这个家伙很懒 什么都没留下~";
    }
    
    [photoImageView sd_setImageWithURL:[NSURL URLWithString:mineModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [activityImageView sd_setImageWithURL:[NSURL URLWithString:mineModel.banner_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    if (![mineModel.banner_url_show_status isEqualToString:@"2"]) {
        /* 不展示活动 */
        activityImageView.hidden = YES;
        self.currentHeight = 320;
    }else{
        /* 展示活动 */
        activityImageView.hidden = NO;
        self.currentHeight = 320+(SCREEN_WIDTH-48)*76/327;
    }
    
    if (mineModel.user_info.title.length > 0) {

        if ([mineModel.user_info.cert_status isEqualToString:@"2"]){
            authenticationView.hidden = NO;
            authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
            
            [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(15);
                make.centerY.mas_equalTo(authenticationView);
                make.left.mas_equalTo(authenticationView.mas_right).mas_offset(5);
                make.width.mas_greaterThanOrEqualTo(10);
                make.right.mas_equalTo(topBgImageView).mas_offset(-5);
            }];
        }else{
            authenticationView.hidden = YES;
            [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(15);
                make.centerY.mas_equalTo(authenticationView);
                make.left.mas_equalTo(f4wIDLabel);
                make.width.mas_greaterThanOrEqualTo(10);
                make.right.mas_equalTo(topBgImageView).mas_offset(-5);
            }];
        }
        authenticationLabel.hidden = NO;
        authenticationLabel.text = mineModel.user_info.title;
        
    }else{
        authenticationView.hidden = YES;
        authenticationLabel.hidden = YES;
    }
}

#pragma mark - > 活动
- (void)activityTapGesture:(UITapGestureRecognizer *)tap{
    
    FWInviteActivityViewController * AVC = [FWInviteActivityViewController new];
    [self.viewController.navigationController pushViewController:AVC animated:YES];
}

#pragma mark - > 影响力
- (void)effectButtonClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.mineModel.h5_jifen;
    [self.viewcontroller.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.viewcontroller.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 设置
- (void)settingButtonClick{
    
    FWSettingViewController * SVC = [[FWSettingViewController alloc] init];
    SVC.mineModel = self.mineModel;
    [self.viewcontroller.navigationController pushViewController:SVC animated:YES];
}

#pragma mark - > 点赞
- (void)thumbUpButtonClick{
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = mineModel.user_info.uid;
    [self.viewcontroller.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 关注
- (void)followButtonClick{
    
    FWFollowViewController * FVC = [[FWFollowViewController alloc] init];
//    if (self.mineViewType == FWMineType) {
//        FVC.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
//    }else{
//        FVC.base_uid = mineModel.user_info.uid;
//    }
    [self.viewcontroller.navigationController pushViewController:FVC animated:YES];
}

#pragma mark - > 粉丝
- (void)funsButtonClick{
    
    FWFunsViewController * FVC = [[FWFunsViewController alloc] init];
//    if (self.mineViewType == FWMineType) {
//        FVC.funsType = @"2";
//        FVC.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
//    }else{
//        FVC.funsType = @"3";
//        FVC.base_uid = mineModel.user_info.uid;
//    }
    [self.viewcontroller.navigationController pushViewController:FVC animated:YES];
}

#pragma mark - > 全部
- (void)videosButtonClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > 视频
- (void)picButtonClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > 草稿箱
- (void)draftButtonClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.delegate headerBtnClick:sender];
    }
}

#pragma mark - > 消息
- (void)messageButtonClick{
    
    FWMessageViewController * MVC = [[FWMessageViewController alloc] init];
    [self.viewcontroller.navigationController pushViewController:MVC animated:YES];
}

#pragma mark - > 个人主页
- (void)mineButtonClick{
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = mineModel.user_info.uid;
    [self.viewcontroller.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 查看大头像
- (void) cleckImageViewAction{
    
    FWChangePhotoViewController * vc = [[FWChangePhotoViewController alloc] init];
    vc.photoURL = mineModel.user_info.header_url_original;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.4];
    [animation setType: kCATransitionMoveIn];
    [animation setSubtype: kCATransitionFromTop];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    [self.viewcontroller.navigationController pushViewController:vc animated:NO];
    [self.viewcontroller.navigationController.view.layer addAnimation:animation forKey:nil];
}


- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewcontroller presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
