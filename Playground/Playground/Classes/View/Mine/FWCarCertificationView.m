//
//  FWCarCertificationView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarCertificationView.h"
#import "FWSelectCertificationCarViewController.h"
#import "FWPersonRankViewController.h"

@interface FWCarCertificationView ()
@property (strong, nonatomic) FWPersonalCertificationModel * cerModel;
@property (strong, nonatomic) NSString * car_style_id;
@end

@implementation FWCarCertificationView
@synthesize mainView;
@synthesize backButton;
@synthesize tipNameLabel;
@synthesize containerView;
@synthesize certificationButton;
@synthesize cerModel;
@synthesize car_style_id;
@synthesize selectLabel;

- (id)init{
    self = [super init];
    if (self) {
        car_style_id = @"";
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    CGFloat bottomMargin = 100+FWSafeBottom;
    
    mainView = [[UIScrollView alloc] init];
    mainView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    mainView.showsVerticalScrollIndicator = NO;
    mainView.showsHorizontalScrollIndicator = NO;
    mainView.delegate = self;
    [self addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.height.mas_equalTo(ScreenHeight -bottomMargin);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    
    tipNameLabel = [[UILabel alloc] init];
    tipNameLabel.text = @"车型名称";
    tipNameLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    tipNameLabel.textColor = FWTextColor_000000;
    tipNameLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:tipNameLabel];
    [tipNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(mainView).mas_offset(25);
        make.size.mas_equalTo(CGSizeMake(250, 20));
        make.left.mas_equalTo(mainView).mas_offset(31);
    }];
    
    selectLabel = [[UILabel alloc] init];
    selectLabel.text = @"请输入您的车型";
    selectLabel.userInteractionEnabled = YES;
    selectLabel.textColor = [UIColor colorWithHexString:@"c4c4c4" alpha:1];
    selectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:18];
    [mainView addSubview:selectLabel];
    [selectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(tipNameLabel).mas_offset(19);
        make.right.mas_equalTo(mainView).mas_offset(-40);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-80, 22));
        make.top.mas_equalTo(tipNameLabel.mas_bottom).mas_offset(25);
    }];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCarClick)];
    [selectLabel addGestureRecognizer:tap];
    
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWTextColor_363445;
    [mainView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(selectLabel);
        make.right.mas_equalTo(selectLabel);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(selectLabel.mas_bottom).mas_offset(10);
    }];
    
    containerView = [[UIView alloc] init];
    [mainView addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(lineView);
        make.right.mas_equalTo(lineView);
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(20);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_greaterThanOrEqualTo(20);
        make.bottom.mas_equalTo(mainView).mas_offset(-20);
    }];
    
    certificationButton = [[UIButton alloc] init];
    certificationButton.backgroundColor = FWTextColor_222222;
    [certificationButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    certificationButton.layer.cornerRadius = 2;
    certificationButton.layer.masksToBounds = YES;
    [certificationButton setTitle:@"保存" forState:UIControlStateNormal];
    certificationButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:16];
    [certificationButton addTarget:self action:@selector(certificationButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:certificationButton];
    [certificationButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(44);
        make.centerX.mas_equalTo(self);
        make.size.mas_offset(CGSizeMake(295, 48));
        make.top.mas_equalTo(self).mas_offset(-bottomMargin+44);
    }];
}

#pragma mark - > 选择车型
- (void)selectCarClick{
    
    FWSelectCertificationCarViewController * VC = [[FWSelectCertificationCarViewController alloc] init];
    VC.myBlock = ^(FWPersonalCertificationListModel * _Nonnull carModel) {
        selectLabel.text = carModel.name;
        car_style_id = carModel.car_style_id;
        selectLabel.textColor = [UIColor colorWithHexString:@"000000" alpha:1];
    };
    [self.viewcontroller.navigationController pushViewController:VC animated:YES];
}

#pragma mark - > 配置热门车型
- (void)configViewForModel:(id)model{
    
    cerModel = (FWPersonalCertificationModel *)model;
    
    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    if (usvm.userModel.car_style.length > 0) {
        selectLabel.text = usvm.userModel.car_style;
        selectLabel.textColor = [UIColor colorWithHexString:@"000000" alpha:1];
    }
    
    NSInteger count = cerModel.list.count;
    
    if (count > 0 ) {
        for (UIView * view in containerView.subviews) {
            [view removeFromSuperview];
        }
        
        NSInteger adapter = 3;
        
        for (int i = 0; i < count; i++) {
            
            FWPersonalCertificationListModel * listModel = cerModel.list[i];
            listModel.isSelect = NO;
            
            NSString *buttonTitle = [NSString stringWithFormat:@"%@", listModel.name];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setSelected:NO];
            button.tag = 2000+i;
            button.layer.cornerRadius = 15;
            button.layer.masksToBounds = YES;
            button.backgroundColor = FWTextColor_66739A;
            button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
            [button setTitle:buttonTitle forState:UIControlStateNormal];
            [button setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
            [containerView addSubview:button];
            
            NSInteger line = i / adapter;
            NSInteger row = i % adapter;
            
            NSInteger buttonWidth = (SCREEN_WIDTH - 80 -15)/3;
            NSInteger buttonHeight = 30;
            
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(containerView).mas_offset(line * 40 + 10);
                make.left.mas_equalTo(containerView).mas_offset((buttonWidth + 5) * row);
                make.height.mas_offset(buttonHeight);
                make.width.mas_equalTo(buttonWidth);
                if (i == count -1) {
                    make.bottom.mas_equalTo(containerView).mas_offset(-20);
                }
            }];
        }
    }
    
    [certificationButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(44);
        make.left.mas_equalTo(self).mas_offset(40);
        make.right.mas_equalTo(self).mas_offset(-40);
        make.bottom.mas_equalTo(self).mas_offset(-40-FWSafeBottom);
    }];
}

#pragma mark - > 选中车型
- (void)buttonClickEvent:(UIButton*)sender{
    
    NSInteger index = sender.tag - 2000;
    
    [sender setSelected:!sender.selected];
    
    for (int i = 0; i<cerModel.list.count; i++) {
        
        FWPersonalCertificationListModel * listModel = cerModel.list[i];
        UIButton * button = containerView.subviews[i];
        
        if (index == i) {
            //            listModel.isSelect = sender.selected;
            //            if (listModel.isSelect) {
            
            car_style_id = listModel.car_style_id;
            selectLabel.text = listModel.name;
            selectLabel.textColor = [UIColor colorWithHexString:@"000000" alpha:1];
            //            }else{
            //                car_style_id = @"";
            //                selectLabel.text = @"请输入您的车型";
            //            }
        }else{
            listModel.isSelect = NO;
        }
        
        button.selected = listModel.isSelect;
        //        [self selectButtonType:listModel.isSelect WithButton:button];
    }
}

- (void)selectButtonType:(BOOL)isSelect WithButton:(UIButton *)button{
    
    if (isSelect) {
        
        button.layer.borderColor = FWClearColor.CGColor;
        button.layer.borderWidth = 0.0f;
        button.backgroundColor = FWOrange;
        [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    }else{
        
        button.layer.borderColor = FWTextColor_525467.CGColor;
        button.layer.borderWidth = 1.0f;
        button.backgroundColor = FWClearColor;
        [button setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    }
}
#pragma mark - > 返回
- (void)backButtonClick{
    
    [self.viewcontroller.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 车手认证
- (void)certificationButtonClick{
    
    if (car_style_id.length <=0 ) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写您的车型" toController:self.viewcontroller];
        return;
    }
    
    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"car_style_id":car_style_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_identify_normal WithDelegate:self.viewcontroller completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        NSDictionary * data = [back objectForKey:@"data"];
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if([[data objectForKey:@"desc"] containsString:@"30"]){
               
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:[data objectForKey:@"title"] message:[data objectForKey:@"desc"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"去看看" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    FWPersonRankViewController * WVC = [[FWPersonRankViewController alloc] init];
                    [self.viewcontroller.navigationController pushViewController:WVC animated:YES];
                }];
                [alertController addAction:okAction];
                [self.viewcontroller presentViewController:alertController animated:YES completion:nil];
                
            }else{
                [[FWHudManager sharedManager] showErrorMessage:@"座驾修改成功" toController:self.viewcontroller];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.viewcontroller.navigationController popViewControllerAnimated:YES];
                });
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewcontroller];
        }
    }];
}


@end
