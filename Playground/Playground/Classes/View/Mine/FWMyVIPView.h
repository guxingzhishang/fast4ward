//
//  FWMyVIPView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMyVIPView : UIView


@property (nonatomic, strong) UIImageView * topImageView;

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton * backButton;

@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIImageView * verImageView;

@property (nonatomic, strong) UILabel * levelLabel;
@property (nonatomic, strong) UIButton * vipImageButton;

@property (nonatomic, strong) UIImageView * equityImageView;

@property (nonatomic, strong) UIButton * equityButton;
@property (nonatomic, strong) UIButton * ticketButton;
@property (nonatomic, strong) UIButton * couponButton;

@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UIImageView * qrcodeImageView;

@property (nonatomic, weak) UIViewController * vc;


- (void)configViewWithModel:(id)model;


@end

NS_ASSUME_NONNULL_END
