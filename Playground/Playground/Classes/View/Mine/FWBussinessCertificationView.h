//
//  FWBussinessCertificationView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 商家认证视图
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWBussinessCertificationViewDelegate <NSObject>

/* 去认证 */
- (void)certificationClick;

@end

@interface FWBussinessCertificationView : UIView<UITextFieldDelegate,UIScrollViewDelegate>

/* 主视图 */
@property (nonatomic, strong) UIScrollView * mainView;

/* 返回按钮 */
@property (nonatomic, strong) UIButton * backButton;

/* 认证名称提示 */
@property (nonatomic, strong) UILabel * tipNameLabel;

/* 认证输入框 */
@property (nonatomic, strong) UITextField * certificationTF;

/* 认证名称提示 */
@property (nonatomic, strong) UILabel * tipLabel;

/* 承载业务多选视图 */
@property (nonatomic, strong) UIView * containerView;

/* 认证按钮 */
@property (nonatomic, strong) UIButton * certificationButton;

@property (nonatomic, weak) UIViewController * viewcontroller;

- (void)configViewForModel:(id)model;

@end

NS_ASSUME_NONNULL_END
