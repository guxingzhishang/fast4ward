//
//  FWThumbUpView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWThumbUpView.h"

@implementation FWThumbUpView
@synthesize mainView;
@synthesize thumbupView;
@synthesize contentLabel;
@synthesize sureButton;
@synthesize cardView;
@synthesize isShow;
@synthesize lineView;

+(FWThumbUpView*)defaultShareView{
    
    static FWThumbUpView * defaultThumbUpView = nil;
    
    if (!defaultThumbUpView){
        defaultThumbUpView = [[FWThumbUpView alloc] init];
    }
    
    return defaultThumbUpView;
}

- (id)init{
    self = [super init];
    
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void) setupSubviews{
    [self clearSubview];
    
    if (!mainView){
        mainView = [[UIView alloc] init];
    }
    mainView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6] ;
    mainView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [[UIApplication sharedApplication].keyWindow addSubview:mainView];
    
    if (!cardView) {
        cardView = [[UIImageView alloc] init];
    }
    cardView.userInteractionEnabled = YES;
    cardView.backgroundColor = DHTitleColor_FFFFFF;
    cardView.layer.cornerRadius = 2;
    cardView.layer.masksToBounds = YES;
    [mainView addSubview:cardView];
    [cardView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView).mas_offset(50);
        make.width.mas_equalTo(SCREEN_WIDTH-100);
        make.height.mas_equalTo(150);
        make.centerY.mas_equalTo(mainView);
    }];
    
    if (!thumbupView) {
        thumbupView = [[UIImageView alloc] init];
    }
    thumbupView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [mainView addSubview:thumbupView];
    [thumbupView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cardView).mas_offset(-25);
        make.size.mas_equalTo(CGSizeMake(90, 50));
        make.centerX.mas_equalTo(cardView);
    }];
    
    if (!contentLabel) {
        contentLabel = [[UILabel alloc] init];
    }
    contentLabel.text = @"";
    contentLabel.numberOfLines = 2;
    contentLabel.font = DHSystemFontOfSize_14;
    contentLabel.textColor = DHTextHexColor_Dark_222222;
    contentLabel.textAlignment = NSTextAlignmentCenter;
    [cardView addSubview:contentLabel];
    [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cardView).mas_offset(20);
        make.right.mas_equalTo(cardView).mas_offset(-20);
        make.top.mas_equalTo(cardView).mas_offset(50);
        make.height.mas_greaterThanOrEqualTo(20);
    }];
    
    if (!lineView) {
        lineView = [[UIView alloc] init];
    }
    lineView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [cardView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(cardView);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(cardView.mas_bottom).mas_offset(-45);
    }];
    
    if (!sureButton) {
        sureButton = [[UIButton alloc] init];
    }
    sureButton.titleLabel.font = DHSystemFontOfSize_16;
    sureButton.backgroundColor = FWClearColor;
    [sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [sureButton setTitleColor:DHTextHexColor_Dark_222222 forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(sureButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [cardView addSubview:sureButton];
    [sureButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(cardView);
        make.height.mas_equalTo(44);
    }];
}

#pragma mark - > 关闭弹窗
- (void)sureButtonClick{
    [self hideView];
}

- (void)showView{
    isShow = YES;
    mainView.hidden = NO;
}

-(void)hideView{
    isShow = NO;
    mainView.hidden = YES;
}

// 清除视图块子视图
-(void)clearSubview{
    
    if (mainView){
        for (UIView * view in mainView.subviews){
            [view removeFromSuperview];
        }
    }
    
    if (cardView){
        for (UIView * view in cardView.subviews){
            [view removeFromSuperview];
        }
    }
}

@end
