//
//  FWMySignupCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/8.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMySignupCell.h"
#import "FWBaomingQrcodeViewController.h"

@implementation FWMySignupCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 10;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.6;
    self.shadowView.layer.shadowColor = FWColor(@"D9D9D9").CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(7);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(188);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-7);
    }];
    
    self.bgView = [[UIView alloc] init];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.shadowView).mas_offset(2);
        make.left.right.bottom.mas_equalTo(self.shadowView);
    }];
    
    self.qrcodeImageView = [[UIImageView alloc] init];
    self.qrcodeImageView.image = [UIImage imageNamed:@"baoming_qrcode"];
    self.qrcodeImageView.userInteractionEnabled = YES;
    [self.bgView addSubview:self.qrcodeImageView];
    [self.qrcodeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(26, 26));
        make.right.mas_equalTo(self.bgView).mas_offset(-13);
        make.top.mas_equalTo(self.bgView).mas_offset(13);
    }];
    [self.qrcodeImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(qrcodeImageClick)]];

    
    self.statusLabel = [[UILabel alloc] init];
    self.statusLabel.layer.cornerRadius = 2;
    self.statusLabel.layer.masksToBounds = YES;
    self.statusLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.statusLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.statusLabel];
    [self.statusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.bgView).mas_offset(16);
        make.width.mas_equalTo(64);
        make.height.mas_equalTo(18);
    }];
    
    self.perfectLabel = [[UILabel alloc] init];
    self.perfectLabel.layer.cornerRadius = 2;
    self.perfectLabel.layer.masksToBounds = YES;
    self.perfectLabel.text = @"请完善信息";
    self.perfectLabel.backgroundColor = FWColor(@"ff6f00");;
    self.perfectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.perfectLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.perfectLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.perfectLabel];
    [self.perfectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.statusLabel);
        make.left.mas_equalTo(self.statusLabel.mas_right).mas_offset(6);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.titleLabel.textColor = FWTextColor_272727;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.statusLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.statusLabel);
        make.right.mas_equalTo(self.bgView).mas_offset(-16);
        make.width.mas_greaterThanOrEqualTo(80);
        make.height.mas_greaterThanOrEqualTo(22);
    }];
    
    self.timeImageView = [[UIImageView alloc] init];
    self.timeImageView.image = [UIImage imageNamed:@"registration_time"];
    [self.bgView addSubview:self.timeImageView];
    [self.timeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(14, 14));
        make.left.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(19);
    }];
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.timeLabel.textColor = FWTextColor_BCBCBC;
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.timeImageView);
        make.left.mas_equalTo(self.timeImageView.mas_right).mas_offset(7);
        make.right.mas_equalTo(self.bgView).mas_offset(-16);
        make.width.mas_greaterThanOrEqualTo(80);
        make.height.mas_equalTo(20);
    }];
    
    self.areaImageView = [[UIImageView alloc] init];
    self.areaImageView.image = [UIImage imageNamed:@"registration_area"];
    [self.bgView addSubview:self.areaImageView];
    [self.areaImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(13, 16));
        make.left.mas_equalTo(self.titleLabel);
        make.top.mas_equalTo(self.timeImageView.mas_bottom).mas_offset(10);
    }];
    
    self.areaLabel = [[UILabel alloc] init];
    self.areaLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.areaLabel.textColor = FWTextColor_BCBCBC;
    self.areaLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.areaLabel];
    [self.areaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.areaImageView);
        make.left.mas_equalTo(self.areaImageView.mas_right).mas_offset(7);
        make.right.mas_equalTo(self.bgView).mas_offset(-16);
        make.width.mas_greaterThanOrEqualTo(80);
        make.height.mas_equalTo(20);
    }];
    
    self.myCardButton = [[UIButton alloc] init];
    self.myCardButton.layer.cornerRadius = 2;
    self.myCardButton.layer.masksToBounds = YES;
    self.myCardButton.layer.borderWidth = 1;
    self.myCardButton.layer.borderColor = FWTextColor_222222.CGColor;
    self.myCardButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.myCardButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.myCardButton setTitle:@"我的车手卡" forState:UIControlStateNormal];
    [self.myCardButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.myCardButton addTarget:self action:@selector(myCardButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.myCardButton];
    [self.myCardButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.areaLabel.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.bgView).mas_offset(31);
        make.height.mas_equalTo(31);
        make.width.mas_equalTo(((SCREEN_WIDTH-28)-110)/2);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-20);
    }];
    
    self.perfectInfoButton = [[UIButton alloc] init];
    self.perfectInfoButton.layer.cornerRadius = 2;
    self.perfectInfoButton.layer.masksToBounds = YES;
    self.perfectInfoButton.backgroundColor = FWTextColor_222222;
    self.perfectInfoButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.perfectInfoButton setTitle:@"完善信息" forState:UIControlStateNormal];
    [self.perfectInfoButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.perfectInfoButton addTarget:self action:@selector(perfectInfoButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.perfectInfoButton];
    [self.perfectInfoButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.myCardButton);
        make.right.mas_equalTo(self.bgView).mas_offset(-31);
        make.height.mas_equalTo(self.myCardButton);
        make.width.mas_equalTo(self.myCardButton);
    }];
    
    self.continueButton = [[UIButton alloc] init];
    self.continueButton.layer.cornerRadius = 2;
    self.continueButton.layer.masksToBounds = YES;
    self.continueButton.backgroundColor = FWTextColor_222222;
    self.continueButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [self.continueButton setTitle:@"继续缴费" forState:UIControlStateNormal];
    [self.continueButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.continueButton addTarget:self action:@selector(continueButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.continueButton];
    [self.continueButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.areaLabel.mas_bottom).mas_offset(20);
        make.centerX.mas_equalTo(self.bgView);
        make.height.mas_equalTo(31);
        make.width.mas_equalTo(157);
        make.bottom.mas_greaterThanOrEqualTo(self.bgView).mas_offset(-20);
    }];
    
    self.myCardButton.hidden = YES;
    self.perfectInfoButton.hidden = YES;
    self.continueButton.hidden =YES;
    
    self.perfectLabel.hidden = YES;
    self.shadowView.hidden = YES;
}

- (void)configForCell:(id)model{
    
    self.homeModel = (FWRegistrationHomeModel *)model;

    self.titleLabel.text = self.homeModel.sport_name;
    self.timeLabel.text = self.homeModel.sport_date;
    self.areaLabel.text = self.homeModel.sport_area;

    if ([self.homeModel.pay_status isEqualToString:@"1"]){
        /* 未缴费 */
        self.statusLabel.backgroundColor = FWColor(@"ff6f00");
        self.statusLabel.text = @"未缴费";

        self.shadowView.hidden = NO;

        self.continueButton.hidden = NO;
        self.myCardButton.hidden = YES;
        self.perfectInfoButton.hidden = YES;
    }else if ([self.homeModel.pay_status isEqualToString:@"2"]){
        /* 报名成功 */

        self.shadowView.hidden = NO;

        self.statusLabel.backgroundColor = FWColor(@"#1FB62B");
        self.statusLabel.text = @"报名成功";
        self.continueButton.hidden = YES;
        self.myCardButton.hidden = NO;
        self.perfectInfoButton.hidden = NO;

        [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView).mas_offset(7);
            make.left.mas_equalTo(self.contentView).mas_offset(14);
            make.right.mas_equalTo(self.contentView).mas_offset(-14);
            make.height.mas_greaterThanOrEqualTo(188);
            make.width.mas_equalTo(SCREEN_WIDTH-28);
            make.bottom.mas_equalTo(self.contentView).mas_offset(-7);
        }];

        [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.shadowView).mas_offset(2);
            make.left.right.bottom.mas_equalTo(self.shadowView);
        }];

        if ([self.homeModel.perfect_info_status isEqualToString:@"1"]) {
            /* 显示完善信息 */
            self.perfectLabel.hidden = NO;
            [self.perfectInfoButton setTitle:@"完善信息" forState:UIControlStateNormal];
            [self.perfectInfoButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self.myCardButton);
                make.right.mas_equalTo(self.bgView).mas_offset(-31);
                make.height.mas_equalTo(self.myCardButton);
                make.width.mas_equalTo(self.myCardButton);
            }];
        }else if([self.homeModel.perfect_info_status isEqualToString:@"2"]){
            /* 显示查看完善信息 */
            self.perfectLabel.hidden = YES;
            [self.perfectInfoButton setTitle:@"我的报名信息" forState:UIControlStateNormal];
            [self.perfectInfoButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self.myCardButton);
                make.right.mas_equalTo(self.bgView).mas_offset(-31);
                make.height.mas_equalTo(self.myCardButton);
                make.width.mas_equalTo((SCREEN_WIDTH-28-110)/2 + 30);
            }];
        }else if([self.homeModel.perfect_info_status isEqualToString:@"3"]){
            /* 修改完善信息 */
            self.perfectLabel.hidden = YES;
            [self.perfectInfoButton setTitle:@"修改报名信息" forState:UIControlStateNormal];
            [self.perfectInfoButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(self.myCardButton);
                make.right.mas_equalTo(self.bgView).mas_offset(-31);
                make.height.mas_equalTo(self.myCardButton);
                make.width.mas_equalTo((SCREEN_WIDTH-28-110)/2 + 30);
            }];
        }
    }
}

#pragma mark - > 我的车手卡
- (void)myCardButtonClick{

    FWPlayerShareViewController * vc = [[FWPlayerShareViewController alloc] init];
    vc.driver_info = self.homeModel.driver_card;
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.vc.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.vc presentViewController:vc animated:YES completion:nil];
}

#pragma mark - > 完善信息
- (void)perfectInfoButtonClick{

    FWPerfectInfomationViewController * PIVC = [[FWPerfectInfomationViewController alloc] init];
    PIVC.baoming_user_id = self.homeModel.baoming_user_id;
    if ([self.homeModel.perfect_info_status isEqualToString:@"1"] ||
        [self.homeModel.perfect_info_status isEqualToString:@"3"] ) {
        PIVC.canEdited = YES;
    }else{
        PIVC.canEdited = NO;
    }
    [self.vc.navigationController pushViewController:PIVC animated:YES];
}

#pragma mark - > 继续缴费
- (void)continueButtonClick{
    
    FWPlayerSignupViewController * PSVC = [[FWPlayerSignupViewController alloc] init];
    PSVC.baoming_user_id = self.homeModel.baoming_user_id;
    PSVC.isShowAlert = NO;
    [self.vc.navigationController pushViewController:PSVC animated:YES];
}

#pragma mark - > 展示二维码
- (void)qrcodeImageClick{
    
    FWBaomingQrcodeViewController * BQVC = [[FWBaomingQrcodeViewController alloc] init];
    BQVC.qrcode = self.homeModel.qrcode_url;
    BQVC.name = self.homeModel.sport_name;
    BQVC.titleString = @"签到/检录二维码";
    BQVC.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    BQVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.vc.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.vc presentViewController:BQVC animated:YES completion:nil];
}
@end
