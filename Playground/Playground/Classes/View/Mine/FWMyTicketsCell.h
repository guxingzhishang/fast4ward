//
//  FWMyTicketsCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMyTicketsCell : UITableViewCell

/* 未领取时的承载视图 */
@property (nonatomic, strong) UIImageView * unReceiveView;
@property (nonatomic, strong) UILabel * unReceiveNameLabel;
@property (nonatomic, strong) UIButton * reeceiveButton;

/* 领取后的承载视图 */
@property (nonatomic, strong) UIImageView * receiveView;
@property (nonatomic, strong) UILabel * receiveNameLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * codeLabel;
@property (nonatomic, strong) UILabel * stateLabel;
@property (nonatomic, strong) UIButton * cpButton;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWTicketsModel * ticketsModel;

- (void)configForCellWithModel:(FWTicketsListModel *)model;

@end

NS_ASSUME_NONNULL_END
