//
//  FWThumbUpView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 点赞弹窗样式
 */

#import <UIKit/UIKit.h>

@interface FWThumbUpView : UIView

/**
 * 背景图
 */
@property (nonatomic, strong) UIView * mainView;

/**
 * 点赞图标
 */
@property (nonatomic, strong) UIImageView * thumbupView;

/**
 * 框
 */
@property (nonatomic, strong) UIImageView * cardView;

/**
 * 内容
 */
@property (nonatomic, strong) UILabel * contentLabel;

/**
 * 内容
 */
@property (nonatomic, strong) UIView * lineView;

/**
 * 确定按钮
 */
@property (nonatomic, strong) UIButton * sureButton;

/**
 * 是否显示
 */
@property (nonatomic,assign) BOOL isShow;

+ (FWThumbUpView *)defaultShareView;

- (void) showView;

- (void) hideView;

- (void) setupSubviews;

@end
