//
//  FWSettingCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWSettingCell.h"

@implementation FWSettingCell
@synthesize arrowImageView;
@synthesize titleLabel;
@synthesize rightLabel;
@synthesize lineView;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    if (!titleLabel) {
        titleLabel = [[UILabel alloc] init];
    }
    titleLabel.font = DHSystemFontOfSize_15;
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(15);
        make.centerY.mas_equalTo(self.contentView);
        make.height.mas_equalTo(25);
    }];
    
    if (!arrowImageView) {
        arrowImageView = [[UIImageView alloc] init];
    }
    arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.contentView addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(6, 11));
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    if (!rightLabel) {
        rightLabel = [[UILabel alloc] init];
    }
    rightLabel.font = DHSystemFontOfSize_14;
    rightLabel.textColor = [UIColor lightGrayColor];
    rightLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:rightLabel];
    [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-15);
        make.centerY.mas_equalTo(arrowImageView);
        make.height.mas_equalTo(20);
    }];
    
    
    if (!lineView) {
        lineView = [[UIView alloc] init];
    }
    lineView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.contentView addSubview:lineView];
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(15);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-0.5);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 0.5));
    }];
    
}

@end
