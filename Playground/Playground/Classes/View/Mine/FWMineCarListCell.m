//
//  FWMineCarListCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMineCarListCell.h"

@implementation FWMineCarListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.carImageView = [[UIImageView alloc] init];
    self.carImageView.layer.cornerRadius = 5;
    self.carImageView.layer.masksToBounds = YES;
    self.carImageView.image = [UIImage imageNamed:@"placeholder"];
    self.carImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.carImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.carImageView];
    [self.carImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 153));
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    self.bgView = [[UIView alloc] init];
    self.bgView.frame = CGRectMake(0, 53, SCREEN_WIDTH-28, 100);
    [self.carImageView addSubview:self.bgView];
    
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bgView.bounds;
    [self.bgView.layer addSublayer:gradientLayer];
    
    gradientLayer.colors = @[
                             (__bridge id)[UIColor colorWithHexString:@"ffffff" alpha:0.0].CGColor,
                             (__bridge id)[UIColor colorWithHexString:@"000000" alpha:0.7].CGColor,
                             ];
    
    gradientLayer.startPoint = CGPointMake(0.0, 0.0);
    gradientLayer.endPoint = CGPointMake(0.0, 1.0);

    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(14);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-11);
        make.width.mas_equalTo(SCREEN_WIDTH-28-28);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.arrowView = [[UIImageView alloc] init];
    self.arrowView.image = [UIImage imageNamed:@"car_arrow"];
    [self.bgView addSubview:self.arrowView];
    [self.arrowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.centerY.mas_equalTo(self.titleLabel);
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(24);
    }];
}

- (void)configForCell:(id)model{
    
    FWCarListModel * listModel = (FWCarListModel *)model;
    
    self.titleLabel.text = listModel.car_style_name;
    [self.carImageView sd_setImageWithURL:[NSURL URLWithString:listModel.car_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}


@end
