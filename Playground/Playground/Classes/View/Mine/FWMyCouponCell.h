//
//  FWMyCouponCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMyCouponCell : UITableViewCell

/* 未领取时的承载视图 */
@property (nonatomic, strong) UIImageView * unReceiveView;
@property (nonatomic, strong) UILabel * unReceivePriceLabel;
@property (nonatomic, strong) UILabel * unConditionLabel;
@property (nonatomic, strong) UIButton * reeceiveButton;

/* 领取后的承载视图 */
@property (nonatomic, strong) UIImageView * receiveView;
@property (nonatomic, strong) UILabel * receivePriceLabel;
@property (nonatomic, strong) UILabel * conditionLabel;
@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) UIButton * cpButton;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWCouponModel * couponModel;

- (void)configForCellWithModel:(FWCouponListModel *)model;
@end

NS_ASSUME_NONNULL_END
