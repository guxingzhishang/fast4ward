//
//  FWVisitorNameListHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWVisitorNameListHeaderView : UIView

@property (nonatomic, strong) UILabel * todayVisivorLabel;
@property (nonatomic, strong) UILabel * totalVisivorLabel;
@property (nonatomic, strong) UIButton * tipButton;
@property (nonatomic, weak) UIViewController * vc;


@end

NS_ASSUME_NONNULL_END
