//
//  FWCarCertificationCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMineCarListCell.h"

@class FWCarCertificationCell;

@protocol FWCarCertificationCellDelegate <NSObject>

- (void)deleteButtonOnClick:(NSInteger)index WithCell:(FWCarCertificationCell *)cell;

@end

NS_ASSUME_NONNULL_BEGIN

@interface FWCarCertificationCell : FWMineCarListCell

@property (nonatomic, strong) UIView * mengcengView;
@property (nonatomic, strong) UIButton * deleteButton;
@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) id<FWCarCertificationCellDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
