//
//  FWSelectCertificationCarCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWSelectCertificationCarCell.h"

@implementation FWSelectCertificationCarCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.photoImageView.image = [UIImage imageNamed:@""];
        self.photoImageView.layer.cornerRadius = 0;
        self.photoImageView.layer.borderColor = FWClearColor.CGColor;
        self.photoImageView.layer.masksToBounds = NO;
        
        self.nameLabel.textColor = FWTextColor_12101D;
        
        [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.centerY.mas_equalTo(self.contentView);
        }];
        
        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_greaterThanOrEqualTo(70);
            make.height.mas_equalTo(self);
            make.left.mas_equalTo(self.contentView).mas_offset(20);
//            make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
            make.top.mas_equalTo(self.contentView);
        }];
        
        [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView).mas_offset(0);
            make.left.mas_equalTo(self.contentView).mas_offset(0);
            make.height.mas_equalTo(0.5);
            make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-1);
        }];
    }
    
    return self;
}

- (void)cellConfigureFor:(id)model{
    
    FWPersonalCertificationListModel * listModel = (FWPersonalCertificationListModel *)model;
    
    self.nameLabel.text = listModel.name;
}
@end
