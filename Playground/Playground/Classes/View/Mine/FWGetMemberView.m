//
//  FWGetMemberView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGetMemberView.h"

@implementation FWGetMemberView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.layer.cornerRadius = 2;
    self.iconImageView.layer.masksToBounds = YES;
    self.iconImageView.image = [UIImage imageNamed:@"vip_icon"];
    [self addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(self).mas_offset(20);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = DHSystemFontOfSize_18;
    self.titleLabel.textColor = FWTextColor_12101D;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconImageView).mas_offset(10);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(120);
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(10);
    }];
    
    self.priceLabel = [UILabel new];
    self.priceLabel.font = DHSystemFontOfSize_13;
    self.priceLabel.textColor = FWTextColor_12101D;
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(120);
        make.left.mas_equalTo(self.titleLabel);
    }];
    
    UIView * lineOneView = [[UIView alloc] init];
    lineOneView.backgroundColor = FWTextColor_F6F8FA;
    [self addSubview:lineOneView];
    [lineOneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(10);
        make.top.mas_equalTo(self.iconImageView.mas_bottom).mas_offset(13);
    }];
    
    UILabel * tipNameLabel = [[UILabel alloc] init];
    tipNameLabel.text = @"联系人";
    tipNameLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    tipNameLabel.textColor = FWTextColor_12101D;
    tipNameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:tipNameLabel];
    [tipNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineOneView.mas_bottom).mas_offset(17);
        make.size.mas_equalTo(CGSizeMake(250, 20));
        make.left.mas_equalTo(self.iconImageView);
    }];
    
    self.nameField = [[UITextField alloc] init];
    self.nameField.delegate = self;
    self.nameField.placeholder = @"请输入联系人";
    self.nameField.textColor = FWTextColor_12101D;
    self.nameField.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:18];
    self.nameField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self addSubview:self.nameField];
    [self.nameField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(tipNameLabel);
        make.right.mas_equalTo(self).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-40, 22));
        make.top.mas_equalTo(tipNameLabel.mas_bottom).mas_offset(25);
    }];
    
    
    UIView * lineView1 = [[UIView alloc] init];
    lineView1.backgroundColor = FWTextColor_12101D;
    [self addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.nameField);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.nameField.mas_bottom).mas_offset(5);
    }];
    
    UILabel * phoenTipLabel = [[UILabel alloc] init];
    phoenTipLabel.text = @"手机号码";
    phoenTipLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    phoenTipLabel.textColor = FWTextColor_12101D;
    phoenTipLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:phoenTipLabel];
    [phoenTipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameField.mas_bottom).mas_offset(20);
        make.size.mas_equalTo(CGSizeMake(250, 20));
        make.left.mas_equalTo(self.nameField);
    }];
    
    self.phoneField = [[UITextField alloc] init];
    self.phoneField.delegate = self;
    self.phoneField.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneField.placeholder = @"请输入手机号码";
    self.phoneField.textColor = FWTextColor_12101D;
    self.phoneField.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:18];
    self.phoneField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self addSubview:self.phoneField];
    [self.phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoenTipLabel);
        make.right.mas_equalTo(self).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-40, 22));
        make.top.mas_equalTo(phoenTipLabel.mas_bottom).mas_offset(25);
    }];
    
    UIView * lineView2 = [[UIView alloc] init];
    lineView2.backgroundColor = FWTextColor_12101D;
    [self addSubview:lineView2];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.phoneField);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.phoneField.mas_bottom).mas_offset(5);
    }];
    
    UIView * lineTwoView = [[UIView alloc] init];
    lineTwoView.backgroundColor = FWTextColor_F6F8FA;
    [self addSubview:lineTwoView];
    [lineTwoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(10);
        make.top.mas_equalTo(lineView2.mas_bottom).mas_offset(21);
    }];
    
    UILabel * chooseLabel = [[UILabel alloc] init];
    chooseLabel.text = @"请选择支付方式";
    chooseLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    chooseLabel.textColor = FWTextColor_12101D;
    chooseLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:chooseLabel];
    [chooseLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineTwoView.mas_bottom).mas_offset(21);
        make.size.mas_equalTo(CGSizeMake(250, 20));
        make.left.mas_equalTo(self.nameField);
    }];
    
    self.payView = [UIView new];
    [self addSubview:self.payView];
    [self.payView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(chooseLabel.mas_bottom).mas_equalTo(20);
        make.height.mas_greaterThanOrEqualTo(50);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    
    [self setupPaySubViews];

    UIView * bottomBgView = [[UIView alloc] init];
    bottomBgView.backgroundColor = FWTextColor_F6F8FA;
    [self addSubview:bottomBgView];
    [bottomBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(200);
        make.top.mas_equalTo(self.payView.mas_bottom);
    }];
    
    self.payButton = [[UIButton alloc] init];
    self.payButton.layer.cornerRadius = 2;
    self.payButton.layer.masksToBounds = YES;
    self.payButton.backgroundColor = FWTextColor_222222;
    self.payButton.titleLabel.font = DHSystemFontOfSize_14;
    [self.payButton setTitle:@"去支付" forState:UIControlStateNormal];
    [self.payButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.payButton addTarget:self action:@selector(payButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.payButton];
    [self.payButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-60, 50));
        make.top.mas_equalTo(self.payView.mas_bottom).mas_offset(21);
        make.left.mas_equalTo(self).mas_offset(30);
    }];
    
    [self sendSubviewToBack:bottomBgView];
}

-(void)setMemberListModel:(FWMemberListModel *)memberListModel{
    _memberListModel = memberListModel;
    
    self.titleLabel.text = self.memberListModel.product_name;
    self.priceLabel.text = [NSString stringWithFormat:@"%@元/年",self.memberListModel.price_yuan];
}


-(void)settingInfo:(FWMemberInfoModel *)infoModel{
    
    if (infoModel.real_name && infoModel.real_name.length > 0) {
        self.nameField.text = infoModel.real_name;
    }
    
    if (infoModel.mobile && infoModel.mobile.length > 0) {
        self.phoneField.text = infoModel.mobile;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.nameField) {
        if (range.length == 1 && string.length == 0) {
            return YES;
        }else{
            
//            NSString * tempString = [self.nameField.text stringByAppendingString:string];
            NSString * tempString = self.nameField.text;

            if (tempString.length >=12) {
                tempString = [tempString substringToIndex:12];
                self.nameField.text = tempString;
                return NO;
            }
            
            return YES;
        }
    }else{
        if (range.length == 1 && string.length == 0) {
            return YES;
        }else{
            NSString * tempString = [self.phoneField.text stringByAppendingString:string];
            if (tempString.length >=11) {
                tempString = [tempString substringToIndex:11];
            }
            self.phoneField.text = tempString;
            return NO;
        }
    }
    
    return YES;
}

/* 初始化支付方式 */
- (void)setupPaySubViews{
    
//    NSArray * titleArray = @[@"微信支付",@"支付宝支付"];
//    NSArray * iconArray = @[@"wechat_pay",@"ali_pay"];
    
    NSArray * titleArray = @[@"微信支付"];
    NSArray * iconArray = @[@"wechat_pay"];
    
    for (int i = 0; i < titleArray.count; i++) {
        
        UIButton * payButton = [[UIButton alloc] init];
        payButton.tag = 1000+i;
        [payButton addTarget:self action:@selector(payButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.payView addSubview:payButton];
        [payButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.payView).mas_offset(0);
            make.top.mas_equalTo(self.payView).mas_offset(i*50+5);
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(SCREEN_WIDTH);
            
            if (i == 1) {
                make.bottom.mas_equalTo(self.payView).mas_offset(-5);
            }
        }];
        
        UIImageView * icon = [UIImageView new];
        icon.image = [UIImage imageNamed:iconArray[i]];
        [payButton addSubview:icon];
        [icon mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payButton);
            make.size.mas_equalTo(CGSizeMake(19, 19));
            make.left.mas_equalTo(payButton).mas_offset(20);
        }];
        
        UILabel * payTitleLabel = [UILabel new];
        payTitleLabel.text = titleArray[i];
        payTitleLabel.font = DHSystemFontOfSize_15;
        payTitleLabel.textColor = FWTextColor_12101D;
        payTitleLabel.textAlignment = NSTextAlignmentLeft;
        [payButton addSubview:payTitleLabel];
        [payTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payButton);
            make.size.mas_equalTo(CGSizeMake(200, 25));
            make.left.mas_equalTo(icon.mas_right).mas_offset(5);
        }];
        
        UIImageView * selectImageView = [UIImageView new];
        selectImageView.tag = payButton.tag +1000;
        [payButton addSubview:selectImageView];
        [selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(payButton);
            make.size.mas_equalTo(CGSizeMake(19, 19));
            make.right.mas_equalTo(payButton).mas_offset(-20);
        }];
        
        if (i == 0) {
            selectImageView.image = [UIImage imageNamed:@"radio_save_sel"];
        }else{
            selectImageView.image = [UIImage imageNamed:@"radio_save_unsel"];
        }
    }
}

#pragma mark - > 选择支付方式
- (void)payButtonClick:(UIButton *)sender{
    
    self.currentIndex = sender.tag -1000;
    
    if (self.currentIndex == 0) {
        
        UIImageView * imageView1 = (UIImageView *)[self.payView viewWithTag:self.currentIndex+2000];
        UIImageView * imageView2 = (UIImageView *)[self.payView viewWithTag:self.currentIndex+2000+1];
        
        imageView1.image = [UIImage imageNamed:@"radio_save_sel"];
        imageView2.image = [UIImage imageNamed:@"radio_save_unsel"];
    }else if (self.currentIndex == 1){
        
        UIImageView * imageView1 = (UIImageView *)[self.payView viewWithTag:self.currentIndex+2000-1];
        UIImageView * imageView2 = (UIImageView *)[self.payView viewWithTag:self.currentIndex+2000];
        
        imageView1.image = [UIImage imageNamed:@"radio_save_unsel"];
        imageView2.image = [UIImage imageNamed:@"radio_save_sel"];
    }
}

#pragma mark - > 支付
- (void)payButtonClick{
    
    if (self.nameField.text.length <= 0) {
    
        [[FWHudManager sharedManager] showErrorMessage:@"请输入联系人" toController:self.vc];
        return;
    }else{
        
        NSString * tempString = [self.nameField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (tempString.length <= 0) {
            
            self.nameField.text = @"";
            [[FWHudManager sharedManager] showErrorMessage:@"请输入有效的联系人" toController:self.vc];
            return;
        }
    }
    
    if (self.phoneField.text.length <= 0) {
        
        [[FWHudManager sharedManager] showErrorMessage:@"请输入手机号码" toController:self.vc];
        return;
    }else  {
        
        NSString * tempString = [self.phoneField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (tempString.length <= 0) {
            
            self.phoneField.text = @"";
            [[FWHudManager sharedManager] showErrorMessage:@"请输入有效的手机号" toController:self.vc];
            return;
        }
    }
    
    NSString * payMethod = @"wechat";
    
    if (self.currentIndex == 1){
        payMethod = @"alipay";
    }
    
    if ([self.delegate respondsToSelector:@selector(payButtonClick:)]) {
        [self.delegate payButtonClick:payMethod];
    }
}
@end
