//
//  FWCarDetailTongxiCell.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/6.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWCarModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCarDetailTongxiCell : UITableViewCell

@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIImageView * picImageView;
@property (nonatomic, strong) UILabel * carNameLabel;
@property (nonatomic, strong) UIImageView * arrowImageView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * signLabel;
@property (nonatomic, strong) UIButton * attentionButton;

@property (nonatomic, weak) UIViewController * vc;


@property (nonatomic, strong) FWCarListModel * carModel;


- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
