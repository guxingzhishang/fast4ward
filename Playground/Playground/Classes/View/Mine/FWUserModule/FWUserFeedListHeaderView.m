//
//  FWUserFeedListHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUserFeedListHeaderView.h"

@interface FWUserFeedListHeaderView()

@property (nonatomic, strong) UIButton * allButton;
@property (nonatomic, strong) UIButton * videoButton;
@property (nonatomic, strong) UIButton * picButton;
@property (nonatomic, strong) UIButton * askButton;
@property (nonatomic, strong) UIButton * reiftButton;
@property (nonatomic, strong) UIButton * idleButton;

@property (nonatomic, strong) UILabel * allLabel;
@property (nonatomic, strong) UILabel * videoLabel;
@property (nonatomic, strong) UILabel * picLabel;
@property (nonatomic, strong) UILabel * askLabel;
@property (nonatomic, strong) UILabel * refitLabel;
@property (nonatomic, strong) UILabel * idleLabel;

@property (nonatomic, strong) UIScrollView * topScrollView;


@property (nonatomic, assign) NSInteger currentType;

//@property (nonatomic, strong) UIView * topScrollView;
@end

@implementation FWUserFeedListHeaderView

+ (NSString *)headerViewIdentifier {
    return @"FWUserFeedListHeaderView";
}

+ (instancetype)headerViewWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath {
    FWUserFeedListHeaderView *headerView = (FWUserFeedListHeaderView*)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[FWUserFeedListHeaderView headerViewIdentifier] forIndexPath:indexPath];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.currentType = 0;
    }
    return self;
}

- (void)setupSubViews{
    
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
               
    self.topScrollView = [[UIScrollView alloc] init];
    self.topScrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.topScrollView];
    [self.topScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(54);
    }];
    
    self.allButton = [self setupButtonsWithTag:10090];
    self.allButton.backgroundColor = FWColor(@"eeeeee");
    [self.topScrollView addSubview:self.allButton];
    [self.allButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topScrollView);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(8);
    }];

    self.allLabel = [[UILabel alloc] init];
    self.allLabel.text = @"全部";
    self.allLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.allLabel.textColor = FWColor(@"515151");
    self.allLabel.textAlignment = NSTextAlignmentCenter;
    [self.allButton addSubview:self.allLabel];
    [self.allLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.allButton);
        make.height.mas_equalTo(self.allButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.allButton).mas_offset(5);
        make.right.mas_equalTo(self.allButton).mas_offset(-5);
    }];
    
    self.videoButton = [self setupButtonsWithTag:10091];
    self.videoButton.backgroundColor = FWClearColor;
    [self.topScrollView addSubview:self.videoButton];
    [self.videoButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topScrollView);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.allButton.mas_right).mas_offset(10);
    }];
    
    self.videoLabel = [[UILabel alloc] init];
    self.videoLabel.text = @"视频";
    self.videoLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.videoLabel.textColor = FWColor(@"515151");
    self.videoLabel.textAlignment = NSTextAlignmentCenter;
    [self.videoButton addSubview:self.videoLabel];
    [self.videoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.videoButton);
        make.height.mas_equalTo(self.videoButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.videoButton).mas_offset(5);
        make.right.mas_equalTo(self.videoButton).mas_offset(-5);
    }];
    
    self.picButton = [self setupButtonsWithTag:10092];
    self.picButton.backgroundColor = FWClearColor;
    [self.topScrollView addSubview:self.picButton];
    [self.picButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topScrollView);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.videoButton.mas_right).mas_offset(10);
    }];
    
    self.picLabel = [[UILabel alloc] init];
    self.picLabel.text = @"图文";
    self.picLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.picLabel.textColor = FWColor(@"515151");
    self.picLabel.textAlignment = NSTextAlignmentCenter;
    [self.picButton addSubview:self.picLabel];
    [self.picLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.picButton);
        make.height.mas_equalTo(self.picButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.picButton).mas_offset(5);
        make.right.mas_equalTo(self.picButton).mas_offset(-5);
    }];

    self.askButton = [self setupButtonsWithTag:10093];
    self.askButton.backgroundColor = FWClearColor;
    [self.topScrollView addSubview:self.askButton];
    [self.askButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topScrollView);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.picButton.mas_right).mas_offset(10);
    }];
    
    self.askLabel = [[UILabel alloc] init];
    self.askLabel.text = @"案例";
    self.askLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.askLabel.textColor = FWColor(@"515151");
    self.askLabel.textAlignment = NSTextAlignmentCenter;
    [self.askButton addSubview:self.askLabel];
    [self.askLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.askButton);
        make.height.mas_equalTo(self.askButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.askButton).mas_offset(5);
        make.right.mas_equalTo(self.askButton).mas_offset(-5);
    }];

    
    self.reiftButton = [self setupButtonsWithTag:10094];
    self.reiftButton.backgroundColor = FWClearColor;
    [self.topScrollView addSubview:self.reiftButton];
    [self.reiftButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topScrollView);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.askButton.mas_right).mas_offset(10);
    }];
    
    self.refitLabel = [[UILabel alloc] init];
    self.refitLabel.text = @"改装";
    self.refitLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.refitLabel.textColor = FWColor(@"515151");
    self.refitLabel.textAlignment = NSTextAlignmentCenter;
    [self.reiftButton addSubview:self.refitLabel];
    [self.refitLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.reiftButton);
        make.height.mas_equalTo(self.reiftButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.reiftButton).mas_offset(5);
        make.right.mas_equalTo(self.reiftButton).mas_offset(-5);
    }];
    
    
    self.idleButton = [self setupButtonsWithTag:10095];
    self.idleButton.backgroundColor = FWClearColor;
    [self.topScrollView addSubview:self.idleButton];
    [self.idleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topScrollView);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.reiftButton.mas_right).mas_offset(10);
        make.right.mas_equalTo(self.topScrollView).mas_offset(-10);
    }];
    
    self.idleLabel = [[UILabel alloc] init];
    self.idleLabel.text = @"闲置";
    self.idleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.idleLabel.textColor = FWColor(@"515151");
    self.idleLabel.textAlignment = NSTextAlignmentCenter;
    [self.idleButton addSubview:self.idleLabel];
    [self.idleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.idleButton);
        make.height.mas_equalTo(self.idleButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.idleButton).mas_offset(5);
        make.right.mas_equalTo(self.idleButton).mas_offset(-5);
    }];

    if (self.currentType == 0) {
        self.allButton.backgroundColor = FWColor(@"eeeeee");
        self.videoButton.backgroundColor = FWClearColor;
        self.picButton.backgroundColor = FWClearColor;
        self.askButton.backgroundColor = FWClearColor;
        self.reiftButton.backgroundColor = FWClearColor;
        self.idleButton.backgroundColor = FWClearColor;
    }else if (self.currentType == 1){
        self.allButton.backgroundColor =FWClearColor;
        self.videoButton.backgroundColor =  FWColor(@"eeeeee");
        self.picButton.backgroundColor = FWClearColor;
        self.askButton.backgroundColor = FWClearColor;
        self.reiftButton.backgroundColor = FWClearColor;
        self.idleButton.backgroundColor = FWClearColor;
    }else if (self.currentType == 2){
        self.allButton.backgroundColor = FWClearColor;
        self.videoButton.backgroundColor = FWClearColor;
        self.picButton.backgroundColor = FWColor(@"eeeeee");
        self.reiftButton.backgroundColor = FWClearColor;
        self.askButton.backgroundColor = FWClearColor;
        self.idleButton.backgroundColor = FWClearColor;
     }else if (self.currentType == 4){
         self.allButton.backgroundColor = FWClearColor;
         self.videoButton.backgroundColor = FWClearColor;
         self.picButton.backgroundColor = FWClearColor;
         self.askButton.backgroundColor = FWColor(@"eeeeee");
         self.reiftButton.backgroundColor = FWClearColor;
         self.idleButton.backgroundColor = FWClearColor;
     }else if (self.currentType == 5){
        self.allButton.backgroundColor = FWClearColor;
        self.videoButton.backgroundColor = FWClearColor;
        self.picButton.backgroundColor = FWClearColor;
        self.askButton.backgroundColor = FWClearColor;
        self.reiftButton.backgroundColor = FWColor(@"eeeeee");
        self.idleButton.backgroundColor = FWClearColor;
     }else{
         self.currentType = 6;
         self.allButton.backgroundColor = FWClearColor;
         self.videoButton.backgroundColor = FWClearColor;
         self.picButton.backgroundColor = FWClearColor;
         self.askButton.backgroundColor = FWClearColor;
         self.reiftButton.backgroundColor = FWClearColor;
         self.idleButton.backgroundColor = FWColor(@"eeeeee");
      }
}

- (UIButton *)setupButtonsWithTag:(NSInteger)tag{
    
    UIButton * topButton = [[UIButton alloc] init];
    topButton.tag = tag;
    topButton.layer.cornerRadius = 2;
    topButton.layer.masksToBounds = YES;
    [topButton addTarget:self action:@selector(topButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    return topButton;
}

#pragma mark  > 切换不同帖子
- (void)topButtonClick:(UIButton *)sender{

    NSInteger index = sender.tag-10090;

    if (index == 0) {
        self.currentType = 0;
        self.allButton.backgroundColor = FWColor(@"eeeeee");
        self.videoButton.backgroundColor = FWClearColor;
        self.picButton.backgroundColor = FWClearColor;
        self.askButton.backgroundColor = FWClearColor;
        self.reiftButton.backgroundColor = FWClearColor;
        self.idleButton.backgroundColor = FWClearColor;
    }else if (index == 1){
        self.currentType = 1;
        self.allButton.backgroundColor =FWClearColor;
        self.videoButton.backgroundColor =  FWColor(@"eeeeee");
        self.picButton.backgroundColor = FWClearColor;
        self.askButton.backgroundColor = FWClearColor;
        self.reiftButton.backgroundColor = FWClearColor;
        self.idleButton.backgroundColor = FWClearColor;
    }else if (index == 2){
        self.currentType = 2;
        self.allButton.backgroundColor = FWClearColor;
        self.videoButton.backgroundColor = FWClearColor;
        self.picButton.backgroundColor = FWColor(@"eeeeee");
        self.askButton.backgroundColor = FWClearColor;
        self.reiftButton.backgroundColor = FWClearColor;
        self.idleButton.backgroundColor = FWClearColor;
     }else if (index == 3){
         self.currentType = 4;
         self.allButton.backgroundColor = FWClearColor;
         self.videoButton.backgroundColor = FWClearColor;
         self.picButton.backgroundColor = FWClearColor;
         self.askButton.backgroundColor = FWColor(@"eeeeee");
         self.reiftButton.backgroundColor = FWClearColor;
         self.idleButton.backgroundColor = FWClearColor;
     }else  if (index == 4){
        self.currentType = 5;
        self.allButton.backgroundColor = FWClearColor;
        self.videoButton.backgroundColor = FWClearColor;
        self.picButton.backgroundColor = FWClearColor;
        self.askButton.backgroundColor = FWClearColor;
        self.reiftButton.backgroundColor = FWColor(@"eeeeee");
        self.idleButton.backgroundColor =FWClearColor;
     }else{
         self.currentType = 6;
         self.allButton.backgroundColor = FWClearColor;
         self.videoButton.backgroundColor = FWClearColor;
         self.picButton.backgroundColor = FWClearColor;
         self.askButton.backgroundColor = FWClearColor;
         self.reiftButton.backgroundColor = FWClearColor;
         self.idleButton.backgroundColor = FWColor(@"eeeeee");
      }
    
    if ([self.delegate respondsToSelector:@selector(reloadFeedList:)]) {
        [self.delegate reloadFeedList:self.currentType];
    }
}

- (void)setButtonText:(FWFeedModel *)model  withType:(NSString *)type{
    
    NSString * videoString = @"视频";
    NSString * picString = @"图文";
    NSString * refitString = @"改装";
    NSString * askString = @"案例";
    NSString * idleString = @"闲置";

    NSInteger total = 0;
    
    NSInteger videoCount = 0;
    NSInteger imageCount = 0;
    NSInteger askCount = 0;
    NSInteger refitCount = 0;
    NSInteger idleCount = 0;

    if ([type integerValue] ==  1) {
        videoCount = [model.feed_count_video integerValue];
        imageCount = [model.feed_count_image integerValue];
        askCount = [model.feed_count_wenda integerValue];
        refitCount = [model.feed_count_gaizhuang integerValue];
        idleCount = [model.feed_count_xianzhi integerValue];
    }else {
        videoCount = [model.total_count_video integerValue];
        imageCount = [model.total_count_image integerValue];
        askCount = [model.total_count_wenda integerValue];
        refitCount = [model.total_count_gaizhuang integerValue];
        idleCount = [model.total_count_xianzhi integerValue];
    }
    
    if (videoCount  > 0) {
        total = total + videoCount;
        videoString = [NSString stringWithFormat:@"视频%@",@(videoCount).stringValue];
    }
    
    if (imageCount > 0) {
        total = total + imageCount;
        picString = [NSString stringWithFormat:@"图文%@",@(imageCount).stringValue];
    }
    
    if (askCount > 0) {
        total = total + askCount;
        askString = [NSString stringWithFormat:@"案例%@",@(askCount).stringValue];
    }
    
    if (refitCount > 0) {
        total = total + refitCount;
        refitString = [NSString stringWithFormat:@"改装%@",@(refitCount).stringValue];
    }
    
    if (idleCount > 0) {
        total = total + idleCount;
        idleString = [NSString stringWithFormat:@"闲置%@",@(idleCount).stringValue];
    }
    
    if (total > 0) {
        self.allLabel.text = [NSString stringWithFormat:@"全部%@",@(total).stringValue] ;
    }
    self.videoLabel.text = videoString;
    self.picLabel.text = picString;
    self.askLabel.text = askString;
    self.refitLabel.text = refitString;
    self.idleLabel.text = idleString;

    
    [self.allLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.allButton);
        make.height.mas_equalTo(self.allButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.allButton).mas_offset(5);
        make.right.mas_equalTo(self.allButton).mas_offset(-5);
    }];
    
    [self.videoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.videoButton);
        make.height.mas_equalTo(self.videoButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.videoButton).mas_offset(5);
        make.right.mas_equalTo(self.videoButton).mas_offset(-5);
    }];
    
    [self.picLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.picButton);
        make.height.mas_equalTo(self.picButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.picButton).mas_offset(5);
        make.right.mas_equalTo(self.picButton).mas_offset(-5);
    }];
    
    [self.askLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.askButton);
        make.height.mas_equalTo(self.askButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.askButton).mas_offset(5);
        make.right.mas_equalTo(self.askButton).mas_offset(-5);
    }];
    
    [self.refitLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.reiftButton);
        make.height.mas_equalTo(self.reiftButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.reiftButton).mas_offset(5);
        make.right.mas_equalTo(self.reiftButton).mas_offset(-5);
    }];
    
    [self.idleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.idleButton);
        make.height.mas_equalTo(self.idleButton);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.idleButton).mas_offset(5);
        make.right.mas_equalTo(self.idleButton).mas_offset(-5);
    }];
}

@end
