//
//  FWNewUserHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWNewUserHeaderView.h"
#import "FWSettingViewController.h"
#import "FWMyGoldenViewController.h"
#import "FWFollowViewController.h"
#import "FWFunsViewController.h"
#import "FWShopInfoDetailViewController.h"
#import "FWPlayerArchivesViewController.h"
#import "FWChangePhotoViewController.h"
#import "FWMyVIPViewController.h"
#import "FWVisitorViewController.h"

@implementation FWNewUserHeaderView
@synthesize mineModel;

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 3);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self).mas_offset(0);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    self.topImageView = [[UIImageView alloc] init];
    self.topImageView.clipsToBounds = YES;
    self.topImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topImageView.userInteractionEnabled = YES;
    self.topImageView.image = [UIImage imageNamed:@"palceholder"];
    [self.bgView addSubview:self.topImageView];
    [self.topImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.bgView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 147+FWCustomeSafeTop));
    }];
    
    UIBlurEffect *blurEffect =[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView =[[UIVisualEffectView alloc]initWithEffect:blurEffect];
    [self.topImageView addSubview:effectView];
    [effectView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.topImageView);
    }];
    
    self.settingButton = [[UIButton alloc] init];
    [self.settingButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.settingButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.settingButton];
    [self.settingButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topImageView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(self.topImageView).mas_offset(30+FWCustomeSafeTop);
    }];
    
    self.shareButton = [[UIButton alloc] init];
    [self.shareButton setImage:[UIImage imageNamed:@"new_white_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.shareButton];

    
    self.signButton = [[UIButton alloc] init];
    [self.signButton setImage:[UIImage imageNamed:@"white_more"] forState:UIControlStateNormal];
    [self.signButton addTarget:self action:@selector(signButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.signButton];
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        self.shareButton.hidden = NO;
        
        [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.topImageView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.settingButton).mas_offset(1);
        }];

        [self.signButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.shareButton.mas_left).mas_offset(-10);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.settingButton);
        }];
    }else{
        self.shareButton.hidden = YES;
        
        [self.signButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.topImageView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.settingButton);
        }];
    }
    [self.topImageView bringSubviewToFront:self.shareButton];
    [self.topImageView bringSubviewToFront:self.settingButton];
    [self.topImageView bringSubviewToFront:self.signButton];

    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.clipsToBounds = YES;
    self.photoImageView.layer.cornerRadius = 89/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.userInteractionEnabled = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.bgView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(14);
        make.bottom.mas_equalTo(self.topImageView).mas_offset(89/2);
        make.size.mas_equalTo(CGSizeMake(89,89));
    }];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cleckImageViewAction)];
    [self.photoImageView addGestureRecognizer:tapGesture];
    
    
    self.rightButton = [[UIButton alloc] init];
    self.rightButton.layer.cornerRadius = 2;
    self.rightButton.layer.masksToBounds = YES;
    self.rightButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    self.rightButton.backgroundColor = FWTextColor_222222;
    [self.rightButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(rightButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.rightButton];
    [self.rightButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(self.topImageView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(68);
    }];
    
    self.leftButton = [[UIButton alloc] init];
    self.leftButton.layer.cornerRadius = 2;
    self.leftButton.layer.masksToBounds = YES;
    self.leftButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    self.leftButton.backgroundColor = FWColor(@"ff6f00");
    [self.leftButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(leftButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.leftButton];
    [self.leftButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.rightButton.mas_left).mas_offset(-10);
        make.height.mas_equalTo(self.rightButton);
        make.centerY.mas_equalTo(self.rightButton);
        make.width.mas_equalTo(self.self.rightButton);
    }];
    
    self.nameLable = [[UILabel alloc] init];
    self.nameLable.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 23];
    self.nameLable.textColor = FWTextColor_222222;
    self.nameLable.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLable];
    [self.nameLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(14);
        make.height.mas_equalTo(30);
        make.top.mas_equalTo(self.photoImageView.mas_bottom).mas_offset(20);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.vipButton = [[UIButton alloc] init];
    [self.vipButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.vipButton addTarget:self action:@selector(vipButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.vipButton];
    [self.vipButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.nameLable);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.left.mas_equalTo(self.nameLable.mas_right).mas_offset(5);
    }];
    
    self.f4wIDButton = [[UIButton alloc] init];
    self.f4wIDButton.enabled = NO;
    self.f4wIDButton.layer.cornerRadius = 2;
    self.f4wIDButton.layer.masksToBounds = YES;
    self.f4wIDButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    self.f4wIDButton.backgroundColor = FWColor(@"eeeeee");
    [self.f4wIDButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
    [self.bgView addSubview:self.f4wIDButton];
    [self.f4wIDButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLable);
        make.size.mas_equalTo(CGSizeMake(100, 21));
        make.top.mas_equalTo(self.nameLable.mas_bottom).mas_offset(5);
    }];
    
    self.efectButton = [[UIButton alloc] init];
    self.efectButton.layer.cornerRadius = 2;
    self.efectButton.layer.masksToBounds = YES;
    self.efectButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    self.efectButton.backgroundColor = FWColor(@"eeeeee");
    [self.efectButton setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
    [self.efectButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
    [self.efectButton addTarget:self action:@selector(effectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.efectButton];
    [self.efectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.f4wIDButton.mas_right).mas_offset(10);
        make.size.mas_equalTo(self.f4wIDButton);
        make.centerY.mas_equalTo(self.f4wIDButton);
    }];
    
    self.biaoshiImageView = [[UIImageView alloc] init];
    self.biaoshiImageView.image = [UIImage imageNamed:@""];
    [self.bgView addSubview:self.biaoshiImageView];
    [self.biaoshiImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(16, 16));
        make.left.mas_equalTo(self.nameLable);
        make.top.mas_equalTo(self.f4wIDButton.mas_bottom).mas_offset(7.5);
    }];
    
    self.renzhengLable = [[UILabel alloc] init];
    self.renzhengLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.renzhengLable.textColor = FWColor(@"515151");
    self.renzhengLable.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.renzhengLable];
    [self.renzhengLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.biaoshiImageView.mas_right).mas_offset(5);
        make.height.mas_equalTo(18);
        make.centerY.mas_equalTo(self.biaoshiImageView);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.signLable = [[UILabel alloc] init];
    self.signLable.numberOfLines = 2;
    self.signLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.signLable.textColor = FWColor(@"515151");
    self.signLable.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.signLable];
    [self.signLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLable);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.biaoshiImageView.mas_bottom).mas_offset(5);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
    }];
    
    self.zanLable = [[UILabel alloc] init];
    self.zanLable.userInteractionEnabled = YES;
    self.zanLable.textColor = FWTextColor_222222;
    self.zanLable.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.zanLable];
    [self.zanLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLable);
        make.height.mas_equalTo(38);
        make.top.mas_equalTo(self.signLable.mas_bottom).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-10);
    }];
    [self.zanLable addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zanClick)]];

    
    self.followLable = [[UILabel alloc] init];
    self.followLable.userInteractionEnabled = YES;
    self.followLable.textColor = FWTextColor_222222;
    self.followLable.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.followLable];
    [self.followLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.zanLable.mas_right).mas_offset(15);
        make.height.mas_equalTo(self.zanLable);
        make.centerY.mas_equalTo(self.zanLable);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    [self.followLable addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followClick)]];
    
    self.funsLable = [[UILabel alloc] init];
    self.funsLable.userInteractionEnabled = YES;
    self.funsLable.textColor = FWTextColor_222222;
    self.funsLable.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.funsLable];
    [self.funsLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.followLable.mas_right).mas_offset(15);
        make.height.mas_equalTo(self.followLable);
        make.centerY.mas_equalTo(self.followLable);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    [self.funsLable addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(funsClick)]];

    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(2);
        make.top.mas_equalTo(self.shadowView.mas_bottom);
    }];
}

#pragma mark - > 影响力
- (void)effectButtonClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.mineModel.h5_jifen;
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

-(void)configForView:(id)model{
    
    mineModel = (FWMineModel *)model;
    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    if ([mineModel.user_info.uid isEqualToString:usvm.userModel.uid]) {
        /* 自己 */
        self.signButton.hidden = YES;
        [self.rightButton setTitle:@"我的店铺" forState:UIControlStateNormal];
//        [self.leftButton setTitle:@"会员中心" forState:UIControlStateNormal];
        self.leftButton.hidden = YES;
    }else{
        [self.rightButton setTitle:@"TA的店铺" forState:UIControlStateNormal];
        
        self.leftButton.hidden = NO;
        self.signButton.hidden = NO;

        if ([mineModel.follow_status isEqualToString:@"2"]) {
            /* 未关注 */
            self.leftButton.backgroundColor = FWColor(@"ff6f00");
            [self.leftButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
            [self.leftButton setTitle:@"关注" forState:UIControlStateNormal];
        }else {
            /* 已关注 */
            self.leftButton.backgroundColor = FWColor(@"eeeeee");
            [self.leftButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
            [self.leftButton setTitle:@"已关注" forState:UIControlStateNormal];
        }
    }

    if ([mineModel.user_info.cert_status isEqualToString:@"2"]||
        ([mineModel.user_info.cert_status isEqualToString:@"1"] && [mineModel.user_info.merchant_cert_status isEqualToString:@"3"])){
        /* 商家 */
        self.rightButton.hidden = NO;
        [self.leftButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.rightButton.mas_left).mas_offset(-10);
            make.height.mas_equalTo(self.rightButton);
            make.centerY.mas_equalTo(self.rightButton);
            make.width.mas_equalTo(self.self.rightButton);
        }];
    }else{
        self.rightButton.hidden = YES;
        [self.leftButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.bgView).mas_offset(-14);
            make.height.mas_equalTo(30);
            make.top.mas_equalTo(self.topImageView.mas_bottom).mas_offset(10);
            make.width.mas_equalTo(68);
        }];
    }
    
    [self.f4wIDButton setTitle:[NSString stringWithFormat:@"ID：%@",mineModel.user_info.f4w_id] forState:UIControlStateNormal];
    [self.efectButton setTitle:[NSString stringWithFormat:@" 影响力%@",mineModel.user_info.effect_value] forState:UIControlStateNormal];
    
    self.photoImageView.layer.cornerRadius = 89/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:mineModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:mineModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];

    self.nameLable.text = mineModel.user_info.nickname;
    
    BOOL haveBiaoshi = NO;
    
    /* 商家认证身份 */
    if([mineModel.user_info.cert_status isEqualToString:@"2"]) {
        haveBiaoshi = YES;
    }else{
        haveBiaoshi = NO;
    }
    
    if (haveBiaoshi) {
        /* 有标识，代表是付费商家，认证过 */
        self.biaoshiImageView.image = [UIImage imageNamed:@"middle_bussiness"];
        self.renzhengLable.text = mineModel.user_info.cert_name;

        [self.biaoshiImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(16, 16));
            make.left.mas_equalTo(self.nameLable);
            make.top.mas_equalTo(self.f4wIDButton.mas_bottom).mas_offset(7.5);
        }];
        [self.renzhengLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.biaoshiImageView.mas_right).mas_offset(5);
            make.height.mas_equalTo(18);
            make.centerY.mas_equalTo(self.biaoshiImageView);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
    }else{
        /* 没标识，继续判断是否认付费过 */
        self.biaoshiImageView.image = [UIImage imageNamed:@""];
        
        if (mineModel.user_info.cert_name.length > 0 ||
            mineModel.user_info.title.length > 0 ) {
            
            if (mineModel.user_info.cert_name.length > 0 ) {
                self.renzhengLable.text = mineModel.user_info.cert_name;
            }else{
                self.renzhengLable.text = mineModel.user_info.title;
            }
            
            [self.biaoshiImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(0.01, 16));
                make.left.mas_equalTo(self.nameLable);
                make.top.mas_equalTo(self.f4wIDButton.mas_bottom).mas_offset(7.5);
            }];
            
            [self.renzhengLable mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.biaoshiImageView.mas_right).mas_offset(0);
                make.height.mas_equalTo(18);
                make.centerY.mas_equalTo(self.biaoshiImageView);
                make.width.mas_greaterThanOrEqualTo(10);
            }];
        }else{
            [self.biaoshiImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(0.01, 0.01));
                make.left.mas_equalTo(self.nameLable);
                make.top.mas_equalTo(self.f4wIDButton.mas_bottom).mas_offset(7.5);
            }];
            
            [self.renzhengLable mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.biaoshiImageView.mas_right).mas_offset(5);
                make.height.mas_equalTo(0.01);
                make.centerY.mas_equalTo(self.biaoshiImageView);
                make.width.mas_greaterThanOrEqualTo(10);
            }];
        }
    }

    if(mineModel.user_info.autograph.length > 0){
        self.signLable.text = mineModel.user_info.autograph;
    }else{
        self.signLable.text = @"这家伙很懒 什么都没留下~";
    }
//        [self.signLable mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(self.nameLable);
//            make.right.mas_equalTo(self.bgView).mas_offset(-14);
//            make.height.mas_greaterThanOrEqualTo(10);
//            make.top.mas_equalTo(self.biaoshiImageView.mas_bottom).mas_offset(10);
//            make.width.mas_equalTo(SCREEN_WIDTH-28);
//        }];
//    }else{
//        [self.signLable mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(self.nameLable);
//            make.right.mas_equalTo(self.bgView).mas_offset(-14);
//            make.height.mas_equalTo(0.01);
//            make.top.mas_equalTo(self.biaoshiImageView.mas_bottom).mas_offset(0);
//            make.width.mas_equalTo(SCREEN_WIDTH-28);
//        }];
//    }
    
    [self.zanLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLable);
        make.height.mas_equalTo(38);
        make.top.mas_equalTo(self.signLable.mas_bottom).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-10);
    }];
    
    self.zanLable.attributedText = [self dealWithNum:mineModel.beliked_count type:@" 获赞"];
    self.followLable.attributedText = [self dealWithNum:mineModel.follow_count type:@" 关注"];
    self.funsLable.attributedText = [self dealWithNum:mineModel.fans_count type:@" 粉丝"];
    
    if ([mineModel.user_info.cert_status isEqualToString:@"2"]){
        self.biaoshiImageView.hidden = NO;
    }else{
        self.biaoshiImageView.hidden = YES;
    }

    
    self.vipButton.enabled = YES;
    if ([mineModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([mineModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([mineModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        [self.vipButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        self.vipButton.enabled = NO;
    }
    
    [self layoutIfNeeded];
    self.currentHeight = CGRectGetHeight(self.bgView.frame);
}

- (NSMutableAttributedString *)dealWithNum:(NSString *)numstring type:(NSString *)type{
    
    NSString * textString = [NSString stringWithFormat:@"%@%@",numstring,type];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:textString];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Semibold" size: 18] range:NSMakeRange(0, textString.length-3)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Regular" size: 12] range:NSMakeRange(textString.length-3, 3)];
    
    return str;
}

#pragma mark - > 会员中心/关注
- (void)leftButtonClick{
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        NSString * action ;
        
        if ([mineModel.follow_status isEqualToString:@"2"]) {
            action = Submit_follow_users;
        }else{
            action = Submit_cancel_follow_users;
        }
        
        FWFollowRequest * request = [[FWFollowRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"f_uid":mineModel.user_info.uid,
                                  };
        [request startWithParameters:params WithAction:action WithDelegate:self .viewController completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([action isEqualToString:Submit_follow_users]) {
                    mineModel.follow_status = [[back objectForKey:@"data"] objectForKey:@"follow_status"];
                    self.leftButton.backgroundColor = FWColor(@"eeeeee");
                    [self.leftButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
                    [self.leftButton setTitle:@"已关注" forState:UIControlStateNormal];

                    [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self.vc];
                }else{
                    mineModel.follow_status = @"2";
                    
                    self.leftButton.backgroundColor = FWColor(@"ff6f00");
                    [self.leftButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
                    [self.leftButton setTitle:@"关注" forState:UIControlStateNormal];
                }
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}

#pragma mark - > 最近访客
- (void)visitClick{
    [self.vc.navigationController pushViewController:[FWVisitorViewController new] animated:YES];
}

#pragma mark - > 我的店铺/ ta的店铺
- (void)rightButtonClick{
    
    if (!mineModel.user_info.uid) {
        return;
    }
    
    if ([self.mineModel.user_info.cert_status isEqualToString:@"2"]){
        // 店铺页
        FWCustomerShopViewController * UIVC = [[FWCustomerShopViewController alloc] init];
        UIVC.user_id = mineModel.user_info.uid;
        UIVC.isUserPage = YES;
        [self.viewController.navigationController pushViewController:UIVC animated:YES];
    }else{
        // 非付费商家 进入介绍页
        FWShopInfoDetailViewController  * UIVC = [[FWShopInfoDetailViewController alloc] init];
        UIVC.user_id = mineModel.user_info.uid;
        UIVC.isUserPage = NO;
        [self.viewController.navigationController pushViewController:UIVC animated:YES];
    }
}

#pragma mark - > vip
- (void)vipButtonClick{
    
}

#pragma mark - > 更多
- (void)signButtonClick{
    if ([self.delegate respondsToSelector:@selector(moreOnClick)]) {
        [self.delegate moreOnClick];
    }
}

#pragma mark - > 返回
- (void)backButtonClick{
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 分享
- (void)shareButtonClick{
    if ([self.delegate respondsToSelector:@selector(shareOnClick)]) {
        [self.delegate shareOnClick];
    }
}

#pragma mark - > 点赞
- (void)zanClick{
    
    NSString * userName = mineModel.user_info.nickname?mineModel.user_info.nickname:@"";
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"获赞数" message:[NSString stringWithFormat:@"%@一共获得 %@ 个赞",userName,mineModel.beliked_count] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark - > 关注
- (void)followClick{
    
    FWFollowViewController * FVC = [[FWFollowViewController alloc] init];
    FVC.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
    [self.vc.navigationController pushViewController:FVC animated:YES];
}

#pragma mark - > 粉丝
- (void)funsClick{
    
    FWFunsViewController * FVC = [[FWFunsViewController alloc] init];
    FVC.funsType = @"2";
    FVC.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
    [self.vc.navigationController pushViewController:FVC animated:YES];
}

#pragma mark - > 查看大头像
- (void) cleckImageViewAction{
    
    FWChangePhotoViewController * vc = [[FWChangePhotoViewController alloc] init];
    vc.photoURL = mineModel.user_info.header_url_original;
    vc.isMine = NO;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.4];
    [animation setType: kCATransitionMoveIn];
    [animation setSubtype: kCATransitionFromTop];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    [self.vc.navigationController pushViewController:vc animated:NO];
    [self.vc.navigationController.view.layer addAnimation:animation forKey:nil];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

@end
