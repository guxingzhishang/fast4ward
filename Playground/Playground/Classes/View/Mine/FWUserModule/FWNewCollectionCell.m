//
//  FWNewCollectionCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/12.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWNewCollectionCell.h"

@implementation FWNewCollectionCell

+ (NSString *)cellIdentifier {
    return @"FWNewCollectionCell";
}

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath {
    FWNewCollectionCell *cell = (FWNewCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:[FWNewCollectionCell cellIdentifier] forIndexPath:indexPath];
    return cell;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}


@end
