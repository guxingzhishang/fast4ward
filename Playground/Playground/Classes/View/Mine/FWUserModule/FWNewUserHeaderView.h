//
//  FWNewUserHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@protocol FWNewUserHeaderViewDelegate <NSObject>

- (void)shareOnClick;

- (void)moreOnClick;

@end

@interface FWNewUserHeaderView : UIView

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UIToolbar *toolBar;

@property (nonatomic, strong) UIButton * settingButton;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) UIButton * signButton;

@property (nonatomic, strong) UIImageView * topImageView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIView *background;//图片放大
/* 查看大图的view */
@property (nonatomic, strong) UIImageView * browseImgView;

@property (nonatomic, strong) UILabel * changeLable;

@property (nonatomic, strong) UIButton * leftButton;// 关注 或会员
@property (nonatomic, strong) UIButton * rightButton;// ta店铺 或我的店铺

@property (nonatomic, strong) UILabel * nameLable;
@property (nonatomic, strong) UIButton * vipButton;
@property (nonatomic, strong) UIButton * f4wIDButton;
@property (nonatomic, strong) UIButton * efectButton;

@property (nonatomic, strong) UIImageView * biaoshiImageView;
@property (nonatomic, strong) UILabel * renzhengLable;
@property (nonatomic, strong) UILabel * signLable;

@property (nonatomic, strong) UILabel * zanLable;
@property (nonatomic, strong) UILabel * followLable;
@property (nonatomic, strong) UILabel * funsLable;

@property (nonatomic, assign) CGFloat  currentHeight;

@property (nonatomic, assign) id<FWNewUserHeaderViewDelegate>delegate;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, strong) FWMineModel * mineModel;

-(void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
