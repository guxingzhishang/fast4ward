//
//  FWUserFeedListHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWUserFeedListHeaderViewDelegate <NSObject>

- (void)reloadFeedList:(NSInteger)index;

@end

@interface FWUserFeedListHeaderView : UICollectionReusableView

@property (nonatomic, assign) id  <FWUserFeedListHeaderViewDelegate>delegate;

+ (NSString *)headerViewIdentifier;

+ (instancetype)headerViewWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath;

/* type:1 帖子   2：收藏 */
- (void)setButtonText:(FWFeedModel *)model withType:(NSString *)type;

- (void)setupSubViews;

@end

NS_ASSUME_NONNULL_END
