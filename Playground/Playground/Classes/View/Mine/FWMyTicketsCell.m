//
//  FWMyTicketsCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyTicketsCell.h"
#import "FWOrderDetailViewController.h"

@interface FWMyTicketsCell()

@property (nonatomic, strong) FWTicketsListModel * listModel;

@end

@implementation FWMyTicketsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupUnReceiveView];
        [self setupReceiveView];
    }
    
    return self;
}

- (void)setupUnReceiveView{
    
    /* 未领取时的承载视图 */
    self.unReceiveView = [UIImageView new];
    self.unReceiveView.userInteractionEnabled = YES;
    self.unReceiveView.image = [UIImage imageNamed:@"ticket_lingqu"];
    [self.contentView addSubview:self.unReceiveView];
    [self.unReceiveView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(0);
        make.left.mas_equalTo(self.contentView).mas_offset(5);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
        make.bottom.mas_equalTo(self.contentView).mas_offset(0);
        make.height.mas_equalTo(123*(SCREEN_WIDTH-10)/365);
    }];
    
    self.unReceiveNameLabel = [[UILabel alloc] init];
    self.unReceiveNameLabel.font = DHSystemFontOfSize_15;
    self.unReceiveNameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.unReceiveNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.unReceiveView addSubview:self.unReceiveNameLabel];
    [self.unReceiveNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.unReceiveView);
        make.left.mas_equalTo(self.unReceiveView).mas_offset(34);
        make.bottom.mas_equalTo(self.unReceiveView);
        make.width.mas_equalTo(SCREEN_WIDTH-100);
    }];
    
    self.reeceiveButton = [[UIButton alloc] init];
    self.reeceiveButton.titleLabel.font = DHSystemFontOfSize_14;
    [self.reeceiveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.reeceiveButton addTarget:self action:@selector(reeceiveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.unReceiveView addSubview:self.reeceiveButton];
    [self.reeceiveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.unReceiveView);
        make.right.mas_equalTo(self.unReceiveView).mas_offset(-24);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(self.unReceiveView);
    }];
}

- (void)setupReceiveView{
    
    /* 领取时的承载视图 */
    self.receiveView = [UIImageView new];
    self.receiveView.userInteractionEnabled = YES;
    self.receiveView.image = [UIImage imageNamed:@"ticket_yilingqu"];
    [self.contentView addSubview:self.receiveView];
    [self.receiveView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(0);
        make.left.mas_equalTo(self.contentView).mas_offset(5);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
        make.bottom.mas_equalTo(self.contentView).mas_offset(0);
        make.height.mas_equalTo(123*(SCREEN_WIDTH-10)/365);
    }];
    self.receiveView.hidden = YES;
    
    self.receiveNameLabel = [[UILabel alloc] init];
    self.receiveNameLabel.font = DHSystemFontOfSize_15;
    self.receiveNameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.receiveNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.receiveView addSubview:self.receiveNameLabel];
    [self.receiveNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.receiveView).mas_offset(20);
        make.left.mas_equalTo(self.receiveView).mas_offset(34);
        make.width.mas_equalTo(SCREEN_WIDTH-75);
        make.height.mas_equalTo(20);
    }];
    
//    self.codeLabel = [[UILabel alloc] init];
//    self.codeLabel.font = DHSystemFontOfSize_13;
//    self.codeLabel.textColor = FWTextColor_C9C9C9;
//    self.codeLabel.textAlignment = NSTextAlignmentLeft;
//    [self.receiveView addSubview:self.codeLabel];
//    [self.codeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.receiveView);
//        make.left.mas_equalTo(self.receiveNameLabel);
//        make.width.mas_greaterThanOrEqualTo(20);
//        make.height.mas_equalTo(20);
//    }];
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.numberOfLines = 0;
    self.timeLabel.font = DHSystemFontOfSize_13;
    self.timeLabel.textColor = FWTextColor_C9C9C9;
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.receiveView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.receiveView).mas_offset(-20);
        make.left.mas_equalTo(self.receiveNameLabel);
        make.width.mas_equalTo(self.receiveView);
        make.height.mas_equalTo(20);
    }];
    
    self.stateLabel = [[UILabel alloc] init];
    self.stateLabel.font = DHSystemFontOfSize_15;
    self.stateLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.stateLabel.textAlignment = NSTextAlignmentRight;
    [self.receiveView addSubview:self.stateLabel];
    [self.stateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.receiveView);
        make.right.mas_equalTo(self.receiveView).mas_offset(-24);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(20);
    }];
    
//    self.cpButton = [[UIButton alloc] init];
//    self.cpButton.titleLabel.font = DHSystemFontOfSize_13;
//    [self.cpButton setTitle:@"复制" forState:UIControlStateNormal];
//    [self.cpButton setTitleColor:FWTextColor_2B98FA forState:UIControlStateNormal];
//    [self.cpButton addTarget:self action:@selector(cpButtonClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.receiveView addSubview:self.cpButton];
//    [self.cpButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(50);
//        make.height.mas_equalTo(self.receiveView);
//        make.left.mas_equalTo(self.codeLabel.mas_right).mas_offset(-5);
//        make.centerY.mas_equalTo(self.codeLabel);
//    }];
}

- (void)cpButtonClick{
    
    [[FWHudManager sharedManager] showSuccessMessage:@"复制成功" toController:self.vc];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [self.codeLabel.text substringFromIndex:4];
}

- (void)configForCellWithModel:(FWTicketsListModel *)model{
    
    self.listModel = (FWTicketsListModel *)model;
    
    if ([self.listModel.get_status isEqualToString:@"1"]) {
        /* 已领取 */
        self.receiveView.hidden = NO;
        self.unReceiveView.hidden = YES;
        
        self.receiveNameLabel.text = self.listModel.name;
//        self.codeLabel.text = [NSString stringWithFormat:@"兑换码:%@",self.listModel.code];

        NSString * remark = self.listModel.remark;
        remark = [remark stringByReplacingOccurrencesOfString:@"适用比赛：" withString:@""];

        self.timeLabel.text = remark;
        
    }else if ([self.listModel.get_status isEqualToString:@"3"]) {
        /* 未领取 */
        self.receiveView.hidden = YES;
        self.unReceiveView.hidden = NO;
        
        self.unReceiveNameLabel.text = self.listModel.name;
        
    }
}

#pragma mark - > 领取门票
- (void)reeceiveButtonClick{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:self.ticketsModel.confirm_text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"领取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self requestGetTickets];
    }];
    
    UIView *subView1 = alertController.view.subviews[0];
    UIView *subView2 = subView1.subviews[0];
    UIView *subView3 = subView2.subviews[0];
    UIView *subView4 = subView3.subviews[0];
    UIView *subView5 = subView4.subviews[0];
    
    for (UILabel * label in subView5.subviews) {
        if ([label isKindOfClass:[UILabel class]]&&
            [label.text isEqualToString:self.ticketsModel.confirm_text]) {
            label.textAlignment = NSTextAlignmentLeft;
        }
    }
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 请求领取门票
- (void)requestGetTickets{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"gift_type":self.listModel.gift_type,
                              };
    
    [request startWithParameters:params WithAction:Submit_get_ticket WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWTicketsInfoModel * infoModel = [FWTicketsInfoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            self.receiveView.hidden = NO;
            self.unReceiveView.hidden = YES;
            
            self.receiveNameLabel.text = self.listModel.name;
//            self.codeLabel.text = [NSString stringWithFormat:@"兑换码:%@",infoModel.code];
            
            NSString * remark = self.listModel.remark;
            remark = [remark stringByReplacingOccurrencesOfString:@"适用比赛：" withString:@""];
            
            self.timeLabel.text = remark;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                FWOrderDetailViewController * ODVC = [[FWOrderDetailViewController alloc] init];
                ODVC.order_id = infoModel.order_id;
                [self.vc.navigationController pushViewController:ODVC animated:YES];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

@end

