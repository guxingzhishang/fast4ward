//
//  FWFollowCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 关注cell
 */
#import "FWListCell.h"
#import "FWFollowModel.h"

@class FWFollowCell;
@protocol FWFollowCellCellDelegate<NSObject>

- (void)followButtonForIndex:(NSInteger)index  withCell:(FWFollowCell *)cell;

@end

@interface FWFollowCell : FWListCell

@property (nonatomic, strong) UIButton * followButton;

@property (nonatomic, strong) UIButton * vipImageButton;

@property (nonatomic, strong) FWFollowUserListModel * followModel;

@property (nonatomic, weak) id<FWFollowCellCellDelegate> delegate;

@end
