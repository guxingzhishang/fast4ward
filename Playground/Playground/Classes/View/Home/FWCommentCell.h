//
//  FWCommentCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWCommentModel.h"
#import "FWCommentFrame.h"

@class FWCommentCell;

@protocol FWCommentCellDelegate <NSObject>

@optional
/** 点击评论的昵称 */
- (void) commentCell:(FWCommentCell *)commentCell didClickedUser:(FWMineInfoModel *)user;

/** 用户点击点赞按钮 */
- (void)likesButtonClickWithCommentFrame:(FWCommentFrame *)commentFrame WithIndex:(NSInteger)index withcommentCell:(FWCommentCell *)commentCell;

- (void)avatarTapClick:(NSInteger)index;

@end

@interface FWCommentCell : FWListCell

//@property (nonatomic, strong) UILabel * contentLabel;
//
@property (nonatomic, strong) UILabel * likesLabel;
//
@property (nonatomic, strong) UIButton * likesButton;

@property (nonatomic, strong) UIImageView * likesImageView;

/** 文本内容 */
@property (nonatomic , weak) YYLabel *contentLabel;

@property (nonatomic, weak) UIViewController * viewcontroller;

@property (nonatomic, weak) id<FWCommentCellDelegate> delegate;

/** 评论Frame */
@property (nonatomic , strong) FWCommentFrame *commentFrame;

/** identityID **/
@property (nonatomic , strong) NSString * identityID;

+ (instancetype)cellWithTableView:(UITableView *)tableView WithID:(NSString *)cellID;

@end
