//
//  FWSearchResultNavigationView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/20.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWSearchResultNavigationView.h"

@implementation FWSearchResultNavigationView

- (id)init{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, 55+40+20+FWCustomeSafeTop);
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.resultSearchBar = [[UISearchBar alloc] init];
    self.resultSearchBar.delegate = self;
    self.resultSearchBar.tintColor = [UIColor darkGrayColor];
    self.resultSearchBar.barTintColor = [UIColor whiteColor];
    self.resultSearchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.resultSearchBar.placeholder = @"";
    [self addSubview:self.resultSearchBar];
    [self.resultSearchBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(7);
        make.top.mas_equalTo(self).mas_offset(20+FWCustomeSafeTop);
        make.right.mas_equalTo(self).mas_offset(-50);
        make.height.mas_equalTo(40);
    }];
    
        
    if (@available(iOS 13.0, *)) {
        UITextField *textField = (UITextField *)self.resultSearchBar.searchTextField;
        
        textField.font = DHSystemFontOfSize_14;
        // 设置 Search 按钮可用
        textField.enablesReturnKeyAutomatically = NO;
        
        UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];
        
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
        [button setTitle:@"收起" forState:UIControlStateNormal];
        [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
        [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
        [bar setItems:buttonsArray];
        textField.inputAccessoryView = bar;
        
        
        // 改变输入框背景色
        textField.subviews[0].backgroundColor = [UIColor clearColor];
//        textField.layer.cornerRadius = 1;
//        textField.layer.masksToBounds = YES;
    }else{
        // 去掉搜索框边框
         for(int i =  0 ;i < self.resultSearchBar.subviews.count;i++){
             UIView * backView = self.resultSearchBar.subviews[i];
             if ([backView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                 [backView removeFromSuperview];
                 [self.resultSearchBar setBackgroundColor:[UIColor clearColor]];

                 break;
             }else{
                 NSArray * arr = self.resultSearchBar.subviews[i].subviews;
                 for(int j = 0;j<arr.count;j++   ){
                     UIView * barView = arr[i];
                     if ([barView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {

                         [barView removeFromSuperview];
                         [self.resultSearchBar setBackgroundColor:[UIColor clearColor]];
                         break;
                     }
                 }
             }
         }
        

        // 改变UISearchBar内部输入框样式
        UIView *searchTextField = nil;
        searchTextField = [[[self.resultSearchBar.subviews firstObject] subviews] lastObject];
        // 改变输入框背景色
        searchTextField.subviews[0].backgroundColor = [UIColor clearColor];
//        searchTextField.layer.cornerRadius = 1;
//        searchTextField.layer.masksToBounds = YES;
    }
    

    // 获取 TextField 并设置
    for (UIView *subView in self.resultSearchBar.subviews) {
        for (UIView *textFieldSubView in subView.subviews) {
            if ([textFieldSubView isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)textFieldSubView;

                // 设置 Search 按钮可用
                textField.enablesReturnKeyAutomatically = NO;

                UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];

                UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
                [button setTitle:@"收起" forState:UIControlStateNormal];
                [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
                [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];

                UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

                UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
                NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
                [bar setItems:buttonsArray];
                textField.inputAccessoryView = bar;

                break;
            }
        }
    }
    
//    if (@available(iOS 13.0, *)) {
//
//        UITextField *textField = (UITextField *)self.resultSearchBar.searchTextField;
//
//        textField.font = DHSystemFontOfSize_14;
//        // 设置 Search 按钮可用
//        textField.enablesReturnKeyAutomatically = NO;
//
//        UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];
//
//        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
//        [button setTitle:@"收起" forState:UIControlStateNormal];
//        [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
//
//        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//
//        UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//        NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
//        [bar setItems:buttonsArray];
//        textField.inputAccessoryView = bar;
//
//
//        // 改变输入框背景色
//        textField.subviews[0].backgroundColor = [UIColor clearColor];
//        textField.layer.cornerRadius = 20;
//        textField.layer.masksToBounds = YES;
//    }else{
//        // 改变UISearchBar内部输入框样式
//        UIView *searchTextField = nil;
//        searchTextField = [[[self.resultSearchBar.subviews firstObject] subviews] lastObject];
//        // 改变输入框背景色
//        searchTextField.subviews[0].backgroundColor = [UIColor clearColor];
//        searchTextField.layer.cornerRadius = 5.0;
//
//        // 获取 TextField 并设置
//        for (UIView *subView in self.resultSearchBar.subviews) {
//            for (UIView *textFieldSubView in subView.subviews) {
//                if ([textFieldSubView isKindOfClass:[UITextField class]]) {
//                    UITextField *textField = (UITextField *)textFieldSubView;
//
//                    UIButton * clearButton = [textField valueForKey:@"clearButton"];
//                    [clearButton addTarget:self action:@selector(clearTextClick) forControlEvents:UIControlEventTouchUpInside];
//
//                    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];
//                    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
//                    [button setTitle:@"收起" forState:UIControlStateNormal];
//                    [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
//                    [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
//
//                    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//
//                    UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//                    NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
//                    [bar setItems:buttonsArray];
//                    textField.inputAccessoryView = bar;
//                    break;
//                }
//            }
//        }
//    }
    
    self.backButton = [[UIButton alloc] init];
    self.backButton.titleLabel.font = DHSystemFontOfSize_14;
    [self.backButton setTitle:@"关闭" forState:UIControlStateNormal];
    [self.backButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-8);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.centerY.mas_equalTo(self.resultSearchBar);
    }];
    
    self.selectView = [[UIView alloc] init];
    self.selectView.layer.cornerRadius = 4;
    self.selectView.layer.masksToBounds = YES;
    self.selectView.layer.borderColor = FWViewBackgroundColor_F3F3F3.CGColor;
    self.selectView.layer.borderWidth = 2;
    [self addSubview:self.selectView];
    [self.selectView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(13.5);
        make.right.mas_equalTo(self).mas_offset(-13.5);
        make.height.mas_equalTo(30);
        make.bottom.mas_equalTo(self).mas_offset(-15);
        make.top.mas_equalTo(self.resultSearchBar.mas_bottom).mas_offset(10);
    }];
    
    self.comprehensiveButton = [[UIButton alloc] init];
    self.comprehensiveButton.tag = 90008;
    self.comprehensiveButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.comprehensiveButton.titleLabel.font = DHSystemFontOfSize_12;
    [self.comprehensiveButton setTitle:@"综合" forState:UIControlStateNormal];
    [self.comprehensiveButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    [self.comprehensiveButton addTarget:self action:@selector(selectButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.selectView addSubview:self.comprehensiveButton];
    [self.comprehensiveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(self.selectView);
        make.width.mas_equalTo((SCREEN_WIDTH-30)/3);
    }];
    
    self.verOneView = [[UIView alloc] init];
    self.verOneView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.selectView addSubview:self.verOneView];
    [self.verOneView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.comprehensiveButton.mas_right);
        make.width.mas_equalTo(0.5);
        make.top.mas_equalTo(self.comprehensiveButton).mas_offset(1);
        make.bottom.mas_equalTo(self.comprehensiveButton).mas_offset(-1);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    
    self.idleButton = [[UIButton alloc] init];
    self.idleButton.tag = 90010;
    self.idleButton.backgroundColor = FWViewBackgroundColor_F3F3F3;
    self.idleButton.titleLabel.font = DHSystemFontOfSize_12;
    [self.idleButton setTitle:@"闲置" forState:UIControlStateNormal];
    [self.idleButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
    [self.idleButton addTarget:self action:@selector(selectButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.selectView addSubview:self.idleButton];
    [self.idleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.mas_equalTo(self.selectView);
        make.width.mas_equalTo(self.comprehensiveButton);
    }];
    
    self.verTwoView = [[UIView alloc] init];
    self.verTwoView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.selectView addSubview:self.verTwoView];
    [self.verTwoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.idleButton.mas_left);
        make.width.mas_equalTo(0.5);
        make.top.mas_equalTo(self.idleButton).mas_offset(1);
        make.bottom.mas_equalTo(self.idleButton).mas_offset(-1);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.userButton = [[UIButton alloc] init];
    self.userButton.tag = 90009;
    self.userButton.backgroundColor = FWViewBackgroundColor_F3F3F3;
    self.userButton.titleLabel.font = DHSystemFontOfSize_12;
    [self.userButton setTitle:@"用户" forState:UIControlStateNormal];
    [self.userButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
    [self.userButton addTarget:self action:@selector(selectButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.selectView addSubview:self.userButton];
    [self.userButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.selectView);
        make.width.mas_equalTo(self.comprehensiveButton);
        make.left.mas_equalTo(self.comprehensiveButton.mas_right).mas_offset(1);
        make.right.mas_equalTo(self.idleButton.mas_left).mas_offset(-1);
    }];
}

#pragma mark - > 选择 综合 or 用户
- (void)selectButtonOnClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(selectButtonClick:)]) {
        [self.delegate selectButtonClick:sender];
    }
}

#pragma mark - > 返回
- (void)backButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(backClick)]) {
        [self.delegate backClick];
    }
}


#pragma mark - > searchBar delegate 实时检测
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([self.resultSearchBar.text length] > 20)
    {
        self.resultSearchBar.text = [self.resultSearchBar.text substringToIndex:20];
    }
}

#pragma mark - > 搜索
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar endEditing:YES];
    
    if ([self.delegate respondsToSelector:@selector(searchResultWithText:)]) {
        [self.delegate searchResultWithText:searchBar.text];
    }
}

#pragma mark - > 去掉输入框边框
- (void)removeBorder:(UISearchBar * )searchBar{
    
    if (@available(iOS 13.0, *)) {

    }else{
        for(int i =  0 ;i < searchBar.subviews.count;i++){
            UIView * backView = searchBar.subviews[i];
            if ([backView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                [backView removeFromSuperview];
                [searchBar setBackgroundColor:[UIColor clearColor]];
                
                break;
            }else{
                NSArray * arr = searchBar.subviews[i].subviews;
                for(int j = 0;j<arr.count;j++   ){
                    UIView * barView = arr[i];
                    if ([barView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                        
                        [barView removeFromSuperview];
                        [searchBar setBackgroundColor:[UIColor clearColor]];
                        break;
                    }
                }
            }
        }
    }
}

- (void)clearTextClick{
    
    self.resultSearchBar.text = @"";
    if ([self.delegate respondsToSelector:@selector(selectClearButton)]) {
        [self.delegate selectClearButton];
    }
}

#pragma mark - > 收起键盘
- (void)hideKeyBoard{
    
    [self.resultSearchBar endEditing:YES];
}

@end
