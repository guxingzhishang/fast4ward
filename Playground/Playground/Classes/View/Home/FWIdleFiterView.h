//
//  FWIdleFiterView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/18.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWIdleFiterViewDelegate <NSObject>

/* 是否全新 */
- (void)allNewClick:(NSString *)if_quanxin;
/* 是否包邮 */
- (void)baoyouClick:(NSString *)freight;
/* 是否自提 */
- (void)zitiClick:(NSString *)if_ziti;
/* 综合选择 */
- (void)doneOneClick:(NSString *)order_type;
/* 地域选择 */
- (void)doneTwoProvinceCode:(NSString *)province_code CityCode:(NSString *)city_code CountyCode:(NSString *)county_code ;

@end


@interface FWIdleFiterView : UIView<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>

@property (nonatomic, weak) UIViewController * vc;

/* 标签封面 */
@property (nonatomic, strong) UIImageView * tagsCoverView;

/* 闲置类型 */
@property (nonatomic, strong) UILabel * tagsLabel;

/* 多少次观看 */
@property (nonatomic, strong) UILabel * titleLabel;

/* 描述 */
@property (nonatomic, strong) UILabel * desLabel;

@property (nonatomic, strong) UITextField * zongheTF;
@property (nonatomic, strong) UILabel * zongheLabel;
@property (nonatomic, strong) UIImageView * zongheArrow;

@property (nonatomic, strong) UITextField * areaTF;
@property (nonatomic, strong) UILabel * areaLabel;
@property (nonatomic, strong) UIImageView * areaArrow;

/* 筛选视图 */
@property (nonatomic, strong) UIView * chooseView;
@property (nonatomic, strong) UILabel * chooseLable;

@property (nonatomic, strong) UIButton * allNewButton;
@property (nonatomic, strong) UIButton * zitiButton;
@property (nonatomic, strong) UIButton * baoyouButton;


@property (nonatomic, strong) UIPickerView * zonghePickerView;
@property (nonatomic, strong) UIPickerView * areaPickerView;

@property (nonatomic, strong) NSArray * paixuArray;

@property (nonatomic, strong) NSMutableArray * provinceArray;
@property (nonatomic, strong) NSMutableArray * cityArray;
@property (nonatomic, strong) NSMutableArray * countryArray;

@property (nonatomic, assign) id<FWIdleFiterViewDelegate>delegate;

@property (nonatomic, strong) FWFeedModel * feedModel;

- (void)congifViewWithModel:(id)model;

- (CGFloat)getCurrentViewHeight;

- (CGFloat)getBarViewTopHeight;
@end

NS_ASSUME_NONNULL_END
