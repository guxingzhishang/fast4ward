//
//  FWSearchUserCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/20.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWListCell.h"
#import "FWSearchUserModel.h"

NS_ASSUME_NONNULL_BEGIN

@class FWSearchUserCell;

@protocol FWSearchUserCellDelegate<NSObject>

- (void)followButtonForIndex:(NSInteger)index  withCell:(FWSearchUserCell *)cell;

@end

@interface FWSearchUserCell : FWListCell

@property (nonatomic, strong) UIButton * followButton;

@property (nonatomic, strong) FWMineInfoModel * userModel;

@property (nonatomic, weak) id<FWSearchUserCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
