//
//  FWIdleCollectionCell.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/17.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWIdleCollectionCell.h"

@implementation FWIdleCollectionCell

+ (NSString *)cellIdentifier {
    return @"FWIdleCollectionCell";
}

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath {
    FWIdleCollectionCell *cell = (FWIdleCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:[FWIdleCollectionCell cellIdentifier] forIndexPath:indexPath];
    return cell;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}
@end
