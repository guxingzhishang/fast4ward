//
//  FWRecommendCollectionCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/2.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWRecommendCollectionCell.h"

@implementation FWRecommendCollectionCell

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupviews];
    }
    return self;
}

- (void)setupviews{
        
    self.liveImageView = [[UIImageView alloc] init];
    self.liveImageView.image = [UIImage imageNamed:@"matchDefaultBG"];
    self.liveImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.liveImageView.layer.cornerRadius = 5;
    self.liveImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.liveImageView];
    [self.liveImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
    self.liveImageView.hidden = YES;
}

- (void)configWithFinalModel:(id)bannerModel WithModel:(id)dataModel isShow:(BOOL)isShow{
    
    self.liveImageView.hidden = !isShow;

    if (dataModel) {
        [super configForCell:dataModel];
    }
    
    if (isShow) {
        self.videoIcon.hidden = YES;
        self.siIcon.hidden = YES;
        self.iv.hidden = YES;
        self.activityImageView.hidden = YES;
        self.jingpinImageView.hidden = YES;
        self.userIcon.hidden = YES;
        self.pvImageView.hidden = YES;
        self.username.hidden = YES;
        self.pvLabel.hidden = YES;
        self.desc.hidden = YES;
        self.userButton.hidden = YES;
        self.authenticationImageView.hidden = YES;

        if (bannerModel) {

            FWFeedModel * model = (FWFeedModel *)bannerModel;
            
            [self.liveImageView sd_setImageWithURL:[NSURL URLWithString:model.banner_img_url] placeholderImage:[UIImage imageNamed:@"matchDefaultBG"]];
        }
    }
}


@end
