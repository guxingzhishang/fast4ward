//
//  FWCommentFooterView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/17.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWCommentFooterView.h"

@implementation FWCommentFooterView
@synthesize showButton;
@synthesize tipLabel;

+ (instancetype)topicFooterView
{
    return [[self alloc] init];
}

+ (instancetype)footerViewWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"TopicFooter";
    FWCommentFooterView *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if (footer == nil) {
        // 缓存池中没有, 自己创建
        footer = [[self alloc] initWithReuseIdentifier:ID];
    }
    return footer;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.userInteractionEnabled = YES;
        self.contentView.userInteractionEnabled = YES;
        
        // 创建自控制器
        [self _setupSubViews];
    }
    return self;
}

- (void)_setupSubViews{

    showButton = [[UIButton alloc] init];
    showButton.backgroundColor = FWClearColor;
    [showButton addTarget:self action:@selector(showButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:showButton];
    showButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    
    tipLabel = [[UILabel alloc] init];
    tipLabel.font = DHSystemFontOfSize_12;
    tipLabel.textColor = FWTextColor_969696;
    tipLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:tipLabel];
    tipLabel.frame = CGRectMake(110, 0, 150, CGRectGetHeight(showButton.frame));
}

- (void)showButtonOnClick:(UIButton *)sender{
    
    NSInteger section = sender.tag - 44444;
    
    if ([self.delegate respondsToSelector:@selector(showButtonOnClickWithIndex:WithFooter:)]) {
        [self.delegate showButtonOnClickWithIndex:section WithFooter:self];
    }
}

@end
