//
//  FWHomeBussinessCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWHomeBussinessCell : UITableViewCell

@property (nonatomic, strong) UIImageView * bgView;
@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UIImageView * statusImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * areaLabel;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * authenticationView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIButton * memberButton;
@property (nonatomic, strong) UIView * bussinessView; // 主营业务

@property (nonatomic, weak) UIViewController * vc;


@property (nonatomic, strong) FWHomeShopListModel * shopListModel;

- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
