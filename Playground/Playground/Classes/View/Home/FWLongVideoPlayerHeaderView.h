//
//  FWLongVideoPlayerHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWLongVideoPlayerHeaderViewDelegate <NSObject>

- (void)attentionButtonClick;
- (void)backButtonClick;

@end

@interface FWLongVideoPlayerHeaderView : UIView<UIGestureRecognizerDelegate>

/**
 * 返回按钮
 */
@property (nonatomic, strong) UIButton * backButton;

/**
 * 头像
 */
@property (nonatomic, strong) UIImageView * photoImageView;

/**
 * 关注按钮
 */
@property (nonatomic, strong) UIButton * attentionButton;


/**
 * 名字
 */
@property (nonatomic, strong) UILabel * nameLabel;

/**
 * vip图标
 */
@property (nonatomic, strong) UIButton * vipImageButton;

/**
 * 内容
 */
@property (nonatomic, strong) UILabel * contentLabel;

/**
 * 承载标签的视图
 */
@property (nonatomic, strong) UIView * containerView;

/**
 * 评论两个字
 */
@property (nonatomic, strong) UILabel * commentLabel;

/**
 * 评论数
 */
@property (nonatomic, strong) UIButton * commentNumberButton;

/* 日期 */
@property (nonatomic, strong) UILabel * timeLabel;

/* 商品背景图 */
@property (nonatomic, strong) UIView * goodsView;
/* 推荐商品 */
@property (nonatomic, strong) UILabel * recammondLabel;
/* 商品展示图 */
@property (nonatomic, strong) UIImageView * goodsImageView;
/* 商品名称 */
@property (nonatomic, strong) UILabel * goodsTitleLabel;
/* 商品价格 */
@property (nonatomic, strong) UILabel * goodsPriceLabel;
/* 商品浏览图片 */
@property (nonatomic, strong) UIImageView * goodsScanView;
/* 商品浏览数量 */
@property (nonatomic, strong) UILabel * goodsScanLabel;

@property (nonatomic, strong) UIView * goodsLineView;

@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) UIViewController *delegate;

@property (nonatomic, strong) NSMutableArray * containerArr;

@property (nonatomic, weak) id<FWLongVideoPlayerHeaderViewDelegate> headerDelegate;

@property (nonatomic, assign) CGFloat containerHeight;

@property (nonatomic, strong) FWFeedListModel * listModel;

- (void)configViewForModel:(id)model;

- (CGFloat)getCurrentViewHeight;

@end

NS_ASSUME_NONNULL_END
