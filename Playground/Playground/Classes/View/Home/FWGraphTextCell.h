//
//  FWGraphTextCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWModuleButton.h"

@class FWGraphTextCell;

@protocol FWGraphTextCellDelegate <NSObject>

- (void)checkAllInfoDelegate:(FWGraphTextCell *)cell;
- (void)showAllButtonClick:(NSInteger)index WithView:(FWGraphTextCell *)cell;
- (void)likesButtonClick:(NSInteger)index WithView:(FWGraphTextCell *)cell;
- (void)commentButtonClick:(NSInteger)index;
- (void)shareButtonClick:(NSInteger)index;
- (void)tagsButtonClick:(NSInteger)index;
- (void)picImageClick:(NSInteger)index;

@end

@interface FWGraphTextCell : UITableViewCell<UIGestureRecognizerDelegate>

/**
 * cell的代理
 */
@property (nonatomic, weak) id<FWGraphTextCellDelegate> delegate;

/**
 * 头像
 */
@property (nonatomic, strong) UIImageView * photoImageView;

/**
 * 关注按钮
 */
@property (nonatomic, strong) UIButton * attentionButton;

/**
 * 名字
 */
@property (nonatomic, strong) UILabel * nameLabel;

/* vip图标 */
@property (nonatomic, strong) UIButton *vipImageButton;

/**
 * 内容
 */
@property (nonatomic, strong) UILabel * contentLabel;

/**
 * 显示全部按钮
 */
@property (nonatomic, strong) UIButton * showAllButton;

/**
 * 承载标签的视图
 */
@property (nonatomic, strong) UIView * containerView;

/**
 * 正文图片按钮
 */
@property (nonatomic, strong) UIButton * picImageButton;

/**
 * 正文图片
 */
@property (nonatomic, strong) UIImageView * picImageView;

/**
 * 更多
 */
@property (nonatomic, strong) UIView * moreView;

/**
 * 图集（有多张图片）
 */
@property (nonatomic, strong) UIImageView * moreImageView;

/**
 *一共有几张图
 */
@property (nonatomic, strong) UILabel * moreLabel;

/**
 * 占位图
 */
@property (strong, nonatomic)  UIImageView *siIcon;

/* 商家店铺 */
@property (nonatomic, strong) FWModuleButton *shopButton;

/**
 * 时间
 */
@property (nonatomic, strong) UILabel * timeLabel;

/**
 * 喜欢按钮
 */
@property (nonatomic, strong) UIButton * likesButton;

/**
 * 喜欢图标
 */
@property (nonatomic, strong) UIImageView * likesImageView;

/**
 * 喜欢数
 */
@property (nonatomic, strong) UILabel * likesLabel;

/**
 * 评论按钮
 */
@property (nonatomic, strong) UIButton * commentButton;

/**
 * 评论图标
 */
@property (nonatomic, strong) UIImageView * commentImageView;

/**
 * 评论数
 */
@property (nonatomic, strong) UILabel * commentLabel;

/**
 * 分享按钮
 */
@property (nonatomic, strong) UIButton * shareButton;

/**
 * 分享图标
 */
@property (nonatomic, strong) UIImageView * shareImageView;

/**
 * 分享数
 */
@property (nonatomic, strong) UILabel * shareLabel;

@property (nonatomic, strong) UIView * lineView ;

/**
 * 标签数组
 */
@property (nonatomic, strong) NSMutableArray * containerArr;

/**
 * 承载该视图的控制器
 */
@property (nonatomic, weak) UIViewController * viewController;

/**
 * 数据模型
 */
@property (nonatomic, strong) FWFeedListModel * listModel;

/**
 * 保存上一页面传来的第几行
 */
@property (nonatomic, assign) NSInteger rowIndex;

/**
 * 点赞的状态
 */
@property (nonatomic, strong) NSString * likeType;

@property (nonatomic, assign) BOOL isShow;

@property (nonatomic, assign) CGFloat  containerHeight;

- (void)configCellForModel:(id)model WithIndex:(NSInteger)index;

@end
