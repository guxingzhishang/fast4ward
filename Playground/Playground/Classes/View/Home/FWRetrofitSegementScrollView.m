//
//  FWRetrofitSegementScrollView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRetrofitSegementScrollView.h"
#import "FWRefrofitFeelingModel.h"

@implementation FWRetrofitSegementScrollView
@synthesize shadowView;

- (CGFloat)menuHeight{
    
    if (_menuHeight == 0) {
        _menuHeight = 50;
    }
    return _menuHeight;
}

- (NSInteger)pageNumberOfItem{
    
    if (_pageNumberOfItem == 0) {
        _pageNumberOfItem = (self.titles.count > 5 ? 5 : self.titles.count);
    }
    return _pageNumberOfItem;
}

- (NSMutableArray *)labelArys{
    
    if (!_labelArys) {
        _labelArys = [NSMutableArray array];
    }
    return _labelArys;
}

- (NSMutableArray *)rectXArray{
    
    if (!_rectXArray) {
        _rectXArray = [NSMutableArray array];
    }
    return _rectXArray;
}

- (id)init{
    
    self = [super init];
    if (self) {
        self.titles = @[].copy;
        
        self.showsHorizontalScrollIndicator = NO;
        self.delegate = self;
        
        self.titleFont = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        self.titleColor = FWTextColor_B6BCC4;
        
        self.titleSelectFont = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        self.titleSelectColor = FWTextColor_626D8F;
        
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        self.lastIndex = 0;
    }
    return self;
}

#pragma mark - 加载顶部视图数据 -
- (void)deliverTitles:(NSArray *)array{
    //标记label的最大Y坐标
    CGFloat preY = 0;
    self.titles = array;
    
    if (self.labelArys.count>0) {
        [self.labelArys removeAllObjects];
    }
    
    for (int i = 0; i < self.titles.count; i++) {
        
        FWRefrofitFeelingCatListModel * listModel = self.titles[i];
        
        UILabel *item = [[UILabel alloc] init];
        item.frame = CGRectMake(24,(self.menuHeight +10) *i,72, self.menuHeight);
        item.text = listModel.cat_name;
        item.font = self.titleFont;
        item.textColor = self.titleColor;
        item.backgroundColor = FWClearColor;
        item.textAlignment = NSTextAlignmentLeft;
        item.tag = 10000 + i;
        [self addSubview:item];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClickTap:)];
        item.userInteractionEnabled = YES;
        [item addGestureRecognizer:tap];
        [self.labelArys addObject:item];
        
        preY = CGRectGetMaxY(item.frame);
        
        [self.rectXArray addObject:@(preY)];
    }
    self.contentSize = CGSizeMake(70, preY);
    
    UILabel * itemLabel = (UILabel *)[self viewWithTag:10000];

    shadowView= [[UIView alloc]init];
    shadowView.frame = CGRectMake(10,CGRectGetMinY(itemLabel.frame)+17, 5, 15);
    shadowView.backgroundColor = FWTextColor_222222;
    shadowView.layer.cornerRadius = 2;
    shadowView.layer.masksToBounds = YES;
    shadowView.tag = 100000;
    [self addSubview:shadowView];

    [self menuUpdateStyle:0];
}

#pragma mark - 菜单点击回调 -
- (void)itemClickTap:(UIGestureRecognizer *)sender{
    NSInteger index = sender.view.tag - 10000;
    
    if (self.lastIndex != index) {
        [self menuUpdateStyle:index];
        [self menuScrollToCenter:index];
        
        self.lastIndex = index;
        
        if ([self.itemDelegate respondsToSelector:@selector(segementItemClickTap:)]) {
            [self.itemDelegate segementItemClickTap:index];
        }
    }
}


#pragma mark - > 顶部菜单滚动
- (void)menuScrollToCenter:(NSInteger)index{
    
    UILabel * itemLabel = (UILabel *)[self viewWithTag:10000+index];

    shadowView.frame = CGRectMake(10,CGRectGetMinY(itemLabel.frame)+17, 5, 15);
}

#pragma mark - > 顶部菜单切换样式
- (void)menuUpdateStyle:(NSInteger)index{
    
    if (self.labelArys.count <= 0) {
        return;
    }
    UILabel *lastLabel = self.labelArys[self.lastIndex];
    lastLabel.font = self.titleFont;
    lastLabel.textColor = self.titleColor;
    
    UILabel *label = self.labelArys[index];
    label.textColor = self.titleSelectColor;
    label.font = self.titleSelectFont;
}

- (void)clearSubViews{
    
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
}




@end
