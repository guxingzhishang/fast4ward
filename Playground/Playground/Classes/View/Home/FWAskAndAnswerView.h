//
//  FWAskAndAnswerView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWAskAndAnswerModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWAskAndAnswerViewDelegate <NSObject>

- (void)changeSortWithType:(NSString *)sortType;

@end

@interface FWAskAndAnswerView : UIView<UIScrollViewDelegate>

@property (nonatomic, strong) UILabel * darenLabel;

@property (nonatomic, strong) UIScrollView * darenScrollView;

@property (nonatomic, strong) UILabel * typeLabel;

@property (nonatomic, strong) UIButton * changeButton;

@property (nonatomic, strong) FWAskAndAnswerModel * wendaModel;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, assign) BOOL  isTimeSort;// yes 时间排序，NO 热度排序

@property (nonatomic, weak) id<FWAskAndAnswerViewDelegate>ankAndAnswerDelegate;


- (void)congifViewWithModel:(id)model;

- (CGFloat)getCurrentViewHeight;
- (CGFloat)getBarViewTopHeight;

@end

NS_ASSUME_NONNULL_END
