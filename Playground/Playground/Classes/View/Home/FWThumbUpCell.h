//
//  FWThumbUpCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 首页 - > 消息 - > 点赞 & 评论
 */

#import "FWUnionMessageModel.h"

@interface FWThumbUpCell : FWListCell

/**
 * 时间
 */
@property (nonatomic, strong) UILabel * timeLabel;

/**
 * 未读
 */
@property (nonatomic, strong) UIImageView * unreadView;

@property (nonatomic, assign) FWUnionMessageModel * messageModel;

@end
