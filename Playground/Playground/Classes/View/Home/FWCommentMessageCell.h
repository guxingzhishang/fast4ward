//
//  FWCommentMessageCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWListCell.h"
#import "FWUnionMessageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCommentMessageCell : FWListCell

@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UILabel * timeLabel;

@property (nonatomic, strong) UIImageView * typeImageView;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, assign) FWUnionMessageModel * messageModel;

@end

NS_ASSUME_NONNULL_END
