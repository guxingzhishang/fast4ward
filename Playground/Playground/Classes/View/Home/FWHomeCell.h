//
//  FWHomeCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWModuleButton.h"
#import "FWHomeCellBannerView.h"

NS_ASSUME_NONNULL_BEGIN

@class FWHomeCell;

@protocol FWHomeCellDelegate <NSObject>

- (void)picImageClick:(NSInteger)index;

@end

@interface FWHomeCell : UITableViewCell<ShareViewDelegate,UIGestureRecognizerDelegate,SDCycleScrollViewDelegate,FWHomeCellBannerViewDelegate>

/**
 * cell的代理
 */
@property (nonatomic, weak) id<FWHomeCellDelegate> delegate;

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIImageView * videoImageView;

@property (nonatomic, strong) UILabel * priceLabel;

/* 头像 */
@property (nonatomic, strong) UIImageView * photoImageView;

/* 关注按钮 */
@property (nonatomic, strong) UIButton * attentionButton;

/* 名字 */
@property (nonatomic, strong) UILabel * nameLabel;

/* 更多按钮 */
@property (nonatomic, strong) UIButton * rightButton;

/* 认证 */
@property (nonatomic, strong) UIImageView * authenticationView;

/* 认证 */
@property (nonatomic, strong) UILabel * authenticationLabel;

/* vip图标 */
@property (nonatomic, strong) UIButton *vipImageButton;

/* 内容 */
@property (nonatomic, strong) UILabel * contentLabel;

/* 文章图标 */
@property (nonatomic, strong) UIImageView * articalIconView;

/* 案例 */
@property (nonatomic, strong) UILabel * anliLabel;

/* 标签 */
@property (nonatomic, strong) FWModuleButton * tagsButton;

/* 正文图片 */
@property (nonatomic, strong) FWHomeCellBannerView * bannerImageView;

/* 视频时间 */
@property (nonatomic, strong) UILabel * durationLabel;

/* 占位图 */
@property (strong, nonatomic)  UIImageView *siIcon;

/* 商家店铺 */
@property (nonatomic, strong) FWModuleButton *shopButton;

/* 时间 */
@property (nonatomic, strong) UILabel * timeLabel;

/* 精品贴 */
@property (nonatomic, strong) UIImageView * jingpinView;

/* 喜欢按钮 */
@property (nonatomic, strong) UIButton * likesButton;

/* 喜欢图标 */
@property (nonatomic, strong) UIImageView * likesImageView;

/* 喜欢数 */
@property (nonatomic, strong) UILabel * likesLabel;

/* 评论按钮 */
@property (nonatomic, strong) UIButton * commentButton;

/* 评论图标 */
@property (nonatomic, strong) UIImageView * commentImageView;

/* 评论数 */
@property (nonatomic, strong) UILabel * commentLabel;

/* 分享按钮 */
@property (nonatomic, strong) UIButton * shareButton;

/* 分享图标 */
@property (nonatomic, strong) UIImageView * shareImageView;

/* 分享数 */
@property (nonatomic, strong) UILabel * shareLabel;

/* 标签数组 */
@property (nonatomic, strong) NSMutableArray * containerArr;

/* 承载该视图的控制器 */
@property (nonatomic, weak) UIViewController * vc;

/* 数据模型 */
@property (nonatomic, strong) FWFeedListModel * listModel;

/* 保存上一页面传来的第几行 */
@property (nonatomic, assign) NSInteger rowIndex;

/* 点赞的状态 */
@property (nonatomic, strong) NSString * likeType;

@property (nonatomic, assign) BOOL isShow;

@property (nonatomic, assign) CGFloat  containerHeight;

@property (nonatomic, strong) NSString * secondString;


@end

NS_ASSUME_NONNULL_END
