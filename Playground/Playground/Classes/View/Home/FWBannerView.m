//
//  FWBannerView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBannerView.h"

@interface FWBannerView()

@property (nonatomic, strong) NSMutableArray<NSString *> *bannerImagesURLArray;// 首页banner图片URL数组

@end

@implementation FWBannerView


- (instancetype)initWithDelegate:(id)delegate {
    
    return [self initWithCarouselDelegate:delegate];
}

- (instancetype)initWithCarouselDelegate:(UIViewController *)delegate
{
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, DHAdaptive(380));
    
    self = [[FWBannerView alloc] init];
    self.backgroundColor = [UIColor whiteColor];
    
    [self createBannerWithDataSource:nil delegate:delegate];
    
    return self;
}

/**
 *  创建一个新的无线轮播图
 */
- (void)createBannerWithDataSource:(FWHomeViewModel *)model delegate:(id<SDCycleScrollViewDelegate>)delegate{
    
    NSArray * bannerArr = @[@"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1539246574&di=5aca1d50654104931b36a5689b03715b&imgtype=jpg&er=1&src=http%3A%2F%2F00imgmini.eastday.com%2Fmobile%2F20180914%2F20180914165719_c8ff1a6fc253c866e6a6d6705ba6e8f4_5.jpeg",
                            @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1539266679&di=1f9d856dcc6b668592e2664c9c183d50&imgtype=jpg&er=1&src=http%3A%2F%2Fg.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2F3801213fb80e7bec4ac4577b2f2eb9389b506b09.jpg",
                            @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1539266726&di=cfd98b0c68e418a797ea6e22c90c8e40&imgtype=jpg&er=1&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201411%2F30%2F20141130170041_QBJUw.jpeg"];
    
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, DHAdaptive(380));
    
    // 先清除原先的Banner数据
    [self.bannerImagesURLArray removeAllObjects];
    [self.homeBanner removeFromSuperview];
    
    // 获得新的数据
    [bannerArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.bannerImagesURLArray addObject:obj];
    }];
    
    
    // 创建新的Banner
    CGFloat bannerH = DHAdaptive(380);// 轮播图的适配高度
    CGRect bannerRect = CGRectMake(0, 0, SCREEN_WIDTH, bannerH);
    self.homeBanner = [SDCycleScrollView cycleScrollViewWithFrame:bannerRect
                                                         delegate:delegate
                                                 placeholderImage:[UIImage imageNamed:@"首页占位图"]];
    self.homeBanner.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    self.homeBanner.infiniteLoop = YES;
    self.homeBanner.imageURLStringsGroup = self.bannerImagesURLArray;
    [self addSubview:self.homeBanner];
//    [DHGlobalVariable sharedManager].homeBanner = self.homeBanner;
    
//    if ([DHGlobalVariable sharedManager].isADDidFinishDisplay == YES &&
//        self.bannerImagesURLArray.count > 1)// 营销也展示结束后的banner都为自动滚动(并且图片大于1张)
    
    if (self.bannerImagesURLArray.count > 1){
        self.homeBanner.autoScroll = YES;
    }else{
        self.homeBanner.autoScroll = NO;
    }
    
    [self intrinsicContentSize];
    [self layoutIfNeeded];
}

- (NSMutableArray<NSString *> *)bannerImagesURLArray
{
    if (_bannerImagesURLArray == nil)
    {
        _bannerImagesURLArray = @[].mutableCopy;
    }
    return _bannerImagesURLArray;
}

- (CGSize)intrinsicContentSize {
    
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, DHAdaptive(380));
    //    [self setNeedsUpdateConstraints];
    [self setNeedsLayout];
    return self.frame.size;
}



@end
