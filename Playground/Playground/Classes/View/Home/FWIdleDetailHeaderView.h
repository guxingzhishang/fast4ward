//
//  FWIdleDetailHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/17.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWBannerView.h"
#import "SDCycleScrollView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWIdleDetailHeaderViewDelegate <NSObject>

- (void)attentionButtonClick;
- (void)backButtonClick;
- (void)commentSort:(NSString *)sortType;

@end

@interface FWIdleDetailHeaderView : UIView<SDCycleScrollViewDelegate,UIGestureRecognizerDelegate>

/**
 * 轮播图
 */
@property (nonatomic, strong) SDCycleScrollView * bannerView;

/**
 * 标题
 */
@property (nonatomic, strong) UILabel * titleLabel;

/* 闲置物品标签，自提、包邮、全新 */
@property (nonatomic, strong) UIView * goodsTagsView;

@property (nonatomic, strong) UILabel * priceLabel;

@property (nonatomic, strong) UILabel * originalPriceLabel;

@property (nonatomic, strong) UILabel * areaLabel;

@property (nonatomic, strong) UILabel * youfeiLabel;

@property (nonatomic, strong) UILabel * carTypeLabel;

/**
 * 内容
 */
@property (nonatomic, strong) UILabel * contentLabel;

/**
 * 承载标签的视图
 */
@property (nonatomic, strong) UIView * containerView;


/**
 * 评论两个字
 */
@property (nonatomic, strong) UILabel * commentLabel;

/**
 * 评论数
 */
@property (nonatomic, strong) UIButton * commentNumberButton;

/* 日期 */
@property (nonatomic, strong) UILabel * timeLabel;

/* 商品背景图 */
@property (nonatomic, strong) UIView * goodsView;
/* 推荐商品 */
@property (nonatomic, strong) UILabel * recammondLabel;
/* 商品展示图 */
@property (nonatomic, strong) UIImageView * goodsImageView;
/* 商品名称 */
@property (nonatomic, strong) UILabel * goodsTitleLabel;
/* 商品价格 */
@property (nonatomic, strong) UILabel * goodsPriceLabel;
/* 商品浏览图片 */
@property (nonatomic, strong) UIImageView * goodsScanView;
/* 商品浏览数量 */
@property (nonatomic, strong) UILabel * goodsScanLabel;

@property (nonatomic, strong) UIView * goodsLineView;

@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) UIViewController *delegate;

@property (nonatomic, strong) NSMutableArray * containerArr;

@property (nonatomic, weak) id<FWAskAndAnswerDetailHeaderViewDelegate> headerDelegate;

@property (nonatomic, assign) CGFloat containerHeight;

@property (nonatomic, strong) FWFeedListModel * listModel;

@property (nonatomic, strong) NSString * sortType;

@property (nonatomic, strong) NSMutableArray * tagsNameArray;

- (void)configViewForModel:(id)model;

- (CGFloat)getCurrentViewHeight;


@end

NS_ASSUME_NONNULL_END
