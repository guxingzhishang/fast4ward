//
//  FWHomeBussinessCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeBussinessCell.h"

@implementation FWHomeBussinessCell
@synthesize shopListModel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.contentView.backgroundColor = FWTextColor_F8F8F8;
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.bgView = [[UIImageView alloc] init];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_greaterThanOrEqualTo(60);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-0);
    }];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.layer.cornerRadius = 5;
    self.iconImageView.layer.masksToBounds = YES;
    self.iconImageView.image = [UIImage imageNamed:@"bussiness_placeholder"];
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.bgView addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(90, 90));
        make.top.left.mas_equalTo(self.bgView).mas_offset(10);
    }];
    
    self.statusImageView = [UIImageView new];
    self.statusImageView.image = [UIImage imageNamed:@"home_bussiness_renzheng"];
    [self.iconImageView addSubview:self.statusImageView];
    [self.statusImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconImageView).mas_offset(10);
        make.left.mas_equalTo(self.iconImageView).mas_offset(0);
        make.width.mas_equalTo(41);
        make.height.mas_equalTo(16);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    self.titleLabel.textColor = FWTextColor_272727;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconImageView);
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(10);
        make.right.mas_equalTo(self.bgView).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(150);
        make.height.mas_equalTo(20);
    }];

    self.areaLabel = [[UILabel alloc] init];
    self.areaLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.areaLabel.textColor = FWTextColor_B6BCC4;
    self.areaLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.areaLabel];
    [self.areaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(5);
        make.left.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self.titleLabel);
        make.width.mas_greaterThanOrEqualTo(200);
        make.height.mas_equalTo(16);
    }];
    
    self.photoImageView = [UIImageView new];
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    self.photoImageView.layer.cornerRadius = 30/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.bgView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel).mas_offset(0);
        make.top.mas_equalTo(self.areaLabel.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(30);
    }];
    
    self.authenticationView = [UIImageView new];
    [self.bgView addSubview:self.authenticationView];
    [self.authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.photoImageView).mas_offset(2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(2);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.nameLabel.textColor = FWTextColor_272727;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-210);
        make.height.mas_equalTo(20);
    }];
    
    self.memberButton = [[UIButton alloc] init];
    [self.memberButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.memberButton];
    [self.memberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.nameLabel).mas_offset(-1);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
    
    self.bussinessView = [[UIView alloc] init];
    [self.bgView addSubview:self.bussinessView];
    [self.bussinessView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-2);
        make.width.mas_equalTo(SCREEN_WIDTH-42);
        make.height.mas_equalTo(1);
    }];
}

- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

- (void) refreshContainer{
    
    [self.bussinessView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    if (shopListModel.main_business.count > 0 ) {
        
        CGFloat x = 10;
        CGFloat width = 0.0;
        CGFloat height = 20;
        
        for (int i = 0; i < shopListModel.main_business.count; i++) {
            
            NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc] initWithString:shopListModel.main_business[i]];
            CGSize  textLimitSize = CGSizeMake(MAXFLOAT, height);
            width = [YYTextLayout layoutWithContainerSize:textLimitSize text:attribute].textBoundingSize.width+10;
            
            if (x + width+5+20 >= SCREEN_WIDTH) {
                continue;
            }
            
            UILabel * titleLable = [[UILabel alloc] init];
            titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
            titleLable.textAlignment = NSTextAlignmentCenter;
            titleLable.text = [NSString stringWithFormat:@"%@",shopListModel.main_business[i]];
            titleLable.textColor = FWTextColor_B6BCC4;
            titleLable.backgroundColor = FWViewBackgroundColor_FFFFFF;
            titleLable.layer.cornerRadius = 3;
            titleLable.layer.masksToBounds = YES;
            titleLable.layer.borderColor = FWTextColor_B6BCC4.CGColor;
            titleLable.layer.borderWidth = 0.6;
            [self.bussinessView addSubview:titleLable];
            titleLable.frame = CGRectMake(x,0, width, height);

            x += width+5;
        }
        
        [self.bussinessView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).mas_offset(14);
            make.right.mas_equalTo(self.contentView).mas_offset(-2);
            make.width.mas_equalTo(SCREEN_WIDTH-42);
            make.height.mas_equalTo(20);
            make.top.mas_equalTo(self.iconImageView.mas_bottom).mas_offset(10);
            make.bottom.mas_equalTo(self.bgView).mas_offset(-10);
        }];
    }else{
        [self.bussinessView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).mas_offset(14);
            make.right.mas_equalTo(self.contentView).mas_offset(-2);
            make.width.mas_equalTo(SCREEN_WIDTH-42);
            make.height.mas_equalTo(1);
            make.top.mas_equalTo(self.iconImageView.mas_bottom).mas_offset(10);
            make.bottom.mas_equalTo(self.contentView).mas_offset(-0);
        }];
    }
}

- (void)configForCell:(id)model{
    
    shopListModel = (FWHomeShopListModel *)model;
    
    self.titleLabel.text = shopListModel.user_info.cert_name;
    self.nameLabel.text = shopListModel.user_info.nickname;
    
    if (!shopListModel.province_name) {
        shopListModel.province_name = @"";
    }
    if (!shopListModel.city_name) {
        shopListModel.city_name = @"";
    }
    if (!shopListModel.area_name) {
        shopListModel.area_name = @"";
    }
    self.areaLabel.text = [NSString stringWithFormat:@"%@%@%@",shopListModel.province_name,shopListModel.city_name,shopListModel.area_name];
    
    [self refreshContainer];
    
    
    if ([shopListModel.user_info.cert_status isEqualToString:@"2"]) {
        /* 有标签 */
        self.statusImageView.hidden = NO;
    }else{
        /* 没有标签 */
        self.statusImageView.hidden = YES;
    }
    
    //vip图标
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-210);
        make.height.mas_equalTo(20);
    }];
    
    self.memberButton.enabled = YES;
    if ([shopListModel.user_info.user_level isEqualToString:@"1"]) {
        [self.memberButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([shopListModel.user_info.user_level isEqualToString:@"2"]) {
        [self.memberButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([shopListModel.user_info.user_level isEqualToString:@"3"]) {
        [self.memberButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.memberButton.enabled = NO;

        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.photoImageView);
            make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
            make.right.mas_equalTo(self.bgView);
            make.width.mas_greaterThanOrEqualTo(100);
            make.height.mas_equalTo(20);
        }];
        [self.memberButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }

    //头像
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:shopListModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];

    //图片
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:shopListModel.cover] placeholderImage:[UIImage imageNamed:@"bussiness_placeholder"]];

    // 认证身份
    if([shopListModel.user_info.cert_status isEqualToString:@"2"]) {
        
        self.authenticationView.hidden = NO;
        self.authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        self.authenticationView.hidden = YES;
    }
}

@end
