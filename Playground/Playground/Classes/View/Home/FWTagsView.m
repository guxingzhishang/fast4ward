//
//  FWTagsView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWTagsView.h"
#import "FWTagsFeedModel.h"

@interface FWTagsView ()

@property (nonatomic, strong) FWFeedModel * subModel;


@end

@implementation FWTagsView
@synthesize tagsLabel;
@synthesize tagsCoverView;
@synthesize nameLabel;
@synthesize nameButton;
@synthesize photoImageView;
@synthesize arrowImageView;
@synthesize desLabel;
@synthesize numberScan;
@synthesize attentionButton;
@synthesize barView;
@synthesize showAllButton;
@synthesize changeButton;
@synthesize typeLabel;
@synthesize isTimeSort;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        isTimeSort = YES;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    tagsCoverView = [[UIImageView alloc] init];
    tagsCoverView.layer.borderColor = [UIColor colorWithHexString:@"eeeeee" alpha:1].CGColor;
    tagsCoverView.contentMode = UIViewContentModeScaleAspectFill;
    tagsCoverView.layer.borderWidth = 0.5;
    tagsCoverView.layer.cornerRadius = 2;
    tagsCoverView.layer.masksToBounds = YES;
    [self addSubview:tagsCoverView];
    [tagsCoverView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(self).mas_offset(20);
        make.height.width.mas_equalTo(140);
    }];
    
    tagsLabel = [[UILabel alloc] init];
    tagsLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    tagsLabel.textColor = FWTextColor_272727;
    tagsLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:tagsLabel];
    [tagsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(tagsCoverView.mas_right).mas_offset(17);
        make.height.mas_equalTo(28);
        make.top.mas_equalTo(tagsCoverView);
        make.width.mas_greaterThanOrEqualTo(20);
        make.right.mas_equalTo(self).mas_offset(-5);
    }];
    
//    photoImageView = [[UIImageView alloc] init];
//    photoImageView.layer.cornerRadius = 9;
//    photoImageView.layer.masksToBounds = YES;
//    [self addSubview:photoImageView];
//    [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(tagsLabel.mas_bottom).mas_offset(10);
//        make.size.mas_equalTo(CGSizeMake(18, 18));
//    }];
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.font =  [UIFont fontWithName:@"PingFangSC-Medium" size:13];;
    nameLabel.textColor = FWTextColor_525467;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tagsLabel.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(tagsLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = FWImage(@"right_arrow");
    [self addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel.mas_right).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(5, 8));
        make.centerY.mas_equalTo(nameLabel);
    }];
    
    nameButton = [[UIButton alloc] init];
    [nameButton addTarget:self action:@selector(nameButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:nameButton];
    [nameButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel);
        make.right.mas_equalTo(arrowImageView);
        make.height.mas_equalTo(40);
        make.centerY.mas_equalTo(nameLabel);
    }];
    
    numberScan = [[UILabel alloc] init];
    numberScan.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 13];
    numberScan.textColor = FWTextColor_9EA3AB;
    numberScan.textAlignment = NSTextAlignmentLeft;
    [self addSubview:numberScan];
    [numberScan mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel);
        make.height.mas_equalTo(17);
        make.top.mas_equalTo(nameButton.mas_bottom);
    }];

    attentionButton = [[UIButton alloc] init];
    attentionButton.layer.cornerRadius = 2;
    attentionButton.layer.masksToBounds = YES;
    attentionButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Medium" size:13];;
    attentionButton.backgroundColor = FWTextColor_222222;
    [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [attentionButton addTarget:self action:@selector(attentionButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:attentionButton];
    [attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(numberScan);
        make.size.mas_equalTo(CGSizeMake(90, 30));
        make.bottom.mas_equalTo(tagsCoverView);
    }];
    
    desLabel = [[UILabel alloc] init];
    desLabel.lineBreakMode = NSLineBreakByWordWrapping;
    desLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 13];
    desLabel.textColor = FWTextColor_9EA3AB;
    desLabel.textAlignment = NSTextAlignmentLeft;
    desLabel.numberOfLines = 2;
    [self addSubview:desLabel];
    desLabel.frame = CGRectMake(20, 178, SCREEN_WIDTH-30, 10);

    showAllButton = [[UIButton alloc] init];
    [showAllButton setImage:[UIImage imageNamed:@"show_down"] forState:UIControlStateNormal];
    [showAllButton addTarget:self action:@selector(showAllButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:showAllButton];
    showAllButton.frame = CGRectMake((SCREEN_WIDTH-40)/2, CGRectGetMaxY(desLabel.frame)-5, 40, 30);
    showAllButton.hidden = YES;
    
    barView = [[FWSelectBarView alloc] init];
    barView.titleArray = @[@"全部",@"视频",@"图文"];
    barView.numType = 3;
    [self addSubview:barView];
    barView.frame = CGRectMake(0, CGRectGetMaxY(showAllButton.frame), SCREEN_WIDTH, 50);

    [barView.firstButton addTarget:self action:@selector(videosButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [barView.secondButton addTarget:self action:@selector(picButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [barView.thirdButton addTarget:self action:@selector(draftButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    typeLabel = [[UILabel alloc] init];
    typeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    typeLabel.textColor = FWTextColor_9EA3AB;
    typeLabel.textAlignment = NSTextAlignmentLeft;
    typeLabel.text = @"时间排序";
    typeLabel.frame = CGRectMake(20, CGRectGetMaxY(barView.frame), 80, 40);
    [self addSubview:typeLabel];
    
    changeButton = [[UIButton alloc] init];
    changeButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [changeButton setTitleColor:FWTextColor_68769F forState:UIControlStateNormal];
    [changeButton setTitle:@"切换" forState:UIControlStateNormal];
    [changeButton addTarget:self action:@selector(changeButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:changeButton];
    [changeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(typeLabel);
        make.size.mas_equalTo(CGSizeMake(50, 40));
        make.right.mas_equalTo(self).mas_offset(-20);
    }];
    
}

#pragma mark - > 关注标签
- (void)attentionButtonOnClick{
    
    if ([self.tagsDelegate respondsToSelector:@selector(attentionButtonClick)]) {
        [self.tagsDelegate attentionButtonClick];
    }
}

#pragma mark - > 全部
- (void)videosButtonClick:(UIButton *)sender{
    
    if ([self.tagsDelegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.tagsDelegate headerBtnClick:sender];
    }
}

#pragma mark - > 视频
- (void)picButtonClick:(UIButton *)sender{
    
    if ([self.tagsDelegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.tagsDelegate headerBtnClick:sender];
    }
}

#pragma mark - > 视频
- (void)draftButtonClick:(UIButton *)sender{
    
    if ([self.tagsDelegate respondsToSelector:@selector(headerBtnClick:)]) {
        [self.tagsDelegate headerBtnClick:sender];
    }
}

- (void)congifViewWithModel:(id)model{
    
    self.subModel = (FWFeedModel *)model;
    
    if ([self.subModel.follow_status isEqualToString:@"1"]) {
        //已关注
        attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
        [attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
        [attentionButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
    }else if ([self.subModel.follow_status isEqualToString:@"2"]){
        //未关注
        attentionButton.backgroundColor = FWTextColor_222222;
        [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
        [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    }
    
    tagsLabel.text = [NSString stringWithFormat:@"#%@",self.subModel.tag_info.tag_name];
    desLabel.text = self.subModel.tag_info.tag_desc;
    numberScan.text = [NSString stringWithFormat:@"%@次播放",self.subModel.tag_info.pv_count_format];
    
    if (self.subModel.tag_info.create_user_nickname.length > 0) {
        
        NSString * str1 = @"创建者：";
        NSString * str3 = self.subModel.tag_info.create_user_nickname;
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",str1,str3]];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Regular" size:13] range:NSMakeRange(0,str1.length)];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Regular" size:13] range:NSMakeRange(str1.length,str3.length)];
        
        [str addAttribute:NSForegroundColorAttributeName value:FWTextColor_9EA3AB range:NSMakeRange(0,str1.length)];
        [str addAttribute:NSForegroundColorAttributeName value:FWTextColor_5F6680 range:NSMakeRange(str1.length,str3.length)];
        
        nameLabel.attributedText = str;
    }
   
//    photoImageView.hidden = !nameLabel.text.length;
    nameLabel.hidden = !nameLabel.text.length;
    arrowImageView.hidden = !nameLabel.text.length;
    nameButton.hidden = !nameLabel.text.length;
    
    if (nameLabel.text.length <= 0) {
        
        [numberScan mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(tagsLabel);
            make.height.mas_equalTo(17);
            make.top.mas_equalTo(tagsLabel.mas_bottom);
        }];
    }else{
        [numberScan mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(nameLabel);
            make.height.mas_equalTo(17);
            make.top.mas_equalTo(nameButton.mas_bottom);
        }];
    }

//    [photoImageView sd_setImageWithURL:[NSURL URLWithString:self.subModel.tag_info.create_user_header] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];

    [tagsCoverView sd_setImageWithURL:[NSURL URLWithString:self.subModel.tag_info.tag_image] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:desLabel.attributedText];
    
    CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 40].height;
    
    
    if (textHeight <= 40) {
        self.showAllButton.hidden = YES;
    }else{
        self.showAllButton.hidden = NO;
    }
    
    //展示文字或隐藏文字
    if (self.subModel.isShow) {
        self.desLabel.numberOfLines = 0;
        [showAllButton setImage:[UIImage imageNamed:@"show_up"] forState:UIControlStateNormal];
    }else{
        self.desLabel.numberOfLines = 2;
        if (textHeight == 14) {
            textHeight = 10;
        }else if (textHeight<=20&&textHeight>14) {
            textHeight = ceilf(desLabel.font.lineHeight);
        }else if (textHeight > 20 &&textHeight<=40) {
            textHeight = 2*ceilf(desLabel.font.lineHeight);
        }else{
            textHeight = 40;
        }
        [showAllButton setImage:[UIImage imageNamed:@"show_down"] forState:UIControlStateNormal];
    }
    
    
    desLabel.frame = CGRectMake(20, 178, SCREEN_WIDTH-30,textHeight);
    showAllButton.frame = CGRectMake((SCREEN_WIDTH-40)/2, CGRectGetMaxY(desLabel.frame)-5, 40, 30);
    
    if (self.showAllButton.hidden) {
        barView.frame = CGRectMake(0, CGRectGetMaxY(desLabel.frame)+10, SCREEN_WIDTH, 50);
    }else{
        barView.frame = CGRectMake(0, CGRectGetMaxY(showAllButton.frame), SCREEN_WIDTH, 50);
    }
    
    typeLabel.frame = CGRectMake(20, CGRectGetMaxY(barView.frame), 80, 40);
    
    if (isTimeSort) {
        typeLabel.text = @"时间排序";
    }else{
        typeLabel.text = @"热度排序";
    }
}

#pragma mark - > 查看个人
- (void)nameButtonClick{
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.subModel.tag_info.create_uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}


#pragma mark - > 切换排序
- (void)changeButtonOnClick{
    
    NSString * sortType = @"";
    if (isTimeSort) {
        typeLabel.text = @"热度排序";
        sortType = @"hot";
    }else{
        typeLabel.text = @"时间排序";
        sortType = @"new";
    }
    
    if ([self.tagsDelegate respondsToSelector:@selector(changeSortWithType:)]) {
        [self.tagsDelegate changeSortWithType:sortType];
    }
    
    isTimeSort = !isTimeSort;
}

#pragma mark - > 显示全部内容
- (void)showAllButtonOnClick{
    
    self.subModel.isShow = !self.subModel.isShow;
    
    if (self.subModel.isShow) {
        desLabel.numberOfLines = 0;
    }else{
        desLabel.numberOfLines = 2;
    }
    
    [self congifViewWithModel:self.subModel];
    
    if ([self.tagsDelegate respondsToSelector:@selector(showAllButtonClick)]) {
        [self.tagsDelegate showAllButtonClick];
    }
}

#pragma mark - > 返回当前顶部视图的高度
- (CGFloat)getCurrentViewHeight{
    
    CGFloat height = 0;
    height = CGRectGetMaxY(typeLabel.frame);
    return height;
}

#pragma mark - > 返回切换标签上部的高度
- (CGFloat)getBarViewTopHeight{
    
    CGFloat height = 0;
    height = CGRectGetMinY(barView.frame);
    return height;
}

@end
