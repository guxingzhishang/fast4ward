//
//  FWArticalDetailHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/7.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWArticalDetailHeaderView.h"

@interface FWArticalDetailHeaderView()

@property (nonatomic, strong) FWFeedModel * feedModel;

@end

@implementation FWArticalDetailHeaderView
@synthesize backButton;
@synthesize topHeaderView;
@synthesize titleLabel;
@synthesize timeLabel;
@synthesize scanLabel;
@synthesize lineOneView;
@synthesize photoView;
@synthesize nameLabel;
@synthesize vipImageButton;
@synthesize attentionButton;
@synthesize fixView;
@synthesize lineTwoView;
@synthesize tagTitleLabel;
@synthesize tagsView;
@synthesize lineThreeView;
@synthesize commentLabel;
@synthesize numberButton;
@synthesize tagsViewHeight;
@synthesize fixViewHeight;
@synthesize goodsView;
@synthesize goodsScanView;
@synthesize goodsImageView;
@synthesize goodsScanLabel;
@synthesize goodsPriceLabel;
@synthesize goodsTitleLabel;
@synthesize recammondLabel;
@synthesize goodsLineView;

- (id)init{
    self = [super init];
    if (self) {
        tagsViewHeight = 1;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    {
        topHeaderView = [UIImageView new];
        topHeaderView.userInteractionEnabled = YES;
        topHeaderView.image = [UIImage imageNamed:@"placeholder"];
        topHeaderView.contentMode = UIViewContentModeScaleAspectFill;
        topHeaderView.clipsToBounds = YES;
        topHeaderView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH*9/16);
        [self addSubview:topHeaderView];
    }
    
    {
        backButton = [[UIButton alloc] init];
        [backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backButton];
        backButton.frame = CGRectMake(10, 28+FWCustomeSafeTop, 30, 28);
    }
    
    {
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(topHeaderView.frame)+8, SCREEN_WIDTH-30, 40)];
        titleLabel.numberOfLines = 2;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 18];
        titleLabel.textColor = FWTextColor_2E2E2E;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:titleLabel];
    }
    
    {
        timeLabel = [[UILabel alloc] init];
        timeLabel.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame)+7, 150, 20);
        timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
        timeLabel.textColor = FWTextColor_AEAEAE;
        timeLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:timeLabel];
    }
    
    {
        scanLabel = [[UILabel alloc] init];
        scanLabel.frame = CGRectMake(CGRectGetMaxX(timeLabel.frame)+10, CGRectGetMinY(timeLabel.frame)+7, 200, 20);
        scanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
        scanLabel.textColor = FWTextColor_AEAEAE;
        scanLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:scanLabel];
    }
    
    {
        lineOneView = [[UIView alloc] init];
        lineOneView.frame = CGRectMake(24, CGRectGetMaxY(scanLabel.frame)+17, SCREEN_WIDTH, 1);
        lineOneView.backgroundColor = FWTextColor_ECECEC;
        [self addSubview:lineOneView];
    }
    
    {
        photoView = [UIImageView new];
        photoView.backgroundColor = FWViewBackgroundColor_F1F1F1;
        photoView.layer.cornerRadius = 16;
        photoView.userInteractionEnabled = YES;
        photoView.layer.masksToBounds = YES;
        photoView.frame = CGRectMake(CGRectGetMinX(scanLabel.frame), CGRectGetMaxY(lineOneView.frame)+20, 32, 32);
        [self addSubview:photoView];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageTap:)];
        [photoView addGestureRecognizer:tap];
    }
    
    {
        attentionButton = [[UIButton alloc] init];
        attentionButton.layer.cornerRadius = 2;
        attentionButton.layer.masksToBounds = YES;
        attentionButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        attentionButton.backgroundColor = FWTextColor_222222;
        [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
        [attentionButton setTitleColor:DHTitleColor_FFFFFF forState:UIControlStateNormal];
        [attentionButton addTarget:self action:@selector(attentionButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:attentionButton];
        [attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).mas_offset(-14);
            make.size.mas_equalTo(CGSizeMake(69, 30));
            make.centerY.mas_equalTo(photoView);
        }];
    }
    
    {
        nameLabel = [[UILabel alloc] init];
        nameLabel.font = DHSystemFontOfSize_14;
        nameLabel.textColor = FWTextColor_2E2E2E;
        nameLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:nameLabel];
        [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(photoView);
            make.left.mas_equalTo(photoView.mas_right).mas_offset(14);
            make.height.mas_equalTo(20);
            make.right.mas_lessThanOrEqualTo(attentionButton.mas_left).mas_offset(-35);
        }];
    }
    
    {
        vipImageButton = [[UIButton alloc] init];
        [vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:vipImageButton];
        [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(5);
            make.centerY.mas_equalTo(self.nameLabel).mas_offset(-0);
            make.size.mas_equalTo(CGSizeMake(16, 15));
        }];
    }
    
    {
        fixView = [UIView new];
        fixView.frame = CGRectMake(0, CGRectGetMaxY(photoView.frame)+8, SCREEN_WIDTH, 1);
        [self addSubview:fixView];
    }
    
    {
        lineTwoView = [[UIView alloc] init];
        lineTwoView.frame = CGRectMake(24, CGRectGetMaxY(fixView.frame)+8, SCREEN_WIDTH, 1);
        lineTwoView.backgroundColor = FWTextColor_ECECEC;
        [self addSubview:lineTwoView];
    }
    
    {
        tagTitleLabel = [[UILabel alloc] init];
        tagTitleLabel.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), CGRectGetMaxY(lineTwoView.frame)+15, 100, 20);
        tagTitleLabel.text= @"相关标签";
        tagTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
        tagTitleLabel.textColor = FWTextColor_2E2E2E;
        tagTitleLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:tagTitleLabel];
    }
    
    {
        tagsView = [[UIView alloc] init];
        tagsView.frame = CGRectMake(0, CGRectGetMaxY(tagTitleLabel.frame), SCREEN_WIDTH, 1);
        [self addSubview:tagsView];
    }
    
    {
        lineThreeView = [[UIView alloc] init];
        lineThreeView.frame = CGRectMake(24, CGRectGetMaxY(tagsView.frame)+14, SCREEN_WIDTH, 1);
        lineThreeView.backgroundColor = FWTextColor_ECECEC;
        [self addSubview:lineThreeView];
    }
    
    {
        goodsView = [[UIView alloc] init];
        goodsView.clipsToBounds = YES;
        goodsView.userInteractionEnabled = YES;
        goodsView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self addSubview:goodsView];
        goodsView.frame = CGRectMake(0, CGRectGetMaxY(lineThreeView.frame), SCREEN_WIDTH, 130);
        UITapGestureRecognizer * goodstap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goodsViewClick)];
        [goodsView addGestureRecognizer:goodstap];
        
        recammondLabel = [[UILabel alloc] init];
        recammondLabel.text = @"推荐商品";
        recammondLabel.textColor = FWTextColor_000000;
        recammondLabel.textAlignment = NSTextAlignmentLeft;
        recammondLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
        [goodsView addSubview:recammondLabel];
        recammondLabel.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), 0, 100, 25);
        
        goodsImageView = [[UIImageView alloc] init];
        goodsImageView.layer.cornerRadius = 4;
        goodsImageView.layer.masksToBounds = YES;
        goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
        [goodsView addSubview:goodsImageView];
        goodsImageView.frame = CGRectMake(CGRectGetMinX(recammondLabel.frame), CGRectGetMaxY(recammondLabel.frame) +5, 88, 88);
        
        goodsTitleLabel = [[UILabel alloc] init];
        goodsTitleLabel.textColor = FWTextColor_000000;
        goodsTitleLabel.numberOfLines = 2;
        goodsTitleLabel.textAlignment = NSTextAlignmentLeft;
        goodsTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        [goodsView addSubview:goodsTitleLabel];
        [goodsTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(goodsImageView).mas_offset(12);
            make.height.mas_greaterThanOrEqualTo (10);
            make.right.mas_equalTo(goodsView).mas_offset(-12);
            make.width.mas_equalTo(SCREEN_WIDTH-CGRectGetMaxX(goodsImageView.frame)-24);
        }];
        
        goodsPriceLabel = [[UILabel alloc] init];
        goodsPriceLabel.textColor = DHRedColorff6f00;
        goodsPriceLabel.textAlignment = NSTextAlignmentLeft;
        goodsPriceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
        [goodsView addSubview:goodsPriceLabel];
        [goodsPriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(goodsImageView).mas_offset(-10);
            make.height.mas_equalTo (20);
            make.left.mas_equalTo(goodsTitleLabel);
            make.width.mas_equalTo(150);
        }];
        
        goodsScanLabel = [[UILabel alloc] init];
        goodsScanLabel.textColor = FWTextColor_9C9C9C;
        goodsScanLabel.textAlignment = NSTextAlignmentRight;
        goodsScanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        [goodsView addSubview:goodsScanLabel];
        [goodsScanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(goodsPriceLabel);
            make.height.mas_equalTo (20);
            make.right.mas_equalTo(goodsView).mas_offset(-12);
            make.width.mas_greaterThanOrEqualTo(5);
        }];
        
        goodsScanView = [[UIImageView alloc] init];
        goodsScanView.image = [UIImage imageNamed:@"card_eye"];
        [goodsView addSubview:goodsScanView];
        [goodsScanView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(14, 10));
            make.right.mas_equalTo(goodsScanLabel.mas_left).mas_offset(-5);
            make.centerY.mas_equalTo(goodsPriceLabel);
        }];
        
        goodsLineView = [UIView new];
        goodsLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
        [self addSubview:goodsLineView];
        goodsLineView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame)-1, SCREEN_WIDTH, 0.5);
        
        goodsView.hidden = YES;
    }
    
    {
        commentLabel = [[UILabel alloc] init];
        commentLabel.frame = CGRectMake(CGRectGetMinX(tagTitleLabel.frame), CGRectGetMaxY(lineThreeView.frame)+15, 100, 20);
        commentLabel.text= @"评论";
        commentLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
        commentLabel.textColor = FWTextColor_2E2E2E;
        commentLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:commentLabel];
    }

    {
        numberButton = [[UIButton alloc] init];
        numberButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
        [numberButton setTitleColor:FWTextColor_AEAEAE forState:UIControlStateNormal];
        [numberButton addTarget:self action:@selector(numberButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:numberButton];
        [numberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).mas_offset(-15);
            make.height.mas_equalTo(20);
            make.width.mas_greaterThanOrEqualTo(10);
            make.centerY.mas_equalTo(commentLabel);
        }];
    }
}

#pragma mark - > 数据处理
- (void)dealWithData:(id)model{
    
    self.feedModel = (FWFeedModel *)model;
    
    titleLabel.text = self.feedModel.feed_info.feed_title;
    timeLabel.text = self.feedModel.feed_info.format_create_time;
    nameLabel.text = self.feedModel.feed_info.user_info.nickname;

    if ([self.feedModel.feed_info.count_realtime.pv_count_format integerValue] > 0) {
        scanLabel.text = [NSString stringWithFormat:@"%@ 次浏览",self.feedModel.feed_info.count_realtime.pv_count_format];
    }
    
    [topHeaderView sd_setImageWithURL:[NSURL URLWithString:self.feedModel.feed_info.feed_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [photoView sd_setImageWithURL:[NSURL URLWithString:self.feedModel.feed_info.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    if ([self.feedModel.feed_info.is_followed isEqualToString:@"2"]) {
        self.attentionButton.backgroundColor = FWTextColor_222222;
        [self.attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    }else{
        self.attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
        [self.attentionButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
        [self.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
    }
    
    self.vipImageButton.enabled = YES;
    if ([self.feedModel.feed_info.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([self.feedModel.feed_info.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([self.feedModel.feed_info.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    [self refreshFixView];
    
    [self refreshContainer:tagsView WithArray:self.feedModel.feed_info.tags];
    
    NSString * comment_count = self.feedModel.feed_info.count_realtime.comment_count?self.feedModel.feed_info.count_realtime.comment_count:@"0";
    
    [numberButton setTitle:[NSString stringWithFormat:@"%@ 条评论",comment_count] forState:UIControlStateNormal];
    
    
    [self refreshFrame];
}

#pragma mark - > 初始化图文混排
- (void)refreshFixView{
    
    for (UIView * view in fixView.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat height = 0;
    
    NSMutableArray * contentArray = (NSMutableArray *)self.feedModel.feed_info.format_content;
    
    if (contentArray.count <= 0) {
        return;
    }
    
    NSInteger imageTag = 0;
    CGFloat sizeFont = 14;
    
    for (FWFormatContentModel * contentModel in contentArray) {
        /* 如果当前是文字 */
        if ([contentModel.type isEqualToString:@"text"]) {
            
            UILabel * textLabel = [[UILabel alloc] init];
            textLabel.numberOfLines = 0;
            textLabel.frame = CGRectMake(15, height+10, SCREEN_WIDTH-30, 10);
        
            if ([contentModel.size isEqualToString:@"h1"]) {
                sizeFont = 18;
            }else if ([contentModel.size isEqualToString:@"h2"]){
                sizeFont = 17;
            }else{
                sizeFont = 16;
            }
            textLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:sizeFont];
            textLabel.textColor = FWTextColor_2E2E2E;
            textLabel.textAlignment = NSTextAlignmentLeft;
            textLabel.text = contentModel.value;
            [fixView addSubview:textLabel];
            [textLabel sizeToFit];
            height = height + textLabel.frame.size.height+10;
        }
        
        /* 如果当前是图片 */
        if ([contentModel.type isEqualToString:@"image"]) {
            UIImageView * imageView = [[UIImageView alloc] init];
            imageView.backgroundColor = FWViewBackgroundColor_F1F1F1;
            imageView.clipsToBounds = YES;
            imageView.tag = imageTag +3333;
            imageView.userInteractionEnabled = YES;
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            if (!contentModel.width || [contentModel.width floatValue]<= 0 ||
                !contentModel.height || [contentModel.width floatValue]<= 0) {
                // 兼容没有宽高的情况
                contentModel.width = @(SCREEN_WIDTH).stringValue;
                contentModel.height = @(SCREEN_WIDTH).stringValue;
            }
            imageView.frame = CGRectMake(0, height+10, SCREEN_WIDTH, (SCREEN_WIDTH*[contentModel.height floatValue]/[contentModel.width floatValue]));
            [imageView sd_setImageWithURL:[NSURL URLWithString:contentModel.src] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            [fixView addSubview:imageView];
            height = height + imageView.frame.size.height+10;
            
            UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapClick:)];
            [imageView addGestureRecognizer:tap];
            
            imageTag += 1 ;
            
            if (contentModel.desc.length > 0) {
                UILabel * textLabel = [[UILabel alloc] init];
                textLabel.numberOfLines = 0;
                textLabel.frame = CGRectMake(15, height+10, SCREEN_WIDTH-30, 20);
                textLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
                textLabel.textColor = FWTextColor_B6BCC4;
                textLabel.textAlignment = NSTextAlignmentLeft;
                textLabel.text = contentModel.desc;
                [fixView addSubview:textLabel];
                [textLabel sizeToFit];
                height = height + textLabel.frame.size.height+10;
            }
        }
    }
    
    fixViewHeight = height;
}

#pragma mark - > 初始化各种标签
- (void) refreshContainer:(UIView *)container WithArray:(NSArray *)array{
    
    for (UIView * view in container.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    CGFloat x = 0;
    CGFloat y = 5;
    CGFloat width = 0.0;
    CGFloat height = 24;
    
    for (int i = 0; i < array.count; i++) {
        FWSearchTagsListModel * tagsModel = array[i];
        
        width = [tagsModel.tag_name length]*14+20;
        
        if (x + width+20 >= SCREEN_WIDTH) {
            x = 0;
            y += height + 10;
        }
        
        UIButton * btn = [[UIButton alloc] init];
        btn.titleLabel.font = DHSystemFontOfSize_14;
        btn.backgroundColor = FWViewBackgroundColor_FFFFFF;
        btn.tag = 10000+i;
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        btn.layer.borderColor = FWViewBackgroundColor_F1F1F1.CGColor;
        [btn setTitle:[NSString stringWithFormat:@" %@",tagsModel.tag_name] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnTagsClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:FWTextColor_AEAEAE forState:UIControlStateNormal];
        [container addSubview:btn];
        
        btn.frame = CGRectMake(x, y, width, height);
        
        x += width+10;
    }
    
    tagsViewHeight = y + 30;
}

#pragma mark - > 重新布局
- (void)refreshFrame{
    
    titleLabel.frame = CGRectMake(15, CGRectGetMaxY(topHeaderView.frame)+8, SCREEN_WIDTH-20, 40);
    [titleLabel sizeToFit];

    timeLabel.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame)+7, 150, 20);
    [timeLabel sizeToFit];
    
    scanLabel.frame = CGRectMake(CGRectGetMaxX(timeLabel.frame)+10, CGRectGetMinY(timeLabel.frame), 200, 20);
    [scanLabel sizeToFit];
    
    lineOneView.frame = CGRectMake(24, CGRectGetMaxY(scanLabel.frame)+12, SCREEN_WIDTH, 1);
    
    photoView.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), CGRectGetMaxY(lineOneView.frame)+20, 32, 32);
    
    /* height应该设置个全局变量 */
    fixView.frame = CGRectMake(0, CGRectGetMaxY(photoView.frame)+20, SCREEN_WIDTH, fixViewHeight);

    lineTwoView.frame = CGRectMake(24, CGRectGetMaxY(fixView.frame)+15, SCREEN_WIDTH, 1);
    
    tagTitleLabel.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), CGRectGetMaxY(lineTwoView.frame)+15, 100, 20);
    
    /* height应该设置个全局变量 */
    tagsView.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), CGRectGetMaxY(tagTitleLabel.frame), SCREEN_WIDTH-30, tagsViewHeight);
    
    if (self.feedModel.feed_info.tags.count > 0) {
        lineTwoView.hidden = NO;
        tagTitleLabel.hidden = NO;
        tagsView.hidden = NO;
        
        lineThreeView.frame = CGRectMake(24, CGRectGetMaxY(tagsView.frame)+8, SCREEN_WIDTH, 1);
    }else{
        lineTwoView.hidden = YES;
        tagTitleLabel.hidden = YES;
        tagsView.hidden = YES;
        
        lineThreeView.frame = CGRectMake(24, CGRectGetMaxY(fixView.frame)+15, SCREEN_WIDTH, 1);
    }
    
    
    if (self.feedModel.feed_info.goods_info.goods_id.length > 0 && ![self.feedModel.feed_info.goods_info.goods_id isEqualToString:@"0"]) {
        
        goodsTitleLabel.text = self.feedModel.feed_info.goods_info.title;
        goodsPriceLabel.text = [NSString stringWithFormat:@"￥%@",self.feedModel.feed_info.goods_info.price];
        goodsScanLabel.text = self.feedModel.feed_info.goods_info.click_count;
        [goodsImageView sd_setImageWithURL:[NSURL URLWithString:self.feedModel.feed_info.goods_info.cover.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        goodsView.hidden = NO;
        goodsLineView.hidden = NO;
        
        goodsView.frame = CGRectMake(0, CGRectGetMaxY(lineThreeView.frame), SCREEN_WIDTH, 130);
        goodsLineView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame)-1, SCREEN_WIDTH, 0.5);
        
        commentLabel.frame = CGRectMake(CGRectGetMinX(self.timeLabel.frame),CGRectGetMaxY(goodsLineView.frame)+5 , 100, 20);
    }else{
        
        goodsView.hidden = YES;
        commentLabel.frame = CGRectMake(CGRectGetMinX(self.timeLabel.frame),CGRectGetMaxY(lineThreeView.frame)+15 , 100, 20);
        goodsLineView.hidden = YES;
    }
    
    
//    commentLabel.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), CGRectGetMaxY(lineThreeView.frame)+15, 100, 20);

    self.viewHeight = CGRectGetMaxY(commentLabel.frame);
}

#pragma mark - > 标签
- (void)btnTagsClick:(UIButton *)sender{
    
    FWSearchTagsSubListModel * tagsModel = self.feedModel.feed_info.tags[sender.tag-10000];
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.traceType = 2;
    TVC.tagsModel = tagsModel;
    TVC.tags_id = tagsModel.tag_id;
    [self.viewController.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 关注
- (void)attentionButtonOnClick{
    
    [self checkLogin];

    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {

        NSString * action ;
        
        FWFeedListModel * listModel = self.feedModel.feed_info;
        
        if ([listModel.is_followed isEqualToString:@"2"]) {
            action = Submit_follow_users;
        }else{
            action = Submit_cancel_follow_users;
        }
        
        FWFollowRequest * request = [[FWFollowRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"f_uid":listModel.uid,
                                  };
        [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([action isEqualToString:Submit_follow_users]) {
                    listModel.is_followed = @"1";
                    self.attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
                    [self.attentionButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
                    [self.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
                    
                    [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self.vc];
                }else{
                    listModel.is_followed = @"2";
                    self.attentionButton.backgroundColor = FWTextColor_222222;
                    [self.attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
                    [self.attentionButton setTitle:@"+ 关注" forState:UIControlStateNormal];
                }
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}

#pragma mark - > 点击头像
- (void)photoImageTap:(UITapGestureRecognizer *)gesture{
    if (nil == self.feedModel.feed_info.user_info.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.feedModel.feed_info.user_info.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 评论页
- (void)numberButtonClick{
    
    FWCommentViewController * vc = [[FWCommentViewController alloc] init];
    vc.feed_id = self.feedModel.feed_info.feed_id;
    vc.listModel = self.feedModel.feed_info;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

#pragma mark - > 返回
- (void)backButtonOnClick{
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 点击图片
- (void)imageTapClick:(UITapGestureRecognizer *)gesture{
    
    int index = (int)(gesture.view.tag-3333);
    
    FWViewPictureViewController * PVC = [[FWViewPictureViewController alloc] init];
    PVC.model = self.feedModel.feed_info;
    PVC.currentIndex = index;
    [self.vc.navigationController pushViewController:PVC animated:YES];
}

#pragma mark - > 检测登录
- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

#pragma mark - > 商品详情
- (void)goodsViewClick{
    
    FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
    GDVC.goods_id = self.feedModel.feed_info.goods_info.goods_id;
    [self.vc.navigationController pushViewController:GDVC animated:YES];
}
@end
