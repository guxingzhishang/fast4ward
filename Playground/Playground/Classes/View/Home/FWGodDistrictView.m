//
//  FWGodDistrictView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/11.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGodDistrictView.h"

@implementation FWGodDistrictView
@synthesize godModel;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWTextColor_F8F8F8;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.attentionScrollView = [[UIScrollView alloc] init];
    self.attentionScrollView.backgroundColor =FWClearColor;
    self.attentionScrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.attentionScrollView];
    self.attentionScrollView.frame = CGRectMake(0, 15, SCREEN_WIDTH, 140);
    [self.attentionScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 140));
        make.top.mas_equalTo(self).mas_offset(15);
        make.left.mas_equalTo(self);
    }];
    
    self.hotContentLabel = [[UILabel alloc] init];
    self.hotContentLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.hotContentLabel.textColor = FWTextColor_BCBCBC;
    self.hotContentLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.hotContentLabel];
    self.hotContentLabel.frame = CGRectMake(14, CGRectGetMaxY(self.attentionScrollView.frame)+20, 200, 20);
}

- (void)configForView:(id)model{
    
    self.hotContentLabel.text = @"热门内容";

    godModel = (FWFeedModel *)model;
    
    [self.attentionScrollView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    NSInteger count = godModel.list_dashen.count;
    
    self.attentionScrollView.contentSize = CGSizeMake(155 *count+30, 0);
    
    for(int i = 0; i < count; i++){
        
        FWMineInfoModel * userInfo = godModel.list_dashen[i];
        //  130  172     150  155
        UIImageView * bgView = [[UIImageView alloc] init];
        bgView.tag = 20000+i;
        bgView.backgroundColor =FWViewBackgroundColor_FFFFFF;
        bgView.layer.cornerRadius =2;
        bgView.layer.masksToBounds = YES;
        bgView.userInteractionEnabled = YES;
        [self.attentionScrollView addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(130, 140));
            make.top.mas_equalTo(self.attentionScrollView);
            make.left.mas_equalTo(self.attentionScrollView).mas_offset(i*140+10);
        }];
        
        UIImageView * photoImageView = [[UIImageView alloc] init];
        photoImageView.layer.cornerRadius = 54/2;
        photoImageView.layer.masksToBounds = YES;
        photoImageView.userInteractionEnabled = YES;
        photoImageView.tag = 25000+i;
        [photoImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.header_url] placeholderImage:nil];
        [bgView addSubview:photoImageView];
        [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(bgView).mas_offset(14);
            make.centerX.mas_equalTo(bgView);
            make.size.mas_equalTo(CGSizeMake(54, 54));
        }];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapClick:)];
        [photoImageView addGestureRecognizer:tap];
        
        UIImageView * authenticationView = [[UIImageView alloc] init];
        [bgView addSubview:authenticationView];
        [authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.width.mas_equalTo(14);
            make.right.mas_equalTo(photoImageView).mas_offset(-0);
            make.bottom.mas_equalTo(photoImageView).mas_offset(-0);
        }];
        // 认证身份
        if (userInfo.title.length > 0) {
            
            if ([userInfo.cert_status isEqualToString:@"2"])
            {
                authenticationView.hidden = NO;
                authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
            }else{
                authenticationView.hidden = YES;
            }
        }else{
            authenticationView.hidden = YES;
        }
        
        
        UILabel * nameLabel = [[UILabel alloc] init];
        nameLabel.font = DHSystemFontOfSize_14;
        if([userInfo.cert_status isEqualToString:@"2"]) {
            nameLabel.textColor = FWTextColor_FFAF3C;
        }else{
            if ([userInfo.merchant_cert_status isEqualToString:@"3"] &&
                ![userInfo.cert_status isEqualToString:@"2"]){
                nameLabel.textColor = FWTextColor_2B98FA;
            }else{
                nameLabel.textColor = FWTextColor_272727;
            }
        }
        nameLabel.text = userInfo.nickname;
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [bgView addSubview:nameLabel];
        [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bgView);
            make.top.mas_equalTo(photoImageView.mas_bottom).mas_offset(13);
            make.height.mas_equalTo(18);
            make.width.mas_equalTo(81);
        }];
        
        UILabel * authenticationLabel = [[UILabel alloc] init];
        if (userInfo.dashen_title.length > 0) {
            /* 优先显示认证 */
            authenticationLabel.text = userInfo.dashen_title;
        }
        authenticationLabel.numberOfLines = 2;
        authenticationLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        authenticationLabel.textAlignment = NSTextAlignmentCenter;
        authenticationLabel.textColor = FWTextColor_BCBCBC;
        [bgView addSubview:authenticationLabel];
        [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_greaterThanOrEqualTo(15);
            make.top.mas_equalTo(nameLabel.mas_bottom);
            make.centerX.mas_equalTo(bgView);
            make.width.mas_equalTo(125);
        }];
        
        UIButton *vipImageButton = [[UIButton alloc] init];
        [vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:vipImageButton];
        [vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(nameLabel.mas_right).mas_offset(5);
            make.centerY.mas_equalTo(nameLabel).mas_offset(-1);
            make.size.mas_equalTo(CGSizeMake(16, 15));
        }];
        
        //vip图标
        vipImageButton.enabled = YES;
        if ([userInfo.user_level isEqualToString:@"1"]) {
            [vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
        }else if ([userInfo.user_level isEqualToString:@"2"]) {
            [vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
        }else if ([userInfo.user_level isEqualToString:@"3"]) {
            [vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
        }else{
            vipImageButton.enabled = NO;
            [vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(bgView);
                make.top.mas_equalTo(photoImageView.mas_bottom).mas_offset(13);
                make.height.mas_equalTo(18);
                make.width.mas_equalTo(110);
            }];
        }

    }
}

#pragma mark - > 查看会员权益
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}
//
//#pragma mark - > 关注
//- (void)attentionButtonOnClick:(UIButton *)sender{
//
//    NSInteger index = sender.tag - 30000;
//
//    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
//
//        FWFollowRequest * request = [[FWFollowRequest alloc] init];
//
//        NSDictionary * params = @{
//                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
//                                  @"f_uid":self.godModel.list_suggestion_user[index].uid,
//                                  };
//        [request startWithParameters:params WithAction:Submit_follow_users WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
//
//            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
//
//            if ([code isEqualToString:NetRespondCodeSuccess]) {
//
//                [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self.viewController];
//                if (self.godModel.list_suggestion_user.count > index) {
//                    [self.godModel.list_suggestion_user removeObjectAtIndex:index];
//
//                    if (self.godModel.list_suggestion_user.count == 0) {
//                        self.attentionBlock(YES);
//                        return ;
//                    }
//
//                    [self configForCell:self.godModel];
//                }
//
//            }else{
//                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
//            }
//        }];
//    }
//}

- (void)photoImageViewTapClick:(UIGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag-25000;
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.godModel.list_dashen[index].uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}


@end
