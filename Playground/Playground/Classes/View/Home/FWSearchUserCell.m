//
//  FWSearchUserCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/20.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWSearchUserCell.h"

@implementation FWSearchUserCell
@synthesize followButton;
@synthesize userModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.contentView.backgroundColor = FWTextColor_FAFAFA;
    [self changeSubviews];
    
    return self;
}

- (void)changeSubviews{
    
    self.photoImageView.layer.cornerRadius = 38/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView.mas_left).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(38, 38));
    }];
    
    self.authenticationImageView.hidden = NO;
    [self.authenticationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.right.mas_equalTo(self.photoImageView).mas_offset(2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(0);
    }];
    
    followButton = [[UIButton alloc] init];
    followButton.layer.cornerRadius = 2;
    followButton.layer.masksToBounds = YES;
    followButton.titleLabel.font = DHSystemFontOfSize_13;
    [followButton setTitle:@"互相关注" forState:UIControlStateNormal];
    [followButton setTitleColor:DHTextHexColor_Dark_222222 forState:UIControlStateNormal];
    [followButton addTarget:self action:@selector(followButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:followButton];
    [followButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(69,30));
    }];
    
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.followButton);
        make.left.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-1);
    }];
    
    self.nameLabel.font = DHSystemFontOfSize_14;
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        make.top.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.followButton.mas_left).mas_offset(-5);
    }];
    
    self.signLabel.font = DHSystemFontOfSize_12;
    [self.signLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.right.mas_equalTo(self.followButton.mas_left).mas_offset(-5);
    }];
    
    self.lineView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.followButton);
        make.left.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-0.6);
    }];
}

- (void)followButtonClick:(UIButton *)sender{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        if ([self.delegate respondsToSelector:@selector(followButtonForIndex:withCell:)]) {
            [self.delegate followButtonForIndex:sender.tag - 6000 withCell:self];
        }
    }
}

- (void)cellConfigureFor:(id)model{
    
    userModel = (FWMineInfoModel *)model;
    
    
    if ([userModel.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        // 自己不显示关注
        followButton.hidden = YES;
    }else{
        followButton.hidden = NO;
    }
    
    self.selectImageView.hidden = YES;
    
    self.nameLabel.text = userModel.nickname;
    self.signLabel.text = userModel.autograph;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:userModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];
    
    if([userModel.cert_status isEqualToString:@"2"]) {
        self.nameLabel.textColor = FWTextColor_FFAF3C;
        self.authenticationImageView.hidden = NO;
        self.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        if ([userModel.merchant_cert_status isEqualToString:@"3"] &&
            ![userModel.cert_status isEqualToString:@"2"]){
            self.nameLabel.textColor = FWTextColor_2B98FA;
            self.authenticationImageView.hidden = YES;
        }else{
            self.authenticationImageView.hidden = YES;
            self.nameLabel.textColor = FWTextColor_000000;
        }
    }
    
    NSString * buttonTitle;
    NSInteger state = [userModel.follow_status integerValue];
    UIColor * titleColor = nil;
    UIColor * buttonBackgroundColor = nil;
    switch (state) {
        case 0:
        {
            buttonTitle = @"关注";
            titleColor = DHTitleColor_FFFFFF;
            buttonBackgroundColor = FWTextColor_222222;
        }
            break;
        case 1:
        {
            buttonTitle = @"已关注";
            titleColor = FWTextColor_515151;
            buttonBackgroundColor = FWViewBackgroundColor_EEEEEE;
        }
            break;
        case 2:
        {
            buttonTitle = @"关注";
            titleColor = FWViewBackgroundColor_FFFFFF;
            buttonBackgroundColor = FWTextColor_222222;
        }
            break;
        case 3:
        {
            buttonTitle = @"互相关注";
            titleColor = FWTextColor_515151;
            buttonBackgroundColor = FWViewBackgroundColor_EEEEEE;
        }
            break;
        default:
            break;
    }
    
    [followButton setBackgroundColor:buttonBackgroundColor];
    [followButton setTitle:buttonTitle forState:UIControlStateNormal];
    [followButton setTitleColor:titleColor forState:UIControlStateNormal];
}

#pragma mark - > 点击用户头像
- (void)photoImageViewTapClick{
    
    if (nil == self.userModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.userModel.uid;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

@end
