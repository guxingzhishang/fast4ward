//
//  FWTagsView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWSelectBarView.h"

@protocol FWTagsViewDelegate <NSObject>

#pragma mark - > 关注标签
- (void)attentionButtonClick;

#pragma mark - > 全部or视频
- (void)headerBtnClick:(UIButton *)sender;

#pragma mark - > 展示所有描述
- (void)showAllButtonClick;

#pragma mark - > 切换排序
- (void)changeSortWithType:(NSString *)sortType;

@end

@interface FWTagsView : UIView

@property (nonatomic, weak) UIViewController * vc;

/* 标签封面 */
@property (nonatomic, strong) UIImageView * tagsCoverView;

/* 标签名字 */
@property (nonatomic, strong) UILabel * tagsLabel;

/* 发起者的头像 */
@property (nonatomic, strong) UIImageView * photoImageView;

/* 名称 */
@property (nonatomic, strong) UIButton * nameButton;

/* 发起者的名字 */
@property (nonatomic, strong) UILabel * nameLabel;

/* 右箭头 */
@property (nonatomic, strong) UIImageView * arrowImageView;

/* 多少次观看 */
@property (nonatomic, strong) UILabel * numberScan;

/* 描述 */
@property (nonatomic, strong) UILabel * desLabel;

/* 描述 */
@property (nonatomic, strong) UIButton * showAllButton;

/*关注按钮 */
@property (nonatomic, strong) UIButton * attentionButton;

@property (nonatomic, strong) FWSelectBarView * barView;

@property (nonatomic, strong) UILabel * typeLabel;

@property (nonatomic, strong) UIButton * changeButton;

@property (nonatomic, weak) id<FWTagsViewDelegate> tagsDelegate;

@property (nonatomic, assign) BOOL  isTimeSort;// yes 时间排序，NO 热度排序

- (void)congifViewWithModel:(id)model;

- (CGFloat)getCurrentViewHeight;

- (CGFloat)getBarViewTopHeight;

@end
