//
//  FWHomeAttentionCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^allAttetionBlock)(BOOL isShouldDelete);

@interface FWHomeAttentionCell : UITableViewCell

@property (nonatomic, strong) UILabel * tipLabel;

@property (nonatomic, strong) UIScrollView * attentionScrollView;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWFeedModel * attentionModel;

@property (nonatomic, copy) allAttetionBlock attentionBlock;


- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
