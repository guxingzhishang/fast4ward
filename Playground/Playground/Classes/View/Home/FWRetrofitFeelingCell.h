//
//  FWRetrofitFeelingCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWRetrofitFeelingCell : UITableViewCell
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UIImageView * arrowImageView;

@end

NS_ASSUME_NONNULL_END
