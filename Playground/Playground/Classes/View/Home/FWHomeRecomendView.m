//
//  FWHomeRecomendView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeRecomendView.h"
#import "FWMatchViewController.h"
#import "FWGodDistrictViewController.h"
#import "FWRetrofitFeelingViewController.h"
#import "FWHotLiveListViewController.h"
#import "FWHomeBussinessViewController.h"
#import "UIScrollView+ABSideRefresh.h"
#import "FWRealScroeAndRankViewController.h"
#import "FWLiveViewController.h"
#import "FWMatchPicsViewController.h"
#import "FWChooseBradeViewController.h"
#import "FWRefitCaseViewController.h"

@implementation FWHomeRecomendView

- (void)setIsPush:(BOOL)isPush{
    _isPush = isPush;
}

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWTextColor_FAFAFA;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{

    self.bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(14, 10, SCREEN_WIDTH-28, 0.01) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.bannerView.layer.cornerRadius = 2;
    self.bannerView.layer.masksToBounds = YES;
    self.bannerView.autoScrollTimeInterval = 4;
    self.bannerView.autoScroll=YES;
    self.bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    self.bannerView.pageDotImage = [UIImage imageWithColor:FWColorWihtAlpha(@"ffffff", 0.4) forSize:CGSizeMake(6, 2)];
    self.bannerView.currentPageDotImage = [UIImage imageWithColor:FWViewBackgroundColor_FFFFFF forSize:CGSizeMake(6, 2)]; // 自定义分页控件小圆标颜色
    self.bannerView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    [self addSubview:self.bannerView];
    
    self.emergencyImageView = [[UIImageView alloc] init];
    self.emergencyImageView.layer.cornerRadius = 2;
    self.emergencyImageView.layer.masksToBounds = YES;
    self.emergencyImageView.userInteractionEnabled = YES;
    self.emergencyImageView.image = [UIImage imageNamed:@"placeholder"];
    [self addSubview:self.emergencyImageView];
    self.emergencyImageView.frame = CGRectMake(14, CGRectGetMaxY(self.bannerView.frame)+5, SCREEN_WIDTH-28, 0.01);
    [self.emergencyImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emergencyImageViewClick)]];


    self.anliLabel = [[UILabel alloc] init];
    self.anliLabel.text = @"改装案例";
    self.anliLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    self.anliLabel.textColor = FWColor(@"2d2d2d");
    self.anliLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.anliLabel];
    self.anliLabel.frame = CGRectMake(14, CGRectGetMaxY(self.emergencyImageView.frame)+5, 100, 30);
    
    self.gaizhuangView = [[UIScrollView alloc] init];
    self.gaizhuangView.delegate = self;
    self.gaizhuangView.showsHorizontalScrollIndicator = NO;
    self.gaizhuangView.pagingEnabled = YES;
    [self addSubview:self.gaizhuangView];
    self.gaizhuangView.frame = CGRectMake(0, CGRectGetMaxY(self.anliLabel.frame)+5, SCREEN_WIDTH, 120);
    
    self.pageControl = [[FWCustomPageControl alloc] init];
    self.pageControl.numberOfPages = 2;
    self.pageControl.currentColor = FWTextColor_222222;
    self.pageControl.otherColor = FWTextColor_BCBCBC;
    self.pageControl.userInteractionEnabled = NO;
    self.pageControl.PageControlContentMode = FWPageControlContentModeCenter; //设置对齐方式
    self.pageControl.controlSpacing = 3.0;  //间距
    self.pageControl.marginSpacing = 10;  //距离初始位置 间距  默认10
    self.pageControl.PageControlStyle = FWPageControlStyelRectangle;//长条样式
    self.pageControl.currentPage = 0;
    [self addSubview:self.pageControl];
    [self.pageControl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.gaizhuangView).mas_offset(-5);
        make.centerX.mas_equalTo(self.gaizhuangView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 20));
    }];
    
    self.questionView = [[UIView alloc] init];
    [self addSubview:self.questionView];
    self.questionView.frame = CGRectMake(0, CGRectGetMaxY(self.gaizhuangView.frame)+5, SCREEN_WIDTH, 0.01);
    
    self.guessLabel = [[UILabel alloc] init];
    self.guessLabel.text = @"猜你喜欢";
    self.guessLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    self.guessLabel.textColor = FWColor(@"2d2d2d");
    self.guessLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.guessLabel];
    self.guessLabel.frame = CGRectMake(14, CGRectGetMaxY(self.questionView.frame)+25, 100, 30);
    
    self.currentHeight = CGRectGetMaxY(self.guessLabel.frame);
    self.currentScrollViewHeight = 120;
}

- (void)emergencyImageViewClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.bannerModel.h5_href_url;
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 赋值
- (void)configViewForModel:(FWFeedModel *)model{
    
    self.bannerModel = (FWFeedModel *)model;
    // 轮播图
    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (FWActivityBannerModel * imageModel in model.list_banner) {
        [tempArr addObject:imageModel.img_url];
    }
    
    if (tempArr.count > 0) {
        self.bannerView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, (SCREEN_WIDTH-28)*146/(375-28));
    }else{
        self.bannerView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, 0.01);
    }
    
    if ([self.bannerModel.h5_show_status isEqualToString:@"1"]) {
        /* 展示 */
        self.emergencyImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.emergencyImageView.clipsToBounds = YES;
        self.emergencyImageView.frame = CGRectMake(14, CGRectGetMaxY(self.bannerView.frame)+10, SCREEN_WIDTH-28, 55);
        [self.emergencyImageView sd_setImageWithURL:[NSURL URLWithString:self.bannerModel.h5_img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }else{
        /* 隐藏 */
        self.emergencyImageView.frame = CGRectMake(14, CGRectGetMaxY(self.bannerView.frame), SCREEN_WIDTH-28, 0.01);
        self.emergencyImageView.image = [UIImage imageNamed:@""];
    }
    

    
    self.anliLabel.frame = CGRectMake(14, CGRectGetMaxY(self.emergencyImageView.frame)+5, 100, 30);

    // 改装案例
    [self setupAnliScrollView];
    self.gaizhuangView.frame = CGRectMake(0, CGRectGetMaxY(self.anliLabel.frame)+5, SCREEN_WIDTH, self.currentScrollViewHeight);
    self.gaizhuangView.contentSize = CGSizeMake(2*SCREEN_WIDTH, 0);

    // 问答模块
    if (self.bannerModel.list_tag.count > 0) {
        [self setupQuestionView];
        self.questionView.frame = CGRectMake(0, CGRectGetMaxY(self.gaizhuangView.frame)+5, SCREEN_WIDTH, 25*self.bannerModel.list_tag.count);
    }else{
        self.questionView.frame = CGRectMake(0, CGRectGetMaxY(self.gaizhuangView.frame)+5, SCREEN_WIDTH, 0.01);
    }
    
    self.guessLabel.frame = CGRectMake(14, CGRectGetMaxY(self.questionView.frame)+25, 100, 30);


    // 获取当前视图的高度
    self.currentHeight = CGRectGetMaxY(self.guessLabel.frame)+5;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.bannerView.imageURLStringsGroup = tempArr;
    });
}

#pragma mark - > 初始化改装案例
- (void)setupAnliScrollView{
    
    for (UIView * view in self.gaizhuangView.subviews) {
        [view removeFromSuperview];
    }
    
    NSInteger padding = 16;
    NSInteger holePadding = 94;
    NSInteger leftCount = self.bannerModel.list_gaizhuang_category1.count;
    NSInteger rightCount = self.bannerModel.list_gaizhuang_category2.count;

    if (SCREEN_WIDTH <= 375) {
        padding = 0;
        holePadding = 30;
    }
    
    
    for (int i = 0; i < leftCount; i++) {
        FWHomeGaiZhuangListModel * listModel = self.bannerModel.list_gaizhuang_category1[i];
        
        FWModuleButton * moduleButton = [[FWModuleButton alloc] init];
        moduleButton.tag = 10009+i;
        [moduleButton addTarget:self action:@selector(preButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.gaizhuangView addSubview:moduleButton];
        moduleButton.frame = CGRectMake(15+i*((SCREEN_WIDTH-holePadding)/leftCount+padding), 0, (SCREEN_WIDTH-holePadding)/leftCount,  ((SCREEN_WIDTH-holePadding)/leftCount)+20);
        
        [moduleButton setModuleTitle:listModel.name];
        [moduleButton.iconImageView sd_setImageWithURL:[NSURL URLWithString:listModel.img]];
        
        moduleButton.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        [moduleButton.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(moduleButton);
            make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-holePadding)/leftCount, (SCREEN_WIDTH-holePadding)/leftCount));
        }];

        moduleButton.nameLabel.font = DHSystemFontOfSize_12;
        moduleButton.nameLabel.textAlignment = NSTextAlignmentCenter;
        moduleButton.nameLabel.textColor = FWColor(@"#39393F");
        [moduleButton.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(moduleButton);
            make.top.mas_equalTo(moduleButton.iconImageView.mas_bottom);
            make.height.mas_equalTo(22);
            make.centerX.mas_equalTo(moduleButton.iconImageView);
            make.width.mas_equalTo(SCREEN_WIDTH/leftCount);
        }];
    }
    

    for (int i = 0; i < rightCount; i++) {
        FWHomeGaiZhuangListModel * listModel = self.bannerModel.list_gaizhuang_category2[i];

        FWModuleButton * moduleButton = [[FWModuleButton alloc] init];
        moduleButton.tag = 10099+i;
        [moduleButton addTarget:self action:@selector(nextButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.gaizhuangView addSubview:moduleButton];
        moduleButton.frame = CGRectMake(SCREEN_WIDTH+15+(i%5)*((SCREEN_WIDTH-holePadding)/5+padding),  (((SCREEN_WIDTH-holePadding)/5)+20)*(i/5), (SCREEN_WIDTH-holePadding)/5,  ((SCREEN_WIDTH-holePadding)/5)+20);
        
        [moduleButton setModuleTitle:listModel.name];
        [moduleButton.iconImageView sd_setImageWithURL:[NSURL URLWithString:listModel.img]];
        
        moduleButton.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        [moduleButton.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(moduleButton);
            make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-holePadding)/5, (SCREEN_WIDTH-holePadding)/5));
        }];

        moduleButton.nameLabel.font = DHSystemFontOfSize_12;
        moduleButton.nameLabel.textAlignment = NSTextAlignmentCenter;
        moduleButton.nameLabel.textColor = FWColor(@"#39393F");
        [moduleButton.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(moduleButton);
            make.top.mas_equalTo(moduleButton.iconImageView.mas_bottom);
            make.height.mas_equalTo(22);
            make.width.mas_equalTo((SCREEN_WIDTH-holePadding)/5);
        }];
    }
}

#pragma mark - > 首页的5个改装案例
- (void)preButtonClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 10009;
    
    if (self.bannerModel.list_gaizhuang_category1.count > index) {
        FWHomeGaiZhuangListModel * listModel = self.bannerModel.list_gaizhuang_category1[index];
        if ([listModel.category_id isEqualToString:@"1000"]) {
            FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
            CBVC.chooseFrom = @"anchexi";
            [self.vc.navigationController pushViewController:CBVC animated:YES];
        }else{
            FWRefitCaseViewController * RCVC = [[FWRefitCaseViewController alloc] init];
            RCVC.category_id = listModel.category_id;
            RCVC.category_name = listModel.name;
            [self.vc.navigationController pushViewController:RCVC animated:YES];
        }
    }
    
}

#pragma mark - > 首页的改装案例第二页
- (void)nextButtonClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 10099;
    
    FWHomeGaiZhuangListModel * listModel = self.bannerModel.list_gaizhuang_category2[index];

    FWRefitCaseViewController * RCVC = [[FWRefitCaseViewController alloc] init];
    RCVC.category_id = listModel.category_id;
    RCVC.category_name = listModel.name;
    [self.vc.navigationController pushViewController:RCVC animated:YES];
}

#pragma mark - > 初始化问答
- (void)setupQuestionView{
    
    for (UIView * view in self.questionView.subviews) {
        [view removeFromSuperview];
    }
    
    for(int i = 0 ; i< self.bannerModel.list_tag.count;i++){
        
        FWHomeQuestionTagListModel * listModel = self.bannerModel.list_tag[i];
        
        UIView * tagsView = [[UIView alloc] init];
        tagsView.tag = 12321 +i;
        [self.questionView addSubview:tagsView];
        [tagsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.questionView);
            make.top.mas_equalTo(self.questionView).mas_offset(30*i);
            make.width.mas_equalTo(SCREEN_WIDTH-20);
            make.height.mas_equalTo(30);
        }];
        [tagsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentLabelClick:)]];

        
        UIImageView * shapeView = [[UIImageView alloc] init];
        shapeView.image = [UIImage imageNamed:@"shape_question"];
        [tagsView addSubview:shapeView];
        [shapeView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(tagsView).mas_offset(9);
            make.size.mas_equalTo(CGSizeMake(13, 12));
            make.left.mas_equalTo(self.questionView).mas_offset(14);
        }];
        
        UILabel * dongtaiLabel = [[UILabel alloc] init];
        dongtaiLabel.font = DHSystemFontOfSize_12;
        dongtaiLabel.textColor = DHLineHexColor_LightGray_999999;
        dongtaiLabel.textAlignment = NSTextAlignmentRight;
        [tagsView addSubview:dongtaiLabel];
        [dongtaiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(shapeView);
            make.right.mas_equalTo(tagsView).mas_offset(-14);
            make.height.mas_equalTo(17);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
        
        UILabel * contentLabel = [[UILabel alloc] init];
        contentLabel.font = DHSystemFontOfSize_14;
        contentLabel.textColor = FWColor(@"2d2d2d");
        contentLabel.textAlignment = NSTextAlignmentLeft;
        [tagsView addSubview:contentLabel];
        [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(shapeView);
            make.left.mas_equalTo(shapeView.mas_right).mas_offset(14);
            make.height.mas_equalTo(18);
            make.right.mas_equalTo(dongtaiLabel.mas_left).mas_offset(-14);
            make.width.mas_greaterThanOrEqualTo(10);
        }];

        dongtaiLabel.text = [NSString stringWithFormat:@"%@条动态",listModel.feed_count];
        contentLabel.text = listModel.tag_name;
    }
}

#pragma mark - > 问答模块
- (void)contentLabelClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 12321;
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.tags_id = self.bannerModel.list_tag[index].tag_id;
    [self.vc.navigationController pushViewController:TVC animated:YES];
}

//- (void)injected{
//
//}

/**
 * 1:赛事主页   2:直播页  3:回放页   4:文章详情页  5:图文详情页   6:话题页  7:商家店铺页  8:商品详情页   9:H5 10:商家排行榜   11：车友排行榜   12:无跳转  13：视频贴 16：长视频
 */
#pragma mark - > banner跳转
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
    FWHomeBannerModel * imageModel = self.bannerModel.list_banner[index];
    
    if (imageModel.banner_id) {
        [TalkingData trackEvent:@"首页banner" label:@"" parameters:@{@"bannerID":imageModel.banner_id}];
    }
    
    if ([imageModel.banner_type isEqualToString:@"1"]){
        /* 赛事主页 */
        self.vc.navigationController.tabBarController.selectedIndex = 1;
    }else if ([imageModel.banner_type isEqualToString:@"4"]){
        /* 文章详情页 */
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = imageModel.banner_val;
        [self.vc.navigationController pushViewController:DVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"5"]){
        /* 图文详情页 */
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.feed_id = imageModel.banner_val;
        TVC.requestType = FWPushMessageRequestType;
        [self.vc.navigationController pushViewController:TVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"6"]){
        /* 标签（话题）页 */
        FWTagsViewController * PVC = [[FWTagsViewController alloc] init];
        PVC.tags_id = imageModel.banner_val;
        [self.vc.navigationController pushViewController:PVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"7"]) {
        /* 商家店铺页*/
        FWCustomerShopViewController * CSVC = [[FWCustomerShopViewController alloc] init];
        CSVC.user_id = imageModel.banner_val;
        CSVC.isUserPage = NO;
        [self.vc.navigationController pushViewController:CSVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"8"]) {
        /* 商品详情页 */
        FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
        GDVC.goods_id = imageModel.banner_val;
        [self.vc.navigationController pushViewController:GDVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"9"]){
        /* 打开H5活动页 */
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.htmlStr = imageModel.banner_val;
        WVC.pageType = WebViewTypeURL;
        [self.vc.navigationController pushViewController:WVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"13"]){
        /* 视频详情页 */
        FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
        controller.currentIndex = 0;
        controller.feed_id = imageModel.banner_val;
        controller.requestType = FWPushMessageRequestType;
        [self.vc.navigationController pushViewController:controller animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"16"]){
        /* 长视频详情页 */
        FWLongVideoPlayerViewController *controller = [[FWLongVideoPlayerViewController alloc] init];
        controller.feed_id = imageModel.banner_val;
        controller.myBlock = ^(FWFeedListModel *listModel) {};
        [self.vc.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - > 根据改装案例左右滚动，首页列表高度改变
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
 
    if (scrollView == self.gaizhuangView) {
    
        float offset =  (float)(((scrollView.contentOffset.x) *1.00)/(SCREEN_WIDTH * 1.00));

        // 如果不加这个判断，到头了在滚动，就会发现又会根据偏移量设置坐标，所以到极限后的偏移直接return
        if (offset<0 || offset>1) {
            return;
        }
        
        if (self.bannerModel.list_gaizhuang_category2.count>0) {
            NSInteger line = (self.bannerModel.list_gaizhuang_category2.count-1)/5;
            self.gaizhuangView.frame = CGRectMake(0, CGRectGetMaxY(self.anliLabel.frame)+5, SCREEN_WIDTH, 120+line*90*offset);
            self.questionView.frame = CGRectMake(0, CGRectGetMaxY(self.gaizhuangView.frame)+5, SCREEN_WIDTH, 75);
            self.guessLabel.frame = CGRectMake(14, CGRectGetMaxY(self.questionView.frame)+25, 100, 30);

            self.currentHeight = CGRectGetMaxY(self.guessLabel.frame)+5;

            self.currentScrollViewHeight = 120+line*90*offset;
            
            self.myBlock(self.currentHeight);
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView == self.gaizhuangView) {
        self.gaizhuangView.contentSize = CGSizeMake(2*SCREEN_WIDTH, 0);

        NSInteger offset =  scrollView.contentOffset.x/SCREEN_WIDTH;
        self.pageControl.currentPage = offset;
    }
}

- (CGFloat)currentAnliHeight{
    CGFloat anliHeight = 0;
    anliHeight = CGRectGetMaxY(self.anliLabel.frame);
    return anliHeight;
}
@end
