//
//  FWThumbUpCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWThumbUpCell.h"

@implementation FWThumbUpCell
@synthesize timeLabel;
@synthesize unreadView;
@synthesize messageModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
    }
    return self;
}


- (void)setupSubviews{
    
    self.selectImageView.hidden = YES;
    
    self.photoImageView.layer.cornerRadius = 43/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(43, 43));
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(24);
    }];
    
    [self.authenticationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.right.mas_equalTo(self.photoImageView).mas_offset(-2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(0);
    }];
    
    self.nameLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 16];
    self.nameLabel.textColor = FWTextColor_000000;
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(12);
        make.height.mas_equalTo(19);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-184);
        make.top.mas_equalTo(self.contentView).mas_offset(14);
    }];
    
    self.signLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 13];
    self.signLabel.textColor = FWTextColor_9EA3AB;
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(self.nameLabel);
        make.width.mas_lessThanOrEqualTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(5);
    }];
    
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 13];
    timeLabel.textColor = FWTextColor_9EA3AB;
    timeLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(self.signLabel);
        make.width.mas_greaterThanOrEqualTo(50);
    }];
    
    self.lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.timeLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-0.5);
    }];
    
    unreadView = [[UIImageView alloc] init];
    unreadView.hidden = YES;
    unreadView.layer.cornerRadius = 3;
    unreadView.layer.masksToBounds = YES;
    unreadView.backgroundColor = DHRedColorff6f00;
    [self.contentView addSubview:unreadView];
    [unreadView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(6, 6));
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.centerY.mas_equalTo(self.contentView).mas_offset(3);
    }];
}

-(void)cellConfigureFor:(id)model{
   
    messageModel = (FWUnionMessageModel *)model;
    
    self.nameLabel.text = messageModel.r_user_info.nickname;
    self.signLabel.text = messageModel.msg_text;
    self.timeLabel.text = messageModel.format_create_time;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:messageModel.r_user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];
    
    if([messageModel.r_user_info.cert_status isEqualToString:@"2"]) {
        self.nameLabel.textColor = FWTextColor_FFAF3C;
        self.authenticationImageView.hidden = NO;
        self.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        if ([messageModel.r_user_info.merchant_cert_status isEqualToString:@"3"] &&
            ![messageModel.r_user_info.cert_status isEqualToString:@"2"]){
            self.nameLabel.textColor = FWTextColor_2B98FA;
            self.authenticationImageView.hidden = YES;
        }else{
            self.authenticationImageView.hidden = YES;
            self.nameLabel.textColor = FWTextColor_000000;
        }
    }
    
    if ([messageModel.read_status isEqualToString:@"1"]) {
        self.unreadView.hidden = YES;
    }else{
        self.unreadView.hidden = NO;
    }
}

#pragma mark - > 点击用户头像
- (void)photoImageViewTapClick{
    
    if (nil == self.messageModel.r_uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.messageModel.r_uid;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}

@end
