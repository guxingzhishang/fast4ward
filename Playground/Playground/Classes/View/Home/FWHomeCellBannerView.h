//
//  FWHomeCellBannerView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWHomeCellBannerViewDelegate <NSObject>

- (void)bannerClick:(NSInteger)index;

@end

@interface FWHomeCellBannerView : UIView<UIScrollViewDelegate>

@property (nonatomic, weak) id<FWHomeCellBannerViewDelegate>bannerDelegate;

@property (nonatomic, strong) UIImageView * firstIV;
@property (nonatomic, strong) UIImageView * secondIV;
@property (nonatomic, strong) UIImageView * thirdIV;
@property (nonatomic, strong) UIImageView * fourthIV;
@property (nonatomic, strong) UIImageView * fifthIV;
@property (nonatomic, strong) UIImageView * sixthIV;
@property (nonatomic, strong) UIImageView * seventhIV;
@property (nonatomic, strong) UIImageView * eighthIV;
@property (nonatomic, strong) UIImageView * ninethIV;

@property (nonatomic, strong) NSArray * imageArr;
@property (nonatomic, assign) NSInteger  imageCount;

- (void)configForBanner:(NSArray *)imageArray;
- (CGFloat)getCurrentHeight;

@end

NS_ASSUME_NONNULL_END
