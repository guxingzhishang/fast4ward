//
//  FWHomeRecomendView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWModuleButton.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^heightBlock)(CGFloat height);

@interface FWHomeRecomendView : UIView<SDCycleScrollViewDelegate,UIScrollViewDelegate>

/* 轮播图 */
@property (nonatomic, strong) SDCycleScrollView * bannerView;

@property (nonatomic, strong) UIImageView * emergencyImageView;

@property (nonatomic, strong) UILabel * anliLabel;

@property (nonatomic, strong) UIScrollView * gaizhuangView;
@property (nonatomic, strong) FWCustomPageControl *pageControl;

@property (nonatomic, strong) UIView * questionView;

@property (nonatomic, strong) UILabel * guessLabel;


@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, assign) CGFloat  currentHeight;
@property (nonatomic, assign) CGFloat  currentAnliHeight;
@property (nonatomic, assign) CGFloat  currentScrollViewHeight;

@property (nonatomic, strong) FWFeedModel * bannerModel;

@property (nonatomic, assign) BOOL  isPush;// 是否跳转到了图集

@property (nonatomic, copy) heightBlock myBlock;

- (void)configViewForModel:(FWFeedModel *)model;

@end

NS_ASSUME_NONNULL_END
