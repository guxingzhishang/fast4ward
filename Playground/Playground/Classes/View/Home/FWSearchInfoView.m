//
//  FWSearchInfoView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/19.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWSearchInfoView.h"
#import "FWSearchResultViewController.h"

@implementation FWSearchInfoView
@synthesize firstLabel;
@synthesize firstContainer;
@synthesize secondContainer;
@synthesize secondLabel;
@synthesize containerOneArr;
@synthesize containerTwoArr;

- (id)init{
    self = [super init];
    if (self) {
        
        containerOneArr = @[].mutableCopy;
        containerTwoArr = @[].mutableCopy;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    [self createSecondView];
    [self createFirstView];
}

- (void)createFirstView{
    
    firstLabel = [[UILabel alloc] init];
    firstLabel.text = @"历史搜索";
    firstLabel.font = DHSystemFontOfSize_14;
    firstLabel.textColor = FWTextColor_A6A6A6;
    firstLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:firstLabel];
    [firstLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(16);
        make.top.mas_equalTo(secondContainer.mas_bottom).mas_offset(23);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    firstContainer = [[UIView alloc] init];
    firstContainer.tag = 325;
    firstContainer.backgroundColor = [UIColor clearColor];
    [self addSubview:firstContainer];
    [firstContainer mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(firstLabel.mas_bottom);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(1);
        make.bottom.mas_equalTo(self);
    }];
}

- (void)createSecondView{
    
    secondContainer = [[UIView alloc] init];
    secondContainer.tag = 327;
    secondContainer.backgroundColor = [UIColor clearColor];
    [self addSubview:secondContainer];
    [secondContainer mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self).mas_offset(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(50);
    }];
    
    secondLabel = [[UILabel alloc] init];
    secondLabel.text = @"热门推荐";
    secondLabel.font = DHSystemFontOfSize_14;
    secondLabel.textColor = FWTextColor_969696;
    secondLabel.textAlignment = NSTextAlignmentLeft;
    [secondContainer addSubview:secondLabel];
    [secondLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(secondContainer).mas_offset(16);
        make.top.mas_equalTo(secondContainer).mas_offset(23);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
}

- (void)configViewWithModel:(NSMutableArray *)tagsList{
    
    NSData * data = [GFStaticData getObjectForKey:Search_History_Array];
    NSMutableArray *userDefaultsArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (userDefaultsArr.count > 0 ) {
        
        if (containerOneArr.count >0 ) {
            [containerOneArr removeAllObjects];
        }
        
        for (NSDictionary * keywordDict in userDefaultsArr) {
            
            if (keywordDict[@"keyword"]) {
                [containerOneArr addObject:keywordDict[@"keyword"]];
            }
        }
    }
    
    if (containerTwoArr.count >0 ) {
        [containerTwoArr removeAllObjects];
    }
    
    if (tagsList.count > 0) {
        for (NSDictionary * dict in tagsList) {
            [containerTwoArr addObject:dict[@"keyword"]];
        }
    }

    [self refeshView];
}

- (void)refeshView{
   
    [self refreshContainer:firstContainer WithArray:containerOneArr withIndex:1];
    [self refreshContainer:secondContainer withLabel:secondLabel WithArray:containerTwoArr withIndex:2];
}

- (void)refreshContainer:(UIView *)container WithArray:(NSArray *)array withIndex:(NSInteger)index{
 
    for (UIView * view in container.subviews) {
        [view removeAllSubviews];
    }
    
    NSInteger count = array.count;
    
    if (count == 0) {
        firstLabel.hidden = YES;
    }else{
        firstLabel.hidden = NO;
    }
    
    for (int i = 0; i < count; i ++) {
        
        NSString * string = [NSString stringWithFormat:@"%@    ",array[i]];
        FWBaseCellView * cellView = [[FWBaseCellView  alloc] initWithTitle:string WithImage:@""];
        cellView.titleLabel.textColor = FWColorWihtAlpha(@"222222", 0.7);
        cellView.titleLabel.backgroundColor = FWViewBackgroundColor_EEEEEE;
        cellView.titleLabel.textAlignment = NSTextAlignmentCenter;
        cellView.titleLabel.layer.cornerRadius = 2;
        cellView.titleLabel.layer.masksToBounds = YES;
        cellView.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];;
        cellView.titleLabel.userInteractionEnabled = NO;
        cellView.cellButton.tag = 338+i;
        cellView.arrowImageView.tag = 380+i;
        cellView.arrowImageView.userInteractionEnabled = YES;
        cellView.rightLabel.hidden = YES;
        cellView.iconImageView.hidden = YES;
        cellView.lineView.hidden = YES;
        cellView.cellButton.hidden = NO;
        [cellView.cellButton addTarget:self action:@selector(cellButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        cellView.arrowImageView.image = [UIImage imageNamed:@"new_search_close"];
        [container addSubview:cellView];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteCellOnClick:)];
        [cellView.arrowImageView addGestureRecognizer:tap];
        
        [cellView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(container);
            make.height.mas_equalTo(48);
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.top.mas_equalTo(container).mas_offset(i * 48);
        }];
        
        [cellView.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(cellView);
            make.right.mas_equalTo(container).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(14, 14));

        }];
        
        [cellView.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cellView).mas_offset(17);
            make.width.mas_greaterThanOrEqualTo(72);
            make.height.mas_equalTo(30);
            make.centerY.mas_equalTo(cellView);
        }];
        
        [cellView.cellButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cellView.titleLabel);
            make.top.bottom.mas_equalTo(cellView);
            make.right.mas_equalTo(cellView.arrowImageView.mas_left);
        }];
    }
    
    [container mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(firstLabel.mas_bottom);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self);
        if (count == 0) {
            make.height.mas_equalTo(1);
        }else{
            make.height.mas_equalTo(48*count);
        }
    }];
}

/**
 更新推荐标签列表
 
 @param container 哪个容器
 @param label 哪个容器下的label
 @param array 该容器内的标签数组
 @param index 第几个容器
 */
- (void) refreshContainer:(UIView *)container withLabel:(UILabel *)label WithArray:(NSArray *)array withIndex:(NSInteger)index{
    
    for (UIView * view in container.subviews) {
        if ([view isKindOfClass:[UIButton class]] &&
            view.tag != 4399) {
            [view removeFromSuperview];
        }
    }
    
    CGFloat x = 16.5;
    CGFloat y = 20;
    CGFloat width = 0.0;
    CGFloat height = 32.0;
    
    for (int i = 0; i < array.count; i++) {
        
        width = [array[i] length]*14+30;
        
        if (x + width+16.5 >= SCREEN_WIDTH) {
            x = 16.5;
            y += height + 12;
        }
        
        UIButton * btn = [[UIButton alloc] init];
        btn.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        btn.backgroundColor = FWViewBackgroundColor_EEEEEE;
        btn.tag = 10000+i;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        [btn setTitle:[NSString stringWithFormat:@"%@",array[i]] forState:UIControlStateNormal];
        [btn setTitleColor:FWColorWihtAlpha(@"222222", 0.7) forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(tagsBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [container addSubview:btn];
        [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(container).mas_equalTo(x);
            make.top.mas_equalTo(label.mas_bottom).mas_equalTo(y);
            make.size.mas_equalTo(CGSizeMake(width, height));
        }];
        
        if (i == array.count-1  && array.count>0) {
            [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(container).mas_equalTo(x);
                make.top.mas_equalTo(label.mas_bottom).mas_equalTo(y);
                make.size.mas_equalTo(CGSizeMake(width, height));
                make.bottom.mas_equalTo(container).mas_offset(-20);
            }];
        }
        
        x += width+10.5;
    }
}

#pragma mark - > 历史记录
- (void)cellButtonOnClick:(UIButton *)sender{
 
    UIView * superView =  [[(UIButton *)sender superview] superview];
    NSInteger superTag = superView.tag;
    
    NSInteger index = sender.tag - 338;
    
    if ([self.infoDelegate respondsToSelector:@selector(tagsBtnOnClickWithSuperIndex:WithSubIndex:)]) {
        [self.infoDelegate tagsBtnOnClickWithSuperIndex:superTag WithSubIndex:index];
    }
}

#pragma mark - > 删除某条历史记录
- (void)deleteCellOnClick:(UIGestureRecognizer *)gesture{
    
    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer *)gesture;
    NSInteger index = singleTap.view.tag - 380;
    
    NSData * data = [GFStaticData getObjectForKey:Search_History_Array];
    NSMutableArray *userDefaultsArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (userDefaultsArr.count > index) {
        
        [userDefaultsArr removeObjectAtIndex:index];
        data = [NSKeyedArchiver archivedDataWithRootObject:userDefaultsArr];
        [GFStaticData saveObject:data forKey:Search_History_Array];
    }
    
    if (containerOneArr.count > index) {
        [containerOneArr removeObjectAtIndex:index];
        [self refeshView];
    }
}

#pragma mark - > 选择的标签
- (void)tagsBtnOnClick:(UIButton *)sender{
    
    NSInteger btnTag = sender.tag - 10000;
    
    UIView * superView =  (UIButton *)sender.superview;
    NSInteger superTag = superView.tag;
    
    if ([self.infoDelegate respondsToSelector:@selector(tagsBtnOnClickWithSuperIndex:WithSubIndex:)]) {
        [self.infoDelegate tagsBtnOnClickWithSuperIndex:superTag WithSubIndex:btnTag];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.vc.view endEditing:YES];
}
@end
