//
//  FWCommentFooterView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/17.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class FWCommentFooterView;
@protocol FWCommentFooterViewDelegate <NSObject>

- (void) showButtonOnClickWithIndex:(NSInteger)index WithFooter:(FWCommentFooterView *)footer;

@end

@interface FWCommentFooterView : UITableViewHeaderFooterView

+ (instancetype)topicFooterView;
+ (instancetype)footerViewWithTableView:(UITableView *)tableView;

@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UIButton * showButton;

@property (nonatomic, weak) UIViewController * viewcontroller;
@property (nonatomic, weak) id<FWCommentFooterViewDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
