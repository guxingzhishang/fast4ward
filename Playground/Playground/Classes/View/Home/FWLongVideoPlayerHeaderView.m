//
//  FWLongVideoPlayerHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWLongVideoPlayerHeaderView.h"
#import "FWCommentViewController.h"

@implementation FWLongVideoPlayerHeaderView
@synthesize backButton;
@synthesize photoImageView;
@synthesize attentionButton;
@synthesize nameLabel;
@synthesize vipImageButton;
@synthesize contentLabel;
@synthesize containerView;
@synthesize containerArr;
@synthesize commentLabel;
@synthesize commentNumberButton;
@synthesize vc;
@synthesize containerHeight;
@synthesize lineView;
@synthesize listModel;
@synthesize timeLabel;
@synthesize goodsView;
@synthesize goodsScanView;
@synthesize goodsImageView;
@synthesize goodsScanLabel;
@synthesize goodsPriceLabel;
@synthesize goodsTitleLabel;
@synthesize recammondLabel;
@synthesize goodsLineView;

- (id)init{
    self = [super init];
    if (self) {
        containerHeight = 1;
        containerArr = @[].mutableCopy;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.layer.cornerRadius = 18;
    photoImageView.layer.masksToBounds = YES;
    photoImageView.userInteractionEnabled = YES;
    photoImageView.image = [UIImage imageNamed:@"placeholder"];
    photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:photoImageView];
    photoImageView.frame = CGRectMake(12, 25, 36, 36);
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageTap:)];
    [photoImageView addGestureRecognizer:tap];
    
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.font = DHBoldSystemFontOfSize_14;
    nameLabel.textColor = FWTextColor_000000;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:nameLabel];
    nameLabel.frame = CGRectMake(CGRectGetMaxX(self.photoImageView.frame)+5, CGRectGetMinY(self.photoImageView.frame), 200, 36);
    [nameLabel sizeToFit];
    
    self.vipImageButton = [[UIButton alloc] init];
    [self.vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.nameLabel);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
    
    attentionButton = [[UIButton alloc] init];
    attentionButton.layer.cornerRadius = 2;
    attentionButton.layer.masksToBounds = YES;
    attentionButton.titleLabel.font = DHBoldSystemFontOfSize_14;
    attentionButton.backgroundColor = FWTextColor_222222;
    [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [attentionButton addTarget:self action:@selector(attentionButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:attentionButton];
    attentionButton.frame = CGRectMake(SCREEN_WIDTH-12-67, CGRectGetMinY(self.photoImageView.frame)+3, 69, 30);
    
    contentLabel = [[UILabel alloc] init];
    contentLabel.numberOfLines = 0;
    contentLabel.font = DHBoldSystemFontOfSize_14;
    contentLabel.textColor = FWTextColor_000000;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:contentLabel];
    contentLabel.frame = CGRectMake(CGRectGetMinX(self.photoImageView.frame), CGRectGetMaxY(self.photoImageView.frame)+20, SCREEN_WIDTH-40, 20);
    
    containerView = [[UIView alloc] init];
    [self addSubview:containerView];
    containerView.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame)+5, CGRectGetWidth(self.contentLabel.frame), 40);
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.text = @"";
    timeLabel.font = DHSystemFontOfSize_12;
    timeLabel.textColor = FWTextColor_969696;
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:timeLabel];
    timeLabel.frame = CGRectMake(CGRectGetMinX(self.photoImageView.frame),CGRectGetMaxY(containerView.frame)+10 , 200, 20);
    
    lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self addSubview:lineView];
    lineView.frame = CGRectMake(0,CGRectGetMaxY(self.timeLabel.frame)+10, SCREEN_WIDTH, 0.5);
    
    goodsView = [[UIView alloc] init];
    goodsView.clipsToBounds = YES;
    goodsView.userInteractionEnabled = YES;
    goodsView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:goodsView];
    goodsView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), SCREEN_WIDTH, 130);
    UITapGestureRecognizer * goodstap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goodsViewClick)];
    [goodsView addGestureRecognizer:goodstap];
    
    recammondLabel = [[UILabel alloc] init];
    recammondLabel.text = @"推荐商品";
    recammondLabel.textColor = FWTextColor_000000;
    recammondLabel.textAlignment = NSTextAlignmentLeft;
    recammondLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [goodsView addSubview:recammondLabel];
    recammondLabel.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), 0, 100, 25);
    
    goodsImageView = [[UIImageView alloc] init];
    goodsImageView.layer.cornerRadius = 2;
    goodsImageView.layer.masksToBounds = YES;
    goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
    [goodsView addSubview:goodsImageView];
    goodsImageView.frame = CGRectMake(CGRectGetMinX(recammondLabel.frame), CGRectGetMaxY(recammondLabel.frame) +5, 88, 88);
    
    goodsTitleLabel = [[UILabel alloc] init];
    goodsTitleLabel.textColor = FWTextColor_000000;
    goodsTitleLabel.numberOfLines = 2;
    goodsTitleLabel.textAlignment = NSTextAlignmentLeft;
    goodsTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [goodsView addSubview:goodsTitleLabel];
    [goodsTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(goodsImageView).mas_offset(12);
        make.height.mas_greaterThanOrEqualTo (10);
        make.right.mas_equalTo(goodsView).mas_offset(-12);
        make.width.mas_equalTo(SCREEN_WIDTH-CGRectGetMaxX(goodsImageView.frame)-24);
    }];
    
    goodsPriceLabel = [[UILabel alloc] init];
    goodsPriceLabel.textColor = DHRedColorff6f00;
    goodsPriceLabel.textAlignment = NSTextAlignmentLeft;
    goodsPriceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    [goodsView addSubview:goodsPriceLabel];
    [goodsPriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(goodsImageView).mas_offset(-10);
        make.height.mas_equalTo (20);
        make.left.mas_equalTo(goodsTitleLabel);
        make.width.mas_equalTo(150);
    }];
    
    goodsScanLabel = [[UILabel alloc] init];
    goodsScanLabel.textColor = FWTextColor_9C9C9C;
    goodsScanLabel.textAlignment = NSTextAlignmentRight;
    goodsScanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [goodsView addSubview:goodsScanLabel];
    [goodsScanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(goodsPriceLabel);
        make.height.mas_equalTo (20);
        make.right.mas_equalTo(goodsView).mas_offset(-12);
        make.width.mas_greaterThanOrEqualTo(5);
    }];
    
    goodsScanView = [[UIImageView alloc] init];
    goodsScanView.image = [UIImage imageNamed:@"card_eye"];
    [goodsView addSubview:goodsScanView];
    [goodsScanView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(14, 10));
        make.right.mas_equalTo(goodsScanLabel.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(goodsPriceLabel);
    }];
    
    goodsLineView = [UIView new];
    goodsLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self addSubview:goodsLineView];
    goodsLineView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame)-1, SCREEN_WIDTH, 0.5);
    
    goodsView.hidden = YES;
    
    commentLabel = [[UILabel alloc] init];
    commentLabel.text = @"评论";
    commentLabel.font = DHSystemFontOfSize_16;
    commentLabel.textColor = FWTextColor_000000;
    commentLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:commentLabel];
    commentLabel.frame = CGRectMake(CGRectGetMinX(self.photoImageView.frame),CGRectGetMaxY(lineView.frame)+5 , 100, 20);
    
    commentNumberButton = [[UIButton alloc] init];
    commentNumberButton.titleLabel.textAlignment = NSTextAlignmentRight;
    commentNumberButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    commentNumberButton.titleLabel.font = DHSystemFontOfSize_12;
    [commentNumberButton setTitle:@"0" forState:UIControlStateNormal];
    [commentNumberButton setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
    [commentNumberButton addTarget:self action:@selector(commentNumberButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:commentNumberButton];
    [commentNumberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-12);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(30);
        make.centerY.mas_equalTo(commentLabel);
    }];
}

- (void)configViewForModel:(id)model{
    
    listModel = (FWFeedListModel *)model;
//
//    // 视频
////    playerView
//
//    if ([listModel.imgs[0].img_width floatValue] == 0) {
//        listModel.imgs[0].img_width = @(SCREEN_WIDTH).stringValue;
//    }
//    if ([listModel.imgs[0].img_height floatValue] == 0) {
//        listModel.imgs[0].img_height = @(SCREEN_WIDTH).stringValue;
//    }
//
//    CGFloat k = [listModel.imgs[0].img_width floatValue]/[listModel.imgs[0].img_height floatValue];
//    CGFloat picHight = SCREEN_WIDTH;
//    CGFloat picWidth = SCREEN_WIDTH;
//
//    if ( k>= 0.667 && k<= 1.5) {
//        picHight = picWidth / k;
//    }else if( k < 0.667) {
//        picHight = picWidth *1.5;
//    }else if (k >1.5){
//        picHight = picWidth *0.667;
//    }
//    playerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, picHight);
    
    photoImageView.frame = CGRectMake(12, 25, 36, 36);
    nameLabel.frame = CGRectMake(CGRectGetMaxX(self.photoImageView.frame)+5, CGRectGetMinY(self.photoImageView.frame), 200, 36);
    attentionButton.frame = CGRectMake(SCREEN_WIDTH-12-67, CGRectGetMinY(self.photoImageView.frame)+3, 69, 30);
    
    if ([listModel.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        // 自己不显示关注
        attentionButton.hidden = YES;
    }else{
        attentionButton.hidden = NO;
    }
    
    nameLabel.text = listModel.user_info.nickname;
    contentLabel.text = listModel.feed_title;
    timeLabel.text = listModel.format_create_time;
    
    [nameLabel sizeToFit];
    
    self.vipImageButton.enabled = YES;
    if ([listModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([listModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([listModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    NSString * comment_count = listModel.count_realtime.comment_count?listModel.count_realtime.comment_count:@"0";
    
    [commentNumberButton setTitle:[NSString stringWithFormat:@"%@ 条评论",comment_count] forState:UIControlStateNormal];
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];
    
    containerArr = listModel.tags;
    
    // 关注
    if ([listModel.is_followed isEqualToString:@"2"]) {
        attentionButton.backgroundColor = FWTextColor_222222;
        [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    }else{
        attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
        [attentionButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
        [attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
    }
    
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:contentLabel.attributedText];
    
    CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 40].height;
    
    //展示文字或隐藏文字
    contentLabel.frame = CGRectMake(CGRectGetMinX(self.photoImageView.frame), CGRectGetMaxY(self.photoImageView.frame)+10, SCREEN_WIDTH-40,textHeight);
    
    
    // 标签
    [self refreshContainer:containerView WithArray:containerArr];
    
    containerView.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame)+5, CGRectGetWidth(self.contentLabel.frame), containerHeight);
    
    timeLabel.frame = CGRectMake(CGRectGetMinX(self.photoImageView.frame),CGRectGetMaxY(containerView.frame)+10 , 200, 20);
    
    lineView.frame = CGRectMake(0,CGRectGetMaxY(timeLabel.frame)+10, SCREEN_WIDTH, 0.5);
    
    if (listModel.goods_id.length > 0 && ![listModel.goods_id isEqualToString:@"0"]) {
        
        goodsTitleLabel.text = listModel.goods_info.title;
        goodsPriceLabel.text = [NSString stringWithFormat:@"￥%@",listModel.goods_info.price];
        goodsScanLabel.text = listModel.goods_info.click_count;
        [goodsImageView sd_setImageWithURL:[NSURL URLWithString:listModel.goods_info.cover.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        goodsView.hidden = NO;
        goodsLineView.hidden = NO;
        
        goodsView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), SCREEN_WIDTH, 130);
        goodsLineView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame)-1, SCREEN_WIDTH, 0.5);
        
        commentLabel.frame = CGRectMake(CGRectGetMinX(self.photoImageView.frame),CGRectGetMaxY(goodsLineView.frame)+5 , 100, 20);
    }else{
        
        goodsView.hidden = YES;
        commentLabel.frame = CGRectMake(CGRectGetMinX(self.photoImageView.frame),CGRectGetMaxY(lineView.frame)+5 , 100, 20);
        goodsLineView.hidden = YES;
    }
    
    
    [commentNumberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-12);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(30);
        make.centerY.mas_equalTo(commentLabel);
    }];
}


- (void) refreshContainer:(UIView *)container WithArray:(NSArray *)array{
    
    for (UIView * view in container.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    CGFloat x = 0;
    CGFloat y = 5;
    CGFloat width = 0.0;
    CGFloat height = 24;
    
    for (int i = 0; i < array.count; i++) {
        FWSearchTagsListModel * tagsModel = array[i];
        
        width = [tagsModel.tag_name length]*14+20;
        
        if (x + width+20 >= SCREEN_WIDTH) {
            x = 0;
            y += height + 10;
        }
        
        UIButton * btn = [[UIButton alloc] init];
        btn.titleLabel.font = DHSystemFontOfSize_14;
        btn.backgroundColor = FWViewBackgroundColor_FFFFFF;
        btn.tag = 10000+i;
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        btn.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
        [btn setTitle:[NSString stringWithFormat:@" %@",tagsModel.tag_name] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnTagsClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
        [container addSubview:btn];
        
        btn.frame = CGRectMake(x, y, width, height);
        
        x += width+10;
    }
    
    containerHeight = y + 30;
}

#pragma mark - > 跳转商品
- (void)goodsViewClick{
    
    FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
    GDVC.goods_id = listModel.goods_id;
    [self.vc.navigationController pushViewController:GDVC animated:YES];
}

#pragma mark - > 关注
- (void)attentionButtonOnClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([self.headerDelegate respondsToSelector:@selector(attentionButtonClick)]) {
            [self.headerDelegate attentionButtonClick];
        }
    }
}

#pragma mark - > 点击头像
- (void)photoImageTap:(UITapGestureRecognizer *)gesture{
    if (nil == self.listModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 点击标签
- (void)btnTagsClick:(UIButton *)sender{
    
    FWSearchTagsSubListModel * tagsModel = self.listModel.tags[sender.tag-10000];
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.traceType = 2;
    TVC.tagsModel = tagsModel;
    TVC.tags_id = tagsModel.tag_id;
    [self.viewController.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 点击进入评论
- (void)commentNumberButtonOnClick{
    
    FWCommentViewController * vc = [[FWCommentViewController alloc] init];
    vc.feed_id = self.listModel.feed_id;
    vc.listModel = self.listModel;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}


- (CGFloat)getCurrentViewHeight{
    
    CGFloat height = 0;
    
    height = CGRectGetMaxY(commentLabel.frame) +10;
    
    return height;
}

@end
