//
//  FWViewPictureView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/16.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWViewPictureView.h"
#import "FWBrowserImageView.h"

@interface FWViewPictureView()

@property (nonatomic,strong) UILongPressGestureRecognizer *longPressTap;

@end

@implementation FWViewPictureView
@synthesize backButton;
@synthesize numLabel;
@synthesize shareButton;
@synthesize imageScrollView;
@synthesize photoImageView;
@synthesize nameLabel;
@synthesize attentionButton;
@synthesize listModel;

- (UILongPressGestureRecognizer *)longPressTap{
    if (!_longPressTap) {
        _longPressTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressClick:)];
    }
    return _longPressTap;
}

- (id)init{
    self = [super init];
    if (self) {
        self.imageCount = 0;
        self.currentPagenum = 0;
        self.endContentOffsetX = 0;
        
        self.backgroundColor = FWTextColor_000000;

        [self setupSubviews];
        [self addGestureRecognizer:self.longPressTap];

    }
    
    return self;
}

- (void)setupSubviews{
    
    {
        backButton = [[UIButton alloc] init];
        [backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backButton];
        [backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self).mas_offset(20+FWCustomeSafeTop);
            make.left.mas_equalTo(self).mas_offset(10);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
    }
    
    {
        numLabel = [[UILabel alloc] init];
        numLabel.layer.cornerRadius = 2;
        numLabel.layer.masksToBounds = YES;
        numLabel.font = DHSystemFontOfSize_18;
        numLabel.textColor = FWViewBackgroundColor_FFFFFF;
        numLabel.backgroundColor = [UIColor colorWithHexString:@"FFFFFF" alpha:0.11];
        numLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:numLabel];
        [numLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.centerY.mas_equalTo(backButton);
            make.size.mas_equalTo(CGSizeMake(60, 25));
        }];
    }
    
    {
        shareButton = [[UIButton alloc] init];
        [shareButton setImage:[UIImage imageNamed:@"white_more"] forState:UIControlStateNormal];
        [shareButton addTarget:self action:@selector(shareButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:shareButton];
        [shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).mas_offset(-20);
            make.centerY.mas_equalTo(backButton);
            make.size.mas_equalTo(CGSizeMake(30, 30));
        }];
        
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
            shareButton.hidden = NO;
        }else{
            shareButton.hidden = YES;
        }
    }
    
    {
        photoImageView = [[UIImageView alloc] init];
        photoImageView.userInteractionEnabled = YES;
        photoImageView.layer.cornerRadius = 20;
        photoImageView.layer.masksToBounds = YES;
        [self addSubview:photoImageView];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapClick)];
        [photoImageView addGestureRecognizer:tap];
        
        [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).mas_offset(20);
            make.bottom.mas_equalTo(self).mas_offset(-30);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
    }
    
    {
        nameLabel = [[UILabel alloc] init];
        nameLabel.font = DHSystemFontOfSize_15;
        nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
        nameLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:nameLabel];
        [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(photoImageView.mas_right).mas_offset(10);
            make.centerY.mas_equalTo(photoImageView);
            make.size.mas_equalTo(CGSizeMake(150, 20));
        }];
    }
    
    {
        attentionButton = [[UIButton alloc] init];
        attentionButton.layer.cornerRadius = 2;
        attentionButton.layer.masksToBounds = YES;
        attentionButton.titleLabel.font = DHSystemFontOfSize_14;
        attentionButton.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.5);
        [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
        [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        [attentionButton addTarget:self action:@selector(attentionButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:attentionButton];
        [attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(photoImageView);
            make.size.mas_equalTo(CGSizeMake(69, 30));
            make.right.mas_equalTo(self).mas_offset(-20);
        }];
    }

    {
        imageScrollView = [[UIScrollView alloc] init];
        imageScrollView.delegate = self;
        imageScrollView.pagingEnabled = YES;
        imageScrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:imageScrollView];
        if (SCREEN_HEIGHT>=812) {
            imageScrollView.frame = CGRectMake(0, 120+FWCustomeSafeTop, SCREEN_WIDTH, SCREEN_WIDTH*4/3);
        }else{
            imageScrollView.frame = CGRectMake(0, 80+FWCustomeSafeTop, SCREEN_WIDTH, SCREEN_WIDTH*4/3);
        }
    }
}

- (void)configViewWithModel:(id)model{
    
    listModel = (FWFeedListModel *)model;
    
    if ([listModel.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        // 自己不显示关注
        attentionButton.hidden = YES;
    }else{
        attentionButton.hidden = NO;
    }
    
    if ([listModel.feed_type isEqualToString:@"2"] ||
        [listModel.feed_type isEqualToString:@"4"] ||
        [listModel.feed_type isEqualToString:@"5"] ) {
        self.imageCount = listModel.imgs_original.count;
    }else if ([listModel.feed_type isEqualToString:@"3"]){
        self.imageCount = listModel.forum_albums.count;
    }

    if (self.imageCount >0) {
        if ([listModel.feed_type isEqualToString:@"2"]) {
            [self createPicView:listModel.imgs_original withType:@"2"];
        }else if ([listModel.feed_type isEqualToString:@"3"]){
            [self createPicView:listModel.forum_albums withType:@"3"];
        }else if ([listModel.feed_type isEqualToString:@"4"]){
            [self createPicView:listModel.imgs_original withType:@"4"];
        }else if ([listModel.feed_type isEqualToString:@"5"]){
            [self createPicView:listModel.imgs_original withType:@"5"];
        }
        numLabel.text = [NSString stringWithFormat:@"1/%ld",(long)self.imageCount];
    }else{
        numLabel.text = @"-/-";
    }
    
    nameLabel.text = listModel.user_info.nickname;
    
    [photoImageView sd_setImageWithURL:[NSURL URLWithString:listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    // 关注
    if ([listModel.is_followed isEqualToString:@"2"]) {
        attentionButton.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.5);
        [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
        [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    }else{
        attentionButton.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.3);
        [attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
        [attentionButton setTitleColor:FWColorWihtAlpha(@"ffffff", 0.5) forState:UIControlStateNormal];
    }
}

- (void)createPicView:(NSArray *)imageArray withType:(NSString *)type{
    
    for (UIView * imageView in imageScrollView.subviews) {
        if ([imageView isKindOfClass:[UIImageView class]]) {
            [imageView removeFromSuperview];
        }
    }
    imageScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * imageArray.count, 0);
    for(int i =0 ; i<imageArray.count; i++){
        NSString * urlString = @"";
        if ([type isEqualToString:@"2"]||
            [type isEqualToString:@"4"]||
            [type isEqualToString:@"5"]) {
            urlString = ((FWFeedImgsModel *)imageArray[i]).img_url;
        }else if ([type isEqualToString:@"3"]){
            urlString = ((FWForumAlbumsModel *)imageArray[i]).pic;
        }
        
        FWBrowserImageView *imageView = [[FWBrowserImageView alloc] init];
        imageView.tag = i+8888;
        imageView.userInteractionEnabled = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.frame = CGRectMake(SCREEN_WIDTH*i, 0, SCREEN_WIDTH, CGRectGetHeight(imageScrollView.frame));

        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapAction:)];
        doubleTap.numberOfTapsRequired = 2;
        
        [imageView addGestureRecognizer:doubleTap];
        [imageScrollView addSubview:imageView];
        

        UIImageView * siIcon = [[UIImageView alloc] init];
        siIcon.image = [UIImage imageNamed:@"placeholder_si"];
        [imageView addSubview:siIcon];
        [siIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(imageView);
            make.centerY.mas_equalTo(imageView);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        siIcon.hidden = YES;
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:urlString] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
            if (!image) {
                siIcon.hidden = NO;
                imageView.backgroundColor = [UIColor colorWithHexString:@"E7E7E7"];
            }else{
                siIcon.hidden = YES;
            }
        }];
    }
}


#pragma mark - > 长按
- (void)longPressClick:(UILongPressGestureRecognizer *)recognizer{
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // do something
        [self endEditing:YES];
        [self shareButtonOnClick];
    }
}

#pragma mark - > 双击图片
- (void)handleDoubleTapAction:(UIGestureRecognizer *)gesture{
    FWBrowserImageView *imageView = (FWBrowserImageView *)gesture.view;
    CGFloat scale;
    if (imageView.isScaled) {
        scale = 1.0;
    } else {
        scale = 2.0;
    }
    
    FWBrowserImageView *view = (FWBrowserImageView *)gesture.view;
    
    [view doubleTapToZoomWithScale:scale ];
}


#pragma mark - > 返回
- (void)backButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(backButtonClick)]) {
        [self.delegate backButtonClick];
    }
}

//#pragma mark - > 保存图片
//- (void)saveButtonOnClick{
//
//    UIImageView * imageView = (UIImageView *)[self viewWithTag:self.currentPagenum+8888];
//    UIImageWriteToSavedPhotosAlbum(imageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
//}

//保存成功调用的方法
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{

    [[FWHudManager sharedManager] showSuccessMessage:@"保存成功" toController:self.vc];
}
#pragma mark - > 分享
- (void)shareButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(shareButtonClick)]) {
        [self.delegate shareButtonClick];
    }
}

#pragma mark - > 头像
- (void)photoImageViewTapClick{
    if (nil == self.listModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 关注
- (void)attentionButtonOnClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSString * action ;
        
        if ([listModel.is_followed isEqualToString:@"2"]) {
            action = Submit_follow_users;
        }else{
            action = Submit_cancel_follow_users;
        }
        
        FWFollowRequest * request = [[FWFollowRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"f_uid":self.listModel.uid,
                                  };
        [request startWithParameters:params WithAction:action WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([action isEqualToString:Submit_follow_users]) {
                    self.listModel.is_followed = @"1";
                    attentionButton.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.3);
                    [attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
                    [attentionButton setTitleColor:FWColorWihtAlpha(@"ffffff", 0.5) forState:UIControlStateNormal];

                    [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self.viewController];
                }else{
                    self.listModel.is_followed = @"2";
                    attentionButton.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.5);
                    [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
                    [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
                }
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    self.endContentOffsetX = scrollView.contentOffset.x;

    self.currentPagenum = round(self.endContentOffsetX/SCREEN_WIDTH);

    numLabel.text = [NSString stringWithFormat:@"%ld/%ld",(long)self.currentPagenum+1,(long)self.imageCount];
}


- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
