//
//  FWIdleCollectionCell.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/17.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWCollecitonBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWIdleCollectionCell : FWCollecitonBaseCell

+ (NSString *)cellIdentifier;

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
