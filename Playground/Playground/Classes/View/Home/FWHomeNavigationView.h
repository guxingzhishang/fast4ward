//
//  FWHomeNavigationView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWHomeNavigationViewDelegate <NSObject>

- (void)searchButtonClick;

@end

@interface FWHomeNavigationView : UIView

/**
 * 搜索按钮
 */
@property (nonatomic, strong) UIButton * searchButton;

/**
 * 搜索图标
 */
@property (nonatomic, strong) UIImageView * searchImageView;

/**
 * 搜索文案
 */
@property (nonatomic, strong) UILabel * searchLabel;

/**
 * 导航栏按钮代理方法
 */
@property (nonatomic, weak) id<FWHomeNavigationViewDelegate>delegate ;


@end
