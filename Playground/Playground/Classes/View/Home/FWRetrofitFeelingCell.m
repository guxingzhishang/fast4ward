//
//  FWRetrofitFeelingCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRetrofitFeelingCell.h"

@implementation FWRetrofitFeelingCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews{
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.nameLabel.textColor = FWTextColor_12101D;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(47);
        make.centerY.mas_equalTo(self.contentView);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(self.contentView).mas_offset(-33);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.right.mas_equalTo(self).mas_offset(-15);
        make.height.mas_equalTo(0.5);
        make.width.mas_greaterThanOrEqualTo(200);
        make.bottom.mas_equalTo(self).mas_offset(-0.5);
    }];
    
    self.arrowImageView = [[UIImageView alloc] init];
    self.arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.contentView addSubview:self.arrowImageView];
    [self.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(7, 11));
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
    }];
}

@end
