//
//  FWBannerView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

@class FWGraphTextDetailViewController;

@interface FWBannerView : UIView

@property (nonatomic, strong) SDCycleScrollView *homeBanner;// 首页无线轮播图

- (instancetype)initWithCarouselDelegate:(UIViewController *)delegate;

- (void)createBannerWithDataSource:(FWHomeViewModel *)model delegate:(id<SDCycleScrollViewDelegate>)delegate;

@end
