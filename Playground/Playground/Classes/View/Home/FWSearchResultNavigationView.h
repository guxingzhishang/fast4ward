//
//  FWSearchResultNavigationView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/20.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWSearchResultNavigationViewDelegate <NSObject>

- (void)backClick;

- (void)searchResultWithText:(NSString *_Nullable)content;

- (void)selectButtonClick:(UIButton *_Nullable)sender;

- (void)selectClearButton;

@end


@interface FWSearchResultNavigationView : UIView<UISearchBarDelegate>

@property (nonatomic, strong) UISearchBar * resultSearchBar;

@property (nonatomic, strong) UIButton * backButton;

@property (nonatomic, strong) UIImageView * backImageView;

@property (nonatomic, weak) id<FWSearchResultNavigationViewDelegate> delegate;

@property (nonatomic, strong) UIView * selectView;

@property (nonatomic, strong) UIButton * comprehensiveButton;

@property (nonatomic, strong) UIButton * userButton;

@property (nonatomic, strong) UIButton * idleButton;

@property (nonatomic, strong) UIView * verOneView;

@property (nonatomic, strong) UIView * verTwoView;

@end

NS_ASSUME_NONNULL_END
