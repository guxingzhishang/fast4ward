//
//  FWRefitCaseDetailHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRefitCaseDetailHeaderView.h"
#import "FWViewPictureViewController.h"
#import "FWCommentViewController.h"

@implementation FWRefitCaseDetailHeaderView
@synthesize contentLabel;
@synthesize containerView;
@synthesize containerArr;
@synthesize commentLabel;
@synthesize commentNumberButton;
@synthesize bannerView;
@synthesize vc;
@synthesize containerHeight;
@synthesize lineView;
@synthesize listModel;
@synthesize timeLabel;
@synthesize goodsView;
@synthesize goodsScanView;
@synthesize goodsImageView;
@synthesize goodsScanLabel;
@synthesize goodsPriceLabel;
@synthesize goodsTitleLabel;
@synthesize recammondLabel;
@synthesize goodsLineView;
@synthesize gaizhuangView;
@synthesize backButton;

- (id)init{
    self = [super init];
    if (self) {
        containerHeight = 1;
        containerArr = @[].mutableCopy;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 280, SCREEN_WIDTH, SCREEN_WIDTH) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    bannerView.autoScrollTimeInterval = 4;
    bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    bannerView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    [self addSubview:bannerView];
    bannerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH);
    
    contentLabel = [[UILabel alloc] init];
    contentLabel.numberOfLines = 0;
    contentLabel.font = DHBoldSystemFontOfSize_16;
    contentLabel.textColor = FWTextColor_222222;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:contentLabel];
    contentLabel.frame = CGRectMake(12, CGRectGetMaxY(self.bannerView.frame)+20, SCREEN_WIDTH-40, 20);
    
    gaizhuangView = [[UIView alloc] init];
    [self addSubview:gaizhuangView];
    gaizhuangView.frame = CGRectMake(CGRectGetMinX(contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame)+10, SCREEN_WIDTH-40, 50);

    containerView = [[UIView alloc] init];
    [self addSubview:containerView];
    containerView.frame = CGRectMake(CGRectGetMinX(self.gaizhuangView.frame), CGRectGetMaxY(self.gaizhuangView.frame)+15, CGRectGetWidth(self.contentLabel.frame), 40);
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.text = @"";
    timeLabel.font = DHSystemFontOfSize_12;
    timeLabel.textColor = FWTextColor_969696;
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:timeLabel];
    timeLabel.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame),CGRectGetMaxY(containerView.frame)+10 , 200, 20);
    
    lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self addSubview:lineView];
    lineView.frame = CGRectMake(0,CGRectGetMaxY(self.timeLabel.frame)+10, SCREEN_WIDTH, 0.5);
    
    goodsView = [[UIView alloc] init];
    goodsView.clipsToBounds = YES;
    goodsView.userInteractionEnabled = YES;
    goodsView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:goodsView];
    goodsView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), SCREEN_WIDTH, 130);
    UITapGestureRecognizer * goodstap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goodsViewClick)];
    [goodsView addGestureRecognizer:goodstap];
    
    recammondLabel = [[UILabel alloc] init];
    recammondLabel.text = @"推荐商品";
    recammondLabel.textColor = FWTextColor_000000;
    recammondLabel.textAlignment = NSTextAlignmentLeft;
    recammondLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [goodsView addSubview:recammondLabel];
    recammondLabel.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), 0, 100, 25);
    
    goodsImageView = [[UIImageView alloc] init];
    goodsImageView.layer.cornerRadius = 2;
    goodsImageView.layer.masksToBounds = YES;
    goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
    [goodsView addSubview:goodsImageView];
    goodsImageView.frame = CGRectMake(CGRectGetMinX(recammondLabel.frame), CGRectGetMaxY(recammondLabel.frame) +5, 88, 88);
    
    goodsTitleLabel = [[UILabel alloc] init];
    goodsTitleLabel.textColor = FWTextColor_000000;
    goodsTitleLabel.numberOfLines = 2;
    goodsTitleLabel.textAlignment = NSTextAlignmentLeft;
    goodsTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [goodsView addSubview:goodsTitleLabel];
    [goodsTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(goodsImageView).mas_offset(12);
        make.height.mas_greaterThanOrEqualTo (10);
        make.right.mas_equalTo(goodsView).mas_offset(-12);
        make.width.mas_equalTo(SCREEN_WIDTH-CGRectGetMaxX(goodsImageView.frame)-24);
    }];
    
    goodsPriceLabel = [[UILabel alloc] init];
    goodsPriceLabel.textColor = DHRedColorff6f00;
    goodsPriceLabel.textAlignment = NSTextAlignmentLeft;
    goodsPriceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    [goodsView addSubview:goodsPriceLabel];
    [goodsPriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(goodsImageView).mas_offset(-10);
        make.height.mas_equalTo (20);
        make.left.mas_equalTo(goodsTitleLabel);
        make.width.mas_equalTo(150);
    }];
    
    goodsScanLabel = [[UILabel alloc] init];
    goodsScanLabel.textColor = FWTextColor_9C9C9C;
    goodsScanLabel.textAlignment = NSTextAlignmentRight;
    goodsScanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [goodsView addSubview:goodsScanLabel];
    [goodsScanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(goodsPriceLabel);
        make.height.mas_equalTo (20);
        make.right.mas_equalTo(goodsView).mas_offset(-12);
        make.width.mas_greaterThanOrEqualTo(5);
    }];
    
    goodsScanView = [[UIImageView alloc] init];
    goodsScanView.image = [UIImage imageNamed:@"card_eye"];
    [goodsView addSubview:goodsScanView];
    [goodsScanView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(14, 10));
        make.right.mas_equalTo(goodsScanLabel.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(goodsPriceLabel);
    }];
    
    goodsLineView = [UIView new];
    goodsLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self addSubview:goodsLineView];
    goodsLineView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame)-1, SCREEN_WIDTH, 0.5);
  
    goodsView.hidden = YES;
    
    commentLabel = [[UILabel alloc] init];
    commentLabel.text = @"评论";
    commentLabel.font = DHSystemFontOfSize_16;
    commentLabel.textColor = FWTextColor_000000;
    commentLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:commentLabel];
    commentLabel.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame),CGRectGetMaxY(lineView.frame)+5 , 100, 20);

    commentNumberButton = [[UIButton alloc] init];
    commentNumberButton.titleLabel.textAlignment = NSTextAlignmentRight;
    commentNumberButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    commentNumberButton.titleLabel.font = DHSystemFontOfSize_12;
    [commentNumberButton setTitle:@"0" forState:UIControlStateNormal];
    [commentNumberButton setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
    [commentNumberButton addTarget:self action:@selector(commentNumberButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:commentNumberButton];
    [commentNumberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-12);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(30);
        make.centerY.mas_equalTo(commentLabel);
    }];
}

- (void)configViewForModel:(id)model{
 
    listModel = (FWFeedListModel *)model;
    
    CGFloat picHight = SCREEN_WIDTH;
    CGFloat picWidth = SCREEN_WIDTH;

    CGFloat currentContentY = 0;
    if (listModel.imgs.count >0) {
        // 轮播图
        NSMutableArray * tempArr = @[].mutableCopy;
        
        for (FWFeedImgsModel * imageModel in listModel.imgs) {
            [tempArr addObject:imageModel.img_url];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            bannerView.imageURLStringsGroup = tempArr;
        });
        
        if ([listModel.imgs[0].img_width floatValue] == 0) {
            listModel.imgs[0].img_width = @(SCREEN_WIDTH).stringValue;
        }
        if ([listModel.imgs[0].img_height floatValue] == 0) {
            listModel.imgs[0].img_height = @(SCREEN_WIDTH).stringValue;
        }
        
        CGFloat k = [listModel.imgs[0].img_width floatValue]/[listModel.imgs[0].img_height floatValue];
        if ( k>= 0.667 && k<= 1.5) {
            picHight = picWidth / k;
        }else if( k < 0.667) {
            picHight = picWidth *1.5;
        }else if (k >1.5){
            picHight = picWidth *0.667;
        }
        bannerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, picHight);
        currentContentY =  CGRectGetMaxY(self.bannerView.frame)+20;
    }else{
        currentContentY =  20;
    }

    [self setupDetailView];
 
    
    contentLabel.text = listModel.feed_title;
    timeLabel.text = [NSString stringWithFormat:@"%@    %@次浏览",listModel.format_create_time,listModel.count_realtime.pv_count_format];
    
    NSString * comment_count = listModel.count_realtime.comment_count?listModel.count_realtime.comment_count:@"0";
    
    [commentNumberButton setTitle:[NSString stringWithFormat:@"%@ 条评论",comment_count] forState:UIControlStateNormal];
    
    containerArr = listModel.tags;
   
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:contentLabel.attributedText];
    
    CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 40].height;
    
    //展示文字或隐藏文字
    contentLabel.frame = CGRectMake(12,currentContentY, SCREEN_WIDTH-40,textHeight);
    
    
    // 标签
    [self refreshContainer:containerView WithArray:containerArr];
    
    CGRect rect = self.gaizhuangView.frame;
    gaizhuangView.frame = CGRectMake(CGRectGetMinX(contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame)+10, SCREEN_WIDTH-40, rect.size.height);
    
    containerView.frame = CGRectMake(CGRectGetMinX(gaizhuangView.frame), CGRectGetMaxY(gaizhuangView.frame)+5, CGRectGetWidth(self.contentLabel.frame), containerHeight);
    
    timeLabel.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame),CGRectGetMaxY(containerView.frame)+10 , 200, 20);

    lineView.frame = CGRectMake(0,CGRectGetMaxY(timeLabel.frame)+10, SCREEN_WIDTH, 0.5);

    if (listModel.goods_id.length > 0 && ![listModel.goods_id isEqualToString:@"0"]) {
        
        goodsTitleLabel.text = listModel.goods_info.title;
        goodsPriceLabel.text = [NSString stringWithFormat:@"￥%@",listModel.goods_info.price];
        goodsScanLabel.text = listModel.goods_info.click_count;
        [goodsImageView sd_setImageWithURL:[NSURL URLWithString:listModel.goods_info.cover.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        goodsView.hidden = NO;
        goodsLineView.hidden = NO;

        goodsView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), SCREEN_WIDTH, 130);
        goodsLineView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame)-1, SCREEN_WIDTH, 0.5);
        
        commentLabel.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame),CGRectGetMaxY(goodsLineView.frame)+5 , 100, 20);
    }else{
        
        goodsView.hidden = YES;
        commentLabel.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame),CGRectGetMaxY(lineView.frame)+5 , 100, 20);
        goodsLineView.hidden = YES;
    }

    
    [commentNumberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-12);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(30);
        make.centerY.mas_equalTo(commentLabel);
    }];
}
    
#pragma mark - > 设置行间距
- (void)setLabelAttribute:(UILabel *)label{
    if (label.text.length <= 0 || nil == label) {
        return;
    }
    
    NSMutableAttributedString * labelAttributed = [[NSMutableAttributedString alloc] initWithString:label.text];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [labelAttributed addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text length])];
    label.attributedText = labelAttributed;
}

- (void)setupDetailView{
    
    for (UIView * view in gaizhuangView.subviews) {
        [view removeFromSuperview];
    }
    
    UILabel * yuanchangLabel = [[UILabel alloc] init];
    yuanchangLabel.text = @"原厂车型";
    yuanchangLabel.font = DHBoldFont(14);
    yuanchangLabel.textColor = FWTextColor_222222;
    yuanchangLabel.textAlignment = NSTextAlignmentLeft;
    [gaizhuangView addSubview:yuanchangLabel];
    [yuanchangLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(gaizhuangView);
        make.top.mas_equalTo(gaizhuangView).mas_offset(0);
        make.right.mas_equalTo(gaizhuangView).mas_offset(-14);
        make.height.mas_equalTo(19);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    UILabel * yuanchangContentLabel = [[UILabel alloc] init];
    yuanchangContentLabel.numberOfLines = 0;
    yuanchangContentLabel.text = [NSString stringWithFormat:@"品牌:%@\n%@-%@\n%@-%@-%@",listModel.car_style.brand,listModel.car_style.type,listModel.car_style.style,listModel.car_style.pailiang,listModel.car_style.jinqi,listModel.car_style.mali];
    yuanchangContentLabel.font = DHFont(14);
    yuanchangContentLabel.textColor = FWTextColor_222222;
    yuanchangContentLabel.textAlignment = NSTextAlignmentLeft;
    [gaizhuangView addSubview:yuanchangContentLabel];
    [yuanchangContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(gaizhuangView);
        make.top.mas_equalTo(yuanchangLabel.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(gaizhuangView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    [self setLabelAttribute:yuanchangContentLabel];
    
    UILabel * jianyaoLabel = [[UILabel alloc] init];
    jianyaoLabel.text = @"改装简要";
    jianyaoLabel.font = DHBoldFont(14);
    jianyaoLabel.textColor = FWTextColor_222222;
    jianyaoLabel.textAlignment = NSTextAlignmentLeft;
    [gaizhuangView addSubview:jianyaoLabel];
    [jianyaoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(yuanchangLabel);
        make.top.mas_equalTo(yuanchangContentLabel.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(gaizhuangView).mas_offset(-14);
        make.height.mas_equalTo(19);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    NSString * jianyao = @"";
    for (int i = 0 ; i < listModel.gaizhuang_jianyao.count; i++) {
        FWRefitJianyaoModel * jianyaoModel = listModel.gaizhuang_jianyao[i];
        
        if (i == listModel.gaizhuang_jianyao.count - 1) {
            jianyao = [jianyao stringByAppendingFormat:@"%@", [NSString stringWithFormat:@"%@:\n%@",jianyaoModel.name,jianyaoModel.val]];
        }else{
            jianyao = [jianyao stringByAppendingFormat:@"%@", [NSString stringWithFormat:@"%@:%@\n",jianyaoModel.name,jianyaoModel.val]];
        }
    }

    UILabel * jianyaoContentLabel = [[UILabel alloc] init];
    jianyaoContentLabel.numberOfLines = 0;
    jianyaoContentLabel.text = jianyao;
    jianyaoContentLabel.font = DHFont(14);
    jianyaoContentLabel.textColor = FWTextColor_222222;
    jianyaoContentLabel.textAlignment = NSTextAlignmentLeft;
    [gaizhuangView addSubview:jianyaoContentLabel];
    [jianyaoContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(jianyaoLabel);
        make.top.mas_equalTo(jianyaoLabel.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(gaizhuangView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(10);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    [self setLabelAttribute:jianyaoContentLabel];

    UILabel * gushiLabel = [[UILabel alloc] init];
    gushiLabel.text = @"改装故事";
    gushiLabel.font = DHBoldFont(14);
    gushiLabel.textColor = FWTextColor_222222;
    gushiLabel.textAlignment = NSTextAlignmentLeft;
    [gaizhuangView addSubview:gushiLabel];
    [gushiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(yuanchangLabel);
        make.top.mas_equalTo(jianyaoContentLabel.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(gaizhuangView).mas_offset(-14);
        make.height.mas_equalTo(19);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    UILabel * gushiContentLabel = [[UILabel alloc] init];
    gushiContentLabel.numberOfLines = 0;
    gushiContentLabel.text = listModel.gaizhuang_content;
    gushiContentLabel.font = DHFont(14);
    gushiContentLabel.textColor = FWTextColor_222222;
    gushiContentLabel.textAlignment = NSTextAlignmentLeft;
    [gaizhuangView addSubview:gushiContentLabel];
    [gushiContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(jianyaoLabel);
        make.top.mas_equalTo(gushiLabel.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(gaizhuangView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    [self setLabelAttribute:gushiContentLabel];
    
    UILabel * qingdanLabel = [[UILabel alloc] init];
    qingdanLabel.text = @"改装清单";
    qingdanLabel.font = DHBoldFont(14);
    qingdanLabel.textColor = FWTextColor_222222;
    qingdanLabel.textAlignment = NSTextAlignmentLeft;
    [gaizhuangView addSubview:qingdanLabel];
    [qingdanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(yuanchangLabel);
        make.top.mas_equalTo(gushiContentLabel.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(gaizhuangView).mas_offset(-14);
        make.height.mas_equalTo(19);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    
    NSString * str = [self.listModel.gaizhuang_list_show componentsJoinedByString:@"\n"];
    
    UILabel * qingdanContentLabel = [[UILabel alloc] init];
    qingdanContentLabel.text = str;
    qingdanContentLabel.numberOfLines = 0;
    qingdanContentLabel.font = DHSystemFontOfSize_14;
    qingdanContentLabel.textColor = FWTextColor_222222;
    qingdanContentLabel.textAlignment = NSTextAlignmentLeft;
    [gaizhuangView addSubview:qingdanContentLabel];
    [qingdanContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(qingdanLabel);
        make.top.mas_equalTo(qingdanLabel.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(gaizhuangView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    [self setLabelAttribute:qingdanContentLabel];

    [gaizhuangView layoutIfNeeded];
    gaizhuangView.frame = CGRectMake(CGRectGetMinX(contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame)+10, SCREEN_WIDTH-40, CGRectGetMaxY(qingdanContentLabel.frame)+10);
}

- (void) refreshContainer:(UIView *)container WithArray:(NSArray *)array{
    
    for (UIView * view in container.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    CGFloat x = 0;
    CGFloat y = 5;
    CGFloat width = 0.0;
    CGFloat height = 24;
    
    for (int i = 0; i < array.count; i++) {
        FWSearchTagsListModel * tagsModel = array[i];
        
        width = [tagsModel.tag_name length]*14+20;
        
        if (x + width+20 >= SCREEN_WIDTH) {
            x = 0;
            y += height + 10;
        }
        
        FWModuleButton * btn = [[FWModuleButton alloc] init];
        btn.titleLabel.font = DHSystemFontOfSize_14;
        btn.backgroundColor = FWViewBackgroundColor_EEEEEE;
        btn.nameLabel.textColor = FWColor(@"515151");
        btn.tag = 10000+i;
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        btn.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
        [btn setModuleTitle:tagsModel.tag_name];
        [btn setModuleImage:@"home_tag"];
        [btn addTarget:self action:@selector(btnTagsClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
        [container addSubview:btn];
        [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(container).mas_offset(x);
            make.top.mas_equalTo(container).mas_offset(y);
            make.height.mas_equalTo(height);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
        
        [btn.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btn);
            make.left.mas_equalTo(btn).mas_offset(8);
            make.size.mas_equalTo(CGSizeMake(14, 14));
        }];

        [btn.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(btn.iconImageView.mas_right).mas_offset(4);
            make.height.mas_equalTo(20);
            make.centerY.mas_equalTo(btn);
            make.width.mas_greaterThanOrEqualTo(10);
            make.right.mas_equalTo(btn).mas_offset(-9);
        }];
        
        [container layoutIfNeeded];
        x += CGRectGetWidth(btn.frame)+10;
    }
    
    if (array.count <= 0) {
        containerHeight = y;
    }else{
        containerHeight = y + 30;
    }
}

#pragma mark - > 返回
- (void)backButtonOnClick{
    
    if ([self.headerDelegate respondsToSelector:@selector(backButtonClick)]) {
        [self.headerDelegate backButtonClick];
    }
}

#pragma mark - > 跳转商品
- (void)goodsViewClick{
    
    FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
    GDVC.goods_id = listModel.goods_id;
    [self.vc.navigationController pushViewController:GDVC animated:YES];
}

#pragma mark - > 关注
- (void)attentionButtonOnClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([self.headerDelegate respondsToSelector:@selector(attentionButtonClick)]) {
            [self.headerDelegate attentionButtonClick];
        }
    }
}

#pragma mark - > 点击头像
- (void)photoImageTap:(UITapGestureRecognizer *)gesture{
    if (nil == self.listModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 点击标签
- (void)btnTagsClick:(UIButton *)sender{
    
    FWSearchTagsSubListModel * tagsModel = self.listModel.tags[sender.tag-10000];
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.traceType = 2;
    TVC.tagsModel = tagsModel;
    TVC.tags_id = tagsModel.tag_id;
    [self.viewController.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 点击进入评论
- (void)commentNumberButtonOnClick{
    
    FWCommentViewController * vc = [[FWCommentViewController alloc] init];
    vc.feed_id = self.listModel.feed_id;
    vc.listModel = self.listModel;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

#pragma mark - > banner跳转
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
//    FWViewPictureViewController * PVC = [[FWViewPictureViewController alloc] init];
//    PVC.model = self.listModel;
//    PVC.currentIndex = index;
//    [self.vc.navigationController pushViewController:PVC animated:YES];
    
    
    NSMutableArray * smallArray = @[].mutableCopy;
    NSMutableArray * originalArray = @[].mutableCopy;

    for (FWFeedImgsModel * model in self.listModel.imgs) {
       [smallArray addObject:model.img_url];
    }

    for (FWFeedImgsModel * model in self.listModel.imgs_original) {
       [originalArray addObject:model.img_url];
    }

    if (index < self.listModel.imgs_original.count) {
       
       FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
       browser.isFullWidthForLandScape = YES;
       browser.isNeedLandscape = YES;
       browser.currentImageIndex = (int)index;
       browser.imageArray = [smallArray mutableCopy];
       browser.smallArray = [smallArray copy];
       browser.originalImageArray = originalArray;
       browser.vc = self.vc;
       browser.fromType = 3;
       [browser show];
    }
}

- (CGFloat)getCurrentViewHeight{
    
    CGFloat height = 0;
    
    height = CGRectGetMaxY(commentLabel.frame) +10;
    
    return height;
}

#pragma mark - NSString 转 字典NSDictionary
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {/*JSON解析失败*/
        
        return nil;
    }
    return dic;
}
@end
