//
//  FWRefitCaseView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWRefitCaseSegement.h"
NS_ASSUME_NONNULL_BEGIN

@protocol FWRefitCaseViewDelegate <NSObject>

#pragma mark - > 切换排序
- (void)changeSortWithType:(NSString *)sortType;

#pragma mark - > 切换标签
- (void)segementItemTap:(NSInteger)index ;

#pragma mark - > 点击车系
- (void)carStyleButtonOnClick:(NSInteger)index ;
@end

@interface FWRefitCaseView : UIView<FWRefitCaseSegementViewDelegate>

@property (nonatomic, weak) UIViewController * vc;

/* 标签封面 */
@property (nonatomic, strong) UIImageView * tagsCoverView;

/* 标签名字 */
@property (nonatomic, strong) UILabel * tagsLabel;

/* 多少条 */
@property (nonatomic, strong) UILabel * numberLabel;

@property (nonatomic, strong) FWRefitCaseSegement * segement;

@property (nonatomic, strong) UIView * carStyleView;

@property (nonatomic, strong) UILabel * typeLabel;

@property (nonatomic, strong) UIButton * changeButton;

@property (nonatomic, weak) id<FWRefitCaseViewDelegate> caseDelegate;

@property (nonatomic, assign) BOOL  isTimeSort;// yes 时间排序，NO 热度排序

@property (nonatomic, assign) BOOL  isFirstRefresh;// yes 第一次刷新，NO 刷新好几次了

- (void)congifViewWithModel:(id)model;

- (CGFloat)getCurrentViewHeight;

- (CGFloat)getBarViewTopHeight;
@end

NS_ASSUME_NONNULL_END
