//
//  FWRefitCaseCollectionViewCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCollecitonBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWRefitCaseCollectionViewCell : FWCollecitonBaseCell
+ (NSString *)cellIdentifier;

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
