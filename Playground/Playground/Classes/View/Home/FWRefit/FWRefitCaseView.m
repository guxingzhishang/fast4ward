//
//  FWRefitCaseView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRefitCaseView.h"
#import "FWTagsFeedModel.h"
#import "FWChooseBradeViewController.h"
@interface FWRefitCaseView ()

@property (nonatomic, strong) FWRefitCaseModel * subModel;


@end

@implementation FWRefitCaseView
@synthesize tagsLabel;
@synthesize tagsCoverView;
@synthesize numberLabel;
@synthesize segement;
@synthesize changeButton;
@synthesize typeLabel;
@synthesize isTimeSort;
@synthesize isFirstRefresh;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        isTimeSort = YES;
        isFirstRefresh = YES;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    tagsCoverView = [[UIImageView alloc] init];
    tagsCoverView.layer.borderColor = [UIColor colorWithHexString:@"eeeeee" alpha:1].CGColor;
    tagsCoverView.contentMode = UIViewContentModeScaleAspectFill;
//    tagsCoverView.layer.borderWidth = 0.5;
//    tagsCoverView.layer.cornerRadius = 2;
//    tagsCoverView.layer.masksToBounds = YES;
    [self addSubview:tagsCoverView];
    tagsCoverView.frame = CGRectMake(14, 14, 90, 90);
    
    tagsLabel = [[UILabel alloc] init];
    tagsLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    tagsLabel.textColor = FWTextColor_272727;
    tagsLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:tagsLabel];
    [tagsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(tagsCoverView.mas_right).mas_offset(12);
        make.height.mas_equalTo(22);
        make.top.mas_equalTo(tagsCoverView).mas_offset(30);
        make.width.mas_greaterThanOrEqualTo(20);
        make.right.mas_equalTo(self).mas_offset(-5);
    }];
    
    numberLabel = [[UILabel alloc] init];
    numberLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 13];
    numberLabel.textColor = FWTextColor_9EA3AB;
    numberLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:numberLabel];
    [numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(tagsLabel);
        make.height.mas_equalTo(18);
        make.top.mas_equalTo(tagsLabel.mas_bottom);
    }];

    self.segement = [[FWRefitCaseSegement alloc]init];
    self.segement.itemDelegate = self;
    [self addSubview:self.segement];
    self.segement.frame = CGRectMake(0, CGRectGetMaxY(self.tagsCoverView.frame), SCREEN_WIDTH, 0.01);

    self.carStyleView = [[UIView alloc] init];
    [self addSubview:self.carStyleView];
    self.carStyleView.frame = CGRectMake(0, CGRectGetMaxY(self.segement.frame), SCREEN_WIDTH, 0.01);

    typeLabel = [[UILabel alloc] init];
    typeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    typeLabel.textColor = FWTextColor_222222;//FWTextColor_9EA3AB
    typeLabel.textAlignment = NSTextAlignmentLeft;
    typeLabel.text = @"时间排序";
    typeLabel.frame = CGRectMake(20, CGRectGetMaxY(self.tagsCoverView.frame)+90, 80, 40);
    [self addSubview:typeLabel];
    
    changeButton = [[UIButton alloc] init];
    changeButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [changeButton setTitleColor:FWTextColor_68769F forState:UIControlStateNormal];
    [changeButton setTitle:@"切换" forState:UIControlStateNormal];
    [changeButton addTarget:self action:@selector(changeButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:changeButton];
    [changeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(typeLabel);
        make.size.mas_equalTo(CGSizeMake(50, 40));
        make.right.mas_equalTo(self).mas_offset(-20);
    }];
    
}

- (void)congifViewWithModel:(id)model{
    
    self.subModel = (FWRefitCaseModel *)model;
    
    
    tagsCoverView.contentMode = UIViewContentModeScaleAspectFill;
    tagsCoverView.clipsToBounds = YES;
    
    if (self.subModel.car_type_info.car_type.length > 0) {
        tagsLabel.text = [NSString stringWithFormat:@"#%@",self.subModel.car_type_info.car_type];
        [tagsCoverView sd_setImageWithURL:[NSURL URLWithString:self.subModel.car_type_info.img] placeholderImage:[UIImage imageNamed:@"tags_icon"]];
    }else if(self.subModel.category_info.name.length > 0){
        tagsLabel.text = [NSString stringWithFormat:@"#%@",self.subModel.category_info.name];
        [tagsCoverView sd_setImageWithURL:[NSURL URLWithString:self.subModel.category_info.img] placeholderImage:[UIImage imageNamed:@"tags_icon"]];
    }
    
    numberLabel.text = [NSString stringWithFormat:@"%@条内容",self.subModel.total_count];

    if (self.subModel.list_tag.count > 0) {
        /* 有标签 */
        self.segement.frame = CGRectMake(0, CGRectGetMaxY(self.tagsCoverView.frame)+14, SCREEN_WIDTH, 40);

        if (isFirstRefresh) {
            isFirstRefresh = NO;
            
            NSMutableArray * titleArray = @[].mutableCopy;
            for (FWRefitCaseTagListModel * tagsListModel in self.subModel.list_tag) {
                [titleArray addObject:tagsListModel.tag_name];
            }
            [self.segement deliverTitles:titleArray];
        }
    }else{
        self.segement.frame = CGRectMake(0, CGRectGetMaxY(self.tagsCoverView.frame), SCREEN_WIDTH, 0.01);
    }
    
    if (self.subModel.list_car_style_suggestion.count > 0) {
        /* 有车系案例 */
        self.carStyleView.frame = CGRectMake(0, CGRectGetMaxY(self.segement.frame)+8, SCREEN_WIDTH, 30);

        [self setupCarStyleView];
    }else{
        self.carStyleView.frame = CGRectMake(0, CGRectGetMaxY(self.segement.frame), SCREEN_WIDTH, 0.01);
    }
    
    typeLabel.frame = CGRectMake(20, CGRectGetMaxY(self.carStyleView.frame), 80, 40);

    if (isTimeSort) {
        typeLabel.text = @"时间排序";
    }else{
        typeLabel.text = @"热度排序";
    }
}

- (void)setupCarStyleView{
    for (UIView * view in self.carStyleView.subviews) {
        [view removeFromSuperview];
    }
    
    UIButton * anchexiButton = [[UIButton alloc] init];
    anchexiButton.frame = CGRectMake(SCREEN_WIDTH-14-80, 0, 80, 30);
    anchexiButton.titleLabel.font = DHSystemFontOfSize_12;
    [anchexiButton setTitle:@"按车系查看 " forState:UIControlStateNormal];
    [anchexiButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [anchexiButton setImage:[UIImage imageNamed:@"perfect_arrow"] forState:UIControlStateNormal];
    [anchexiButton addTarget:self action:@selector(anchexiButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.carStyleView addSubview:anchexiButton];
    anchexiButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    CGFloat leftlab = [@"按车系查看 " sizeWithAttributes:@{NSFontAttributeName : anchexiButton.titleLabel.font}].width;
    [anchexiButton setImageEdgeInsets:UIEdgeInsetsMake(0, leftlab + 5, 0, 0)];
    [anchexiButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    
    CGFloat currentX = 14;
    
    for (int i =0 ; i<self.subModel.list_car_style_suggestion.count; i++) {
        
        UIView * bgView = [[UIView alloc] init];
        bgView.layer.cornerRadius = 1;
        bgView.layer.masksToBounds = YES;
        bgView.backgroundColor = FWViewBackgroundColor_F1F1F1;
        [self.carStyleView addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.carStyleView).mas_offset(currentX);
            make.height.mas_equalTo(18);
            make.width.mas_greaterThanOrEqualTo(10);
            make.centerY.mas_equalTo(self.carStyleView);
        }];
        
        UIButton * carStyleButton = [[UIButton alloc] init];
        carStyleButton.tag = 100879+i;
        carStyleButton.titleLabel.font = DHSystemFontOfSize_12;
        [carStyleButton setImage:[UIImage imageNamed:@"shape_question"] forState:UIControlStateNormal];
        [carStyleButton setTitle:self.subModel.list_car_style_suggestion[i] forState:UIControlStateNormal];
        [carStyleButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        [carStyleButton addTarget:self action:@selector(carStyleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:carStyleButton];
        [carStyleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(bgView).mas_offset(5);
            make.right.mas_equalTo(bgView).mas_offset(-5);
            make.height.mas_equalTo(bgView);
            make.width.mas_greaterThanOrEqualTo(44);
            make.centerY.mas_equalTo(bgView);
        }];
        
        [self.carStyleView layoutIfNeeded];
        currentX = 10+CGRectGetMaxX(bgView.frame);
    }
}

#pragma mark - > 按车系查看
- (void)anchexiButtonClick{
    FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
    CBVC.chooseFrom = @"anchexi";
    [self.vc.navigationController pushViewController:CBVC animated:YES];
}

#pragma mark - > 车系案例
- (void)carStyleButtonClick:(UIButton *)sender{
    
    NSInteger index = sender.tag -100879;
    if ([self.caseDelegate respondsToSelector:@selector(carStyleButtonOnClick:)]) {
        [self.caseDelegate carStyleButtonOnClick:index];
    }
}

#pragma mark - > 切换标签
- (void)segementItemClickTap:(NSInteger)index{
    
    if ([self.caseDelegate respondsToSelector:@selector(segementItemTap:)]) {
        [self.caseDelegate segementItemTap:index];
    }
}

#pragma mark - > 切换排序
- (void)changeButtonOnClick{
    
    NSString * sortType = @"";
    if (isTimeSort) {
        typeLabel.text = @"热度排序";
        sortType = @"hot";
    }else{
        typeLabel.text = @"时间排序";
        sortType = @"new";
    }
    
    if ([self.caseDelegate respondsToSelector:@selector(changeSortWithType:)]) {
        [self.caseDelegate changeSortWithType:sortType];
    }
    
    isTimeSort = !isTimeSort;
}

#pragma mark - > 返回当前顶部视图的高度
- (CGFloat)getCurrentViewHeight{
    
    CGFloat height = 0;
    height = CGRectGetMaxY(typeLabel.frame);
    return height;
}

#pragma mark - > 返回切换标签上部的高度
- (CGFloat)getBarViewTopHeight{
    
    CGFloat height = 0;
    height = CGRectGetMinY(segement.frame);
    return height;
}

@end
