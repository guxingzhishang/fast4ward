//
//  FWRefitCaseCollectionViewCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRefitCaseCollectionViewCell.h"

@implementation FWRefitCaseCollectionViewCell
+ (NSString *)cellIdentifier {
    return @"FWRefitCaseCollectionViewCell";
}

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath {
    FWRefitCaseCollectionViewCell *cell = (FWRefitCaseCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:[FWRefitCaseCollectionViewCell cellIdentifier] forIndexPath:indexPath];
    return cell;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

@end
