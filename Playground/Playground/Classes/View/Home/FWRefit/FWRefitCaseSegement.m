//
//  FWRefitCaseSegement.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRefitCaseSegement.h"

@implementation FWRefitCaseSegement

- (CGFloat)menuHeight{
    
    if (_menuHeight == 0) {
        _menuHeight = 28;
    }
    return _menuHeight;
}

- (NSInteger)pageNumberOfItem{
    
    if (_pageNumberOfItem == 0) {
        _pageNumberOfItem = (self.titles.count > 5 ? 5 : self.titles.count);
    }
    return _pageNumberOfItem;
}

- (NSMutableArray *)labelArys{
    
    if (!_labelArys) {
        _labelArys = [NSMutableArray array];
    }
    return _labelArys;
}

- (NSMutableArray *)rectXArray{
    
    if (!_rectXArray) {
        _rectXArray = [NSMutableArray array];
    }
    return _rectXArray;
}

- (id)init{
    
    self = [super init];
    if (self) {
        self.titles = @[].copy;
        
        self.showsHorizontalScrollIndicator = NO;
        self.delegate = self;
        self.firstIndex = -1;
        
        self.titleColor = FWTextColor_222222;
        self.titleSelectColor = FWViewBackgroundColor_FFFFFF;
        
        self.titleFont = [UIFont systemFontOfSize:12];
        self.titleSelectFont = [UIFont boldSystemFontOfSize:12];
        
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        self.lastIndex = 0;
    }
    return self;
}

#pragma mark - 加载顶部视图数据 -
- (void)deliverTitles:(NSArray *)array{
    //标记label的最大X坐标
    CGFloat preX = 4;
    self.titles = array;
    
    if (self.labelArys.count>0) {
        [self.labelArys removeAllObjects];
    }
    
    for (int i = 0; i < self.titles.count; i++) {
        
        NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc] initWithString:self.titles[i]];
        CGSize  textLimitSize = CGSizeMake(MAXFLOAT, self.menuHeight);
        CGFloat textW = [YYTextLayout layoutWithContainerSize:textLimitSize text:attribute].textBoundingSize.width;
        
        UIView *shadowView = [[UIView alloc]initWithFrame:CGRectMake(preX+10, 6,textW+20, self.menuHeight)];
        shadowView.userInteractionEnabled = YES;
        shadowView.tag = 100000 + i;
        [self addSubview:shadowView];
        
        UILabel *item = [[UILabel alloc] init];
        item.frame = CGRectMake(0, 0, textW+20, self.menuHeight);
        item.text = self.titles[i];
        item.font = self.titleFont;
        item.layer.cornerRadius = 2;
        item.layer.masksToBounds = YES;
        item.layer.borderColor = FWTextColor_222222.CGColor;
        item.layer.borderWidth = 1;
        item.textColor = self.titleColor;
        item.backgroundColor = FWViewBackgroundColor_FFFFFF;
        item.textAlignment = NSTextAlignmentCenter;
        item.tag = 10000 + i;
        [shadowView addSubview:item];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClickTap:)];
        item.userInteractionEnabled = YES;
        [item addGestureRecognizer:tap];
        [self.labelArys addObject:item];
        
        preX = CGRectGetMaxX(shadowView.frame);
        
        [self.rectXArray addObject:@(preX)];
    }
    self.contentSize = CGSizeMake(preX+10, self.menuHeight);
    [self menuUpdateStyle:0];
}

#pragma mark - 菜单点击回调 -
- (void)itemClickTap:(UIGestureRecognizer *)sender{
    NSInteger index = sender.view.tag - 10000;
    
    if (self.lastIndex != index) {
        [self menuUpdateStyle:index];
        [self menuScrollToCenter:index];
        
        self.lastIndex = index;
        
        if ([self.itemDelegate respondsToSelector:@selector(segementItemClickTap:)]) {
            [self.itemDelegate segementItemClickTap:index];
        }
    }
}

#pragma mark - > 顶部菜单滚动
- (void)menuScrollToCenter:(NSInteger)index{
    
    //    UIView * shadowView = (UIView *)[self viewWithTag:100000+index];
    //
    //    CGFloat lastX = [[self.rectXArray lastObject] floatValue];
    //
    //    CGFloat left = shadowView.center.x - SCREEN_WIDTH / 2.0;
    //    left = left <= 0 ? 0 : left;
    //    CGFloat maxLeft = lastX - SCREEN_WIDTH;
    //    left = left >= maxLeft ? maxLeft : left;
    //    [self setContentOffset:CGPointMake(left, 0) animated:YES];
}

#pragma mark - > 顶部菜单切换样式
- (void)menuUpdateStyle:(NSInteger)index{
    
    if (self.labelArys.count <= 0) {
        return;
    }
    
    if (self.lastIndex >= 0) {
        UILabel *lastLabel = self.labelArys[self.lastIndex];
        lastLabel.font = self.titleFont;
        lastLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
        lastLabel.textColor = self.titleColor;
        lastLabel.layer.borderColor = FWTextColor_222222.CGColor;
        lastLabel.layer.borderWidth = 1;
    }

    UILabel *label = self.labelArys[index];
    label.backgroundColor = FWTextColor_222222;
    label.textColor = self.titleSelectColor;
    label.font = self.titleSelectFont;
    label.layer.borderColor = FWClearColor.CGColor;
    label.layer.borderWidth = 0;
}

- (void)clearSubViews{
    
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
}


@end
