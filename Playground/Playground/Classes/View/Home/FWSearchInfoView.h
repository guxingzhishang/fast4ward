//
//  FWSearchInfoView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/19.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWBaseCellView.h"

NS_ASSUME_NONNULL_BEGIN


@protocol FWSearchInfoViewDelegate <NSObject>

- (void)tagsBtnOnClickWithSuperIndex:(NSInteger)superIndex WithSubIndex:(NSInteger)subIndex;

@end

@interface FWSearchInfoView : UIScrollView<UIGestureRecognizerDelegate>

/**
 * 历史搜索
 */
@property (nonatomic, strong) UILabel * firstLabel;

/**
 * 承载历史搜索的视图
 */
@property (nonatomic, strong) UIView * firstContainer;

/**
 * 承载推荐标签的视图
 */
@property (nonatomic, strong) UIView * secondContainer;

/**
 * 推荐标签
 */
@property (nonatomic, strong) UILabel * secondLabel;

/**
 * 承载历史搜索的数组
 */
@property (nonatomic, strong) NSMutableArray * containerOneArr;

/**
 * 承载推荐的数组
 */
@property (nonatomic, strong) NSMutableArray * containerTwoArr;

@property (nonatomic, weak) id<FWSearchInfoViewDelegate> infoDelegate;

@property (nonatomic, weak) UIViewController * vc;

- (void)configViewWithModel:(NSMutableArray *)tagsList;

@end

NS_ASSUME_NONNULL_END
