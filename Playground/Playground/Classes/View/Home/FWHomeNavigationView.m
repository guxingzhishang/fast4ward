//
//  FWHomeNavigationView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWHomeNavigationView.h"

@implementation FWHomeNavigationView
@synthesize searchButton;
@synthesize searchImageView;
@synthesize searchLabel;

- (id)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.clipsToBounds = NO;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    searchButton = [[UIButton alloc] init];
    searchButton.backgroundColor = FWViewBackgroundColor_F1F1F1;
    searchButton.layer.cornerRadius = 35/2;
    searchButton.layer.masksToBounds = YES;
    [searchButton addTarget:self action:@selector(searchButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:searchButton];
    [searchButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).mas_offset(24+FWCustomeSafeTop);
        make.centerX.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(14);
        make.right.mas_equalTo(self).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-28, 35));
    }];
    
    
    searchLabel = [[UILabel alloc] init];
    searchLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];;
    searchLabel.textColor = FWTextColor_A7ADB4;
    searchLabel.textAlignment = NSTextAlignmentLeft;
    searchLabel.text = @"搜索你想要看的内容";
    [searchButton addSubview:searchLabel];
    [searchLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchButton);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(searchButton);
        make.centerX.mas_equalTo(searchButton);
    }];
    
    searchImageView = [[UIImageView alloc] init];
    searchImageView.image = [UIImage imageNamed:@"activity_search"];
    [searchButton addSubview:searchImageView];
    [searchImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(searchButton);
        make.right.mas_equalTo(searchLabel.mas_left).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(13, 12));
    }];
}

#pragma mark - > 搜索
- (void)searchButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(searchButtonClick)]) {
        [self.delegate searchButtonClick];
    }
}
@end
