//
//  FWGodDistrictView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/11.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 大神专区
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWGodDistrictView : UIView<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView * attentionScrollView;

@property (nonatomic, strong) UILabel * hotContentLabel;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWFeedModel * godModel;

@property (nonatomic, assign) CGFloat  currentHeight;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
