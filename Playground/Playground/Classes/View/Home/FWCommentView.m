//
//  FWCommentView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWCommentView.h"
#import "FWFeedModel.h"
#import <IQKeyboardManager.h>
@implementation FWCommentView
@synthesize contentView;
@synthesize shareButton;
@synthesize likesButton;
@synthesize heightText;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
    
    shareButton = [[UIButton alloc] init];
    shareButton.titleLabel.font = DHSystemFontOfSize_12;
    [shareButton setTitleColor:FWTextColor_646464 forState:UIControlStateNormal];
    [shareButton setTitle:@"  0" forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"home_share"] forState:UIControlStateNormal];
    [self addSubview:shareButton];
    [shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.width.mas_greaterThanOrEqualTo(30);
        make.right.mas_equalTo(self).mas_offset(-10);
        if (SCREEN_HEIGHT >= 812) {
            make.top.mas_equalTo(self).mas_offset(15);
//            make.bottom.mas_equalTo(self).mas_offset(-15);
        }else{
            make.centerY.mas_equalTo(self);
//            make.bottom.mas_equalTo(self).mas_offset(-10);
        }
    }];
    
    [shareButton addTarget:self action:@selector(shareButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    likesButton = [[UIButton alloc] init];
    likesButton.titleLabel.font = DHSystemFontOfSize_12;
    [likesButton setTitleColor:FWTextColor_646464 forState:UIControlStateNormal];
    [likesButton setTitle:@"  0" forState:UIControlStateNormal];
    [likesButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
    [likesButton addTarget:self action:@selector(likesButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:likesButton];
    [likesButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.width.mas_greaterThanOrEqualTo(30);
        make.centerY.mas_equalTo(shareButton);
        make.right.mas_equalTo(shareButton.mas_left).mas_offset(-15);
    }];
    
    contentView = [[IQTextView alloc] init];
    contentView.delegate = self;
    contentView.returnKeyType=UIReturnKeySend;
    contentView.placeholder = @" 点赞都是套路，评论才是真情";
    contentView.font = DHSystemFontOfSize_14;
    contentView.layer.cornerRadius = 2;
    contentView.layer.masksToBounds = YES;
    contentView.textContainerInset = UIEdgeInsetsMake(7, 5, 0, 5);
    contentView.backgroundColor = FWViewBackgroundColor_F0F0F0;
    [self addSubview:contentView];
    [contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        if (SCREEN_HEIGHT >=812){
            make.left.mas_equalTo(self).mas_offset(15);
            make.top.mas_equalTo(self).mas_offset(15);
            make.right.mas_equalTo(likesButton.mas_left).mas_offset(-20);
            make.height.mas_greaterThanOrEqualTo (10);
            make.bottom.mas_equalTo(self).mas_offset(-40);
        }
        else{
            make.left.mas_equalTo(self).mas_offset(10);
            make.right.mas_equalTo(likesButton.mas_left).mas_offset(-20);
            make.height.mas_equalTo (32);
            make.centerY.mas_equalTo(self);
//            make.top.mas_equalTo(self).mas_offset(10);
//            make.bottom.mas_equalTo(self).mas_offset(-10);
        }
    }];
    
     [contentView addDoneOnKeyboardWithTarget:self action:@selector(doneAction)];
}

- (void)shareButtonOnClick{
    
//    [self checkLogin];
//
//    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([self.delegate respondsToSelector:@selector(shareButtonClick)]) {
            [self.delegate shareButtonClick];
        }
//    }
}

- (void)likesButtonOnClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([self.delegate respondsToSelector:@selector(likesButtonClick)]) {
            [self.delegate likesButtonClick];
        }
    }
}

-(void)textViewDidChange:(UITextView *)textView{
    
    NSInteger limit = 300;
    if ([self.feed_type isEqualToString:@"4"]) {
        /* 回答的评论框 */
        limit = 5000;
    }
    
    [self refreshContentView:textView];

    if (textView.text.length >= limit) {
        textView.text = [textView.text substringToIndex:limit];
        [[FWHudManager sharedManager] showErrorMessage:[NSString stringWithFormat:@"不能超过%ld字",(long)limit] toController:self.vc];
    }
}

- (void)refreshContentView:(UITextView *)textView{
   
    NSString *content=textView.text;
    NSDictionary *dict=@{NSFontAttributeName:[UIFont systemFontOfSize:20.0]};
    CGSize contentSize=[content sizeWithAttributes:dict];//计算文字长度
  
    int line = contentSize.width/(SCREEN_WIDTH-115);
  
    [shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(30);
        make.width.mas_greaterThanOrEqualTo(30);
        make.right.mas_equalTo(self).mas_offset(-10);
        if (SCREEN_HEIGHT >= 812) {
//                make.top.mas_equalTo(self).mas_offset(15);
            make.bottom.mas_equalTo(self).mas_offset(-15);
        }else{
//                make.centerY.mas_equalTo(self);
            make.bottom.mas_equalTo(self).mas_offset(-10);
        }
    }];
    
    if (line <= 1) {
        [contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self).mas_offset(15);
            if (self.type == 1) {
                make.right.mas_equalTo(self).mas_offset(-15);
            }else{
                make.right.mas_equalTo(likesButton.mas_left).mas_offset(-20);
            }
            make.height.mas_equalTo (30);
            make.bottom.mas_equalTo(self).mas_offset(-10);
        }];
    }else if(line > 8){
        [contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self).mas_offset(15);
            if (self.type == 1) {
                make.right.mas_equalTo(self).mas_offset(-15);
            }else{
                make.right.mas_equalTo(likesButton.mas_left).mas_offset(-20);
            }
            make.height.mas_equalTo(170);
            make.bottom.mas_equalTo(self).mas_offset(-10);
        }];
    }else{
        [contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self).mas_offset(15);
            if (self.type == 1) {
                make.right.mas_equalTo(self).mas_offset(-15);
            }else{
                make.right.mas_equalTo(likesButton.mas_left).mas_offset(-20);
            }
            make.height.mas_equalTo(22*line);
            make.bottom.mas_equalTo(self).mas_offset(-10);
        }];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [self doneAction];
        return NO;
    }
    return YES;
}

#pragma mark - > 发送、done
- (void)doneAction{
    
    [contentView addDoneOnKeyboardWithTarget:self action:nil];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [contentView addDoneOnKeyboardWithTarget:self action:@selector(doneAction)];
    });
    
    if (contentView.text.length >0 ) {

        if ([self.delegate respondsToSelector:@selector(sendContentText:)]) {
            [self.delegate sendContentText:contentView.text];
        }
    }else{
        [contentView endEditing:YES];
    }
}

- (void)configForView:(id)model{
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([self.delegate respondsToSelector:@selector(textViewBeginEditing)]) {
        [self.delegate textViewBeginEditing];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    if ([self.delegate respondsToSelector:@selector(textViewEndEditing)]) {
        [self.delegate textViewEndEditing];
    }
    return YES;
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
