//
//  FWCommentView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWCommentViewDelegate <NSObject>

- (void)shareButtonClick;

- (void)likesButtonClick;

- (void)sendContentText:(NSString *)content;

@optional

- (void)textViewBeginEditing;
- (void)textViewEndEditing;

@end

@interface FWCommentView : UIView<UITextViewDelegate>

@property (nonatomic, strong) IQTextView * contentView;

@property (nonatomic, strong) UIButton * likesButton;

@property (nonatomic, strong) UIButton * shareButton;

@property (nonatomic, strong) NSString * feed_type;

@property (nonatomic, assign) float heightText;//文字高度

@property (nonatomic, weak) id<FWCommentViewDelegate> delegate;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, assign) NSInteger type;//1:评论详情 2:其他

- (void)configForView:(id)model;

- (void)refreshContentView:(UITextView *)textView;

@end
