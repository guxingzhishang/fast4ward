//
//  FWIdleDetailHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/17.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWIdleDetailHeaderView.h"
#import "FWViewPictureViewController.h"
#import "FWCommentViewController.h"
#import "FWRefitCaseViewController.h"

@implementation FWIdleDetailHeaderView
@synthesize contentLabel;
@synthesize titleLabel;
@synthesize containerView;
@synthesize containerArr;
@synthesize commentLabel;
@synthesize commentNumberButton;
@synthesize bannerView;
@synthesize vc;
@synthesize containerHeight;
@synthesize lineView;
@synthesize listModel;
@synthesize timeLabel;
@synthesize goodsView;
@synthesize goodsScanView;
@synthesize goodsImageView;
@synthesize goodsScanLabel;
@synthesize goodsPriceLabel;
@synthesize goodsTitleLabel;
@synthesize recammondLabel;
@synthesize goodsLineView;

- (NSMutableArray *)tagsNameArray{
    if (!_tagsNameArray) {
        _tagsNameArray = [[NSMutableArray alloc] init];
    }
    return _tagsNameArray;
}

- (id)init{
    self = [super init];
    if (self) {
        containerHeight = 1;
        containerArr = @[].mutableCopy;
        self.sortType = @"new";
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 280, SCREEN_WIDTH, SCREEN_WIDTH) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    bannerView.autoScrollTimeInterval = 4;
    bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    bannerView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    [self addSubview:bannerView];
    bannerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.01);

    self.goodsTagsView = [[UIView alloc] init];
    [self addSubview:self.goodsTagsView];
    [self.goodsTagsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bannerView).mas_offset(14);
        make.height.mas_equalTo(0.01);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.top.mas_equalTo(bannerView.mas_bottom).mas_offset(10);
    }];
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = DHBoldFont(22);
    self.priceLabel.textColor = FWColor(@"dd3333");
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.goodsTagsView);
        make.top.mas_equalTo(self.goodsTagsView.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(22);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.originalPriceLabel = [[UILabel alloc] init];
    self.originalPriceLabel.font = DHFont(14);
    self.originalPriceLabel.textColor = FWColor(@"bcbcbc");
    self.originalPriceLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.originalPriceLabel];
    [self.originalPriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.priceLabel.mas_right).mas_offset(10);
        make.bottom.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(18);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.areaLabel = [[UILabel alloc] init];
    self.areaLabel.font = DHFont(14);
    self.areaLabel.textColor = FWTextColor_9C9C9C;
    self.areaLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.areaLabel];
    [self.areaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.priceLabel);
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(10);
    }];
    
    self.youfeiLabel = [[UILabel alloc] init];
    self.youfeiLabel.font = DHFont(14);
    self.youfeiLabel.textColor = FWTextColor_9C9C9C;
    self.youfeiLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.youfeiLabel];
    [self.youfeiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.areaLabel);
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.top.mas_equalTo(self.areaLabel.mas_bottom).mas_offset(10);
    }];
    
    contentLabel = [[UILabel alloc] init];
    contentLabel.numberOfLines = 0;
    contentLabel.font = DHFont(14);
    contentLabel.textColor = FWColor(@"222222");
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:contentLabel];
    [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.areaLabel);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.top.mas_equalTo(self.youfeiLabel.mas_bottom).mas_offset(10);
    }];

    [contentLabel layoutIfNeeded];

    containerView = [[UIView alloc] init];
    [self addSubview:containerView];
    containerView.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame)+5, CGRectGetWidth(self.contentLabel.frame), 40);
    
    self.carTypeLabel = [[UILabel alloc] init];
    self.carTypeLabel.userInteractionEnabled = YES;
    self.carTypeLabel.font = DHFont(14);
    self.carTypeLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.carTypeLabel];
    self.carTypeLabel.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame), CGRectGetMaxY(self.containerView.frame)+5, 200, 20);
    [self.carTypeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(carTypeLabelClick)]];

    
    timeLabel = [[UILabel alloc] init];
    timeLabel.text = @"";
    timeLabel.font = DHSystemFontOfSize_12;
    timeLabel.textColor = FWTextColor_969696;
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:timeLabel];
    timeLabel.frame = CGRectMake(12,CGRectGetMaxY(self.carTypeLabel.frame)+10 , 200, 20);
    
    lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self addSubview:lineView];
    lineView.frame = CGRectMake(0,CGRectGetMaxY(self.timeLabel.frame)+10, SCREEN_WIDTH, 0.5);
    
    goodsView = [[UIView alloc] init];
    goodsView.clipsToBounds = YES;
    goodsView.userInteractionEnabled = YES;
    goodsView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:goodsView];
    goodsView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), SCREEN_WIDTH, 130);
    UITapGestureRecognizer * goodstap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goodsViewClick)];
    [goodsView addGestureRecognizer:goodstap];
    
    recammondLabel = [[UILabel alloc] init];
    recammondLabel.text = @"推荐商品";
    recammondLabel.textColor = FWTextColor_000000;
    recammondLabel.textAlignment = NSTextAlignmentLeft;
    recammondLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [goodsView addSubview:recammondLabel];
    recammondLabel.frame = CGRectMake(CGRectGetMinX(timeLabel.frame), 0, 100, 25);
    
    goodsImageView = [[UIImageView alloc] init];
    goodsImageView.layer.cornerRadius = 2;
    goodsImageView.layer.masksToBounds = YES;
    goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
    [goodsView addSubview:goodsImageView];
    goodsImageView.frame = CGRectMake(CGRectGetMinX(recammondLabel.frame), CGRectGetMaxY(recammondLabel.frame) +5, 88, 88);
    
    goodsTitleLabel = [[UILabel alloc] init];
    goodsTitleLabel.textColor = FWTextColor_000000;
    goodsTitleLabel.numberOfLines = 2;
    goodsTitleLabel.textAlignment = NSTextAlignmentLeft;
    goodsTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [goodsView addSubview:goodsTitleLabel];
    [goodsTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(goodsImageView).mas_offset(12);
        make.height.mas_greaterThanOrEqualTo (10);
        make.right.mas_equalTo(goodsView).mas_offset(-12);
        make.width.mas_equalTo(SCREEN_WIDTH-CGRectGetMaxX(goodsImageView.frame)-24);
    }];
    
    goodsPriceLabel = [[UILabel alloc] init];
    goodsPriceLabel.textColor = DHRedColorff6f00;
    goodsPriceLabel.textAlignment = NSTextAlignmentLeft;
    goodsPriceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    [goodsView addSubview:goodsPriceLabel];
    [goodsPriceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(goodsImageView).mas_offset(-10);
        make.height.mas_equalTo (20);
        make.left.mas_equalTo(goodsTitleLabel);
        make.width.mas_equalTo(150);
    }];
    
    goodsScanLabel = [[UILabel alloc] init];
    goodsScanLabel.textColor = FWTextColor_9C9C9C;
    goodsScanLabel.textAlignment = NSTextAlignmentRight;
    goodsScanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [goodsView addSubview:goodsScanLabel];
    [goodsScanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(goodsPriceLabel);
        make.height.mas_equalTo (20);
        make.right.mas_equalTo(goodsView).mas_offset(-12);
        make.width.mas_greaterThanOrEqualTo(5);
    }];
    
    goodsScanView = [[UIImageView alloc] init];
    goodsScanView.image = [UIImage imageNamed:@"card_eye"];
    [goodsView addSubview:goodsScanView];
    [goodsScanView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(14, 10));
        make.right.mas_equalTo(goodsScanLabel.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(goodsPriceLabel);
    }];
    
    goodsLineView = [UIView new];
    goodsLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self addSubview:goodsLineView];
    goodsLineView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame)-1, SCREEN_WIDTH, 0.5);
  
    goodsView.hidden = YES;
    
    commentLabel = [[UILabel alloc] init];
    commentLabel.text = @"0条留言";
    commentLabel.font = DHSystemFontOfSize_16;
    commentLabel.textColor = FWTextColor_000000;
    commentLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:commentLabel];
    commentLabel.frame = CGRectMake(12,CGRectGetMaxY(lineView.frame)+5 , 100, 20);

    commentNumberButton = [[UIButton alloc] init];
    commentNumberButton.titleLabel.textAlignment = NSTextAlignmentRight;
    commentNumberButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    commentNumberButton.titleLabel.font = DHSystemFontOfSize_12;
    [commentNumberButton setTitle:@"按时间" forState:UIControlStateNormal];
    [commentNumberButton setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
    [commentNumberButton addTarget:self action:@selector(commentNumberButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:commentNumberButton];
    [commentNumberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-12);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(30);
        make.centerY.mas_equalTo(commentLabel);
    }];
}

- (void)configViewForModel:(id)model{
 
    listModel = (FWFeedListModel *)model;

    if (listModel.imgs.count >0) {
        // 轮播图
        NSMutableArray * tempArr = @[].mutableCopy;
        
        for (FWFeedImgsModel * imageModel in listModel.imgs) {
            [tempArr addObject:imageModel.img_url];
        }

        if (tempArr.count > 0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                bannerView.imageURLStringsGroup = tempArr;
            });
        }else{
            if ([self.listModel.feed_type isEqualToString:@"4"]) {
                bannerView.placeholderImage = [UIImage imageNamed:@""];
            }
        }
    }
    
    [self setupGoodsTagsView];
    
    self.priceLabel.text = listModel.price_sell_show;
    self.originalPriceLabel.text = [NSString stringWithFormat:@"原价%@",listModel.price_buy_show];
    self.areaLabel.text = [NSString stringWithFormat:@"发布于 %@%@%@",listModel.province_code,listModel.city_code,listModel.county_code];
    
    if ([listModel.freight isEqualToString:@"2"]) {
        /* 包邮 */
        self.youfeiLabel.text = [NSString stringWithFormat:@"邮费：%@",@"包邮"];
    }else{
        self.youfeiLabel.text = [NSString stringWithFormat:@"邮费：%@",@"按距离计算"];
    }
    
    self.contentLabel.text = listModel.feed_title;
    
    timeLabel.text = [NSString stringWithFormat:@"%@    %@次浏览",listModel.format_create_time,listModel.count_realtime.pv_count_format];
    
    NSString * comment_count = listModel.count_realtime.comment_count?listModel.count_realtime.comment_count:@"0";
    
    commentLabel.text = [NSString stringWithFormat:@"%@条留言",comment_count];

    containerArr = listModel.tags;
    
    // 标签
    [self refreshContainer:containerView WithArray:containerArr];

    if (listModel.goods_id.length > 0 && ![listModel.goods_id isEqualToString:@"0"]) {
        
        goodsTitleLabel.text = listModel.goods_info.title;
        goodsPriceLabel.text = [NSString stringWithFormat:@"￥%@",listModel.goods_info.price];
        goodsScanLabel.text = listModel.goods_info.click_count;
        [goodsImageView sd_setImageWithURL:[NSURL URLWithString:listModel.goods_info.cover.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        goodsView.hidden = NO;
        goodsLineView.hidden = NO;
    }else{
        goodsView.hidden = YES;
        goodsLineView.hidden = YES;
    }

    if (self.listModel.car_type.length > 0) {
        self.carTypeLabel.text = self.listModel.car_type;
        
        self.carTypeLabel.font = DHFont(13);
        NSString * str1 = @"关联车系 ";
        NSString * str2 = self.listModel.car_type;
        NSMutableAttributedString * hotLineStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",str1,str2]];
        [hotLineStr addAttribute:NSForegroundColorAttributeName value:FWTextColor_222222 range:NSMakeRange(0,str1.length)];
        [hotLineStr addAttribute:NSForegroundColorAttributeName value:FWColor(@"4169E1") range:NSMakeRange(str1.length,str2.length)];
        self.carTypeLabel.attributedText = hotLineStr;
    }
    
    [self refreshFrame];
}

- (void)setupGoodsTagsView{
    for (UIView * view in self.goodsTagsView.subviews) {
        [view removeFromSuperview];
    }
    
    if (self.tagsNameArray.count > 0) {
        [self.tagsNameArray removeAllObjects];
    }
    
    if ([listModel.if_quanxin isEqualToString:@"1"]) {
        /* 全新 */
        [self.tagsNameArray addObject:@"全新"];
    }

    if ([listModel.if_ziti isEqualToString:@"1"]) {
        /* 自提 */
        [self.tagsNameArray addObject:@"自提"];
    }
    
    if ([listModel.freight isEqualToString:@"2"]) {
        /* 包邮 */
        [self.tagsNameArray addObject:@"包邮"];
    }
    
    CGFloat X = 0;
    for (int i = 0; i < self.tagsNameArray.count; i++) {
        
        UILabel * tagsNameLabel = [[UILabel alloc] init];
        tagsNameLabel.font = DHFont(12);
        tagsNameLabel.text = self.tagsNameArray[i];
        tagsNameLabel.textColor = FWTextColor_9C9C9C;
        tagsNameLabel.layer.borderColor = FWTextColor_9C9C9C.CGColor;
        tagsNameLabel.layer.borderWidth = 1;
        tagsNameLabel.layer.cornerRadius = 2;
        tagsNameLabel.layer.masksToBounds = YES;
        tagsNameLabel.textAlignment = NSTextAlignmentCenter;
        [self.goodsTagsView addSubview:tagsNameLabel];
        tagsNameLabel.frame = CGRectMake(X, 0, 35, 22);
        
        X += 45;
    }
}

#pragma mark - > 重新布局
- (void)refreshFrame{
       
    CGFloat picHight = SCREEN_WIDTH;
    CGFloat picWidth = SCREEN_WIDTH;

    CGFloat currentContentY = 0;
    
    if (listModel.imgs.count >0) {
        // 轮播图
        NSMutableArray * tempArr = @[].mutableCopy;
        
        for (FWFeedImgsModel * imageModel in listModel.imgs) {
            [tempArr addObject:imageModel.img_url];
        }

        if (tempArr.count > 0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                bannerView.imageURLStringsGroup = tempArr;
            });
        }else{
            if ([self.listModel.feed_type isEqualToString:@"4"]) {
                bannerView.placeholderImage = [UIImage imageNamed:@""];
            }
        }

        
        if ([listModel.imgs[0].img_width floatValue] == 0) {
            listModel.imgs[0].img_width = @(SCREEN_WIDTH).stringValue;
        }
        if ([listModel.imgs[0].img_height floatValue] == 0) {
            listModel.imgs[0].img_height = @(SCREEN_WIDTH).stringValue;
        }
        
        CGFloat k = [listModel.imgs[0].img_width floatValue]/[listModel.imgs[0].img_height floatValue];
        if ( k>= 0.667 && k<= 1.5) {
            picHight = picWidth / k;
        }else if( k < 0.667) {
            picHight = picWidth *1.5;
        }else if (k >1.5){
            picHight = picWidth *0.667;
        }
        bannerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, picHight);
        currentContentY =  CGRectGetMaxY(self.bannerView.frame)+20;
    }else{
        currentContentY =  20;
    }
    
    
    if (self.tagsNameArray.count > 0) {
        [self.goodsTagsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.bannerView).mas_offset(14);
            make.height.mas_equalTo(22);
            make.width.mas_equalTo(SCREEN_WIDTH-28);
            make.top.mas_equalTo(bannerView.mas_bottom).mas_offset(10);
        }];
    }else{
        [self.goodsTagsView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.bannerView).mas_offset(14);
            make.height.mas_equalTo(0.01);
            make.width.mas_equalTo(SCREEN_WIDTH-28);
            make.top.mas_equalTo(bannerView.mas_bottom).mas_offset(5);
        }];
    }
    
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.goodsTagsView);
        make.top.mas_equalTo(self.goodsTagsView.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(22);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    [self.areaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.priceLabel);
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(5);
    }];
    
    [self.youfeiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.areaLabel);
        make.height.mas_equalTo(18);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.top.mas_equalTo(self.areaLabel.mas_bottom).mas_offset(5);
    }];
    
    [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.areaLabel);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.top.mas_equalTo(self.youfeiLabel.mas_bottom).mas_offset(5);
    }];
    
    
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.contentLabel);
        make.height.mas_equalTo(containerHeight);
        make.left.mas_equalTo(self.contentLabel);
        make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(5);
    }];
    
    CGFloat carTypeHeight = 0.01;
    if (self.listModel.car_type.length > 0) {
        carTypeHeight = 20;
    }
    [self.carTypeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(carTypeHeight);
        make.left.mas_equalTo(self.contentLabel);
        make.top.mas_equalTo(self.containerView.mas_bottom).mas_offset(5);
    }];
    
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(12);
        make.top.mas_equalTo(self.carTypeLabel.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(20);
    }];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(0);
        make.top.mas_equalTo(timeLabel.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(0.5);
    }];
    
    [self layoutIfNeeded];
    
    if (listModel.goods_id.length > 0 && ![listModel.goods_id isEqualToString:@"0"]) {
        goodsView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), SCREEN_WIDTH, 130);
        goodsLineView.frame = CGRectMake(0,CGRectGetMaxY(self.goodsView.frame)-1, SCREEN_WIDTH, 0.5);
        commentLabel.frame = CGRectMake(12,CGRectGetMaxY(goodsLineView.frame)+5 , 100, 20);
    }else{
        commentLabel.frame = CGRectMake(12,CGRectGetMaxY(lineView.frame)+5 , 100, 20);
    }
    
    [commentNumberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-12);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(30);
        make.centerY.mas_equalTo(commentLabel);
    }];
}

- (void) refreshContainer:(UIView *)container WithArray:(NSArray *)array{
    
    for (UIView * view in container.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    CGFloat x = 0;
    CGFloat y = 5;
    CGFloat width = 0.0;
    CGFloat height = 24;
    
    for (int i = 0; i < array.count; i++) {
        FWSearchTagsListModel * tagsModel = array[i];
        
        width = [tagsModel.tag_name length]*14+20;
        
        if (x + width+20 >= SCREEN_WIDTH) {
            x = 0;
            y += height + 10;
        }
        
        FWModuleButton * btn = [[FWModuleButton alloc] init];
        btn.titleLabel.font = DHSystemFontOfSize_14;
        btn.backgroundColor = FWViewBackgroundColor_EEEEEE;
        btn.nameLabel.textColor = FWColor(@"515151");
        btn.tag = 10000+i;
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        btn.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
        [btn setModuleTitle:tagsModel.tag_name];
        [btn setModuleImage:@"home_tag"];
        [btn addTarget:self action:@selector(btnTagsClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
        [container addSubview:btn];
        [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(container).mas_offset(x);
            make.top.mas_equalTo(container).mas_offset(y);
            make.height.mas_equalTo(height);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
        
        [btn.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btn);
            make.left.mas_equalTo(btn).mas_offset(8);
            make.size.mas_equalTo(CGSizeMake(14, 14));
        }];

        [btn.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(btn.iconImageView.mas_right).mas_offset(4);
            make.height.mas_equalTo(20);
            make.centerY.mas_equalTo(btn);
            make.width.mas_greaterThanOrEqualTo(10);
            make.right.mas_equalTo(btn).mas_offset(-9);
        }];
        
        [container layoutIfNeeded];
        x += CGRectGetWidth(btn.frame)+10;
    }
    
    if (array.count <= 0) {
        containerHeight = y;
    }else{
        containerHeight = y + 30;
    }
}

#pragma mark - > 跳转商品
- (void)goodsViewClick{
    
    FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
    GDVC.goods_id = listModel.goods_id;
    [self.vc.navigationController pushViewController:GDVC animated:YES];
}

#pragma mark - > 关注
- (void)attentionButtonOnClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([self.headerDelegate respondsToSelector:@selector(attentionButtonClick)]) {
            [self.headerDelegate attentionButtonClick];
        }
    }
}

#pragma mark - > 点击头像
- (void)photoImageTap:(UITapGestureRecognizer *)gesture{
    if (nil == self.listModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 点击标签
- (void)btnTagsClick:(UIButton *)sender{
    
    FWSearchTagsSubListModel * tagsModel = self.listModel.tags[sender.tag-10000];
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.traceType = 2;
    TVC.tagsModel = tagsModel;
    TVC.tags_id = tagsModel.tag_id;
    [self.viewController.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 排序（new：按时间  hot:按热度）
- (void)commentNumberButtonOnClick{
    
    if ([self.sortType isEqualToString:@"new"]) {
        self.sortType = @"hot";
        [commentNumberButton setTitle:@"按热度" forState:UIControlStateNormal];
    }else{
        self.sortType = @"new";
        [commentNumberButton setTitle:@"按时间" forState:UIControlStateNormal];
    }
    
    if ([self.headerDelegate respondsToSelector:@selector(commentSort:)]) {
        [self.headerDelegate commentSort:self.sortType];
    }
}

#pragma mark - > 点击车系
- (void)carTypeLabelClick{
    
    FWRefitCaseViewController * RCVC = [[FWRefitCaseViewController alloc] init];
    RCVC.category_id = @"";
    RCVC.car_type = self.listModel.car_type;
    [self.vc.navigationController pushViewController:RCVC animated:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

#pragma mark - > banner跳转
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{

    NSMutableArray * smallArray = @[].mutableCopy;
    NSMutableArray * originalArray = @[].mutableCopy;

    for (FWFeedImgsModel * model in self.listModel.imgs) {
       [smallArray addObject:model.img_url];
    }

    for (FWFeedImgsModel * model in self.listModel.imgs_original) {
       [originalArray addObject:model.img_url];
    }

    if (index < self.listModel.imgs_original.count) {
       
       FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
       browser.isFullWidthForLandScape = YES;
       browser.isNeedLandscape = YES;
       browser.currentImageIndex = (int)index;
       browser.imageArray = [smallArray mutableCopy];
       browser.smallArray = [smallArray copy];
       browser.originalImageArray = originalArray;
       browser.vc = self.vc;
       browser.fromType = 3;
       [browser show];
    }
}

- (CGFloat)getCurrentViewHeight{
    
    CGFloat height = 0;
    
    height = CGRectGetMaxY(commentLabel.frame) +10;
    
    return height;
}


@end
