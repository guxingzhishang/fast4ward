//
//  FWCommentHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWCommentHeaderView.h"
#import "FWCommentFrame.h"
#import "FWLikeRequest.h"

@interface FWCommentHeaderView ()

@end

@implementation FWCommentHeaderView

+ (instancetype)topicHeaderView
{
    return [[self alloc] init];
}

+ (instancetype)headerViewWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"TopicHeader";
    FWCommentHeaderView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if (header == nil) {
        // 缓存池中没有, 自己创建
        header = [[self alloc] initWithReuseIdentifier:ID];
    }
    return header;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        // 初始化
        [self _setup];
        
        // 创建自控制器
        [self _setupSubViews];
        
        // 布局子控件
        [self _makeSubViewsConstraints];
    }
    return self;
}

#pragma mark - 公共方法
#pragma mark - Setter
- (void)setTopicFrame:(FWTopicCommentFrame *)topicFrame
{
    _topicFrame = topicFrame;
    
    FWCommentListModel *topic = topicFrame.topic;
    FWMineInfoModel *user = topic.user_info;
    
    // 头像
    self.avatarView.frame = topicFrame.avatarFrame;
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:user.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    // 昵称
    self.nicknameLable.frame = topicFrame.nicknameFrame;
    self.nicknameLable.text = user.nickname;
    
    // 点赞
    self.thumbBtn.frame = topicFrame.thumbFrame;
    
    // 点赞图标
    self.thumbImageView.frame = topicFrame.thumbImageFrame;
    if ([topic.is_liked isEqualToString:@"1"]) {
        self.thumbImageView.image = [UIImage imageNamed:@"car_detail_like"];
    }else{
        self.thumbImageView.image = [UIImage imageNamed:@"unlike"];
    }
    
    // 点赞数
    self.thumbNumberLabel.frame = topicFrame.thumbNumberFrame;
    self.thumbNumberLabel.text = topic.like_count_format;

    // 时间
    self.createTimeLabel.frame = topicFrame.createTimeFrame;
    self.createTimeLabel.text = topic.format_create_time;
    
    // 内容
    self.contentLabel.frame = topicFrame.textFrame;
    self.contentLabel.attributedText = topic.attributedText;
    
    CGFloat commentButtonX = topicFrame.nicknameFrame.origin.x;
    CGFloat commentButtonY = topicFrame.createTimeFrame.origin.y+topicFrame.createTimeFrame.size.height/2;
    CGFloat commentButtonW = topicFrame.textFrame.size.width - topicFrame.thumbFrame.size.width ;
    CGFloat commentButtonH = topicFrame.textFrame.size.height + topicFrame.textFrame.origin.y;

    self.commentButton.frame = CGRectMake(commentButtonX, commentButtonY, commentButtonW, commentButtonH);
}


#pragma mark - 私有方法
#pragma mark - 初始化
- (void)_setup
{
    self.contentView.userInteractionEnabled = YES;
    self.contentView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - 创建自控制器
- (void)_setupSubViews
{
    // 头像
    UIImageView *avatarView = [UIImageView new];
    avatarView.userInteractionEnabled = YES;
    avatarView.layer.cornerRadius = MHTopicAvatarWH*.5f;
    // 这样写比较消耗性能
    avatarView.layer.masksToBounds = YES;
    avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView = avatarView;
    [self.contentView addSubview:avatarView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_avatarOrNicknameDidClicked)];
    [avatarView addGestureRecognizer:tap];
    
    
    // 昵称
    YYLabel *nicknameLable = [[YYLabel alloc] init];
    nicknameLable.text = @"";
    nicknameLable.font = DHSystemFontOfSize_14;
    nicknameLable.textAlignment = NSTextAlignmentLeft;
    nicknameLable.userInteractionEnabled = YES;
    nicknameLable.textColor = FWTextColor_000000;
    [self.contentView addSubview:nicknameLable];
    self.nicknameLable = nicknameLable;
    UITapGestureRecognizer * nameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_avatarOrNicknameDidClicked)];
    [nicknameLable addGestureRecognizer:nameTap];
    
    // 时间
    YYLabel *createTimeLabel = [[YYLabel alloc] init];
    createTimeLabel.text = @"";
    createTimeLabel.font = DHSystemFontOfSize_12;
    createTimeLabel.textAlignment = NSTextAlignmentLeft;
    createTimeLabel.textColor = FWTextColor_797980;
    [self.contentView addSubview:createTimeLabel];
    self.createTimeLabel = createTimeLabel;
    createTimeLabel.textTapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
//        [weakSelf _avatarOrNicknameDidClicked];
    };
    
    
    // 点赞按钮
    UIButton *thumbBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    thumbBtn.adjustsImageWhenHighlighted = NO;
    [thumbBtn addTarget:self action:@selector(_thumbBtnDidClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:thumbBtn];
    self.thumbBtn = thumbBtn;
    
    UIImageView * thumbImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"unlike"]];
    [thumbBtn addSubview:thumbImageView];
    self.thumbImageView = thumbImageView;
    
    // 点赞数
    YYLabel * thumbNumberLabel = [[YYLabel alloc] init];
    thumbNumberLabel.userInteractionEnabled = NO;
    thumbNumberLabel.text = @"";
    thumbNumberLabel.font = DHSystemFontOfSize_12;
    thumbNumberLabel.textAlignment = NSTextAlignmentCenter;
    thumbNumberLabel.textColor = DHTitleColor_666666;
    [thumbBtn addSubview:thumbNumberLabel];
    self.thumbNumberLabel = thumbNumberLabel;
    
    // 文本
    YYLabel *contentLabel = [[YYLabel alloc] init];
    // 设置文本的额外区域，修复用户点击文本没有效果
    UIEdgeInsets textContainerInset = contentLabel.textContainerInset;
    textContainerInset.top = MHTopicVerticalSpace;
    textContainerInset.bottom = MHTopicVerticalSpace;
    contentLabel.textContainerInset = textContainerInset;
    contentLabel.textColor = DHTitleColor_666666;
    contentLabel.numberOfLines = 0 ;
    contentLabel.font = DHSystemFontOfSize_13;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:contentLabel];
    self.contentLabel = contentLabel;
    
    
    UIButton * commentButton = [[UIButton alloc] init];
    commentButton.backgroundColor = FWClearColor;
    [commentButton addTarget:self action:@selector(commentButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:commentButton];
    self.commentButton = commentButton;
}


#pragma mark - 布局子控件
- (void)_makeSubViewsConstraints
{
    
}

#pragma mark - 事件处理
#pragma mark - > 喜欢该条评论
- (void)_thumbBtnDidClicked:(UIButton *)sender{
    
    [self checkLogin];
    

    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        FWCommentListModel *topic = self.topicFrame.topic;
        
        NSString * type;
        if ([topic.is_liked isEqualToString:@"2"]) {
            type = @"add";
        }else if ([topic.is_liked isEqualToString:@"1"]){
            type = @"cancel";
        }
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"comment_id":topic.comment_id,
                                  @"like_type":type,
                                  };
        
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        [request startWithParameters:params WithAction:Submit_like_comment  WithDelegate:self.viewcontroller completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([type isEqualToString:@"add"]) {
                    
                    topic.is_liked = @"1";
                    
                    if ([topic.like_count_format integerValue] ||
                        [topic.like_count_format integerValue] == 0) {
                        
                        topic.like_count_format = @([topic.like_count_format integerValue] +1).stringValue;
                    }
                }else if ([type isEqualToString:@"cancel"]){
                    
                    topic.is_liked = @"2";
                    
                    if ([topic.like_count_format integerValue] ||
                        [topic.like_count_format integerValue] == 0) {
                        
                        topic.like_count_format = @([topic.like_count_format integerValue] -1).stringValue;
                    }
                }
                
                self.topicFrame.topic = topic;
                [self setTopicFrame:self.topicFrame];
                
                NSInteger index = sender.tag - 66666;
                
                if ([self.delegate respondsToSelector:@selector(_thumbBtnClicked:WithIndex:)]) {
                    [self.delegate _thumbBtnClicked:self.topicFrame WithIndex:index];
                }
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewcontroller];
            }
        }];
    }
}

#pragma mark - > 点击主评论头像
- (void)_avatarOrNicknameDidClicked{
    
    NSInteger index = self.avatarView.tag - 90000;
    if (self.delegate && [self.delegate respondsToSelector:@selector(topicHeaderViewDidClickedUser:WithIndex:)]) {
        [self.delegate topicHeaderViewDidClickedUser:self WithIndex:index];
    }
    
    FWCommentListModel *topic = self.topicFrame.topic;

    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = topic.uid;
    [self.viewcontroller.navigationController pushViewController:UIVC animated:YES];
}


- (void)commentButtonOnClick:(UIButton *)sender{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {

        NSInteger index = sender.tag - 30000;
        if ([self.delegate respondsToSelector:@selector(commentButtonClick:)]) {
            [self.delegate commentButtonClick:index];
        }
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
