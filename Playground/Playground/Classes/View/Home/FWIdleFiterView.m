//
//  FWIdleFiterView.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/18.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWIdleFiterView.h"
#import "UIBarButtonItem+Item.h"
#import "FWArearModel.h"

@interface FWIdleFiterView ()

@property (nonatomic, assign) NSInteger  currentCom;

@property (nonatomic, strong) NSString * proviceName;
@property (nonatomic, strong) NSString * cityName;
@property (nonatomic, strong) NSString * countryName;

@end

@implementation FWIdleFiterView

- (id)init{
    self = [super init];
    if (self) {

        self.currentCom = -1;

        self.paixuArray = @[@"最新发布",@"价格从低到高",@"价格从高到低"];
        
        self.provinceArray = @[].mutableCopy;
        self.cityArray = @[].mutableCopy;
        self.countryArray = @[].mutableCopy;
        
        
        self.areaPickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
        self.areaPickerView.delegate=self;
        self.areaPickerView.dataSource=self;
        self.areaPickerView.backgroundColor=[UIColor clearColor];

        
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
        
        [self requestAreaData];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.tagsCoverView = [[UIImageView alloc] init];
    self.tagsCoverView.layer.borderColor = [UIColor colorWithHexString:@"eeeeee" alpha:1].CGColor;
    self.tagsCoverView.contentMode = UIViewContentModeScaleAspectFill;
    self.tagsCoverView.layer.masksToBounds = YES;
    [self addSubview:self.tagsCoverView];
    self.tagsCoverView.frame = CGRectMake(14, 20, 90, 90);
    
    self.tagsLabel = [[UILabel alloc] init];
    self.tagsLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.tagsLabel.textColor = FWTextColor_272727;
    self.tagsLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.tagsLabel];
    self.tagsLabel.frame = CGRectMake(CGRectGetMaxX(self.tagsCoverView.frame)+5, CGRectGetMinY(self.tagsCoverView.frame)+25, SCREEN_WIDTH-28-5-90, 20);

    
    self.desLabel = [[UILabel alloc] init];
    self.desLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 13];
    self.desLabel.textColor = FWTextColor_9EA3AB;
    self.desLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.desLabel];
    self.desLabel.frame = CGRectMake(CGRectGetMinX(self.tagsLabel.frame), CGRectGetMaxY(self.tagsLabel.frame)+5, SCREEN_WIDTH-28-5-140, 20);

    /* 综合区域 */
    self.zongheTF = [[UITextField alloc] init];
    self.zongheTF.delegate = self;
    [self addSubview:self.zongheTF];
    self.zongheTF.frame = CGRectMake(-50, CGRectGetMaxY(self.tagsCoverView.frame)+5, SCREEN_WIDTH/3+50, 35);
    
    self.zonghePickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.zonghePickerView.delegate=self;
    self.zonghePickerView.dataSource=self;
    self.zonghePickerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.zonghePickerView.backgroundColor=[UIColor clearColor];
    
    UIToolbar *bar1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    
    UIBarButtonItem *doneButton1 = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneButtonOneClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton1 = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonOneClick) forControlEvents:UIControlEventTouchUpInside];
    
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem1.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar1.items = @[cancelButton1,flexItem1, fixItem1, doneButton1];
    
    self.zongheTF.inputView = self.zonghePickerView;
    self.zongheTF.inputAccessoryView = bar1;
    
    
    self.zongheLabel = [[UILabel alloc] init];
    self.zongheLabel.font = DHFont(14);
    self.zongheLabel.text = @"综合排序";
    self.zongheLabel.textColor = FWTextColor_222222;
    self.zongheLabel.textAlignment = NSTextAlignmentLeft;
    [self.zongheTF addSubview:self.zongheLabel];
    self.zongheLabel.frame = CGRectMake(68, 0, CGRectGetWidth(self.zongheTF.frame)-15, CGRectGetHeight(self.zongheTF.frame));
    
    self.zongheArrow = [[UIImageView alloc] init];
    self.zongheArrow.image = [UIImage imageNamed:@"icon_zhankai_1"];
    [self.zongheTF addSubview:self.zongheArrow];
    [self.zongheArrow mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.zongheTF).mas_offset(130);
        make.centerY.mas_equalTo(self.zongheTF);
        make.size.mas_equalTo(CGSizeMake(10, 10));
    }];
    
    /* 选择区域 */
    self.areaTF = [[UITextField alloc] init];
    self.areaTF.delegate = self;
    [self addSubview:self.areaTF];
    self.areaTF.frame = CGRectMake(CGRectGetMaxX(self.zongheTF.frame)-50, CGRectGetMinY(self.zongheTF.frame), SCREEN_WIDTH*2/3+50, 35);

    UIToolbar *bar2 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    
    UIBarButtonItem *doneButton2 = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneButtonTwoClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton2 = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonTwoClick) forControlEvents:UIControlEventTouchUpInside];
    
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem2.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar2.items = @[cancelButton2,flexItem2, fixItem2, doneButton2];
    
    self.areaTF.inputView = self.areaPickerView;
    self.areaTF.inputAccessoryView = bar2;
        
    self.areaLabel = [[UILabel alloc] init];
    self.areaLabel.font = DHFont(14);
    self.areaLabel.text = @"区域";
    self.areaLabel.textColor = FWTextColor_222222;
    self.areaLabel.textAlignment = NSTextAlignmentLeft;
    [self.areaTF addSubview:self.areaLabel];
    self.areaLabel.frame = CGRectMake(56, 0,  SCREEN_WIDTH*2/3, CGRectGetHeight(self.areaTF.frame));
    
    self.areaArrow = [[UIImageView alloc] init];
    self.areaArrow.image = [UIImage imageNamed:@"icon_zhankai_1"];
    [self.areaTF addSubview:self.areaArrow];
    [self.areaArrow mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.areaTF).mas_offset(90);
        make.centerY.mas_equalTo(self.areaTF);
        make.size.mas_equalTo(CGSizeMake(10, 10));
    }];
    
    self.chooseView = [[UIView alloc] init];
    [self addSubview:self.chooseView];
    self.chooseView.frame = CGRectMake(0, CGRectGetMaxY(self.areaTF.frame), SCREEN_WIDTH, 40);
    
    self.chooseLable = [[UILabel alloc] init];
    self.chooseLable.text = @"筛选：";
    self.chooseLable.font = DHFont(14);
    self.chooseLable.textColor = FWTextColor_222222;
    self.chooseLable.textAlignment = NSTextAlignmentLeft;
    [self.chooseView addSubview:self.chooseLable];
    self.chooseLable.frame = CGRectMake(18, 0, 50, 40);
    
    self.allNewButton = [[UIButton alloc] init];
    self.allNewButton.selected = NO;
    self.allNewButton.titleLabel.font = DHFont(14);
    self.allNewButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
    self.allNewButton.layer.borderWidth = 1;
    self.allNewButton.layer.cornerRadius = 2;
    self.allNewButton.layer.masksToBounds = YES;
    [self.allNewButton setTitle:@"全新" forState:UIControlStateNormal];
    [self.allNewButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.allNewButton addTarget:self action:@selector(allNewButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.chooseView addSubview:self.allNewButton];
    self.allNewButton.frame = CGRectMake(CGRectGetMaxX(self.chooseLable.frame)+5, 7.5, 60, 25);
    
    self.baoyouButton = [[UIButton alloc] init];
    self.baoyouButton.selected = NO;
    self.baoyouButton.titleLabel.font = DHFont(14);
    self.baoyouButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
    self.baoyouButton.layer.borderWidth = 1;
    self.baoyouButton.layer.cornerRadius = 2;
    self.baoyouButton.layer.masksToBounds = YES;
    [self.baoyouButton setTitle:@"包邮" forState:UIControlStateNormal];
    [self.baoyouButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.baoyouButton addTarget:self action:@selector(baoyouButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.chooseView addSubview:self.baoyouButton];
    self.baoyouButton.frame = CGRectMake(CGRectGetMaxX(self.allNewButton.frame)+20, CGRectGetMinY(self.allNewButton.frame), CGRectGetWidth(self.allNewButton.frame),CGRectGetHeight(self.allNewButton.frame));
    
    self.zitiButton = [[UIButton alloc] init];
    self.zitiButton.selected = NO;
    self.zitiButton.titleLabel.font = DHFont(14);
    self.zitiButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
    self.zitiButton.layer.borderWidth = 1;
    self.zitiButton.layer.cornerRadius = 2;
    self.zitiButton.layer.masksToBounds = YES;
    [self.zitiButton setTitle:@"自提" forState:UIControlStateNormal];
    [self.zitiButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.zitiButton addTarget:self action:@selector(zitiButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.chooseView addSubview:self.zitiButton];
    self.zitiButton.frame = CGRectMake(CGRectGetMaxX(self.baoyouButton.frame)+20, CGRectGetMinY(self.baoyouButton.frame), CGRectGetWidth(self.baoyouButton.frame),CGRectGetHeight(self.baoyouButton.frame));

    self.zongheTF.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self bringSubviewToFront:self.zongheTF];
}

- (void)congifViewWithModel:(id)model{
    
    self.feedModel = (FWFeedModel *)model;
    
    self.tagsLabel.text = self.feedModel.xianzhi_category_name;
    [self.tagsCoverView sd_setImageWithURL:[NSURL URLWithString:self.feedModel.xianzhi_category_img] placeholderImage:[UIImage imageNamed:@"tags_icon"]];
    self.desLabel.text = [NSString stringWithFormat:@"%@条内容",self.feedModel.total_count];
}

#pragma mark - > 是否全新
- (void)allNewButtonClick{
    
    self.allNewButton.selected = !self.allNewButton.selected;
    
    if (self.allNewButton.isSelected) {
        /* 全新 */
        self.allNewButton.backgroundColor = FWTextColor_BCBCBC;
        self.allNewButton.layer.borderColor = FWClearColor.CGColor;
        [self.allNewButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        
        if ([self.delegate respondsToSelector:@selector(allNewClick:)]) {
            [self.delegate allNewClick:@"1"];
        }
    }else{
        self.allNewButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.allNewButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
        [self.allNewButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        
        if ([self.delegate respondsToSelector:@selector(allNewClick:)]) {
            [self.delegate allNewClick:@"2"];
        }
    }
}

#pragma mark - > 包邮
- (void)baoyouButtonClick{
    
    self.baoyouButton.selected = !self.baoyouButton.selected;
    
    if (self.baoyouButton.isSelected) {
        /* 包邮 */
        self.baoyouButton.backgroundColor = FWTextColor_BCBCBC;
        self.baoyouButton.layer.borderColor = FWClearColor.CGColor;
        [self.baoyouButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        
        if ([self.delegate respondsToSelector:@selector(baoyouClick:)]) {
            [self.delegate baoyouClick:@"1"];
        }
    }else{
        self.baoyouButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.baoyouButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
        [self.baoyouButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        
        if ([self.delegate respondsToSelector:@selector(baoyouClick:)]) {
            [self.delegate baoyouClick:@"2"];
        }
    }
}

#pragma mark - > 自提
- (void)zitiButtonClick{
    
    self.zitiButton.selected = !self.zitiButton.selected;
    
    if (self.zitiButton.isSelected) {
        /* 自提 */
        self.zitiButton.backgroundColor = FWTextColor_BCBCBC;
        self.zitiButton.layer.borderColor = FWClearColor.CGColor;
        [self.zitiButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        
        
        if ([self.delegate respondsToSelector:@selector(zitiClick:)]) {
            [self.delegate zitiClick:@"1"];
        }
    }else{
        self.zitiButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.zitiButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
        [self.zitiButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        
        if ([self.delegate respondsToSelector:@selector(zitiClick:)]) {
            [self.delegate zitiClick:@"2"];
        }
    }
}

#pragma mark - > pickerview数据源及代理方法
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (pickerView == self.zonghePickerView) {
        return 1;
    }else{
        return 3;
    }
}

#pragma mark 数据源  numberOfRowsInComponent
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == self.zonghePickerView) {
        return self.paixuArray.count;
    }else {
        if (component == 0) {
            return self.provinceArray.count;
        }else if (component == 1){
            return self.cityArray.count;
        }else{
            return self.countryArray.count;
        }
    }
}

#pragma delegate 显示信息方法
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == self.zonghePickerView) {
        return [self.paixuArray objectAtIndex:row];
    }else{
        if (component==0){
              if (self.provinceArray.count > row) {
                  return ((FWRegionListModel *)self.provinceArray[row]).region_name;
              }
          }else if (component==1){
              if (self.cityArray.count > row) {
                  return ((FWRegionListModel *)self.cityArray[row]).region_name;
              }
          }else{
              if (self.countryArray.count > row) {
                  return ((FWRegionListModel *)self.countryArray[row]).region_name;
              }
          }
    }
    return @"";
}

#pragma mark 选中行信息
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (pickerView == self.areaPickerView) {
       
        if (component==0){

            self.currentCom = 0;

            if (self.provinceArray.count > row) {
                self.proviceName = ((FWRegionListModel *)self.provinceArray[row]).region_name;
            }
            [self requestCityComponent];

            if (self.countryArray.count > 0) {
                [self.countryArray removeAllObjects];
            }

            [pickerView reloadComponent:1];
            [pickerView reloadComponent:2];
            [pickerView selectRow:0 inComponent:1 animated:YES];
        }else if (component == 1){

            self.currentCom = 1;
            if (self.cityArray.count > row) {
                self.cityName = ((FWRegionListModel *)self.cityArray[row]).region_name;
            }
            [self requestThirdComponent];
            [pickerView reloadComponent:2];
            [pickerView selectRow:0 inComponent:2 animated:YES];
        }else{
            if (self.countryArray.count > row) {
                self.countryName = ((FWRegionListModel *)self.countryArray[row]).region_name;
            }
        }
    }
}

#pragma mark 显示行高
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.00;
}

#pragma mark - pickerView 每列宽度
- (CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    if (pickerView == self.zonghePickerView) {
        return SCREEN_WIDTH;
    }
    return SCREEN_WIDTH/3;
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = (UILabel *)view;
    if (lbl == nil) {
        lbl = [[UILabel alloc]init];
        //在这里设置字体相关属性
        lbl.font = [UIFont systemFontOfSize:18];
        lbl.textColor = FWTextColor_000000;
        [lbl setTextAlignment:1];
        [lbl setBackgroundColor:[UIColor clearColor]];
    }
    //重新加载lbl的文字内容
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return lbl;
}

#pragma mark - > 综合筛选选择
- (void)doneButtonOneClick{
    
    NSInteger onerow=[self.zonghePickerView selectedRowInComponent:0];

    if (self.paixuArray.count > onerow) {
        self.zongheLabel.text = self.paixuArray[onerow];
        
        NSString * order_type = @"";
        
        if (onerow == 0) {
            order_type = @"new";
        }else if (onerow == 1){
            order_type = @"priceup";
        }else{
            order_type = @"pricedown";
        }
        
        self.zongheArrow.hidden = YES;
        
        if ([self.delegate respondsToSelector:@selector(doneOneClick:)]) {
            [self.delegate doneOneClick:order_type];
        }
    }
    
    [self cancelButtonOneClick];
}

#pragma mark - > 综合筛选取消
- (void)cancelButtonOneClick{
    self.zongheArrow.image = [UIImage imageNamed:@"icon_zhankai_1"];
    [self endEditing:YES];
}

#pragma mark - > 地区筛选选择
- (void)doneButtonTwoClick{
    
    NSInteger onerow=[self.areaPickerView selectedRowInComponent:0];
    NSInteger tworow=[self.areaPickerView selectedRowInComponent:1];
    NSInteger threerow=[self.areaPickerView selectedRowInComponent:2];

    NSString * provice = @"";
    NSString * city = @"";
    NSString * country = @"";

    if (self.provinceArray.count > onerow) {
        provice = ((FWRegionListModel *)self.provinceArray[onerow]).region_name;
        
        if (self.cityArray.count > tworow) {
            city = ((FWRegionListModel *)self.cityArray[tworow]).region_name;
            
            if (self.countryArray.count > threerow) {
                country = ((FWRegionListModel *)self.countryArray[threerow]).region_name;
                
                self.areaLabel.text = [NSString stringWithFormat:@"%@%@%@",provice,city,country];
                self.areaArrow.hidden = YES;

                if ([self.delegate respondsToSelector:@selector(doneTwoProvinceCode:CityCode:CountyCode:)]) {
                    [self.delegate doneTwoProvinceCode:provice CityCode:city CountyCode:country];
                }
            }
        }
    }

    [self cancelButtonTwoClick];
}

#pragma mark - > 地区筛选取消
- (void)cancelButtonTwoClick{
    
    self.areaArrow.image = [UIImage imageNamed:@"icon_zhankai_1"];
    [self endEditing:YES];
}

#pragma mark - > 返回当前顶部视图的高度
- (CGFloat)getCurrentViewHeight{
    
    CGFloat height = 0;
    height = CGRectGetMaxY(self.chooseView.frame);
    return height;
}

#pragma mark - > 返回切换标签上部的高度
- (CGFloat)getBarViewTopHeight{
    
    CGFloat height = 0;
    height = CGRectGetMinY(self.zongheTF.frame);
    return height;
}


#pragma mark - > 获取地区
-(void)requestAreaData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_province_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            FWRegionModel * areaModel = [FWRegionModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            if (self.provinceArray.count > 0) {
                [self.provinceArray removeAllObjects];
            }

            if (self.cityArray.count > 0) {
                [self.cityArray removeAllObjects];
            }

            [self.provinceArray addObjectsFromArray:areaModel.list];
            [self.cityArray addObjectsFromArray:areaModel.list_city];

            [self.areaPickerView reloadComponent:0];
            [self.areaPickerView reloadComponent:1];
            
            if (self.cityArray.count > 0) {
                self.cityName = ((FWRegionListModel *)self.cityArray[0]).region_name;
                [self requestThirdComponent];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

#pragma mark - > 请求市区
- (void)requestCityComponent{
    
    if (((FWRegionListModel *)self.provinceArray[0]).region_name <= 0) {
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];

    NSDictionary * params = @{
        @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
        @"p_name":self.proviceName,
    };
    
    [request startWithParameters:params WithAction:Get_city_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            FWRegionModel * areaModel = [FWRegionModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            if (self.cityArray.count > 0) {
                [self.cityArray removeAllObjects];
            }
            
            [self.cityArray addObjectsFromArray:areaModel.list];
            [self.areaPickerView reloadComponent:1];
            
            if (self.cityArray.count > 0) {
                self.cityName = ((FWRegionListModel *)self.cityArray[0]).region_name;
                [self requestThirdComponent];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

#pragma mark - > 每次省滚动后，自动取出市的第一行id，去请求第三列
- (void)requestThirdComponent{
    
    if (((FWRegionListModel *)self.cityArray[0]).region_name <= 0) {
        return;
    }
    
    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"p_name":self.cityName,
                              };
    request.isNeedShowHud = YES;
    NSLog(@"params----%@",params);
    [request startWithParameters:params WithAction:Get_county_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWRegionModel * areaModel = [FWRegionModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (self.countryArray.count > 0) {
                [self.countryArray removeAllObjects];
            }
            
            [self.countryArray addObjectsFromArray:areaModel.list];
            [self.areaPickerView reloadComponent:2];
        }
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.zongheTF) {
        self.zongheArrow.image = [UIImage imageNamed:@"icon_zhankai_2"];
    }else{
        self.areaArrow.image = [UIImage imageNamed:@"icon_zhankai_2"];
    }
    return YES;
}
@end
