//
//  FWHomeCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeCell.h"
#import "FWFeedModel.h"
#import "FWTagsViewController.h"
#import "FWLikeRequest.h"
#import "FWViewPictureViewController.h"
#import "FWReportViewController.h"

@implementation FWHomeCell
@synthesize mainView;
@synthesize photoImageView;
@synthesize nameLabel;
@synthesize authenticationView;
@synthesize authenticationLabel;
@synthesize vipImageButton;
@synthesize rightButton;
@synthesize attentionButton;
@synthesize contentLabel;
@synthesize articalIconView;
@synthesize tagsButton;
@synthesize bannerImageView;
@synthesize likesButton;
@synthesize likesImageView;
@synthesize likesLabel;
@synthesize commentButton;
@synthesize commentImageView;
@synthesize commentLabel;
@synthesize shareButton;
@synthesize shareImageView;
@synthesize shareLabel;
@synthesize containerArr;
@synthesize containerHeight;
@synthesize timeLabel;
@synthesize shopButton;
@synthesize videoImageView;
@synthesize durationLabel;
@synthesize anliLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        containerHeight = 1;
        
        [self setupSubviews];
        self.contentView.backgroundColor = FWTextColor_FAFAFA;
        containerArr = @[].mutableCopy;
    }
    
    return self;
}

- (void)setupSubviews{
    
    mainView = [[UIView alloc] init];
    mainView.userInteractionEnabled = YES;
    mainView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    mainView.layer.cornerRadius = 2;
    mainView.layer.masksToBounds = YES;
    mainView.clipsToBounds = YES;
    [self.contentView addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(0);
        make.left.mas_equalTo(self.contentView).mas_offset(0);
        make.right.mas_equalTo(self.contentView).mas_offset(-0);
        make.width.mas_equalTo(SCREEN_WIDTH);
//        make.height.mas_greaterThanOrEqualTo(100);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(0);
    }];
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.layer.cornerRadius = 33/2;
    photoImageView.layer.masksToBounds = YES;
    photoImageView.userInteractionEnabled = YES;
    photoImageView.image = [UIImage imageNamed:@""];
    [mainView addSubview:photoImageView];
    [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(mainView).mas_offset(5);
        make.left.mas_equalTo(mainView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(33, 33));
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapClick)];
    [photoImageView addGestureRecognizer:tap];
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    nameLabel.textColor = FWTextColor_272727;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        make.top.mas_equalTo(self.photoImageView).mas_offset(-1);
        make.height.mas_equalTo(18);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH -77);
    }];
    
    authenticationView = [[UIImageView alloc] init];
    authenticationView.image = [UIImage imageNamed:@"primary_bussiness"];
    [mainView addSubview:authenticationView];
    [authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.right.mas_equalTo(photoImageView).mas_offset(-0);
        make.bottom.mas_equalTo(photoImageView).mas_offset(-0);
    }];
    authenticationView.hidden = YES;
    
    authenticationLabel = [[UILabel alloc] init];
    authenticationLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    authenticationLabel.textAlignment = NSTextAlignmentLeft;
    authenticationLabel.textColor = FWTextColor_BCBCBC;
    [mainView addSubview:authenticationLabel];
    [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(nameLabel.mas_bottom);
        make.left.mas_equalTo(nameLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.vipImageButton = [[UIButton alloc] init];
    [self.vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.nameLabel).mas_offset(-1);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
    
    self.rightButton = [[UIButton alloc] init];
    [self.rightButton setImage:[UIImage imageNamed:@"home_new_more"] forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(rightButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:self.rightButton];
    [self.rightButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(52);
        make.right.mas_equalTo(mainView);
        make.centerY.mas_equalTo(self.photoImageView);
    }];
    
    attentionButton = [[UIButton alloc] init];
    attentionButton.layer.cornerRadius = 2;
    attentionButton.layer.masksToBounds = YES;
    attentionButton.titleLabel.font = DHSystemFontOfSize_14;
    attentionButton.backgroundColor = FWTextColor_222222;
    [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [attentionButton addTarget:self action:@selector(attentionButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:attentionButton];
    [attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(mainView).mas_offset(-20);
        make.centerY.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    attentionButton.hidden = YES;
    
    bannerImageView = [[FWHomeCellBannerView alloc] init];
    bannerImageView.frame = CGRectMake(CGRectGetMaxX(self.photoImageView.frame), 57, SCREEN_WIDTH-(CGRectGetMaxX(self.photoImageView.frame))-10, 188*(SCREEN_WIDTH)/(347));
    bannerImageView.bannerDelegate = self;
    [mainView addSubview:bannerImageView];
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.textColor = FWColor(@"dd3333");
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    self.priceLabel.font = DHBoldFont(18);
    [mainView addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(200, 0.01));
        make.left.mas_equalTo(self.bannerImageView);
        make.top.mas_equalTo(self.bannerImageView.mas_bottom).mas_offset(0);
    }];
    
    self.jingpinView = [[UIImageView alloc] init];
    self.jingpinView.image = [UIImage imageNamed:@"new_jingpin"];
    [bannerImageView addSubview:self.jingpinView];
    [self.jingpinView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(22.5, 26.5));
        make.top.mas_equalTo(bannerImageView);
        make.right.mas_equalTo(self.bannerImageView).mas_offset(-10);
    }];
    self.jingpinView.hidden = YES;
    
    articalIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_artical"]];
    [mainView addSubview:articalIconView];
    [articalIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(bannerImageView).mas_offset(-10);
        make.bottom.mas_equalTo(bannerImageView).mas_offset(-10);
        make.width.mas_equalTo(51);
        make.height.mas_equalTo(23);
    }];
    articalIconView.hidden = YES;
    
   
    durationLabel = [[UILabel alloc] init];
    durationLabel.font = DHSystemFontOfSize_12;
    durationLabel.backgroundColor = FWColorWihtAlpha(@"#020202", 0.4);
    durationLabel.layer.cornerRadius = 2;
    durationLabel.layer.masksToBounds = YES;
    durationLabel.textColor = FWViewBackgroundColor_FFFFFF;
    durationLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:durationLabel];
    [durationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.mas_equalTo(self.bannerImageView).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(50);
        make.height.mas_equalTo(24);
    }];
    durationLabel.hidden = YES;
    
    videoImageView = [[UIImageView alloc] init];
    videoImageView.image = [UIImage imageNamed:@"icon_video"];
    [mainView addSubview:videoImageView];
    [videoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.centerX.mas_equalTo(bannerImageView);
        make.width.mas_equalTo(21);
        make.height.mas_equalTo(32);
    }];
    videoImageView.hidden = YES;
    
    anliLabel = [[UILabel alloc] init];
    anliLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    anliLabel.textColor = FWViewBackgroundColor_FFFFFF;
    anliLabel.backgroundColor = FWTextColor_000000;
    anliLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:anliLabel];
    [anliLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bannerImageView).mas_offset(8);
        make.top.mas_equalTo(self.bannerImageView).mas_offset(8);
        make.width.mas_equalTo(43);
        make.height.mas_equalTo(20);
    }];
    anliLabel.hidden = YES;
    
    contentLabel = [[UILabel alloc] init];
    contentLabel.numberOfLines = 3;
    contentLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    contentLabel.textColor = FWTextColor_272727;
    contentLabel.preferredMaxLayoutWidth = [UIScreen mainScreen].bounds.size.width-40;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [mainView addSubview:contentLabel];
    [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bannerImageView);
        make.right.mas_equalTo(self.bannerImageView).mas_offset(0);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(0);
        make.width.mas_equalTo(SCREEN_WIDTH-24);
        make.height.mas_greaterThanOrEqualTo(1);
    }];
    
    tagsButton = [[FWModuleButton alloc] init];
    tagsButton.layer.cornerRadius = 2;
    tagsButton.layer.masksToBounds = YES;
    tagsButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
    tagsButton.nameLabel.textColor = FWColor(@"515151");
    [tagsButton addTarget:self action:@selector(btnTagsClick:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:tagsButton];
    [tagsButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0.01);
        make.left.mas_equalTo(contentLabel);
        make.top.mas_equalTo(contentLabel.mas_bottom);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    
    shopButton = [[FWModuleButton alloc] init];
    shopButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
    shopButton.layer.cornerRadius = 2;
    shopButton.layer.masksToBounds = YES;
    shopButton.nameLabel.textColor = FWColor(@"515151");
    [shopButton addTarget:self action:@selector(shopButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:shopButton];
    [shopButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(self.tagsButton);
        make.top.mas_equalTo(self.tagsButton.mas_bottom).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        shareButton.enabled = YES;
    }else{
        shareButton.enabled = NO;
    }
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.text = @"";
    timeLabel.font = DHSystemFontOfSize_12;
    timeLabel.textColor = FWTextColor_A7ADB4;
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentLabel);
        make.height.mas_equalTo(15);
        make.width.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(shopButton.mas_bottom).mas_offset(10);
        make.bottom.mas_equalTo(mainView).mas_offset(-16);
    }];
    
    shareButton = [[UIButton alloc] init];
    [shareButton setImage:[UIImage imageNamed:@"home_share"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:shareButton];
    [shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(mainView).mas_offset(0);
        make.height.mas_equalTo(40);
        make.centerY.mas_equalTo(self.timeLabel);
        make.width.mas_equalTo(40);
    }];

    likesButton = [[UIButton alloc] init];
    [likesButton addTarget:self action:@selector(likesButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:likesButton];
    [likesButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(shareButton.mas_left).mas_offset(0);
        make.height.mas_equalTo(shareButton);
        make.width.mas_equalTo(65);
        make.top.mas_equalTo(shareButton);
    }];
    
    likesLabel = [[UILabel alloc] init];
    likesLabel.text = @"";
    likesLabel.font = DHSystemFontOfSize_12;
    likesLabel.textColor = FWTextColor_A7ADB4;
    likesLabel.textAlignment = NSTextAlignmentLeft;
    [likesButton addSubview:likesLabel];
    [likesLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(likesButton).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(likesButton);
    }];
    
    likesImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_unlike"]];
    [likesButton addSubview:likesImageView];
    [likesImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(likesButton);
        make.size.mas_equalTo(CGSizeMake(19, 16));
        make.right.mas_equalTo(likesLabel.mas_left).mas_offset(-5);
    }];
    
    commentButton = [[UIButton alloc] init];
    [commentButton addTarget:self action:@selector(commentButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:commentButton];
    [commentButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(likesButton.mas_left).mas_offset(0);
        make.height.mas_equalTo(likesButton);
        make.width.mas_equalTo(65);
        make.top.mas_equalTo(likesButton);
    }];
    
    commentLabel = [[UILabel alloc] init];
    commentLabel.text = @"";
    commentLabel.font = DHSystemFontOfSize_12;
    commentLabel.textColor = FWTextColor_A7ADB4;
    commentLabel.textAlignment = NSTextAlignmentLeft;
    [commentButton addSubview:commentLabel];
    [commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(commentButton).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(commentButton);
    }];
    
    commentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pinglun"]];
    [commentButton addSubview:commentImageView];
    [commentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(18, 16));
        make.centerY.mas_equalTo(commentButton);
        make.right.mas_equalTo(commentLabel.mas_left).mas_offset(-5);
    }];
}

#pragma mark - > 配置cell
- (void)setListModel:(FWFeedListModel *)listModel{
    _listModel = listModel;
    
    if ([self.listModel isKindOfClass:[NSArray class]]) {
        return;
    }
    containerArr = self.listModel.tags;

    likesLabel.text = self.listModel.count_realtime.like_count_format;
    shareLabel.text = self.listModel.count_realtime.share_count;
    commentLabel.text = self.listModel.count_realtime.comment_count;
    
    if ([self.listModel.feed_type isEqualToString:@"6"]) {
        self.priceLabel.text = self.listModel.price_sell_show;
    }
    
    contentLabel.text = self.listModel.feed_title;
//    timeLabel.text = self.listModel.format_create_time;
    timeLabel.text = [NSString stringWithFormat:@"%@·%@次浏览",self.listModel.format_create_time,self.listModel.count_realtime.pv_count_format];
    authenticationLabel.text = self.listModel.user_info.title;
    nameLabel.text = self.listModel.user_info.nickname;

    if (self.listModel.user_info.title.length > 0) {
        authenticationLabel.text = self.listModel.user_info.title;
    }else if (self.listModel.user_info.car_style.length > 0) {
        authenticationLabel.text = self.listModel.user_info.car_style;
    }else{
        authenticationLabel.text = @"";
    }
    
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        if (self.listModel.user_info.title.length > 0 ||
            self.listModel.user_info.car_style.length > 0 ) {
            make.top.mas_equalTo(self.photoImageView).mas_offset(-1);
        }else{
            make.centerY.mas_equalTo(self.photoImageView);
        }
        make.height.mas_equalTo(18);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-77);
    }];
    
    
    durationLabel.hidden = YES;
    anliLabel.hidden = YES;
    
    if ([self.listModel.feed_type isEqualToString:@"1"]) {
        videoImageView.hidden = NO;
        articalIconView.hidden = YES;
        
        bannerImageView.userInteractionEnabled = NO;
        
        if (self.listModel.video_info.duration) {
            durationLabel.hidden = NO;
            durationLabel.text = self.listModel.video_info.duration;
            if ([durationLabel.text isEqualToString:@"00:00"]) {
                articalIconView.hidden = YES;
            }
        }
    }else  if ([self.listModel.feed_type isEqualToString:@"2"]) {
        videoImageView.hidden = YES;
        articalIconView.hidden = YES;
        bannerImageView.userInteractionEnabled = YES;
    }else  if ([self.listModel.feed_type isEqualToString:@"3"]) {
        bannerImageView.userInteractionEnabled = NO;

        videoImageView.hidden = YES;
        articalIconView.hidden = NO;
    }else{
        bannerImageView.userInteractionEnabled = NO;
        videoImageView.hidden = YES;
        articalIconView.hidden = YES;
        
        if ([self.listModel.feed_type isEqualToString:@"5"]) {
            anliLabel.hidden = NO;
            anliLabel.text = @"案例";
        }else if ([self.listModel.feed_type isEqualToString:@"6"]){
            anliLabel.hidden = NO;
            anliLabel.text = @"闲置";
        }
    }
    
    // vip图标
    self.vipImageButton.enabled = YES;
    if ([self.listModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([self.listModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([self.listModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    // 右侧的图标
//    if ([self.listModel.user_info.show_effect isEqualToString:@"1"]) {
//        self.rightImageView.hidden = NO;
//        self.rightImageView.image = [UIImage imageNamed:@"home_yingxiangli"];
//    }
    
    if ([self.listModel.feed_type isEqualToString:@"4"]) {
        commentButton.enabled = NO;
        commentImageView.image = [UIImage imageNamed:@"ask_wenda"];
        [commentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.centerY.mas_equalTo(commentButton);
            make.right.mas_equalTo(commentLabel.mas_left).mas_offset(-5);
        }];
    }else{
        commentButton.enabled = YES;
        commentImageView.image = [UIImage imageNamed:@"pinglun"];
        [commentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 16));
            make.centerY.mas_equalTo(commentButton);
            make.right.mas_equalTo(commentLabel.mas_left).mas_offset(-5);
        }];
    }
    
    /* 精品 */
    self.jingpinView.hidden = YES;
    if ([self.listModel.is_good isEqualToString:@"1"]) {
        self.jingpinView.hidden = NO;
    }

    // 头像
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];
    
    
    // 认证身份
    if([self.listModel.user_info.cert_status isEqualToString:@"2"]) {
        authenticationView.hidden = NO;
        authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        if ([self.listModel.user_info.merchant_cert_status isEqualToString:@"3"] &&
            ![self.listModel.user_info.cert_status isEqualToString:@"2"]){
            authenticationView.hidden = YES;
        }else{
            authenticationView.hidden = YES;
        }
    }
    
    self.nameLabel.textColor = FWTextColor_272727;
    /*********单独为某人飘红开始************/
    if ([self.listModel.user_info.uid isEqualToString:@"1001385"]) {
        nameLabel.textColor = FWGradual_Red_E67436;
    }
    /*********单独为某人飘红结束***********/
    
    // 是否收藏
    if ([self.listModel.is_favourited isEqualToString:@"1"]) {
        self.secondString = @"取消收藏";
    }else{
        self.secondString = @"收藏";
    }
    
    // 是否点赞
    if ([self.listModel.is_liked isEqualToString:@"2"]) {
        self.likeType = @"add";
        self.likesImageView.image = [UIImage imageNamed:@"home_unlike"];
    }else if ([self.listModel.is_liked isEqualToString:@"1"]){
        self.likeType = @"cancel";
        self.likesImageView.image = [UIImage imageNamed:@"home_like"];
    }
    
//    if ([self.listModel.feed_cover_width floatValue] == 0) {
//        self.listModel.feed_cover_width = @(SCREEN_WIDTH-28).stringValue;
//    }
//    if ([self.listModel.feed_cover_height floatValue] == 0) {
//        self.listModel.feed_cover_height = @(188*(SCREEN_WIDTH-28)/(347)).stringValue;
//    }
    
    NSMutableArray * tempImageArr = @[].mutableCopy;
    // 图片
    if ([self.listModel.feed_type isEqualToString:@"2"]) {

        for (FWFeedImgsModel * imgModel in self.listModel.imgs) {
            [tempImageArr addObject:imgModel.img_url_nowatermark];
        }
    }else{

        if (self.listModel.feed_cover_nowatermark.length > 0) {
            [tempImageArr addObject:self.listModel.feed_cover_nowatermark];
        }
    }

    if (tempImageArr.count > 0) {
        [bannerImageView configForBanner:tempImageArr];
    }
    
    if ([self.listModel.feed_type isEqualToString:@"2"]) {

        CGFloat picHight = [bannerImageView getCurrentHeight];
        bannerImageView.frame = CGRectMake(45, 47, SCREEN_WIDTH-45-20,picHight);
    }else{
        if ([self.listModel.feed_type isEqualToString:@"4"]){
            if (self.listModel.feed_cover.length <= 0 ||
                self.listModel.imgs.count <= 0 ||
                self.listModel.feed_cover_nowatermark.length <= 0) {
                bannerImageView.frame = CGRectMake(45, 47, SCREEN_WIDTH-45-20, 0.01);
            }else{
                bannerImageView.frame = CGRectMake(45, 47, SCREEN_WIDTH-45-20, (SCREEN_WIDTH-45-20)/2);
            }
        }else{
            // 188*(SCREEN_WIDTH-28)/(347)
            bannerImageView.frame = CGRectMake(45, 47, SCREEN_WIDTH-45-20, (SCREEN_WIDTH-45-20)/2);
        }
    }
    
 
    
    if ([self.listModel.feed_type isEqualToString:@"6"]) {
        [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(200, 24));
            make.left.mas_equalTo(self.bannerImageView).mas_offset(0);
            make.top.mas_equalTo(self.bannerImageView.mas_bottom).mas_offset(5);
        }];
    }else{
        [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(200, 0.01));
            make.left.mas_equalTo(self.bannerImageView).mas_offset(0);
            make.top.mas_equalTo(self.bannerImageView.mas_bottom).mas_offset(0);
        }];
    }

    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:contentLabel.attributedText];
    CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 52].height;
    
    self.contentLabel.numberOfLines = 3;
    CGFloat contentTop = 10;
   
    if (self.contentLabel.text.length <=0 ) {
        textHeight = 1;
        contentTop = 0;
    }else{
        contentTop = 10;
        if (textHeight<=20 && textHeight>14) {
            textHeight = ceilf(contentLabel.font.lineHeight);
        }else if (textHeight > 20 &&textHeight<=40) {
            textHeight = 2*ceilf(contentLabel.font.lineHeight);
        }else if (textHeight > 40){
            textHeight = 3*ceilf(contentLabel.font.lineHeight);
        }
    }
    [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bannerImageView).mas_offset(0);
        make.right.mas_equalTo(self.bannerImageView).mas_offset(0);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(contentTop);
        make.width.mas_equalTo(SCREEN_WIDTH-52);
        make.height.mas_equalTo(textHeight);
    }];
    
    CGFloat tagsButtonHeight = 24;
    CGFloat tagsButtonTopMargin = 0;

    if (containerArr.count >0) {
        
        FWSearchTagsListModel * tagsModel = containerArr[0];
        
        [tagsButton setModuleTitle:tagsModel.tag_name];
        [tagsButton setModuleImage:@"home_tag"];
        
        tagsButtonHeight = 24;
        tagsButtonTopMargin = 10;
    }else{
        tagsButtonHeight = 0.01;
        tagsButtonTopMargin = 0.01;
    }
    
    [tagsButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(tagsButtonHeight);
        make.left.mas_equalTo(contentLabel);
        make.top.mas_equalTo(contentLabel.mas_bottom).mas_offset(tagsButtonTopMargin);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    
    CGFloat shopButtonHeight = 24;
    CGFloat shopButtonTopMargin = 0;

    if (self.listModel.goods_id.length > 0 && ![self.listModel.goods_id isEqualToString:@"0"]) {
    /* 有商品链接 */
    [shopButton setModuleTitle:listModel.goods_info.title_short];
    
        shopButtonHeight = 24;
        shopButtonTopMargin = 10;
    }else{
        shopButtonHeight = 0.01;
        shopButtonTopMargin = 0.01;
    }
    
    [shopButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(shopButtonHeight);
        make.left.mas_equalTo(self.tagsButton);
        make.top.mas_equalTo(self.tagsButton.mas_bottom).mas_offset(shopButtonTopMargin);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentLabel);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(150);
        make.top.mas_equalTo(shopButton.mas_bottom).mas_offset(10);
        make.bottom.mas_equalTo(mainView).mas_offset(-16);
    }];
    
    [shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mainView.mas_right).mas_offset(0);
        make.height.mas_equalTo(40);
        make.centerY.mas_equalTo(self.timeLabel);
        make.width.mas_equalTo(40);
    }];
}

#pragma mark - > 关注（取消关注）
- (void)attentionButtonOnClick:(UIButton *)sender {
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
    }
}

#pragma mark - > 查看图组
- (void)picImageButtonOnClick:(UIButton *)sender {
    
    NSInteger index = sender.tag -1000;

    if ([self.listModel.feed_type isEqualToString:@"2"]) {
        
        FWViewPictureViewController * PVC = [[FWViewPictureViewController alloc] init];
        PVC.model = self.listModel;
        PVC.currentIndex = index;
        [self.vc.navigationController pushViewController:PVC animated:YES];
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(picImageClick:)]) {
        [self.delegate picImageClick:index];
    }
}

#pragma mark - > 查看标签
- (void)btnTagsClick:(UIButton *)sender{
    
    FWSearchTagsSubListModel * tagsModel = self.listModel.tags[0];
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.traceType = 2;
    TVC.tagsModel = tagsModel;
    TVC.tags_id = tagsModel.tag_id;
    [self.vc.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}


#pragma mark - > 喜欢
- (void)likesButtonOnClick:(UIButton *)sender{
    
    [self checkLogin];
    
    FWFeedListModel * model = self.listModel;
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.listModel.feed_id,
                                  @"like_type":self.likeType,
                                  };
        
        [request startWithParameters:params WithAction:Submit_like_feed WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([self.likeType isEqualToString: @"add"]) {
                    model.is_liked = @"1";
                    
                    if ([model.count_realtime.like_count_format integerValue] ||
                        [model.count_realtime.like_count_format integerValue] == 0) {
                        
                        model.count_realtime.like_count_format = @([model.count_realtime.like_count_format integerValue] +1).stringValue;
                    }
                }else if ([self.likeType isEqualToString: @"cancel"]){
                    model.is_liked = @"2";
                    
                    if ([model.count_realtime.like_count_format integerValue] ||
                        [model.count_realtime.like_count_format integerValue] == 0) {
                        
                        model.count_realtime.like_count_format = @([model.count_realtime.like_count_format integerValue] -1).stringValue;
                    }
                }
                
                self.listModel = model;
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}

#pragma mark - > 点击用户头像
- (void)photoImageViewTapClick{
    
    if (nil == self.listModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 评论
- (void)commentButtonOnClick:(UIButton *)sender{
    
    FWCommentViewController * VC = [[FWCommentViewController alloc] init];
    VC.feed_id = self.listModel.feed_id;
    [self.vc.navigationController pushViewController:VC animated:YES];
}

#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{
    
    NSDictionary * params = @{
                              @"aweme":self.listModel,
                              @"type":self.listModel.feed_type,
                              @"hideDownload":@"YES",
                              };
    
    SharePopView *popView = [[SharePopView alloc] initWithParams:params];
    popView.viewcontroller = self.vc;
    popView.aweme = self.listModel;
    popView.myBlock = ^(FWFeedListModel *awemeModel) {};
    [popView show];
}

#pragma mark - > 跳转至商品
- (void)shopButtonOnClick{
    
    FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
    GDVC.goods_id = self.listModel.goods_id;
    [self.vc.navigationController pushViewController:GDVC animated:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

#pragma mark - > 轮播代理
-(void)bannerClick:(NSInteger)index{
    
    if (!self.listModel.feed_type) {
        return;
    }
    if ([self.listModel.feed_type isEqualToString:@"2"]) {
        
        FWViewPictureViewController * PVC = [[FWViewPictureViewController alloc] init];
        PVC.model = self.listModel;
        PVC.currentIndex = index;
        [self.vc.navigationController pushViewController:PVC animated:YES];
    }
}

#pragma mark - > 更多
- (void)rightButtonClick{
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]) {
        
        if ([self.listModel.is_favourited isEqualToString:@"1"]) {
            self.secondString = @"取消收藏";
        }else{
            self.secondString = @"收藏";
        }
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"您可以进行以下操作" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            FWReportViewController * RVC = [[FWReportViewController alloc] init];
            RVC.f_uid = self.listModel.user_info.uid;
            [self.vc.navigationController pushViewController:RVC animated:YES];
        }];
        UIAlertAction *selectAction = [UIAlertAction actionWithTitle:self.secondString style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if ([self.listModel.is_favourited isEqualToString:@"1"]) {
                [self requestFavourate:@"cancel"];
            }else{
                [self requestFavourate:@"add"];
            }
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alertController addAction:takePhotoAction];
        [alertController addAction:selectAction];
        [alertController addAction:cancelAction];
        
        [self.vc presentViewController:alertController animated:YES completion:nil];
    }else{
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self checkLogin];
        }];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 屏蔽二次确认
- (void)secondMakeSure{

}

#pragma mark - > 屏蔽
- (void)requestFavourate:(NSString *)favourate{
    
    if (!self.listModel.feed_id) {
        return;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.listModel.feed_id,
                              @"favourite_type":favourate,
                              };
    
    [request startWithParameters:params WithAction:Submit_favourite_feed WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([favourate isEqualToString:@"add"]) {
                self.listModel.is_favourited = @"1";
                [[FWHudManager sharedManager] showSuccessMessage:@"收藏成功" toController:self.vc];
            }else if ([favourate isEqualToString:@"cancel"]){
                self.listModel.is_favourited = @"2";
                [[FWHudManager sharedManager] showSuccessMessage:@"取消收藏成功" toController:self.vc];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

@end
