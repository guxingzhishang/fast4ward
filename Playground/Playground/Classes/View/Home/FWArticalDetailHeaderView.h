//
//  FWArticalDetailHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/7.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWArticalDetailHeaderView : UIView

@property (nonatomic, strong) UIImageView * topHeaderView;
@property (nonatomic, strong) UIButton * backButton;

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * scanLabel;

@property (nonatomic, strong) UIView * lineOneView;
@property (nonatomic, strong) UIImageView * photoView;
@property (nonatomic, strong) UILabel * nameLabel;

@property (nonatomic, strong) UIButton * vipImageButton;
@property (nonatomic, strong) UIButton * attentionButton;

/* 图文混排的视图 */
@property (nonatomic, strong) UIView * fixView;
@property (nonatomic, strong) UIView * lineTwoView;

/* 标签视图 */
@property (nonatomic, strong) UILabel * tagTitleLabel;
@property (nonatomic, strong) UIView * tagsView;
@property (nonatomic, strong) UIView * lineThreeView;

@property (nonatomic, strong) UILabel * commentLabel;
@property (nonatomic, strong) UIButton * numberButton;

/* 商品背景图 */
@property (nonatomic, strong) UIView * goodsView;
/* 推荐商品 */
@property (nonatomic, strong) UILabel * recammondLabel;
/* 商品展示图 */
@property (nonatomic, strong) UIImageView * goodsImageView;
/* 商品名称 */
@property (nonatomic, strong) UILabel * goodsTitleLabel;
/* 商品价格 */
@property (nonatomic, strong) UILabel * goodsPriceLabel;
/* 商品浏览图片 */
@property (nonatomic, strong) UIImageView * goodsScanView;
/* 商品浏览数量 */
@property (nonatomic, strong) UILabel * goodsScanLabel;

@property (nonatomic, strong) UIView * goodsLineView;

/* 当前视图的高度 */
@property (nonatomic, assign) CGFloat  viewHeight;
/* 标签视图的高度 */
@property (nonatomic, assign) CGFloat  tagsViewHeight;
/* 图文混排视图的高度 */
@property (nonatomic, assign) CGFloat  fixViewHeight;

@property (nonatomic, weak) UIViewController * vc;


- (void)dealWithData:(id)model;

@end

NS_ASSUME_NONNULL_END
