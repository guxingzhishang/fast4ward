//
//  FWSegementScrollView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/27.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWSegementScrollViewDelegate <NSObject>

/**
 * 点击了第几个item
 */
- (void)segementItemClickTap:(NSInteger)index;

@end

@interface FWSegementScrollView : UIScrollView<UIScrollViewDelegate>

/**
 顶部菜单数据
 */
@property (nonatomic,strong)NSArray * titles;

/**
 一个页面显示几个按钮
 */
@property (nonatomic,assign)NSInteger pageNumberOfItem;

@property (nonatomic,strong)NSMutableArray *labelArys;
@property (nonatomic,strong)NSMutableArray *rectXArray;

@property (nonatomic,assign)NSInteger lastIndex;

@property (nonatomic,assign)NSInteger currentItem;
@property (nonatomic,assign)CGFloat menuHeight;

@property (nonatomic,strong)UIFont *titleFont;
@property (nonatomic,strong)UIFont *titleSelectFont;

@property (nonatomic,strong)UIColor *titleColor;
@property (nonatomic,strong)UIColor *titleSelectColor;

@property (nonatomic, weak) id<FWSegementScrollViewDelegate> itemDelegate;

/* 是否是首页的segement 1：是  2：不是 */
@property (nonatomic, assign) NSInteger  fromHome;

- (void)deliverTitles:(NSArray *)array;
- (void)clearSubViews;
@end
