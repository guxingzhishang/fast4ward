//
//  FWHomeSegementScrollView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeSegementScrollView.h"

@implementation FWHomeSegementScrollView
@synthesize shadowView;

- (CGFloat)menuHeight{
    
    if (_menuHeight == 0) {
        _menuHeight = 40;
    }
    return _menuHeight;
}

- (NSInteger)pageNumberOfItem{
    
    if (_pageNumberOfItem == 0) {
        _pageNumberOfItem = (self.titles.count > 5 ? 5 : self.titles.count);
    }
    return _pageNumberOfItem;
}

- (NSMutableArray *)labelArys{
    
    if (!_labelArys) {
        _labelArys = [NSMutableArray array];
    }
    return _labelArys;
}

- (NSMutableArray *)rectXArray{
    
    if (!_rectXArray) {
        _rectXArray = [NSMutableArray array];
    }
    return _rectXArray;
}

- (id)init{
    
    self = [super init];
    if (self) {
        self.titles = @[].copy;
        
        self.showsHorizontalScrollIndicator = NO;
        self.delegate = self;
        
        self.titleFont = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        self.titleColor = FWTextColor_A7ADB4;

        self.titleSelectFont = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        self.titleSelectColor = FWTextColor_626D8F;

        self.backgroundColor = FWTextColor_F8F8F8;
        
        self.lastIndex = 0;
    }
    return self;
}

#pragma mark - 加载顶部视图数据 -
- (void)deliverTitles:(NSArray *)array{
    //标记label的最大X坐标
    CGFloat preX = 0;
    self.titles = array;
    
    if (self.labelArys.count>0) {
        [self.labelArys removeAllObjects];
    }

    for (int i = 0; i < self.titles.count; i++) {
        
        FWHomeNoteCategrayModel * listModel = self.titles[i];
        
        NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc] initWithString:listModel.cat_name];
        CGSize  textLimitSize = CGSizeMake(MAXFLOAT, self.menuHeight);
        CGFloat textW = [YYTextLayout layoutWithContainerSize:textLimitSize text:attribute].textBoundingSize.width;
        
        UILabel *item = [[UILabel alloc] init];
        item.frame = CGRectMake(preX+20,0, textW+20, self.menuHeight);
        item.text = listModel.cat_name;
        item.font = self.titleFont;
        item.textColor = self.titleColor;
        item.backgroundColor = FWClearColor;
        item.textAlignment = NSTextAlignmentCenter;
        item.tag = 10000 + i;
        [self addSubview:item];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClickTap:)];
        item.userInteractionEnabled = YES;
        [item addGestureRecognizer:tap];
        [self.labelArys addObject:item];
        
        preX = CGRectGetMaxX(item.frame);
        
        [self.rectXArray addObject:@(preX)];
    }
    self.contentSize = CGSizeMake(preX+20, self.menuHeight);
    
//    UILabel * itemLabel = (UILabel *)[self viewWithTag:10000];
    
//    shadowView= [[UIView alloc]init];
//    shadowView.frame = CGRectMake(CGRectGetMinX(itemLabel.frame)+(CGRectGetWidth(itemLabel.frame)-32)/2, CGRectGetMaxY(itemLabel.frame),32, 4);
//    shadowView.backgroundColor = FWTextColor_68769F;
//    shadowView.layer.cornerRadius = 2;
//    shadowView.layer.masksToBounds = YES;
//    shadowView.tag = 100000;
//    [self addSubview:shadowView];

    
    [self menuUpdateStyle:0];
}

#pragma mark - 菜单点击回调 -
- (void)itemClickTap:(UIGestureRecognizer *)sender{
    NSInteger index = sender.view.tag - 10000;
    
    if (self.lastIndex != index) {
        [self menuUpdateStyle:index];
        [self menuScrollToCenter:index];
        
        self.lastIndex = index;
        
        if ([self.itemDelegate respondsToSelector:@selector(segementItemClickTap:)]) {
            [self.itemDelegate segementItemClickTap:index];
        }
    }
}


#pragma mark - > 顶部菜单滚动
- (void)menuScrollToCenter:(NSInteger)index{
    
//    UILabel * itemLabel = (UILabel *)[self viewWithTag:10000+index];
//    CGFloat lastX = [[self.rectXArray lastObject] floatValue];
//    CGFloat left = itemLabel.center.x - SCREEN_WIDTH / 2.0;
//    left = left <= 0 ? 0 : left;
//    CGFloat maxLeft = lastX - SCREEN_WIDTH;
//    left = left >= maxLeft ? maxLeft : left;
//    [self setContentOffset:CGPointMake(left, 0) animated:YES];
//    shadowView.frame = CGRectMake(CGRectGetMinX(itemLabel.frame)+(CGRectGetWidth(itemLabel.frame)-32)/2, CGRectGetMaxY(itemLabel.frame),32, 5);
}

#pragma mark - > 顶部菜单切换样式
- (void)menuUpdateStyle:(NSInteger)index{
    
    if (self.labelArys.count <= 0) {
        return;
    }
    UILabel *lastLabel = self.labelArys[self.lastIndex];
    lastLabel.font = self.titleFont;
    lastLabel.textColor = self.titleColor;
    
    UILabel *label = self.labelArys[index];
    label.textColor = self.titleSelectColor;
    label.font = self.titleSelectFont;
}

- (void)clearSubViews{
    
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
}

@end
