//
//  FWViewPictureView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/16.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/*
 
 */
#import <UIKit/UIKit.h>

@protocol FWViewPictureViewDelegate <NSObject>

- (void)backButtonClick;
- (void)shareButtonClick;
- (void)photoButtonClick;
- (void)attentionButtonClick;
- (void)saveButtonClick;

@end

@interface FWViewPictureView : UIView<UIScrollViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * numLabel;

@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) UIButton * attentionButton;

@property (strong, nonatomic) UIImageView *siIcon;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIScrollView * imageScrollView;

@property(nonatomic,assign) NSInteger imageCount;//总共多少页
@property(nonatomic,assign) CGFloat endContentOffsetX;//拖动结束位置
@property(nonatomic,assign) int currentPagenum;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) id<FWViewPictureViewDelegate>delegate;

@property (nonatomic, strong) FWFeedListModel * listModel;

- (void)configViewWithModel:(id)model;

@end
