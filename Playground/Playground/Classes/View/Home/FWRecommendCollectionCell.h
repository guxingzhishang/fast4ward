//
//  FWRecommendCollectionCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/2.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWCollecitonBaseCell.h"

@interface FWRecommendCollectionCell : FWCollecitonBaseCell

@property (nonatomic, strong) UIImageView * liveImageView;

- (void)configWithFinalModel:(id)bannerModel WithModel:(id)dataModel isShow:(BOOL)isShow;

@end
