//
//  FWCommentCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWCommentCell.h"
#import "FWLikeRequest.h"

@interface FWCommentCell ()


@property (nonatomic, assign) CGFloat cellHeight;

@end

@implementation FWCommentCell

@synthesize likesImageView;
@synthesize likesLabel;
@synthesize likesButton;
@synthesize identityID;

- (void)awakeFromNib
{
    [super awakeFromNib];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView  WithID:(NSString *)cellID
{
    FWCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {

        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self.identityID = reuseIdentifier;

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        // 初始化
        [self _setup];

        // 创建自控制器
        [self _setupSubViews];
        
        // 布局子控件
        [self _makeSubViewsConstraints];
        
    }
    
    return self;
}

#pragma mark - 公共方法
- (void)setCommentFrame:(FWCommentFrame *)commentFrame
{
    _commentFrame = commentFrame;
    
    FWReplyComment *comment = commentFrame.comment;
  
    // 头像
    self.photoImageView.layer.cornerRadius = self.cellHeight/2;
    self.photoImageView.layer.masksToBounds = YES;
//    self.photoImageView.frame = commentFrame.avatarFrame;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:comment.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.contentView).mas_offset(12);
        make.size.mas_equalTo(CGSizeMake(self.cellHeight, self.cellHeight));
    }];
    
    //昵称
//    self.nameLabel.frame = commentFrame.nicknameFrame;
    self.nameLabel.text = comment.user_info.nickname;
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-45);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.photoImageView);
    }];
    
    // 点赞
    self.likesButton.frame = commentFrame.thumbFrame;
    
    // 点赞图标
    self.likesImageView.frame = commentFrame.thumbImageFrame;
    if ([comment.is_liked isEqualToString:@"1"]) {
        self.likesImageView.image = [UIImage imageNamed:@"car_detail_like"];
    }else{
        self.likesImageView.image = [UIImage imageNamed:@"unlike"];
    }
    
    
    // 点赞数
    self.likesLabel.frame = commentFrame.thumbNumberFrame;
    self.likesLabel.text = comment.like_count_format;
    
    // 时间
//    self.signLabel.frame = commentFrame.createTimeFrame;
    self.signLabel.text = comment.format_create_time;
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(200);
        make.height.mas_equalTo(15);
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
    }];
  
    if (commentFrame.thumbFrame.size.width == 0) {
        self.likesButton.hidden = YES;
    }else{
        self.likesButton.hidden = NO;
    }
    
    
    // 赋值
//    self.contentLabel.frame = commentFrame.textFrame;
    self.contentLabel.attributedText = comment.attributedText;
    [self.contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.width.mas_equalTo(commentFrame.textFrame.size.width-20);
        make.height.mas_equalTo(commentFrame.textFrame.size.height);
        make.top.mas_equalTo(commentFrame.textFrame.origin.y);
    }];
    
}


#pragma mark - 私有方法
#pragma mark - 初始化
- (void)_setup
{
    // 设置颜色
    self.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
}

#pragma mark - 创建自控制器
- (void)_setupSubViews
{
    self.photoImageView.layer.cornerRadius = self.cellHeight/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.contentView).mas_offset(12);
        make.size.mas_equalTo(CGSizeMake(self.cellHeight, self.cellHeight));
    }];
    self.photoImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapClick)];
    [self.photoImageView addGestureRecognizer:tap];
    
    self.nameLabel.font = DHSystemFontOfSize_14;
    self.signLabel.font = DHSystemFontOfSize_12;
    self.signLabel.textColor = FWTextColor_797980;
    
    
    likesButton = [[UIButton alloc] init];
    [likesButton addTarget:self action:@selector(likesButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:likesButton];
    
    likesImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"unlike"]];
    [likesButton addSubview:likesImageView];
    
    likesLabel = [[UILabel alloc] init];
    likesLabel.userInteractionEnabled = NO;
    likesLabel.font = DHSystemFontOfSize_12;
    likesLabel.textColor = DHTitleColor_666666;
    likesLabel.textAlignment = NSTextAlignmentCenter;
    [likesButton addSubview:likesLabel];

    // 文本
    YYLabel *contentLabel = [[YYLabel alloc] init];
    contentLabel.numberOfLines = 0 ;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:contentLabel];
    self.contentLabel = contentLabel;
    
    __weak typeof(self) weakSelf = self;
    contentLabel.highlightTapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
        
        // 利用KVC获取UserInfo 其实可以在MHComment模型里面利用 通知告知控制器哪个用户被点击了
        YYTextHighlight *highlight = [containerView valueForKeyPath:@"_highlight"];
        
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(commentCell:didClickedUser:)]) {
            [weakSelf.delegate commentCell:weakSelf didClickedUser:highlight.userInfo[FWCommentUserKey]];
        }
    };
    
    self.lineView.hidden = YES;
    
    UITapGestureRecognizer * nameGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapClick)];
    self.nameLabel.userInteractionEnabled = YES;
    [self.nameLabel addGestureRecognizer:nameGesture];
}


#pragma mark - 布局子控件
- (void)_makeSubViewsConstraints{
}

#pragma mark - override
- (void)setFrame:(CGRect)frame
{
    if ([self.identityID isEqualToString:@"CommentDetailID"]) {
        self.cellHeight = 36;
        self.nameLabel.font = DHSystemFontOfSize_14;
        self.signLabel.font = DHSystemFontOfSize_12;
        frame.size.width = SCREEN_WIDTH;
    }else{
        self.cellHeight = 30;
        self.nameLabel.font = DHSystemFontOfSize_12;
        self.signLabel.font = DHSystemFontOfSize_10;
        frame.origin.x = MHTopicAvatarWH+2*MHTopicHorizontalSpace;
        frame.size.width = SCREEN_WIDTH - frame.origin.x;
    }
    
    [super setFrame:frame];
}

#pragma mark - 布局子控件
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // 布局子控件
}

#pragma mark - > 喜欢该条评论
- (void)likesButtonOnClick:(UIButton *)sender{
    
    [self checkLogin];
    
    FWReplyComment *comment = self.commentFrame.comment;

    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSString * type;
        if ([comment.is_liked isEqualToString:@"2"]) {
            type = @"add";
        }else if ([comment.is_liked isEqualToString:@"1"]){
            type = @"cancel";
        }
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"comment_id":comment.comment_id,
                                  @"like_type":type,
                                  };
        
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        [request startWithParameters:params WithAction:Submit_like_comment  WithDelegate:self.viewcontroller completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([type isEqualToString:@"add"]) {
                    
                    comment.is_liked = @"1";
                    
                    if ([comment.like_count_format integerValue] ||
                        [comment.like_count_format integerValue] == 0) {
                        
                        comment.like_count_format = @([comment.like_count_format integerValue] +1).stringValue;
                    }
                }else if ([type isEqualToString:@"cancel"]){
                    
                    comment.is_liked = @"2";
                    
                    if ([comment.like_count_format integerValue] ||
                        [comment.like_count_format integerValue] == 0) {
                        
                        comment.like_count_format = @([comment.like_count_format integerValue] -1).stringValue;
                    }
                }
                
                self.commentFrame.comment = comment;
                [self setCommentFrame:self.commentFrame];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewcontroller];
            }
        }];
        
        NSInteger index = sender.tag - 98765;
        if ([self.delegate respondsToSelector:@selector(likesButtonClickWithCommentFrame:WithIndex:withcommentCell:)]) {
            [self.delegate likesButtonClickWithCommentFrame:self.commentFrame WithIndex:index withcommentCell:self];
        }
    }
}

#pragma mark - > 点击用户头像
- (void)photoImageViewTapClick{
    
    FWReplyComment *comment = self.commentFrame.comment;
    
    if (nil == comment.uid) {
        return;
    }
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = comment.uid;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}


- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
