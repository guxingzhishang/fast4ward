//
//  FWGraphTextCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWGraphTextCell.h"
#import "FWFeedModel.h"
#import "FWTagsViewController.h"
#import "FWLikeRequest.h"

@implementation FWGraphTextCell
@synthesize photoImageView;
@synthesize nameLabel;
@synthesize vipImageButton;
@synthesize attentionButton;
@synthesize contentLabel;
@synthesize showAllButton;
@synthesize containerView;
@synthesize picImageButton;
@synthesize picImageView;
@synthesize likesButton;
@synthesize likesImageView;
@synthesize likesLabel;
@synthesize commentButton;
@synthesize commentImageView;
@synthesize commentLabel;
@synthesize shareButton;
@synthesize shareImageView;
@synthesize shareLabel;
@synthesize lineView;
@synthesize containerArr;
@synthesize containerHeight;
@synthesize moreImageView;
@synthesize moreLabel;
@synthesize moreView;
@synthesize timeLabel;
@synthesize shopButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        containerHeight = 1;

        self.isShow = NO;
        self.contentView.clipsToBounds = YES;
        
        [self setupSubviews];
        
        containerArr = @[].mutableCopy;
    }
    
    return self;
}

- (void)setupSubviews{
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.layer.cornerRadius = 25;
    photoImageView.layer.masksToBounds = YES;
    photoImageView.userInteractionEnabled = YES;
    photoImageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:photoImageView];
    [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.contentView).mas_offset(12);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapClick)];
    [photoImageView addGestureRecognizer:tap];
    
    nameLabel = [[UILabel alloc] init];
    nameLabel.font = DHSystemFontOfSize_14;
    nameLabel.textColor = FWTextColor_000000;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    self.vipImageButton = [[UIButton alloc] init];
    [self.vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.nameLabel).mas_offset(-1);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
    
    attentionButton = [[UIButton alloc] init];
    attentionButton.layer.cornerRadius = 2;
    attentionButton.layer.masksToBounds = YES;
    attentionButton.titleLabel.font = DHSystemFontOfSize_14;
    attentionButton.backgroundColor = FWTextColor_222222;
    [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [attentionButton addTarget:self action:@selector(attentionButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:attentionButton];
    [attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-20);
        make.centerY.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    attentionButton.hidden = YES;
    
    contentLabel = [[UILabel alloc] init];
    contentLabel.numberOfLines = 3;
    contentLabel.font = DHSystemFontOfSize_14;
    contentLabel.textColor = FWTextColor_000000;
    contentLabel.preferredMaxLayoutWidth = [UIScreen mainScreen].bounds.size.width-40;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.contentView addSubview:contentLabel];
    [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.top.mas_equalTo(self.photoImageView.mas_bottom).mas_offset(14);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
        make.height.mas_greaterThanOrEqualTo(1);
    }];
    
    containerView = [[UIView alloc] init];
    [self.contentView addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentLabel);
        make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(5);
        make.height.mas_greaterThanOrEqualTo(1);
    }];
    
    showAllButton = [[UIButton alloc] init];
    [showAllButton setImage:[UIImage imageNamed:@"show_down"] forState:UIControlStateNormal];
    [showAllButton addTarget:self action:@selector(showAllButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:showAllButton];
    [showAllButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentLabel).mas_offset(10);
        make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(40, 30));
    }];
    
    showAllButton.hidden = YES;
    
    picImageView = [[UIImageView alloc] init];
    picImageView.layer.masksToBounds = YES;
    picImageView.image = [UIImage imageNamed:@"placeholder"];
    picImageView.contentMode = UIViewContentModeScaleAspectFill;
    picImageView.userInteractionEnabled = YES;
    [self.contentView addSubview:picImageView];
    [picImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.containerView.mas_bottom).mas_offset(5);
        make.width.height.mas_equalTo(SCREEN_WIDTH);
    }];
    
    moreView = [[UIView alloc] init];
    moreView.layer.cornerRadius = 2;
    moreView.layer.masksToBounds = YES;
    moreView.backgroundColor = [UIColor colorWithHexString:@"000000" alpha:0.9];
    [picImageView addSubview:moreView];
    [moreView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];
//    moreView.hidden = YES;

    moreImageView = [[UIImageView alloc] init];
    moreImageView.image = [UIImage imageNamed:@"tuji"];
    [moreView addSubview:moreImageView];
    [moreImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(moreView);
        make.left.mas_equalTo(moreView).mas_offset(7);
        make.size.mas_equalTo(CGSizeMake(12, 10));
    }];
    
    moreLabel = [[UILabel alloc] init];
    moreLabel.textColor = FWViewBackgroundColor_FFFFFF;
    moreLabel.textAlignment = NSTextAlignmentLeft;
    moreLabel.font = DHSystemFontOfSize_10;
    [moreView addSubview:moreLabel];
    [moreLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(moreView);
        make.left.mas_equalTo(moreImageView.mas_right).mas_offset(5);
        make.right.mas_equalTo(moreView);
    }];
    
//    self.siIcon = [[UIImageView alloc] init];
//    self.siIcon.image = [UIImage imageNamed:@"placeholder_si"];
//    [self.picImageView addSubview:self.siIcon];
//    [self.siIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(self.picImageView);
//        make.centerY.mas_equalTo(self.picImageView);
//        make.size.mas_equalTo(CGSizeMake(50, 50));
//    }];
//    self.siIcon.hidden = YES;
    
    picImageButton = [[UIButton alloc] init];
    [picImageButton addTarget:self action:@selector(picImageButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [picImageView addSubview:picImageButton];
    [picImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(picImageView);
    }];
    
    shopButton = [[FWModuleButton alloc] init];
    shopButton.backgroundColor = FWColor(@"F7F8F9");
    shopButton.nameLabel.textColor = FWTextColor_616C91;
    [shopButton addTarget:self action:@selector(shopButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:shopButton];
    [shopButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(self.contentView).mas_offset(11);
        make.top.mas_equalTo(self.picImageView.mas_bottom).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(20);
    }];

    shareButton = [[UIButton alloc] init];
    [shareButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:shareButton];
    [shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(0);
        make.height.mas_equalTo(90);
        make.top.mas_equalTo(self.shopButton.mas_bottom).mas_offset(5);
        make.width.mas_equalTo(65);
    }];
    
    shareLabel = [[UILabel alloc] init];
    shareLabel.text = @"80";
    shareLabel.font = DHSystemFontOfSize_12;
    shareLabel.textColor = FWTextColor_969696;
    shareLabel.textAlignment = NSTextAlignmentRight;
    [shareButton addSubview:shareLabel];
    [shareLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(shareButton).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(shareButton);
    }];
    
    shareImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_share"]];
    [shareButton addSubview:shareImageView];
    [shareImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(18, 15));
        make.centerY.mas_equalTo(shareButton);
        make.right.mas_equalTo(shareLabel.mas_left).mas_offset(-5);
    }];
    
    commentButton = [[UIButton alloc] init];
    [commentButton addTarget:self action:@selector(commentButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:commentButton];
    [commentButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(shareButton.mas_left).mas_offset(-5);
        make.height.mas_equalTo(shareButton);
        make.width.mas_equalTo(65);
        make.top.mas_equalTo(shareButton);
    }];

    commentLabel = [[UILabel alloc] init];
    commentLabel.text = @"1240";
    commentLabel.font = DHSystemFontOfSize_12;
    commentLabel.textColor = FWTextColor_969696;
    commentLabel.textAlignment = NSTextAlignmentLeft;
    [commentButton addSubview:commentLabel];
    [commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(commentButton).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(commentButton);
    }];
    
    commentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pinglun"]];
    [commentButton addSubview:commentImageView];
    [commentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(18, 16));
        make.centerY.mas_equalTo(commentButton);
        make.right.mas_equalTo(commentLabel.mas_left).mas_offset(-5);
    }];
    
    
    likesButton = [[UIButton alloc] init];
    [likesButton addTarget:self action:@selector(likesButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:likesButton];
    [likesButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(commentButton.mas_left).mas_offset(-5);
        make.width.mas_equalTo(65);
        make.top.mas_equalTo(commentButton);
        make.bottom.mas_equalTo(commentButton);
    }];
    
    likesLabel = [[UILabel alloc] init];
    likesLabel.text = @"1240";
    likesLabel.font = DHSystemFontOfSize_12;
    likesLabel.textColor = FWTextColor_969696;
    likesLabel.textAlignment = NSTextAlignmentLeft;
    [likesButton addSubview:likesLabel];
    [likesLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(likesButton).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(likesButton);
    }];
    
    likesImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"unlike"]];
    [likesButton addSubview:likesImageView];
    [likesImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(likesButton);
        make.size.mas_equalTo(CGSizeMake(19, 16));
        make.right.mas_equalTo(likesLabel.mas_left).mas_offset(-5);
    }];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.text = @"";
    timeLabel.font = DHSystemFontOfSize_12;
    timeLabel.textColor = FWTextColor_969696;
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.shopButton).mas_offset(12);
        make.height.mas_equalTo(60);
        make.width.mas_equalTo(150);
        make.centerY.mas_equalTo(likesButton);
    }];
    
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        shareButton.enabled = YES;
    }else{
        shareButton.enabled = NO;
    }
    
    
    lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(likesButton.mas_bottom);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-1);
    }];
}

#pragma mark - > 配置cell
- (void)setListModel:(FWFeedListModel *)listModel{
    _listModel = listModel;
    
    containerArr = self.listModel.tags;
    
    [self refreshContainer:containerView WithArray:containerArr];
    
    nameLabel.text = self.listModel.user_info.nickname;
    contentLabel.text = self.listModel.feed_title;
    likesLabel.text = self.listModel.count_realtime.like_count_format;
    shareLabel.text = self.listModel.count_realtime.share_count;
    commentLabel.text = self.listModel.count_realtime.comment_count;
    contentLabel.text = self.listModel.feed_title;
    timeLabel.text = self.listModel.format_create_time;
    
    if (self.listModel.imgs.count > 0) {
        moreView.hidden = NO;
        moreLabel.text = @(self.listModel.imgs.count).stringValue;
    }else{
        moreView.hidden = YES;
    }
    
    //vip图标
    self.vipImageButton.enabled = YES;
    if ([self.listModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([self.listModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([self.listModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    //头像
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];
    
    //图片
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.feed_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    // 是否点赞
    if ([self.listModel.is_liked isEqualToString:@"2"]) {
        self.likeType = @"add";
        self.likesImageView.image = [UIImage imageNamed:@"unlike"];
    }else if ([self.listModel.is_liked isEqualToString:@"1"]){
        self.likeType = @"cancel";
        self.likesImageView.image = [UIImage imageNamed:@"car_detail_like"];
    }


    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:contentLabel.attributedText];
    
    CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 40].height;
    
    if (textHeight <= 60) {
        self.showAllButton.hidden = YES;
    }else{
        self.showAllButton.hidden = NO;
    }
    
    //展示文字或隐藏文字
    if (self.listModel.isShow) {
        self.contentLabel.numberOfLines = 0;
        [showAllButton setImage:[UIImage imageNamed:@"show_up"] forState:UIControlStateNormal];
    }else{
        self.contentLabel.numberOfLines = 3;
        
        if (self.contentLabel.text.length <=0 ) {
            textHeight = 1;
        }else{
            if (textHeight<=20 && textHeight>14) {
                textHeight = ceilf(contentLabel.font.lineHeight);
            }else if (textHeight > 20 &&textHeight<=40) {
                textHeight = 2*ceilf(contentLabel.font.lineHeight);
            }else if (textHeight > 40){
                textHeight = 3*ceilf(contentLabel.font.lineHeight);
            }
        }
        [showAllButton setImage:[UIImage imageNamed:@"show_down"] forState:UIControlStateNormal];
    }
    [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.top.mas_equalTo(self.photoImageView.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-40,textHeight));
    }];
    
    
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentLabel);
        make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(containerHeight);
    }];
    
    CGFloat picHeight = 60;
    CGFloat shopButtonHeight = 28;
    
    if (self.listModel.goods_id.length > 0 && ![self.listModel.goods_id isEqualToString:@"0"]) {
        /* 有商品链接 */
        [shopButton setModuleTitle:listModel.goods_info.title_short];
        shopButtonHeight = 28;
    }else{
        shopButtonHeight = 0.1;
    }
    
    [self.picImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.containerView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-picHeight-shopButtonHeight);
    }];
    
    [shopButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(shopButtonHeight);
        make.left.mas_equalTo(self.contentView).mas_offset(11);
        make.top.mas_equalTo(self.picImageView.mas_bottom).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
}

- (void) refreshContainer:(UIView *)container WithArray:(NSArray *)array{
    
    for (UIView * view in container.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    CGFloat x = 0;
    CGFloat y = 5;
    CGFloat width = 0.0;
    CGFloat height = 24;
    
    for (int i = 0; i < array.count; i++) {
        
        FWSearchTagsListModel * tagsModel = array[i];
        
        width = [tagsModel.tag_name length]*14+20;
        
        if (x + width+20 >= SCREEN_WIDTH) {
            x = 0;
            y += height + 10;
        }
        
        UIButton * btn = [[UIButton alloc] init];
        btn.titleLabel.font = DHSystemFontOfSize_14;
        btn.backgroundColor = FWViewBackgroundColor_FFFFFF;
        btn.tag = 10000+i;
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        btn.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
        [btn setTitle:[NSString stringWithFormat:@" %@",tagsModel.tag_name] forState:UIControlStateNormal];
        [btn setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnTagsClick:) forControlEvents:UIControlEventTouchUpInside];
        [container addSubview:btn];
        [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(container).mas_equalTo(x);
            make.top.mas_equalTo(container).mas_equalTo(y);
            make.size.mas_equalTo(CGSizeMake(width, height));
        }];
    
        x += width+10;
    }
    
    if (array.count <= 0) {
        containerHeight = 1;
    }else{
        containerHeight = y + 30;
    }
}

#pragma mark - > 关注（取消关注）
- (void)attentionButtonOnClick:(UIButton *)sender {
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
    }
}

#pragma mark - > 查看图组
- (void)picImageButtonOnClick:(UIButton *)sender {
    
    NSInteger index = sender.tag -1000;
    if ([self.delegate respondsToSelector:@selector(picImageClick:)]) {
        [self.delegate picImageClick:index];
    }
}

#pragma mark - > 查看标签
- (void)btnTagsClick:(UIButton *)sender{
    
    FWSearchTagsSubListModel * tagsModel = self.listModel.tags[sender.tag-10000];
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.traceType = 2;
    TVC.tagsModel = tagsModel;
    TVC.tags_id = tagsModel.tag_id;
    [self.viewController.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 显示全部
- (void)showAllButtonOnClick:(UIButton * )sender{
    
    NSInteger index = sender.tag -2000;
    self.contentLabel.numberOfLines = 0;

    if ([self.delegate respondsToSelector:@selector(showAllButtonClick:WithView:)]) {
        [self.delegate showAllButtonClick:index WithView:self];
    }
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.viewController.navigationController pushViewController:VVC animated:YES];
}


#pragma mark - > 喜欢
- (void)likesButtonOnClick:(UIButton *)sender{
    
    [self checkLogin];
    
    FWFeedListModel * model = self.listModel;
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.listModel.feed_id,
                                  @"like_type":self.likeType,
                                  };
        
        [request startWithParameters:params WithAction:Submit_like_feed WithDelegate:self.viewController  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([self.likeType isEqualToString: @"add"]) {
                    model.is_liked = @"1";
                    
                    if ([model.count_realtime.like_count_format integerValue] ||
                        [model.count_realtime.like_count_format integerValue] == 0) {

                        model.count_realtime.like_count_format = @([model.count_realtime.like_count_format integerValue] +1).stringValue;
                    }
                }else if ([self.likeType isEqualToString: @"cancel"]){
                    model.is_liked = @"2";
                    
                    if ([model.count_realtime.like_count_format integerValue] ||
                        [model.count_realtime.like_count_format integerValue] == 0) {

                        model.count_realtime.like_count_format = @([model.count_realtime.like_count_format integerValue] -1).stringValue;
                    }
                }
                
                self.listModel = model;
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewController];
            }
        }];
    }
}

#pragma mark - > 点击用户头像
- (void)photoImageViewTapClick{
    
    if (nil == self.listModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.uid;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 评论
- (void)commentButtonOnClick:(UIButton *)sender{
    
    NSInteger index = sender.tag -4000;
    
    if ([self.delegate respondsToSelector:@selector(commentButtonClick:)]) {
        [self.delegate commentButtonClick:index];
    }
}

#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{
    
//    [self checkLogin];
//
//    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSInteger index = sender.tag -5000;
        
        if ([self.delegate respondsToSelector:@selector(shareButtonClick:)]) {
            [self.delegate shareButtonClick:index];
        }
//    }
}

#pragma mark - > 跳转至商品
- (void)shopButtonOnClick{
    
    FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
    GDVC.goods_id = self.listModel.goods_id;
    [self.viewController.navigationController pushViewController:GDVC animated:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {

        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}
@end
