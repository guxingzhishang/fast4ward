//
//  FWCommentHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWTopicCommentFrame.h"

NS_ASSUME_NONNULL_BEGIN

@class FWCommentHeaderView;

@protocol FWCommentHeaderViewDelegate <NSObject>

@optional

/** 点击头像或昵称的事件回调 */
- (void)topicHeaderViewDidClickedUser:(FWCommentHeaderView *)topicHeaderView WithIndex:(NSInteger)index;

/** 用户点击点赞按钮 */
- (void)_thumbBtnClicked:(FWTopicCommentFrame *)topicCommentFrame WithIndex:(NSInteger)index;

/** 点击主评论 **/
- (void)commentButtonClick:(NSInteger)index;
@end


@interface FWCommentHeaderView : UITableViewHeaderFooterView

+ (instancetype)topicHeaderView;
+ (instancetype)headerViewWithTableView:(UITableView *)tableView;

/** 话题模型数据源 */
@property (nonatomic , strong) FWTopicCommentFrame *topicFrame;

/** 代理 */
@property (nonatomic , weak) id <FWCommentHeaderViewDelegate> delegate;


/** 当前header的点击 **/
@property (nonatomic, strong) UIButton * commentButton;

@property (nonatomic, weak) UIViewController * viewcontroller;

/** 头像 */
@property (nonatomic , strong) UIImageView *avatarView;

/** 昵称 */
@property (nonatomic , strong) YYLabel *nicknameLable;

/** 点赞 */
@property (nonatomic , strong) UIButton *thumbBtn;

/** 点赞图标 */
@property (nonatomic , strong) UIImageView *thumbImageView;

/** 点赞数 */
@property (nonatomic , strong) YYLabel *thumbNumberLabel;

/** 创建时间 */
@property (nonatomic , strong) YYLabel *createTimeLabel;

/** ContentView */
@property (nonatomic , strong) UIView *contentBaseView;

/** 文本内容 */
@property (nonatomic , strong) YYLabel *contentLabel;

@property (nonatomic , strong) NSString * comment_id;
@end

NS_ASSUME_NONNULL_END

