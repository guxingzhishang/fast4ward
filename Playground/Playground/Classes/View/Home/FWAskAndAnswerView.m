//
//  FWAskAndAnswerView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWAskAndAnswerView.h"

@implementation FWAskAndAnswerView
@synthesize isTimeSort;

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
 
    self.darenLabel = [[UILabel alloc] init];
    self.darenLabel.text = @"改装达人";
    self.darenLabel.frame = CGRectMake(16, CGRectGetMaxY(self.frame)+10, 250, 23);
    self.darenLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    self.darenLabel.textColor = FWTextColor_222222;
    self.darenLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.darenLabel];
    
    self.darenScrollView = [[UIScrollView alloc] init];
    self.darenScrollView.delegate = self;
    self.darenScrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.darenScrollView];
    self.darenScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.darenLabel.frame)+6, SCREEN_WIDTH, 0.01);
    
    self.typeLabel = [[UILabel alloc] init];
    self.typeLabel.textColor = FWTextColor_222222;
    self.typeLabel.textAlignment = NSTextAlignmentLeft;
    self.typeLabel.text = @"改装问答";
    self.typeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.typeLabel.frame = CGRectMake(CGRectGetMinX(self.darenLabel.frame)+2, CGRectGetMaxY(self.darenScrollView.frame)+5, 150, 40);
    [self addSubview:self.typeLabel];
    
    self.changeButton = [[UIButton alloc] init];
    self.changeButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.changeButton setTitleColor:FWColor(@"999999") forState:UIControlStateNormal];
    [self.changeButton setTitle:@"按时间" forState:UIControlStateNormal];
    [self.changeButton setImage:[UIImage imageNamed:@"wenda_qiehuan"] forState:UIControlStateNormal];
    [self.changeButton addTarget:self action:@selector(changeButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.changeButton];
    [self.changeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.typeLabel);
        make.size.mas_equalTo(CGSizeMake(50, 40));
        make.right.mas_equalTo(self).mas_offset(-20);
    }];
}

- (void)congifViewWithModel:(id)model{
    
    self.wendaModel = (FWAskAndAnswerModel *)model;
    
    if (self.wendaModel.list_wenda_suggestion.count >0) {
        self.darenScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.darenLabel.frame)+6, SCREEN_WIDTH, 126);
        [self setupDarenScrollView];
    }else{
        self.darenScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.darenLabel.frame)+6, SCREEN_WIDTH, 0.01);
    }
    self.typeLabel.frame = CGRectMake(CGRectGetMinX(self.darenLabel.frame), CGRectGetMaxY(self.darenScrollView.frame)+5, 150, 40);
}

#pragma mark - > 初始化ScrollView
- (void)setupDarenScrollView{
    
    for (UIView * view in self.darenScrollView.subviews) {
        [view removeFromSuperview];
    }
    
    for(int i = 0; i < self.wendaModel.list_wenda_suggestion.count; i++){
        
        FWAskAndAnswerListModel * listModel = self.wendaModel.list_wenda_suggestion[i];
        
        UIImageView * bgView = [[UIImageView alloc] init];
        bgView.tag = 20000+i;
        bgView.layer.cornerRadius = 2;
        bgView.layer.masksToBounds = YES;
        bgView.userInteractionEnabled = YES;
        bgView.backgroundColor = FWColor(@"F7F7F7");
        [self.darenScrollView addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(106, 126));
            make.top.mas_equalTo(self.darenScrollView);
            make.left.mas_equalTo(self.darenScrollView).mas_offset(i*116+14);
        }];
        
        UIImageView * photoImageView = [[UIImageView alloc] init];
        photoImageView.layer.cornerRadius = 55/2;
        photoImageView.layer.masksToBounds = YES;
        photoImageView.userInteractionEnabled = YES;
        photoImageView.tag = 25000+i;
        [photoImageView sd_setImageWithURL:[NSURL URLWithString:listModel.header_url] placeholderImage:nil];
        [bgView addSubview:photoImageView];
        [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(bgView).mas_offset(13);
            make.centerX.mas_equalTo(bgView);
            make.size.mas_equalTo(CGSizeMake(55, 55));
        }];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapClick:)];
        [photoImageView addGestureRecognizer:tap];
        
//        UIImageView * authenticationView = [[UIImageView alloc] init];
//        [bgView addSubview:authenticationView];
//        [authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.height.width.mas_equalTo(14);
//            make.right.mas_equalTo(photoImageView).mas_offset(-0);
//            make.bottom.mas_equalTo(photoImageView).mas_offset(-0);
//        }];
//        // 认证身份
//        if (userInfo.title.length > 0) {
//
//            if ([userInfo.cert_status isEqualToString:@"2"]){
//                authenticationView.hidden = NO;
//                authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];
//            }else{
//                authenticationView.hidden = YES;
//            }
//        }else{
//            authenticationView.hidden = YES;
//        }
        
        UILabel * nameLabel = [[UILabel alloc] init];
        nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        nameLabel.textColor = FWTextColor_272727;
        nameLabel.text = listModel.nickname;
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [bgView addSubview:nameLabel];
        [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(bgView);
            make.top.mas_equalTo(photoImageView.mas_bottom).mas_offset(13);
            make.height.mas_equalTo(18);
            make.width.mas_equalTo(81);
        }];
        
        UILabel * authenticationLabel = [[UILabel alloc] init];
        authenticationLabel.text = listModel.wenda_title;
        authenticationLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        authenticationLabel.textAlignment = NSTextAlignmentCenter;
        authenticationLabel.textColor = FWColor(@"BCBCBC");
        [bgView addSubview:authenticationLabel];
        [authenticationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.top.mas_equalTo(nameLabel.mas_bottom).mas_offset(2);
            make.centerX.mas_equalTo(bgView);
            make.width.mas_equalTo(125);
        }];
    }
    
    self.darenScrollView.contentSize = CGSizeMake(116 *self.wendaModel.list_wenda_suggestion.count, 0);
}

#pragma mark - > 切换排序
- (void)changeButtonOnClick{
    
    NSString * sortType = @"";
    isTimeSort = !isTimeSort;
    if (isTimeSort) {
        [self.changeButton setTitle:@"按时间" forState:UIControlStateNormal];
        sortType = @"new";
    }else{
        [self.changeButton setTitle:@"按热度" forState:UIControlStateNormal];
        sortType = @"hot";
    }
    
    if ([self.ankAndAnswerDelegate respondsToSelector:@selector(changeSortWithType:)]) {
        [self.ankAndAnswerDelegate changeSortWithType:sortType];
    }
}

- (void)photoImageViewTapClick:(UIGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag-25000;
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.wendaModel.list_wenda_suggestion[index].uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 返回当前顶部视图的高度
- (CGFloat)getCurrentViewHeight{
    
    CGFloat height = 0;
    height = CGRectGetMaxY(self.typeLabel.frame);
    return height;
}

#pragma mark - > 返回切换标签上部的高度
- (CGFloat)getBarViewTopHeight{
    
    CGFloat height = 0;
    height = CGRectGetMinY(self.typeLabel.frame);
    return height;
}

@end
