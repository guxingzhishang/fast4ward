//
//  FWBrowserImageView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/26.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWBrowserImageView : UIImageView

@property(nonatomic, assign) BOOL isScaled;

- (void)setImageWithUrl:(NSURL *)url placeholderImage:(UIImage *)placeholder;
- (void)doubleTapToZoomWithScale:(CGFloat)scale;

@end

NS_ASSUME_NONNULL_END
