//
//  FWCommentMessageCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWCommentMessageCell.h"
#import "FWCarDetailViewController.h"

@implementation FWCommentMessageCell
@synthesize tipLabel;
@synthesize messageModel;
@synthesize timeLabel;
@synthesize typeImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews{

    self.photoImageView.layer.cornerRadius = 43/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(15);
        make.left.mas_equalTo(self.contentView).mas_offset(24);
        make.size.mas_equalTo(CGSizeMake(43, 43));
    }];
    
    [self.authenticationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.right.mas_equalTo(self.photoImageView).mas_offset(2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(2);
    }];
    
    self.nameLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size:16];
    self.nameLabel.textColor = FWTextColor_000000;
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(16);
        make.top.mas_equalTo(self.contentView).mas_offset(18);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.selectImageView.userInteractionEnabled = YES;
    self.selectImageView.layer.cornerRadius = 2;
    self.selectImageView.layer.masksToBounds = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImageViewTapClick)];
    [self.selectImageView addGestureRecognizer:tap];
    [self.selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(64);
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView).mas_offset(-16);
    }];
    
    
    self.typeImageView = [UIImageView new];
    [self.selectImageView addSubview:self.typeImageView];
    [self.typeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(13, 13));
        make.right.bottom.mas_equalTo(-5);
    }];
    self.typeImageView.hidden = YES;
    
    self.signLabel.font =  [UIFont fontWithName:@"PingFang-SC-Regular" size:13];;
    self.signLabel.textColor = FWTextColor_000000;
    self.signLabel.numberOfLines = 1;
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.selectImageView.mas_left).mas_offset(-15);
        make.left.mas_equalTo(self.nameLabel).mas_offset(0);
        make.height.mas_equalTo(16);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(5);
    }];
    
    
    tipLabel = [[UILabel alloc] init];
    tipLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size:13];;
    tipLabel.textColor = FWTextColor_9EA3AB;
    tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.nameLabel).mas_offset(0);
        make.top.mas_equalTo(self.signLabel.mas_bottom).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font =  [UIFont fontWithName:@"PingFang-SC-Regular" size:13];
    timeLabel.textColor = FWTextColor_9EA3AB;
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(tipLabel);
        make.left.mas_equalTo(self.tipLabel.mas_right).mas_offset(5);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    self.lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.selectImageView).mas_offset(0);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(16);
        make.bottom.mas_equalTo(self.contentView).mas_equalTo(-0.5);
    }];
}

-(void)cellConfigureFor:(id)model{
    
    messageModel = (FWUnionMessageModel *)model;
    
    self.nameLabel.text = messageModel.r_user_info.nickname;
    self.signLabel.text = messageModel.msg_content;
    self.tipLabel.text  = messageModel.msg_text;
    self.timeLabel.text = messageModel.format_create_time;
    
    if([messageModel.r_user_info.cert_status isEqualToString:@"2"]) {
        self.nameLabel.textColor = FWTextColor_FFAF3C;
        self.authenticationImageView.hidden = NO;
        self.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        if ([messageModel.r_user_info.merchant_cert_status isEqualToString:@"3"] &&
            ![messageModel.r_user_info.cert_status isEqualToString:@"2"]){
            self.nameLabel.textColor = FWTextColor_2B98FA;
            self.authenticationImageView.hidden = YES;
        }else{
            self.authenticationImageView.hidden = YES;
            self.nameLabel.textColor = FWTextColor_000000;
        }
    }
    
    if ([messageModel.feed_type isEqualToString:@"1"]) {
        typeImageView.hidden = NO;
        typeImageView.image = [UIImage imageNamed:@"comment_video"];
    }else if ([messageModel.feed_type isEqualToString:@"2"]) {
        typeImageView.hidden = NO;
        typeImageView.image = [UIImage imageNamed:@"comment_pic"];
    }else if ([messageModel.feed_type isEqualToString:@"3"]) {
        typeImageView.hidden = NO;
        typeImageView.image = [UIImage imageNamed:@"card_artical"];
    }else{
        typeImageView.hidden = YES;
        typeImageView.image = [UIImage imageNamed:@""];
    }
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:messageModel.r_user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];
    
    [self.selectImageView sd_setImageWithURL:[NSURL URLWithString:messageModel.feed_cover] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];
}

#pragma mark - > 点击用户头像
- (void)photoImageViewTapClick{
    
    if (nil == self.messageModel.r_uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.messageModel.r_uid;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 跳转至帖子详情页
- (void)selectImageViewTapClick{
    

    if ([self.messageModel.feed_type isEqualToString:@"1"]) {
        
        if ([self.messageModel.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.feed_id = self.messageModel.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {};
            [self.vc.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            // 视频流 由于筛选过，所以每次点击的都是数据源第一个。
            FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
            controller.currentIndex = 0;
            controller.requestType = FWPushMessageRequestType;
            controller.tag_id = @"";
            controller.flag_return = @"1";
            controller.feed_id = self.messageModel.feed_id;
            [self.vc.navigationController pushViewController:controller animated:YES];
        }
    }else if ([self.messageModel.feed_type isEqualToString:@"2"]) {
        
        // 图文详情
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.feed_id = self.messageModel.feed_id;
        TVC.myBlock = ^(FWFeedListModel *awemeModel) {};
        [self.vc.navigationController pushViewController:TVC animated:YES];
    }else if ([self.messageModel.feed_type isEqualToString:@"3"]) {
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = self.messageModel.feed_id;
        [self.vc.navigationController pushViewController:DVC animated:YES];
    }else if([messageModel.feed_type isEqualToString:@"10"]) {
        
        // 文章页
        FWCarDetailViewController * DVC = [[FWCarDetailViewController alloc] init];
        DVC.user_car_id = self.messageModel.user_car_id;
        [self.vc.navigationController pushViewController:DVC animated:YES];
    }
}


@end
