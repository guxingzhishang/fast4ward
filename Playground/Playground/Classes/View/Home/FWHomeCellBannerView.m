//
//  FWHomeCellBannerView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeCellBannerView.h"

@interface FWHomeCellBannerView  ()

@property (nonatomic, assign) CGFloat  bannerWidth;

@end

@implementation FWHomeCellBannerView

- (id)init{
    self = [super init];
    if (self) {
        
        self.bannerWidth = SCREEN_WIDTH-45-20;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    if (!self.firstIV) {
        self.firstIV = [[UIImageView alloc] init];
    }
    self.firstIV.layer.cornerRadius = 4;
    self.firstIV.layer.masksToBounds = YES;
    self.firstIV.clipsToBounds = YES;
    self.firstIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.firstIV];
    self.firstIV.tag = 88990;
    self.firstIV.userInteractionEnabled = YES;
    [self.firstIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];


    if (!self.secondIV) {
        self.secondIV = [[UIImageView alloc] init];
    }
    self.secondIV.layer.cornerRadius = 4;
    self.secondIV.layer.masksToBounds = YES;
    self.secondIV.clipsToBounds = YES;
    self.secondIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.secondIV];
    self.secondIV.hidden = YES;
    self.secondIV.tag = 88991;
    self.secondIV.userInteractionEnabled = YES;
    [self.secondIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];
    
    if (!self.thirdIV) {
        self.thirdIV = [[UIImageView alloc] init];
    }
    self.thirdIV.layer.cornerRadius = 4;
    self.thirdIV.layer.masksToBounds = YES;
    self.thirdIV.clipsToBounds = YES;
    self.thirdIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.thirdIV];
    self.thirdIV.hidden = YES;
    self.thirdIV.tag = 88992;
    self.thirdIV.userInteractionEnabled = YES;
    [self.thirdIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];
    
    if (!self.fourthIV) {
        self.fourthIV = [[UIImageView alloc] init];
    }
    self.fourthIV.layer.cornerRadius = 4;
    self.fourthIV.layer.masksToBounds = YES;
    self.fourthIV.clipsToBounds = YES;
    self.fourthIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.fourthIV];
    self.fourthIV.hidden = YES;
    self.fourthIV.tag = 88993;
    self.fourthIV.userInteractionEnabled = YES;
    [self.fourthIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];

    if (!self.fifthIV) {
        self.fifthIV = [[UIImageView alloc] init];
    }
    self.fifthIV.layer.cornerRadius = 4;
    self.fifthIV.layer.masksToBounds = YES;
    self.fifthIV.clipsToBounds = YES;
    self.fifthIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.fifthIV];
    self.fifthIV.hidden = YES;
    self.fifthIV.tag = 88994;
    self.fifthIV.userInteractionEnabled = YES;
    [self.fifthIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];

    if (!self.sixthIV) {
        self.sixthIV = [[UIImageView alloc] init];
    }
    self.sixthIV.layer.cornerRadius = 4;
    self.sixthIV.layer.masksToBounds = YES;
    self.sixthIV.clipsToBounds = YES;
    self.sixthIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.sixthIV];
    self.sixthIV.hidden = YES;
    self.sixthIV.tag = 88995;
    self.sixthIV.userInteractionEnabled = YES;
    [self.sixthIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];
    
    if (!self.seventhIV) {
        self.seventhIV = [[UIImageView alloc] init];
    }
    self.seventhIV.layer.cornerRadius = 4;
    self.seventhIV.layer.masksToBounds = YES;
    self.seventhIV.clipsToBounds = YES;
    self.seventhIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.seventhIV];
    self.seventhIV.hidden = YES;
    self.seventhIV.tag = 88996;
    self.seventhIV.userInteractionEnabled = YES;
    [self.seventhIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];
    
    if (!self.eighthIV) {
        self.eighthIV = [[UIImageView alloc] init];
    }
    self.eighthIV.layer.cornerRadius = 4;
    self.eighthIV.layer.masksToBounds = YES;
    self.eighthIV.clipsToBounds = YES;
    self.eighthIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.eighthIV];
    self.eighthIV.hidden = YES;
    self.eighthIV.tag = 88997;
    self.eighthIV.userInteractionEnabled = YES;
    [self.eighthIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];
    
    if (!self.ninethIV) {
        self.ninethIV = [[UIImageView alloc] init];
    }
    self.ninethIV.layer.cornerRadius = 4;
    self.ninethIV.layer.masksToBounds = YES;
    self.ninethIV.clipsToBounds = YES;
    self.ninethIV.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.ninethIV];
    self.ninethIV.hidden = YES;
    self.ninethIV.tag = 88998;
    self.ninethIV.userInteractionEnabled = YES;
    [self.ninethIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(IVClick:)]];
}

- (void)IVClick:(UITapGestureRecognizer *)tap{
    NSInteger index = tap.view.tag - 88990;
    
    if ([self.bannerDelegate respondsToSelector:@selector(bannerClick:)]) {
        [self.bannerDelegate bannerClick:index];
    }
}

#pragma mark - > 赋值显示九宫格
- (void)configForBanner:(NSArray *)imageArray{
    
    self.imageArr = imageArray;

    if (self.imageArr.count == 1) {
        [self setupOneView];
    }else if (self.imageArr.count == 2) {
        [self setupTwoView];
    }else if (self.imageArr.count == 3) {
        [self setupThreeView];
    }else if (self.imageArr.count == 4) {
        [self setupFourView];
    }else if (self.imageArr.count == 5) {
        [self setupFiveView];
    }else if (self.imageArr.count == 6) {
        [self setupSixView];
    }else if (self.imageArr.count == 7) {
        [self setupSevenView];
    }else if (self.imageArr.count == 8) {
        [self setupEightView];
    }else if (self.imageArr.count == 9) {
        [self setupNineView];
    }
}

- (void)setupOneView{
    
    self.secondIV.hidden = YES;
    self.thirdIV.hidden = YES;
    self.fourthIV.hidden = YES;
    self.fifthIV.hidden = YES;
    self.sixthIV.hidden = YES;
    self.seventhIV.hidden = YES;
    self.eighthIV.hidden = YES;
    self.ninethIV.hidden = YES;
    
    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.firstIV.frame = CGRectMake(0, 0, self.bannerWidth, (self.bannerWidth)/2);
}

- (void)setupTwoView{
    
    self.secondIV.hidden = NO;
    self.thirdIV.hidden = YES;
    self.fourthIV.hidden = YES;
    self.fifthIV.hidden = YES;
    self.sixthIV.hidden = YES;
    self.seventhIV.hidden = YES;
    self.eighthIV.hidden = YES;
    self.ninethIV.hidden = YES;
    
    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.secondIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[1]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    self.firstIV.frame = CGRectMake(0, 0, (self.bannerWidth-3)/2, (self.bannerWidth-3)/2);
    self.secondIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, 0, CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
}

- (void)setupThreeView{
    
    self.secondIV.hidden = NO;
    self.thirdIV.hidden = NO;
    self.fourthIV.hidden = YES;
    self.fifthIV.hidden = YES;
    self.sixthIV.hidden = YES;
    self.seventhIV.hidden = YES;
    self.eighthIV.hidden = YES;
    self.ninethIV.hidden = YES;

    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.secondIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[1]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.thirdIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[2]] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    self.firstIV.frame = CGRectMake(0, 0, (self.bannerWidth-6)/3, (self.bannerWidth-6)/3);
    self.secondIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, 0, CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
    self.thirdIV.frame = CGRectMake(CGRectGetMaxX(self.secondIV.frame)+3, 0, CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
}

- (void)setupFourView{
    
    self.secondIV.hidden = NO;
    self.thirdIV.hidden = NO;
    self.fourthIV.hidden = NO;
    self.fifthIV.hidden = YES;
    self.sixthIV.hidden = YES;
    self.seventhIV.hidden = YES;
    self.eighthIV.hidden = YES;
    self.ninethIV.hidden = YES;

    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.secondIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[1]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.thirdIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[2]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fourthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[3]] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    self.firstIV.frame = CGRectMake(0, 0, (self.bannerWidth-3)/2,  (self.bannerWidth-3)/2);
    self.secondIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, 0, CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
    self.thirdIV.frame = CGRectMake(0, CGRectGetMaxY(self.firstIV.frame)+3,CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
    self.fourthIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, CGRectGetMaxY(self.firstIV.frame)+3, CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
}

- (void)setupFiveView{
    
    self.secondIV.hidden = NO;
    self.thirdIV.hidden = NO;
    self.fourthIV.hidden = NO;
    self.fifthIV.hidden = NO;
    self.sixthIV.hidden = YES;
    self.seventhIV.hidden = YES;
    self.eighthIV.hidden = YES;
    self.ninethIV.hidden = YES;
    
    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.secondIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[1]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.thirdIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[2]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fourthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[3]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fifthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[4]] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    self.firstIV.frame = CGRectMake(0, 0,2*(self.bannerWidth-3)/3, 2*(self.bannerWidth-3)/3+3);
    self.secondIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, 0, (self.bannerWidth-3)/3,(self.bannerWidth-3)/3);
    self.thirdIV.frame = CGRectMake(CGRectGetMinX(self.secondIV.frame), CGRectGetMaxY(self.secondIV.frame)+3, CGRectGetWidth(self.secondIV.frame), CGRectGetHeight(self.secondIV.frame));
    self.fourthIV.frame = CGRectMake(0, CGRectGetMaxY(self.firstIV.frame)+3, (self.bannerWidth-3)/2,CGRectGetHeight(self.firstIV.frame)/2);
    self.fifthIV.frame = CGRectMake(CGRectGetMaxX(self.fourthIV.frame)+3,CGRectGetMinY(self.fourthIV.frame), CGRectGetWidth(self.fourthIV.frame),CGRectGetHeight(self.fourthIV.frame));
}

- (void)setupSixView{
    
    self.secondIV.hidden = NO;
    self.thirdIV.hidden = NO;
    self.fourthIV.hidden = NO;
    self.fifthIV.hidden = NO;
    self.sixthIV.hidden = NO;
    self.seventhIV.hidden = YES;
    self.eighthIV.hidden = YES;
    self.ninethIV.hidden = YES;

    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.secondIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[1]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.thirdIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[2]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fourthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[3]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fifthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[4]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.sixthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[5]] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    self.firstIV.frame = CGRectMake(0, 0,2*(self.bannerWidth-6)/3+3, 2*(self.bannerWidth-6)/3+3);
    self.secondIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, 0, (self.bannerWidth-6)/3,(self.bannerWidth-6)/3);
    self.thirdIV.frame = CGRectMake(CGRectGetMinX(self.secondIV.frame), CGRectGetMaxY(self.secondIV.frame)+3, CGRectGetWidth(self.secondIV.frame), CGRectGetWidth(self.secondIV.frame));
    self.fourthIV.frame = CGRectMake(0, CGRectGetMaxY(self.firstIV.frame)+3, (self.bannerWidth-6)/3, (self.bannerWidth-6)/3);
    self.fifthIV.frame = CGRectMake(CGRectGetMaxX(self.fourthIV.frame)+3,CGRectGetMinY(self.fourthIV.frame), (self.bannerWidth-6)/3, (self.bannerWidth-6)/3);
    self.sixthIV.frame = CGRectMake(CGRectGetMaxX(self.fifthIV.frame)+3,CGRectGetMinY(self.fourthIV.frame),(self.bannerWidth-6)/3, (self.bannerWidth-6)/3);
}

- (void)setupSevenView{
    
    self.secondIV.hidden = NO;
    self.thirdIV.hidden = NO;
    self.fourthIV.hidden = NO;
    self.fifthIV.hidden = NO;
    self.sixthIV.hidden = NO;
    self.seventhIV.hidden = NO;
    self.eighthIV.hidden = YES;
    self.ninethIV.hidden = YES;

    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.secondIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[1]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.thirdIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[2]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fourthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[3]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fifthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[4]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.sixthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[5]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.seventhIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[6]] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    self.firstIV.frame = CGRectMake(0, 0, (self.bannerWidth-3)/2,  (self.bannerWidth-3)/2);
    self.secondIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, 0,  (((self.bannerWidth-3)/2)-3)/2, (((self.bannerWidth-3)/2)-3)/2);
    self.thirdIV.frame = CGRectMake(CGRectGetMaxX(self.secondIV.frame)+3, 0, CGRectGetWidth(self.secondIV.frame),CGRectGetHeight(self.secondIV.frame));
    self.fourthIV.frame = CGRectMake(CGRectGetMinX(self.secondIV.frame), CGRectGetMaxY(self.thirdIV.frame)+3, CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
    self.fifthIV.frame = CGRectMake(0, CGRectGetMaxY(self.firstIV.frame)+3, CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
    self.sixthIV.frame = CGRectMake(CGRectGetMinX(self.fourthIV.frame), CGRectGetMaxY(self.fourthIV.frame)+3, CGRectGetWidth(self.secondIV.frame),CGRectGetHeight(self.secondIV.frame));
    self.seventhIV.frame = CGRectMake(CGRectGetMaxX(self.sixthIV.frame)+3, CGRectGetMinY(self.sixthIV.frame), CGRectGetWidth(self.secondIV.frame),CGRectGetHeight(self.secondIV.frame));
}

- (void)setupEightView{
    
    self.secondIV.hidden = NO;
    self.thirdIV.hidden = NO;
    self.fourthIV.hidden = NO;
    self.fifthIV.hidden = NO;
    self.sixthIV.hidden = NO;
    self.seventhIV.hidden = NO;
    self.eighthIV.hidden = NO;
    self.ninethIV.hidden = YES;

    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.secondIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[1]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.thirdIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[2]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fourthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[3]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fifthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[4]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.sixthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[5]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.seventhIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[6]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.eighthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[7]] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    self.firstIV.frame = CGRectMake(0, 0, (self.bannerWidth-6)/3, (self.bannerWidth-6)/3);
    self.secondIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, 0, CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
    self.thirdIV.frame = CGRectMake(CGRectGetMaxX(self.secondIV.frame)+3, 0, CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
    self.fourthIV.frame = CGRectMake(0, CGRectGetMaxY(self.firstIV.frame)+3, (self.bannerWidth-3)/2, (self.bannerWidth-6)/3);
    self.fifthIV.frame = CGRectMake(CGRectGetMaxX(self.fourthIV.frame)+3, CGRectGetMinY(self.fourthIV.frame), CGRectGetWidth(self.fourthIV.frame), CGRectGetHeight(self.fourthIV.frame));
    self.sixthIV.frame = CGRectMake(0, CGRectGetMaxY(self.fourthIV.frame)+3, CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
    self.seventhIV.frame = CGRectMake(CGRectGetMaxX(self.sixthIV.frame)+3, CGRectGetMinY(self.sixthIV.frame),CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
    self.eighthIV.frame = CGRectMake(CGRectGetMaxX(self.seventhIV.frame)+3, CGRectGetMinY(self.sixthIV.frame), CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
}

- (void)setupNineView{
    
    self.secondIV.hidden = NO;
    self.thirdIV.hidden = NO;
    self.fourthIV.hidden = NO;
    self.fifthIV.hidden = NO;
    self.sixthIV.hidden = NO;
    self.seventhIV.hidden = NO;
    self.eighthIV.hidden = NO;
    self.ninethIV.hidden = NO;
    
    [self.firstIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[0]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.secondIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[1]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.thirdIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[2]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fourthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[3]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.fifthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[4]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.sixthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[5]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.seventhIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[6]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.eighthIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[7]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.ninethIV sd_setImageWithURL:[NSURL URLWithString:self.imageArr[8]] placeholderImage:[UIImage imageNamed:@"placeholder"]];


    self.firstIV.frame = CGRectMake(0, 0, (self.bannerWidth-6)/3, (self.bannerWidth-6)/3);
    self.secondIV.frame = CGRectMake(CGRectGetMaxX(self.firstIV.frame)+3, 0, CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
    self.thirdIV.frame = CGRectMake(CGRectGetMaxX(self.secondIV.frame)+3, 0, CGRectGetWidth(self.firstIV.frame),CGRectGetHeight(self.firstIV.frame));
    
    self.fourthIV.frame = CGRectMake(0, CGRectGetMaxY(self.firstIV.frame)+3, CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
    self.fifthIV.frame = CGRectMake(CGRectGetMaxX(self.fourthIV.frame)+3, CGRectGetMinY(self.fourthIV.frame), CGRectGetWidth(self.fourthIV.frame), CGRectGetHeight(self.fourthIV.frame));
    self.sixthIV.frame = CGRectMake(CGRectGetMaxX(self.fifthIV.frame)+3, CGRectGetMinY(self.fourthIV.frame), CGRectGetWidth(self.fourthIV.frame), CGRectGetHeight(self.fourthIV.frame));
    
    self.seventhIV.frame = CGRectMake(0, CGRectGetMaxY(self.fourthIV.frame)+3,CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
    self.eighthIV.frame = CGRectMake(CGRectGetMaxX(self.seventhIV.frame)+3, CGRectGetMinY(self.seventhIV.frame), CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
    self.ninethIV.frame = CGRectMake(CGRectGetMaxX(self.eighthIV.frame)+3, CGRectGetMinY(self.eighthIV.frame), CGRectGetWidth(self.firstIV.frame), CGRectGetHeight(self.firstIV.frame));
}

- (CGFloat)getCurrentHeight{
    
    CGFloat currentHeight = 0;
    
    if (self.imageArr.count == 1) {
        currentHeight = CGRectGetMaxY(self.firstIV.frame);
    }else if (self.imageArr.count == 2) {
        currentHeight = CGRectGetMaxY(self.secondIV.frame);
    }else if (self.imageArr.count == 3) {
        currentHeight = CGRectGetMaxY(self.thirdIV.frame);
    }else if (self.imageArr.count == 4) {
        currentHeight = CGRectGetMaxY(self.fourthIV.frame);
    }else if (self.imageArr.count == 5) {
        currentHeight = CGRectGetMaxY(self.fifthIV.frame);
    }else if (self.imageArr.count == 6) {
        currentHeight = CGRectGetMaxY(self.sixthIV.frame);
    }else if (self.imageArr.count == 7) {
        currentHeight = CGRectGetMaxY(self.seventhIV.frame);
    }else if (self.imageArr.count == 8) {
        currentHeight = CGRectGetMaxY(self.eighthIV.frame);
    }else if (self.imageArr.count == 9) {
        currentHeight = CGRectGetMaxY(self.ninethIV.frame);
    }
//    NSLog(@"currentHeight== %.2f",currentHeight);
    return currentHeight;
}


@end
