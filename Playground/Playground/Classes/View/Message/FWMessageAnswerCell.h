//
//  FWMessageAnswerCell.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/3.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMessageAnswerCell : UITableViewCell

@property (nonatomic, strong) UIImageView * photoImage;
//@property (nonatomic, strong) UIImageView * <#name#>;
//@property (nonatomic, strong) <#type#> * <#name#>;
//@property (nonatomic, strong) <#type#> * <#name#>;
//@property (nonatomic, strong) <#type#> * <#name#>;
//@property (nonatomic, strong) <#type#> * <#name#>;
//@property (nonatomic, strong) <#type#> * <#name#>;


@end

NS_ASSUME_NONNULL_END
