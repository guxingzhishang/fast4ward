
//
//  FWMessageCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMessageCell.h"

@implementation FWMessageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews{
    
    self.iconView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.iconView];
    [self.iconView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(21, 19));
        make.left.mas_equalTo(self.contentView).mas_offset(23);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 14];
    self.titleLabel.textColor = FWTextColor_222222;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconView.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(self.contentView);
        make.height.mas_equalTo(25);
    }];
    
    self.arrowImageView = [[UIImageView alloc] init];
    self.arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.contentView addSubview:self.arrowImageView];
    [self.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(6, 11));
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    CGFloat height = 20;
    self.badgeLabel = [[UILabel alloc] init];
    self.badgeLabel.backgroundColor = FWColor(@"FF6f00");
    self.badgeLabel.layer.cornerRadius = height/2;
    self.badgeLabel.textAlignment = NSTextAlignmentCenter;
    self.badgeLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.badgeLabel.layer.masksToBounds = YES;
    self.badgeLabel.font = DHSystemFontOfSize_12;
    [self.contentView addSubview:self.badgeLabel];
    [self.badgeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.arrowImageView.mas_left).mas_offset(-15);
        make.centerY.mas_equalTo(self.arrowImageView);
        make.height.mas_equalTo(height);
        make.width.mas_greaterThanOrEqualTo(height);
    }];
    
//    self.lineView = [[UIView alloc] init];
//    self.lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
//    [self.contentView addSubview:self.lineView];
//    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self.arrowImageView);
//        make.left.mas_equalTo(self.iconView);
//        make.bottom.mas_equalTo(self.contentView).mas_offset(-1);
//        make.height.mas_equalTo(1);
//    }];
    
}

@end
