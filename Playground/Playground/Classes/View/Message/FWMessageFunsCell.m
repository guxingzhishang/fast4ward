//
//  FWMessageFunsCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMessageFunsCell.h"

@implementation FWMessageFunsCell
@synthesize followButton;
@synthesize preArray;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [self changeSubviews];
    
    return self;
}

- (void)changeSubviews{
    
    followButton = [[UIButton alloc] init];
    followButton.layer.cornerRadius = 2;
    followButton.layer.masksToBounds = YES;
    followButton.titleLabel.font = DHSystemFontOfSize_14;
    [followButton setTitle:@"互相关注" forState:UIControlStateNormal];
    [followButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
    [followButton addTarget:self action:@selector(followButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:followButton];
    [followButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(69,30));
    }];
    followButton.hidden = YES;
    
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-20);
        make.left.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-1);
    }];
    
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.photoImageView).mas_offset(3);
        make.right.mas_equalTo(self.followButton.mas_left).mas_offset(-5);
    }];
    
    self.signLabel.textColor = FWTextColor_9EA3AB;
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    [self.signLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.right.mas_equalTo(self.followButton.mas_left).mas_offset(-5);
    }];
}

- (void)cellConfigureFor:(id)model{
    self.messageModel = (FWUnionMessageModel *)model;
    
    if ([self.messageModel.r_user_info.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        // 自己不显示关注
        followButton.hidden = YES;
    }
    
    self.selectImageView.hidden = YES;
    
    self.nameLabel.text = self.messageModel.r_user_info.nickname;
    self.signLabel.text = self.messageModel.msg_text;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.messageModel.r_user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]  options:SDWebImageRefreshCached];
    
    NSString * buttonTitle;
//    NSInteger state = [listModel.follow_status integerValue];
    NSInteger state = 1;
    UIColor * titleColor = nil;
    UIColor * buttonBackgroundColor = nil;
    switch (state) {
            
        case 1:
        {
            buttonTitle = @"已关注";
            titleColor = FWTextColor_515151;
            buttonBackgroundColor = FWViewBackgroundColor_EEEEEE;
        }
            break;
        case 2:
        {
            buttonTitle = @"关注";
            titleColor = FWViewBackgroundColor_FFFFFF;
            buttonBackgroundColor = FWTextColor_222222;
        }
            break;
        case 3:
        {
            buttonTitle = @"互相关注";
            titleColor = FWTextColor_515151;
            buttonBackgroundColor = FWViewBackgroundColor_EEEEEE;
        }
            break;
        default:
            break;
    }
    
    [followButton setBackgroundColor:buttonBackgroundColor];
    [followButton setTitle:buttonTitle forState:UIControlStateNormal];
    [followButton setTitleColor:titleColor forState:UIControlStateNormal];
}

#pragma mark - > 关注
- (void)followButtonClick:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(followButtonForIndex:withCell:)]) {
        [self.delegate followButtonForIndex:sender.tag - 5000 withCell:self];
    }
}

#pragma mark - > 点击用户头像
- (void)photoImageViewTapClick{
    
    if (nil == self.messageModel.r_uid) {
        return;
    }
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.messageModel.r_uid;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}
@end
