//
//  FWMessageNewFunsCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/25.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMessageNewFunsCell : FWListCell

@property (nonatomic, weak) UIViewController * vc;


@end

NS_ASSUME_NONNULL_END
