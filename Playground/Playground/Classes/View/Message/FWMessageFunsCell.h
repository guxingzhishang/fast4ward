//
//  FWMessageFunsCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 由于消息页改造，迁移到tabbar下，所以这个控制器，目前只有个人页中的粉丝用
 * 消息页中的粉丝列表，另用其他cell(FWMessageNewFunsCell)
 */

#import "FWListCell.h"
#import "FWUnionMessageModel.h"

@class FWMessageFunsCell;

@protocol FWMessageFunsCellDelegate<NSObject>

- (void)followButtonForIndex:(NSInteger)index  withCell:(FWMessageFunsCell *)cell;

@end

@interface FWMessageFunsCell : FWListCell

@property (nonatomic, strong) FWUnionMessageModel * messageModel;

@property (nonatomic, strong) UIButton * followButton;

@property (nonatomic, strong) NSMutableArray * preArray;

@property (nonatomic, weak) id<FWMessageFunsCellDelegate> delegate;

- (void)cellConfigureFor:(id)model;

@end
