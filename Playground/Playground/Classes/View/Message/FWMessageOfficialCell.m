//
//  FWMessageOfficialCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMessageOfficialCell.h"
#import "FWMessageHomeModel.h"

@implementation FWMessageOfficialCell
@synthesize timeLabel;
@synthesize unreadView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.selectImageView.hidden = YES;
    self.photoImageView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    self.photoImageView.layer.cornerRadius = 43/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(43, 43));
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(24);
    }];
    
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:16];
    self.nameLabel.textColor = FWTextColor_000000;
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(14);
        make.height.mas_equalTo(20);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-184);
        make.top.mas_equalTo(self.photoImageView).mas_offset(0);
    }];
    
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
    self.signLabel.textColor = FWTextColor_000000;
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(self.nameLabel);
        make.width.mas_lessThanOrEqualTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(3);
    }];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
    timeLabel.textColor = FWTextColor_9EA3AB;
    timeLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(self.nameLabel);
        make.width.mas_greaterThanOrEqualTo(50);
    }];
    
    self.lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.timeLabel);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-0.5);
    }];
    
    unreadView = [[UIImageView alloc] init];
    unreadView.hidden = YES;
    unreadView.layer.cornerRadius = 3;
    unreadView.layer.masksToBounds = YES;
    unreadView.backgroundColor = DHRedColorff6f00;
    [self.contentView addSubview:unreadView];
    [unreadView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(6, 6));
        make.right.mas_equalTo(self.contentView).mas_offset(-15);
        make.centerY.mas_equalTo(self.contentView).mas_offset(3);
    }];
    unreadView.hidden = YES;
}

-(void)cellConfigureFor:(id)model{
    
    FWMessageListModel * listModel = (FWMessageListModel *)model;
    
    self.nameLabel.text = listModel.nickname;
    self.signLabel.text = listModel.content;
    self.timeLabel.text = listModel.format_create_time;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:listModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"] options:SDWebImageRefreshCached];
    
    if ([listModel.read_status isEqualToString:@"1"]) {
        unreadView.hidden = YES;
    }else if ([listModel.read_status isEqualToString:@"2"]){
        unreadView.hidden = NO;
    }
}


@end
