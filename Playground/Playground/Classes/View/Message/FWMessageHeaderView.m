//
//  FWMessageHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMessageHeaderView.h"
#import "FWFunsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "FWThumbUpViewController.h"
#import "FWMessageHomeModel.h"
#import "FWShareScoreImageViewController.h"

#define PHOTOWIDTH 60

@implementation FWMessageHeaderView

@synthesize thumbUpButton;
@synthesize thumbUpLabel;
@synthesize thumbUpImageView;
@synthesize thumbUpUnreadLabel;

@synthesize commentButton;
@synthesize commentLabel;
@synthesize commentImageView;
@synthesize commentUnreadLabel;

@synthesize funsButton;
@synthesize funsLabel;
@synthesize funsImageView;
@synthesize funsUnreadLabel;

@synthesize honView;

- (instancetype)init{
    
    self = [super init];
    
    if (self) {
        
        self.backgroundColor = DHTitleColor_FFFFFF;
        [self setupTopSubviews];
    }
    
    return self;
}

#pragma mark - > 初始化头部视图
- (void)setupTopSubviews{
    
    thumbUpButton = [[UIButton alloc] init];
    thumbUpButton.backgroundColor = [UIColor clearColor];
    [self addSubview:thumbUpButton];
    [thumbUpButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(84);
        make.width.mas_equalTo(100);
        make.left.mas_equalTo((SCREEN_WIDTH-300)/2);
        make.top.mas_equalTo(self);
    }];
    
    thumbUpImageView = [[UIImageView alloc] init];
    thumbUpImageView.image = [UIImage imageNamed:@"message_favourite"];
    [thumbUpButton addSubview:thumbUpImageView];
    [thumbUpImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(17);
        make.width.mas_equalTo(18);
        make.centerX.mas_equalTo(thumbUpButton);
        make.top.mas_equalTo(thumbUpButton).mas_offset(20);
    }];
    
    thumbUpUnreadLabel = [[UILabel alloc] init];
    thumbUpUnreadLabel.hidden = YES;
    thumbUpUnreadLabel.layer.cornerRadius = 8;
    thumbUpUnreadLabel.layer.masksToBounds = YES;
    thumbUpUnreadLabel.font = [UIFont systemFontOfSize:8];
    thumbUpUnreadLabel.backgroundColor = DHPinkColorFF8B8C;
    thumbUpUnreadLabel.textAlignment = NSTextAlignmentCenter;
    thumbUpUnreadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    [thumbUpButton addSubview:thumbUpUnreadLabel];
    [thumbUpUnreadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(16);
        make.top.mas_equalTo(thumbUpImageView).mas_offset(-8);
        make.left.mas_equalTo(thumbUpImageView.mas_right).mas_offset(-5);
    }];

    thumbUpLabel = [[UILabel alloc] init];
    thumbUpLabel.text = @"点赞";
    thumbUpLabel.font = DHSystemFontOfSize_11;
    thumbUpLabel.textColor = FWTextColor_000000;
    thumbUpLabel.textAlignment = NSTextAlignmentCenter;
    [thumbUpButton addSubview:thumbUpLabel];
    [thumbUpLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/3);
        make.top.mas_equalTo(thumbUpImageView.mas_bottom).mas_offset(5);
        make.left.right.mas_equalTo(thumbUpButton);
    }];

    funsButton = [[UIButton alloc] init];
    funsButton.backgroundColor = [UIColor clearColor];
    [self addSubview:funsButton];
    [funsButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(thumbUpButton);
        make.width.mas_equalTo(thumbUpButton);
        make.left.mas_equalTo(thumbUpButton.mas_right);
        make.top.mas_equalTo(thumbUpButton);
    }];
    
    funsImageView = [[UIImageView alloc] init];
    funsImageView.image = [UIImage imageNamed:@"mine_fensi"];
    [funsButton addSubview:funsImageView];
    [funsImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(17);
        make.width.mas_equalTo(18);
        make.centerX.mas_equalTo(funsButton);
        make.top.mas_equalTo(funsButton).mas_offset(20);
    }];
    
    funsUnreadLabel = [[UILabel alloc] init];
    funsUnreadLabel.hidden = YES;
    funsUnreadLabel.layer.cornerRadius = 8;
    funsUnreadLabel.layer.masksToBounds = YES;
    funsUnreadLabel.font = [UIFont systemFontOfSize:8];
    funsUnreadLabel.backgroundColor = DHPinkColorFF8B8C;
    funsUnreadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    funsUnreadLabel.textAlignment = NSTextAlignmentCenter;
    [funsButton addSubview:funsUnreadLabel];
    [funsUnreadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(16);
        make.top.mas_equalTo(funsImageView).mas_offset(-8);
        make.left.mas_equalTo(funsImageView.mas_right).mas_offset(-5);
    }];

    
    funsLabel = [[UILabel alloc] init];
    funsLabel.text = @"粉丝";
    funsLabel.font = DHSystemFontOfSize_11;
    funsLabel.textColor = FWTextColor_000000;
    funsLabel.textAlignment = NSTextAlignmentCenter;
    [funsButton addSubview:funsLabel];
    [funsLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/3);
        make.top.mas_equalTo(funsImageView.mas_bottom).mas_offset(5);
        make.left.right.mas_equalTo(funsButton);
    }];

    commentButton = [[UIButton alloc] init];
    commentButton.backgroundColor = [UIColor clearColor];
    [self addSubview:commentButton];
    [commentButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(funsButton);
        make.width.mas_equalTo(funsButton);
        make.left.mas_equalTo(funsButton.mas_right);
        make.top.mas_equalTo(funsButton);
    }];
    
    commentImageView = [[UIImageView alloc] init];
    commentImageView.image = [UIImage imageNamed:@"mine_pinglun"];
    [commentButton addSubview:commentImageView];
    [commentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(17);
        make.width.mas_equalTo(18);
        make.centerX.mas_equalTo(commentButton);
        make.top.mas_equalTo(commentButton).mas_offset(20);
    }];
    
    commentUnreadLabel = [[UILabel alloc] init];
    commentUnreadLabel.hidden = YES;
    commentUnreadLabel.layer.cornerRadius = 8;
    commentUnreadLabel.layer.masksToBounds = YES;
    commentUnreadLabel.font = [UIFont systemFontOfSize:8];
    commentUnreadLabel.backgroundColor = DHPinkColorFF8B8C;
    commentUnreadLabel.textAlignment = NSTextAlignmentCenter;
    commentUnreadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    [commentButton addSubview:commentUnreadLabel];
    [commentUnreadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(16);
        make.top.mas_equalTo(commentImageView).mas_offset(-8);
        make.left.mas_equalTo(commentImageView.mas_right).mas_offset(-5);
    }];
    
    commentLabel = [[UILabel alloc] init];
    commentLabel.text = @"评论";
    commentLabel.font = DHSystemFontOfSize_11;
    commentLabel.textColor = FWTextColor_000000;
    commentLabel.textAlignment = NSTextAlignmentCenter;
    [commentButton addSubview:commentLabel];
    [commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/3);
        make.top.mas_equalTo(commentImageView.mas_bottom).mas_offset(5);
        make.left.right.mas_equalTo(commentButton);
    }];

    honView = [[UIView alloc] init];
    honView.backgroundColor = FWBgColor_Lightgray_F7F8FA;
    [self addSubview:honView];
    [honView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 10));
        make.top.mas_equalTo(commentButton.mas_bottom).mas_offset(16);
        make.bottom.mas_equalTo(self);
    }];
}

#pragma mark - > 显示未读数
- (void)viewConfigureFor:(id)model{
    
    FWMessageHomeModel * messageModel = (FWMessageHomeModel *)model;
    
    if ([[messageModel.unread_like_count description] integerValue] >0 ) {
        thumbUpUnreadLabel.hidden = NO;
        
        NSString * unread_like_count = messageModel.unread_like_count;
        if ([unread_like_count integerValue]>99) {
            unread_like_count = @"99+";
        }
        thumbUpUnreadLabel.text = unread_like_count;
    }else{
        thumbUpUnreadLabel.hidden = YES;
    }
    
    if ([[messageModel.unread_follow_count description] integerValue] >0 ) {
        funsUnreadLabel.hidden = NO;
        
        NSString * unread_follow_count = messageModel.unread_follow_count;
        if ([unread_follow_count integerValue]>99) {
            unread_follow_count = @"99+";
        }
        funsUnreadLabel.text = unread_follow_count;
    }else{
        funsUnreadLabel.hidden = YES;
    }
    
    if ([[messageModel.unread_comment_count description] integerValue] >0 ) {
        commentUnreadLabel.hidden = NO;
        
        NSString * unread_comment_count = messageModel.unread_comment_count;
        if ([unread_comment_count integerValue]>99) {
            unread_comment_count = @"99+";
        }
        commentUnreadLabel.text = unread_comment_count;
    }else{
        commentUnreadLabel.hidden = YES;
    }
}

#pragma mark - > 初始化所有按钮的点击事件，并把控制器传进来
- (void)initClickMethodWithController:(UIViewController *)delegate{
    
    self.viewController = delegate;

    [thumbUpButton addTarget:self action:@selector(thumbUpButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [commentButton addTarget:self action:@selector(commentButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [funsButton addTarget:self action:@selector(funsButtonClick) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - > 粉丝
- (void)funsButtonClick{
    
    FWFunsViewController * FVC = [[FWFunsViewController alloc] init];
    FVC.funsType = @"1";
    FVC.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
    [self.viewController.navigationController pushViewController:FVC animated:YES];
}

#pragma mark - > 点赞
- (void)thumbUpButtonClick{
    
    FWThumbUpViewController * TUVC = [[FWThumbUpViewController alloc] init];
    TUVC.listType = FWMessageThumbUpList;
    [self.viewController.navigationController pushViewController:TUVC animated:YES];
}

#pragma mark - > 评论
- (void)commentButtonClick{
    
    FWThumbUpViewController * TUVC = [[FWThumbUpViewController alloc] init];
    TUVC.listType = FWMessageCommentList;
    [self.viewController.navigationController pushViewController:TUVC animated:YES];
}

@end
