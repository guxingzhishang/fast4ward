//
//  FWMessageCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWListCell.h"

@interface FWMessageCell : UITableViewCell

@property (nonatomic, strong) UIImageView * iconView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UILabel * badgeLabel;
@property (nonatomic, strong) UIImageView * arrowImageView;

@end
