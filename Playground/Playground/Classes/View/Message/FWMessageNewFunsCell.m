//
//  FWMessageNewFunsCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/12/25.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWMessageNewFunsCell.h"

@interface FWMessageNewFunsCell()

@property (nonatomic, strong) FWUnionMessageModel * messageModel;

@end

@implementation FWMessageNewFunsCell
@synthesize messageModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = FWTextColor_0D0A1B;
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI{
    
    self.photoImageView.layer.cornerRadius = 32;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(64, 64));
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(24);
    }];
    
    self.authenticationImageView.hidden = NO;
    [self.authenticationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.right.mas_equalTo(self.photoImageView).mas_offset(-2);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(0);
    }];
    
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    self.nameLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.8);
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(24);
        make.height.mas_equalTo(22);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-184);
        make.top.mas_equalTo(self.photoImageView).mas_offset(5);
    }];
    
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    self.signLabel.textColor = FWTextColor_969696;
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(self.nameLabel);
        make.width.mas_lessThanOrEqualTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(10);
    }];
    
    self.selectImageView.hidden = YES;
    [self.selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).mas_offset(-24);
        make.height.mas_equalTo(24);
        make.width.mas_equalTo(24);
        make.centerY.mas_equalTo(self.contentView);
    }];
    
    self.lineView.backgroundColor = FWColorWihtAlpha(@"363445", 0.2);
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView);
        make.right.mas_equalTo(self.contentView).mas_offset(-24);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-0.5);
    }];
}

- (void)cellConfigureFor:(id)model{
    
    messageModel = (FWUnionMessageModel *)model;
    
    self.nameLabel.text = messageModel.r_user_info.nickname;
    
    if([messageModel.r_user_info.cert_status isEqualToString:@"2"]) {
        self.nameLabel.textColor = FWTextColor_FFAF3C;
        self.authenticationImageView.hidden = NO;
        self.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        if ([messageModel.r_user_info.merchant_cert_status isEqualToString:@"3"] &&
            ![messageModel.r_user_info.cert_status isEqualToString:@"2"]){
            self.nameLabel.textColor = FWTextColor_2B98FA;
            self.authenticationImageView.hidden = YES;
        }else{
            self.authenticationImageView.hidden = YES;
            self.nameLabel.textColor = FWColorWihtAlpha(@"FFFFFF", 0.8);
        }
    }
    
    self.signLabel.text = messageModel.msg_text;

    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:messageModel.r_user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    self.selectImageView.image = [UIImage imageNamed:@"message_attention"];
    self.selectImageView.image = [UIImage imageNamed:@"message_unattention"];

}

- (void)photoImageViewTapClick{
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = messageModel.r_uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

@end
