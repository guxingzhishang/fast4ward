//
//  FWMessageHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FWMessageHeaderView : UIView

/**
 * 粉丝按钮
 */
@property (nonatomic, strong) UIButton * funsButton;

/**
 * 粉丝图标
 */
@property (nonatomic, strong) UIImageView * funsImageView;

/**
 * 粉丝
 */
@property (nonatomic, strong) UILabel * funsLabel;

/**
 * 粉丝未读数
 */
@property (nonatomic, strong) UILabel * funsUnreadLabel;

/**
 * 点赞按钮
 */
@property (nonatomic, strong) UIButton * thumbUpButton;

/**
 * 点赞图标
 */
@property (nonatomic, strong) UIImageView * thumbUpImageView;

/**
 * 点赞
 */
@property (nonatomic, strong) UILabel * thumbUpLabel;

/**
 * 点赞未读数
 */
@property (nonatomic, strong) UILabel * thumbUpUnreadLabel;

/**
 * 关注按钮
 */
@property (nonatomic, strong) UIButton * commentButton;

/**
 * 关注图标
 */
@property (nonatomic, strong) UIImageView * commentImageView;

/**
 * 关注文案
 */
@property (nonatomic, strong) UILabel * commentLabel;

/**
 * 关注未读数
 */
@property (nonatomic, strong) UILabel * commentUnreadLabel;

/**
 * 灰色横线
 */
@property (nonatomic, strong) UIView * honView;


@property (nonatomic, weak) UIViewController * viewController;

- (void)initClickMethodWithController:(UIViewController *)delegate;

- (void)viewConfigureFor:(id)model;
@end
