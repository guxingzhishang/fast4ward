//
//  FWMessageOfficialCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWListCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMessageOfficialCell : FWListCell

/**
 * 时间
 */
@property (nonatomic, strong) UILabel * timeLabel;

/**
 * 未读
 */
@property (nonatomic, strong) UIImageView * unreadView;

@property (nonatomic, strong) NSString * messageType;


@end

NS_ASSUME_NONNULL_END
