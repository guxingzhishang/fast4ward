//
//  FWLiveADView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWLiveADView : UIView

@property (nonatomic, strong) UIImageView * adImageView;
@property (nonatomic, strong) UIButton * closeView;

@property (nonatomic, weak) UIViewController * vc;

@end

NS_ASSUME_NONNULL_END
