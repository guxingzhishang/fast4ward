//
//  FWBaseAutoTextView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/6.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWBaseAutoTextView.h"


@interface FWBaseAutoTextView ()<UITextViewDelegate, UIGestureRecognizerDelegate>

@end

@implementation FWBaseAutoTextView

- (instancetype)init {
    self = [super init];
    if(self) {
        self.frame = SCREEN_FRAME;
        self.backgroundColor = ColorClear;
        self.userInteractionEnabled = YES;
        
        self.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
        self.layer.shadowOffset = CGSizeMake(0, -3);
        self.layer.shadowOpacity = 0.4;
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGuesture:)]];
        
        
        
        if (SCREEN_HEIGHT >= 812) {
            _commentViewBgHeight = 40;
        }else{
            _commentViewBgHeight = 20;
        }
        _container = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 50 - _commentViewBgHeight, SCREEN_WIDTH, 65 + _commentViewBgHeight)];
        _container.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self addSubview:_container];
        
        _sendButton = [[UIButton alloc] init];
        _sendButton.layer.cornerRadius= 2;
        _sendButton.layer.masksToBounds = YES;
        _sendButton.frame = CGRectMake(SCREEN_WIDTH-55-10, 15, 55, 28);
        _sendButton.titleLabel.font = DHSystemFontOfSize_14;
        _sendButton.backgroundColor = FWTextColor_222222;
        [_sendButton setTitle:@"发送" forState:UIControlStateNormal];
        [_sendButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        [_sendButton addTarget:self action:@selector(sendButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_container addSubview:_sendButton];
        
        _bulletSwitch = [[UISwitch alloc] init];
        _bulletSwitch.frame = CGRectMake(SCREEN_WIDTH-55-40, 15, 20, 20);
        _bulletSwitch.transform = CGAffineTransformMakeScale(0.8, 0.8);//缩放
        _bulletSwitch.on = YES;
        _bulletSwitch.onTintColor = FWOrange;
        [_bulletSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:(UIControlEventValueChanged)];
//        [_container addSubview:_bulletSwitch];
        
        _keyboardHeight = _commentViewBgHeight;
        
        _textView = [[IQTextView alloc] initWithFrame:CGRectMake(15, 12, CGRectGetMinX(_sendButton.frame)-30, 36)];
        _textView.placeholder = @"吐个槽呗~";
        _textView.textColor = FWTextColor_000000;
        _textView.font = SmallFont;
        _textView.returnKeyType = UIReturnKeySend;
        _textView.layer.masksToBounds = YES;
        _textView.textContainerInset = UIEdgeInsetsMake(10, LEFT_INSET, 0,LEFT_INSET);
        _textView.backgroundColor = [UIColor colorWithHexString:@"E2E2E2" alpha:1];
        _textHeight = ceilf(_textView.font.lineHeight);
        [_container addSubview:_textView];
        
        _textView.delegate = self;
       
    }
    return self;
}


//handle guesture tap
- (void)handleGuesture:(UITapGestureRecognizer *)sender {
    CGPoint point = [sender locationInView:_textView];
    if(![_textView.layer containsPoint:point]) {
        [_textView resignFirstResponder];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    if(hitView == self){
        if(hitView.backgroundColor == ColorClear) {
            return nil;
        }
    }
    return hitView;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - > switch开关
- (void)switchValueChanged:(UISwitch *)sender{
    
}

#pragma mark - > 分享
- (void)shareButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(shareMatchPost)]) {
        [self.delegate shareMatchPost];
    }
}

#pragma mark - > 发送
- (void)sendButtonClick{
    
    if ([self.delegate respondsToSelector:@selector(sendMessage)]) {
        [self.delegate sendMessage];
    }
}
@end
