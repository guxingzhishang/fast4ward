//
//  FWSelectBarView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 用来承载选择collectionview的图标
 */

#import <UIKit/UIKit.h>

@interface FWSelectBarView : UIView


/**
 * 灰色横线
 */
@property (nonatomic, strong) UIView * honOneView;

/**
 * 灰色横线
 */
@property (nonatomic, strong) UIView * honTwoView;

/**
 * 视频按钮
 */
@property (nonatomic, strong) UIButton * firstButton;

/**
 * 草稿箱按钮/座驾按钮
 */
@property (nonatomic, strong) UIButton * thirdButton;

/**
 * 草稿箱按钮
 */
@property (nonatomic, strong) UIButton * fourthButton;


/**
 * 图片按钮
 */
@property (nonatomic, strong) UIButton * secondButton;

/**
 * 光标
 */
@property (nonatomic, strong) UIView * shadowView;

/**
 * 有几个图标
 */
@property (nonatomic, assign) NSInteger numType;

/* 标题 */
@property (nonatomic, strong) NSArray * titleArray;


- (void)setTabNmuber:(FWMineModel *)model;

@end
