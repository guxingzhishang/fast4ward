//
//  FWVideoPlayerControlView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/13.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 视频播放，控制层(slider/封面/单双击)
 */
#import <UIKit/UIKit.h>
#import "ZFPlayer.h"
#import "ZFSliderView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWVideoPlayerControlViewDelegate <NSObject>

- (void)controlViewRequestLike;

@end

@interface FWVideoPlayerControlView : UIView<ZFPlayerMediaControl>

@property (nonatomic, strong) ZFSliderView *sliderView;
@property (nonatomic, weak) id<FWVideoPlayerControlViewDelegate>delegate;


- (void)resetControlView;

//- (void)showCoverViewWithUrl:(NSString *)coverUrl;

- (void)showCoverViewWithUrl:(NSString *)coverUrl withImageMode:(UIViewContentMode)contentMode;

@end

NS_ASSUME_NONNULL_END
