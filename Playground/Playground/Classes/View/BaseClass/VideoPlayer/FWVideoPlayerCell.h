//
//  FWVideoPlayerCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/13.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWVideoPlayerCellDelegate <NSObject>

- (void)changeDirection:(BOOL)isHen;
- (void)cellDoubleTapRequestLike;
- (void)cellRequestLike;
@end

NS_ASSUME_NONNULL_BEGIN

@interface FWVideoPlayerCell : UITableViewCell

/* 点赞三件套 */
@property (nonatomic, strong) UIButton *favoriteButton;
@property (nonatomic, strong) UIImageView *favorite;
@property (nonatomic, strong) UILabel *favoriteNum;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, strong) FWFeedListModel *data;
@property (nonatomic, strong) RACSubject *payAttentionSignal;

@property (nonatomic, weak) id<FWVideoPlayerCellDelegate>delegate;

- (void)updateUI;

@end

NS_ASSUME_NONNULL_END
