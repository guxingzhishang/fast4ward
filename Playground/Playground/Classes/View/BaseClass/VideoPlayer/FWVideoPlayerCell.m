//
//  FWVideoPlayerCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/13.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVideoPlayerCell.h"

#import "UIImageView+ZFCache.h"
#import "UIView+ZFFrame.h"
#import "UIImageView+ZFCache.h"
#import "ZFUtilities.h"
#import "ZFLoadingView.h"

#import "HoverTextView.h"
#import "CommentsPopView.h"
#import "SharePopView.h"
#import "FWLikeRequest.h"

#define LIKE_BEFORE_TAP_ACTION 1000
#define LIKE_AFTER_TAP_ACTION 2000
#define COMMENT_TAP_ACTION 3000
#define SHARE_TAP_ACTION 4000
#define Like_TAP_ACTION 5000
#define DIRECTION_TAP_ACTION 6000

@interface FWVideoPlayerCell ()<SendTextDelegate, HoverTextViewDelegate, UIGestureRecognizerDelegate,CommentsPopViewDelegate>

@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic, strong) UIImageView *coverImageView;
@property (nonatomic, strong) HoverTextView    *hoverTextView;
@property (nonatomic, strong) UITapGestureRecognizer   *singleTapGesture;
@property (nonatomic, assign) NSTimeInterval           lastTapTime;
@property (nonatomic, assign) CGPoint                  lastTapPoint;

/* 旋转方向的按钮 */
@property (nonatomic, strong) UIButton *directionButton;
/* 详情视图，承载头像、昵称、标签组、关注、描述 */
@property (nonatomic, strong) UIView *detailView;
/* 底部视图，承载点赞、评论、分享 */
@property (nonatomic, strong) UIView *bottomView;

/* 评论三件套 */
@property (nonatomic, strong) UIButton *commentButton;
@property (nonatomic, strong) UIImageView *comment;
@property (nonatomic, strong) UILabel *commentNum;
/* 分享三件套 */
@property (nonatomic, strong) UIButton *shareButton;
@property (nonatomic, strong) UIImageView *share;
@property (nonatomic, strong) UILabel *shareNum;
/* 承载标签的视图 */
@property (nonatomic, strong) UIView *containerView;
/* 商家店铺 */
@property (nonatomic, strong) FWModuleButton *shopButton;
/* 头像 */
@property (nonatomic, strong) UIImageView *avatar;
/* 姓名 */
@property (nonatomic, strong) UILabel *nickName;
/* vip图标 */
@property (nonatomic, strong) UIButton *vipImageButton;
/* 关注 */
@property (nonatomic, strong) UIButton *focus;
/* 描述 */
@property (nonatomic, strong) UITextView *desc;
/* 展示全部描述 */
@property (nonatomic, strong) UIButton *showAllButton;

@end

@implementation FWVideoPlayerCell

- (RACSubject *)payAttentionSignal {
    if (!_payAttentionSignal) {
        _payAttentionSignal = [RACSubject subject];
    }
    return _payAttentionSignal;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = FWTextColor_000000;
        
        self.lastTapTime = 0;
        self.lastTapPoint = CGPointZero;

        [self.contentView addSubview:self.coverImageView];
        /* 暂时屏蔽方向按钮 */
//        [self.contentView addSubview:self.directionButton];
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.detailView];
        
        [self.bottomView addSubview:self.favoriteButton];
        [self.favoriteButton addSubview:self.favorite];
        [self.favoriteButton addSubview:self.favoriteNum];

        [self.bottomView addSubview:self.commentButton];
        [self.commentButton addSubview:self.comment];
        [self.commentButton addSubview:self.commentNum];

        [self.bottomView addSubview:self.shareButton];
        [self.shareButton addSubview:self.shareNum];
        [self.shareButton addSubview:self.share];

        [self.detailView addSubview:self.containerView];
        [self.detailView addSubview:self.showAllButton];
        [self.detailView addSubview:self.desc];
        [self.detailView addSubview:self.nickName];
        [self.detailView addSubview:self.vipImageButton];
        [self.detailView addSubview:self.avatar];
        [self.detailView addSubview:self.focus];
        [self.detailView addSubview:self.shopButton];
        
        UITapGestureRecognizer * singleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
        [self.contentView addGestureRecognizer:singleTapGesture];
    }
    return self;
}

#pragma mark - > 手势 gesture
- (void)handleGesture:(UITapGestureRecognizer *)sender {
    
    switch (sender.view.tag) {
        case 0:
        default: {
            //获取点击坐标，用于设置爱心显示位置
            CGPoint point = [sender locationInView:self.contentView];
            //获取当前时间
            NSTimeInterval time = [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970];
            //判断当前点击时间与上次点击时间的时间间隔
            if(time - self.lastTapTime > 0.25f) {
                //推迟0.25秒执行单击方法
                [self performSelector:@selector(singleTapAction) withObject:nil afterDelay:0.25f];
            }else {
                //取消执行单击方法
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(singleTapAction) object: nil];
                //执行连击显示爱心的方法
                [self showLikeViewAnim:point oldPoint:self.lastTapPoint];
            }
            //更新上一次点击位置
            self.lastTapPoint = point;
            //更新上一次点击时间
            self.lastTapTime =  time;
            break;
        }
    }
}

- (void)singleTapAction {
    if([self.hoverTextView isFirstResponder]) {
        [self.hoverTextView resignFirstResponder];
    }
}

//连击爱心动画
- (void)showLikeViewAnim:(CGPoint)newPoint oldPoint:(CGPoint)oldPoint {

    if ([self.delegate respondsToSelector:@selector(cellDoubleTapRequestLike)]) {
        [self.delegate cellDoubleTapRequestLike];
    }
    
    UIImageView *likeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_home_like_after"]];
    CGFloat k = ((oldPoint.y - newPoint.y)/(oldPoint.x - newPoint.x));
    k = fabs(k) < 0.5 ? k : (k > 0 ? 0.5f : -0.5f);
    CGFloat angle = M_PI_4 * -k;
    likeImageView.frame = CGRectMake(newPoint.x, newPoint.y, 80, 80);
    likeImageView.transform = CGAffineTransformScale(CGAffineTransformMakeRotation(angle), 0.8f, 1.8f);
    
    [self.contentView addSubview:likeImageView];
    [self.contentView bringSubviewToFront:likeImageView];
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
         usingSpringWithDamping:0.5f
          initialSpringVelocity:1.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         likeImageView.transform = CGAffineTransformScale(CGAffineTransformMakeRotation(angle), 1.0f, 1.0f);
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.5f
                                               delay:0.5f
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              likeImageView.transform = CGAffineTransformScale(CGAffineTransformMakeRotation(angle), 3.0f, 3.0f);
                                              likeImageView.alpha = 0.0f;
                                          }
                                          completion:^(BOOL finished) {
                                              [likeImageView removeFromSuperview];
                                          }];
                     }];
}

#pragma mark - > 展示数据
- (void)setData:(FWFeedListModel *)data {
    
    _data = data;
    [self.coverImageView setImageWithURLString:data.feed_cover placeholder:[UIImage imageNamed:@"loading_bgView"]];
    
    if ([self.data.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        // 自己不显示关注
        self.focus.hidden = YES;
    }else{
        self.focus.hidden = NO;
    }
    
    // 默认都让边成竖屏
    self.data.is_horizontal = YES;
    [self changeDirection:self.data];
    
    [self.nickName setText:[NSString stringWithFormat:@"%@", data.user_info.nickname]];
    [self.desc setText:data.feed_title];

    [self.favoriteNum setText:data.count_realtime.like_count_format];
    [self.commentNum setText:data.count_realtime.comment_count];
    [self.shareNum setText:data.count_realtime.share_count];
    
    self.vipImageButton.enabled = YES;
    if ([self.data.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([self.data.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([self.data.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString:data.user_info.header_url] placeholderImage:[UIImage imageNamed:@"img_find_default"]];
    
    NSMutableArray * attentionArray = [GFStaticData getObjectForKey:Attention_Feed_id];
    if (attentionArray.count > 0) {
        if ([attentionArray containsObject:self.data.uid]) {
            data.is_followed = @"1";
        }else {
            data.is_followed = @"2";
        }
    }
    
    // 关注
    if ([data.is_followed isEqualToString:@"2"]) {
        self.focus.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.5);
        [self.focus setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        [self.focus setTitle:@"关注" forState:UIControlStateNormal];
    }else{
        self.focus.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.3);
        [self.focus setTitleColor:FWColorWihtAlpha(@"ffffff", 0.5) forState:UIControlStateNormal];
        [self.focus setTitle:@"已关注" forState:UIControlStateNormal];
    }
    
    /* 有商品 */
    if (![data.goods_id isEqualToString:@"0"] && data.goods_id.length > 0) {
        
        self.shopButton.hidden = NO;
        self.shopButton.iconImageView.image = [UIImage imageNamed:@"videoplayer_goods"];
        [self.shopButton setModuleTitle:data.goods_info.title_short];
    }else{
        self.shopButton.hidden = YES;
    }
    
    // 是否点赞
    if ([data.is_liked isEqualToString:@"2"]) {
        self.favorite.image = [UIImage imageNamed:@"home_unlike"];
    }else if ([data.is_liked isEqualToString:@"1"]){
        self.favorite.image = [UIImage imageNamed:@"home_like"];
    }
    
    [self refreshContainer:self.containerView WithArray:data.tags];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:self.desc.attributedText];
    
    CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 40].height;
    
    if (textHeight <= 60) {
        self.showAllButton.hidden = YES;
    }else{
        self.showAllButton.hidden = NO;
    }
    
    //展示文字或隐藏文字
    if (data.isShow) {
        self.desc.scrollEnabled = YES;
        
        if (textHeight >= SCREEN_HEIGHT/2 -100) {
            textHeight =  SCREEN_HEIGHT/2 -100;
        }
        
        [self.showAllButton setImage:[UIImage imageNamed:@"show_up"] forState:UIControlStateNormal];
    }else{
        self.desc.scrollEnabled = NO;
        if (self.desc.text.length <=0 ) {
            textHeight = 1;
        }else{
            
            if (textHeight<=20 && textHeight>14) {
                textHeight = ceilf(self.desc.font.lineHeight);
            }else if (textHeight > 20 &&textHeight<=40) {
                textHeight = 2*ceilf(self.desc.font.lineHeight);
            }else if (textHeight > 40){
                textHeight = 3*ceilf(self.desc.font.lineHeight);
            }
        }
        
        [self.showAllButton setImage:[UIImage imageNamed:@"show_down"] forState:UIControlStateNormal];
    }
    [self.desc mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.favorite);
        make.bottom.mas_equalTo(self.containerView.mas_top).mas_equalTo(-5);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-24, textHeight));
    }];
}

- (void) refreshContainer:(UIView *)container WithArray:(NSArray *)array{
    
    for (UIView * view in container.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    CGFloat x = 0;
    CGFloat y = 5;
    CGFloat width = 0.0;
    CGFloat height = 24;
    
    for (int i = 0; i < array.count; i++) {
        
        FWSearchTagsListModel * tagsModel = array[i];
        
        width = [tagsModel.tag_name length]*14+30;
        
        if (x + width+20 >= SCREEN_WIDTH) {
            x = 0;
            y += height + 10;
        }
        
        UIButton * btn = [[UIButton alloc] init];
        btn.titleLabel.font = DHSystemFontOfSize_14;
        btn.backgroundColor = [UIColor colorWithHexString:@"404041" alpha:0.4];
        btn.tag = 30000+i;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        [btn setTitle:[NSString stringWithFormat:@" %@",tagsModel.tag_name] forState:UIControlStateNormal];
        [btn setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnTagsClick:) forControlEvents:UIControlEventTouchUpInside];
        [container addSubview:btn];
        [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(container).mas_equalTo(x);
            make.top.mas_equalTo(container).mas_equalTo(y);
            make.size.mas_equalTo(CGSizeMake(width, height));
        }];
        
        if (i == array.count-1  && array.count>0) {
            [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(container).mas_equalTo(x);
                make.top.mas_equalTo(container).mas_equalTo(y);
                make.size.mas_equalTo(CGSizeMake(width, height));
                make.bottom.mas_equalTo(container).mas_offset(-10);
            }];
        }
        
        x += width+15;
    }
}

//SendTextDelegate delegate
- (void)onSendText:(NSString *)text {}

//HoverTextViewDelegate delegate
-(void)hoverTextViewStateChange:(BOOL)isHover {
    self.contentView.alpha = isHover ? 0.0f : 1.0f;
}

#pragma mark - > 发送评论（针对未登录用户写的代理）
- (void)sendTextOnClick:(CommentsPopView *)popView{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        [popView dismiss];
    }
    [self checkLogin];
}

#pragma mark - > 给帖子点赞（针对未登录用户写的代理）
- (void)likesOnClick:(CommentsPopView *)popView{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        [popView dismiss];
    }
    [self checkLogin];
}


#pragma mark - > 关闭评论窗口 回传评论数
- (void)closeCommentsPopView:(NSString *)commentsCount{
    
    [self.commentNum setText:commentsCount];
}

#pragma mark - >  关注
- (void)focusOnClick:(UIButton *)sender{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        NSMutableArray * attentionArray = [[GFStaticData getObjectForKey:Attention_Feed_id] mutableCopy];
        
        if (nil == attentionArray) {
            attentionArray = [[NSMutableArray alloc] init];
        }
        
        NSString * action ;
        
        if ([self.data.is_followed isEqualToString:@"2"]) {
            action = Submit_follow_users;
        }else{
            action = Submit_cancel_follow_users;
        }
        
        FWFollowRequest * request = [[FWFollowRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"f_uid":self.data.uid,
                                  };
        
        [request startWithParameters:params WithAction:action  WithDelegate:self.vc completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([action isEqualToString:Submit_follow_users]) {
                    self.data.is_followed = @"1";
                    
                    self.focus.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.3);
                    [self.focus setTitleColor:FWColorWihtAlpha(@"ffffff", 0.5) forState:UIControlStateNormal];
                    [self.focus setTitle:@"已关注" forState:UIControlStateNormal];
                    
                    if (![attentionArray containsObject:self.data.uid]) {
                        [attentionArray addObject:self.data.uid];
                    }
                    [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self.vc];
                }else{
                    
                    if ([attentionArray containsObject:self.data.uid]) {
                        [attentionArray removeObject:self.data.uid];
                    }
                    self.data.is_followed = @"2";
                    
                    self.focus.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.5);
                    [self.focus setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
                    [self.focus setTitle:@"关注" forState:UIControlStateNormal];                }
                [self.payAttentionSignal sendNext:self.data.is_followed];
                [GFStaticData saveObject:attentionArray forKey:Attention_Feed_id];
            }
        }];
    }
}

#pragma mark - > 展示收缩更多文字
- (void)showAllButtonOnClick:(UIButton *)sender{
    
    self.data.isShow = !self.data.isShow;
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:self.desc.attributedText];
    
    CGFloat textHeight = [attributedText multiLineSize:SCREEN_WIDTH - 40].height;
    
    if (textHeight <= 60) {
        self.showAllButton.hidden = YES;
    }else{
        self.showAllButton.hidden = NO;
    }
    
    //展示文字或隐藏文字
    if (self.data.isShow) {
        
        self.desc.scrollEnabled = YES;
        
        if (textHeight >= SCREEN_HEIGHT/2 -100) {
            textHeight =  SCREEN_HEIGHT/2 -100;
        }
        
        [self.showAllButton setImage:[UIImage imageNamed:@"show_up"] forState:UIControlStateNormal];
        self.detailView.backgroundColor = ColorBlackAlpha60;
        
        //        UIBlurEffect *blurEffect =[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        //        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
        //        visualEffectView.frame = self.bounds;
        //        visualEffectView.alpha = 0.9f;
        //        [self.detailView addSubview:visualEffectView];
        //        [self.detailView sendSubviewToBack:visualEffectView];
    }else{
        
        self.desc.scrollEnabled = NO;
        
        if (self.desc.text.length <=0 ) {
            textHeight = 1;
        }else{
            
            if (textHeight<=20 && textHeight>14) {
                textHeight = ceilf(self.desc.font.lineHeight);
            }else if (textHeight > 20 &&textHeight<=40) {
                textHeight = 2*ceilf(self.desc.font.lineHeight);
            }else if (textHeight > 40){
                textHeight = 3*ceilf(self.desc.font.lineHeight);
            }
        }
        self.detailView.backgroundColor = [UIColor clearColor];
        
        [self.showAllButton setImage:[UIImage imageNamed:@"show_down"] forState:UIControlStateNormal];
    }
    [self.desc mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.favorite);
        make.bottom.mas_equalTo(self.containerView.mas_top).mas_equalTo(-5);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-24, textHeight));
    }];
}

#pragma mark - > 点击标签
- (void)btnTagsClick:(UIButton *)sender{
    
    FWSearchTagsSubListModel * tagsModel = self.data.tags[sender.tag-30000];
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.tagsModel = tagsModel;
    TVC.tags_id = tagsModel.tag_id;
    TVC.traceType = 2;
    [self.vc.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > 点击用户头像
- (void)avatarTapClick{
    
    if (nil == self.data.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.data.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 进商品详情
- (void)goToShopClick{
    
    FWGoodsDetailViewController * CSVC = [[FWGoodsDetailViewController alloc] init];
    CSVC.goods_id = self.data.goods_id;
    [self.vc.navigationController pushViewController:CSVC animated:YES];
}

#pragma mark - > 喜欢、评论、转发
- (void)buttonHandleGesture:(UIButton *)sender{
    
    switch (sender.tag) {
        case COMMENT_TAP_ACTION: {
            CommentsPopView *popView = [[CommentsPopView alloc] initWithFeedID:_data.feed_id];
            popView.delegate = self;
            popView.p_comment_id = @"0";
            popView.vc = self.vc;
            
            [popView show];
            
            break;
        }
        case SHARE_TAP_ACTION: {
            
            NSDictionary * params = @{
                                      @"aweme":self.data,
                                      @"type":@"1",
                                      };
            
            SharePopView *popView = [[SharePopView alloc] initWithParams:params];
            popView.viewcontroller = self.viewController;
            popView.aweme = self.data;
            popView.myBlock = ^(FWFeedListModel *dataModel) {};
            [popView show];
            
            break;
        }
        case Like_TAP_ACTION:{
            [self checkLogin];
            
            if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
                if ([self.delegate respondsToSelector:@selector(cellRequestLike)]) {
                    [self.delegate cellRequestLike];
                }
            }
            break;
        }
        case DIRECTION_TAP_ACTION:{
            
            [self changeDirection:self.data];
        }
        default:
            break;
    }
}

#pragma mark - > 更改播放器的方向
- (void)changeDirection:(FWFeedListModel *)model{
    
    if (model.is_horizontal) {
        // 横屏切换到竖屏
        model.is_horizontal = NO;
        
        self.bottomView.hidden = NO;
        self.detailView.hidden = NO;
        
        [self.directionButton setImage:[UIImage imageNamed:@"show_hen"] forState:UIControlStateNormal];
    }else{
        // 竖屏切换成横屏
        model.is_horizontal = YES;
        
        self.bottomView.hidden = YES;
        self.detailView.hidden = YES;
        
        [self.directionButton setImage:[UIImage imageNamed:@"show_ver"] forState:UIControlStateNormal];
    }
    
    if ([self.delegate respondsToSelector:@selector(changeDirection:)]) {
        [self.delegate changeDirection:model.is_horizontal];
    }
}

- (void)checkLogin{
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

- (void)updateUI {
    if ([self.data.is_followed isEqualToString:@"2"]) {
        self.focus.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.5);
        [self.focus setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        [self.focus setTitle:@"关注" forState:UIControlStateNormal];
    }else{
        self.focus.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.3);
        [self.focus setTitleColor:FWColorWihtAlpha(@"ffffff", 0.5) forState:UIControlStateNormal];
        [self.focus setTitle:@"已关注" forState:UIControlStateNormal];
    }
}

#pragma mark - > 各控件懒加载
- (UIButton *)directionButton{
    if (!_directionButton) {
        _directionButton = [UIButton new];
    [_directionButton setImage:[UIImage imageNamed:@"show_hen"] forState:UIControlStateNormal];
    _directionButton.tag = DIRECTION_TAP_ACTION;
    [_directionButton addTarget:self action:@selector(buttonHandleGesture:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _directionButton;
}

- (UIView *)detailView{
    if (!_detailView) {
        _detailView = [UIView new];
        _detailView.backgroundColor = FWClearColor;
    }
    return _detailView;
}

- (UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [UIView new];
        if (@available(iOS 13.0, *)) {
            // 13以上，底部不显示透明
            _bottomView.backgroundColor = [UIColor colorWithHexString:@"222222"];
        }else{
            _bottomView.backgroundColor = [UIColor colorWithHexString:@"404041" alpha:0.3];
        }
    }
    return _bottomView;
}

- (UIButton *)favoriteButton {
    if (!_favoriteButton) {
        _favoriteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _favoriteButton.tag = Like_TAP_ACTION;
        [_favoriteButton addTarget:self action:@selector(buttonHandleGesture:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _favoriteButton;
}

- (UIImageView *)favorite{
    if (!_favorite) {
        _favorite = [UIImageView new];
        _favorite.image = [UIImage imageNamed:@"home_unlike"];
    }
    
    return _favorite;
}

- (UILabel *)favoriteNum {
    if (!_favoriteNum) {
        _favoriteNum = [UILabel new];
        _favoriteNum.text = @"0";
        _favoriteNum.textColor = FWTextColor_9C9C9C;
        _favoriteNum.font = DHSystemFontOfSize_12;
    }
    return _favoriteNum;
}

- (UIButton *)commentButton {
    if (!_commentButton) {
        _commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _commentButton.tag = COMMENT_TAP_ACTION;
        _commentButton.contentMode = UIViewContentModeCenter;
        [_commentButton addTarget:self action:@selector(buttonHandleGesture:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commentButton;
}

- (UIImageView *)comment{
    if (!_comment) {
        _comment = [UIImageView new];
        _comment.contentMode = UIViewContentModeCenter;
        _comment.image = [UIImage imageNamed:@"pinglun"];
    }
    
    return _comment;
}

- (UILabel *)commentNum {
    if (!_commentNum) {
        _commentNum = [UILabel new];
        _commentNum.text = @"0";
        [_commentNum sizeToFit];
        _commentNum.textColor = FWTextColor_9C9C9C;
        _commentNum.font = DHSystemFontOfSize_12;
    }
    return _commentNum;
}

- (UIButton *)shareButton {
    if (!_shareButton) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareButton.tag = SHARE_TAP_ACTION;
        [_shareButton addTarget:self action:@selector(buttonHandleGesture:) forControlEvents:UIControlEventTouchUpInside];
        _shareButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    return _shareButton;
}

- (UIImageView *)share{
    if (!_share) {
        _share = [UIImageView new];
        _share.contentMode = UIViewContentModeCenter;
        _share.image = [UIImage imageNamed:@"home_share"];
    }
    
    return _share;
}

- (UILabel *)shareNum {
    if (!_shareNum) {
        _shareNum = [UILabel new];
        _shareNum.text = @"0";
        [_shareNum sizeToFit];
        _shareNum.textColor = FWTextColor_9C9C9C;
        _shareNum.font = DHSystemFontOfSize_12;
        _shareNum.textAlignment = NSTextAlignmentLeft;
    }
    return _shareNum;
}

- (UIView *)containerView{
    if (!_containerView) {
        _containerView = [UIView new];
    }
    return _containerView;
}

- (UIButton *)showAllButton{
    if (!_showAllButton) {
        _showAllButton = [[UIButton alloc] init];
        [_showAllButton setImage:[UIImage imageNamed:@"show_down"] forState:UIControlStateNormal];
        [_showAllButton addTarget:self action:@selector(showAllButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        _showAllButton.hidden = YES;
    }
    return _showAllButton;
}

- (UIImageView *)avatar{
    if (!_avatar) {
        CGFloat avatarRadius = 18;
        _avatar = [[UIImageView alloc] init];
        _avatar.image = [UIImage imageNamed:@"img_find_default"];
        _avatar.layer.cornerRadius = avatarRadius;
        _avatar.layer.masksToBounds = YES;
        _avatar.userInteractionEnabled = YES;
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTapClick)];
        [_avatar addGestureRecognizer:tap];
    }
    return _avatar;
}

- (UILabel *)nickName {
    if (!_nickName) {
        _nickName = [UILabel new];
        _nickName.textColor = [UIColor whiteColor];
        _nickName.textColor = FWViewBackgroundColor_FFFFFF;
        _nickName.font = DHSystemFontOfSize_15;
    }
    return _nickName;
}

- (UIButton *)vipImageButton{
    if (!_vipImageButton) {
        _vipImageButton = [UIButton new];
        [_vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _vipImageButton;
}

- (UITextView *)desc{
    if (!_desc) {
        _desc = [[UITextView alloc]init];
        _desc.editable = NO;
        _desc.scrollEnabled = NO;
        _desc.font = DHSystemFontOfSize_14;
        _desc.textColor = FWViewBackgroundColor_FFFFFF;
        _desc.backgroundColor = FWClearColor;
        _desc.textContainerInset = UIEdgeInsetsMake(0 , 0, 0, 0);
        _desc.textContainer.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _desc;
}

- (UIButton *)focus{
    if (!_focus) {
        _focus = [UIButton new];
        _focus.titleLabel.font = DHSystemFontOfSize_14;
        _focus.layer.cornerRadius = 2;
        _focus.layer.masksToBounds = YES;
        _focus.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.5);
        [_focus setTitle:@"关注" forState:UIControlStateNormal];
        [_focus setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        [_focus addTarget:self action:@selector(focusOnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _focus;
}

- (FWModuleButton *)shopButton{
    if (!_shopButton) {
        _shopButton = [[FWModuleButton alloc] init];
        _shopButton.backgroundColor = FWColorWihtAlpha(@"000000", 0.3);
        [_shopButton addTarget:self action:@selector(goToShopClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shopButton;
}

- (UIImage *)placeholderImage {
    if (!_placeholderImage) {
        _placeholderImage = [ZFUtilities imageWithColor:[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1] size:CGSizeMake(1, 1)];
    }
    return _placeholderImage;
}

- (UIImageView *)coverImageView {
    if (!_coverImageView) {
        _coverImageView = [[UIImageView alloc] init];
        _coverImageView.userInteractionEnabled = YES;
        _coverImageView.tag = 100;
        _coverImageView.backgroundColor = [UIColor blackColor];
        _coverImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _coverImageView;
}

#pragma mark - > 控件布局
- (void)layoutSubviews {
    [super layoutSubviews];
    self.coverImageView.frame = self.contentView.bounds;
    
    CGFloat bottomHeight = 55;
//    if (DH_isiPhone6Plus) {
//        bottomHeight = 53;
//    }
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.contentView);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(bottomHeight+ SafeAreaBottomHeight);
    }];
    
    [self.favoriteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bottomView).mas_offset(0);
        make.left.mas_equalTo(self.bottomView).mas_offset(0);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(bottomHeight);
    }];
    
    [self.favorite mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.favoriteButton);
        make.left.mas_equalTo(self.favoriteButton).mas_offset(12);
        make.width.mas_equalTo(19);
        make.height.mas_equalTo(16);
    }];
    
    [self.favoriteNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.favorite);
        make.left.mas_equalTo(self.favorite.mas_right).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 20));
    }];
    
    [self.commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.favoriteButton);
        make.centerX.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.favoriteButton.mas_right);
        make.width.mas_equalTo(self.favoriteButton);
        make.height.mas_equalTo(self.favoriteButton);
    }];
    
    
    [self.comment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.commentButton);
        make.centerX.mas_equalTo(self.commentButton).mas_offset(-5);
        make.size.mas_equalTo(CGSizeMake(18, 16));
    }];
    
    [self.commentNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.comment);
        make.left.mas_equalTo(self.comment.mas_right).mas_offset(5);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    
    [self.shareButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.favoriteButton);
        make.right.mas_equalTo(self.bottomView);
        make.width.mas_equalTo(self.favoriteButton);
        make.height.mas_equalTo(self.favoriteButton);
    }];
    
    [self.shareNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.shareButton);
        make.right.mas_equalTo(self.shareButton).mas_offset(-12);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(20);
    }];
    
    [self.share mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.shareButton);
        make.right.mas_equalTo(self.shareNum.mas_left).mas_offset(-5);
        make.width.mas_equalTo(17);
        make.height.mas_equalTo(14);
    }];
    
    [self.detailView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.bottomView.mas_top).mas_offset(-3);
        make.height.mas_greaterThanOrEqualTo(20);
    }];
    
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.detailView).mas_offset(12);
        make.right.mas_equalTo(self.detailView).mas_offset(-12);
        make.bottom.mas_equalTo(self.detailView).mas_offset(-5);
        make.height.mas_greaterThanOrEqualTo(1);
    }];
    
    [self.desc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.favorite);
        make.bottom.mas_equalTo(self.containerView.mas_top).mas_equalTo(-5);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-24);
    }];
    
    [self.showAllButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.desc).mas_offset(10);
        make.top.mas_equalTo(self.desc.mas_bottom).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(40, 30));
    }];
    
    CGFloat avatarRadius = 18;
    [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.desc.mas_top).mas_offset(-12);
        make.left.mas_equalTo(self.desc);
        make.width.height.mas_equalTo(avatarRadius*2);
//        make.top.mas_equalTo(self.detailView).mas_offset(17+28);
    }];
    
    [self.nickName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.avatar.mas_right).mas_offset(10);
        make.centerY.mas_equalTo(self.avatar);
        make.height.mas_equalTo(30);
        make.right.mas_lessThanOrEqualTo(self.detailView).mas_offset(-105);
    }];
    
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nickName.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.nickName);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
    
//    [self.directionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.contentView).mas_offset(22+FWCustomeSafeTop);
//        make.size.mas_equalTo(CGSizeMake(30, 30));
//        make.right.mas_equalTo(self.contentView).mas_offset(-15);
//    }];
    
    [self.focus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.detailView).mas_offset(-12);
        make.centerY.mas_equalTo(self.nickName);
        make.width.mas_equalTo(69);
        make.height.mas_equalTo(30);
    }];
    
    [self.shopButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(28);
        make.left.mas_equalTo(self.avatar);
        make.bottom.mas_equalTo(self.avatar.mas_top).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.detailView).mas_offset(10);
    }];
}
@end
