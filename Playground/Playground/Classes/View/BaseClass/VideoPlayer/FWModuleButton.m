//
//  FWModuleButton.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWModuleButton.h"

@implementation FWModuleButton

- (id)init{
    self = [super init];
    if (self) {
        self.layer.cornerRadius = 2;
        self.layer.masksToBounds = YES;
        
        self.backgroundColor = FWColorWihtAlpha(@"FFFFFF", 0.2);
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.iconImageView = [UIImageView new];
    self.iconImageView.image = [UIImage imageNamed:@"shopping"];
    [self addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(8);
        make.size.mas_equalTo(CGSizeMake(14, 14));
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(4);
        make.height.mas_equalTo(20);
        make.centerY.mas_equalTo(self);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self).mas_offset(-9);
    }];
}

- (void)setModuleTitle:(NSString *)title{
    
    self.nameLabel.text = title;
}

- (void)setModuleImage:(NSString *)imageNamed{
    self.iconImageView.image = [UIImage imageNamed:imageNamed];
}

@end
