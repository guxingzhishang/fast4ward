//
//  FWModuleButton.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWModuleButton : UIButton

@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, assign) CGFloat  CornerRadius;


- (void)setModuleTitle:(NSString *)title;

- (void)setModuleImage:(NSString *)imageNamed;



@end

NS_ASSUME_NONNULL_END
