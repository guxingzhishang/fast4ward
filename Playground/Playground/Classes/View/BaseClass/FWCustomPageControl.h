//
//  FWCustomPageControl.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, FWPageControlContentMode){
    
    FWPageControlContentModeLeft=0,
    FWPageControlContentModeCenter,
    FWPageControlContentModeRight,
};

typedef NS_ENUM(NSInteger, FWPageControlStyle)
{
    /** 默认按照 controlSize 设置的值,如果controlSize未设置 则按照大小为5的小圆点 */
    FWPageControlStyelDefault = 0,
    /** 长条样式 */
    FWPageControlStyelRectangle,
    /** 圆点 + 长条 样式 */
    FWPageControlStyelDotAndRectangle,
    
};


@class FWPageControl;
@protocol FWPageControlDelegate <NSObject>

-(void)FWPageControlClick:(FWPageControl*_Nonnull)pageControl index:(NSInteger)clickIndex;

@end


@interface FWCustomPageControl : UIControl


/** 位置 默认居中 */
@property(nonatomic) FWPageControlContentMode PageControlContentMode;

/** 滚动条样式 默认按照 controlSize 设置的值,如果controlSize未设置 则为大小为5的小圆点 */
@property(nonatomic) FWPageControlStyle PageControlStyle;

@property(nonatomic) NSInteger numberOfPages;          // default is 0
@property(nonatomic) NSInteger currentPage;            // default is 0. value pinned to 0..numberOfPages-1

/** 距离初始位置 间距  默认10 */
@property (nonatomic) CGFloat marginSpacing;
/** 间距 默认3 */
@property (nonatomic) CGFloat controlSpacing;

/** 大小 默认(5,5) 如果设置PageControlStyle,则失效 */
@property (nonatomic) CGSize controlSize;

/** 其他page颜色 */
@property(nullable, nonatomic,strong) UIColor *otherColor;

/** 当前page颜色 */
@property(nullable, nonatomic,strong) UIColor *currentColor;

/** 设置图片 */
@property(nonatomic,strong) UIImage * _Nullable currentBkImg;

@property(nonatomic,weak)id<FWPageControlDelegate> _Nullable delegate;


@end


NS_ASSUME_NONNULL_END
