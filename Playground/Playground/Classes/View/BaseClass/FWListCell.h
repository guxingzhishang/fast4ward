//
//  FWListCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FWListCell : UITableViewCell<UIGestureRecognizerDelegate>

/**
 * 头像
 */
@property (nonatomic, strong) UIImageView * photoImageView;

/**
 * 认证标识
 */
@property (nonatomic, strong) UIImageView * authenticationImageView;

/**
 * 姓名
 */
@property (nonatomic, strong) UILabel * nameLabel;

/**
 * 个性签名
 */
@property (nonatomic, strong) UILabel * signLabel;

/**
 * 选择框
 */
@property (nonatomic, strong) UIImageView * selectImageView;

/**
 * 分割线
 */
@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, weak) UIViewController * vc;


- (void)setupSubViews;

- (void)cellConfigureFor:(id)model;

- (void)photoImageViewTapClick;

@end
