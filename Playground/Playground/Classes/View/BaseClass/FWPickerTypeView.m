//
//  FWPickerTypeView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWPickerTypeView.h"

@interface FWPickerTypeView()

@end

@implementation FWPickerTypeView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.6];
    self.bgViewHeight = 270.f;
    
    self.bgVIew = [[UIView alloc] init];
    self.bgVIew.backgroundColor = [UIColor whiteColor];
    self.bgVIew.frame = CGRectMake(0, SCREEN_HEIGHT-self.bgViewHeight, SCREEN_WIDTH, self.bgViewHeight);
    [self addSubview:self.bgVIew];
    
    self.toolBgView = [[UIView alloc] init];
    self.toolBgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    self.toolBgView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.bgVIew addSubview:self.toolBgView];

    self.cancelBtn= [[UIButton alloc] init];
    self.cancelBtn.frame = CGRectMake(15, 0, 46, 40);
    self.cancelBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelBtn setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(leftBtnResignFirstResponder) forControlEvents:UIControlEventTouchUpInside];
    [self.toolBgView addSubview:self.cancelBtn];
    
    self.sureBtn= [[UIButton alloc] init];
    self.sureBtn.frame = CGRectMake(SCREEN_WIDTH-46-15, 0, 46, 40);
    self.sureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.sureBtn setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [self.sureBtn addTarget:self action:@selector(rightBtnResignFirstResponder) forControlEvents:UIControlEventTouchUpInside];
    [self.toolBgView addSubview:self.sureBtn];
    
    self.lzPickerView = [[UIPickerView alloc] init];
    self.lzPickerView.frame = CGRectMake(0, CGRectGetMaxY(self.toolBgView.frame), SCREEN_WIDTH, 230);
    self.lzPickerView.delegate = self;
    self.lzPickerView.dataSource = self;
    [self.bgVIew addSubview:self.lzPickerView];
    
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    self.firstTitle  = self.oneTitleArray[0];
    self.secondTitle  = self.twoTitleArray[0];
    self.thirdTitle  = self.threeTitleArray[0];
}


- (void)rightBtnResignFirstResponder
{
    NSString * appendString = @"";
    NSInteger firstSelect ;
    NSInteger secondSelect ;
    if (self.pickerType == FWSignlePickerViewType)
    {
        appendString = self.firstTitle;
        firstSelect = [self.lzPickerView selectedRowInComponent:0];
        
        if ([self.delegate respondsToSelector:@selector(selectType:withFirstRow:withSecondRow:)])
        {
            [self.delegate selectType:appendString withFirstRow:firstSelect withSecondRow:0];
        }
    }else if(self.pickerType == FWDoublePickerViewType){
        appendString = [NSString stringWithFormat:@"%@",self.secondTitle];
        
        firstSelect = [self.lzPickerView selectedRowInComponent:0];
        secondSelect = [self.lzPickerView selectedRowInComponent:1];
        
        if ([self.delegate respondsToSelector:@selector(selectType:withFirstRow:withSecondRow:)])
        {
            [self.delegate selectType:appendString withFirstRow:firstSelect withSecondRow:secondSelect];
        }
    }else if(self.pickerType == FWThreePickerViewType){
        
        appendString = [NSString stringWithFormat:@"%@%@%@",self.firstTitle,self.secondTitle,self.thirdTitle];
        firstSelect = [self.lzPickerView selectedRowInComponent:0];
        secondSelect = [self.lzPickerView selectedRowInComponent:1];
    }
    
    
    [self leftBtnResignFirstResponder];
}

/**
 弹出视图
 */
- (void)show
{
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [[[UIApplication sharedApplication].delegate window] addSubview:self];
    
    //动画出现
    CGRect frame = self.bgVIew.frame;
    if (frame.origin.y == SCREEN_HEIGHT) {
        frame.origin.y -= self.bgViewHeight;
        [UIView animateWithDuration:0.3 animations:^{
            self.bgVIew.frame = frame;
        }];
    }
}

/**
 * 移除视图
 */
- (void)leftBtnResignFirstResponder
{
    CGRect selfFrame = self.bgVIew.frame;
    if (selfFrame.origin.y == SCREEN_HEIGHT - self.bgViewHeight) {
        selfFrame.origin.y += self.bgViewHeight;
        [UIView animateWithDuration:0.3 animations:^{
            self.bgVIew.frame = selfFrame;
        }completion:^(BOOL finished) {
            [self removeFromSuperview];
            
            if ([self.delegate respondsToSelector:@selector(cancelShow)]) {
                [self.delegate cancelShow];
            }
        }];
    }
}

/**
 点击空白关闭
 */
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self leftBtnResignFirstResponder];
}


- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    
    if (self.pickerType == FWSignlePickerViewType)
        return 1;
    
    else if(self.pickerType == FWDoublePickerViewType)
        return 2;
    
    else if(self.pickerType == FWThreePickerViewType)
        return 3;
    
    return 0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.pickerType == FWSignlePickerViewType){
        
        return self.oneTitleArray.count;
    }else if(self.pickerType == FWDoublePickerViewType){
        
        if (component == 0){
            return self.oneTitleArray.count;
        }else{
            return self.twoTitleArray.count;
        }
    }else if(self.pickerType == FWThreePickerViewType){
        
        if (component == 0){
            return self.oneTitleArray.count;
        }else if(component == 2){
            return self.twoTitleArray.count;
        }else{
            return self.threeTitleArray.count;
        }
    }
    return 0;
}

#pragma mark - UIPickerViewDelegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (self.pickerType == FWSignlePickerViewType)
        return SCREEN_WIDTH;
    else if(self.pickerType == FWDoublePickerViewType){
        if (component == 0){
            return 100;
        }else{
            return SCREEN_WIDTH -100;
        }
    }else
        return SCREEN_WIDTH/3;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component __TVOS_PROHIBITED {
    
    return 40;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (self.pickerType == FWSignlePickerViewType){
        ((UILabel *)[pickerView.subviews objectAtIndex:1]).backgroundColor = FWViewBackgroundColor_EEEEEE;//显示分隔线
        ((UILabel *)[pickerView.subviews objectAtIndex:2]).backgroundColor = FWViewBackgroundColor_EEEEEE;//显示分隔线
        return [self setTitleFWSignlePickerViewType:row];
    }else if(self.pickerType == FWDoublePickerViewType){
        
        ((UILabel *)[pickerView.subviews objectAtIndex:1]).backgroundColor = FWViewBackgroundColor_EEEEEE;//显示分隔线
        ((UILabel *)[pickerView.subviews objectAtIndex:2]).backgroundColor = FWViewBackgroundColor_EEEEEE;//显示分隔线
        
        if ([self.delegate respondsToSelector:@selector(FWPickerView:titleForRow:component:)]) {
            [self.delegate FWPickerView:pickerView titleForRow:row component:component];
        }
        return [self setTitleFWDoublePickerViewType:component row:row];
    }else if(self.pickerType == FWThreePickerViewType){
    
        if (component == 0)
            
            return self.oneTitleArray[row];
        else if (component == 1)
            
            return self.twoTitleArray[row];
        else
            
            return self.threeTitleArray[row];
    }
    
    return @"";
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = (UILabel *)view;
    if (lbl == nil) {
        lbl = [[UILabel alloc]init];
        //在这里设置字体相关属性
        lbl.font = [UIFont systemFontOfSize:20];
        lbl.textColor = FWTextColor_000000;
        [lbl setTextAlignment:1];
        [lbl setBackgroundColor:[UIColor clearColor]];
    }
    //重新加载lbl的文字内容
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return lbl;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (self.pickerType == FWSignlePickerViewType)
        
        self.firstTitle = self.oneTitleArray[row];
    else if(self.pickerType == FWDoublePickerViewType)
        
        [self setSelectFWDoublePickerViewType:row inComponent:component];
    else if(self.pickerType == FWThreePickerViewType)
        
        [self setSelectFWThreePickerViewType:pickerView row:row inComponent:component];
}

- (NSString*)setTitleFWSignlePickerViewType:(NSInteger)row{
    
    return self.oneTitleArray[row];
}

- (NSString *)setTitleFWDoublePickerViewType:(NSInteger)component row:(NSInteger)row{
    
    if (component == 0){
        
        return  self.firstTitle = self.oneTitleArray[row];
    }else{
        
        return self.secondTitle = self.twoTitleArray[row];
    }
}

- (void)setSelectFWDoublePickerViewType:(NSInteger)row inComponent:(NSInteger)component{

    if (component == 0)
        self.firstTitle = self.oneTitleArray[row];
    else
        self.secondTitle = self.twoTitleArray[row];
}

//MARK: - FWThreePickerViewType
- (void)setSelectFWThreePickerViewType:(UIPickerView *)pickerView row:(NSInteger)row inComponent:(NSInteger)component{
    
    if (component == 0){
        self.firstTitle = self.oneTitleArray[row];
    }else if (component == 1){
        self.secondTitle = self.twoTitleArray[row];
    }else{
        self.thirdTitle = self.threeTitleArray[row];
    }
}

@end
