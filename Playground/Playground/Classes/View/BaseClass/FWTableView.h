//
//  FWTableView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWTableViewEmptyButtonDelegate <NSObject>

- (void)FWTableViewEmptyDataSetDidTapButton;
@end


@interface FWTableView : UITableView

@property (weak, nonatomic) id<FWTableViewEmptyButtonDelegate> emptyButtonDelegate;

// 是否有空白占位图
@property (nonatomic, assign) BOOL isNeedEmptyPlaceHolder;


/**
 空白占位图图片名称
 */
@property (nonatomic, copy)   NSString *emptyPlaceHolderImageName;

/**
 占位图描述
 */
@property (nonatomic, strong) NSMutableAttributedString *emptyDescriptionString;

/**
 占位图按钮文字
 */
@property (nonatomic, strong) NSMutableAttributedString *emptyButtonTitleString;

/**
 占位图按钮背景图片
 */
@property (nonatomic, strong) UIImage *buttonBackgroundImage;

/**
 空白占位图背景色
 */
@property (nonatomic, strong) UIColor *emptyPlaceHolderBackgroundColor;

/**
 空白占位图位置
 */
@property (nonatomic, assign) CGFloat verticalOffset;

/**
 元素间隙
 */
@property (nonatomic, assign) CGFloat spaceHeight;

@end
