//
//  FWLiveADView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWLiveADView.h"

@implementation FWLiveADView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    // 93, 62+7.5
    self.backgroundColor = FWColorWihtAlpha(@"000000", 0.6);
    
    self.adImageView = [[UIImageView alloc] init];
    self.adImageView.userInteractionEnabled = YES;
//    self.adImageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.adImageView.image = [UIImage imageNamed:@"ad_golden"];
    [self addSubview:self.adImageView];
    self.adImageView.frame = CGRectMake(0,FWAdaptive(180)+FWCustomeSafeTop, SCREEN_WIDTH, FWAdaptive(846));
 
    
    self.closeView= [[UIButton alloc] init];
    [self.closeView setImage:[UIImage imageNamed:@"golden_ad_close"] forState:UIControlStateNormal];
    [self addSubview:self.closeView];
    self.closeView.frame = CGRectMake(SCREEN_WIDTH-50-68, FWAdaptive(180)+FWCustomeSafeTop+85, 50, 50);
}

@end
