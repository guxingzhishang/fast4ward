//
//  FWCustomNavigationView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/12/10.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWCustomNavigationView.h"

@implementation FWCustomNavigationView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
   
    self.navigationView = [[UIView alloc] init];
    self.navigationView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    self.navigationView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self addSubview:self.navigationView];
    
    
    self.navigationLabel= [[UILabel alloc] init];
    self.navigationLabel.font =  [UIFont fontWithName:@"PingFang-SC-Bold" size: 19];
    self.navigationLabel.textColor = FWTextColor_000000;
    self.navigationLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigationView addSubview:self.navigationLabel];
    [self.navigationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 40));
        make.bottom.mas_equalTo(self.navigationView).mas_offset(-1);
        make.left.right.mas_equalTo(self.navigationView);
    }];
    
    self.navigationImageView = [[UIImageView alloc] init];
    [self.navigationView addSubview:self.navigationImageView];
    [self.navigationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(136, 15));
        make.centerX.mas_equalTo(self.navigationView);
        make.bottom.mas_equalTo(self.navigationView).mas_offset(-14);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.navigationView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.navigationView);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(self.navigationView.mas_bottom).mas_offset(-0.5);
    }];
    self.lineView.hidden = YES;
}

- (void)setTitle:(NSString *)title{
    
    _title = title;
    self.navigationLabel.text = self.title;
}

@end
