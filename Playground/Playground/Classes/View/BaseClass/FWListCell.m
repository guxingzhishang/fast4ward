//
//  FWListCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWListCell.h"
static NSInteger cellHeight = 43;

@implementation FWListCell
@synthesize lineView;
@synthesize photoImageView;
@synthesize nameLabel;
@synthesize signLabel;
@synthesize selectImageView;
@synthesize authenticationImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews{
    
    self.photoImageView = [UIImageView new];
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.layer.cornerRadius = cellHeight/2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView.mas_left).mas_offset(20);
        make.size.mas_equalTo(CGSizeMake(cellHeight, cellHeight));
    }];
    self.photoImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageViewTapClick)];
    [self.photoImageView addGestureRecognizer:tap];
    
    authenticationImageView = [UIImageView new];
    authenticationImageView.image =  [UIImage imageNamed:@"primary_bussiness"];
    [self addSubview:authenticationImageView];
    [authenticationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(14);
        make.right.mas_equalTo(photoImageView).mas_offset(2);
        make.bottom.mas_equalTo(photoImageView).mas_offset(0);
    }];
    authenticationImageView.hidden = YES;
    
    self.nameLabel = [UILabel new];
    self.nameLabel.numberOfLines = 1;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    self.nameLabel.textColor = FWTextColor_000000;
    self.nameLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.photoImageView).mas_offset(5);
    }];
    
    self.signLabel = [UILabel new];
    self.signLabel.numberOfLines = 1;
    self.signLabel.textAlignment = NSTextAlignmentLeft;
    self.signLabel.textColor = FWTextColor_646464;
    self.signLabel.font = [UIFont systemFontOfSize:12];
    self.signLabel.preferredMaxLayoutWidth = SCREEN_WIDTH - 88;
    [self.contentView addSubview:self.signLabel];
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_greaterThanOrEqualTo(70);
        make.height.mas_equalTo(cellHeight/2);
        make.left.mas_equalTo(nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
    }];

    self.selectImageView = [UIImageView new];
    [self.contentView addSubview:self.selectImageView];
    [self.selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
    [self.contentView addSubview:lineView];
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.selectImageView);
        make.left.mas_equalTo(nameLabel);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-1);
    }];
}


- (void)cellConfigureFor:(id)model{

    nameLabel.text = @"";
    signLabel.text = @"";
    photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    selectImageView.image = [UIImage imageNamed:@"placeholder_photo"];
}

- (void)photoImageViewTapClick{}

@end
