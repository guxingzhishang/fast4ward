//
//  FWBaseCollectionLayout.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseCollectionLayout.h"

@interface FWBaseCollectionLayout()

@property (nonatomic, strong) NSMutableDictionary *attributes;
@property (nonatomic, assign) BOOL isAutuContentSize;

@property (nonatomic, assign) NSInteger contentSizeHeight;
@property (nonatomic, assign) NSInteger contentSizeWidth;

@property (nonatomic, assign) NSInteger leftBottom;
@property (nonatomic, assign) NSInteger rightBottom;

@property (nonatomic, assign) NSInteger itemSizeWidth;
@property (nonatomic, assign) NSInteger itemSizeHeight;
@property (nonatomic, assign) NSInteger itemOriginX;
@property (nonatomic, assign) NSInteger itemOriginY;
@property (nonatomic, assign) NSInteger itemMargin;

@end

@implementation FWBaseCollectionLayout

- (NSMutableDictionary *)attributes {
    if (!_attributes) {
        _attributes = @{}.mutableCopy;
    }
    return _attributes;
}

- (void)prepareLayout {
    
    [super prepareLayout];
    
    [self resetData];
    
    NSInteger count = [self.collectionView numberOfItemsInSection:0];
    for (NSInteger i = 0 ;i < count; i++) {
        
        NSIndexPath* indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        UICollectionViewLayoutAttributes *attribute = [self layoutAttributesForItemAtIndexPath:indexPath];
        [self.attributes setObject:attribute forKey:indexPath];
    }
}

- (void)resetData {
    
    [self.attributes removeAllObjects];
    
    self.contentSizeWidth = self.collectionView.bounds.size.width;
    self.itemSizeWidth = (self.contentSizeWidth - 50) / 2.0;
    self.itemMargin = 10;
    self.leftBottom = 0;
    self.rightBottom = 0;
    self.contentSizeHeight = 0;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    __block NSMutableArray *store = @[].mutableCopy;
    for (NSIndexPath *key in self.attributes.allKeys) {
        UICollectionViewLayoutAttributes *attribute = self.attributes[key];
        if (CGRectIntersectsRect(attribute.frame, rect)) {
            [store addObject:attribute];
        }
    }
    
    return store;
}


- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.attributes[indexPath]) {
        return self.attributes[indexPath];
    }
    
    UICollectionViewLayoutAttributes *current = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    [self.attributes setObject:current forKey:indexPath];
    
    
    // 如果是banner
    // 在这里改
    if (indexPath.row == 0 && self.isShowHeader) {
        // 在这里改
        
        self.leftBottom = MAX(self.leftBottom, self.rightBottom);
        self.itemOriginY = 0;
        self.itemOriginX = 20;
        self.itemSizeWidth = self.contentSizeWidth - 20 * 2;
        self.itemSizeHeight = 141;
        
        self.leftBottom = self.leftBottom + self.itemSizeHeight + self.itemMargin;
        self.rightBottom = self.leftBottom;
        
    } else {
        
        self.itemSizeWidth = (self.contentSizeWidth - 50) / 2.0;
        
        if ([self.delegate respondsToSelector:@selector(FWCollectionLayout:itemHeightForIndexPath:)]) {
            self.itemSizeHeight = [self.delegate FWCollectionLayout:self itemHeightForIndexPath:indexPath];
        }
        
        if (self.leftBottom <= self.rightBottom) {
            self.itemOriginX = 20;
            if (indexPath.row == 0) {
                self.itemOriginY = 0;
            }else{
                if (indexPath.row == 1 && self.isShowHeader) {
                    self.itemOriginY =  self.leftBottom;
                }else{
                    self.itemOriginY = self.leftBottom + self.itemMargin;
                }
            }
            self.leftBottom = self.itemOriginY + self.itemSizeHeight;
        } else {
            
            self.itemOriginX = self.itemSizeWidth + 2 * self.itemMargin+10;
            if (indexPath.row == 1) {
                self.itemOriginY = 0;
            }else{
                if (indexPath.row == 2 && self.isShowHeader) {
                    self.itemOriginY = self.rightBottom;
                }else{
                    self.itemOriginY = self.rightBottom + self.itemMargin;
                }
            }
            self.rightBottom = self.itemOriginY + self.itemSizeHeight;
        }
    }
    current.frame = CGRectMake(self.itemOriginX, self.itemOriginY, self.itemSizeWidth, self.itemSizeHeight);
    self.contentSizeHeight = MAX(self.contentSizeHeight, CGRectGetMaxY(current.frame));
    return current;
}

- (void)autuContentSize {
    
    _isAutuContentSize = YES;
}

- (CGSize)collectionViewContentSize {
    return CGSizeMake(self.contentSizeWidth, self.contentSizeHeight);
}

@end
