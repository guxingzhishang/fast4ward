//
//  FWBlockButton.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/10.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ButtonBlock)(UIButton * sender);

@interface FWBlockButton : UIButton

@property(nonatomic,copy)ButtonBlock block;
- (void)addButtonTapBlock:(ButtonBlock)block;

@end

NS_ASSUME_NONNULL_END
