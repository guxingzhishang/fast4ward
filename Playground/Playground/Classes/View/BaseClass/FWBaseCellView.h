//
//  FWBaseCellView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FWBaseCellView : UIButton

- (instancetype)initWithTitle:(NSString *)title WithImage:(NSString *)imageName;

/**
 * 左侧图标
 */
@property (nonatomic, strong) UIImageView * iconImageView;

/**
 * 右侧箭头
 */
@property (nonatomic, strong) UIImageView * arrowImageView;

/**
 * 标题
 */
@property (nonatomic, strong) UILabel *titleLabel;

/**
 * 右侧副标题
 */
@property (nonatomic, strong) UILabel *rightLabel;

/**
 * 底线
 */
@property (nonatomic, strong) UIView * lineView;

/**
 * 整行作为按钮
 */
@property (nonatomic, strong) UIButton *cellButton;

@end
