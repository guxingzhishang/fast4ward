//
//  FWScrollViewFollowCollectionView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/14.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWScrollViewFollowCollectionView : NSObject

@property (nonatomic,assign)BOOL bounces; //反弹效果默认YES
@property (nonatomic,assign)CGFloat scrollRange;

//利用initRoot创建 给予底部上下滑动的ScrollView 要滑动的ScrollRange 大小和左右滑动的Table数组
//- (instancetype)initRootScrollView:(UIScrollView *)rootScrollView AndRootScrollViewScrollRange:(CGFloat)scrollRange AndTableArray:(NSArray *)tableArray;
- (void)RootScrollView:(UIScrollView *)rootScrollView AndRootScrollViewScrollRange:(CGFloat)scrollRange AndTableArray:(NSArray *)tableArray;

//在scrollViewDidScroll代理里调用此方法传入滑动的scrollView
- (void)followScrollViewScrollScrollViewScroll:(UIScrollView *)scrollView;
@end
