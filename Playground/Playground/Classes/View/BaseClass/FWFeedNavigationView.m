//
//  FWFeedNavigationView.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/2.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWFeedNavigationView.h"

@implementation FWFeedNavigationView
@synthesize blackBackButton;
@synthesize attentionButton;
@synthesize nameLabel;
@synthesize photoImageView;
@synthesize vipImageButton;

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    
    blackBackButton = [[UIButton alloc] init];
    [blackBackButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [blackBackButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:blackBackButton];
    blackBackButton.frame = CGRectMake(10, 30+FWCustomeSafeTop, 30, 25);
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.layer.cornerRadius = 15;
    photoImageView.layer.masksToBounds = YES;
    photoImageView.userInteractionEnabled = YES;
    photoImageView.image = [UIImage imageNamed:@"placeholder"];
    photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoImageView.frame = CGRectMake(CGRectGetMaxX(blackBackButton.frame)+15, 30+FWCustomeSafeTop-2.5, 30, 30);
    [self addSubview:photoImageView];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageTap:)];
    [photoImageView addGestureRecognizer:tap];


    attentionButton = [[UIButton alloc] init];
    attentionButton.layer.cornerRadius = 2;
    attentionButton.layer.masksToBounds = YES;
    attentionButton.titleLabel.font = DHBoldSystemFontOfSize_14;
    attentionButton.backgroundColor = FWTextColor_222222;
    [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [attentionButton addTarget:self action:@selector(attentionButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:attentionButton];
    [attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(blackBackButton);
        make.size.mas_equalTo(CGSizeMake(69, 30));
        make.right.mas_equalTo(self).mas_offset(-14);
    }];

    nameLabel = [[UILabel alloc] init];
    nameLabel.font = DHBoldSystemFontOfSize_14;
    nameLabel.textColor = FWTextColor_000000;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:nameLabel];
    nameLabel.frame = CGRectMake(CGRectGetMaxX(self.photoImageView.frame)+15, CGRectGetMinY(self.photoImageView.frame)+5, SCREEN_WIDTH-170, 20);

    
    self.vipImageButton = [[UIButton alloc] init];
    [self.vipImageButton addTarget:self action:@selector(vipImageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.vipImageButton];
    [self.vipImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.nameLabel);
        make.size.mas_equalTo(CGSizeMake(16, 15));
    }];
}

- (void)configForView:(id)model{
    
    self.listModel = (FWFeedListModel *)model;
    
    if ([self.listModel.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        // 自己不显示关注
        attentionButton.hidden = YES;
    }else{
        attentionButton.hidden = NO;
    }
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"tags_icon"]  options:SDWebImageRefreshCached];

    self.vipImageButton.enabled = YES;
    if ([self.listModel.user_info.user_level isEqualToString:@"1"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_primary"] forState:UIControlStateNormal];
    }else if ([self.listModel.user_info.user_level isEqualToString:@"2"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_middle"] forState:UIControlStateNormal];
    }else if ([self.listModel.user_info.user_level isEqualToString:@"3"]) {
        [self.vipImageButton setImage:[UIImage imageNamed:@"vip_senior"] forState:UIControlStateNormal];
    }else{
        self.vipImageButton.enabled = NO;
        [self.vipImageButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    
    // 关注
    if ([self.listModel.is_followed isEqualToString:@"2"]) {
        attentionButton.backgroundColor = FWTextColor_222222;
        [attentionButton setTitle:@"关注" forState:UIControlStateNormal];
        [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    }else{
        attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
        [attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
        [attentionButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
    }
    
    if ([self.listModel.status_niming isEqualToString:@"2"]) {
        /* 匿名 */
        self.photoImageView.userInteractionEnabled = NO;
        self.photoImageView.image = [UIImage imageNamed:@"tags_icon"];
        self.attentionButton.hidden = YES;
        self.nameLabel.text = @"匿名用户";
    }else{
        self.photoImageView.userInteractionEnabled = YES;
        self.attentionButton.hidden = NO;
        nameLabel.text = self.listModel.user_info.nickname;
        [nameLabel sizeToFit];
    }
}

#pragma mark - > 返回
- (void)backButtonClick{
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 点击头像
- (void)photoImageTap:(UITapGestureRecognizer *)gesture{
    
    if (nil == self.listModel.uid) {
        return;
    }

    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.listModel.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 点击会员图标
- (void)vipImageButtonClick{
    
    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
    if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] isEqualToString:@"0"]) {
        VVC.webTitle = @"肆放会员";
    }else{
        VVC.webTitle = @"会员权益";
    }
    [self.vc.navigationController pushViewController:VVC animated:YES];
}

#pragma mark - > 关注
- (void)attentionButtonOnClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([self.delegate respondsToSelector:@selector(attentionButtonClick)]) {
            [self.delegate attentionButtonClick];
        }
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

@end
