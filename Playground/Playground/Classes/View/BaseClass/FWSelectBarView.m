//
//  FWSelectBarView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWSelectBarView.h"


@implementation FWSelectBarView
@synthesize secondButton;
@synthesize firstButton;
@synthesize thirdButton;
@synthesize honOneView;
@synthesize honTwoView;
@synthesize numType;
@synthesize shadowView;
@synthesize fourthButton;

- (id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    honOneView = [[UIView alloc] init];
    honOneView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:honOneView];

    firstButton = [[UIButton alloc] init];
    firstButton.selected = YES;
    firstButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [firstButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [firstButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:firstButton];
    
    secondButton = [[UIButton alloc] init];
    secondButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [secondButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [secondButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:secondButton];
   
    thirdButton = [[UIButton alloc] init];
    thirdButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [thirdButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [thirdButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:thirdButton];
    
    fourthButton = [[UIButton alloc] init];
    fourthButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14];
    [fourthButton setTitleColor:FWTextColor_BCBCBC forState:UIControlStateNormal];
    [fourthButton setTitleColor:FWTextColor_222222 forState:UIControlStateSelected];
    [self addSubview:fourthButton];
    
    honTwoView = [[UIView alloc] init];
    honTwoView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [self addSubview:honTwoView];
    honTwoView.hidden = YES;
    
    shadowView= [[UIView alloc]init];
    shadowView.backgroundColor = FWTextColor_222222;
    shadowView.layer.masksToBounds = YES;
    shadowView.tag = 100000;
    [self addSubview:shadowView];
}

- (void)setNumType:(NSInteger)numType{
    
    [honOneView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 0.5));
        make.top.mas_equalTo(self).mas_offset(0);
    }];
    
    [honTwoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 0.5));
        make.top.mas_equalTo(honOneView.mas_bottom).mas_offset(49);
    }];
    
    if (numType == 2) {
        
        firstButton.frame = CGRectMake((SCREEN_WIDTH-200-30)/2, CGRectGetMaxY(honOneView.frame)+10, 100, 30);
        
        [firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(100);
            make.left.mas_equalTo((SCREEN_WIDTH-200-30)/2);
            make.top.mas_equalTo(honOneView.mas_bottom).mas_offset(10);
        }];
        [secondButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(firstButton);
            make.width.mas_equalTo(firstButton);
            make.left.mas_equalTo(firstButton.mas_right).mas_offset(30);
            make.top.mas_equalTo(firstButton);
        }];
        
        thirdButton.hidden = YES;
        fourthButton.hidden = YES;
    }else if (numType == 3){
        
        firstButton.frame = CGRectMake((SCREEN_WIDTH-270-60)/2, CGRectGetMaxY(honOneView.frame)+10, 90, 30);

        [firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(90);
            make.left.mas_equalTo((SCREEN_WIDTH-270-60)/2);
            make.top.mas_equalTo(honOneView.mas_bottom).mas_offset(10);
        }];
        [secondButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(firstButton);
            make.width.mas_equalTo(firstButton);
            make.left.mas_equalTo(firstButton.mas_right).mas_offset(30);
            make.top.mas_equalTo(firstButton);
        }];
       
        [thirdButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(secondButton);
            make.left.mas_equalTo(secondButton.mas_right).mas_offset(30);
            make.top.mas_equalTo(secondButton);
        }];
        
        thirdButton.hidden = NO;
        fourthButton.hidden = YES;
    }else if (numType == 4){
        
        firstButton.frame = CGRectMake(10, CGRectGetMaxY(honOneView.frame)+10, (SCREEN_WIDTH-35)/4, 30);
        
        [firstButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.width.mas_equalTo((SCREEN_WIDTH-35)/4);
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(honOneView.mas_bottom).mas_offset(10);
        }];
        [secondButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(firstButton);
            make.width.mas_equalTo(firstButton);
            make.left.mas_equalTo(firstButton.mas_right).mas_offset(5);
            make.top.mas_equalTo(firstButton);
        }];
        
        [thirdButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(secondButton);
            make.left.mas_equalTo(secondButton.mas_right).mas_offset(5);
            make.top.mas_equalTo(secondButton);
        }];
        
        [fourthButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(thirdButton);
            make.left.mas_equalTo(thirdButton.mas_right).mas_offset(5);
            make.top.mas_equalTo(thirdButton);
        }];
        thirdButton.hidden = NO;
        fourthButton.hidden = NO;
    }
    
   
    
    if (firstButton.isSelected) {
        shadowView.frame = CGRectMake(CGRectGetMinX(firstButton.frame)+(CGRectGetWidth(firstButton.frame)-32)/2, CGRectGetMaxY(firstButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if(secondButton.isSelected){
        shadowView.frame = CGRectMake(CGRectGetMinX(secondButton.frame)+(CGRectGetWidth(secondButton.frame)-32)/2, CGRectGetMaxY(secondButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if (thirdButton.isSelected){
        shadowView.frame = CGRectMake(CGRectGetMinX(thirdButton.frame)+(CGRectGetWidth(thirdButton.frame)-32)/2, CGRectGetMaxY(thirdButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if (fourthButton.isSelected){
        shadowView.frame = CGRectMake(CGRectGetMinX(fourthButton.frame)+(CGRectGetWidth(fourthButton.frame)-32)/2, CGRectGetMaxY(fourthButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }
}

- (void)setTabNmuber:(FWMineModel *)model{
    
    NSString * firstTitle;
    NSString * secondTitle;
    NSString * thirdTitle;
    NSString * fourthTitle;

    if (model.feed_count_video.length > 0) {
        firstTitle = [NSString stringWithFormat:@"%@(%@)",self.titleArray[0],model.feed_count_video];
    }else{
        firstTitle = self.titleArray[0];
    }

    if (model.feed_count_image.length > 0) {
        secondTitle = [NSString stringWithFormat:@"%@(%@)",self.titleArray[1],model.feed_count_image];
    }else{
        secondTitle = self.titleArray[1];
    }
    
    NSMutableArray * array = [GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];

    /* 去重 */
    NSArray * tempArr = [NSArray arrayWithArray:array];
    NSSet *set = [NSSet setWithArray:tempArr];
    [array removeAllObjects];
    for (NSString * str in set){
        [array addObject:str];
    }
    
    long draftCount = 0 ;
    
    if (self.titleArray.count > 2 ) {
        
        if ([self.titleArray[2] isEqualToString:@"座驾"] && self.titleArray.count == 3) {
            /* 用户页 */
            thirdTitle= [NSString stringWithFormat:@"%@(%@)",self.titleArray[2],model.car_count];
            
        }else if ([self.titleArray[2] isEqualToString:@"座驾"] && self.titleArray.count == 4) {
            /* 个人页 */
            thirdTitle= [NSString stringWithFormat:@"%@(%@)",self.titleArray[2],model.car_count];
            
            if (array.count<=0 && model.longfeed_draft_count.length <= 0) {
                if (self.titleArray.count > 2) {
                    fourthTitle = [NSString stringWithFormat:@"%@(0)",self.titleArray[3]];
                }
            }else{
                if (model.longfeed_draft_count.length > 0) {
                    draftCount = [model.longfeed_draft_count integerValue];
                }
                if (array.count > 0) {
                    draftCount += array.count;
                }
                
                if (self.titleArray.count > 2) {
                    fourthTitle= [NSString stringWithFormat:@"%@(%ld)",self.titleArray[3],draftCount];
                }
            }
        }else{
            /* 不包含座驾页 */
            if (model.feed_count_image.length > 0) {
                thirdTitle = [NSString stringWithFormat:@"%@(%@)",self.titleArray[2],model.feed_count_image];
            }else{
                thirdTitle = self.titleArray[2];
            }
        }
    }
 
    [firstButton setTitle:firstTitle forState:UIControlStateNormal];
    [firstButton setTitle:[NSString stringWithFormat:@" %@",firstTitle] forState:UIControlStateSelected];
    
    [secondButton setTitle:secondTitle forState:UIControlStateNormal];
    [secondButton setTitle:[NSString stringWithFormat:@" %@",secondTitle] forState:UIControlStateSelected];
    
    [thirdButton setTitle:thirdTitle forState:UIControlStateNormal];
    [thirdButton setTitle:[NSString stringWithFormat:@" %@",thirdTitle] forState:UIControlStateSelected];
    
    [fourthButton setTitle:fourthTitle forState:UIControlStateNormal];
    [fourthButton setTitle:[NSString stringWithFormat:@" %@",fourthTitle] forState:UIControlStateSelected];
}
@end
