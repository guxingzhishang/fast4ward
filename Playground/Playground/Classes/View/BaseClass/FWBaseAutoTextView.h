//
//  FWBaseAutoTextView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/6.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#define LEFT_INSET                 15
#define RIGHT_INSET                60
#define TOP_BOTTOM_INSET           15

NS_ASSUME_NONNULL_BEGIN

@protocol FWBaseAutoTextViewDelegate <NSObject>

- (void)shareMatchPost;
- (void)sendMessage;

@end

@interface FWBaseAutoTextView : UIView

@property (nonatomic, strong) UIView              *container;
@property (nonatomic, strong) IQTextView          *textView;
@property (nonatomic, strong) UISwitch            *bulletSwitch;
@property (nonatomic, strong) UIButton            *sendButton;

@property (nonatomic, assign) CGFloat            textHeight;
@property (nonatomic, assign) CGFloat            keyboardHeight;
@property (nonatomic, assign) CGFloat            commentViewBgHeight;

@property (nonatomic, weak) id<FWBaseAutoTextViewDelegate>    delegate;

@end

NS_ASSUME_NONNULL_END
