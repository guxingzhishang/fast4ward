//
//  FWCollecitonBaseCell.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWCollecitonBaseCell.h"
#import "FWLikeRequest.h"
#import "FWArticalLinkGoodsViewController.h"

@interface FWCollecitonBaseCell()

@end


@implementation FWCollecitonBaseCell
@synthesize icon;

- (instancetype)initWithFrame:(CGRect)frame {

    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.backgroundColor = FWClearColor;

        [self setupSubviews];
    }
    return self;
}


- (void)setupSubviews{
    
    [self setupLayer];

    _iv = [[UIImageView alloc] init];
    _iv.layer.cornerRadius = 2;
    _iv.layer.masksToBounds = YES;
    _iv.backgroundColor = FWColor(@"E7E7E7");
    _iv.contentMode = UIViewContentModeScaleAspectFill;
    _iv.clipsToBounds = YES;
    _iv.userInteractionEnabled = YES;
    [self.contentView addSubview:self.iv];
    [self.iv mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.contentView).mas_offset(2);
    }];
    
    self.activityImageView = [[UIImageView alloc] init];
    self.activityImageView.image = [UIImage imageNamed:@"activity"];
    [self.iv addSubview:self.activityImageView];
    [self.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.iv).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(30, 15));
    }];
    self.activityImageView.hidden = YES;
    
    self.jingpinImageView = [[UIImageView alloc] init];
    self.jingpinImageView.image = [UIImage imageNamed:@"new_jingpin"];
    [self.iv addSubview:self.jingpinImageView];
    [self.jingpinImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.iv).mas_offset(-5);
        make.top.mas_equalTo(self.iv).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(22, 26));
    }];
    self.jingpinImageView.hidden = YES;
    
    self.linkButton = [[UIButton alloc] init];
    [self.linkButton setImage:[UIImage imageNamed:@"guanlianshangpin"] forState:UIControlStateNormal];
    [self.linkButton addTarget:self action:@selector(linkButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.iv addSubview:self.linkButton];
    [self.linkButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.iv);
        make.centerX.mas_equalTo(self.iv);
        make.size.mas_equalTo(CGSizeMake(83, 24));
    }];
    self.linkButton.hidden = YES;
    
    self.siIcon = [[UIImageView alloc] init];
    self.siIcon.backgroundColor = FWColor(@"E7E7E7");
    [self.iv addSubview:self.siIcon];
    self.siIcon.contentMode = UIViewContentModeCenter;
    [self.siIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.iv);
    }];
    
    icon = [[UIImageView alloc] init];
    icon.image = [UIImage imageNamed:@"placeholder_si"];
    [self.siIcon addSubview:icon];
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.iv);
        make.centerY.mas_equalTo(self.iv);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    self.videoIcon = [[UIImageView alloc] init];
    self.videoIcon.image = [UIImage imageNamed:@"icon_video"];
    [self.contentView addSubview:self.videoIcon];
    [self.videoIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.iv);
        make.centerY.mas_equalTo(self.iv);
        make.size.mas_equalTo(CGSizeMake(21, 32));
    }];
    self.videoIcon.hidden = YES;
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.textColor = FWColor(@"dd3333");
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    self.priceLabel.font = DHBoldFont(18);
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(5);
        make.top.mas_equalTo(self.iv.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(self.contentView).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(0.01);
    }];
    
    self.desc = [[UILabel alloc] init];
    self.desc.textColor = FWTextColor_222222;
    self.desc.numberOfLines = 2;
    self.desc.textAlignment = NSTextAlignmentLeft;
    self.desc.lineBreakMode = NSLineBreakByWordWrapping;
    self.desc.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 13];
    [self.contentView addSubview:self.desc];
    [self.desc mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(5);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(self.contentView).mas_offset(-10);
        make.width.mas_equalTo(self.contentView.frame.size.width-15);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.wendaLabel  = [[UILabel alloc] init];
    self.wendaLabel.textColor = DHTitleColor_666666;
    self.wendaLabel.textAlignment = NSTextAlignmentLeft;
    self.wendaLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 12];
    [self.contentView addSubview:self.wendaLabel];
    [self.wendaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(self.desc);
       make.top.mas_equalTo(self.desc.mas_bottom).mas_offset(5);
       make.right.mas_equalTo(self.contentView).mas_offset(-10);
       make.width.mas_equalTo(self.contentView.frame.size.width-20);
       make.height.mas_equalTo(16);
    }];
    self.wendaLabel.hidden = YES;
    
    self.userButton = [[UIButton alloc] init];
    [self.userButton addTarget:self action:@selector(userButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.userButton];
    [self.userButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView);
        make.height.mas_equalTo(35);
        make.width.mas_equalTo((SCREEN_WIDTH-15)/4);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    self.userIcon = [[UIImageView alloc] init];
    self.userIcon.backgroundColor = FWViewBackgroundColor_F1F1F1;
    self.userIcon.layer.cornerRadius = 9;
    self.userIcon.layer.masksToBounds = YES;
    self.userIcon.userInteractionEnabled = NO;
    [self.contentView addSubview:self.userIcon];
    [self.userIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.userButton).mas_offset(11);
        make.left.mas_equalTo(self.iv).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(18, 18));
        make.bottom.mas_equalTo(self.contentView).mas_offset(-10);
    }];
    
    self.authenticationImageView = [[UIImageView alloc] init];
    self.authenticationImageView.image = [UIImage imageNamed:@"primary_bussiness"];
    [self addSubview:self.authenticationImageView];
    [self.authenticationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(10);
        make.right.mas_equalTo(self.userIcon).mas_offset(2);
        make.bottom.mas_equalTo(self.userIcon).mas_offset(2);
    }];
    self.authenticationImageView.hidden = YES;
    
    self.pvLabel = [[UILabel alloc] init];
    self.pvLabel.textColor = FWTextColor_BCBCBC;
    self.pvLabel.textAlignment = NSTextAlignmentRight;
    self.pvLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 11];
    [self.contentView addSubview:self.pvLabel];
    [self.pvLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.userIcon);
        make.width.mas_greaterThanOrEqualTo(5);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(self.contentView).mas_offset(-5);
    }];
    
//    self.pvImageView = [[UIImageView alloc] init];
//    self.pvImageView.image = [UIImage imageNamed:@"card_eye"];
//    [self.contentView addSubview:self.pvImageView];
//    [self.pvImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self.pvLabel.mas_left).mas_offset(-5);
//        make.centerY.mas_equalTo(self.pvLabel);
//        make.size.mas_equalTo(CGSizeMake(16, 11));
//    }];
    
    self.username = [[UILabel alloc] init];
    self.username.textColor = FWColor(@"515151");
    self.username.userInteractionEnabled = NO;
    self.username.textAlignment = NSTextAlignmentLeft;
    self.username.font = [UIFont fontWithName:@"PingFang-SC-Regular" size: 11];
    [self.contentView addSubview:self.username];
    [self.username mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userIcon.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.userIcon);
//        make.width.mas_greaterThanOrEqualTo(50);
        make.height.mas_equalTo(20);
        make.right.mas_lessThanOrEqualTo(self.pvLabel.mas_left).mas_offset(-3);
    }];
}

#pragma mark - > feed流点赞
- (void)likesButtonOnClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.model.feed_id,
                                  @"like_type":self.likeType,
                                  };
        
        [request startWithParameters:params WithAction:Submit_like_feed WithDelegate:self.viewController  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([self.likeType isEqualToString: @"add"]) {
                    self.model.is_liked = @"1";
                    
                    if ([self.model.count_realtime.like_count_format integerValue]||
                        [self.model.count_realtime.like_count_format integerValue] == 0) {
                        self.model.count_realtime.like_count_format = @([self.model.count_realtime.like_count_format integerValue] +1).stringValue;
                    }
                }else if ([self.likeType isEqualToString: @"cancel"]){
                    self.model.is_liked = @"2";
                    
                    if ([self.model.count_realtime.like_count_format integerValue]||
                        [self.model.count_realtime.like_count_format integerValue] == 0) {
                        self.model.count_realtime.like_count_format = @([self.model.count_realtime.like_count_format integerValue] -1).stringValue;
                    }
                }
                [self configForCell:self.model];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewController];
            }
        }];
    }
}

#pragma mark - > 点击头像查看用户
- (void)userButtonOnClick:(UIButton *)sender{
    
    if (nil == self.model.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.model.uid;
    [self.viewController.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 关联商品
- (void)linkButtonOnClick{
    
    FWArticalLinkGoodsViewController * ALGVC = [[FWArticalLinkGoodsViewController alloc] init];
    ALGVC.listModel = self.model;
    [self.viewController.navigationController pushViewController:ALGVC animated:YES];
}

#pragma mark - > 展示
- (void)configForCell:(id)listmodel {
    
    _model = (FWFeedListModel *)listmodel;
    
    [self setupLayer];
    
    self.videoIcon.hidden = NO;

    self.iv.hidden = NO;
    self.activityImageView.hidden = NO;
    self.jingpinImageView.hidden = NO;
    self.userIcon.hidden = NO;
//    self.pvImageView.hidden = NO;
    self.username.hidden = NO;
    self.pvLabel.hidden = NO;
    self.desc.hidden = NO;
    self.userButton.hidden = NO;
    self.authenticationImageView.hidden = YES;

    if (_model.h5_url.length > 0) {
        self.activityImageView.hidden = NO;
    }else{
        self.activityImageView.hidden = YES;
    }
    
    if ([_model.is_good isEqualToString:@"1"]) {
        self.jingpinImageView.hidden = NO;
    }else{
        self.jingpinImageView.hidden = YES;
    }
    
    self.desc.text = self.model.feed_title;
    self.pvLabel.text = [NSString stringWithFormat:@"%@次浏览",self.model.count_realtime.pv_count_format];
    self.username.text = self.model.user_info.nickname;
    
    if ([self.model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        self.wendaLabel.hidden = NO;
        self.wendaLabel.text = [NSString stringWithFormat:@"%@个回答",self.model.count_realtime.comment_count];
     }else{
        self.wendaLabel.hidden = YES;
     }
    
    if ([self.model.feed_type isEqualToString:@"6"] ) {
        /* 闲置贴 */
        self.priceLabel.text = self.model.price_sell_show;
    }else{
        self.priceLabel.text = @"";
    }

    
    if([self.model.user_info.cert_status isEqualToString:@"2"]) {
        self.username.textColor = FWTextColor_FFAF3C;
        self.authenticationImageView.hidden = NO;
        self.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
    }else{
        if ([self.model.user_info.merchant_cert_status isEqualToString:@"3"] &&
            ![self.model.user_info.cert_status isEqualToString:@"2"]){
            self.username.textColor = FWTextColor_2B98FA;
            self.authenticationImageView.hidden = YES;
        }else{
            self.authenticationImageView.hidden = YES;
            self.username.textColor = FWTextColor_B6BCC4;
        }
    }
    
    /*********单独为某人飘红开始************/
    if ([self.model.user_info.uid isEqualToString:@"1001385"]) {
        self.username.textColor = FWGradual_Red_E67436;
    }
    /*********单独为某人飘红结束***********/

    
    if ([self.model.feed_type isEqualToString:@"1"]) {
        self.videoIcon.hidden = NO;
        self.videoIcon.image = [UIImage imageNamed:@"icon_video"];
        [self.videoIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.iv);
            make.centerY.mas_equalTo(self.iv);
            make.size.mas_equalTo(CGSizeMake(21, 32));
        }];
    }else if([self.model.feed_type isEqualToString:@"2"]){
        self.videoIcon.hidden = YES;
    }else if([self.model.feed_type isEqualToString:@"3"]){
        self.videoIcon.hidden = NO;
        self.videoIcon.image = [UIImage imageNamed:@"home_artical"];
        [self.videoIcon mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.right.mas_equalTo(self.iv).mas_offset(-5);
            make.size.mas_equalTo(CGSizeMake(51, 23));
        }];
    }else{
        self.videoIcon.hidden = YES;
    }
    
    DHWeakSelf;
    
    if (self.model.feed_cover_nowatermark.length > 0) {
        /* 如果有图片就显示，因为问答可能不含有图片 */
        if (self.model.didNotNeedFuckingFadeAnimateEffect) {
            self.siIcon.alpha = 0;
        } else {
            self.siIcon.alpha = 1;
        }
        
        [self.iv sd_setImageWithURL:[NSURL URLWithString:self.model.feed_cover_nowatermark] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            if (!weakSelf.model.didNotNeedFuckingFadeAnimateEffect) {
                
                [UIView animateWithDuration:0.5 animations:^{
                    weakSelf.siIcon.alpha = 0;
                } completion:^(BOOL finished) {
                    weakSelf.model.didNotNeedFuckingFadeAnimateEffect = YES;
                }];
            }
        }];
    }else{
        self.iv.image = [UIImage imageNamed:@"placeholder"];
    }

    
    UIImage *placeholder = [UIImage imageNamed:@"tags_icon"];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:self.model.user_info.header_url] placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.userIcon.image = (image ? [image js_circleImage] : placeholder);
    }];
    
    if ([self.model.status_niming isEqualToString:@"2"]) {
        self.username.text = @"匿名";
        self.userIcon.image = placeholder;
        self.userButton.userInteractionEnabled = NO;
    }else{
        self.userButton.userInteractionEnabled = YES;
    }
    
    
    [self refreshFrame];
}

#pragma mark - > 重新布局图片和描述
- (void)refreshFrame{
    
    CGFloat cover_width = (SCREEN_WIDTH-38)/2;
    CGFloat height = 0;
    
    if ([self.model.feed_type isEqualToString:@"2"]) {
        
        height = (cover_width *4)/3;
        [self.iv mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_equalTo(self.contentView);
            make.height.mas_equalTo(height);
        }];
    }else if ([self.model.feed_type isEqualToString:@"3"]){
        
        height = (cover_width *4)/5;
        [self.iv mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_equalTo(self.contentView);
            make.height.mas_equalTo(height);
        }];
    }else if ([self.model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (self.model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            [self.iv mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.mas_equalTo(self.contentView);
                make.height.mas_equalTo(height);
            }];
        }else{
            height = (cover_width *4)/3;
            [self.iv mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.top.mas_equalTo(self.contentView);
                make.height.mas_equalTo(height);
            }];
        }
    }else{
        
        if (self.model.feed_cover_width.length >0 && self.model.feed_cover_height.length >0) {
            
            CGFloat feed_cover_width = [self.model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [self.model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height < 3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            self.model.feed_cover_width = @(cover_width).stringValue;
            self.model.feed_cover_height = @(height).stringValue;
        }

        [self.iv mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_equalTo(self.contentView);
            make.height.mas_equalTo(height);
        }];
    }
    
    
    if ([self.model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.left.mas_equalTo(self.contentView).mas_offset(5);
           make.top.mas_equalTo(self.iv.mas_bottom).mas_offset(5);
           make.right.mas_equalTo(self.contentView).mas_offset(-10);
           make.width.mas_greaterThanOrEqualTo(10);
           make.height.mas_equalTo(25);
        }];
    }else{
        [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.left.mas_equalTo(self.contentView).mas_offset(5);
           make.top.mas_equalTo(self.iv.mas_bottom).mas_offset(5);
           make.right.mas_equalTo(self.contentView).mas_offset(-10);
           make.width.mas_greaterThanOrEqualTo(10);
           make.height.mas_equalTo(0.01);
        }];
    }
    
    
    CGFloat textHeight ;
    
    if (self.model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:self.model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    [self.desc mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(5);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(0);
        make.right.mas_equalTo(self.contentView).mas_offset(-12);
        make.width.mas_equalTo(cover_width - 17);
        make.height.mas_equalTo(textHeight);
    }];
    
    if ([self.model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        [self.wendaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.left.mas_equalTo(self.desc);
           make.top.mas_equalTo(self.desc.mas_bottom).mas_offset(5);
           make.right.mas_equalTo(self.contentView).mas_offset(-10);
           make.width.mas_equalTo(self.contentView.frame.size.width-20);
           make.height.mas_equalTo(16);
        }];
    }
}

- (void)checkLogin{
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {

        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewController presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}


- (void)setupLayer{
    
    self.userInteractionEnabled = YES;
    self.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    self.layer.cornerRadius = 5.0f;
    self.contentView.layer.cornerRadius = 5.0f;
    self.contentView.layer.borderWidth = 0.4f;
    self.contentView.layer.borderColor = FWClearColor.CGColor;
    self.contentView.layer.masksToBounds = YES;
    
    self.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowOpacity = 0.7;
    self.layer.masksToBounds = NO;
    self.layer.contentsScale = [UIScreen mainScreen].scale;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:6.f].CGPath;
    
    //设置缓存
    self.layer.shouldRasterize = YES;
    
    //设置抗锯齿边缘
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

@end
