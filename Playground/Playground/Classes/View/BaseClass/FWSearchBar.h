//
//  FWSearchBar.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWSearchBar : UISearchBar

@end

NS_ASSUME_NONNULL_END
