//
//  FWEmptyView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWEmptyView.h"

@implementation FWEmptyView
@synthesize emptyLabel;
@synthesize emptyImageView;

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    emptyImageView = [[UIImageView alloc] init];
    [self addSubview:emptyImageView];
    [emptyImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.top.mas_equalTo(self).mas_offset(50);
        make.size.mas_equalTo(CGSizeMake(107, 115));
    }];
    
    emptyLabel = [[UILabel alloc] init];
    emptyLabel.font = [UIFont fontWithName:@"PingFangSC-Light" size:14];
    emptyLabel.textColor = FWTextColor_969696;
    emptyLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:emptyLabel];
    [emptyLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(emptyImageView);
        make.top.mas_equalTo(self.emptyImageView.mas_bottom).mas_offset(20);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 20));
    }];
    
}

- (void)settingWithImageName:(NSString *)iamgeName WithTitle:(NSString *)title{
    
    emptyImageView.image = [UIImage imageNamed:iamgeName];
    emptyLabel.text = title;
}

@end
