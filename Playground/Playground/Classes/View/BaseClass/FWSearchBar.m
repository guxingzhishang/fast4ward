//
//  FWSearchBar.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWSearchBar.h"

@implementation FWSearchBar

- (void)layoutSubviews{
    
    [super layoutSubviews];
    for (UIView*view in self.subviews) {
        for (UIView*subView in view.subviews) {
            if ([subView isKindOfClass:[UITextField class]]) {
                //你想要设置的高度
                subView.frame = CGRectMake(5, 3, self.frame.size.width-10, self.frame.size.height-6);
            }
        }
    }
}
@end
