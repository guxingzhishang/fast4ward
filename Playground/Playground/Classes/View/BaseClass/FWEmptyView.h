//
//  FWEmptyView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FWEmptyView : UIView

@property (nonatomic, strong) UIImageView * emptyImageView;

@property (nonatomic, strong) UILabel * emptyLabel;

/**
 设置缺省页面

 @param iamgeName 图片名称
 @param title 缺省文字
 */
- (void)settingWithImageName:(NSString *)iamgeName WithTitle:(NSString *)title;


@end
