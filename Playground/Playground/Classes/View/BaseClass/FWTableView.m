//
//  FWTableView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWTableView.h"
#import "UIScrollView+EmptyDataSet.h"

@interface FWTableView ()<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@end

@implementation FWTableView

- (void)setIsNeedEmptyPlaceHolder:(BOOL)isNeedEmptyPlaceHolder {
    
    _isNeedEmptyPlaceHolder = isNeedEmptyPlaceHolder;
    if (_isNeedEmptyPlaceHolder) {
        
        self.emptyDataSetSource = self;
        self.emptyDataSetDelegate = self;
    }
}

#pragma mark - > DZNEmptyDataSetSource DZNEmptyDataSetDelegate

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    if (self.emptyDescriptionString) {
        return self.emptyDescriptionString;
    }
    return nil;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    
    return self.isNeedEmptyPlaceHolder;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    
    return YES;
}


- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    
    if (self.emptyPlaceHolderImageName) {
        return DHImage(self.emptyPlaceHolderImageName);
    }
    else {
        return DHImage(@"empty");
    }
}

- (nullable UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    
    
    if (self.emptyPlaceHolderBackgroundColor) {
        return self.emptyPlaceHolderBackgroundColor;
    }
    return FWViewBackgroundColor_FFFFFF;
}


- (BOOL)touchesShouldCancelInContentView:(UIView *)view {
    return YES;
}

- (nullable NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    
    if (self.emptyButtonTitleString) {
        return self.emptyButtonTitleString;
    }
    return nil;
}

- (nullable UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state;{
    if (self.buttonBackgroundImage) {
        return self.buttonBackgroundImage;
    }
    return nil;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    if (self.verticalOffset) {
        return self.verticalOffset;
    }
    return -100.0f;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView{
    if (self.spaceHeight) {
        return self.spaceHeight;
    }
    return 30.0f;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{
    
    if ([self.emptyButtonDelegate respondsToSelector:@selector(FWTableViewEmptyDataSetDidTapButton)]) {
        [self.emptyButtonDelegate FWTableViewEmptyDataSetDidTapButton];
    }
}

@end
