//
//  FWPickerTypeView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWMatchModel.h"

NS_ASSUME_NONNULL_BEGIN

//pickerView 格式
typedef NS_ENUM(NSUInteger, FWPickerViewType)
{
    FWSignlePickerViewType = 0,// 0:只有一列
    FWDoublePickerViewType,// 1:有两列
    FWThreePickerViewType,// 2:有三列
};


//此pickerView 实现的是单列选取，点击button自动唤起pickerView


@class FWPickerTypeView;

@protocol SelectPickerTypeDelegate <NSObject>

@optional
- (void)cancelShow;

- (void) selectType:(NSString*)selectTitle withFirstRow:(NSInteger)firstRow withSecondRow:(NSInteger)secondRow;

- (void) pickViewWillBecomeFirstResponder:(FWPickerTypeView *)pickView;

- (void) FWPickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row component:(NSInteger)component;

@end


@interface FWPickerTypeView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>

@property (strong, nonatomic) UIButton *cancelBtn;
@property (strong, nonatomic) UIButton *sureBtn;
@property (strong, nonatomic) UIPickerView *lzPickerView;

@property (strong, nonatomic) UIView *bgVIew;
@property (strong, nonatomic) UIView *toolBgView;

@property (assign, nonatomic) CGFloat bgViewHeight;

//有几列，就用几个array接受数据
@property (nonatomic,strong) NSMutableArray * oneTitleArray;
@property (nonatomic,strong) NSMutableArray * twoTitleArray;
@property (nonatomic,strong) NSMutableArray * threeTitleArray;

//有几列，就用几个title
@property (nonatomic,strong) NSString * firstTitle;
@property (nonatomic,strong) NSString * secondTitle;
@property (nonatomic,strong) NSString * thirdTitle;

@property (nonatomic,assign) NSInteger  pickerType; //哪种类型

@property (nonatomic, strong) FWMatchModel *matchModel;

@property (nonatomic, weak) id<SelectPickerTypeDelegate>delegate;

- (void)reloadData;
//显示
- (void)show;

@end

NS_ASSUME_NONNULL_END
