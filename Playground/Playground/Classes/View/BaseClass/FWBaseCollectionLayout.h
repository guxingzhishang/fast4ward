//
//  FWBaseCollectionLayout.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FWBaseCollectionLayout;

@protocol FWBaseCollectionLayoutDalegate <NSObject>

- (CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath;

@end

@interface FWBaseCollectionLayout : UICollectionViewFlowLayout


@property(nonatomic, weak) id <FWBaseCollectionLayoutDalegate> delegate;

/**
 *  每行之间的间距
 */
@property (nonatomic, assign) CGFloat rowMargin;

/**
 *  每列之间的间距
 */
@property (nonatomic, assign) CGFloat colMargin;

/**
 *  列数
 */
@property (nonatomic, assign) NSInteger columns;

/**
 *  contentSize
 */
@property (nonatomic, assign) CGSize contentSize;

/**
 *  自动配置 contentSize
 */
@property (nonatomic, assign) UIEdgeInsets sectionInset;

@property (nonatomic, assign) BOOL isShowHeader;


-(void)autuContentSize;

@end
