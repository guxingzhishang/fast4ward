//
//  FWCollecitonBaseCell.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWFeedModel.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Extension.h"

@interface FWCollecitonBaseCell : UICollectionViewCell

@property(nonatomic, strong) FWFeedListModel *model;

@property (strong, nonatomic)  UIImageView *iv;
@property (nonatomic, strong)  UIImageView * activityImageView;
@property (nonatomic, strong)  UIImageView * jingpinImageView;
@property (nonatomic, strong)  UIImageView *icon;

@property (strong, nonatomic)  UIImageView *userIcon;
@property (strong, nonatomic)  UIImageView *authenticationImageView;
@property (strong, nonatomic)  UIImageView *videoIcon;
@property (strong, nonatomic)  UIImageView *siIcon;
@property (strong, nonatomic)  UIImageView *pvImageView;

@property (strong, nonatomic)  UILabel *username;
@property (strong, nonatomic)  UILabel *pvLabel;
@property (strong, nonatomic)  UILabel *priceLabel;
@property (strong, nonatomic)  UILabel *desc;
@property (strong, nonatomic)  UILabel *wendaLabel;

@property (strong, nonatomic)  UIButton *userButton;
@property (strong, nonatomic)  UIButton *linkButton; // 文章-关联商品

@property (nonatomic, strong)  NSString * likeType;

@property (weak, nonatomic)  UIViewController * viewController;

- (void)setupSubviews;

- (void)configForCell:(id)model ;

- (void)checkLogin;

@end
