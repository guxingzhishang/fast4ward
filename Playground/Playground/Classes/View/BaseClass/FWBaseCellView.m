//
//  FWBaseCellView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseCellView.h"

@implementation FWBaseCellView
@synthesize iconImageView;
@synthesize arrowImageView;
@synthesize titleLabel;
@synthesize rightLabel;
@synthesize lineView;
@synthesize cellButton;

- (instancetype)initWithTitle:(NSString *)title WithImage:(NSString *)imageName{
    
    self = [super init];
    
    if (self) {
        
        [self setupSubviewsWithTitle:title WithImage:imageName];
    }
    
    return self;
}

#pragma mark - > 初始化视图
- (void)setupSubviewsWithTitle:(NSString *)title WithImage:(NSString *)imageName{
    
    if (!iconImageView) {
        iconImageView = [[UIImageView alloc] init];
    }
    iconImageView.image = [UIImage imageNamed:imageName];
    iconImageView.backgroundColor = [UIColor orangeColor];
    [self addSubview:iconImageView];
    [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25,25));
    }];
    
    if (!titleLabel) {
        titleLabel = [[UILabel alloc] init];
    }
    titleLabel.text = title;
    titleLabel.font = DHSystemFontOfSize_15;
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(iconImageView.mas_right).mas_offset(15);
        make.centerY.mas_equalTo(iconImageView);
        make.height.mas_equalTo(25);
    }];
    
    if (!arrowImageView) {
        arrowImageView = [[UIImageView alloc] init];
    }
    arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(6, 11));
        make.centerY.mas_equalTo(self);
    }];
    
    if (!rightLabel) {
        rightLabel = [[UILabel alloc] init];
    }
    rightLabel.font = DHSystemFontOfSize_12;
    rightLabel.textColor = FWTextColor_646464;
    rightLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:rightLabel];
    [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-15);
        make.centerY.mas_equalTo(arrowImageView);
        make.height.mas_equalTo(20);
    }];
    
    
    if (!lineView) {
        lineView = [[UIView alloc] init];
    }
    lineView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self addSubview:lineView];
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(15);
        make.bottom.mas_equalTo(self).mas_offset(-0.5);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 0.5));
    }];
    
    if (!cellButton) {
        cellButton = [[UIButton alloc] init];
    }
    cellButton.backgroundColor = FWClearColor;
    [self addSubview:cellButton];
    [cellButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(60);
        make.right.mas_equalTo(self).mas_offset(-60);
    }];
    cellButton.hidden = YES;
}

@end
