//
//  DHWindow.m
//  Project_Model
//
//  Created by 韩丛旸 on 2017/2/24.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import "DHWindow.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

const DHWindowLevel DHWindowLevelMask = 200;
const DHWindowLevel DHWindowLevelBomb = 500;
const DHWindowLevel DHWindowLevelActivity = 1900;
const DHWindowLevel DHWindowLevelUpdate = 2000;

@interface DHWindow ()



@end

@implementation DHWindow

- (NSMutableArray<DHWindowSubViewModel *> *)windowSubViews
{
    if (_windowSubViews == nil)
    {
        _windowSubViews = @[].mutableCopy;
    }
    return _windowSubViews;
}

- (NSMutableArray<UIViewController *> *)vcContainer
{
    if (_vcContainer == nil)
    {
        _vcContainer = @[].mutableCopy;
    }
    return _vcContainer;
}

+ (void)addDHViewController:(UIViewController *)vc priority:(NSInteger)priority
{
    DHWindow *dhWindow = [self getCurrentWindow];
    [dhWindow.vcContainer addObject:vc];
    [DHWindow addDHCustomSubView:vc.view priority:priority];
}

+ (void)addDHCustomSubView:(UIView *)subView priority:(NSInteger)priority
{
    DHWindow *dhWindow = [self getCurrentWindow];
    DHWindowSubViewModel *model = [[DHWindowSubViewModel alloc] init];
    model.subView = subView;
    model.priority = priority;
    
    if (dhWindow.windowSubViews.count == 0)
    {
        [dhWindow addSubview:subView];
        [dhWindow.windowSubViews addObject:model];
    }
    else
    {
        __block DHWindowSubViewModel *m;

        [dhWindow.windowSubViews enumerateObjectsUsingBlock:^(DHWindowSubViewModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.priority > priority) {
                m = obj;
                *stop = YES;
            }
        }];
                
        NSInteger point = [dhWindow.windowSubViews indexOfObject:m];
        
        if (m != nil)
        {
            [dhWindow insertSubview:subView belowSubview:m.subView];
            [dhWindow.windowSubViews insertObject:model atIndex:point];
        }
        else
        {
            [dhWindow addSubview:subView];
            [dhWindow.windowSubViews addObject:model];
        }
    }
}

+ (instancetype)getCurrentWindow
{
    NSArray<UIWindow *> *arr = [[UIApplication sharedApplication] windows];
    __block DHWindow *dhWindow;
    [arr enumerateObjectsUsingBlock:^(UIWindow * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[DHWindow class]])
            dhWindow = (DHWindow *)obj;
    }];
    return dhWindow;
}

+ (void)removeDHViewController:(UIViewController *)vc
{
    
    DHWindow *dhWindow = [self getCurrentWindow];
    if ([dhWindow.vcContainer containsObject:vc])
    {
        [dhWindow.vcContainer removeObject:vc];
    }
    [dhWindow.windowSubViews enumerateObjectsUsingBlock:^(DHWindowSubViewModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.subView isEqual:vc.view]) {
            [vc.view removeFromSuperview];
            [dhWindow.windowSubViews removeObject:obj];
            *stop = YES;
        }
    }];
}

+ (void)removeSubView:(UIView *)subView
{
    DHWindow *dhWindow = [self getCurrentWindow];
    [dhWindow.windowSubViews enumerateObjectsUsingBlock:^(DHWindowSubViewModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.subView isEqual:subView]) {
            [subView removeFromSuperview];
            [dhWindow.windowSubViews removeObject:obj];
            *stop = YES;
        }
    }];
}

UIImage *UIImageFromFullScreenShot(void)
{
    
    BOOL ignoreOrientation = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0");
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    
    CGSize imageSize = CGSizeZero;
    if (UIInterfaceOrientationIsPortrait(orientation) || ignoreOrientation)
        imageSize = [UIScreen mainScreen].bounds.size;
    else
        imageSize = CGSizeMake([UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
    
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for (UIWindow *window in [[UIApplication sharedApplication] windows])
    {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, window.center.x, window.center.y);
        CGContextConcatCTM(context, window.transform);
        CGContextTranslateCTM(context, -window.bounds.size.width * window.layer.anchorPoint.x, -window.bounds.size.height * window.layer.anchorPoint.y);
        
        if(!ignoreOrientation)
        {
            if(orientation == UIInterfaceOrientationLandscapeLeft)
            {
                CGContextRotateCTM(context, (CGFloat)M_PI_2);
                CGContextTranslateCTM(context, 0, -imageSize.width);
            }
            else if(orientation == UIInterfaceOrientationLandscapeRight)
            {
                CGContextRotateCTM(context, (CGFloat)-M_PI_2);
                CGContextTranslateCTM(context, -imageSize.height, 0);
            }
            else if(orientation == UIInterfaceOrientationPortraitUpsideDown)
            {
                CGContextRotateCTM(context, (CGFloat)M_PI);
                CGContextTranslateCTM(context, -imageSize.width, -imageSize.height);
            }
        }
        
        if([window respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)])
            [window drawViewHierarchyInRect:window.bounds afterScreenUpdates:NO];
        else
            [window.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        CGContextRestoreGState(context);
    }
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

@end
