//
//  DHWindowSubViewModel.h
//  Project_Model
//
//  Created by 韩丛旸 on 2017/3/20.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DHWindowSubViewModel : NSObject

@property (nonatomic, assign) NSInteger priority;// 视图的优先级
@property (nonatomic, strong) UIView *subView;// 子视图

@end
