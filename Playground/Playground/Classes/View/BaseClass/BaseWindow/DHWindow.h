//
//  DHWindow.h
//  Project_Model
//
//  Created by 韩丛旸 on 2017/2/24.
//  Copyright © 2017年 北京搭伙科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHWindowSubViewModel.h"

typedef CGFloat DHWindowLevel;

UIKIT_EXTERN const DHWindowLevel DHWindowLevelMask;// 升级提示
UIKIT_EXTERN const DHWindowLevel DHWindowLevelBomb;// 弹屏
UIKIT_EXTERN const DHWindowLevel DHWindowLevelActivity;// 活动
UIKIT_EXTERN const DHWindowLevel DHWindowLevelUpdate;// 强制升级


@interface DHWindow : UIWindow

@property (nonatomic, weak) UIButton *IMBtn;

// 主window上的子视图容器
@property (nonatomic, strong) NSMutableArray<DHWindowSubViewModel *> *windowSubViews;
// 主window上的控制器容器
@property (nonatomic, strong) NSMutableArray<UIViewController *> *vcContainer;


/**
 添加控制器的view放到主window上

 @param vc view的控制器
 @param priority 优先级
 */
+ (void)addDHViewController:(UIViewController *)vc priority:(NSInteger)priority;


/**
 添加view放到主window上
 
 @param view
 @param priority 优先级
 */
+ (void)addDHCustomSubView:(UIView *)subView priority:(NSInteger)priority;


/**
 删除window上的控制器以及控制器的view
 */
+ (void)removeDHViewController:(UIViewController *)vc;


/**
 删除window上的view
 */
+ (void)removeSubView:(UIView *)subView;

// 截频
UIImage *UIImageFromFullScreenShot(void);

@end
