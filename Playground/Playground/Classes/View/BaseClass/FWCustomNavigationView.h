//
//  FWCustomNavigationView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/10.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWCustomNavigationView : UIView

@property (nonatomic, strong) UIView * navigationView;

@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, strong) UILabel * navigationLabel;

@property (nonatomic, strong) UIImageView * navigationImageView;

@property (nonatomic, strong) NSString * title;


@end

NS_ASSUME_NONNULL_END
