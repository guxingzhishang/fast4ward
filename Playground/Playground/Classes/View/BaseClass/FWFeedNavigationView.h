//
//  FWFeedNavigationView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/2.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWFeedNavigationViewDelegate <NSObject>

- (void)attentionButtonClick;

@end

@interface FWFeedNavigationView : UIView

@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIButton * blackBackButton;
@property (nonatomic, strong) UIButton * vipImageButton;
@property (nonatomic, strong) UIButton * attentionButton;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) FWFeedListModel * listModel;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, weak) id <FWFeedNavigationViewDelegate>delegate;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
