//
//  FWBaseRankCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 社区 - > 商家排行榜 + 车友排行榜
 */
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWBaseRankCell : UITableViewCell

@property (nonatomic, strong) UILabel * numberLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * signLabel;
@property (nonatomic, strong) UILabel * lineLabel;
@property (nonatomic, strong) UIImageView * rightImageView;
@property (nonatomic, strong) UIImageView * rankNumImageView;

@property (nonatomic, strong) UIImageView * rankImageView;//只有前三名有
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * authenticationView;

/* 1:bussiness    2:person */
@property (nonatomic, strong) NSString * type;


- (void)configCellForModel:(id)model;

@end

NS_ASSUME_NONNULL_END
