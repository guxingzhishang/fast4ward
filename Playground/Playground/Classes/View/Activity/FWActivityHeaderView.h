//
//  FWActivityHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/11.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWBannerView.h"
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWActivityRankView : UIImageView

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * photoBgImageView;
@property (nonatomic, strong) UIImageView * activityRightView;
@property (nonatomic, strong) UIImageView * top10View;

@property (nonatomic, assign) NSInteger  rankType;

- (void)configForView:(id)model;

@end

@interface FWActivityHeaderView : UIView<SDCycleScrollViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate>

/* 轮播图 */
@property (nonatomic, strong) SDCycleScrollView * bannerView;

@property (nonatomic, strong) FWActivityRankView * playerRankView;

@property (nonatomic, strong) FWActivityRankView * bussinessRankView;

/* 热门话题 */ 
@property (nonatomic, strong) UILabel * hotTopicLabel;

/* 话题 */
@property (nonatomic, strong) UIScrollView * topicScrollView;

/* 新车爆照 */
@property (nonatomic, strong) UIView * showCarView;

/* 社区精选 */
@property (nonatomic, strong) UILabel * jxArticolLabel;

@property (nonatomic, assign) CGFloat  currentHeight;

@property (nonatomic, weak) UIViewController * vc;

- (void)configViewForModel:(id)model;

@end

NS_ASSUME_NONNULL_END
