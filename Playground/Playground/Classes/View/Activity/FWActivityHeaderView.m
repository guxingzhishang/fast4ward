//
//  FWActivityHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/12/11.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWActivityHeaderView.h"
#import "FWBussinessRankViewController.h"
#import "FWPersonRankViewController.h"
#import "FWMatchViewController.h"
#import "FWShowCarViewController.h"
#import "FWCarDetailViewController.h"

@implementation FWActivityRankView

- (id)init{
    self = [super init];
    if (self) {
        
        self.userInteractionEnabled = YES;
        self.layer.cornerRadius = 2;
        self.layer.masksToBounds = YES;
        self.contentMode = UIViewContentModeScaleAspectFill;
        self.backgroundColor = FWTextColor_161426;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    self.titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).mas_offset(FWAdaptive(14));
        make.left.mas_equalTo(self).mas_offset(9);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(19);
    }];
    
    self.activityRightView = [[UIImageView alloc] init];
    self.activityRightView.image = [UIImage imageNamed:@"activity_right"];
    [self addSubview:self.activityRightView];
    [self.activityRightView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.titleLabel);
        make.right.mas_equalTo(self).mas_offset(-9);
        make.width.mas_equalTo(10);
        make.height.mas_equalTo(14);
    }];
    
    self.photoBgImageView = [[UIImageView alloc] init];
    self.photoBgImageView.image = [UIImage imageNamed:@"photo_bg_circle"];
    [self addSubview:self.photoBgImageView];
    [self.photoBgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(FWAdaptive(14));
        make.left.mas_equalTo(self.titleLabel);
        make.width.mas_equalTo(34);
        make.height.mas_equalTo(34);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 29/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.photoBgImageView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoBgImageView);
        make.centerX.mas_equalTo(self.photoBgImageView);
        make.width.mas_equalTo(29);
        make.height.mas_equalTo(29);
    }];
    
    self.top10View = [[UIImageView alloc] init];
    self.top10View.image = [UIImage imageNamed:@"TOP_10"];
    [self addSubview:self.top10View];
    [self.top10View mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(6);
        make.top.mas_equalTo(self.photoImageView);
        make.width.mas_equalTo(38);
        make.height.mas_equalTo(13);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13.6];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.photoBgImageView);
        make.left.mas_equalTo(self.top10View);
        make.right.mas_equalTo(self).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(19);
    }];
}

- (void)configForView:(id)model{
    
    self.nameLabel.text = @"xxxxxx";
}

- (void)setRankType:(NSInteger)rankType{
    _rankType = rankType;
    
    if (self.rankType == 1) {
        self.titleLabel.text = @"商家人气榜";
        self.image = [UIImage imageNamed:@"bussiness_rank"];
    }else if (self.rankType == 2){
        self.titleLabel.text = @"车友人气榜";
        self.image = [UIImage imageNamed:@"person_rank"];
    }
}

@end

@interface FWActivityHeaderView()

@property (strong, nonatomic) FWActivityModel * activity;

@end

@implementation FWActivityHeaderView
@synthesize playerRankView;
@synthesize bussinessRankView;
@synthesize hotTopicLabel;
@synthesize topicScrollView;
@synthesize jxArticolLabel;
@synthesize showCarView;

- (id)init{
    self = [super init];
    if (self) {
        self.currentHeight = 0;
        
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(14, 10, SCREEN_WIDTH-28, 0.01) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.bannerView.layer.cornerRadius = 2;
    self.bannerView.layer.masksToBounds = YES;
    self.bannerView.autoScrollTimeInterval = 4;
    self.bannerView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.bannerView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    [self addSubview:self.bannerView];
    
    CGFloat height = 75*((SCREEN_WIDTH-38)/2)/169;
    
    bussinessRankView = [[FWActivityRankView alloc] init];
    bussinessRankView.rankType = 1;
    bussinessRankView.tag = 1080;
    bussinessRankView.frame = CGRectMake(CGRectGetMinX(self.bannerView.frame), CGRectGetMaxY(self.bannerView.frame)+10, (SCREEN_WIDTH-38)/2, height);
    [self addSubview:bussinessRankView];
    UITapGestureRecognizer * playerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [bussinessRankView addGestureRecognizer:playerTap];
    
    
    playerRankView = [[FWActivityRankView alloc] init];
    playerRankView.rankType = 2;
    playerRankView.tag = 1090;
    playerRankView.frame = CGRectMake(SCREEN_WIDTH-20-CGRectGetWidth(bussinessRankView.frame), CGRectGetMinY(bussinessRankView.frame), CGRectGetWidth(bussinessRankView.frame), CGRectGetHeight(bussinessRankView.frame));
    [self addSubview:playerRankView];
    
    UITapGestureRecognizer * bussinessTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [playerRankView addGestureRecognizer:bussinessTap];
    
    hotTopicLabel = [[UILabel alloc] init];
    hotTopicLabel.text = @"热门话题";
    hotTopicLabel.frame = CGRectMake(14, CGRectGetMaxY(playerRankView.frame)+20, 100, 23);
    hotTopicLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    hotTopicLabel.textColor = FWTextColor_BCBCBC;
    hotTopicLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:hotTopicLabel];
    
    topicScrollView = [[UIScrollView alloc] init];
    topicScrollView.delegate = self;
    topicScrollView.frame = CGRectMake(0, CGRectGetMaxY(hotTopicLabel.frame)+15, SCREEN_WIDTH, 140);
    [self addSubview:topicScrollView];
    
    showCarView = [[UIView alloc] init];
    [self addSubview:showCarView];
    showCarView.frame = CGRectMake(0, CGRectGetMaxY(topicScrollView.frame), SCREEN_WIDTH, 0.01);
    
    jxArticolLabel = [[UILabel alloc] init];
    jxArticolLabel.text = @"社区精选";
    jxArticolLabel.frame = CGRectMake(14, CGRectGetMaxY(showCarView.frame)+20, 100, 23);
    jxArticolLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    jxArticolLabel.textColor = FWTextColor_BCBCBC;
    jxArticolLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:jxArticolLabel];
}

- (void)configViewForModel:(id)model{
    
    self.activity = (FWActivityModel *)model;
    
    // 轮播图
    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (FWActivityBannerModel * imageModel in self.activity.list_banner) {
        [tempArr addObject:imageModel.img_url];
    }
    
    bussinessRankView.nameLabel.text = self.activity.merchant_top1.nickname;
    playerRankView.nameLabel.text = self.activity.car_top1.nickname;
    
    [bussinessRankView.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.activity.merchant_top1.header_url]];
    [playerRankView.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.activity.car_top1.header_url]];
    
    CGFloat showCarHeight = 0;
    
    
    if (self.activity.list_car.count > 0) {
        if (self.activity.list_car.count <= 2) {
            showCarHeight = (127*(SCREEN_WIDTH-38)/2)/169 +50+10;
        }else{
            showCarHeight = 2* ((127*(SCREEN_WIDTH-38)/2)/169) +50 +20;
        }
        [self setupShowCarView];
    }else{
        if (showCarView) {
            for (UIView * view in showCarView.subviews){
                [view removeFromSuperview];
            }
        }
        
        showCarHeight = 0.01;
    }
    
    if (tempArr.count > 0) {
        self.bannerView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, (SCREEN_WIDTH-28)*146/(375-28));
    }else{
        self.bannerView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, 0.01);
    }
    
    CGFloat height = 75*((SCREEN_WIDTH-38)/2)/169;
    
    bussinessRankView.frame = CGRectMake(CGRectGetMinX(self.bannerView.frame), CGRectGetMaxY(self.bannerView.frame)+10, (SCREEN_WIDTH-38)/2, height);
    
    playerRankView.frame = CGRectMake(SCREEN_WIDTH-14-CGRectGetWidth(bussinessRankView.frame), CGRectGetMinY(bussinessRankView.frame), CGRectGetWidth(bussinessRankView.frame), CGRectGetHeight(bussinessRankView.frame));
    
    if (self.activity.list_tag.count > 0) {
        /// 有热门活动
        [self setupScrollView];
        
        hotTopicLabel.hidden = NO;
        topicScrollView.hidden = NO;
        
        hotTopicLabel.frame = CGRectMake(14, CGRectGetMaxY(playerRankView.frame)+15, 100, 23);
        topicScrollView.frame = CGRectMake(0, CGRectGetMaxY(hotTopicLabel.frame)+8, SCREEN_WIDTH, 100);
        showCarView.frame = CGRectMake(0, CGRectGetMaxY(topicScrollView.frame), SCREEN_WIDTH, showCarHeight);
    }else{
        hotTopicLabel.hidden = YES;
        topicScrollView.hidden = YES;
        showCarView.frame = CGRectMake(0, CGRectGetMaxY(bussinessRankView.frame), SCREEN_WIDTH, showCarHeight);
    }
    
    jxArticolLabel.frame = CGRectMake(14, CGRectGetMaxY(showCarView.frame)-5, 100, 25);
    
    
    // 获取当前视图的高度
    self.currentHeight = CGRectGetMaxY(jxArticolLabel.frame)+5;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.bannerView.imageURLStringsGroup = tempArr;
    });
}


#pragma mark - > 初始化热门话题
- (void)setupScrollView{
    
    for (UIView * view in topicScrollView.subviews) {
        [view removeFromSuperview];
    }
    
    for(int i = 0; i < self.activity.list_tag.count; i++){
        FWTagsInfoModel * model = self.activity.list_tag[i];
        
        UIImageView * bgImageView = [[UIImageView alloc] init];
        bgImageView.tag= 666+i;
        bgImageView.contentMode = UIViewContentModeScaleAspectFill;
        bgImageView.layer.cornerRadius = 2;
        bgImageView.layer.masksToBounds = YES;
        bgImageView.userInteractionEnabled = YES;
        [bgImageView sd_setImageWithURL:[NSURL URLWithString:model.tag_image] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        [topicScrollView addSubview:bgImageView];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgTapClick:)];
        [bgImageView addGestureRecognizer:tap];
        [bgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(144, 93));
            make.top.mas_equalTo(topicScrollView);
            make.left.mas_equalTo(topicScrollView).mas_offset(14+ 154*i);
        }];
        
        UIImageView * shadowImageView = [[UIImageView alloc] init];
        shadowImageView.image = [UIImage imageNamed:@"activity_hot_topic_bg"];
        [bgImageView addSubview:shadowImageView];
        [shadowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(bgImageView);
        }];
        
        UILabel * nameLabel = [[UILabel alloc] init];
        nameLabel.text = [NSString stringWithFormat:@"#%@",model.tag_name];
        nameLabel.font =  [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
        nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [shadowImageView addSubview:nameLabel];
        [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(17);
            make.centerY.mas_equalTo(shadowImageView).mas_offset(-5);
            make.centerX.mas_equalTo(shadowImageView);
            make.width.mas_equalTo(shadowImageView);
        }];
        
        UILabel * pvLabel = [[UILabel alloc] init];
        NSString * pvCount = model.pv_count_format?model.pv_count_format:@"";
        pvLabel.text = [NSString stringWithFormat:@"%@次浏览",pvCount];
        pvLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
        pvLabel.textColor = FWViewBackgroundColor_FFFFFF;
        pvLabel.textAlignment = NSTextAlignmentCenter;
        [bgImageView addSubview:pvLabel];
        [pvLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(nameLabel.mas_bottom).mas_offset(0);
            make.centerX.mas_equalTo(shadowImageView);
            make.height.mas_equalTo(15);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
    }
    
    topicScrollView.contentSize = CGSizeMake(self.activity.list_tag.count * 150 +90, 0);
}

#pragma mark - > 新车爆照
- (void)setupShowCarView{
    
    for (UIView * view in showCarView.subviews){
        [view removeFromSuperview];
    }
    
    UILabel * showCarLabel = [[UILabel alloc] init];
    showCarLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    showCarLabel.textColor = FWTextColor_BCBCBC;
    showCarLabel.text = @"新车爆照";
    showCarLabel.textAlignment = NSTextAlignmentLeft;
    showCarLabel.frame = CGRectMake(14, 8, 100, 23);
    [showCarView addSubview:showCarLabel];
    
    UIButton * allButton = [[UIButton alloc] init];
    allButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    allButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 6);
    [allButton setImage:[UIImage imageNamed:@"right_arrow"] forState:UIControlStateNormal];
    [allButton addTarget:self action:@selector(allButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [showCarView addSubview:allButton];
    [allButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(showCarLabel);
        make.size.mas_equalTo(CGSizeMake(50, 40));
        make.right.mas_equalTo(showCarView).mas_offset(-10);
    }];
    
    for (int i = 0; i<self.activity.list_car.count; i++) {
        
        NSInteger lie = i/2;
        NSInteger row = i%2;
        
        UIImageView * bgImageView = [[UIImageView alloc] init];
        [bgImageView sd_setImageWithURL:[NSURL URLWithString:self.activity.list_car[i].car_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        bgImageView.contentMode = UIViewContentModeScaleAspectFill;
        bgImageView.clipsToBounds = YES;
        bgImageView.layer.cornerRadius = 2;
        bgImageView.layer.masksToBounds = YES;
        bgImageView.userInteractionEnabled = YES;
        bgImageView.tag = i +4444;
        [showCarView addSubview:bgImageView];
        bgImageView.frame = CGRectMake(14+row *((SCREEN_WIDTH-38)/2+10),(CGRectGetMaxY(showCarLabel.frame)+9 + lie*(10+(127*(SCREEN_WIDTH-38)/2)/169)), (SCREEN_WIDTH-38)/2, (127*(SCREEN_WIDTH-38)/2)/169);
        
        [bgImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(carDetailClick:)]];
        
        
        UIView * bgView = [[UIView alloc] init];
        bgView.frame = CGRectMake(0, CGRectGetHeight(bgImageView.frame)-65, CGRectGetWidth(bgImageView.frame), 65);
        [bgImageView addSubview:bgView];
        
        CAGradientLayer * gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = bgView.bounds;
        [bgView.layer addSublayer:gradientLayer];
        
        gradientLayer.colors = @[
                                 (__bridge id)[UIColor colorWithHexString:@"ffffff" alpha:0.0].CGColor,
                                 (__bridge id)[UIColor colorWithHexString:@"000000" alpha:0.7].CGColor,
                                 ];
        
        gradientLayer.startPoint = CGPointMake(0.0, 0.0);
        gradientLayer.endPoint = CGPointMake(0.0, 1.0);
        
        UIImageView * photoImageView = [[UIImageView alloc] init];
        photoImageView.layer.cornerRadius = 11;
        photoImageView.layer.masksToBounds = YES;
        [photoImageView sd_setImageWithURL:[NSURL URLWithString:self.activity.list_car[i].user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
        [bgImageView addSubview:photoImageView];
        [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(22, 22));
            make.left.mas_equalTo(bgImageView).mas_offset(8);
            make.bottom.mas_equalTo(bgImageView).mas_offset(-8);
        }];
        
        UILabel * titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 2;
        titleLabel.text = self.activity.list_car[i].car_style_name;
        titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
        titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [bgImageView addSubview:titleLabel];
        [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(photoImageView.mas_right).mas_offset(7);
            make.centerY.mas_equalTo(photoImageView);
            make.height.mas_equalTo(20);
            make.width.mas_greaterThanOrEqualTo(5);
            make.right.mas_equalTo(bgImageView).mas_offset(0);
        }];
    }
}

#pragma mark - > 全部
- (void)allButtonOnClick{
    
    [self.vc.navigationController pushViewController:[FWShowCarViewController new] animated:YES];
}

#pragma mark - > 跳转至标签页
- (void)bgTapClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 666;
    
    FWTagsInfoModel * model = self.activity.list_tag[index];
    FWSearchTagsSubListModel * tagInfo = [[FWSearchTagsSubListModel alloc] init];
    tagInfo.tag_id = model.tag_id;
    tagInfo.tag_name = model.tag_name;
    
    FWTagsViewController * tagsVC = [[FWTagsViewController alloc] init];
    tagsVC.tags_id = model.tag_id;
    tagsVC.traceType = 1;
    tagsVC.tagsModel = tagInfo;
    [self.vc.navigationController pushViewController:tagsVC animated:YES];
}

#pragma mark - > 座驾详情
- (void)carDetailClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 4444;
    
    if (index < self.activity.list_car.count) {
        FWCarDetailViewController * CDVC = [[FWCarDetailViewController alloc] init];
        CDVC.user_car_id = ((FWCarListModel *)self.activity.list_car[index]).user_car_id;
        [self.vc.navigationController pushViewController:CDVC animated:YES];
    }
}

#pragma mark - > 车手/商家排行
- (void)tapClick:(UITapGestureRecognizer *)gesture{
    
    FWActivityRankListModel * rankModel ;
    
    if (gesture.view.tag == 1080) {
        /* 商家排行 */
        rankModel = self.activity.rank_list[0];
        [TalkingData trackEvent:@"影响力榜" label:@"商家" parameters:nil];
        
        FWBussinessRankViewController * BRVC = [[FWBussinessRankViewController alloc] init];
        [self.vc.navigationController pushViewController:BRVC animated:YES];
    }else if (gesture.view.tag == 1090){
        /* 车友排行 */
        rankModel = self.activity.rank_list[1];
        [TalkingData trackEvent:@"影响力榜" label:@"车友" parameters:nil];
        
        FWPersonRankViewController * BRVC = [[FWPersonRankViewController alloc] init];
        [self.vc.navigationController pushViewController:BRVC animated:YES];
    }
    
    //    if (!rankModel) {
    //        return;
    //    }
    //    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    //    WVC.htmlStr = rankModel.url;
    //    WVC.qrcode_url = rankModel.qrcode_url;
    //    WVC.needShare = YES;
    //    WVC.pageType = WebViewTypeURL;
    //    [self.vc.navigationController pushViewController:WVC animated:YES];
}
/**
 *1:赛事主页   2:直播页  3:回放页   4:文章详情页  5:图文详情页   6:话题页  7:商家店铺页  8:商品详情页   9:H5 10:商家排行榜   11：车友排行榜   12:无跳转  13：视频贴
 */
#pragma mark - > banner跳转
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
    FWActivityBannerModel * imageModel = self.activity.list_banner[index];
    
    if (imageModel.banner_id) {
        [TalkingData trackEvent:@"发现banner" label:@"" parameters:@{@"bannerID":imageModel.banner_id}];
    }
    
    if ([imageModel.banner_type isEqualToString:@"1"]){
        /* 赛事主页 */
//        FWMatchViewController * MVC = [[FWMatchViewController alloc] init];
//        [self.vc.navigationController pushViewController:MVC animated:YES];
        self.vc.navigationController.tabBarController.selectedIndex = 1;
    }else if ([imageModel.banner_type isEqualToString:@"4"]){
        /* 文章详情页 */
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = imageModel.banner_val;
        [self.vc.navigationController pushViewController:DVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"5"]){
        /* 图文详情页 */
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.feed_id = imageModel.banner_val;
        TVC.requestType = FWPushMessageRequestType;
        [self.vc.navigationController pushViewController:TVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"6"]){
        /* 标签（话题）页 */
        FWTagsViewController * PVC = [[FWTagsViewController alloc] init];
        PVC.tags_id = imageModel.banner_val;
        [self.vc.navigationController pushViewController:PVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"7"]) {
        /* 商家店铺页*/
        FWCustomerShopViewController * CSVC = [[FWCustomerShopViewController alloc] init];
        CSVC.user_id = imageModel.banner_val;
        CSVC.isUserPage = NO;
        [self.vc.navigationController pushViewController:CSVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"8"]) {
        /* 商品详情页 */
        FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
        GDVC.goods_id = imageModel.banner_val;
        [self.vc.navigationController pushViewController:GDVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"9"]){
        /* 打开H5活动页 */
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.htmlStr = imageModel.banner_val;
        WVC.pageType = WebViewTypeURL;
        [self.vc.navigationController pushViewController:WVC animated:YES];
    }else if ([imageModel.banner_type isEqualToString:@"13"]){
        /* 视频详情页 */
        FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
        controller.currentIndex = 0;
        controller.feed_id = imageModel.banner_val;
        controller.requestType = FWPushMessageRequestType;
        [self.vc.navigationController pushViewController:controller animated:YES];
    }else if ([imageModel.banner_type  isEqualToString:@"16"]){
        /* 长视频详情页 */
        FWLongVideoPlayerViewController *controller = [[FWLongVideoPlayerViewController alloc] init];
        controller.feed_id = imageModel.banner_val;
        controller.myBlock = ^(FWFeedListModel *listModel) {};
        [self.vc.navigationController pushViewController:controller animated:YES];
    }
}

@end

