//
//  FWShowCarCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWShowCarCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView * bgImageView;
@property (nonatomic, strong) UIView * bgView ;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UIImageView * likeImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * likeLabel;
@property (nonatomic, strong) UIButton * likeButton;
@property (nonatomic, strong) FWCarListModel * listModel;
/* 点赞的状态 */
@property (nonatomic, strong) NSString * likeType;

@property (nonatomic, weak) UIViewController * vc;


- (void)configForCell:(id)model;

@end

NS_ASSUME_NONNULL_END
