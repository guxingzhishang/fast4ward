//
//  FWBaseRankCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseRankCell.h"

@implementation FWBaseRankCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.numberLabel.textColor = FWTextColor_858585;
    self.numberLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.numberLabel];
    [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).mas_offset(24);
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(20, 40));
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 41/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.backgroundColor = [UIColor lightGrayColor];
    self.photoImageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel.mas_right).mas_offset(0);
        make.top.mas_equalTo(self.contentView).mas_offset(24);
        make.size.mas_equalTo(CGSizeMake(41, 41));
    }];
    
    self.rankImageView = [[UIImageView alloc] init];
    self.rankImageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:self.rankImageView];
    [self.rankImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.photoImageView.mas_top).mas_offset(9);
        make.centerX.mas_equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(26, 18));
    }];
    self.rankImageView.hidden = YES;
    
    self.rankNumImageView = [[UIImageView alloc] init];
    self.rankNumImageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:self.rankNumImageView];
    [self.rankNumImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.centerY.mas_equalTo(self.photoImageView);
       make.size.mas_equalTo(CGSizeMake(16, 16));
       make.right.mas_equalTo(self.photoImageView.mas_left).mas_offset(-8.5);
    }];
    self.rankNumImageView.hidden = YES;
    
    self.rightImageView = [[UIImageView alloc] init];
    self.rightImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.contentView addSubview:self.rightImageView];
    [self.rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(5, 9));
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.nameLabel.textColor = FWTextColor_12101D;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(13);
        make.top.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(100);
    }];

    self.authenticationView = [[UIImageView alloc] init];
    self.authenticationView.image = [UIImage imageNamed:@"primary_bussiness"];
    [self.contentView addSubview:self.authenticationView];
    [self.authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(0.5);
        make.left.mas_equalTo(self.nameLabel).mas_offset(0);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
    }];
    self.authenticationView.hidden = YES;
    
    
    self.signLabel = [[UILabel alloc] init];
    self.signLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.signLabel.textColor = FWTextColor_858585;
    self.signLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.signLabel];
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.authenticationView.mas_right).mas_offset(5);
        make.centerY.mas_equalTo(self.authenticationView);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-5);
    }];

    // 优先级高一些，先撑起影响力，压缩认证
    
    self.lineLabel = [[UILabel alloc] init];
    self.lineLabel.backgroundColor = FWTextColor_ECECEC;
    [self.contentView addSubview:self.lineLabel];
    [self.lineLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numberLabel);
        make.width.mas_greaterThanOrEqualTo(100);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-1);
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(self.rightImageView);
    }];
}

- (void)configCellForModel:(id)model{
    
    FWMineInfoModel * infoModel = (FWMineInfoModel *)model;
    
    self.nameLabel.text = infoModel.nickname;
    
    if ([self.type isEqualToString:@"2"]) {
        /* 如果是车友排行榜，先让titled = car_style,方便后面统一判断 */
        infoModel.title = [NSString stringWithFormat:@"座驾：%@",infoModel.car_style];
    }
    self.signLabel.text = infoModel.title;

    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:infoModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    
    
    CGFloat authenViewWidth = 0.5;
    CGFloat signLabelLeftOffset = 0;
    
    if ([infoModel.cert_status isEqualToString:@"2"]) {
        
        self.authenticationView.hidden = NO;
        self.authenticationView.image = [UIImage imageNamed:@"middle_bussiness"];

        authenViewWidth = 14;
        signLabelLeftOffset = 5;
    }else{
        self.authenticationView.hidden = YES;
        authenViewWidth = 0.5;
        signLabelLeftOffset = 0;
    }
    
    [self.authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(authenViewWidth);
        make.height.mas_equalTo(14);
        make.left.mas_equalTo(self.nameLabel).mas_offset(0);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
    }];
    [self.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.authenticationView.mas_right).mas_offset(signLabelLeftOffset);
        make.centerY.mas_equalTo(self.authenticationView);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-5);
    }];
    
    
    if (infoModel.title.length <= 0 ) {
        self.signLabel.hidden = YES;
        
        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(13);
            make.centerY.mas_equalTo(self.contentView);
            make.height.mas_equalTo(18);
            make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-10);
            make.width.mas_greaterThanOrEqualTo(100);
        }];
    }else{
        self.signLabel.hidden = NO;

        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(13);
            make.top.mas_equalTo(self.photoImageView);
            make.height.mas_equalTo(23);
            make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-10);
            make.width.mas_greaterThanOrEqualTo(100);
        }];
    }
}


@end
