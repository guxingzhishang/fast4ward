//
//  FWShowCarCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWShowCarCell.h"
#import <SDImageCache.h>
#import <SDWebImageDownloaderOperation.h>
@implementation FWShowCarCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.bgImageView = [[UIImageView alloc] init];
    self.bgImageView.layer.cornerRadius = 5;
    self.bgImageView.layer.masksToBounds = YES;
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgImageView.image = [UIImage imageNamed:@"placeholder"];
    self.bgImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.bgImageView];
    [self.bgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-5);
    }];

    self.bgView = [[UIView alloc] init];
    self.bgView.frame = CGRectMake(0, 0, (SCREEN_WIDTH-38)/2, (((SCREEN_WIDTH-38)/2)*127/169));
    [self.bgImageView addSubview:self.bgView];
    
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bgView.bounds;
    [self.bgView.layer addSublayer:gradientLayer];
    
    gradientLayer.colors = @[
                             (__bridge id)[UIColor colorWithHexString:@"ffffff" alpha:0.0].CGColor,
                             (__bridge id)[UIColor colorWithHexString:@"000000" alpha:0.5].CGColor,
                             ];
    
    gradientLayer.startPoint = CGPointMake(0.0, 0.0);
    gradientLayer.endPoint = CGPointMake(0.0, 1.0);
    
    self.likeButton = [[UIButton alloc] init];
    [self.likeButton addTarget:self action:@selector(likeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.likeButton];
    [self.likeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(11);
        make.right.mas_equalTo(self.bgImageView);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(40);
    }];
    
    self.likeLabel = [[UILabel alloc] init];
    self.likeLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.likeLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.likeLabel.textAlignment = NSTextAlignmentCenter;
    [self.likeButton addSubview:self.likeLabel];
    [self.likeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.likeButton);
        make.height.mas_equalTo(self.likeButton);
        make.width.mas_greaterThanOrEqualTo(20);
        make.right.mas_equalTo(self.likeButton).mas_offset(-5);
    }];
    
    self.likeImageView = [[UIImageView alloc] init];
    self.likeImageView.image = [UIImage imageNamed:@"showcar_like"];
    [self.likeButton addSubview:self.likeImageView];
    [self.likeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(23, 19));
        make.right.mas_equalTo(self.likeLabel.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(self.likeButton);
        make.left.mas_equalTo(self.likeButton);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 11;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.bgImageView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(22, 22));
        make.left.mas_equalTo(self.bgImageView).mas_offset(8);
        make.bottom.mas_equalTo(self.bgImageView).mas_offset(-8);
    }];
    
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    self.titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgImageView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(7);
        make.centerY.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(5);
        make.right.mas_equalTo(self.bgImageView).mas_offset(0);
    }];
}

- (void)configForCell:(id)model{
    
    self.listModel = (FWCarListModel *)model;

    self.titleLabel.text = self.listModel.car_style_name;
    self.likeLabel.text = self.listModel.count_realtime.like_count_format;

    if ([self.listModel.is_liked isEqualToString:@"1"]) {
        self.likeType = @"cancel";
        self.likeImageView.image = [UIImage imageNamed:@"showcar_like"];
    }else{
        self.likeType = @"add";
        self.likeImageView.image = [UIImage imageNamed:@"showcar_unlike"];
    }
    
    [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.car_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.user_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

- (void)likeButtonClick:(UIButton *)sender{
    
    [self checkLogin];
    
    FWCarListModel * model = self.listModel;
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.listModel.feed_id,
                                  @"like_type":self.likeType,
                                  };
        
        [request startWithParameters:params WithAction:Submit_like_feed WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([self.likeType isEqualToString: @"add"]) {
                    model.is_liked = @"1";
                    
                    if ([model.count_realtime.like_count_format integerValue] ||
                        [model.count_realtime.like_count_format integerValue] == 0) {
                        
                        model.count_realtime.like_count_format = @([model.count_realtime.like_count_format integerValue] +1).stringValue;
                    }
                }else if ([self.likeType isEqualToString: @"cancel"]){
                    model.is_liked = @"2";
                    
                    if ([model.count_realtime.like_count_format integerValue] ||
                        [model.count_realtime.like_count_format integerValue] == 0) {
                        
                        model.count_realtime.like_count_format = @([model.count_realtime.like_count_format integerValue] -1).stringValue;
                    }
                }
                
                [self configForCell:model];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

@end
