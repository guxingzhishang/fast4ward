//
//  FWRankHeaderView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRankHeaderView.h"

@implementation FWRankHeaderView

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.bgImageView = [UIImageView new];
    self.bgImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 179*SCREEN_WIDTH/375);
    self.bgImageView.userInteractionEnabled = YES;
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgImageView.image = [UIImage imageNamed:@"bussiness_rank"];
    [self addSubview:self.bgImageView];
    
    self.backButton = [[UIButton alloc] init];
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgImageView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.size.mas_equalTo(CGSizeMake(45, 30));
        make.top.mas_equalTo(self).mas_offset(FWCustomeSafeTop + 20);
    }];
    
    self.shareButton = [[UIButton alloc] init];
    [self.shareButton setImage:[UIImage imageNamed:@"new_white_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgImageView addSubview:self.shareButton];
    [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgImageView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.centerY.mas_equalTo(self.backButton);
    }];
    
    self.ruleButton = [[UIButton alloc] init];
    self.ruleButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [self.ruleButton setTitle:@"榜单规则" forState:UIControlStateNormal];
    [self.ruleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.ruleButton addTarget:self action:@selector(ruleButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgImageView addSubview:self.ruleButton];
    [self.ruleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.shareButton.mas_left).mas_offset(-5);
        make.size.mas_equalTo(CGSizeMake(60, 40));
        make.centerY.mas_equalTo(self.backButton);
    }];
}

- (void)setIndex:(NSInteger)index{
    _index = index;
    [self remarkSubViews];
}

- (void)remarkSubViews{
    
    if (self.index == 1) {
        self.bgImageView.image = [UIImage imageNamed:@"bussiness_header_bg"];
    }else if (self.index == 2){
        self.bgImageView.image = [UIImage imageNamed:@"person_header_bg"];
    }else{
        self.bgImageView.image = [UIImage imageNamed:@"placeholder"];
    }
}

#pragma mark - > 返回
- (void)backButtonOnClick:(UIButton *)sender{
    [self.vc.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{

    if ([self.delegate respondsToSelector:@selector(shareClick)]) {
        [self.delegate shareClick];
    }
}

#pragma mark - > 榜单规则
- (void)ruleButtonOnClick:(UIButton *)sender{
    
    NSString * message = self.rankModel.desc;
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"影响力榜单说明" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIView *subView1 = alertController.view.subviews[0];
    UIView *subView2 = subView1.subviews[0];
    UIView *subView3 = subView2.subviews[0];
    UIView *subView4 = subView3.subviews[0];
    UIView *subView5 = subView4.subviews[0];
    
    for (UILabel * label in subView5.subviews) {
        if ([label isKindOfClass:[UILabel class]]&&
            [label.text isEqualToString:message]) {
            label.textAlignment = NSTextAlignmentLeft;
        }
    }
    
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

@end
