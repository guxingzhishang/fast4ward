//
//  FWRankHeaderView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWTopRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWRankHeaderViewDelegate <NSObject>

- (void)shareClick;

@end

@interface FWRankHeaderView : UIView

/* 1:商家排行榜    2：车友排行榜*/
@property (nonatomic, assign) NSInteger  index;

@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIImageView * bgImageView;
@property (nonatomic, strong) UIButton * ruleButton;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, weak) FWTopRankModel * rankModel;
@property (nonatomic, weak) id<FWRankHeaderViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
