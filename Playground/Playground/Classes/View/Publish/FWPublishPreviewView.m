//
//  FWPublishPreviewView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWPublishPreviewView.h"
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import <AVFoundation/AVTime.h>
#import "FWSearchTagsListModel.h"
#import "FWChooseGoodsViewController.h"
#import "FWSettingModel.h"

@interface FWPublishPreviewView ()
@property (nonatomic, strong) FWSettingModel * settingModel;
@end

@implementation FWPublishPreviewView
@synthesize fengmianView;
@synthesize editButton;
@synthesize contentTextView;
@synthesize deleteButton;
@synthesize tagsButton;
@synthesize textViewHeight;
@synthesize publishButton;
@synthesize linkButton;
@synthesize hasLinkLabel;
@synthesize settingModel;
@synthesize hasTopicLabel;
@synthesize hasCarTypeLabel;
@synthesize carTypeButton;
@synthesize listView;

- (NSMutableArray *)atUserMutableArray{
    if (!_atUserMutableArray) {
        _atUserMutableArray = [[NSMutableArray alloc] init];
    }
    return _atUserMutableArray;
}

- (void)setPathURL:(NSURL *)pathURL{
    _pathURL = pathURL;
    
    fengmianView.image = [self getVideoPreViewImage:_pathURL];
}

- (id)init{
    self = [super init];
    if (self) {
        self.tempGoods_id = @"0";
        self.listCount = 2;

        [self requestSetting];
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    contentTextView = [[IQTextView alloc] init];
    contentTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    contentTextView.backgroundColor = [UIColor clearColor];
    contentTextView.delegate = self;
    contentTextView.textColor = FWTextColor_000000;
    contentTextView.placeholder = @"请输入描述...";
    contentTextView.placeholderTextColor = FWTextColor_ADADAD;
    contentTextView.textContainerInset = UIEdgeInsetsMake(5, 5, 0, 0);
    [self addSubview:contentTextView];
    contentTextView.frame = CGRectMake(15, 30, SCREEN_WIDTH-30, 96);

    fengmianView = [[UIImageView alloc] init];
    fengmianView.layer.cornerRadius = 3;
    fengmianView.layer.masksToBounds = YES;
    fengmianView.userInteractionEnabled = YES;
    fengmianView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:fengmianView];
    fengmianView.frame = CGRectMake(15, CGRectGetMaxY(contentTextView.frame)+20, 100, 100);
    UITapGestureRecognizer * fengmianTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editButtonOnClick)];
    [fengmianView addGestureRecognizer:fengmianTap];
    
    editButton = [[UIButton alloc] init];
    editButton.layer.cornerRadius = 10;
    editButton.layer.masksToBounds = YES;
    editButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:10];
    editButton.backgroundColor = FWColor(@"000000");
//    [editButton setImage:[UIImage imageNamed:@"publish_edit"] forState:UIControlStateNormal];
    [editButton setTitle:@"编辑封面" forState:UIControlStateNormal];
    [editButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(editButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:editButton];
    [editButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(fengmianView).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(CGRectGetWidth(fengmianView.frame), 20));
        make.bottom.mas_equalTo(fengmianView);
    }];

    
    listView = [[UIView alloc] init];
    [self addSubview:listView];
    listView.frame  =CGRectMake(CGRectGetMinX(contentTextView.frame), CGRectGetMaxY(fengmianView.frame)+20, CGRectGetWidth(contentTextView.frame), 180);
    
    deleteButton = [[UIButton alloc] init];
    deleteButton.frame = CGRectMake((SCREEN_WIDTH-180-36)/2, SCREEN_HEIGHT-(FWSafeBottom+40)-(64+FWCustomeSafeTop)-30, 92, 30);
    deleteButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    deleteButton.hidden = NO;
    deleteButton.layer.cornerRadius = 2;
    deleteButton.layer.masksToBounds = YES;
    deleteButton.layer.borderColor = FWTextColor_222222.CGColor;
    deleteButton.layer.borderWidth = 1;
    [deleteButton setTitle:@"存入草稿" forState:UIControlStateNormal];
    [deleteButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:deleteButton];

    publishButton = [[UIButton alloc] init];
    publishButton.frame=CGRectMake(CGRectGetMaxX(deleteButton.frame)+36, CGRectGetMinY(deleteButton.frame), CGRectGetWidth(deleteButton.frame), CGRectGetHeight(deleteButton.frame));
    publishButton.backgroundColor = FWTextColor_222222;
    publishButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    publishButton.layer.cornerRadius = 2;
    publishButton.layer.masksToBounds = YES;
    [publishButton setTitle:@"发布" forState:UIControlStateNormal];
    [publishButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [publishButton addTarget:self action:@selector(publishButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:publishButton];
}

- (void)setupList:(NSInteger)count{
    
    self.listCount = count;
    
    listView.frame  =CGRectMake(CGRectGetMinX(contentTextView.frame), CGRectGetMaxY(fengmianView.frame)+20, CGRectGetWidth(contentTextView.frame), self.listCount *50);

    for (UIView * view in listView.subviews) {
        [view removeFromSuperview];
    }
    
    NSArray * iconArray = @[@"link_topic",@"link_carType",@"link_goods"];
    NSArray * titleArray = @[@"添加话题",@"关联车系",@"关联商品"];

    //,@"link_at"  ,@"提醒谁看"
    for (int i = 0; i < count; i++) {
       UIButton * apartButton = [[UIButton alloc] init];
       apartButton.tag = 100578+i;
       [listView addSubview:apartButton];
       apartButton.frame = CGRectMake(0, 55*i , CGRectGetWidth(listView.frame), 55);
       
       UIView * upLineView= [[UIView alloc] init];
       upLineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
       [apartButton addSubview:upLineView];
       upLineView.frame = CGRectMake(0,0,CGRectGetWidth(listView.frame), 0.5);

       UIImageView * iconImageView = [[UIImageView alloc] init];
       iconImageView.image = [UIImage imageNamed:iconArray[i]];
       [apartButton addSubview:iconImageView];
       iconImageView.frame = CGRectMake(0, (55-19)/2, 19, 19);

        UILabel * leftLabel = [[UILabel alloc] init];
        leftLabel.font = DHFont(12);
        leftLabel.text = titleArray[i];
        leftLabel.textColor = FWColor(@"181615");
        leftLabel.textAlignment = NSTextAlignmentLeft;
        [apartButton addSubview:leftLabel];
        leftLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, 0, 100, 55);
        
       UIImageView * arrowImageView = [[UIImageView alloc] init];
       arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
       [apartButton addSubview:arrowImageView];
        [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(apartButton).mas_offset(-5);
            make.centerY.mas_equalTo(apartButton);
            make.width.mas_equalTo(6);
            make.height.mas_equalTo(11);
        }];
       
       UILabel * rightLabel = [[UILabel alloc] init];
       rightLabel.font = DHFont(12);
       rightLabel.textColor = FWColor(@"BCBCBC");
       rightLabel.textAlignment = NSTextAlignmentRight;
       [apartButton addSubview:rightLabel];
       [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
           make.centerY.mas_equalTo(apartButton);
           make.width.mas_greaterThanOrEqualTo(20);
           make.height.mas_equalTo(30);
       }];
       
       if (i == 0) {
           [apartButton addTarget:self action:@selector(tagsButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
           tagsButton = apartButton;
           
           rightLabel.text = @"去选择";
           hasTopicLabel = rightLabel;

       }else if (i == 1){
           [apartButton addTarget:self action:@selector(carTypeButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
           carTypeButton = apartButton;
           
           rightLabel.text = @"去关联";
           hasCarTypeLabel = rightLabel;
           
           if (count == 2) {
               UIView * downLineView = [[UIView alloc] init];
               downLineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
               [apartButton addSubview:downLineView];
               downLineView.frame = CGRectMake(0,55,CGRectGetWidth(listView.frame), 0.5);
           }
       }
//       else if (i == 2){
//           [apartButton addTarget:self action:@selector(atButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
//           self.atButton = apartButton;
//
//           self.atUserView = [[UIView alloc] init];
//           [self.atButton addSubview:self.atUserView];
//           [self.atUserView mas_remakeConstraints:^(MASConstraintMaker *make) {
//               make.height.mas_equalTo(55);
//               make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
//               make.centerY.mas_equalTo(self.atButton);
//               make.width.mas_greaterThanOrEqualTo(1);
//           }];
//
//           if (count == 3) {
//               UIView * downLineView = [[UIView alloc] init];
//               downLineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
//               [apartButton addSubview:downLineView];
//               downLineView.frame = CGRectMake(0,55,CGRectGetWidth(listView.frame), 0.5);
//           }
//       }
       else if (i == 2){
           [apartButton addTarget:self action:@selector(linkButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
           linkButton = apartButton;
           
           rightLabel.text = @"去关联";
           hasLinkLabel = rightLabel;
           
           if (count == 3) {
               UIView * downLineView = [[UIView alloc] init];
               downLineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
               [apartButton addSubview:downLineView];
               downLineView.frame = CGRectMake(0,55,CGRectGetWidth(listView.frame), 0.5);
           }
       }
    }
}

- (void)configForView:(id)model{
    
    FWFeedListModel * listModel = (FWFeedListModel *)model;
    if (listModel) {
        
        contentTextView.text = listModel.feed_title;
        
        [fengmianView sd_setImageWithURL:[NSURL URLWithString:listModel.feed_cover] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        fengmianView.contentMode = UIViewContentModeScaleAspectFill;
        fengmianView.clipsToBounds = YES;
        fengmianView.frame = CGRectMake(15, 20, 100, 133);
        
        if (listModel.goods_id) {
            self.tempGoods_id = listModel.goods_id;
        }else{
            self.tempGoods_id = @"0";
        }
        
        [self setupTagsView:listModel.tags];
    }
    
    FWUserDefaultsVariableModel * user_info = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    if ([user_info.userModel.cert_status isEqualToString:@"2"] ||
        ([user_info.userModel.merchant_cert_status isEqualToString:@"3"] && ![user_info.userModel.cert_status isEqualToString:@"2"])) {
        
        /* 需是认证过身份，并且是商家（非车友）*/
        [self setupList:3];

        if ([self.tempGoods_id isEqualToString:@"0"] || !self.tempGoods_id) {
            hasLinkLabel.hidden = YES;
        }else{
            hasLinkLabel.hidden = NO;
        }
    }else{
        [self setupList:2];
    }
    
    if (listModel.car_type.length >0 ) {
        [self setupCarTypeView:listModel.car_type];
    }else{
        hasTopicLabel.text = @"请选择";
    }
}

#pragma mark - > 关联商品
- (void)linkButtonOnClick{
    
    FWUserDefaultsVariableModel * user_info = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    if ([user_info.userModel.cert_status isEqualToString:@"2"]) {
        
        FWChooseGoodsViewController * CGVC = [[FWChooseGoodsViewController alloc] init];
        CGVC.goods_id = self.tempGoods_id ;
        CGVC.myBlock = ^(NSString * _Nonnull goods_id) {
            self.tempGoods_id = goods_id;
            if (goods_id.length > 0 && ![goods_id isEqualToString:@"0"]) {
                self.hasLinkLabel.hidden = NO;
            }else{
                self.hasLinkLabel.hidden = YES;
            }
        };
        [self.vc.navigationController pushViewController:CGVC animated:YES];
    }else{
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"升级成为肆放东家，立享开店权益，内容直链商品" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"了解东家权益" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            FWWebViewController * WVC = [[FWWebViewController alloc] init];
            WVC.pageType = WebViewTypeURL;
            WVC.htmlStr = settingModel.h5_cert;
            [self.vc.navigationController pushViewController:WVC animated:YES];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 提醒谁看
- (void)atButtonOnClick{
    FWPublishSearchUsersViewController * PSUVC = [[FWPublishSearchUsersViewController alloc] init];
    PSUVC.atUserMutableArray = self.atUserMutableArray;
    @weakify(self);
    PSUVC.chooseBlock = ^(FWMineInfoModel * _Nonnull userInfo) {
        if (userInfo.uid) {
            @strongify(self);
            [self.atUserMutableArray addObject:userInfo];
            [self setupAtView];
        }
    };
    [self.vc.navigationController pushViewController:PSUVC animated:YES];
}

#pragma mark - > 提醒的用户
- (void)setupAtView{
    
    for (UIView * view in self.atUserView.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat rightMargin = -5;
    for (int i = 0; i < self.atUserMutableArray.count ; i++) {
        FWMineInfoModel * userInfo = self.atUserMutableArray[i];
        
        UIImageView * photoImageView = [[UIImageView alloc] init];
        photoImageView.layer.cornerRadius = 22/2;
        photoImageView.layer.masksToBounds = YES;
        [photoImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_header"]];
        [self.atUserView addSubview:photoImageView];
        [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.atUserView);
            make.size.mas_equalTo(CGSizeMake(22, 22));
            make.right.mas_equalTo(self.atUserView).mas_offset(rightMargin);
            if (i == self.atUserMutableArray.count -1) {
                make.left.mas_equalTo(self.atUserView).mas_offset(-5);
            }
        }];
        
        UIButton * deleteButton = [[UIButton alloc] init];
        deleteButton.tag = 4455+i;
        [deleteButton setImage:[UIImage imageNamed:@"publish_delete_tags"] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(deleteAtButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.atUserView addSubview:deleteButton];
        [deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(photoImageView).mas_offset(8);
            make.top.mas_equalTo(photoImageView).mas_offset(-8);
            make.size.mas_equalTo(CGSizeMake(16, 16));
        }];
        
        rightMargin = rightMargin-22-5;
    }
}

#pragma mark - > 删除at的好友
- (void)deleteAtButtonOnClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 4455;
    
    if (self.atUserMutableArray.count > index) {
        [self.atUserMutableArray removeObjectAtIndex:index];
        [self setupAtView];
    }
}

#pragma mark - > 编辑封面
- (void)editButtonOnClick{
    
    if ([self.publishDelegate respondsToSelector:@selector(editButtonClick)]) {
        [self.publishDelegate editButtonClick];
    }
}

#pragma mark - > 删除草稿
- (void)deleteButtonOnClick:(UIButton *)sender{
    
    deleteButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        deleteButton.enabled = YES;
    });
    
    if ([self.publishDelegate respondsToSelector:@selector(deleteButtonClick:)]) {
        [self.publishDelegate deleteButtonClick:sender];
    }
}

#pragma mark - > 发布
- (void)publishButtonOnClick:(UIButton *)sender{
    
    publishButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        publishButton.enabled = YES;
    });
    
    if ([self.publishDelegate respondsToSelector:@selector(publishButtonClick:)]) {
        [self.publishDelegate publishButtonClick:sender];
    }
}

#pragma mark - > 选择标签
- (void)tagsButtonOnClick{
    
    if ([self.publishDelegate respondsToSelector:@selector(tagsButtonClick)]) {
        [self.publishDelegate tagsButtonClick];
    }
}


// 获取视频第一帧
- (UIImage*) getVideoPreViewImage:(NSURL *)path
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:path options:nil];
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return videoImage;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

#pragma mark - > 选择车系
- (void)carTypeButtonOnClick{
    if ([self.publishDelegate respondsToSelector:@selector(carTypeButtonClick)]) {
        [self.publishDelegate carTypeButtonClick];
    }
}

#pragma mark - > 选择车系后显示
- (void)setupCarTypeView:(NSString *)carType{
    
    if (carType.length <= 0) {
        for (UIView * view in hasCarTypeLabel.subviews) {
            [view removeFromSuperview];
        }
        return;
    }
    
    if(nil == hasCarTypeLabel){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self refreshContainer:hasCarTypeLabel WithText:carType];
        });
    }else{
        [self refreshContainer:hasCarTypeLabel WithText:carType];
    }
}

- (void)setupTagsView:(id)array{
    
    NSMutableArray * tempArray = (NSMutableArray *)array;
    if (tempArray.count <= 0) {
        for (UIView * view in hasTopicLabel.subviews) {
            [view removeFromSuperview];
        }
        return;
    }
    if(nil == hasTopicLabel){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            FWSearchTagsSubListModel * model = array[0];
            [self refreshContainer:hasTopicLabel WithText:model.tag_name];
        });
    }else{
        FWSearchTagsSubListModel * model = array[0];
        [self refreshContainer:hasTopicLabel WithText:model.tag_name];
    }
}

- (void) refreshContainer:(UILabel *)container WithText:(NSString *)text{

    if (nil == container) {
        return;
    }

    for (UIView * view in container.subviews) {
        [view removeFromSuperview];
    }
    
    if (text.length > 0 ) {

        container.text = @"";
        container.userInteractionEnabled = YES;
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat width = 0.0;
        CGFloat height = 28;

        NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc] initWithString:text];
        CGSize  textLimitSize = CGSizeMake(MAXFLOAT, height);
        width = [YYTextLayout layoutWithContainerSize:textLimitSize text:attribute].textBoundingSize.width+20;

        UIView * bgView = [[UIView alloc] init];
        bgView.backgroundColor = FWTextColor_222222;
        bgView.userInteractionEnabled = YES;
        bgView.layer.cornerRadius = 2;
        bgView.layer.masksToBounds = YES;
        bgView.frame = CGRectMake(x,y, width+30, height);
        [container addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(container).mas_offset(-5);
            make.centerY.mas_equalTo(container);
            make.width.mas_equalTo( width+30);
            make.height.mas_equalTo(height);
        }];

        UILabel * titleLable = [[UILabel alloc] init];
        titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        titleLable.textAlignment = NSTextAlignmentCenter;
        titleLable.text = [NSString stringWithFormat:@" %@",text];
        titleLable.textColor = FWViewBackgroundColor_FFFFFF;
        [bgView addSubview:titleLable];
        titleLable.frame = CGRectMake(5,0, width, height);

        UIButton * btn = [[UIButton alloc] init];
        if ([container isEqual:hasTopicLabel]) {
            btn.tag = 10000;
        }else{
            btn.tag = 10001;
        }
        [btn setImage:[UIImage imageNamed:@"delete_tag_new"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(deleteTagsOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:btn];
        btn.frame = CGRectMake(width+2,0, height, height);
    }else{
        container.text = @"去选择";
        container.userInteractionEnabled = NO;
    }
}

#pragma mark - > 删除选择的标签
- (void)deleteTagsOnClick:(UIButton *)sender{
    
    NSInteger btnTag = sender.tag - 10000;
    if ([self.publishDelegate respondsToSelector:@selector(deleteTagsClick:)]) {
        [self.publishDelegate deleteTagsClick:btnTag];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@""]){
        // 允许删除
        return YES;
    }
    if (textView == contentTextView) {
        if (textView.text.length >=1000) {
            
            [self endEditing:YES];
            [[FWHudManager sharedManager] showErrorMessage:@"最多只能输入1000个字哦~" toController:self.vc];
            
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return YES;
}


#pragma mark - > 请求基本配置参数
- (void)requestSetting{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_settings  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
        }
    }];
}

@end
