//
//  FWPublishPreviewRefitView.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/10.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWPublishPreviewRefitView.h"
#import "FWSearchTagsListModel.h"
#import "FWChooseGoodsViewController.h"
#import "FWEditRefitModuleViewController.h"
#import "FWChooseBradeViewController.h"


@interface FWPublishPreviewRefitView ()
@property (nonatomic, strong) FWSettingModel * settingModel;
@end

@implementation FWPublishPreviewRefitView

@synthesize imageContainer;
@synthesize contentTextView;
@synthesize deleteButton;
@synthesize tagsButton;
@synthesize listView;
@synthesize addImageView;
@synthesize publishButton;
@synthesize linkButton;
@synthesize hasLinkLabel;
@synthesize settingModel;
@synthesize hasTopicLabel;
@synthesize hasCarTypeLabel;
@synthesize carTypeButton;

- (NSMutableArray *)atUserMutableArray{
    if (!_atUserMutableArray) {
        _atUserMutableArray = [[NSMutableArray alloc] init];
    }
    return _atUserMutableArray;
}

- (void)dealloc{
//    self.refitQingdanView = nil;
    NSLog(@"------改装发布页销毁了---------");
}

- (void)setVc:(UIViewController *)vc{
    _vc = vc;
    addImageView.vc = self.vc;
}

- (void)setImages:(NSMutableArray *)images{
    _images = images;
    addImageView.showImage = self.images;
}

- (id)init{
    self = [super init];
    if (self) {
        self.tempGoods_id = @"0";
        self.listCount = 2;
        
        [self requestSetting];
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    
    self.carLabel = [[UILabel alloc] init];
    self.carLabel.font = DHFont(14);
    self.carLabel.userInteractionEnabled = YES;
    self.carLabel.text = @"原厂车型（必填）";
    self.carLabel.textColor = FWTextColor_222222;
    self.carLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.carLabel];
    self.carLabel.frame = CGRectMake(15, 20, SCREEN_WIDTH-30, 50);
    [self.carLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(carLabelClick)]];

    
    self.arrowImageView = [[UIImageView alloc] init];
    self.arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [self.carLabel addSubview:self.arrowImageView];
    [self.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.carLabel);
        make.size.mas_equalTo(CGSizeMake(8, 11));
        make.centerY.mas_equalTo(self.carLabel);
    }];
    
    self.toChooseLabel = [[UILabel alloc] init];
    self.toChooseLabel.font = DHFont(12);
    self.toChooseLabel.text = @"去选择";
    self.toChooseLabel.textColor = FWTextColor_BCBCBC;
    self.toChooseLabel.textAlignment = NSTextAlignmentRight;
    [self.carLabel addSubview:self.toChooseLabel];
    [self.toChooseLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.arrowImageView.mas_left).mas_offset(-5);
        make.size.mas_equalTo(CGSizeMake(60, 50));
        make.centerY.mas_equalTo(self.carLabel);
    }];

    
    self.carTypeLineView = [[UIView alloc] init];
    self.carTypeLineView.backgroundColor = FWColor(@"F4F4F4");
    [self addSubview:self.carTypeLineView];
    self.carTypeLineView.frame = CGRectMake(CGRectGetMinX(self.carLabel.frame), CGRectGetMaxY(self.carLabel.frame), CGRectGetWidth(self.carLabel.frame), 0.5);
    
    contentTextView = [[IQTextView alloc] init];
    contentTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    contentTextView.textContainerInset = UIEdgeInsetsMake(5, -5, 0, 0);
    contentTextView.backgroundColor = [UIColor clearColor];
    contentTextView.delegate = self;
    contentTextView.textColor = FWTextColor_12101D;
    contentTextView.placeholder = @"输入改装标题";
    contentTextView.placeholderTextColor = FWTextColor_BCBCBC;
    [self addSubview:contentTextView];
    contentTextView.frame = CGRectMake(15, CGRectGetMaxY(self.carTypeLineView.frame), SCREEN_WIDTH-30, 70);
     
    
    imageContainer = [[UIView alloc] init];
    imageContainer.frame = CGRectMake(0, CGRectGetMaxY(contentTextView.frame)+10, SCREEN_WIDTH, 50);
    imageContainer.clipsToBounds = YES;
    [self addSubview:imageContainer];

    addImageView = [[SZAddImage alloc] init];
    addImageView.delegate = self;
    addImageView.comeFrom = 1;
    addImageView.maxCount = 9;
    addImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetHeight(imageContainer.frame));
    [imageContainer addSubview:addImageView];

    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"小秘诀，上传改装前后对比照更增加曝光哦~";
    self.tipLabel.font = DHSystemFontOfSize_12;
    self.tipLabel.textColor = FWColor(@"#FA6400");
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.tipLabel];
    self.tipLabel.frame = CGRectMake(15, CGRectGetMaxY(imageContainer.frame)+5, SCREEN_WIDTH-30, 20);
    
    
    self.refitJianyaoView = [[UIView alloc] init];
    [self addSubview:self.refitJianyaoView];
    self.refitJianyaoView.frame  =CGRectMake(CGRectGetMinX(self.tipLabel.frame), CGRectGetMaxY(self.tipLabel.frame)+20, CGRectGetWidth(self.tipLabel.frame), 383);
    
    self.refitQingdanView = [[UIView alloc] init];
    [self addSubview:self.refitQingdanView];
    [self.refitQingdanView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.refitJianyaoView);
        make.top.mas_equalTo(self.refitJianyaoView.mas_bottom).mas_offset(20);
        make.width.mas_equalTo(self.refitJianyaoView);
        make.height.mas_greaterThanOrEqualTo(53*9+30);
    }];
    [self.refitQingdanView layoutIfNeeded];
    
    self.refitStoryView = [[UIView alloc] init];
    [self addSubview:self.refitStoryView];
    self.refitStoryView.frame  =CGRectMake(CGRectGetMinX(self.refitQingdanView.frame), CGRectGetMaxY(self.refitQingdanView.frame)+20, CGRectGetWidth(self.refitQingdanView.frame), 124);
    
    listView = [[UIView alloc] init];
    [self addSubview:listView];
    listView.frame  =CGRectMake(CGRectGetMinX(contentTextView.frame), CGRectGetMaxY(imageContainer.frame)+20, CGRectGetWidth(contentTextView.frame), 180);
    
    deleteButton = [[UIButton alloc] init];
    deleteButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    deleteButton.hidden = NO;
    deleteButton.layer.cornerRadius = 2;
    deleteButton.layer.masksToBounds = YES;
    deleteButton.layer.borderColor = FWTextColor_222222.CGColor;
    deleteButton.layer.borderWidth = 1.25;
    [deleteButton setTitle:@"存入草稿" forState:UIControlStateNormal];
    [deleteButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:deleteButton];
//    deleteButton.frame = CGRectMake((SCREEN_WIDTH-180-36)/2, SCREEN_HEIGHT-(FWSafeBottom+40)-(64+FWCustomeSafeTop)-30, 92, 30);
    [deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(92, 30));
        make.left.mas_equalTo((SCREEN_WIDTH-180-36)/2);
        make.bottom.mas_equalTo(self).mas_offset(-FWSafeBottom-40);
        make.top.mas_greaterThanOrEqualTo(listView.mas_bottom).mas_offset(100);
    }];

    publishButton = [[UIButton alloc] init];
//    publishButton.frame=CGRectMake(CGRectGetMaxX(deleteButton.frame)+36, CGRectGetMinY(deleteButton.frame), CGRectGetWidth(deleteButton.frame), CGRectGetHeight(deleteButton.frame));
    publishButton.backgroundColor = FWTextColor_222222;
    publishButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    publishButton.layer.cornerRadius = 2;
    publishButton.layer.masksToBounds = YES;
    [publishButton setTitle:@"发布" forState:UIControlStateNormal];
    [publishButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [publishButton addTarget:self action:@selector(publishButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:publishButton];
    [publishButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(deleteButton);
        make.left.mas_equalTo(deleteButton.mas_right).mas_offset(36);
        make.centerY.mas_equalTo(deleteButton);
    }];
}

#pragma mark - > 初始化改装简要
- (void)setupRefitJianyao{
    
    for (UIView * view in self.refitJianyaoView.subviews) {
        [view removeFromSuperview];
    }
    
    UILabel * jianyaoTitleLabel = [[UILabel alloc] init];
    jianyaoTitleLabel.text = @"改装简要（必填，没有请填无）";
    jianyaoTitleLabel.font = DHBoldFont(14);
    jianyaoTitleLabel.textColor = FWTextColor_222222;
    jianyaoTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.refitJianyaoView addSubview:jianyaoTitleLabel];
    jianyaoTitleLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH-30, 20);
    
    NSArray * placeholderArray = @[@"改装后马力（HP）",
                                   @"改装后扭矩（NM）",
                                   @"0-100km/h加速（S）",
                                   @"改装总花费（￥）",
                                   @"改装服务商",
                                   @"获得荣誉（多条荣誉请换行）"];
    
    for (int i =0; i < placeholderArray.count; i++) {
        
        IQTextView * jianyaoTF = [[IQTextView alloc] init];
        jianyaoTF.delegate = self;
        jianyaoTF.tag = 333+i;
        jianyaoTF.placeholder = placeholderArray[i];
        jianyaoTF.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
        jianyaoTF.textColor = FWTextColor_222222;
        jianyaoTF.textContainerInset = UIEdgeInsetsMake(20, -5, 0, 0);
        jianyaoTF.backgroundColor = [UIColor clearColor];
        jianyaoTF.placeholderTextColor = FWTextColor_BCBCBC;
        [self.refitJianyaoView addSubview:jianyaoTF];
        
        if (self.listModel.gaizhuang_jianyao.count > 0) {
            jianyaoTF.text = self.listModel.gaizhuang_jianyao[i].val;
        }
        
        UIView * jianyaoLineView = [[UIView alloc] init];
        jianyaoLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
        [self.refitJianyaoView addSubview:jianyaoLineView];
        
        if (i == 5) {
            jianyaoTF.frame = CGRectMake(0, CGRectGetMaxY(jianyaoTitleLabel.frame)+5+55*i, SCREEN_WIDTH-30, 84);
            jianyaoLineView.frame = CGRectMake(0, CGRectGetMaxY(jianyaoTF.frame)+0.5, SCREEN_WIDTH-30, 0.5);
        }else{
            jianyaoTF.frame = CGRectMake(0, CGRectGetMaxY(jianyaoTitleLabel.frame)+5+55*i, SCREEN_WIDTH-30, 54);
            jianyaoLineView.frame = CGRectMake(0, CGRectGetMaxY(jianyaoTF.frame)+0.5, SCREEN_WIDTH-30, 0.5);
        }
    }
}

#pragma mark - > 初始化改装故事
- (void)setupRefitStory{

    for (UIView * view in self.refitStoryView.subviews) {
        [view removeFromSuperview];
    }
    
    UILabel * storyTitleLabel = [[UILabel alloc] init];
    storyTitleLabel.font = DHBoldFont(14);
    storyTitleLabel.text = @"分享你的改装故事（必填）";
    storyTitleLabel.textColor = FWTextColor_222222;
    storyTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.refitStoryView addSubview:storyTitleLabel];
    storyTitleLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH-30, 20);

    IQTextView * storyTF = [[IQTextView alloc] init];
    storyTF.tag = 555;
    storyTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    storyTF.textContainerInset = UIEdgeInsetsMake(5, -5, 0, 0);
    storyTF.backgroundColor = [UIColor clearColor];
    storyTF.delegate = self;
    storyTF.textColor = FWTextColor_12101D;
    storyTF.placeholder = @"分享你的改装故事";
    storyTF.placeholderTextColor = FWTextColor_BCBCBC;
    [self.refitStoryView addSubview:storyTF];
    storyTF.frame = CGRectMake(0, CGRectGetMaxY(storyTitleLabel.frame)+5, SCREEN_WIDTH-30, 84);
    if (self.listModel.gaizhuang_content.length > 0) {
        storyTF.text = self.listModel.gaizhuang_content;
    }
    
    
//    UIView * storyLineView = [[UIView alloc] init];
//    storyLineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
//    [self.refitStoryView addSubview:storyLineView];
//    storyLineView.frame = CGRectMake(0, CGRectGetMaxY(storyTF.frame)+0.5, SCREEN_WIDTH-30, 0.5);
}


#pragma mark - > 初始化选择列表（话题、车系、商品）
- (void)setupList:(NSInteger)count{
    
    self.listCount = count;
    
    for (UIView * view in listView.subviews) {
        [view removeFromSuperview];
    }
    
    NSArray * iconArray = @[@"link_topic",@"link_goods"];
    NSArray * titleArray = @[@"添加话题",@"关联商品"];
    // ,@"link_at"   ,@"提醒谁看"
    
    for (int i = 0; i < count; i++) {
       UIButton * apartButton = [[UIButton alloc] init];
       apartButton.tag = 100578+i;
       [listView addSubview:apartButton];
       apartButton.frame = CGRectMake(0, 55*i , CGRectGetWidth(listView.frame), 55);
       
       UIView * upLineView= [[UIView alloc] init];
       upLineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
       [apartButton addSubview:upLineView];
       upLineView.frame = CGRectMake(0,0,CGRectGetWidth(listView.frame), 0.5);

       UIImageView * iconImageView = [[UIImageView alloc] init];
       iconImageView.image = [UIImage imageNamed:iconArray[i]];
       [apartButton addSubview:iconImageView];
       iconImageView.frame = CGRectMake(0, (55-19)/2, 19, 19);

        UILabel * leftLabel = [[UILabel alloc] init];
        leftLabel.font = DHFont(12);
        leftLabel.text = titleArray[i];
        leftLabel.textColor = FWColor(@"181615");
        leftLabel.textAlignment = NSTextAlignmentLeft;
        [apartButton addSubview:leftLabel];
        leftLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, 0, 100, 55);
        
       UIImageView * arrowImageView = [[UIImageView alloc] init];
       arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
       [apartButton addSubview:arrowImageView];
        [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(apartButton).mas_offset(-5);
            make.centerY.mas_equalTo(apartButton);
            make.width.mas_equalTo(6);
            make.height.mas_equalTo(11);
        }];
       
       UILabel * rightLabel = [[UILabel alloc] init];
       rightLabel.font = DHFont(12);
       rightLabel.textColor = FWColor(@"BCBCBC");
       rightLabel.textAlignment = NSTextAlignmentRight;
       [apartButton addSubview:rightLabel];
       [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
           make.centerY.mas_equalTo(apartButton);
           make.width.mas_greaterThanOrEqualTo(20);
           make.height.mas_equalTo(30);
       }];
       
       if (i == 0) {
           [apartButton addTarget:self action:@selector(tagsButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
           tagsButton = apartButton;
           
           rightLabel.text = @"去选择";
           hasTopicLabel = rightLabel;

       }
//       else if (i == 1){
//           [apartButton addTarget:self action:@selector(atButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
//           self.atButton = apartButton;
//
//           self.atUserView = [[UIView alloc] init];
//           [self.atButton addSubview:self.atUserView];
//           [self.atUserView mas_remakeConstraints:^(MASConstraintMaker *make) {
//               make.height.mas_equalTo(55);
//               make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
//               make.centerY.mas_equalTo(self.atButton);
//               make.width.mas_greaterThanOrEqualTo(1);
//           }];
//
//           if (count == 2) {
//               UIView * downLineView = [[UIView alloc] init];
//               downLineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
//               [apartButton addSubview:downLineView];
//               downLineView.frame = CGRectMake(0,55,CGRectGetWidth(listView.frame), 0.5);
//           }
//       }
       else if (i == 1){
           [apartButton addTarget:self action:@selector(carTypeButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
           carTypeButton = apartButton;

           rightLabel.text = @"去关联";
           hasCarTypeLabel = rightLabel;

           if (count == 2) {
               UIView * downLineView = [[UIView alloc] init];
               downLineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
               [apartButton addSubview:downLineView];
               downLineView.frame = CGRectMake(0,55,CGRectGetWidth(listView.frame), 0.5);
           }
       }
    }
}

- (void)configForView:(id)model{
    
    self.listModel = (FWFeedListModel *)model;
    
    if (self.listModel) {
        
        if (self.listModel.car_style.style.length <= 0 ||
            [self.listModel.car_style.style isEqualToString:@"无"]) {
            self.carLabel.text = @"原厂车型（必填）";
        }else{
            self.carLabel.text = self.listModel.car_style.style;
        }
        contentTextView.text = self.listModel.feed_title;
        [self setupTagsView:self.listModel.tags];
        
        if (self.listModel.goods_id) {
            self.tempGoods_id = self.listModel.goods_id;
        }else{
            self.tempGoods_id = @"0";
        }
    }
    
    
    
    [self setupRefitJianyao];
    [self setupQinqdanView];
    [self setupRefitStory];
    
    FWUserDefaultsVariableModel * user_info = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    if ([user_info.userModel.cert_status isEqualToString:@"2"] ||
        ([user_info.userModel.merchant_cert_status isEqualToString:@"3"] && ![user_info.userModel.cert_status isEqualToString:@"2"])) {
        
        /* 需是认证过身份，并且是商家（非车友）*/
        [self setupList:2];

        if ([self.tempGoods_id isEqualToString:@"0"] || !self.tempGoods_id) {
            hasLinkLabel.hidden = YES;
        }else{
            hasLinkLabel.hidden = NO;
        }
    }else{
        [self setupList:1];
    }
    
    if (self.listModel.car_type.length >0 ) {
        [self setupCarTypeView:self.listModel.car_type];
    }else{
        hasTopicLabel.text = @"请选择";
    }
    
    [self refreshFrame];
}

#pragma mark - > 关联商品
- (void)linkButtonOnClick{
    
    FWUserDefaultsVariableModel * user_info = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];

    if ([user_info.userModel.cert_status isEqualToString:@"2"]) {

        FWChooseGoodsViewController * CGVC = [[FWChooseGoodsViewController alloc] init];
        CGVC.goods_id = self.tempGoods_id ;
        CGVC.myBlock = ^(NSString * _Nonnull goods_id) {
            self.tempGoods_id = goods_id;
            if (goods_id.length > 0 && ![goods_id isEqualToString:@"0"]) {
                self.hasLinkLabel.hidden = NO;
            }else{
                self.hasLinkLabel.hidden = YES;
            }
        };
        [self.vc.navigationController pushViewController:CGVC animated:YES];
    }else{
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"升级成为肆放东家，立享开店权益，内容直链商品" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"了解东家权益" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            FWWebViewController * WVC = [[FWWebViewController alloc] init];
            WVC.pageType = WebViewTypeURL;
            WVC.htmlStr = settingModel.h5_cert;
            [self.vc.navigationController pushViewController:WVC animated:YES];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 提醒谁看
- (void)atButtonOnClick{
    FWPublishSearchUsersViewController * PSUVC = [[FWPublishSearchUsersViewController alloc] init];
    PSUVC.atUserMutableArray = self.atUserMutableArray;
    @weakify(self);
    PSUVC.chooseBlock = ^(FWMineInfoModel * _Nonnull userInfo) {
        if (userInfo.uid) {
            @strongify(self);
            [self.atUserMutableArray addObject:userInfo];
            [self setupAtView];
        }
    };
    [self.vc.navigationController pushViewController:PSUVC animated:YES];
}

#pragma mark - > 提醒的用户
- (void)setupAtView{
    
    for (UIView * view in self.atUserView.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat rightMargin = -5;
    for (int i = 0; i < self.atUserMutableArray.count ; i++) {
        FWMineInfoModel * userInfo = self.atUserMutableArray[i];
        
        UIImageView * photoImageView = [[UIImageView alloc] init];
        photoImageView.layer.cornerRadius = 22/2;
        photoImageView.layer.masksToBounds = YES;
        [photoImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_header"]];
        [self.atUserView addSubview:photoImageView];
        [photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.atUserView);
            make.size.mas_equalTo(CGSizeMake(22, 22));
            make.right.mas_equalTo(self.atUserView).mas_offset(rightMargin);
            if (i == self.atUserMutableArray.count -1) {
                make.left.mas_equalTo(self.atUserView).mas_offset(-5);
            }
        }];
        
        UIButton * deleteButton = [[UIButton alloc] init];
        deleteButton.tag = 4455+i;
        [deleteButton setImage:[UIImage imageNamed:@"publish_delete_tags"] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(deleteAtButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.atUserView addSubview:deleteButton];
        [deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(photoImageView).mas_offset(8);
            make.top.mas_equalTo(photoImageView).mas_offset(-8);
            make.size.mas_equalTo(CGSizeMake(16, 16));
        }];
        
        rightMargin = rightMargin-22-5;
    }
}

#pragma mark - > 删除at的好友
- (void)deleteAtButtonOnClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 4455;
    
    if (self.atUserMutableArray.count > index) {
        [self.atUserMutableArray removeObjectAtIndex:index];
        [self setupAtView];
    }
}


#pragma mark - > 增加或删除了图片
-(void)operationPicture{
    
    [self refreshFrame];
}

#pragma mark - > 重新计算布局
- (void)refreshFrame{
    
    [self layoutIfNeeded];
    
    NSInteger imageCount = [addImageView getImagesArray].count<9?[addImageView getImagesArray].count+1:9;
    
    CGFloat imageW = (SCREEN_WIDTH-60)/3;
    CGFloat marginY = 20;
    
    NSInteger y = (imageCount-1)/3+1;
    
    contentTextView.frame = CGRectMake(15, CGRectGetMaxY(self.carTypeLineView.frame), SCREEN_WIDTH-30, 70);
    imageContainer.frame = CGRectMake(0, CGRectGetMaxY(contentTextView.frame)+10, SCREEN_WIDTH, y*(imageW  + marginY));
    addImageView.frame =  CGRectMake(0, 0, SCREEN_WIDTH, y*(imageW  + marginY));
    
    self.tipLabel.frame = CGRectMake(15, CGRectGetMaxY(imageContainer.frame)+5, SCREEN_WIDTH-30, 20);
    self.refitJianyaoView.frame  =CGRectMake(CGRectGetMinX(self.tipLabel.frame), CGRectGetMaxY(self.tipLabel.frame)+20, CGRectGetWidth(self.tipLabel.frame), 383);

    [self.refitQingdanView layoutIfNeeded];

    self.refitStoryView.frame  =CGRectMake(CGRectGetMinX(self.refitQingdanView.frame), CGRectGetMaxY(self.refitQingdanView.frame)+20, CGRectGetWidth(self.refitQingdanView.frame), 124);
    listView.frame = CGRectMake(CGRectGetMinX(contentTextView.frame), CGRectGetMaxY(self.refitStoryView.frame)+20, CGRectGetWidth(contentTextView.frame), 55*self.listCount);
}

#pragma mark - > 删除草稿（存入草稿）
- (void)deleteButtonOnClick:(UIButton *)sender{
    
    deleteButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        deleteButton.enabled = YES;
    });
    

    if ([self.publishDelegate respondsToSelector:@selector(deleteButtonClick:)]) {
        [self.publishDelegate deleteButtonClick:sender];
    }
}

#pragma mark - > 发布
- (void)publishButtonOnClick:(UIButton *)sender{
    
    /* 改装简要 */
    NSArray * jianyaoArray = @[@"改装后马力（HP）",
                                @"改装后扭矩（NM）",
                                @"0-100km/h加速（S）",
                                @"改装总花费（￥）",
                                @"改装服务商",
                                @"获得荣誉（多条荣誉请换行）"];
    NSMutableArray * jianyaoTempArray = @[].mutableCopy;

    for ( int i =0; i<6; i++) {
        IQTextView * textView = [self.refitJianyaoView viewWithTag:333+i];
        self.jianyaoString = textView.text;

        if (self.jianyaoString.length <= 0) {
            [[FWHudManager sharedManager] showErrorMessage:[NSString stringWithFormat:@"请填写%@",jianyaoArray[i]] toController:self.vc];
            return;
        }

        NSDictionary * dict = @{
                                @"name":jianyaoArray[i],
                                @"val":self.jianyaoString,
                                };
        [jianyaoTempArray addObject:dict];
    }
    self.jianyaoString = [jianyaoTempArray mj_JSONString];

    /* 改装故事 */
    IQTextView * gushiTextView = [self.refitStoryView viewWithTag:555];
    self.gushiString = gushiTextView.text;

    
    /* 改装清单 */
    NSMutableArray * qingdanArray = @[].mutableCopy;
    for(int i = 0; i< self.categaryModel.list_gaizhuang_category.count;i++){
        
        /* 第一层数据 */
        NSMutableArray * subArray = @[].mutableCopy;
        FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[i];
        
        for (int j = 0; j <firstModel.sub.count; j++) {
        
            /* 第二层数据 */
            NSMutableArray * listArray = @[].mutableCopy;
            FWRefitCategarySubListModel * secondModel = firstModel.sub[j];
            
            if (secondModel.tempName.length > 0) {
                if ([secondModel.tempName containsString:@"原厂"]) {
                    /* 原厂 */
                    secondModel.val = @"原厂";
                    
                    for (int k = 0; k<secondModel.list.count; k++) {
                         
                        NSDictionary * thirdDict = @{
                            @"name":secondModel.list[k].name,
                            @"val":@"",
                            @"type":secondModel.list[k].type,
                         };
                        
                        [listArray addObject:thirdDict];
                    }
                }else {
                    /* 编辑了 */
                    secondModel.val = @"";
                    
                     for (int k = 0; k<secondModel.list.count; k++) {
                        
                         NSDictionary * thirdDict = @{
                             @"name":secondModel.list[k].name,
                             @"val":secondModel.list[k].val,
                             @"type":secondModel.list[k].type,
                          };
                         
                         [listArray addObject:thirdDict];
                     }
                }
            }else{
                /* 改装清单没填写完 */
                [[FWHudManager sharedManager] showErrorMessage:@"请完善改装清单" toController:self.vc];
                return;
            }
            
            NSDictionary * secondDict = @{
                @"name":secondModel.name,
                @"type":secondModel.type,
                @"val":secondModel.val,
                @"list":listArray,
            };
            
            [subArray addObject:secondDict];
        }
        
        NSDictionary * firstDict = @{
            @"name":firstModel.name,
            @"sub":subArray,
        };
        
        [qingdanArray addObject:firstDict];
    }
    
    self.qingdanString = [qingdanArray mj_JSONString];
    
    if (self.carLabel.text.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写原厂车型" toController:self.vc];
        return;
    }
    
    if (self.contentTextView.text.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写标题" toController:self.vc];
        return;
    }
    
    if (self.qingdanString.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写改装清单" toController:self.vc];
        return;
    }
    
    if (self.gushiString.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写改装故事" toController:self.vc];
        return;
    }

    
    publishButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        publishButton.enabled = YES;
    });
    
    if ([self.publishDelegate respondsToSelector:@selector(publishClick:)]) {
        [self.publishDelegate publishClick:sender];
    }
}

#pragma mark - > 选择标签
- (void)tagsButtonOnClick{

    if ([self.publishDelegate respondsToSelector:@selector(tagsButtonClick)]) {
        [self.publishDelegate tagsButtonClick];
    }
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

#pragma mark - > 选择车系
- (void)carTypeButtonOnClick{
    if ([self.publishDelegate respondsToSelector:@selector(carTypeButtonClick)]) {
        [self.publishDelegate carTypeButtonClick];
    }
}

#pragma mark - > 选择车系后显示
- (void)setupCarTypeView:(NSString *)carType{
    
    if (carType.length <= 0) {
        for (UIView * view in hasCarTypeLabel.subviews) {
            [view removeFromSuperview];
        }
        return;
    }
    
    if(nil == hasCarTypeLabel){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self refreshContainer:hasCarTypeLabel WithText:carType];
        });
    }else{
        [self refreshContainer:hasCarTypeLabel WithText:carType];
    }
}

- (void)setupTagsView:(id)array{
    
    NSMutableArray * tempArray = (NSMutableArray *)array;
    if (tempArray.count <= 0) {
        for (UIView * view in hasTopicLabel.subviews) {
            [view removeFromSuperview];
        }
        return;
    }
    
    if(nil == hasTopicLabel){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            FWSearchTagsSubListModel * model = array[0];
            [self refreshContainer:hasTopicLabel WithText:model.tag_name];
        });
    }else{
        FWSearchTagsSubListModel * model = array[0];
        [self refreshContainer:hasTopicLabel WithText:model.tag_name];
    }
}

- (void) refreshContainer:(UILabel *)container WithText:(NSString *)text{
    
    if (nil == container) {
        return;
    }
    for (UIView * view in container.subviews) {
        [view removeFromSuperview];
    }

    if (text.length > 0 ) {

        container.text = @"";
        container.userInteractionEnabled = YES;
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat width = 0.0;
        CGFloat height = 28;

        NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc] initWithString:text];
        CGSize  textLimitSize = CGSizeMake(MAXFLOAT, height);
        width = [YYTextLayout layoutWithContainerSize:textLimitSize text:attribute].textBoundingSize.width+20;

        UIView * bgView = [[UIView alloc] init];
        bgView.backgroundColor = FWTextColor_222222;
        bgView.userInteractionEnabled = YES;
        bgView.layer.cornerRadius = 2;
        bgView.layer.masksToBounds = YES;
        bgView.frame = CGRectMake(x,y, width+30, height);
        [container addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(container).mas_offset(-5);
            make.centerY.mas_equalTo(container);
            make.width.mas_equalTo( width+30);
            make.height.mas_equalTo(height);
        }];

        UILabel * titleLable = [[UILabel alloc] init];
        titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        titleLable.textAlignment = NSTextAlignmentCenter;
        titleLable.text = [NSString stringWithFormat:@" %@",text];
        titleLable.textColor = FWViewBackgroundColor_FFFFFF;
        [bgView addSubview:titleLable];
        titleLable.frame = CGRectMake(5,0, width, height);

        UIButton * btn = [[UIButton alloc] init];
        if ([container isEqual:hasTopicLabel]) {
            btn.tag = 10000;
        }else{
            btn.tag = 10001;
        }
        [btn setImage:[UIImage imageNamed:@"delete_tag_new"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(deleteTagsOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:btn];
        btn.frame = CGRectMake(width+2,0, height, height);
    }else{
        container.text = @"去选择";
        container.userInteractionEnabled = NO;
    }
}

#pragma mark - > 删除选择的标签
- (void)deleteTagsOnClick:(UIButton *)sender{
    
    NSInteger btnTag = sender.tag - 10000;

    if ([self.publishDelegate respondsToSelector:@selector(deleteTagsClick:)]) {
        [self.publishDelegate deleteTagsClick:btnTag];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@""]){
        // 允许删除
        return YES;
    }
    if (textView == contentTextView) {
        if (textView.text.length >=1000) {
            
            [self endEditing:YES];
            [[FWHudManager sharedManager] showErrorMessage:@"最多只能输入1000个字哦~" toController:self.vc];
            return NO;
        }
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    
    if (textView.text.length >1000) {

        textView.text = [textView.text substringToIndex:1000];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
   
    return YES;
}

#pragma mark - > 请求基本配置参数
- (void)requestSetting{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_settings  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
        }
    }];
}

#pragma mark - > 初始化改装清单
- (void)setupQinqdanView{
    
    for (UIView * view in self.refitQingdanView.subviews) {
        [view removeFromSuperview];
    }
    
    UILabel * qingdanTitleLabel = [[UILabel alloc] init];
    qingdanTitleLabel.font = DHBoldFont(14);
    qingdanTitleLabel.text = @"完成你的改装清单（必填）";
    qingdanTitleLabel.textColor = FWTextColor_222222;
    qingdanTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.refitQingdanView addSubview:qingdanTitleLabel];
    qingdanTitleLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH-30, 20);
    
    CGFloat currentY = 25;
    
    for(int i = 0; i< self.categaryModel.list_gaizhuang_category.count;i++){
        
        /* 第一层数据 */
        FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[i];
        
        UIView * firstView = [[UIView alloc] init];
        [self.refitQingdanView addSubview:firstView];
        [firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.refitQingdanView).mas_offset(currentY);
            make.left.right.mas_equalTo(qingdanTitleLabel);
            make.height.mas_equalTo(53);
        }];
        
        UIButton * firstOneKeyButton = [[UIButton alloc] init];
        firstOneKeyButton.tag = 12306+i;
        firstOneKeyButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
        firstOneKeyButton.layer.borderWidth = 1;
        firstOneKeyButton.layer.cornerRadius = 2;
        firstOneKeyButton.layer.masksToBounds = YES;
        firstOneKeyButton.titleLabel.font = DHFont(12);
        [firstOneKeyButton setTitle:@"一键原厂" forState:UIControlStateNormal];
        [firstOneKeyButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        [firstOneKeyButton addTarget:self action:@selector(onKeyToOriginal:) forControlEvents:UIControlEventTouchUpInside];
        [firstView addSubview:firstOneKeyButton];
        [firstOneKeyButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(firstView);
            make.size.mas_equalTo(CGSizeMake(108, 24));
            make.centerY.mas_equalTo(firstView);
        }];

        
        UIButton * firstEditButton = [[UIButton alloc] init];
        firstEditButton.tag = 12406+i;
        firstEditButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
        firstEditButton.layer.borderWidth = 1;
        firstEditButton.layer.cornerRadius = 2;
        firstEditButton.layer.masksToBounds = YES;
        firstEditButton.titleLabel.font = DHFont(12);
        [firstEditButton setTitle:@"编辑" forState:UIControlStateNormal];
        [firstEditButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        [firstEditButton addTarget:self action:@selector(firstEditClick:) forControlEvents:UIControlEventTouchUpInside];
        [firstView addSubview:firstEditButton];
        [firstEditButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(firstView);
            make.size.mas_equalTo(CGSizeMake(50, 24));
            make.centerY.mas_equalTo(firstView);
        }];
        
        UIButton * firstOrigianlButton = [[UIButton alloc] init];
        firstOrigianlButton.tag = 12506+i;
        firstOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
        firstOrigianlButton.layer.borderWidth = 1;
        firstOrigianlButton.layer.cornerRadius = 2;
        firstOrigianlButton.layer.masksToBounds = YES;
        firstOrigianlButton.titleLabel.font = DHFont(12);
        [firstOrigianlButton setTitle:@"原厂" forState:UIControlStateNormal];
        [firstOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        [firstOrigianlButton addTarget:self action:@selector(firstOriginalClick:) forControlEvents:UIControlEventTouchUpInside];
        [firstView addSubview:firstOrigianlButton];
        [firstOrigianlButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(firstEditButton.mas_left).mas_offset(-8);
            make.size.mas_equalTo(CGSizeMake(50, 24));
            make.centerY.mas_equalTo(firstView);
        }];

        
        UILabel * firstLabel = [[UILabel alloc] init];
        firstLabel.tag = 12606+i;
        firstLabel.userInteractionEnabled = YES;
        firstLabel.text = [NSString stringWithFormat:@"· %@",firstModel.name];
        firstLabel.font = DHSystemFontOfSize_16;
        firstLabel.textColor = FWTextColor_222222;
        firstLabel.textAlignment = NSTextAlignmentLeft;
        [firstView addSubview:firstLabel];
        [firstLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.mas_equalTo(firstView);
            make.right.mas_equalTo(firstView).mas_offset(-108);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
        [firstLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(firstQingdanLabelClick:)]];

        
        UIView * firstlineView = [[UIView alloc] init];
        firstlineView.backgroundColor = FWColor(@"F4F4F4");
        [firstView addSubview:firstlineView];
        [firstlineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(firstView);
            make.height.mas_equalTo(0.5);
            make.top.mas_equalTo(firstView.mas_bottom);
        }];
        
        if (firstModel.firstLevelType == 1) {
            /* 展开，显示一键原厂 */
            firstOneKeyButton.hidden = NO;
            firstEditButton.hidden = YES;
            firstOrigianlButton.hidden = YES;
        }else if (firstModel.firstLevelType == 2){
            /* 编辑完成，全隐藏 */
            firstOneKeyButton.hidden = YES;
            firstEditButton.hidden = YES;
            firstOrigianlButton.hidden = YES;
            firstLabel.userInteractionEnabled = NO;
        }else{
            /* 显示编辑和原厂 */
            firstOneKeyButton.hidden = YES;
            firstEditButton.hidden = NO;
            firstOrigianlButton.hidden = NO;
        }

        
        CGFloat secondCurrentY = 0;
        
        if (firstModel.firstLevelType != 0) {
            
            for (int j = 0; j <firstModel.sub.count; j++) {
                
                /* 第二层数据 */
                FWRefitCategarySubListModel * secondModel = firstModel.sub[j];
                
                UIView * secondView = [[UIView alloc] init];
                [self.refitQingdanView addSubview:secondView];
                [secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(firstView.mas_bottom).mas_offset(secondCurrentY);
                    make.left.right.mas_equalTo(qingdanTitleLabel);
                    make.height.mas_equalTo(53);
                    if (j == firstModel.sub.count-1 &&
                        i == self.categaryModel.list_gaizhuang_category.count - 1) {
                        make.bottom.mas_equalTo(self.refitQingdanView).mas_offset(-5);
                    }
                }];
                
                UILabel * secondLabel = [[UILabel alloc] init];
                secondLabel.numberOfLines = 2;
                secondLabel.font = DHSystemFontOfSize_14;
                secondLabel.textColor = FWColor(@"999999");
                secondLabel.textAlignment = NSTextAlignmentLeft;
                [secondView addSubview:secondLabel];
                [secondLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.bottom.left.mas_equalTo(secondView);
                    make.right.mas_equalTo(secondView).mas_offset(-108);
                    make.width.mas_greaterThanOrEqualTo(10);
                }];
                
                UIView * secondlineView = [[UIView alloc] init];
                secondlineView.backgroundColor = FWColor(@"F4F4F4");
                [secondView addSubview:secondlineView];
                [secondlineView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(secondView);
                    make.height.mas_equalTo(0.5);
                    make.top.mas_equalTo(secondView.mas_bottom);
                }];
                
                FWBlockButton * secondEditButton = [[FWBlockButton alloc] init];
                secondEditButton.tag = 124060+i;
                secondEditButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
                secondEditButton.layer.borderWidth = 1;
                secondEditButton.layer.cornerRadius = 2;
                secondEditButton.layer.masksToBounds = YES;
                secondEditButton.titleLabel.font = DHFont(12);
                [secondEditButton setTitle:@"编辑" forState:UIControlStateNormal];
                [secondEditButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
                [secondView addSubview:secondEditButton];
                [secondEditButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_equalTo(secondView);
                    make.size.mas_equalTo(CGSizeMake(50, 24));
                    make.centerY.mas_equalTo(secondView);
                }];
                
                FWBlockButton * secondOrigianlButton = [[FWBlockButton alloc] init];
                secondOrigianlButton.tag = 125060+i;
                secondOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
                secondOrigianlButton.layer.borderWidth = 1;
                secondOrigianlButton.layer.cornerRadius = 2;
                secondOrigianlButton.layer.masksToBounds = YES;
                secondOrigianlButton.titleLabel.font = DHFont(12);
                [secondOrigianlButton setTitle:@"原厂" forState:UIControlStateNormal];
                [secondOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
                [secondView addSubview:secondOrigianlButton];
                [secondOrigianlButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_equalTo(secondEditButton.mas_left).mas_offset(-8);
                    make.size.mas_equalTo(CGSizeMake(50, 24));
                    make.centerY.mas_equalTo(secondView);
                }];
                
                __weak typeof(self) weakSelf = self;
                [secondEditButton addButtonTapBlock:^(UIButton * _Nonnull sender) {
                    /* 二级编辑 */
                    FWEditRefitModuleViewController * RMVC = [[FWEditRefitModuleViewController alloc] init];
                    RMVC.type = secondModel.name;
                    RMVC.dict = secondModel.secondDict;
                    RMVC.moduleBlock = ^(NSDictionary * _Nonnull moduleDict) {

                        for (int k = 0;k < secondModel.list.count; k++) {
                            if ([secondModel.list[k].name isEqualToString:@"品牌"]) {
                                 secondModel.list[k].val = moduleDict[@"品牌"];
                             }else  if ([secondModel.list[k].name isEqualToString:@"型号"]) {
                                 secondModel.list[k].val = moduleDict[@"型号"];
                             }else  if ([secondModel.list[k].name isEqualToString:@"规格"]) {
                                 secondModel.list[k].val = moduleDict[@"规格"];
                             }else  if ([secondModel.list[k].name isEqualToString:@"费用"]) {
                                 secondModel.list[k].val = moduleDict[@"费用"];
                             }
                             secondModel.secondDict = moduleDict;
                        }
 
                        secondModel.tempName = [NSString stringWithFormat:@"%@\n%@",secondModel.name,moduleDict[@"moduleString"]];
                        secondLabel.text = secondModel.tempName;
                        secondLabel.textColor = FWTextColor_222222;
                        
                        secondOrigianlButton.enabled = YES;
                        secondOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
                        [secondOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];

                        
                        secondModel.secondLevelType = 2;
                        firstModel.isFirstOriginalType = 2;
                    };
                    [weakSelf.vc.navigationController pushViewController:RMVC animated:YES];
                }];
                
                
                /* 非一键原厂才可以点击设置原厂 */
                [secondOrigianlButton addButtonTapBlock:^(UIButton * _Nonnull sender) {
                    if (firstModel.isFirstOriginalType != 1) {
                        /* 二级设置原厂 */
                        secondModel.secondLevelType = 1;
                        
                        secondModel.tempName = [NSString stringWithFormat:@"%@-原厂",secondModel.name];
                        secondLabel.text = secondModel.tempName;
                        secondLabel.textColor = FWColor(@"222222");
                        
                        secondOrigianlButton.enabled = NO;
                        secondOrigianlButton.layer.borderColor = FWColor(@"EEEEEE").CGColor;
                        [secondOrigianlButton setTitleColor:FWViewBackgroundColor_EEEEEE forState:UIControlStateNormal];
                    }
                }];

                if (secondModel.tempName.length > 0) {
                    secondLabel.text = secondModel.tempName;
                }else{
                    secondLabel.text = secondModel.name;
                }

                if (secondModel.secondLevelType == 0) {
                    /* 初始状态 */
                    secondLabel.textColor = FWColor(@"999999");
                    
                    secondOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
                    [secondOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
                }else{
                    secondLabel.textColor = FWColor(@"666666");
                    if (secondModel.secondLevelType == 1) {
                        /* 原厂 */
                        secondOrigianlButton.enabled = NO;
                        secondOrigianlButton.layer.borderColor = FWColor(@"EEEEEE").CGColor;
                        [secondOrigianlButton setTitleColor:FWViewBackgroundColor_EEEEEE forState:UIControlStateNormal];
                    }else{
                        /* 非原厂 */
                        secondOrigianlButton.enabled = YES;
                        secondOrigianlButton.layer.borderColor = FWColor(@"dcdcdc").CGColor;
                        [secondOrigianlButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
                    }
                }
                
                [self.refitQingdanView layoutIfNeeded];
                secondCurrentY = secondCurrentY+CGRectGetHeight(secondView.frame);
            }

        }else{
            [firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.refitQingdanView).mas_offset(currentY);
                make.left.right.mas_equalTo(qingdanTitleLabel);
                make.height.mas_equalTo(53);
                if (i == self.categaryModel.list_gaizhuang_category.count - 1) {
                    make.bottom.mas_equalTo(self.refitQingdanView).mas_offset(-5);
                }
            }];
        }
        
        [self.refitQingdanView layoutIfNeeded];
        currentY = currentY + CGRectGetHeight(firstView.frame) +secondCurrentY;
    }
    
    [self refreshFrame];
}

#pragma mark - > 一键原厂
- (void)onKeyToOriginal:(UIButton *)sender{
    NSInteger index = sender.tag - 12306;
    
    FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[index];
    
    if (firstModel.isFirstOriginalType != 1) {
        /* 非一键原厂才可以设置 */
        firstModel.isFirstOriginalType = 1;
        firstModel.firstLevelType = 2;

        for (int i =0; i<firstModel.sub.count; i++) {
            FWRefitCategarySubListModel * model = firstModel.sub[i];
            model.secondLevelType = 1;
            model.tempName = [NSString stringWithFormat:@"%@-原厂",model.name];
        }
        
        [self setupQinqdanView];
    }
}


#pragma mark - > 第一级原厂
- (void)firstOriginalClick:(UIButton *)sender{
    NSInteger index = sender.tag - 12506;
    
    FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[index];

    if (firstModel.isFirstOriginalType != 1) {
        
        [self showFirstSubView:index];
        
        /* 非一键原厂才可以设置 */
        firstModel.isFirstOriginalType = 1;
        firstModel.firstLevelType = 2;
        
         for (int i =0; i<firstModel.sub.count; i++) {
             FWRefitCategarySubListModel * model = firstModel.sub[i];
             model.secondLevelType = 1;
             model.tempName = [NSString stringWithFormat:@"%@-原厂",model.name];
         }
        [self setupQinqdanView];
    }
    
}

#pragma mark - > 第一级点击item展示第二级
- (void)firstEditClick:(UIButton *)sender{
    NSInteger index = sender.tag - 12406;
    [self showFirstSubView:index];
}

- (void)firstQingdanLabelClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 12606;
    [self showFirstSubView:index];
}

- (void)showFirstSubView:(NSInteger)index{
    
    FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[index];

    if (firstModel.isFirstOriginalType != 0) {
        /* 一旦设置后就禁止在收缩起来 */
        return;
    }
    
    for (int i =0; i<firstModel.sub.count; i++) {
        FWRefitCategarySubListModel * model = firstModel.sub[i];
        if (model.secondLevelType != 0) {
            /* 只要有一个修改过就不允许缩起来 */
            return;
        }
    }
    
    if (self.categaryModel.list_gaizhuang_category[index].firstLevelType == 1) {
        self.categaryModel.list_gaizhuang_category[index].firstLevelType = 0;
    }else{
        self.categaryModel.list_gaizhuang_category[index].firstLevelType = 1;
    }
    [self setupQinqdanView];
}

- (void)setCategaryModel:(FWRefitCategaryModel *)categaryModel{
    _categaryModel = categaryModel;
    
    [self setupQinqdanView];
}

#pragma mark - > 选择车系
- (void)carLabelClick{
    
    FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
    CBVC.chooseFrom = @"chooseChexi";
    [self.vc.navigationController pushViewController:CBVC animated:YES];
}
@end
