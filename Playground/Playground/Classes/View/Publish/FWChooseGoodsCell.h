//
//  FWChooseGoodsCell.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWChooseGoodsCellDelegate <NSObject>

- (void)chooseButtonClick:(NSString *)goods_id;

@end

@interface FWChooseGoodsCell : UITableViewCell

@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIImageView * cardImageView;
@property (nonatomic, strong) UIImageView * scanImageView;
@property (nonatomic, strong) UILabel * scanLabel;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * priceLabel;

@property (nonatomic, strong) UIButton * chooseButton;
@property (nonatomic, assign) BOOL isChoose;

@property (nonatomic, weak) id<FWChooseGoodsCellDelegate> delegate;

- (void)configForShopCell:(id)model;
- (void)resetChooseButton:(BOOL)Choose;

@end

NS_ASSUME_NONNULL_END
