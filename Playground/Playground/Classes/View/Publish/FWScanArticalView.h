//
//  FWScanArticalView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWScanArticalView : UIView

@property (weak, nonatomic) IBOutlet UIView *upView;

@property (weak, nonatomic) IBOutlet UIView *downView;

@property (weak, nonatomic) IBOutlet UILabel *upNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *upValueLabel;
@property (weak, nonatomic) IBOutlet UIButton *copysButton;

@property (weak, nonatomic) IBOutlet UILabel *downLabel;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@property (nonatomic, weak) UIViewController * vc;


@end

NS_ASSUME_NONNULL_END
