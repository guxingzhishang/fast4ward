//
//  FWPublishCameraView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/7.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWPublishCameraView.h"

@implementation FWPublishCameraView
@synthesize mainView;
@synthesize backButton;
@synthesize videoButton;
@synthesize articalButton;
@synthesize photographButton;
@synthesize draftButton;

- (id)init{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        self.backgroundColor =FWViewBackgroundColor_FFFFFF;
        
        [self setupMainView];
    }
    
    return self;
}

#pragma mark - > 视图初始化
- (void)setupMainView{
    
    mainView = [[UIImageView alloc] init];
    mainView.backgroundColor = FWClearColor;
    mainView.userInteractionEnabled = YES;
    [self addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    [mainView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backButtonOnClick)]];

    self.idleButton = [[UIImageView alloc] init];
    self.idleButton.userInteractionEnabled = YES;
    self.idleButton.contentMode = UIViewContentModeScaleToFill;
//    self.idleButton.clipsToBounds = YES;
    [self.idleButton sd_setImageWithURL:[NSURL URLWithString:[GFStaticData getObjectForKey:kPublishIdelImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.idleButton.frame = CGRectMake(22, SCREEN_HEIGHT-200-FWSafeBottom-45-80, SCREEN_WIDTH-44, 45);
    [mainView addSubview:self.idleButton];
    [self.idleButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(idleButtonOnClick)]];

//    self.idleButton = [[UIButton alloc] init];
//    [self.idleButton setImage:[UIImage imageNamed:@"xianzhi.jpg"] forState:UIControlStateNormal];
////    self.idleButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
////    self.idleButton.layer.borderWidth = 1;
////    [self.idleButton setTitle:@"这是一张图片，用来发布闲置贴" forState:UIControlStateNormal];
////    [self.idleButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
//    [self.idleButton addTarget:self action:@selector(idleButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
//    [mainView addSubview:self.idleButton];
    
    NSArray * imageArray = @[@"publish_video",@"publish_camera",@"publish_refit",@"publish_ask",@"publish_artical"];
    NSArray * titleArray = @[@"小视频",@"图片",@"改装分享",@"提问",@"PC发文"];
    
    for(int i = 0;i < titleArray.count;i++){
        
        UIButton * button = [[UIButton alloc] init];
        button.tag = 10086+i;
        [button addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:button];
        button.frame = CGRectMake( i*SCREEN_WIDTH/5, SCREEN_HEIGHT-200-FWSafeBottom-45, SCREEN_WIDTH/5, 75);
        
        UIImageView * iconImageView = [[UIImageView alloc] init];
        iconImageView.image = [UIImage imageNamed:imageArray[i]];
        [button addSubview:iconImageView];

        UILabel * iconLabel = [[UILabel alloc] init];
        iconLabel.font = DHSystemFontOfSize_14;
        iconLabel.textColor = FWTextColor_222222;
        iconLabel.textAlignment = NSTextAlignmentCenter;
        iconLabel.text = titleArray[i];
        [button addSubview:iconLabel];
        
        [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(button);
            make.size.mas_equalTo(CGSizeMake(48, 48));
            make.top.mas_equalTo(button);
        }];
        [iconLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(button);
            make.top.mas_equalTo(iconImageView.mas_bottom).mas_offset(10);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/5, 19));
        }];
    }

    draftButton = [[UIButton alloc] init];
    draftButton.layer.cornerRadius = 2;
    draftButton.layer.masksToBounds = YES;
    draftButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
    draftButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [draftButton setTitle:@"  草稿箱" forState:UIControlStateNormal];
    [draftButton setImage:[UIImage imageNamed:@"mine_draft"] forState:UIControlStateNormal];
    [draftButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
    [draftButton addTarget:self action:@selector(draftButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:draftButton];
    [self.draftButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.left.mas_equalTo(self).mas_offset(54);
        make.right.mas_equalTo(self).mas_offset(-54);
        make.height.mas_equalTo(34);
        make.width.mas_greaterThanOrEqualTo(10);
//        make.top.mas_equalTo(self.videoButton.mas_bottom).mas_offset(30);
        make.top.mas_equalTo(self).mas_offset(SCREEN_HEIGHT-200-FWSafeBottom-45+75+40);
    }];
    
    backButton = [[UIButton alloc] init];
    [backButton setImage:[UIImage imageNamed:@"publish_close"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:backButton];
    [backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mainView);
        make.bottom.mas_equalTo(mainView).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
}


#pragma mark - > 返回上一页面
- (void)backButtonOnClick{
    
    [UIView beginAnimations:@"rotation1" context:nil];
    [UIView setAnimationDuration:0.4];
    backButton.transform = CGAffineTransformMakeRotation(-M_PI/4);
    [UIView commitAnimations];
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.4];
    [animation setType: kCATransitionReveal];
    [animation setSubtype: kCATransitionFromBottom];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    [self.vc.navigationController.view.layer addAnimation:animation forKey:nil];
    [self.vc.navigationController popViewControllerAnimated:NO];
}

- (void)buttonOnClick:(UIButton *)sender{
    
    if (sender.tag == 10086) {
        [self videoButtonOnClick];
    }else if (sender.tag == 10087){
        [self photographButtonOnClick];
    }else if (sender.tag == 10088){
        [self refitButtonOnClick];
    }else if (sender.tag == 10089){
        [self askButtonOnClick];
    }else if (sender.tag == 10090){
        [self articalButtonOnClick];
    }
}

#pragma mark - > 改装分享
- (void)refitButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(refitButtonClick)]) {
        [self.delegate refitButtonClick];
    }
}

#pragma mark - > 提问
- (void)askButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(askButtonClick)]) {
        [self.delegate askButtonClick];
    }
}

#pragma mark - > 拍视频
- (void)videoButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(videoButtonClick)]) {
        [self.delegate videoButtonClick];
    }
}

#pragma mark - > 拍照
- (void)photographButtonOnClick{

    if ([self.delegate respondsToSelector:@selector(photographButtonClick)]) {
        [self.delegate photographButtonClick];
    }
}

#pragma mark - > 文章
- (void)articalButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(articalButtonClick)]) {
        [self.delegate articalButtonClick];
    }
}

#pragma mark - > 查看草稿
- (void)draftButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(draftButtonClick)]) {
        [self.delegate draftButtonClick];
    }
}

#pragma mark - > 发布闲置
- (void)idleButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(idleButtonClick)]) {
        [self.delegate idleButtonClick];
    }
}

@end
