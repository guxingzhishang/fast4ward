//
//  FWPublishPreviewRefitView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/10.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTextView.h"
#import "SZAddImage.h"
#import <IQTextView.h>
#import "FWRefitCategaryModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWPublishPreviewRefitViewDelegate <NSObject>

#pragma mark - > 删除草稿（存入草稿）
- (void)deleteButtonClick:(UIButton *)sender;

#pragma mark - > 发布
- (void)publishClick:(UIButton *)sender;

#pragma mark - > 选择标签
- (void)tagsButtonClick;

#pragma mark - > 选择车系
- (void)carTypeButtonClick;

#pragma mark - > 保存在本地
- (void)saveButtonClick;

#pragma mark - > 删除标签
- (void)deleteTagsClick:(NSInteger)index;

@end

@interface FWPublishPreviewRefitView : UIScrollView<UIScrollViewDelegate,UITextViewDelegate,SZAddImageDelegate,UITextFieldDelegate>

@property (nonatomic, strong) UIView * mainView;

/* new property */
//@property (nonatomic, strong) UITextField * carTypeTF;
@property (nonatomic, strong) UILabel * carLabel;
@property (nonatomic, strong) UILabel * toChooseLabel;
@property (nonatomic, strong) UIImageView * arrowImageView;

@property (nonatomic, strong) UIView * carTypeLineView;

/* original property */
@property (nonatomic, strong) IQTextView * contentTextView;
@property (nonatomic, strong) UIView * imageContainer;
@property (nonatomic, strong) SZAddImage * addImageView;

/* new property */
@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UIView * refitJianyaoView; // 改装简要
@property (nonatomic, strong) UIView * refitQingdanView; // 改装清单
@property (nonatomic, strong) UIView * refitStoryView;   // 改装故事

/* original property */
@property (nonatomic, strong) UIButton * deleteButton;
@property (nonatomic, strong) UIButton * publishButton;

@property (nonatomic, strong) UIView * listView;

@property (nonatomic, strong) UIButton * tagsButton;
@property (nonatomic, strong) UIButton * linkButton;
@property (nonatomic, strong) UIButton * carTypeButton;
@property (nonatomic, strong) UIButton * atButton;

@property (nonatomic, strong) UIView * atUserView;
@property (nonatomic, strong) NSMutableArray * atUserMutableArray;

@property (nonatomic, strong) UILabel * hasLinkLabel;
@property (nonatomic, strong) UILabel * hasTopicLabel;
@property (nonatomic, strong) UILabel * hasCarTypeLabel;

@property (nonatomic, assign) NSInteger  listCount;

@property (nonatomic, weak) id<FWPublishPreviewRefitViewDelegate> publishDelegate;

@property (nonatomic, strong) NSURL * pathURL;

@property (nonatomic, strong) NSString * tempGoods_id;// 用于临时存放关联商品的id

@property (nonatomic, assign) CGFloat textViewHeight;

@property (nonatomic, strong) FWFeedListModel * listModel;

@property (nonatomic, assign) NSInteger editType;

/**
 *  存储所有的照片(UIImage)
 */
@property (nonatomic, strong) NSMutableArray *images;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) FWRefitCategaryModel * categaryModel;

@property (nonatomic, strong) NSString * qingdanString;
@property (nonatomic, strong) NSString * gushiString;
@property (nonatomic, strong) NSString * jianyaoString;

- (void)configForView:(id)model;

/* 设置标签视图 */
- (void)setupTagsView:(id)array;

/* 选择车系 */
- (void)setupCarTypeView:(NSString *)carType;

@end

NS_ASSUME_NONNULL_END
