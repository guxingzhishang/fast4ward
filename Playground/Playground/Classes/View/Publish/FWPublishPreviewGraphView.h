//
//  FWPublishPreviewGraphView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/11.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTextView.h"
#import "SZAddImage.h"
#import <IQTextView.h>

@protocol FWPublishPreviewGraphViewDelegate <NSObject>

#pragma mark - > 删除草稿（存入草稿）
- (void)deleteButtonClick:(UIButton *)sender;

#pragma mark - > 发布
- (void)publishClick:(UIButton *)sender;

#pragma mark - > 选择标签
- (void)tagsButtonClick;

#pragma mark - > 选择车系
- (void)carTypeButtonClick;

#pragma mark - > 保存在本地
- (void)saveButtonClick;

#pragma mark - > 删除标签
- (void)deleteTagsClick:(NSInteger)index;

@end
@interface FWPublishPreviewGraphView : UIScrollView<UIScrollViewDelegate,UITextViewDelegate,SZAddImageDelegate>

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIView * imageContainer;

@property (nonatomic, strong) SZAddImage * addImageView;

@property (nonatomic, strong) IQTextView * contentTextView;

@property (nonatomic, strong) UIButton * deleteButton;

@property (nonatomic, strong) UIButton * publishButton;

@property (nonatomic, strong) UIView * listView;

@property (nonatomic, strong) UIButton * tagsButton;
@property (nonatomic, strong) UIButton * linkButton;
@property (nonatomic, strong) UIButton * carTypeButton;
@property (nonatomic, strong) UIButton * atButton;

@property (nonatomic, strong) UIView * atUserView;
@property (nonatomic, strong) NSMutableArray * atUserMutableArray;

@property (nonatomic, strong) UILabel * hasLinkLabel;
@property (nonatomic, strong) UILabel * hasTopicLabel;
@property (nonatomic, strong) UILabel * hasCarTypeLabel;

@property (nonatomic, assign) NSInteger  listCount;

@property (nonatomic, weak) id<FWPublishPreviewGraphViewDelegate> publishDelegate;

@property (nonatomic, strong) NSURL * pathURL;

@property (nonatomic, strong) NSString * tempGoods_id;// 用于临时存放关联商品的id

@property (nonatomic, assign) CGFloat textViewHeight;

@property (nonatomic, strong) FWFeedListModel * listModel;

/**
 *  存储所有的照片(UIImage)
 */
@property (nonatomic, strong) NSMutableArray *images;

@property (nonatomic, weak) UIViewController * vc;

- (void)configForView:(id)model;

/* 设置标签视图 */
- (void)setupTagsView:(id)array;

/* 选择车系 */
- (void)setupCarTypeView:(NSString *)carType;
@end
