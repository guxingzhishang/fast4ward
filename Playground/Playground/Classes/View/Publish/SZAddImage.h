//
//  SZAddImage.h
//  demo
//
//  Created by 孤星之殇 on 2018/10/11.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SZAddImageDelegate <NSObject>

#pragma mark - > 对图片进行了操作（添加或删除）
- (void)operationPicture;

@end

@interface SZAddImage : UIView<UIGestureRecognizerDelegate>

/**
*  存储所有的照片(UIImage)
*/
@property (nonatomic, strong) NSMutableArray *images;

@property (nonatomic, strong) NSMutableArray * showImage;

@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, weak) id<SZAddImageDelegate> delegate;

/* 1、上传图文贴（9张图片）   2、上传商品展示图（9张图片）   3、上传商品详情（6张图片） 4、店铺信息（3张图片）*/
@property (nonatomic, assign) NSInteger  comeFrom;

/* 最多显示图片个数 */
@property (nonatomic, assign) NSInteger  maxCount;

- (void)stop;

- (NSMutableArray *)getImagesArray;

@end
