//
//  FWPublishPreviewIdleView.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/19.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "STTextView.h"
#import "SZAddImage.h"
#import <IQTextView.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FWPublishPreviewIdleViewDelegate <NSObject>

#pragma mark - > 删除草稿（存入草稿）
- (void)deleteButtonClick:(UIButton *)sender;

#pragma mark - > 发布
- (void)publishClick:(UIButton *)sender;

#pragma mark - > 选择标签
- (void)tagsButtonClick;

#pragma mark - > 选择车系
- (void)carTypeButtonClick;

#pragma mark - > 保存在本地
- (void)saveButtonClick;

#pragma mark - > 删除标签
- (void)deleteTagsClick:(NSInteger)index;

@end
@interface FWPublishPreviewIdleView : UIScrollView<UIScrollViewDelegate,UITextViewDelegate,SZAddImageDelegate>

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, strong) UIView * imageContainer;

@property (nonatomic, strong) SZAddImage * addImageView;

@property (nonatomic, strong) IQTextView * contentTextView;

@property (nonatomic, strong) UILabel * tipLable;

@property (nonatomic, strong) UIButton * deleteButton;

@property (nonatomic, strong) UIButton * publishButton;

@property (nonatomic, strong) UIView * fahuoView;

@property (nonatomic, strong) UIView * priceView;

@property (nonatomic, strong) UIView * otherView;

@property (nonatomic, strong) UILabel * nimingLabel;
@property (nonatomic, strong) UISwitch * nimingSwitch;

@property (nonatomic, strong) NSDictionary * priceDictionary;
@property (nonatomic, strong) NSDictionary * otherDictionary;
@property (nonatomic, strong) NSDictionary * fahuoDictionary;

@property (nonatomic, strong) NSString * provice ;
@property (nonatomic, strong) NSString * city ;
@property (nonatomic, strong) NSString * country ;

@property (nonatomic, weak) id<FWPublishPreviewIdleViewDelegate> publishDelegate;

@property (nonatomic, assign) CGFloat textViewHeight;

@property (nonatomic, strong) FWFeedListModel * listModel;

@property (nonatomic, assign) NSInteger  isDeny;
/**
 *  存储所有的照片(UIImage)
 */
@property (nonatomic, strong) NSMutableArray *images;

@property (nonatomic, weak) UIViewController * vc;

- (void)configForView:(id)model;

@end

NS_ASSUME_NONNULL_END
