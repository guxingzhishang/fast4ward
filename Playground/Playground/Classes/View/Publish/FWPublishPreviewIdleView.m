//
//  FWPublishPreviewIdleView.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/19.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWPublishPreviewIdleView.h"
#import "FWSearchTagsListModel.h"
#import "FWChooseGoodsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "FWPublishIdlePriceViewController.h"
#import "FWChooseBradeViewController.h"
#import "FWPublishIdleOtherViewController.h"
#import "FWPublishIdleRegionViewController.h"

@interface FWPublishPreviewIdleView ()<CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *manager;

@property (nonatomic, strong) FWSettingModel * settingModel;
@property (nonatomic, strong) NSString * fahuoString;
@property (nonatomic, strong) NSString * priceString;
@property (nonatomic, strong) NSString * otherString;

@property (nonatomic, assign) CGFloat  priceHeight;
@property (nonatomic, assign) CGFloat  otherHeight;

@end

@implementation FWPublishPreviewIdleView

@synthesize imageContainer;
@synthesize contentTextView;
@synthesize deleteButton;
@synthesize fahuoView;
@synthesize priceView;
@synthesize otherView;
@synthesize addImageView;
@synthesize publishButton;
@synthesize settingModel;

- (void)setVc:(UIViewController *)vc{
    _vc = vc;
    addImageView.vc = self.vc;
}

- (void)setImages:(NSMutableArray *)images{
    _images = images;
    addImageView.showImage = self.images;
}

- (id)init{
    self = [super init];
    if (self) {
        
        self.fahuoString = @"";
        self.priceString = @"";
        self.otherString = @"";
        
        self.priceHeight = 0;
        self.otherHeight = 0;

        self.otherDictionary = @{};
        self.priceDictionary = @{};
        self.fahuoDictionary = @{};

        
        // 判断定位操作是否被允许
        _manager = [[CLLocationManager alloc] init];
        [_manager requestWhenInUseAuthorization];//申请定位服务权限
        _manager.delegate = self;
        //设置定位精度
        _manager.desiredAccuracy = kCLLocationAccuracyBest;
        //定位频率,每隔多少米定位一次
        CLLocationDistance distance = 10.0;//十米定位一次
        _manager.distanceFilter = distance;
        [_manager startUpdatingLocation];
        
        
        [self requestSetting];
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    contentTextView = [[IQTextView alloc] init];
    contentTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    contentTextView.textContainerInset = UIEdgeInsetsMake(5, 0, 0, 0);
    contentTextView.backgroundColor = [UIColor clearColor];
    contentTextView.delegate = self;
    contentTextView.textColor = FWTextColor_12101D;
    contentTextView.placeholder = @"品牌型号，新旧程度，适用车型，转手原因......";
    contentTextView.placeholderTextColor = FWTextColor_AEAEAE;
    [self addSubview:contentTextView];
    contentTextView.frame = CGRectMake(15, 30, SCREEN_WIDTH-30, 96);
     
    self.tipLable = [[UILabel alloc] init];
    self.tipLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];;
    self.tipLable.textColor = FWTextColor_BCBCBC;
    self.tipLable.textAlignment = NSTextAlignmentLeft;
    self.tipLable.text = @"品牌型号，新旧程度，适用车型，转手原因";
    [self addSubview:self.tipLable];
    self.tipLable.frame = CGRectMake(15, CGRectGetMaxY(contentTextView.frame)+5, SCREEN_WIDTH-30, 20);
    self.tipLable.hidden = YES;
    //提示:输入框内请输入
    imageContainer = [[UIView alloc] init];
    imageContainer.frame = CGRectMake(0, CGRectGetMaxY(contentTextView.frame)+5, SCREEN_WIDTH, 50);
    imageContainer.clipsToBounds = YES;
    [self addSubview:imageContainer];

    addImageView = [[SZAddImage alloc] init];
    addImageView.delegate = self;
    addImageView.comeFrom = 1;
    addImageView.maxCount = 9;
    addImageView.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetHeight(imageContainer.frame));
    [imageContainer addSubview:addImageView];

    fahuoView = [[UIView alloc] init];
    fahuoView.userInteractionEnabled = YES;
    [self addSubview:fahuoView];
    fahuoView.frame  =CGRectMake(CGRectGetMinX(contentTextView.frame), CGRectGetMaxY(imageContainer.frame)+20, CGRectGetWidth(contentTextView.frame), 50);
    [fahuoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fahuoViewClick)]];

    
    priceView = [[UIView alloc] init];
    [self addSubview:priceView];
    priceView.frame  =CGRectMake(CGRectGetMinX(fahuoView.frame), CGRectGetMaxY(fahuoView.frame)+1, CGRectGetWidth(fahuoView.frame), CGRectGetHeight(fahuoView.frame));
    [priceView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(priceViewClick)]];

    otherView = [[UIView alloc] init];
    [self addSubview:otherView];
    otherView.frame  =CGRectMake(CGRectGetMinX(priceView.frame), CGRectGetMaxY(priceView.frame)+1, CGRectGetWidth(priceView.frame), CGRectGetHeight(fahuoView.frame));
    [otherView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(otherViewClick)]];

    self.nimingSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-46-14, CGRectGetMaxY(self.otherView.frame)+14, 46, 26)];
    self.nimingSwitch.on = NO;
    self.nimingSwitch.tintColor = FWViewBackgroundColor_EEEEEE;
    self.nimingSwitch.onTintColor = FWTextColor_222222;
    [self addSubview:self.nimingSwitch];
    [self.nimingSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:(UIControlEventValueChanged)];
    
    self.nimingLabel = [[UILabel alloc] init];
    self.nimingLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.nimingLabel.textColor = FWTextColor_222222;
    self.nimingLabel.text = @"匿名";
    self.nimingLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.nimingLabel];
    
    [self setupFahuoView];
    [self setupPriceView];
    [self setupOtherView];
    
    self.nimingSwitch.frame = CGRectMake(SCREEN_WIDTH-46-14, CGRectGetMaxY(self.otherView.frame)+14, 46, 26);
    self.nimingLabel.frame = CGRectMake(CGRectGetMinX(self.nimingSwitch.frame)- 10-100, CGRectGetMinY(self.nimingSwitch.frame)+5, 100, 26);
    
    
    deleteButton = [[UIButton alloc] init];
    deleteButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    deleteButton.hidden = NO;
    deleteButton.layer.cornerRadius = 2;
    deleteButton.layer.masksToBounds = YES;
    deleteButton.layer.borderColor = FWTextColor_222222.CGColor;
    deleteButton.layer.borderWidth = 1.25;
    [deleteButton setTitle:@"存入草稿" forState:UIControlStateNormal];
    [deleteButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:deleteButton];
    [deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(92, 30));
        make.left.mas_equalTo((SCREEN_WIDTH-180-36)/2);
        make.bottom.mas_equalTo(self).mas_offset(-FWSafeBottom-50);
        make.top.mas_greaterThanOrEqualTo(self.nimingSwitch.mas_bottom).mas_offset(100);
    }];

    publishButton = [[UIButton alloc] init];
    publishButton.backgroundColor = FWTextColor_222222;
    publishButton.titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    publishButton.layer.cornerRadius = 2;
    publishButton.layer.masksToBounds = YES;
    [publishButton setTitle:@"发布" forState:UIControlStateNormal];
    [publishButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [publishButton addTarget:self action:@selector(publishButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:publishButton];
    [publishButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(deleteButton);
        make.left.mas_equalTo(deleteButton.mas_right).mas_offset(36);
        make.centerY.mas_equalTo(deleteButton);
    }];
}

#pragma mark - > 发货地
- (void)fahuoViewClick{
  
    DHWeakSelf;
    FWPublishIdleRegionViewController * RVC = [[FWPublishIdleRegionViewController alloc] init];
    RVC.isDeny = self.isDeny;
    if (self.isDeny == 1) {
        RVC.currentRegion = [NSString stringWithFormat:@"%@ %@ %@",self.provice,self.city,self.country];
    }
    RVC.regionBlock = ^(NSString * _Nonnull provice, NSString * _Nonnull city, NSString * _Nonnull county) {
        weakSelf.fahuoString = [NSString stringWithFormat:@"%@%@%@",provice,city,county];
        weakSelf.provice = provice;
        weakSelf.city = city;
        weakSelf.country = county;
        [weakSelf setupFahuoView];
    };
    [self.vc.navigationController pushViewController:RVC animated:YES];
}

#pragma mark - > 去开价
- (void)priceViewClick{
    
    DHWeakSelf;
    FWPublishIdlePriceViewController * vc = [[FWPublishIdlePriceViewController alloc] init];
    vc.priceDictionary = self.priceDictionary;
    vc.priceBlock = ^(NSDictionary * _Nonnull dict) {
        weakSelf.priceDictionary = dict;
        weakSelf.priceString = [NSString stringWithFormat:@"原    价：￥%@\n出手价：￥%@\n邮    费：%@",dict[@"yuanjia"],dict[@"chushoujia"],dict[@"baoyou"]];
        [weakSelf setupPriceView];
    };
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.vc.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.vc presentViewController:vc animated:YES completion:nil];
}

#pragma mark - > 其他
- (void)otherViewClick{
    
    DHWeakSelf;
    FWPublishIdleOtherViewController * otherVC = [[FWPublishIdleOtherViewController alloc] init];
    otherVC.otherDict = self.otherDictionary;
    otherVC.otherBlock = ^(NSDictionary * _Nonnull dict) {
        
        weakSelf.otherDictionary = [dict copy];
        weakSelf.otherString = [NSString stringWithFormat:@"分       类：%@\n适用车系：%@\n话       题：%@\n其       他：%@ %@",dict[@"xianzhi_category_name"],dict[@"car_type"],dict[@"tags_show"],dict[@"quanxin"],dict[@"ziti"]];
        [weakSelf setupOtherView];
    };
    [self.vc.navigationController pushViewController:otherVC animated:YES];
}

#pragma mark - > 匿名
- (void)switchValueChanged:(UISwitch *)sender{
    
    if (sender.on) {
        // 开启匿名
        NSLog(@"开启");
    }else{
        NSLog(@"关闭");
    }
}

- (void)setupFahuoView{
    
    for (UIView * view in fahuoView.subviews) {
        [view removeFromSuperview];
    }
    
    UIImageView * iconImageView = [[UIImageView alloc] init];
    iconImageView.image = [UIImage imageNamed:@""];
    [fahuoView addSubview:iconImageView];
    iconImageView.frame = CGRectMake(0, (50-19)/2, 0.01, 19);

    UILabel * leftLabel = [[UILabel alloc] init];
    leftLabel.font = DHFont(12);
    leftLabel.text = @"发货地";
    leftLabel.textColor = FWColor(@"222222");
    leftLabel.textAlignment = NSTextAlignmentLeft;
    [fahuoView addSubview:leftLabel];
    leftLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, 0, 100, 50);

    UIImageView * arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [fahuoView addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.right.mas_equalTo(fahuoView).mas_offset(-5);
       make.centerY.mas_equalTo(fahuoView);
       make.width.mas_equalTo(6);
       make.height.mas_equalTo(11);
    }];

    if (self.fahuoString.length <= 0) {

        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = DHFont(12);
        rightLabel.text = @"去选择";
        rightLabel.textColor = FWColor(@"BCBCBC");
        rightLabel.textAlignment = NSTextAlignmentRight;
        [fahuoView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
            make.centerY.mas_equalTo(fahuoView);
            make.width.mas_greaterThanOrEqualTo(20);
            make.height.mas_equalTo(30);
        }];
    }else{
        
        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = DHFont(12);
        rightLabel.text = self.fahuoString;
        rightLabel.textColor = FWColor(@"222222");
        rightLabel.textAlignment = NSTextAlignmentRight;
        [fahuoView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
          make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
          make.centerY.mas_equalTo(fahuoView);
          make.width.mas_greaterThanOrEqualTo(20);
          make.height.mas_equalTo(30);
        }];
    }
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [fahuoView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(fahuoView);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(fahuoView);
    }];
    
    [self refreshFrame];
}

- (void)setupPriceView{
    
    for (UIView * view in priceView.subviews) {
        [view removeFromSuperview];
    }

    UIImageView * iconImageView = [[UIImageView alloc] init];
    iconImageView.image = [UIImage imageNamed:@""];
    [priceView addSubview:iconImageView];
    iconImageView.frame = CGRectMake(0, (50-19)/2, 0.01, 19);

    UILabel * leftLabel = [[UILabel alloc] init];
    leftLabel.font = DHFont(12);
    leftLabel.text = @"价格";
    leftLabel.textColor = FWColor(@"222222");
    leftLabel.textAlignment = NSTextAlignmentLeft;
    [priceView addSubview:leftLabel];
    leftLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, 0, 100, CGRectGetHeight(priceView.frame));

    UIImageView * arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [priceView addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.right.mas_equalTo(priceView).mas_offset(-5);
       make.centerY.mas_equalTo(priceView);
       make.width.mas_equalTo(6);
       make.height.mas_equalTo(11);
    }];

    if (self.priceString.length <= 0) {

        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = DHFont(12);
        rightLabel.text = @"去开价";
        rightLabel.textColor = FWColor(@"BCBCBC");
        rightLabel.textAlignment = NSTextAlignmentRight;
        [priceView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
          make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
          make.centerY.mas_equalTo(priceView);
          make.width.mas_greaterThanOrEqualTo(20);
          make.height.mas_equalTo(30);
        }];
        
        self.priceHeight = 50;
    }else{

        priceView.frame  =CGRectMake(CGRectGetMinX(fahuoView.frame), CGRectGetMaxY(fahuoView.frame)+1, CGRectGetWidth(fahuoView.frame), 60);

        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = DHFont(12);
        rightLabel.numberOfLines = 0;
        rightLabel.text = self.priceString;
        rightLabel.textColor = FWColor(@"222222");
        rightLabel.textAlignment = NSTextAlignmentLeft;
        [priceView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
            make.centerY.mas_equalTo(priceView);
            make.width.mas_greaterThanOrEqualTo(20);
            make.height.mas_equalTo(55);
        }];
        self.priceHeight = 60;
    }
    
    priceView.frame  =CGRectMake(CGRectGetMinX(fahuoView.frame), CGRectGetMaxY(fahuoView.frame)+1, CGRectGetWidth(fahuoView.frame), self.priceHeight);
    leftLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, 0, 100, CGRectGetHeight(priceView.frame));

    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [priceView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(priceView);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(priceView);
    }];
    
    [self refreshFrame];
}

- (void)setupOtherView{
    
    for (UIView * view in otherView.subviews) {
        [view removeFromSuperview];
    }
    
    UIImageView * iconImageView = [[UIImageView alloc] init];
    iconImageView.image = [UIImage imageNamed:@""];
    [otherView addSubview:iconImageView];
    iconImageView.frame = CGRectMake(0, (50-19)/2, 0.01, 19);

    UILabel * leftLabel = [[UILabel alloc] init];
    leftLabel.font = DHFont(12);
    leftLabel.text = @"其他";
    leftLabel.textColor = FWColor(@"222222");
    leftLabel.textAlignment = NSTextAlignmentLeft;
    [otherView addSubview:leftLabel];
    leftLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, 0, 100, 50);

    UIImageView * arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"right_arrow"];
    [otherView addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.right.mas_equalTo(otherView).mas_offset(-5);
       make.centerY.mas_equalTo(otherView);
       make.width.mas_equalTo(6);
       make.height.mas_equalTo(11);
    }];

    if (self.otherString.length <= 0) {
        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = DHFont(12);
        rightLabel.text = @"去选择";
        rightLabel.textColor = FWColor(@"BCBCBC");
        rightLabel.textAlignment = NSTextAlignmentRight;
        [otherView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
          make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
          make.centerY.mas_equalTo(otherView);
          make.width.mas_greaterThanOrEqualTo(20);
          make.height.mas_equalTo(30);
        }];
        
        self.otherHeight = 50;
    }else{
        
        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = DHFont(12);
        rightLabel.numberOfLines = 0;
        rightLabel.text = self.otherString;
        rightLabel.textColor = FWColor(@"222222");
        rightLabel.textAlignment = NSTextAlignmentLeft;
        [otherView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(arrowImageView.mas_left).mas_offset(-5);
            make.top.mas_equalTo(otherView);
            make.width.mas_greaterThanOrEqualTo(20);
            make.height.mas_equalTo(80);
            make.bottom.mas_equalTo(otherView);
        }];
        self.otherHeight = 80;
    }
    
    otherView.frame  =CGRectMake(CGRectGetMinX(priceView.frame), CGRectGetMaxY(priceView.frame)+1, CGRectGetWidth(priceView.frame), self.otherHeight);
    leftLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, 0, 100, CGRectGetHeight(otherView.frame));
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [otherView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(otherView);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(otherView);
    }];
    
    [self refreshFrame];
}

- (void)configForView:(id)model{
    
    self.listModel = (FWFeedListModel *)model;
    
    if (self.listModel) {

        contentTextView.text = self.listModel.feed_title;
        
        if ([self.listModel.status_niming isEqualToString:@"2"]) {
            self.nimingSwitch.on = YES;
        }
        
        if (self.listModel.province_name.length > 0) {
            
            self.fahuoString = [NSString stringWithFormat:@"%@%@%@",self.listModel.province_name,self.listModel.city_name,self.listModel.county_name];
            [self setupFahuoView];
        }
        
        if (self.listModel.car_type.length >0 ||
            self.listModel.tags.count > 0 ||
            [self.listModel.if_ziti isEqualToString:@"1"] ||
            [self.listModel.if_quanxin isEqualToString:@"1"] ) {
            
            NSString * zitiString = @"";
            NSString * ifZiti = @"2";
            
            NSString * allNewString = @"";
            NSString * ifQuanxin = @"2";
            
            NSString * xianzhi_category_id = self.listModel.xianzhi_category_id?self.listModel.xianzhi_category_id:@"";
            NSString * xianzhi_category_name = self.listModel.xianzhi_category_name?self.listModel.xianzhi_category_name:@"";

            NSString * topicString = @"";
            NSString * tags = @"";
            if (self.listModel.tags.count > 0) {
                topicString = self.listModel.tags[0].tag_name;
                tags = self.listModel.tags[0].tag_id;
            }
            
            NSString * carTypeString = self.listModel.car_type?self.listModel.car_type:@"";
  
            if ([self.listModel.if_ziti isEqualToString:@"1"]) {
                zitiString = @"自提";
                ifZiti = @"1";
            }

            if ([self.listModel.if_quanxin isEqualToString:@"1"]) {
                allNewString = @"全新";
                ifQuanxin = @"1";
            }
            
            if ([self.listModel.if_quanxin isEqualToString:@"1"]) {
                allNewString = @"全新";
                ifQuanxin = @"1";
            }
            
            NSDictionary * dict = @{
                @"xianzhi_category_id":xianzhi_category_id,
                @"xianzhi_category_name":xianzhi_category_name,
                @"car_type":carTypeString,
                @"tags":tags,
                @"tags_show":topicString,
                @"ziti":zitiString,
                @"quanxin":allNewString,
                @"if_ziti":ifZiti,
                @"if_quanxin":ifQuanxin,
            };
            
            
            self.otherDictionary = dict;
            self.otherString = [NSString stringWithFormat:@"分       类：%@\n适用车系：%@\n话       题：%@\n其       他：%@ %@",dict[@"xianzhi_category_name"],dict[@"car_type"],dict[@"tags_show"],dict[@"quanxin"],dict[@"ziti"]];
            [self setupOtherView];
        }

        NSString * baoyou = @"";
        if ([self.listModel.freight intValue] > 0 ||
            self.listModel.price_sell_show.length > 0 ||
            self.listModel.price_buy_show.length > 0 ) {
            baoyou = @"按距离计算";
            if ([self.listModel.freight isEqualToString:@"2"]) {
                baoyou = @"包邮";
            }
            
            NSString * yuanjia = self.listModel.price_buy_show;
            NSString * chushoujia = self.listModel.price_sell_show;

            if (yuanjia.length > 0 && [[yuanjia substringToIndex:1] isEqualToString:@"￥"]) {
                yuanjia = [yuanjia substringFromIndex:1];
            }
            
            if (chushoujia.length > 0 && [[chushoujia substringToIndex:1] isEqualToString:@"￥"]) {
                chushoujia = [chushoujia substringFromIndex:1];
            }
            
            NSDictionary * pricedict = @{
                @"yuanjia":yuanjia,
                @"chushoujia":chushoujia,
                @"baoyou":baoyou,
            };
            
            self.priceDictionary = pricedict;
            self.priceString = [NSString stringWithFormat:@"原    价：￥%@\n出手价：￥%@\n邮    费：%@",pricedict[@"yuanjia"],pricedict[@"chushoujia"],pricedict[@"baoyou"]];
            [self setupPriceView];
        }
    }
    
    [self refreshFrame];
}

#pragma mark - > 增加或删除了图片
-(void)operationPicture{
    
    [self refreshFrame];
}

#pragma mark - > 重新计算布局
- (void)refreshFrame{
    
    [self layoutIfNeeded];
    
    NSInteger imageCount = [addImageView getImagesArray].count<9?[addImageView getImagesArray].count+1:9;
    
    CGFloat imageW = (SCREEN_WIDTH-60)/3;
    CGFloat marginY = 20;
    
    NSInteger y = (imageCount-1)/3+1;
    
    contentTextView.frame = CGRectMake(15, 30, SCREEN_WIDTH-30, 96);
    
    self.tipLable.frame = CGRectMake(15, CGRectGetMaxY(contentTextView.frame)+5, SCREEN_WIDTH-30, 20);
    
    imageContainer.frame = CGRectMake(0, CGRectGetMaxY(self.tipLable.frame)+5, SCREEN_WIDTH, y*(imageW  + marginY));
    
    addImageView.frame =  CGRectMake(0, 0, SCREEN_WIDTH, y*(imageW  + marginY));

    fahuoView.frame  =CGRectMake(CGRectGetMinX(contentTextView.frame), CGRectGetMaxY(imageContainer.frame)+20, CGRectGetWidth(contentTextView.frame), 50);
    
    priceView.frame  =CGRectMake(CGRectGetMinX(fahuoView.frame), CGRectGetMaxY(fahuoView.frame)+1, CGRectGetWidth(fahuoView.frame), self.priceHeight);

    otherView.frame  =CGRectMake(CGRectGetMinX(priceView.frame), CGRectGetMaxY(priceView.frame)+1, CGRectGetWidth(priceView.frame), self.otherHeight);

    self.nimingSwitch.frame = CGRectMake(SCREEN_WIDTH-46-14, CGRectGetMaxY(self.otherView.frame)+14, 46, 26);
    self.nimingLabel.frame = CGRectMake(CGRectGetMinX(self.nimingSwitch.frame)- 10-100, CGRectGetMinY(self.nimingSwitch.frame)+5, 100, 26);
}

#pragma mark - > 删除草稿（存入草稿）
- (void)deleteButtonOnClick:(UIButton *)sender{
    
    deleteButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        deleteButton.enabled = YES;
    });
    
    
    if ([self.publishDelegate respondsToSelector:@selector(deleteButtonClick:)]) {
        [self.publishDelegate deleteButtonClick:sender];
    }
}

#pragma mark - > 发布
- (void)publishButtonOnClick:(UIButton *)sender{
    
    publishButton.enabled = NO;
    
    [self endEditing:YES];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        publishButton.enabled = YES;
    });
    
    if (self.contentTextView.text.length < 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"描述不能为空" toController:self.vc];
        return;
    }else if (self.contentTextView.text.length > 1000) {
        [[FWHudManager sharedManager] showErrorMessage:@"描述最多1000字" toController:self.vc];
        return;
    }
    
    if (self.provice.length <= 0 ||
        self.city.length <= 0 ||
        self.country.length <= 0 ) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择发货地区" toController:self.vc];
        return;
    }
    
    if ([self.priceDictionary[@"yuanjia"] length] <= 0){
        [[FWHudManager sharedManager] showErrorMessage:@"请输入原价" toController:self.vc];
        return;
    }
    
    if ([self.priceDictionary[@"chushoujia"] length] <= 0){
        [[FWHudManager sharedManager] showErrorMessage:@"请输入出手价" toController:self.vc];
        return;
    }
    
    if ([self.priceDictionary[@"baoyou"] length] <= 0 ) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择运费方式" toController:self.vc];
        return;
    }
    
    if ([self.publishDelegate respondsToSelector:@selector(publishClick:)]) {
        [self.publishDelegate publishClick:sender];
    }
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@""]){
        // 允许删除
        return YES;
    }
    if (textView == contentTextView) {
        if (textView.text.length >=1000) {
            
            [self endEditing:YES];
            [[FWHudManager sharedManager] showErrorMessage:@"最多只能输入1000个字哦~" toController:self.vc];
            return NO;
        }
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    
    if (textView.text.length >1000) {

        textView.text = [textView.text substringToIndex:1000];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    self.tipLable.hidden = NO;
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    self.tipLable.hidden = YES;
    return YES;
}

#pragma mark - > 请求基本配置参数
- (void)requestSetting{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_settings  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
        }
    }];
}

#pragma mark - > 定位成功
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *location = [locations lastObject];
    [self.manager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        CLPlacemark *place = [placemarks lastObject];
        
        self.isDeny = 1;

        self.provice = place.administrativeArea?place.administrativeArea:@"";
        self.city = place.locality?place.locality:@"";
        self.country = place.subLocality?place.subLocality:@"";

        if ([self.provice isEqualToString:@""] &&
            [self.city isEqualToString:@"北京市"]) {
            self.provice = @"北京";
        }
        if ([self.provice isEqualToString:@""] &&
            [self.city isEqualToString:@"天津市"]) {
            self.provice = @"天津";
        }
        if ([self.provice isEqualToString:@""] &&
            [self.city isEqualToString:@"重庆市"]) {
            self.provice = @"重庆";
        }
        if ([self.provice isEqualToString:@""] &&
            [self.city isEqualToString:@"上海市"]) {
            self.provice = @"上海";
        }
        
        self.fahuoDictionary = @{
            @"provice_code":self.provice,
            @"city_code":self.city,
            @"county_code":self.country,
        };
        
        self.fahuoString = [NSString stringWithFormat:@"%@%@%@",self.provice,self.city,self.country];
        [self setupFahuoView];
    }];
}


#pragma mark - > 定位失败
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{

    if (error.code == kCLErrorDenied) {
        /* 用户拒绝 */
        self.isDeny = 0;
    }
}
@end
