//
//  FWScanArticalView.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWScanArticalView.h"
#import "SWQRCodeViewController.h"
#import "SWQRCodeConfig.h"

@implementation FWScanArticalView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = FWViewBackgroundColor_F1F1F1;
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews{
}

#pragma mark - > 复制
- (IBAction)copyButtonClick:(id)sender {
    
    [[FWHudManager sharedManager] showSuccessMessage:@"复制成功" toController:self.vc];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.upValueLabel.text;
}

#pragma mark - > 扫一扫
- (IBAction)scanButtonClick:(id)sender {
    
    SWQRCodeConfig *config = [[SWQRCodeConfig alloc]init];
    config.scannerType = SWScannerTypeBoth;
    
    SWQRCodeViewController *qrcodeVC = [[SWQRCodeViewController alloc]init];
    qrcodeVC.codeConfig = config;
    qrcodeVC.type = @"login";
    [self.vc.navigationController pushViewController:qrcodeVC animated:YES];
}

@end
