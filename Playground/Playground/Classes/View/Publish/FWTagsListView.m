//
//  FWTagsListView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWTagsListView.h"
#import "FWSearchTagsListModel.h"

@implementation FWTagsListView
@synthesize firstContainer;
@synthesize secondContainer;
@synthesize firstLabel;
@synthesize lineView;
@synthesize iconImageView;
@synthesize secondLabel;
@synthesize deleteButton;
@synthesize containerOneArr;
@synthesize containerTwoArr;

- (id)init{
    self = [super init];
    if (self) {
        
        containerOneArr = @[].mutableCopy;
        containerTwoArr = @[].mutableCopy;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    [self createFirstView];
    [self createSecondView];
}

- (void)createFirstView{
    
    firstContainer = [[UIView alloc] init];
    firstContainer.tag = 321;
    firstContainer.backgroundColor = [UIColor clearColor];
    [self addSubview:firstContainer];
    [firstContainer mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(1);
    }];
    
    firstLabel = [[UILabel alloc] init];
    firstLabel.text = @"历史标签";
    firstLabel.font = DHSystemFontOfSize_14;
    firstLabel.textColor = FWTextColor_AEAEAE;
    firstLabel.textAlignment = NSTextAlignmentLeft;
    [firstContainer addSubview:firstLabel];
    [firstLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(firstContainer).mas_offset(30);
        make.top.mas_equalTo(firstContainer).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    deleteButton  = [[UIButton alloc] init];
    deleteButton.tag = 4399;
    deleteButton.hidden = NO;
    [deleteButton setImage:[UIImage imageNamed:@"trash"] forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [firstContainer addSubview:deleteButton];
    [deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(firstContainer).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.mas_equalTo(firstLabel);
    }];
}

- (void)createSecondView{
    
    secondContainer = [[UIView alloc] init];
    secondContainer.tag = 322;
    secondContainer.backgroundColor = [UIColor clearColor];
    [self addSubview:secondContainer];
    [secondContainer mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.top.mas_equalTo(firstContainer.mas_bottom).mas_offset(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(50);
        make.bottom.mas_equalTo(self).mas_offset(-10);
    }];
    
    secondLabel = [[UILabel alloc] init];
    secondLabel.text = @"推荐标签";
    secondLabel.font = DHSystemFontOfSize_14;
    secondLabel.textColor = FWTextColor_000000;
    secondLabel.textAlignment = NSTextAlignmentLeft;
    [secondContainer addSubview:secondLabel];
    [secondLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(secondContainer).mas_offset(55);
        make.top.mas_equalTo(secondContainer).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    iconImageView = [[UIImageView alloc] init];
    iconImageView.image = [UIImage imageNamed:@"home_tag"];
    [secondContainer addSubview:iconImageView];
    [iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(15, 15));
        make.left.mas_equalTo(secondContainer).mas_offset(24);
        make.centerY.mas_equalTo(secondLabel);
    }];
    
    lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
    [secondContainer addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(secondContainer).mas_offset(24);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(secondLabel.mas_bottom).mas_offset(1);
        make.right.mas_equalTo(secondContainer).mas_offset(-24);
    }];
}

- (void)configViewWithModel:(NSMutableArray *)tagsList{
    
    NSData * data = [GFStaticData getObjectForKey:Tags_History_Array];
    NSMutableArray *userDefaultsArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];

    if (userDefaultsArr.count > 0 ) {
        
        if (containerOneArr.count >0 ) {
            [containerOneArr removeAllObjects];
        }
        
        for (FWSearchTagsSubListModel * listModel in userDefaultsArr) {
            [containerOneArr addObject:listModel.tag_name];
        }
    }

    if (containerTwoArr.count >0 ) {
        [containerTwoArr removeAllObjects];
    }
    
    if (tagsList.count > 0) {
        for (FWSearchTagsSubListModel * listModel in tagsList) {
            [containerTwoArr addObject:listModel.tag_name];
        }
    }

    [self refeshView];
}

- (void)refeshView{
    
    [self refreshContainer:firstContainer withLabel:firstLabel WithArray:containerOneArr withIndex:1];
    [self refreshContainer:secondContainer withLabel:secondLabel WithArray:containerTwoArr withIndex:2];
}
    
/**
 更新推荐标签列表

 @param container 哪个容器
 @param label 哪个容器下的label
 @param array 该容器内的标签数组
 @param index 第几个容器(从1开始)
 */
- (void) refreshContainer:(UIView *)container withLabel:(UILabel *)label WithArray:(NSArray *)array withIndex:(NSInteger)index{
    
    if (index == 1) {
        if (array.count <= 0) {
            deleteButton.hidden = YES;
            firstLabel.hidden = YES;
            
        }else{
            deleteButton.hidden = NO;
            firstLabel.hidden = NO;
        }
    }
    
    for (UIView * view in container.subviews) {
        if ([view isKindOfClass:[UIButton class]] &&
            view.tag != 4399) {
            [view removeFromSuperview];
        }
    }

    CGFloat x = 20;
    CGFloat y = 20;
    CGFloat width = 0.0;
    CGFloat height = 27.f;
    
    for (int i = 0; i < array.count; i++) {
               
        NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc] initWithString:array[i]];
        CGSize  textLimitSize = CGSizeMake(MAXFLOAT, height);
        width = [YYTextLayout layoutWithContainerSize:textLimitSize text:attribute].textBoundingSize.width+40;
        
        if (x + width+20 >= SCREEN_WIDTH) {
            x = 20;
            y += height + 11;
        }
        
        UIButton * btn = [[UIButton alloc] init];
        btn.titleLabel.font = DHSystemFontOfSize_12;
        btn.tag = 10000+i;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        if (index == 1) {
            btn.backgroundColor = FWTextColor_F5F5F5;
            [btn setTitleColor:FWTextColor_ADADAD forState:UIControlStateNormal];
        }else{
            btn.backgroundColor = FWTextColor_222222;
            [btn setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        }
        [btn setTitle:[NSString stringWithFormat:@"%@",array[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(tagsBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [container addSubview:btn];
        [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(container).mas_equalTo(x);
            make.top.mas_equalTo(label.mas_bottom).mas_equalTo(y);
            make.size.mas_equalTo(CGSizeMake(width, height));
        }];
        
        if (i == array.count-1  && array.count>0) {
            [btn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(container).mas_equalTo(x);
                make.top.mas_equalTo(label.mas_bottom).mas_equalTo(y);
                make.size.mas_equalTo(CGSizeMake(width, height));
                make.bottom.mas_equalTo(container).mas_offset(-20);
            }];
        }
        
        x += width+15;
    }
}

#pragma mark - > 选择的标签
- (void)tagsBtnOnClick:(UIButton *)sender{
    
    NSInteger btnTag = sender.tag - 10000;
    
    UIView * superView =  (UIButton *)sender.superview;
    NSInteger superTag = superView.tag;
    
    if ([self.tagsDelegate respondsToSelector:@selector(tagsBtnOnClickWithSuperIndex:WithSubIndex:)]) {
        [self.tagsDelegate tagsBtnOnClickWithSuperIndex:superTag WithSubIndex:btnTag];
    }
}

#pragma mark - > 删除历史标签
- (void)deleteButtonOnClick{
    
    if (containerOneArr.count>0) {
        [containerOneArr removeAllObjects];
    }

    [GFStaticData saveObject:nil forKey:Tags_History_Array];
    
    [self refeshView];
}

@end
