//
//  FWFilterView.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/11.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWFilterView.h"
#import "ColorFliterAuxiliary.h"

@implementation FWFilterView
@synthesize isAlert;
@synthesize mainView;
@synthesize theaArr;
@synthesize vc;

- (id)init{
    self = [super init];
    if (self) {
        
         theaArr = [[NSArray alloc]initWithObjects:@"原图",@"LOMO",@"黑白",@"复古",@"哥特",@"锐化",@"淡雅",@"酒红",@"青柠",@"浪漫",@"光晕",@"蓝调",@"梦幻",@"夜色", nil];

        self.delegate = self;
        self.contentSize = CGSizeMake(theaArr.count * 100, 0);
        self.backgroundColor = FWViewBackgroundColor_F0F0F0;

        [self setupSubviews];
    }
    
    return self;
}

- (void)setOriginImage:(UIImage *)originImage{
    _originImage = originImage;
    
    [self setupSubviews];
}

- (void)setupSubviews{
    
    if (self.originImage) {
        for (int i =0; i<theaArr.count; i ++) {
            
            UIImage * image = [self thumbnailWithImage:self.originImage size:CGSizeMake(60, 60)];
            
            UIButton * filterButton = [[UIButton alloc] init];
            filterButton.tag = 10000+i;
            [filterButton addTarget:self action:@selector(filterButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:filterButton];
            filterButton.frame = CGRectMake(i*100+20, 20, 100, 100);
            
            UILabel * filterLabel = [[UILabel alloc] init];
            filterLabel.text = self.theaArr[i];
            filterLabel.font = DHSystemFontOfSize_14;
            filterLabel.textColor = FWTextColor_969696;
            filterLabel.textAlignment = NSTextAlignmentCenter;
            [filterButton addSubview:filterLabel];
            filterLabel.frame = CGRectMake(0, CGRectGetHeight(filterButton.frame)-20, CGRectGetWidth(filterButton.frame), 20);
            
            UIImageView * filterImageView = [[UIImageView alloc] init];
            filterImageView.image = [ColorFliterAuxiliary changeImage:(int)i imageView:image];
            filterImageView.frame = CGRectMake(20, 10, 60, 60);
            [filterButton addSubview:filterImageView];
        }
    }
    
}

- (UIImage *)thumbnailWithImage:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    
    if (nil == image) {
        
        newimage = nil;
    }else{
        UIGraphicsBeginImageContext(asize);
        
        [image drawInRect:CGRectMake(0, 0, asize.width, asize.height)];
        
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
    }
    
    return newimage;
    
}

- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    
    if (nil == image) {
        
        newimage = nil;
    }else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
    }
    return newimage;
}


#pragma mark - > 点击选择滤镜
- (void)filterButtonOnClick:(UIButton *)sender{
    
    if ([self.filterDelegate respondsToSelector:@selector(settingFilterClick:)]) {
        [self.filterDelegate settingFilterClick:sender.tag-10000];
    }
}

- (void)showView{
    
    mainView.hidden = NO;
}

- (void)hideView{
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    
    [animation setType: kCATransitionFade];
    
    [animation setSubtype: kCATransitionFromBottom];
    
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    
    self.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 200);

    [self.layer addAnimation:animation forKey:nil];
}

- (void)clearSubview{
    
    if (mainView) {
        for (UIView * view in mainView.subviews) {
            [view removeFromSuperview];
        }
    }
}

@end
