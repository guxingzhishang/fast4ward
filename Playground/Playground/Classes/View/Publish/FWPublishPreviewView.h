//
//  FWPublishPreviewView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTextView.h"

@protocol FWPublishPreviewViewDelegate <NSObject>


#pragma mark - > 编辑封面
- (void)editButtonClick;

#pragma mark - > 删除草稿
- (void)deleteButtonClick:(UIButton *)sender;

#pragma mark - > 发布
- (void)publishButtonClick:(UIButton *)sender;

#pragma mark - > 选择标签
- (void)tagsButtonClick;

#pragma mark - > 选择车系
- (void)carTypeButtonClick;

#pragma mark - > 保存在本地
- (void)saveButtonClick;

#pragma mark - > 删除标签
- (void)deleteTagsClick:(NSInteger)index;

@end

@interface FWPublishPreviewView : UIScrollView<UITextViewDelegate>

@property (nonatomic, strong) UIImageView * fengmianView;

@property (nonatomic, strong) UIButton * editButton;

@property (nonatomic, strong) IQTextView * contentTextView;

@property (nonatomic, strong) UIButton * deleteButton;
@property (nonatomic, strong) UIButton * publishButton;

@property (nonatomic, strong) UIView * listView;

@property (nonatomic, strong) UIButton * tagsButton;
@property (nonatomic, strong) UIButton * linkButton;
@property (nonatomic, strong) UIButton * carTypeButton;
@property (nonatomic, strong) UIButton * atButton;

@property (nonatomic, strong) UIView * atUserView;
@property (nonatomic, strong) NSMutableArray * atUserMutableArray;

@property (nonatomic, strong) UILabel * hasLinkLabel;
@property (nonatomic, strong) UILabel * hasTopicLabel;
@property (nonatomic, strong) UILabel * hasCarTypeLabel;

@property (nonatomic, assign) NSInteger  listCount;

@property (nonatomic, weak) id<FWPublishPreviewViewDelegate> publishDelegate;

@property (nonatomic, strong) NSURL * pathURL;

@property (nonatomic, strong) NSString * tempGoods_id;

@property (nonatomic, assign) CGFloat textViewHeight;

@property (nonatomic, weak) UIViewController * vc;


/* 设置标签视图 */
- (void)setupTagsView:(id)array;

/* 选择车系 */
- (void)setupCarTypeView:(NSString *)carType;

- (void)configForView:(id)model;

- (UIImage*) getVideoPreViewImage:(NSURL *)path;

@end
