//
//  FWChooseGoodsCell.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWChooseGoodsCell.h"

@interface FWChooseGoodsCell ()

@property (nonatomic, strong) FWBussinessShopGoodsListModel * shopModel;

@end

@implementation FWChooseGoodsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
 
    if (self) {
        
        self.contentView.backgroundColor = FWTextColor_F5F5F5;
        self.contentView.layer.cornerRadius = 4;
        self.contentView.layer.masksToBounds = YES;
        
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews{
    
    self.bgView = [[UIView alloc] init];
    self.bgView.layer.cornerRadius = 4;
    self.bgView.layer.masksToBounds = YES;
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.contentView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(12);
        make.bottom.mas_equalTo(self.contentView).mas_offset(-10);
        make.right.mas_equalTo(self.contentView).mas_offset(-12);
        make.height.mas_equalTo(91);
        make.width.mas_equalTo(SCREEN_WIDTH-24);
    }];
    
    self.cardImageView = [[UIImageView alloc] init];
    self.cardImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.cardImageView.image = [UIImage imageNamed:@"placeholder"];
    self.cardImageView.clipsToBounds = YES;
    [self.bgView addSubview:self.cardImageView];
    [self.cardImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(91);
        make.height.mas_equalTo(91);
        make.top.left.bottom.mas_equalTo(self.bgView);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = DHSystemFontOfSize_13;
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.cardImageView.mas_right).mas_offset(11);
        make.top.mas_equalTo(self.cardImageView).mas_offset(10);
        make.height.mas_greaterThanOrEqualTo(5);
        make.width.mas_greaterThanOrEqualTo(100);
        make.right.mas_equalTo(self.bgView).mas_offset(-10);
    }];
    
    self.chooseButton = [[UIButton alloc] init];
    self.chooseButton.layer.cornerRadius = 4;
    self.chooseButton.layer.masksToBounds = YES;
    self.chooseButton.layer.borderColor = FWTextColor_B6BCC4.CGColor;
    self.chooseButton.layer.borderWidth = 1;
    self.chooseButton.titleLabel.font = DHSystemFontOfSize_12;
    [self.chooseButton setTitle:@"选择" forState:UIControlStateNormal];
    [self.chooseButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
    [self.chooseButton addTarget:self action:@selector(chooseButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.chooseButton];
    [self.chooseButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.mas_equalTo(self.bgView).mas_offset(-10);
        make.height.mas_equalTo(22);
        make.width.mas_equalTo(48);
    }];
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = DHSystemFontOfSize_15;
    self.priceLabel.textColor = DHRedColorff6f00;
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.bottom.mas_equalTo(self.bgView).mas_offset(-10);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.chooseButton.mas_left).mas_offset(-5);
    }];
    
    self.scanLabel = [[UILabel alloc] init];
    self.scanLabel.font = DHSystemFontOfSize_12;
    self.scanLabel.textColor = FWTextColor_B6BCC4;
    self.scanLabel.textAlignment = NSTextAlignmentRight;
    [self.bgView addSubview:self.scanLabel];
    [self.scanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.chooseButton).mas_offset(0);
        make.bottom.mas_equalTo(self.chooseButton.mas_top).mas_offset(0);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.scanImageView = [[UIImageView alloc] init];
    self.scanImageView.image = [UIImage imageNamed:@"card_eye"];
    [self.bgView addSubview:self.scanImageView];
    [self.scanImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(16, 11));
        make.centerY.mas_equalTo(self.scanLabel);
        make.right.mas_equalTo(self.scanLabel.mas_left).mas_offset(-5);
    }];
}

- (void)configForShopCell:(id)model{
    
    self.shopModel = (FWBussinessShopGoodsListModel *)model;
    
    self.scanLabel.text = self.shopModel.click_count;
    self.titleLabel.text = self.shopModel.title;
    self.priceLabel.text = [NSString stringWithFormat:@"￥%@",self.shopModel.price];
    
    [self.cardImageView sd_setImageWithURL:[NSURL URLWithString:self.shopModel.cover.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

- (void)resetChooseButton:(BOOL)Choose{
    
    self.isChoose = Choose;
    
    if (Choose) {
        
        [self.chooseButton setTitle:@"取消关联" forState:UIControlStateNormal];
        [self.chooseButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.mas_equalTo(self.bgView).mas_offset(-10);
            make.height.mas_equalTo(22);
            make.width.mas_equalTo(60);
        }];
    }else{
        
        [self.chooseButton setTitle:@"选择" forState:UIControlStateNormal];
        [self.chooseButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.mas_equalTo(self.bgView).mas_offset(-10);
            make.height.mas_equalTo(22);
            make.width.mas_equalTo(48);
        }];
    }
}

#pragma mark - > 选择
- (void)chooseButtonOnClick{
    
    NSString * goods_id;
    
    if (self.isChoose) {
        /* 已经选择，取消选择 */
        goods_id = @"0";
    }else{
        /* 未选择，记录当前id */
        goods_id = self.shopModel.goods_id;
    }
    if ([self.delegate respondsToSelector:@selector(chooseButtonClick:)]) {
        [self.delegate chooseButtonClick:goods_id];
    }
}


@end
