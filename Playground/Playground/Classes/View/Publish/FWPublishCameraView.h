//
//  FWPublishCameraView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/7.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWPublishCameraViewDelegate <NSObject>

#pragma mark - > 视频
- (void)videoButtonClick;

#pragma mark - > 照片
- (void)photographButtonClick;

#pragma mark - > 文章
- (void)articalButtonClick;

#pragma mark - > 草稿箱
- (void)draftButtonClick;

#pragma mark - > 改装分享
- (void)refitButtonClick;

#pragma mark - > 提问
- (void)askButtonClick;

#pragma mark - > 发布闲置
- (void)idleButtonClick;
@end

@interface FWPublishCameraView : UIView

/**
 * 顶部视图
 */
@property (nonatomic, strong) UIImageView * mainView;

/**
 * 返回按钮
 */
@property (nonatomic, strong) UIButton * backButton;

/**
 * 发布闲置按钮
 */
@property (nonatomic, strong) UIImageView * idleButton;

/**
 * 视频按钮
 */
@property (nonatomic, strong) UIButton * videoButton;

/**
 * 相片按钮
 */
@property (nonatomic, strong) UIButton * photographButton;

/**
 * 文章按钮
 */
@property (nonatomic, strong) UIButton * articalButton;

/**
 * 草稿
 */
@property (nonatomic, strong) UIButton * draftButton;


/**
 * 承载该视图的控制器
 */
@property (nonatomic, weak) UIViewController * vc;


@property (nonatomic, weak) id<FWPublishCameraViewDelegate> delegate;

@end
