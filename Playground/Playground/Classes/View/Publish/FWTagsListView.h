//
//  FWTagsListView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWTagsListViewDelegate <NSObject>

- (void)tagsBtnOnClickWithSuperIndex:(NSInteger)superIndex WithSubIndex:(NSInteger)subIndex;

@end

@interface FWTagsListView : UIScrollView

/**
 * 承载历史标签的视图
 */
@property (nonatomic, strong) UIView * firstContainer;

/**
 * 承载推荐标签的视图
 */
@property (nonatomic, strong) UIView * secondContainer;

/**
 * 历史标签
 */
@property (nonatomic, strong) UILabel * firstLabel;

/**
 * 清空搜索记录
 */
@property (nonatomic, strong) UIButton * deleteButton;


/**
 * 推荐标签图标
 */
@property (nonatomic, strong) UIImageView * iconImageView ;

/**
 * 横线
 */
@property (nonatomic, strong) UIView * lineView ;

/**
 * 推荐标签
 */
@property (nonatomic, strong) UILabel * secondLabel;

/**
 * 承载历史标签的数组
 */
@property (nonatomic, strong) NSMutableArray * containerOneArr;

/**
 * 承载推荐标签的数组
 */
@property (nonatomic, strong) NSMutableArray * containerTwoArr;

@property (nonatomic, weak) id<FWTagsListViewDelegate> tagsDelegate;


- (void)configViewWithModel:(NSMutableArray *)tagsList;

@end
