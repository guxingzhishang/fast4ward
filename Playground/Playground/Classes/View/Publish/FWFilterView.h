//
//  FWFilterView.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/11.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FWFilterViewDelegate <NSObject>

- (void)settingFilterClick:(NSInteger) index;

@end

@interface FWFilterView : UIScrollView<UIScrollViewDelegate>

@property (nonatomic, strong) UIView * mainView;

@property (nonatomic, assign) BOOL isAlert;

@property (nonatomic, strong) NSArray * theaArr;

@property (nonatomic, weak) UIViewController * vc;

/* 原图 */
@property (nonatomic, strong) UIImage *originImage;

@property (nonatomic, weak) id<FWFilterViewDelegate> filterDelegate;

- (void) showView;

- (void) hideView;
@end
