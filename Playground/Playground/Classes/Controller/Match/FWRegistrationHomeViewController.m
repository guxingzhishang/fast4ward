//
//  FWRegistrationHomeViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRegistrationHomeViewController.h"
#import "FWRegistrationHomeCell.h"
#import "FWRegistrationHomeModel.h"
#import "FWPlayerRegistrationViewController.h"
#import "FWMySignupViewController.h"
#import "FWPlayerSignupViewController.h"
#import "FWEmptyView.h"
#import "FWRegistrationDetailViewController.h"
#import "FWMySignupViewController.h"

@interface FWRegistrationHomeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWEmptyView * emptyView;
@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWRegistrationHomeModel * model;

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIButton * registrationButton;
@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, assign) NSInteger  currentIndexRow;
@end

@implementation FWRegistrationHomeViewController
@synthesize infoTableView;
@synthesize pageNum;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)requestData:(BOOL)isLoadMoreData{
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if (isLoadMoreData) {
        pageNum += 1;
    }else{
        pageNum = 1;
        
        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(pageNum).stringValue,
                              @"page_size":@"20",
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_baoming_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if (isLoadMoreData) {
            [infoTableView.mj_footer endRefreshing];
        }else{
            self.infoTableView.mj_footer.hidden = NO;
            [infoTableView.mj_header endRefreshing];
        }
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.model = [FWRegistrationHomeModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.dataSource addObjectsFromArray:self.model.list_baoming];
            
            if([self.model.list_baoming count] == 0 &&
               pageNum != 1){
                
                self.infoTableView.mj_footer.hidden = YES;
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = FWTextColor_F8F8F8;
    [self trackPageBegin:@"报名主页"];
    
    [self requestData:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"报名主页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"车手报名";
    self.pageNum = 1;
    
    UIButton * recordButton = [[UIButton alloc] init];
    recordButton.frame = CGRectMake(0, 0, 76, 30);
    [recordButton setImage:[UIImage imageNamed:@"baoming_record"] forState:UIControlStateNormal];
    [recordButton addTarget:self action:@selector(recordButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:recordButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    
    [self setupSubViews];
}

#pragma mark - > 报名记录
- (void)recordButtonClick{
    
    [self checkLogin];
           
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWMySignupViewController * msvc = [[FWMySignupViewController alloc] init];
        [self.navigationController pushViewController:msvc animated:YES];
    }
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    infoTableView.rowHeight = 158;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"敬请期待下次比赛~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestData:NO];
    }];
    
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestData:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWRegistrationHomeCellID";
    
    FWRegistrationHomeCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWRegistrationHomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.dataSource.count) {
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.currentIndexRow = indexPath.row;

    if (indexPath.row >= self.dataSource.count) {
        return;
    }
    
    FWRegistrationDetailViewController * PSVC = [[FWRegistrationDetailViewController alloc] init];
    PSVC.baoming_id = ((FWRegistrationHomeListModel *)self.dataSource[indexPath.row]).baoming_id;
    [self.navigationController pushViewController:PSVC animated:YES];
}


- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

@end
