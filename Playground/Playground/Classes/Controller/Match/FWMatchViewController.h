//
//  FWMatchViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 赛事页面
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchViewController : FWBaseViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>

@end

NS_ASSUME_NONNULL_END
