//
//  FWHotLiveListViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHotLiveListViewController.h"
#import "FWHotLiveCell.h"
#import "FWHotLiveNoticeCell.h"
#import "FWHotLiveViewController.h"
#import "FWLiveModel.h"
#import "FWHotLiveBackViewController.h"
#import "FWConversationViewController.h"

@interface FWHotLiveListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) NSMutableArray * sectionHeaderTitle;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) FWLiveModel * homeModel;

@end

@implementation FWHotLiveListViewController
@synthesize infoTableView;

#pragma mark - > 获取直播首页
- (void)requestLiveDataWithLoadMoredData:(BOOL)isLoadMoredData {
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_zhibo_index WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.homeModel = [FWLiveModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.infoTableView.mj_header endRefreshing];
            
            if (self.dataSource.count > 0) {
                [self.dataSource removeAllObjects];
            }
            if (self.sectionHeaderTitle.count > 0) {
                [self.sectionHeaderTitle removeAllObjects];
            }
            
            if (self.homeModel.list_yugao.list.count > 0) {
                [self.dataSource addObject:self.homeModel.list_yugao.list];
                [self.sectionHeaderTitle addObject:self.homeModel.list_yugao.title];
            }
            
            if (self.homeModel.list_zhibo.list.count > 0) {
                [self.dataSource addObject:self.homeModel.list_zhibo.list];
                [self.sectionHeaderTitle addObject:self.homeModel.list_zhibo.title];
            }
            
            if (self.homeModel.list_huifang.list.count > 0) {
                [self.dataSource addObject:self.homeModel.list_huifang.list];
                [self.sectionHeaderTitle addObject:self.homeModel.list_huifang.title];
            }
            
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view.backgroundColor = FWTextColor_F8F8F8;
    [self requestSetting];
    
    [self trackPageBegin:@"直播列表页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"直播列表页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"直播广场";
    
    self.dataSource = @[].mutableCopy;
    self.sectionHeaderTitle = @[].mutableCopy;
    
    UIButton * rightButton = [[UIButton alloc] init];
    rightButton.layer.cornerRadius = 2;
    rightButton.layer.masksToBounds = YES;
    rightButton.frame = CGRectMake(0, 0, 82, 30);
    rightButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    rightButton.backgroundColor = FWTextColor_222222;
    [rightButton setTitle:@"我要直播" forState:UIControlStateNormal];
    [rightButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(toLiveOnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];

    [self setupSubViews];
    [self requestLiveDataWithLoadMoredData:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.backgroundColor = FWTextColor_F8F8F8;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"直播准备中，敬请期待~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestLiveDataWithLoadMoredData:NO];
    }];
    
    [infoTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray * listArray = self.dataSource[section];

    if (section == 0) {
        return listArray.count;
    }else{
        if (listArray.count %2 == 0) {
            /* 偶数 */
            return listArray.count/2;
        }else{
            /* 奇数 */
            return listArray.count/2+1;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 169+10;
    }else{
        return (SCREEN_WIDTH-38)/2+60+10;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (self.dataSource.count > indexPath.section) {
        
        NSArray * listArray = self.dataSource[indexPath.section];
       
        if (self.homeModel.list_yugao.list.count > 0 && indexPath.section == 0) {
            static NSString * cellID = @"hotLiveNoticeCellID";
            
            FWHotLiveNoticeCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[FWHotLiveNoticeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.vc = self;
            if (listArray.count > indexPath.row) {
                [cell configForCell:listArray[indexPath.row]];
            }
            
            return cell;
        }else{
            static NSString * cellID = @"hotLiveCellID";
            
            FWHotLiveCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[FWHotLiveCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
            
            cell.backgroundColor = FWTextColor_F8F8F8;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            NSInteger leftindex = indexPath.row *2;
            NSInteger rightindex = indexPath.row *2 +1;
            
            if (listArray.count > leftindex) {
                [cell configForLeftView:listArray[leftindex]];
                
                //左侧列的按钮Block回调
                cell.leftButtonBlock = ^(void){
                    [self pushToLiveRoom:listArray[leftindex]];
                };
            }
            
            if (listArray.count > rightindex) {
                
                cell.rightView.hidden = NO;
                [cell configForRightView:listArray[rightindex]];
                
                //右侧列的按钮Block回调
                cell.rightButtonBlock = ^(void){
                    [self pushToLiveRoom:listArray[leftindex]];
                };
            }else{
                cell.rightView.hidden = YES;
            }
            
            return cell;
        }
    }

    return [[UITableViewCell alloc] initWithFrame:CGRectZero];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.dataSource.count > indexPath.section) {
        if (self.homeModel.list_yugao.list.count > 0 && indexPath.section == 0) {
            if (self.homeModel.list_yugao.list[0].feed_id.length > 0) {
                
                FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
                DVC.feed_id = self.homeModel.list_yugao.list[0].feed_id;
                [self.navigationController pushViewController:DVC animated:YES];
            }
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 54)];
    view.backgroundColor = FWTextColor_F8F8F8;
    
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(14, 0, 100, 54);
    if (self.sectionHeaderTitle.count > section) {
        titleLabel.text = self.sectionHeaderTitle[section];
    }
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    titleLabel.textColor = FWTextColor_272727;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:titleLabel];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 54;
}

#pragma mark - > 跳转到直播页
- (void)pushToLiveRoom:(id)model{
    
    FWLiveListModel * listModel = (FWLiveListModel *)model;

    if ([listModel.zhibo_status integerValue] == 2) {
        /* 直播中 */
        FWHotLiveViewController * HLVC = [[FWHotLiveViewController alloc] init];
        HLVC.targetId = listModel.room_id;
        HLVC.liveListModel = listModel;
        HLVC.conversationType = ConversationType_CHATROOM;
        HLVC.defaultHistoryMessageCountOfChatRoom = -1;
        [self.navigationController pushViewController:HLVC animated:YES];
    }else{
        /* 直播回看 */
        FWHotLiveBackViewController * HLVC = [[FWHotLiveBackViewController alloc] init];
        HLVC.liveListModel = listModel;
        [self.navigationController pushViewController:HLVC animated:YES];
    }
}

#pragma mark - > 请求基本配置参数
- (void)requestSetting{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_settings  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            FWSettingModel * settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [GFStaticData saveObject:settingModel.kefu_uid forKey:kKefuUID];
            [GFStaticData saveObject:settingModel.kefu_nickname forKey:kKefuNickname];
        }
    }];
}

#pragma mark - > 我要直播
- (void)toLiveOnClick{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:self.homeModel.msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"联系TA" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([FWUserCenter sharedManager].user_IsLogin) {
            if ([GFStaticData getObjectForKey:kKefuUID]) {
                
                FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:[GFStaticData getObjectForKey:kKefuUID]];
                [self.navigationController pushViewController:CVC animated:YES];
            }
        }else{
            [self checkLogin];
        }
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

@end
