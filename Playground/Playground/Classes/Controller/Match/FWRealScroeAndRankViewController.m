//
//  FWRealScroeAndRankViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/8.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRealScroeAndRankViewController.h"
#import "FWRealPerformanceViewController.h"
#import "FWRankViewController.h"
#import "FWGuessingCompetitionViewController.h"
#import "FWLiveADView.h"

static NSInteger adViewTag = 123456789;

@interface FWRealScroeAndRankViewController ()

@property (nonatomic, strong) NSArray *titleData;
@property (nonatomic, assign) CGFloat menuHeight;
@property (nonatomic, assign) CGFloat wmMenuY;
@property (nonatomic, strong) FWLiveADView * adView;
@property (nonatomic, strong) FWMatchModel * matchModel;

@end

@implementation FWRealScroeAndRankViewController

- (void)dealloc{
    NSLog(@" ========= FWRealScroeAndRankViewController 销毁了 +++========");
}
#pragma mark - > 获取比赛分组
- (void)requestTeam{
    
    FWMatchRequest * request = [[FWMatchRequest alloc] init];
    
    if (!self.sport_id) return;
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"sport_id":self.sport_id,
                              };
    
    [request startWithParameters:params WithAction:Get_sport_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.matchModel = [FWMatchModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            self.title = self.matchModel.sport_name;
            if ([self.matchModel.guess_status isEqualToString:@"1"]) {
                self.titleData = @[@"实时成绩", @"排名",@"竞猜"];
                [self reloadData];
            }
            
            if (self.adView) {
                [self.adView.adImageView sd_setImageWithURL:[NSURL URLWithString:self.matchModel.gold_img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if (!image) {
                         [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];

                        if (self.adView) {
                            for (UIView * view in self.adView.subviews) {
                                [view removeFromSuperview];
                            }
                        }

                    }
                }];
            }
            
            [self requestGoldStatus];
        }
    }];
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        self.menuViewStyle = WMMenuViewStyleLine;
        self.progressHeight = 2.0f;
        self.titleFontName = @"PingFangSC-Semibold";
        self.titleSizeSelected = 14.0f;
        self.titleSizeNormal = 14.0f;
        self.scrollEnable = NO;
        self.titleColorSelected = FWTextColor_222222;
        self.titleColorNormal = FWTextColor_BCBCBC;
        self.menuViewLayoutMode = WMMenuViewLayoutModeScatter;
        self.menuHeight = 44.0f;
        self.wmMenuY =  0;
    }
    return self;
}

#pragma mark - > 标题数组
- (NSArray *)titleData {
    if (!_titleData) {
        _titleData = @[@"实时成绩", @"排名"];
    }
    return _titleData;
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = @"比赛信息";
    
    UIButton * backButton = [[UIButton alloc] init];
    backButton.frame = CGRectMake(0, 0, 45, 40);
    backButton.titleLabel.font = DHSystemFontOfSize_16;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItems = @[leftBtnItem];
    
    UIButton * shareButton = [[UIButton alloc] init];
    shareButton.frame = CGRectMake(0, 0, 45, 40);
    [shareButton setImage:[UIImage imageNamed:@"new_black_share"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    
    [self setupADView];

    [self requestTeam];
}

#pragma mark - > 查询金币领取状态
- (void)requestGoldStatus{

    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"sport_id":self.sport_id,
                             };
    
    [request startWithParameters:params WithAction:Get_gold_get_status WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (![[[back objectForKey:@"data"] objectForKey:@"status"] isEqualToString:@"1"]) {
                self.adView.hidden = NO;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

-(void)setupADView{
    
    self.adView = [[FWLiveADView alloc] init];
    self.adView.tag = adViewTag;
    self.adView.frame = CGRectMake(0, 0, SCREEN_WIDTH, ScreenHeight);
    [[UIApplication sharedApplication].keyWindow addSubview:self.adView];
    
    self.adView.hidden = YES;

    [self.adView.adImageView sd_setImageWithURL:[NSURL URLWithString:self.matchModel.gold_img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    UITapGestureRecognizer * adTap = [[UITapGestureRecognizer alloc] init];
    [self.adView.adImageView addGestureRecognizer:adTap];
    [self.adView.adImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adTap)]];

    [self.adView.closeView addTarget:self action:@selector(closeTap) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - > 点击广告，领取金币
- (void)adTap{
    
    [self checkLogin];

    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];

      /* 广告 */
      FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
      
      NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"sport_id":self.sport_id,
      };
        request.isNeedShowHud = YES;
      [request startWithParameters:params WithAction:Submit_get_guess_gold WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
          
          NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
          
          if ([code isEqualToString:NetRespondCodeSuccess]) {
              NSString *handledStr = [[[back objectForKey:@"data"] objectForKey:@"notice_str"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];

              UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:handledStr preferredStyle:UIAlertControllerStyleAlert];
              UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"立刻竞猜" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                  if (self.adView) {
                     [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];
                  }
                  
                  if (self.titleData.count > 0) {
                     self.selectIndex = (int)(self.titleData.count -1);
                  }
              }];
              [alertController addAction:okAction];
              [self presentViewController:alertController animated:YES completion:nil];
          }else{
              if (self.adView && [[back objectForKey:@"errmsg"] isEqualToString:@"您已经领取过本场比赛的赠送金币"]) {
                  [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];
                  for (UIView * view in self.adView.subviews) {
                      [view removeFromSuperview];
                  }
                  return ;
              }
              [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
          }
      }];
    }
}

#pragma mark - > 关闭
- (void)closeTap{
    [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];
}

#pragma mark - > 返回
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 分享
- (void)shareButtonClick{
    
    NSString *urlString = self.share_image;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    UIImage *image = [UIImage imageWithData:data];

    FWShareScoreImageViewController * vc = [[FWShareScoreImageViewController alloc] init];
    vc.shareType = @"3";
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    vc.postImage = image;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - > WMPageController 相关配置
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController
{
    return self.titleData.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    
    return self.titleData[index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    
    if (index == 0){
        FWRealPerformanceViewController * RPVC = [[FWRealPerformanceViewController alloc] init];
        RPVC.sport_id = self.sport_id;
        RPVC.myBlock = ^(NSString * _Nonnull countString) {};
        return RPVC;
    }else if (index == 1){
        FWRankViewController * rankVC = [[FWRankViewController alloc] init];
        rankVC.sport_id = self.sport_id;
        rankVC.myBlock = ^(NSString * _Nonnull countString) {};
        return rankVC;
    }else{
        FWGuessingCompetitionViewController * guessVC = [[FWGuessingCompetitionViewController alloc] init];
        guessVC.sport_id = self.sport_id;
        guessVC.myBlock = ^(NSString * _Nonnull countString) {};
        return guessVC;
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView
{
    return CGRectMake(0,self.wmMenuY, self.view.frame.size.width, self.menuHeight);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView
{
    return CGRectMake(0, self.wmMenuY+self.menuHeight, self.view.frame.size.width, self.view.frame.size.height - self.menuHeight-self.wmMenuY);
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}
@end
