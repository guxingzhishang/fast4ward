//
//  FWPlayerWonderfulVideoViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 车手精彩视频
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerWonderfulVideoViewController : FWBaseViewController

@property (nonatomic, strong) NSString * videoURL;
@property (nonatomic, strong) NSString * videoCover;

@end

NS_ASSUME_NONNULL_END
