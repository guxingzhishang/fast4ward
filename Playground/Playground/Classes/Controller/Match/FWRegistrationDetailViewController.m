//
//  FWRegistrationDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRegistrationDetailViewController.h"
#import "FWConversationViewController.h"
#import "FWRegistrationDetailShareViewController.h"
#import "FWPlayerRegistrationViewController.h"
#import "FWMySignupViewController.h"
#import "FWPerfectInfomationViewController.h"
#import "FWPerfectInfoModel.h"

#import "FWPlayerSignupViewController.h"


@interface FWRegistrationDetailViewController ()

@property (nonatomic, strong) FWBaomingInfoModel * infoModel;
@property (nonatomic, strong) UIView * naviBarView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) UILabel * navititleLabel;
@property (nonatomic, strong) FWPerfectInfoModel * perfectInfoModel;
@property (nonatomic, strong) FWWechatOrderModel * wechatModel ;

@property (nonatomic, assign) CGFloat alphaValue;

@end

@implementation FWRegistrationDetailViewController
@synthesize naviBarView;
@synthesize navititleLabel;
@synthesize backButton;
@synthesize shareButton;
@synthesize alphaValue;

- (void)requestRongYunTokenIM{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_im_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:userId];
                                            [RCIM sharedRCIM].currentUserInfo = userInfo;
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"IM登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            NSLog(@"IMtoken错误");
                                        }];
        }
    }];
}

#pragma mark - > 请求完善信息页数据
- (void)requestSelectInfo{

    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];

    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"baoming_user_id":self.infoModel.baoming_user_id,
                              };

    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_baoming_select_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.perfectInfoModel = [FWPerfectInfoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
        }
    }];
}

#pragma mark - > 请求报名详情
- (void)requestBaoMingInfo{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"baoming_id":self.baoming_id,
                              };
    
    [request startWithParameters:params WithAction:Get_baoming_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.infoModel = [FWBaomingInfoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self requestSelectInfo];
            
            NSString *  htmlStr = [self.infoModel.h5_sport_desc stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:htmlStr]];
            [self.detailWebView loadRequest:request];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.topView.frame = CGRectMake(0, -(239+FWCustomeSafeTop)-49, SCREEN_WIDTH, 239+FWCustomeSafeTop);
                [self.descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.size.mas_equalTo(CGSizeMake(160, 49));
                    make.top.mas_equalTo(self.topView.mas_bottom);
                    make.left.mas_equalTo(self.topView).mas_offset(14);
                }];
                if (@available(iOS 11.0, *)) {
                    
                    self.detailWebView.scrollView.contentInset = UIEdgeInsetsMake(219+49, 0, 0, 0);
                }else{
                    self.detailWebView.scrollView.contentInset = UIEdgeInsetsMake(239+49, 0, 0, 0);
                }
                
                [self.detailWebView.scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            });

            self.titleLabel.text = self.infoModel.sport_name;

            if (self.infoModel.sport_type_name.length > 0) {
                self.nameLabel.hidden = NO;
                self.nameLabel.text = [NSString stringWithFormat:@"%@%@",self.infoModel.sport_type_name,@"   "];
            }else{
                self.nameLabel.hidden = YES;
            }

            self.timeLabel.text = [NSString stringWithFormat:@"比赛时间：%@",self.infoModel.sport_date];
            self.areaLabel.text = [NSString stringWithFormat:@"地点：%@",self.infoModel.sport_area];

            if ([self.infoModel.fee_type isEqualToString:@"3"]) {
                self.sumLabel.hidden = YES;
                self.typeLabel.hidden = YES;
                self.statusLabel.hidden = NO;

                self.statusLabel.text = self.infoModel.show_fee_type;
            }else{
                self.sumLabel.hidden = NO;
                self.typeLabel.hidden = NO;
                self.statusLabel.hidden = YES;

                self.sumLabel.text = self.infoModel.show_money;
                self.typeLabel.text = self.infoModel.show_fee_type;
            }

            [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:self.infoModel.toutu_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            self.coverImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.coverImageView.clipsToBounds = YES;

            [self.topView sd_setImageWithURL:[NSURL URLWithString:self.infoModel.toutu_beijing_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            self.topView.contentMode = UIViewContentModeScaleAspectFill;
            self.topView.clipsToBounds = YES;
            
            
            if ([self.infoModel.my_baoming_status isEqualToString:@"1"]) {
                /* 未报名 */
                if ([self.infoModel.baoming_status isEqualToString:@"1"]) {
                    /* 未开始 */
                    self.signupButton.enabled = NO;
                    self.signupButton.backgroundColor = FWColorWihtAlpha(@"#68769F", 0.4);
                    [self.signupButton setTitle:self.infoModel.begin_time_show forState:UIControlStateNormal];
                }else if ([self.infoModel.baoming_status isEqualToString:@"2"]){
                    /* 进行中 */
                    self.signupButton.enabled = YES;
                    self.signupButton.backgroundColor = FWTextColor_222222;
                    [self.signupButton setTitle:@"立即报名" forState:UIControlStateNormal];
                }else if ([self.infoModel.baoming_status isEqualToString:@"3"]){
                    /* 已结束 */
                    self.signupButton.enabled = NO;
                    self.signupButton.backgroundColor = FWViewBackgroundColor_E5E5E5;
                    [self.signupButton setTitle:@"已结束" forState:UIControlStateNormal];
                }else if ([self.infoModel.baoming_status isEqualToString:@"4"]){
                    /* 名额已满 */
                    self.signupButton.enabled = NO;
                    self.signupButton.backgroundColor = FWViewBackgroundColor_E5E5E5;
                    [self.signupButton setTitle:@"名额已满" forState:UIControlStateNormal];
                }
            }else if ([self.infoModel.my_baoming_status isEqualToString:@"2"]){
                /* 报名成功且已支付 */
                if ([self.infoModel.perfect_info_status isEqualToString:@"1"]){
                    /* 完善个人信息 */
                    self.signupButton.enabled = YES;
                    self.signupButton.backgroundColor = FWTextColor_222222;
                    [self.signupButton setTitle:@"立即完善报名信息" forState:UIControlStateNormal];
                }else if([self.infoModel.perfect_info_status isEqualToString:@"2"]){
                    /* 查看个人信息 */
                    self.signupButton.enabled = YES;
                    self.signupButton.backgroundColor = FWTextColor_222222;
                    [self.signupButton setTitle:@"我的报名信息" forState:UIControlStateNormal];
                }else if([self.infoModel.perfect_info_status isEqualToString:@"3"]){
                    /* 显示修改报名信息 */
                    self.signupButton.enabled = YES;
                    self.signupButton.backgroundColor = FWTextColor_222222;
                    [self.signupButton setTitle:@"修改我的报名信息" forState:UIControlStateNormal];
                }
            }else if ([self.infoModel.my_baoming_status isEqualToString:@"3"]){
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:self.infoModel.pay_tip preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"暂不" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"立即缴费" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    [self payButtonClick:@"wechat"];
                    FWPlayerSignupViewController * psvc = [[FWPlayerSignupViewController alloc] init];
                    psvc.baoming_user_id = self.infoModel.baoming_user_id;
                    psvc.isShowAlert = YES;
                    [self.navigationController pushViewController:psvc animated:YES];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                
                /* 报名成功未支付 */
                self.signupButton.enabled = YES;
                self.signupButton.backgroundColor = FWTextColor_222222;
                [self.signupButton setTitle:@"立即缴费" forState:UIControlStateNormal];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"报名详情页"];
    [self requestRongYunTokenIM];
    [self requestBaoMingInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"报名详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    
    alphaValue = 0;

    [self setupNaviBarView];
    [self setupBottomView];
    [self setupTopView];
}

- (void)setupNaviBarView{
    
    naviBarView = [[UIView alloc] init];
    naviBarView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    naviBarView.userInteractionEnabled = YES;
    [self.view addSubview:naviBarView];
    naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    naviBarView.alpha = 0;
    
    navititleLabel = [[UILabel alloc] init];
    navititleLabel.text = @"报名详情";
    navititleLabel.font = DHSystemFontOfSize_18;
    navititleLabel.textColor = FWTextColor_000000;
    navititleLabel.textAlignment = NSTextAlignmentCenter;
    [naviBarView addSubview:navititleLabel];
    [navititleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(naviBarView);
        make.top.mas_equalTo(naviBarView).mas_offset(23+FWCustomeSafeTop);
        make.height.mas_equalTo(35);
    }];

    self.backButton = [[UIButton alloc] init];
    [self.backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.naviBarView).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.top.mas_equalTo(self.naviBarView).mas_offset(20+FWCustomeSafeTop);
    }];
    
    self.shareButton = [[UIButton alloc] init];
    [self.shareButton setImage:[UIImage imageNamed:@"new_black_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.shareButton];
    [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.naviBarView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.centerY.mas_equalTo(self.backButton);
    }];
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        self.shareButton.hidden = NO;
    }else{
        self.shareButton.hidden = YES;
    }
}

#pragma mark - > 分享
- (void)shareButtonClick{
  
    FWRegistrationDetailShareViewController * vc = [[FWRegistrationDetailShareViewController alloc] init];
    vc.share_url = self.infoModel.fenixang_img;
    vc.qrcode_url = self.infoModel.qrcode_url;
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - > 返回
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 报名咨询
- (void)contectButtonClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:self.infoModel.kefu_uid];
        [self.navigationController pushViewController:CVC animated:YES];
    }
}

#pragma mark - > 立即报名
- (void)signupButtonClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        if ([self.infoModel.my_baoming_status isEqualToString:@"1"]) {
            /* 未报名 */
            if ([self.infoModel.baoming_status isEqualToString:@"2"]){
                /* 进行中 */
                FWPlayerRegistrationViewController * PRVC = [[FWPlayerRegistrationViewController alloc] init];
                PRVC.baoming_id = self.baoming_id;
                [self.navigationController pushViewController:PRVC animated:YES];
            }
        }else if ([self.infoModel.my_baoming_status isEqualToString:@"2"]){
            /* 报名成功且已支付 */
           
            if (!self.perfectInfoModel.driver_info.nickname) {
                [self requestSelectInfo];
                return;
            }
            
            FWPerfectInfomationViewController * PIVC = [[FWPerfectInfomationViewController alloc] init];
            PIVC.arrayModel = self.perfectInfoModel;
            PIVC.baoming_user_id = self.infoModel.baoming_user_id;
            if ([self.infoModel.perfect_info_status isEqualToString:@"1"]){
                /* 完善个人信息 */
                PIVC.canEdited = YES;
            }else if([self.infoModel.perfect_info_status isEqualToString:@"2"]){
                /* 查看个人信息 */
                PIVC.canEdited = NO;
            }else if([self.infoModel.perfect_info_status isEqualToString:@"3"]){
                /* 修改个人信息 */
                PIVC.canEdited = YES;
            }
            [self.navigationController pushViewController:PIVC animated:YES];
        }else if ([self.infoModel.my_baoming_status isEqualToString:@"3"]){
            /* 报名成功未支付 */
//            [self payButtonClick:@"wechat"];
            FWPlayerSignupViewController * psvc = [[FWPlayerSignupViewController alloc] init];
            psvc.baoming_user_id = self.infoModel.baoming_user_id;
            psvc.isShowAlert = YES;
            [self.navigationController pushViewController:psvc animated:YES];
        }
    }
}

#pragma mark - > 初始化视图
- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupTopView{
 
    self.topView = [[UIImageView alloc] init];
    self.topView.userInteractionEnabled = YES;
    self.topView.contentMode = UIViewContentModeScaleAspectFill;
    self.topView.clipsToBounds = YES;
    [self.detailWebView.scrollView addSubview:self.topView];
    
//    UIToolbar *bar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH, CGRectGetHeight(self.topView.frame))];
//    bar.barStyle = UIBarStyleBlack;
//    bar.alpha = 0.8;
//    [self.topView addSubview:bar];
    
    self.blackBackButton = [[UIButton alloc] init];
    [self.blackBackButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.blackBackButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.blackBackButton];
    [self.blackBackButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(self.topView).mas_offset(25+FWCustomeSafeTop);
    }];

    self.blackShareButton = [[UIButton alloc] init];
    [self.blackShareButton setImage:[UIImage imageNamed:@"new_white_share"] forState:UIControlStateNormal];
    [self.blackShareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.blackShareButton];
    [self.blackShareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.topView).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(36, 20));
        make.centerY.mas_equalTo(self.blackBackButton);
    }];

    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        self.blackShareButton.hidden = NO;
    }else{
        self.blackShareButton.hidden = YES;
    }
    
    self.coverImageView = [[UIImageView alloc] init];
    self.coverImageView.layer.cornerRadius = 5;
    self.coverImageView.layer.masksToBounds = YES;
    self.coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.coverImageView.image = [UIImage imageNamed:@"placeholder"];
    [self.topView addSubview:self.coverImageView];
    [self.coverImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topView).mas_offset(14);
        make.top.mas_equalTo(self.topView).mas_offset(73+FWCustomeSafeTop);
        make.size.mas_equalTo(CGSizeMake(111, 148));
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.coverImageView.mas_right).mas_offset(14);
        make.right.mas_equalTo(self.topView).mas_offset(-10);
        make.top.mas_equalTo(self.coverImageView);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.layer.borderColor = FWColor(@"ffffff").CGColor;
    self.nameLabel.layer.borderWidth = 1;
    self.nameLabel.layer.cornerRadius = 1;
    self.nameLabel.layer.masksToBounds = YES;
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.topView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel);
        make.height.mas_equalTo(16);
        make.width.mas_greaterThanOrEqualTo(20);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(5);
    }];
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.timeLabel];
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.topView);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(31);
    }];
    
    self.areaLabel = [[UILabel alloc] init];
    self.areaLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.areaLabel.numberOfLines = 0;
    self.areaLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.areaLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.areaLabel];
    [self.areaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.timeLabel);
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(0);
        make.right.mas_equalTo(self.topView);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.sumLabel = [[UILabel alloc] init];
    self.sumLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.sumLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 20];
    self.sumLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.sumLabel];
    [self.sumLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.areaLabel);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(25);
        make.bottom.mas_equalTo(self.coverImageView);
    }];
    
    self.typeLabel = [[UILabel alloc] init];
    self.typeLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.typeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.typeLabel.textAlignment = NSTextAlignmentLeft;
    [self.topView addSubview:self.typeLabel];
    [self.typeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.sumLabel.mas_right).mas_offset(3);
        make.centerY.mas_equalTo(self.sumLabel).mas_offset(1);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(17);
    }];
    
    self.statusLabel = [[UILabel alloc] init];
    self.statusLabel.layer.borderColor = FWClearColor.CGColor;
    self.statusLabel.layer.borderWidth = 1;
    self.statusLabel.layer.cornerRadius = 1;
    self.statusLabel.layer.masksToBounds = YES;
    self.statusLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.statusLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self.topView addSubview:self.statusLabel];
    [self.statusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.areaLabel);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.coverImageView);
    }];
    
    
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.coverImageView.clipsToBounds = YES;
    
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.text = @"赛事介绍";
    self.descLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.descLabel.textColor = FWTextColor_272727;
    self.descLabel.textAlignment = NSTextAlignmentLeft;
    [self.detailWebView.scrollView addSubview:self.descLabel];
    
    self.topView.hidden = YES;
    self.descLabel.hidden = YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.topView.hidden = NO;
        self.descLabel.hidden = NO;
    });
}

- (void)setupBottomView{
    
    self.detailWebView = [[UIWebView alloc] init];
    self.detailWebView.scalesPageToFit = YES;
    self.detailWebView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.detailWebView.scrollView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.detailWebView.scrollView.showsVerticalScrollIndicator = NO;
    self.detailWebView.scrollView.showsHorizontalScrollIndicator = NO;
    self.detailWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.detailWebView.contentMode =UIViewContentModeScaleAspectFit;
    self.detailWebView.scrollView.delegate = self;
    [self.view addSubview:self.detailWebView];
    self.detailWebView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -(60+FWSafeBottom));
    
    self.bottomView = [[UIView alloc] init];
    self.bottomView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view).mas_offset(0);
        make.bottom.mas_equalTo(self.view);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(60+FWSafeBottom);
    }];
    
    self.contectButton = [[UIButton alloc] init];
    [self.contectButton addTarget:self action:@selector(contectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:self.contectButton];
    [self.contectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(self.bottomView);
        make.size.mas_equalTo(CGSizeMake(80, 60));
    }];
    
    UIImageView * contectIV = [[UIImageView alloc] init];
    contectIV.image = [UIImage imageNamed:@"signup_connect"];
    [self.contectButton addSubview:contectIV];
    [contectIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(19, 22));
        make.top.mas_equalTo(self.bottomView).mas_offset(14);
        make.left.mas_equalTo(self.bottomView).mas_offset(31);
    }];
    
    UILabel * contectLabel = [[UILabel alloc] init];
    contectLabel.text = @"报名咨询";
    contectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
    contectLabel.textColor = FWColor(@"9FA3AA");
    contectLabel.textAlignment = NSTextAlignmentCenter;
    [self.contectButton addSubview:contectLabel];
    [contectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(contectIV);
        make.top.mas_equalTo(contectIV.mas_bottom).mas_offset(3);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(15);
    }];
    
    self.signupButton = [[UIButton alloc] init];
    self.signupButton.layer.cornerRadius = 2;
    self.signupButton.layer.masksToBounds = YES;
    self.signupButton.titleLabel.font = [UIFont fontWithName:@"PingFang SC" size: 16];
    [self.signupButton addTarget:self action:@selector(signupButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:self.signupButton];
    [self.signupButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contectButton.mas_right);
        make.top.mas_equalTo(self.bottomView).mas_offset(8);
        make.right.mas_equalTo(self.bottomView).mas_offset(-14);
        make.height.mas_equalTo(44);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    [self.view bringSubviewToFront:naviBarView];
}



- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == self.detailWebView.scrollView) {
        
        if (scrollView.contentOffset.y+288 > 30){
            alphaValue = 1;
        }else{
            alphaValue = ((scrollView.contentOffset.y+288)/30);
        }
        naviBarView.alpha = alphaValue;
        navititleLabel.alpha = alphaValue;
        shareButton.alpha = alphaValue;
        backButton.alpha = alphaValue;
        
        if (scrollView.contentOffset.y+288 >0) {
            self.blackBackButton.hidden = YES;
            self.blackShareButton.hidden = YES;
        }else{
            self.blackBackButton.hidden = NO;
            self.blackShareButton.hidden = NO;
        }
    }
}


#pragma mark - > 去支付
- (void)payButtonClick:(NSString *)payMethod{
    
    if ([payMethod isEqualToString:@"wechat"]) {
        [self requestPay:@"weixin"];
    }else if ([payMethod isEqualToString:@"alipay"]){
        [self requestPay:@"alipay"];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:payMethod forKey:@"支付返回"];
}

#pragma mark - > 调起微信支付
- (void)payrequestWithModel:(FWWechatOrderModel *)model {
    
    NSString * timeStampStr = model.timestamp;
    UInt32 timesta = (UInt32)[timeStampStr intValue];
    
    PayReq *request = [[PayReq alloc] init];
    request.sign      = model.sign;
    request.package   = @"Sign=WXPay";
    request.nonceStr  = model.noncestr;
    request.prepayId  = model.prepayid;
    request.partnerId = model.partnerid;
    request.timeStamp = timesta;
    
    /* 调起支付 */
    if ([WXApi sendReq:request]) {
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"支付失败" message:@"未安装微信客户端,请使用其他支付方式" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - > 微信pay成功
- (void)paySucceed{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"订单号"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"支付返回"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    FWMySignupViewController * MSVC = [[FWMySignupViewController alloc] init];
    [self.navigationController pushViewController:MSVC animated:YES];
}

#pragma mark - > 微信pay失败
- (void)payFailed{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付取消" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 自行操作返回pay失败
- (void)selfPayFailed{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付失败，请重试" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 提交支付(微信/支付宝)
- (void)requestPay:(NSString *)type{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    if (self.infoModel.baoming_user_id.length < 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"未查询到用户报名ID，请联系工作人员咨询" toController:self];
        return;
    }
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"pay_service":type,
                              @"baoming_user_id":self.infoModel.baoming_user_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_baoming_pay WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([type isEqualToString:@"weixin"]) {
                /* 微信支付 */
                NSDictionary * result = [[back objectForKey:@"data"] objectForKey:@"result_weixin"];
                NSDictionary * pay_order_id = [[back objectForKey:@"data"] objectForKey:@"pay_order_id"];
                
                self.wechatModel = [FWWechatOrderModel mj_objectWithKeyValues:result];
                [self payrequestWithModel:self.wechatModel];
                
                [[NSUserDefaults standardUserDefaults] setObject:pay_order_id forKey:@"订单号"];
            }else if ([type isEqualToString:@"alipay"]){
                /* 支付宝支付 */
                //                [[NSUserDefaults standardUserDefaults] setObject:[[back objectForKey:@"data"] objectForKey:@"pay_order_id"] forKey:@"订单号"];
                //                [self alipayRequestWithResult:[[back objectForKey:@"data"] objectForKey:@"result"]];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


@end

