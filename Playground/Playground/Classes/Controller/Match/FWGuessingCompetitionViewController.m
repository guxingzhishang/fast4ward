
//
//  FWGuessingCompetitionViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGuessingCompetitionViewController.h"
#import "FWGuessingBettingViewController.h"
#import "FWGuessingCompetitionCell.h"
#import "FWGuessingRecordViewController.h"
#import "FWGuessingModel.h"
#import "FWMyGoldenViewController.h"

@interface FWGuessingCompetitionViewController ()<UITableViewDelegate,UITableViewDataSource,FWGuessingCompetitionCellDelegate>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger pageNum;

@property (nonatomic, strong) UIButton * ruleButton;
@property (nonatomic, strong) UIButton * enableButton;
@property (nonatomic, strong) UIButton * myJingcaiButton;
@property (nonatomic, strong) UIButton * refreshButton;
@property (nonatomic, strong) FWGuessingModel * guessingModel;
@property (nonatomic, strong) UIView * footView;
@property (nonatomic, strong) UILabel * placeholderLabel;
@property (nonatomic, assign) NSInteger currentRow;
@end

@implementation FWGuessingCompetitionViewController
@synthesize infoTableView;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

- (void)requestData:(BOOL)isLoadMore{
    
    if ([self.guess_id isEqualToString:@"0"]) {
        /* 全局刷新才用下面这个逻辑 */
        if (isLoadMore == NO) {
            self.pageNum = 1;
            
            if (self.dataSource.count > 0 ) {
                [self.dataSource removeAllObjects];
            }
        }else{
            self.pageNum +=1;
        }
    }
    
    if (!self.sport_id) {
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"sport_id":self.sport_id,
                                @"page":@(self.pageNum).stringValue,
                                @"page_size":@"30",
                                @"guess_id":self.guess_id,
                            };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_sport_guess_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
           
            if ([self.guess_id isEqualToString:@"0"]) {
                if (isLoadMore) {
                    [infoTableView.mj_footer endRefreshing];
                }else{
                    self.infoTableView.mj_footer.hidden = NO;
                    [infoTableView.mj_header endRefreshing];
                }
                
                self.guessingModel= [FWGuessingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                
                if (self.guessingModel.live_user_count.length > 0) {
                    self.myBlock(self.guessingModel.live_user_count);
                }
                
                for (FWGuessingListModel * listModel in self.guessingModel.guess_list) {
                    if ([listModel.bid_status isEqualToString:@"1"]) {
                        listModel.showMore = YES;
                    }
                }
                
                [self.dataSource addObjectsFromArray: self.guessingModel.guess_list];

                [self.enableButton setTitle:[NSString stringWithFormat:@"可用金币: %@",self.guessingModel.gold_balance_value] forState:UIControlStateNormal];

                if (self.dataSource.count <= 0) {
                    self.placeholderLabel.hidden = NO;
                    self.footView.frame =  CGRectMake(0, 0, SCREEN_WIDTH, 200);
                    infoTableView.tableFooterView = self.footView;
                    
                    infoTableView.mj_header = nil;
                    infoTableView.mj_footer = nil;
                }else{
                    self.placeholderLabel.hidden = YES;
                    self.footView.frame =  CGRectMake(0, 0, SCREEN_WIDTH, 50);
                    infoTableView.tableFooterView = self.footView;
                }
                
                if([self.guessingModel.guess_list count] == 0 &&
                   self.pageNum != 1){
                    self.infoTableView.mj_footer.hidden = YES;
                    [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
                }else{
                    [infoTableView reloadData];
                }
                
                if (!isLoadMore) {
                    // 下拉刷新，或者第一次请求，直接滚动到顶部
                    [self.infoTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                }
            }else{
                if (self.dataSource.count > self.currentRow) {
                    self.guessingModel= [FWGuessingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

                    FWGuessingListModel * newListModel = self.guessingModel.guess_list[0];
                    newListModel.showMore = YES;
                    self.dataSource[self.currentRow] = newListModel;
                    
                    [self.enableButton setTitle:[NSString stringWithFormat:@"可用金币: %@",self.guessingModel.gold_balance_value] forState:UIControlStateNormal];

                    [UIView setAnimationsEnabled:NO];
                    if (@available(iOS 11.0, *)) {
                        [self.infoTableView performBatchUpdates:^{
                            [self.infoTableView reloadRow:self.currentRow inSection:0 withRowAnimation:UITableViewRowAnimationNone];
                        } completion:^(BOOL finished) {
                            [UIView setAnimationsEnabled:YES];
                        }];
                    } else {
                        [self.infoTableView reloadRow:self.currentRow inSection:0 withRowAnimation:UITableViewRowAnimationNone];
                        [UIView setAnimationsEnabled:YES];
                    }
                }
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"精彩页"];
    
    self.view.backgroundColor = FWTextColor_FAFAFA;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"精彩页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.guess_id= @"0";
    self.pageNum = 1;
    
    [self setupTopViews];
    [self setupSubViews];
    [self requestData:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.backgroundColor = FWTextColor_FAFAFA;
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.ruleButton.mas_bottom).mas_offset(5);
    }];

    self.footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    
    self.placeholderLabel = [[UILabel alloc] init];
    self.placeholderLabel.text = @"当前无比赛竞猜";
    self.placeholderLabel.font = DHFont(14);
    self.placeholderLabel.textColor = FWTextColor_BCBCBC;
    self.placeholderLabel.textAlignment = NSTextAlignmentCenter;
    [self.footView addSubview:self.placeholderLabel];
    self.placeholderLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH, 200);
    self.placeholderLabel.hidden = YES;
    
    infoTableView.tableFooterView = self.footView;

    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.guess_id = @"0";
        [weakSelf requestData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.guess_id = @"0";
        [weakSelf requestData:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"GuessingCompetitionID";
    
    FWGuessingCompetitionCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWGuessingCompetitionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.delegate = self;
    cell.vc = self;
    
    cell.showButton.tag = 8899+indexPath.row;
    cell.leftTouzhuButton.tag = 6666+indexPath.row;
    cell.rightTouzhuButton.tag = 7777+indexPath.row;

    if (indexPath.row < self.dataSource.count) {
        [cell configForView:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)showButtonClickShowMore:(BOOL)isShowMore withCell:(id)cell withIndex:(NSInteger)index{
    
    FWGuessingListModel * listModel = self.dataSource[index];
    listModel.showMore = !listModel.showMore;
    self.dataSource[index] = listModel;

    [UIView setAnimationsEnabled:NO];
    if (@available(iOS 11.0, *)) {
        [self.infoTableView performBatchUpdates:^{
            [self.infoTableView reloadRow:index inSection:0 withRowAnimation:UITableViewRowAnimationNone];
        } completion:^(BOOL finished) {
            [UIView setAnimationsEnabled:YES];
        }];
    } else {
        [self.infoTableView reloadRow:index inSection:0 withRowAnimation:UITableViewRowAnimationNone];
        [UIView setAnimationsEnabled:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWGuessingListModel * listModel = self.dataSource[indexPath.row];
    if (!listModel.showMore) {
        return 137;
    }else if(listModel.showMore && [listModel.bid_status isEqual:@"3"]){
        return 171;
    }else if (listModel.showMore && [listModel.bid_status isEqual:@"1"]){
        return 191;
    }
    
    return 137;
}

- (void)setupTopViews{
 
    self.ruleButton = [[UIButton alloc] init];
    self.ruleButton.layer.cornerRadius = 2;
    self.ruleButton.layer.masksToBounds = YES;
    self.ruleButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.ruleButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.ruleButton setTitle:@"竞猜规则" forState:UIControlStateNormal];
    [self.ruleButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
    [self.ruleButton addTarget:self action:@selector(ruleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.ruleButton];
    self.ruleButton.frame = CGRectMake(14, 10, 81, 25);
    
    self.enableButton = [[UIButton alloc] init];
    self.enableButton.layer.cornerRadius = 2;
    self.enableButton.layer.masksToBounds = YES;
    self.enableButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.enableButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.enableButton setTitle:@"可用金币: --" forState:UIControlStateNormal];
    [self.enableButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
    [self.enableButton addTarget:self action:@selector(enableButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.enableButton];
    self.enableButton.frame = CGRectMake(CGRectGetMaxX(self.ruleButton.frame)+5, CGRectGetMinY(self.ruleButton.frame), SCREEN_WIDTH-233, CGRectGetHeight(self.ruleButton.frame));
    
    self.myJingcaiButton = [[UIButton alloc] init];
    self.myJingcaiButton.layer.cornerRadius = 2;
    self.myJingcaiButton.layer.masksToBounds = YES;
    self.myJingcaiButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.myJingcaiButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.myJingcaiButton setTitle:@"我的竞猜" forState:UIControlStateNormal];
    [self.myJingcaiButton setTitleColor:FWTextColor_515151 forState:UIControlStateNormal];
    [self.myJingcaiButton addTarget:self action:@selector(myJingcaiButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.myJingcaiButton];
    self.myJingcaiButton.frame = CGRectMake(CGRectGetMaxX(self.enableButton.frame)+5,  CGRectGetMinY(self.ruleButton.frame), CGRectGetWidth(self.ruleButton.frame), CGRectGetHeight(self.ruleButton.frame));
    
    self.refreshButton = [[UIButton alloc] init];
    self.refreshButton.layer.cornerRadius = 2;
    self.refreshButton.layer.masksToBounds = YES;
    [self.refreshButton setImage:[UIImage imageNamed:@"guess_refresh"] forState:UIControlStateNormal];
    self.refreshButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.refreshButton addTarget:self action:@selector(refreshButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.refreshButton];
    self.refreshButton.frame = CGRectMake(CGRectGetMaxX(self.myJingcaiButton.frame)+5,  CGRectGetMinY(self.myJingcaiButton.frame), 28, 25);
}

#pragma mark - > 竞猜规则
- (void)ruleButtonClick{
 
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.guessingModel.help_url;
    [self.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 可用金币
- (void)enableButtonClick{
    [self checkLogin];

    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        [self.navigationController pushViewController:[FWMyGoldenViewController new] animated:YES];
    }
}

#pragma mark - > 刷新
- (void)refreshButtonClick{
    
    self.guess_id = @"0";
    
    [self requestData:NO];
}

#pragma mark - > 我的竞猜
- (void)myJingcaiButtonClick{
    
    [self checkLogin];
           
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWGuessingRecordViewController * GRVC = [[FWGuessingRecordViewController alloc] init];
        GRVC.sport_id = self.sport_id;
        [self.navigationController pushViewController:GRVC animated:YES];
    }
}

#pragma mark - > 下注
- (void)submitSportGuessWithType:(NSString *)type withIndex:(NSInteger)index{
    
    [self checkLogin];
           
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        DHWeakSelf;
        self.currentRow = index;
        
        FWGuessingBettingViewController * GBVC = [[FWGuessingBettingViewController alloc] init];
        GBVC.myBlock = ^(NSString * _Nonnull guess_id) {
            weakSelf.guess_id = guess_id;
            [weakSelf requestData:NO];
        };
        GBVC.type = type;
        GBVC.listModel = self.dataSource[index];
        GBVC.gold_balance_value = self.guessingModel.gold_balance_value;
        GBVC.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        GBVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:GBVC animated:YES completion:nil];
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}
@end
