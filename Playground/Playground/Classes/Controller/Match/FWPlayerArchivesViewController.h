//
//  FWPlayerArchivesViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 车手档案
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerArchivesViewController : FWBaseViewController
@property (nonatomic, strong) NSString * driver_id;
@end

NS_ASSUME_NONNULL_END
