//
//  FWHotLiveViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHotLiveViewController.h"
#import "ZFPlayer.h"
#import "ZFAVPlayerManager.h"
#import "ZFIJKPlayerManager.h"
#import "KSMediaPlayerManager.h"
#import "FWHotLivePlayerControlView.h"
#import "UIImageView+ZFCache.h"
#import "ZFUtilities.h"
#import <AVFoundation/AVFoundation.h>
#import "FWHotLiveCollectionCell.h"
#import "FWHotLiveShareViewController.h"
#import "FWLiveTipView.h"

static NSString * const banNotifyContent = @"您已被管理员禁言";

static NSString * const ConversationMessageCollectionViewCell = @"ConversationMessageCollectionViewCell";

static NSString *const textCellIndentifier = @"textCellIndentifier";

static NSString *const startAndEndCellIndentifier = @"startAndEndCellIndentifier";

@interface FWHotLiveViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, RCCRInputBarControlDelegate, UIGestureRecognizerDelegate, RCCRLoginViewDelegate, RCCRGiftViewDelegate, RCCRHostInformationViewDelegate,FWHotLivePlayerControlViewDelegate,FWLiveTipViewDelegate>

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) UIImageView *containerView;
@property (nonatomic, strong) FWHotLivePlayerControlView *controlView;
@property (nonatomic, strong) UIButton *playBtn;
@property (nonatomic, strong) NSArray <NSURL *>*assetURLs;
@property (nonatomic, strong) ZFAVPlayerManager *playerManager;
/* 最近一次的网络状态  wifi 还是 4G */
@property (nonatomic, strong) NSString * lastNetWork;
@property (nonatomic, strong) FWLiveTipView * tipView;

@property (nonatomic, strong) UIButton * closeButton;

@property (nonatomic, strong) UIView * userView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * numLabel;
@property (nonatomic, strong) UIButton * attentionButton;

@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, assign) BOOL isLive;// 正在直播，只请求一次，其余请求，是为了看人数


/* 消息列表CollectionView和输入框都在这个view里 */
@property(nonatomic, strong) UIView *messageContentView;
/* 会话页面的CollectionView */
@property(nonatomic, strong) UICollectionView *conversationMessageCollectionView;
/* 输入工具栏 */
@property(nonatomic,strong) RCCRInputBarControl *inputBar;
/* 底部按钮容器，底部的四个按钮都添加在此view上*/
@property(nonatomic, strong) UIView *bottomBtnContentView;
/* 评论按钮 */
@property(nonatomic,strong)UIButton *commentBtn;
/* 登录的界面 */
@property(nonatomic,strong)RCCRLoginView *loginView;
/* 是否需要滚动到底部 */
@property(nonatomic, assign) BOOL isNeedScrollToButtom;
/* 滚动条不在底部的时候，接收到消息不滚动到底部，记录未读消息数 */
@property (nonatomic, assign) NSInteger unreadNewMsgCount;

/* 聊天内容的消息Cell数据模型的数据源
 @discussion 数据源中存放的元素为消息Cell的数据模型，即RCDLiveMessageModel对象。*/
@property(nonatomic, strong) NSMutableArray<RCCRMessageModel *> *conversationDataRepository;

@end

@implementation FWHotLiveViewController

- (void)requestLiveInfo{
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"live_id":self.liveListModel.live_id,
                              @"type":@"zhibo",
                              };
    
    [request startWithParameters:params WithAction:Get_zhibo_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.liveListModel = [FWLiveListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            self.numLabel.text = [NSString stringWithFormat:@"%@人观看",self.liveListModel.user_total];

            if (self.isLive) {
                self.isLive = !self.isLive;
            
                self.assetURLs = @[[NSURL URLWithString:self.liveListModel.list_url.hls_url]];
                self.player.assetURLs = self.assetURLs;
                [self.player playTheIndex:0];
            }
        }else{
            
            if ([[back objectForKey:@"errno"] isEqualToString:@"-10"]) {
                
                [self.timer invalidate];
                [[FWHudManager sharedManager] showErrorMessage:@"直播已结束，下期再见" toController:self];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });

                return ;
            }
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc {
    
    NSLog(@"销毁了");
    self.timer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self trackPageBegin:@"直播页"];

    [self.timer setFireDate:[NSDate date]];
    self.player.viewControllerDisappear = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    
    if (![self.currentID isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        [self requestRongYunTokenChatroom];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self trackPageEnd:@"直播页"];

    [self.timer setFireDate:[NSDate distantFuture]];

    self.player.viewControllerDisappear = YES;
    [[RCCRManager sharedRCCRManager] setUserUnban];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.containerView];

    self.isLive = YES;
    
    [self.containerView addSubview:self.playBtn];

    [self setupPlayer];
    [self setupTopView];
    
    [self initializedSubViews];
    
    [self checkWifi];
    
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] setIsLogin:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendAndShowMessage:) name:RongYunRecieveNofi object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteMessage:) name:RongYunChangeRoomNofi object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HidekeyboardHide:) name:HideKeyboard object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(monitorLiveNet) name:MonitorLiveNetworking object:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(requestLiveInfo) userInfo:nil repeats:YES];
    });
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat w = CGRectGetWidth(self.view.frame);
    CGFloat h = CGRectGetHeight(self.view.frame);
    self.containerView.frame = CGRectMake(x, y, w, h);
    
    w = 44;
    h = w;
    x = (CGRectGetWidth(self.containerView.frame)-w)/2;
    y = (CGRectGetHeight(self.containerView.frame)-h)/2;
    self.playBtn.frame = CGRectMake(x, y, w, h);
    
    w = SCREEN_WIDTH;
    h = 50+FWCustomeSafeTop;
    x = 0;
    y = SCREEN_HEIGHT - 60;
    self.bottomView.frame = CGRectMake(x, y, w, h);
    
    [self updateTextViewFrame];
}

#pragma mark - > 播放器相关配置
- (void)setupPlayer{
    self.playerManager= [[ZFAVPlayerManager alloc] init];
    self.playerManager.scalingMode = ZFPlayerScalingModeAspectFill;
    
    /// 播放器相关
    self.player = [ZFPlayerController playerWithPlayerManager:self.playerManager containerView:self.containerView];
    self.player.controlView = self.controlView;
    self.player.lockedScreen = YES;
    self.player.allowOrentitaionRotation = NO;
    /// 设置退到后台继续播放
    self.player.pauseWhenAppResignActive = NO;
    
    self.player.customAudioSession = YES;
    self.player.playerReadyToPlay = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSURL * _Nonnull assetURL) {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:nil];
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
    };
}

#pragma mark - > 设置顶部视图
- (void)setupTopView{
 
    self.tipView = [[FWLiveTipView alloc] initWithFrame:self.view.frame];
    self.tipView.delegate = self;
    [self.tipView.bgImageView sd_setImageWithURL:[NSURL URLWithString:self.liveListModel.live_cover]];
    [self.view addSubview:self.tipView];
    self.tipView.hidden = YES;
    
    self.closeButton = [[UIButton alloc] init];
    self.closeButton.frame = CGRectMake(SCREEN_WIDTH-64, FWCustomeSafeTop +14, 64, 66);
    [self.closeButton setImage:[UIImage imageNamed:@"hot_live_close"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
    
    self.userView = [[UIView alloc] init];
    self.userView.layer.cornerRadius=45/2;
    self.userView.layer.masksToBounds = YES;
    self.userView.backgroundColor = FWColorWihtAlpha(@"000000", 0.4);
    [self.view addSubview:self.userView];
    [self.userView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(self.view).mas_offset(14);
        make.centerY.mas_equalTo(self.closeButton);
        make.width.mas_lessThanOrEqualTo(199);
    }];
    UITapGestureRecognizer * photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoClick)];
    [self.userView addGestureRecognizer:photoTap];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius=39/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.userView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.userView);
        make.size.mas_equalTo(CGSizeMake(39, 39));
        make.left.mas_equalTo(self.userView).mas_offset(3);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 13];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.userView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(4);
        make.width.mas_lessThanOrEqualTo(105);
    }];
    
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 11];
    self.numLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.numLabel.textAlignment = NSTextAlignmentLeft;
    [self.userView addSubview:self.numLabel];
    [self.numLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.height.mas_equalTo(16);
        make.left.mas_equalTo(self.nameLabel);
        make.width.mas_lessThanOrEqualTo(105);
    }];
    
    self.attentionButton = [[UIButton alloc] init];
    self.attentionButton.layer.cornerRadius=2;
    self.attentionButton.layer.masksToBounds = YES;
    self.attentionButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.attentionButton.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.4);
    [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [self.attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.attentionButton addTarget:self action:@selector(attentionButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.userView addSubview:self.attentionButton];
    
    [self.view addSubview:self.bottomView];

    
    CGFloat attentionWH= 39;
    if ([self.liveListModel.user_info.follow_status isEqualToString:@"1"]) {
        // 已关注
        attentionWH = 0.1;
    }
    [self.attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.userView);
        make.size.mas_equalTo(CGSizeMake(attentionWH, attentionWH));
        make.right.mas_equalTo(self.userView).mas_offset(-3);
        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(8);
    }];
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.liveListModel.user_info.header_url]];

    self.nameLabel.text = self.liveListModel.user_info.nickname;
}

#pragma mark - > 关闭当前页面
- (void)closeButtonClick{
    
    [self quitChatRoom];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
        [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] setIsLogin:NO];
    });
}

#pragma mark - > 进入个人页
- (void)photoClick{
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.liveListModel.user_info.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 关注主播
- (void)attentionButtonOnClick{
    
    NSString * action = Submit_follow_users;
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":self.liveListModel.user_info.uid,
                              };
    
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([action isEqualToString: Submit_follow_users]) {
                
                [self.attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.mas_equalTo(self.userView);
                    make.size.mas_equalTo(CGSizeMake(0.1, 0.1));
                    make.right.mas_equalTo(self.userView).mas_offset(-3);
                    make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(8);
                }];
                
                [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 用户单机屏幕，收键盘
- (void)singleTap{
    [self.view endEditing:YES];
}

- (void)playClick:(UIButton *)sender {
    [self.player playTheIndex:0];
    [self.controlView showTitle:@"" coverURLString:self.liveListModel.live_cover fullScreenMode:ZFFullScreenModeAutomatic];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (FWHotLivePlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [FWHotLivePlayerControlView new];
        _controlView.delegate = self;
        _controlView.prepareShowLoading = YES;
        _controlView.fullScreenOnly = NO;
    }
    return _controlView;
}

- (UIImageView *)containerView {
    if (!_containerView) {
        _containerView = [UIImageView new];
        [_containerView setImageWithURLString:self.liveListModel.live_cover placeholder:[ZFUtilities imageWithColor:[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1] size:CGSizeMake(1, 1)]];
    }
    return _containerView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setImage:[UIImage imageNamed:@"new_allPlay_44x44_"] forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(playClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playBtn;
}

#pragma mark - > ------------------------------ 聊天模块 ------------------------------

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self rcinit];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self rcinit];
    }
    return self;
}

- (void)rcinit {
    self.conversationDataRepository = [[NSMutableArray alloc] init];
    self.conversationMessageCollectionView = nil;
    [self registerNotification];
    self.defaultHistoryMessageCountOfChatRoom = 0;
}

#pragma mark - > 分享当前直播
- (void)shareMatchPost{
    
    FWHotLiveShareViewController * vc = [[FWHotLiveShareViewController alloc] init];
    vc.listModel = self.liveListModel;
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)requestRongYunTokenChatroom{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_chatroom_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            
                                            [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo = userInfo;
                                            
                                            [[RCIM sharedRCIM]refreshUserInfoCache:userInfo withUserId:userId];
                                            [self joinChatRoom];
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            /* token过期或者不正确。
                                             * 如果设置了token有效期并且token过期，请重新请求您的服务器获取新的token
                                             * 如果没有设置token有效期却提示token错误，请检查您客户端和服务器的appkey是否匹配，还有检查您获取token的流程。*/
                                            NSLog(@"token错误");
                                        }];
        }
    }];
}

#pragma mark - > 加入聊天室
- (void)joinChatRoom{
    
    //聊天室类型进入时需要调用加入聊天室接口，退出时需要调用退出聊天室接口
    DHWeakSelf;
    [[RCIMClient sharedRCIMClient]
     joinChatRoom:self.targetId
     messageCount:-1
     success:^{
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"] &&
                 [[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
                 
                 RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                 
                 [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo = userInfo;
                 
                 RCChatroomWelcome *message = [[RCChatroomWelcome alloc] init];
                 message.id = userInfo.userId;
                 message.extra = userInfo.name;
                 message.senderUserInfo = userInfo;
                 [weakSelf sendMessage:message pushContent:nil success:nil error:nil];
             }
         });
         weakSelf.currentID = [GFStaticData getObjectForKey:kTagUserKeyID];
     }
     error:^(RCErrorCode status) {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (status != KICKED_FROM_CHATROOM) {
                 // 提示错误信息
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [weakSelf joinChatRoom];
                 });
             }
         });
     }];
}

#pragma mark - > 退出聊天室
- (void)quitChatRoom{
    
    if (self.conversationType == ConversationType_CHATROOM) {
        
        [[RCIMClient sharedRCIMClient] quitChatRoom:self.targetId
                                            success:^{
                                                NSLog(@"退出聊天室~");
                                            } error:^(RCErrorCode status) {
                                                NSLog(@"退出失败~");
                                            }];
    }
}


#pragma mark - > 输入框跟随键盘动
- (void)updateTextViewFrame {
    CGFloat textViewHeight = self.bottomView.keyboardHeight > self.bottomView.commentViewBgHeight ? self.bottomView.textHeight + 2*TOP_BOTTOM_INSET-9 : ceilf(self.bottomView.textView.font.lineHeight) + 2*TOP_BOTTOM_INSET-9;
    self.bottomView.textView.layer.cornerRadius = 2;
    self.bottomView.textView.frame = CGRectMake(15, 12, CGRectGetMinX(self.bottomView.shareButton.frame)-30, textViewHeight);
    self.bottomView.container.frame = CGRectMake(0, 0, SCREEN_WIDTH, textViewHeight + self.bottomView.keyboardHeight);
    self.bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-textViewHeight - self.bottomView.keyboardHeight, SCREEN_WIDTH, textViewHeight + self.bottomView.keyboardHeight);
}

//keyboard notification
- (void)keyboardWillShow:(NSNotification *)notification {
    self.bottomView.keyboardHeight = [notification keyBoardHeight]+25;

    [self updateTextViewFrame];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.bottomView.keyboardHeight = self.bottomView.commentViewBgHeight;

    [self updateTextViewFrame];
}

- (void)HidekeyboardHide:(NSNotification *)notification{
    
    [self.view endEditing:YES];
}

//textView delegate
-(void)textViewDidChange:(UITextView *)textView {
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:textView.attributedText];
    
    if(!textView.hasText) {
        self.bottomView.textHeight = ceilf(self.bottomView.textView.font.lineHeight);
    }else {
        self.bottomView.textHeight = [attributedText multiLineSize:SCREEN_WIDTH - LEFT_INSET - RIGHT_INSET].height;
    }
    [self updateTextViewFrame];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@""]) {
        return YES;
    }
    
    NSString * tempString = [textView.text stringByAppendingString:text];
    if (tempString.length >=50) {
        tempString = [tempString substringToIndex:50];
        
        self.bottomView.textView.text = tempString;
        return NO;
    }
    
    
    if([text isEqualToString:@"\n"]) {
        [self sendMessage];
    }
    return YES;
}

#pragma mark - > 发送消息
- (void)sendMessage{
    
    [self onSendText:self.bottomView.textView.text];
    self.bottomView.textView.text = @"";
    self.bottomView.textHeight = ceilf(self.bottomView.textView.font.lineHeight);
    [self.bottomView.textView resignFirstResponder];
}

#pragma mark - > 发送消息
- (void)onSendText:(NSString *)string{
    
    [self cheackLogin];
    
    if ([[string stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0) {
        
        [[FWHudManager sharedManager] showErrorMessage:@"写点什么吧~" toController:self];
        return;
    }
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]) {
        RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:string];
        rcTextMessage.senderUserInfo = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo;
        [self sendMessage:rcTextMessage pushContent:nil success:nil error:nil];
    }
}

#pragma mark sendMessage/showMessage
/**
 发送消息
 
 @param messageContent 消息
 @param pushContent pushContent
 */
- (void)sendMessage:(RCMessageContent *)messageContent
        pushContent:(NSString *)pushContent
            success:(void (^)(long messageId))successBlock
              error:(void (^)(RCErrorCode nErrorCode, long messageId))errorBlock {
    if (_targetId == nil) {
        return;
    }
    messageContent.senderUserInfo = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo;
    if (messageContent == nil) {
        return;
    }
    
    __weak typeof(&*self) __weakself = self;
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] sendMessage:self.conversationType targetId:self.targetId content:messageContent pushContent:pushContent pushData:nil success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            RCMessage *message = [[RCMessage alloc] initWithType:__weakself.conversationType
                                                        targetId:__weakself.targetId
                                                       direction:MessageDirection_SEND
                                                       messageId:messageId
                                                         content:messageContent];
            //  过滤礼物消息，弹幕消息,退出聊天室消息不插入数据源中；
            if ([messageContent isMemberOfClass:[RCChatroomGift class]] || [messageContent isMemberOfClass:[RCChatroomBarrage class]] || [messageContent isMemberOfClass:[RCChatroomUserQuit class]]) {
            } else {
                message.senderUserId = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId;
                
                [__weakself.bottomView.textView endEditing:YES];
                [__weakself.bottomView.textView setText:@""];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:RongYunRecieveNofi object:nil userInfo:@{@"rcMessage":message}];
            }
        });
        if (successBlock) {
            successBlock(messageId);
        }
    } error:^(RCErrorCode nErrorCode, long messageId) {
        [__weakself.bottomView.textView setText:@""];
        NSLog(@"发送失败，errorcode is: %ld",(long)nErrorCode);
        if (errorBlock) {
            errorBlock(nErrorCode, messageId);
        }
    }];
    
}

/**
 *  接收到消息的回调
 */
- (void)didReceiveMessageNotification:(NSNotification *)notification {
    __block RCMessage *rcMessage = notification.object;
    RCCRMessageModel *model = [[RCCRMessageModel alloc] initWithMessage:rcMessage];
    
    if (model.conversationType == self.conversationType &&
        [model.targetId isEqual:self.targetId]) {
        __weak typeof(&*self) __blockSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            //  对礼物消息,赞消息进行拦截，展示动画，不插入到数据源中,对封禁消息，弹出alert
            if (rcMessage) {
                if ([rcMessage.content isMemberOfClass:[RCChatroomWelcome class]]) {
                    //  过滤自己发送的欢迎消息
                    if ([rcMessage.senderUserId isEqualToString:[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId]) {
                        return;
                    }
                }
                NSDictionary *leftDic = notification.userInfo;
                if (leftDic && [leftDic[@"left"] isEqual:@(0)]) {
                    __blockSelf.isNeedScrollToButtom = YES;
                }
                [__blockSelf appendAndDisplayMessage:rcMessage];
                UIMenuController *menu = [UIMenuController sharedMenuController];
                menu.menuVisible=NO;
                //如果消息不在最底部，收到消息之后不滚动到底部，加到列表中只记录未读数
                if (![__blockSelf isAtTheBottomOfTableView]) {
                    __blockSelf.unreadNewMsgCount ++ ;
                    [__blockSelf updateUnreadMsgCountLabel];
                }
            }
        });
    }
}

/**
 *  注册监听Notification
 */
- (void)registerNotification {
    //注册接收消息
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didReceiveMessageNotification:)
     name:RCCRKitDispatchMessageNotification
     object:nil];
}

#pragma mark <UIScrollViewDelegate,UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    return self.conversationDataRepository.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RCCRMessageModel *model =
    [self.conversationDataRepository objectAtIndex:indexPath.row];
    RCMessageContent *messageContent = model.content;
    RCCRMessageBaseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ConversationMessageCollectionViewCell forIndexPath:indexPath];;
    if ([messageContent isMemberOfClass:[RCTextMessage class]] ||
        [messageContent isMemberOfClass:[RCChatroomWelcome class]] ||
        [messageContent isMemberOfClass:[RCChatroomFollow class]] ||
        [messageContent isMemberOfClass:[RCChatroomLike class]] ||
        [messageContent isMemberOfClass:[RCChatroomStart class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserBan class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserUnBan class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserBlock class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserUnBlock class]] ||
        [messageContent isMemberOfClass:[RCChatroomNotification class]] ||
        [messageContent isMemberOfClass:[RCChatroomEnd class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserQuit class]]){
        FWHotLiveCollectionCell *__cell = nil;
        NSString *indentifier = nil;
        if ([messageContent isMemberOfClass:[RCChatroomStart class]] ||
            [messageContent isMemberOfClass:[RCChatroomEnd class]]) {
            indentifier = startAndEndCellIndentifier;
        } else {
            indentifier = textCellIndentifier;
        }
        __cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
        [__cell setDataModel:model];
        
        cell = __cell;
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RCCRMessageModel *model = self.conversationDataRepository[indexPath.row];
    if ([model.content isKindOfClass:[RCChatroomStart class]] || [model.content isKindOfClass:[RCChatroomEnd class]]) {
        return CGSizeMake(SCREEN_WIDTH-20,30);
    }else{
        if ([model.content isMemberOfClass:[RCTextMessage class]] &&
            model.senderUserId ) {
            RCTextMessage *textMessage = (RCTextMessage *)model.content;
            
            NSString *userName = textMessage.senderUserInfo.name;
            NSString *str =[NSString stringWithFormat:@"%@:   %@",userName,textMessage.content];
            
            NSMutableAttributedString * messageStringAttr = [[NSMutableAttributedString alloc] initWithString:str];
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.paragraphSpacing = 10;// 字体的行间距
            
            messageStringAttr.yy_font = DHSystemFontOfSize_14;
            messageStringAttr.yy_paragraphStyle = paragraphStyle;
            
            CGSize  textLimitSize = CGSizeMake(SCREEN_WIDTH - 40, MAXFLOAT);
            CGFloat textHeight = [YYTextLayout layoutWithContainerSize:textLimitSize text:messageStringAttr].textBoundingSize.height;
            
            return CGSizeMake(SCREEN_WIDTH-20,textHeight+10);
        }
        return CGSizeMake(SCREEN_WIDTH-20,30);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HideKeyboard object:nil];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.f;
}

/**
 *  消息滚动到底部
 *
 *  @param animated 是否开启动画效果
 */
- (void)scrollToBottomAnimated:(BOOL)animated {
    if ([self.conversationMessageCollectionView numberOfSections] == 0) {
        return;
    }
    NSUInteger finalRow = MAX(0, [self.conversationMessageCollectionView numberOfItemsInSection:0] - 1);
    if (0 == finalRow) {
        return;
    }
    NSIndexPath *finalIndexPath =
    [NSIndexPath indexPathForItem:finalRow inSection:0];
    [self.conversationMessageCollectionView scrollToItemAtIndexPath:finalIndexPath
                                                   atScrollPosition:UICollectionViewScrollPositionTop
                                                           animated:animated];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 是否显示右下未读icon
    if (self.unreadNewMsgCount != 0) {
        [self checkVisiableCell];
    }
}

//  发送消息
- (void)onTouchSendButton:(NSString *)text {
    //判断是否禁言
    if ([RCCRManager sharedRCCRManager].isBan) {
        [self insertNotificationMessage:banNotifyContent];
    } else {
        [self touristSendMessage:text];
    }
}

- (void)touristSendMessage:(NSString *)text {
    
    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:text];
    rcTextMessage.senderUserInfo = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo;
    [self sendMessage:rcTextMessage pushContent:nil success:nil error:nil];
}

/**
 *  将消息加入本地数组
 */
- (void)appendAndDisplayMessage:(RCMessage *)rcMessage {
    if (!rcMessage) {
        return;
    }
    RCCRMessageModel *model = [[RCCRMessageModel alloc] initWithMessage:rcMessage];
    //    model.userInfo = [[RCCRManager sharedRCCRManager]getUserInfo:rcMessage.senderUserId];
    if ([self appendMessageModel:model]) {
        NSIndexPath *indexPath =
        [NSIndexPath indexPathForItem:self.conversationDataRepository.count - 1
                            inSection:0];
        if ([self.conversationMessageCollectionView numberOfItemsInSection:0] !=
            self.conversationDataRepository.count - 1) {
            return;
        }
        //  view刷新
        [self.conversationMessageCollectionView
         insertItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        if ([self isAtTheBottomOfTableView] || self.isNeedScrollToButtom) {
            [self scrollToBottomAnimated:YES];
            self.isNeedScrollToButtom=NO;
        }
    }
    return;
}

- (void)appendAndShowMessage:(NSNotification *)noti {
    
    NSDictionary * dict  = (NSDictionary *)noti.userInfo;
    
    RCMessage * rcMessage = [dict objectForKey:@"rcMessage"];
    
    if (!rcMessage) {
        return;
    }
    RCCRMessageModel *model = [[RCCRMessageModel alloc] initWithMessage:rcMessage];
    //    model.userInfo = [[RCCRManager sharedRCCRManager]getUserInfo:rcMessage.senderUserId];
    if ([self appendMessageModel:model]) {
        NSIndexPath *indexPath =
        [NSIndexPath indexPathForItem:self.conversationDataRepository.count - 1
                            inSection:0];
        if ([self.conversationMessageCollectionView numberOfItemsInSection:0] !=
            self.conversationDataRepository.count - 1) {
            return;
        }
        //  view刷新
        [self.conversationMessageCollectionView
         insertItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        if ([self isAtTheBottomOfTableView] || self.isNeedScrollToButtom) {
            [self scrollToBottomAnimated:YES];
            self.isNeedScrollToButtom=NO;
        }
    }
    return;
}

#pragma mark - > 每次切换聊天室时删除聊天记录
- (void)deleteMessage:(NSNotification *)noti {
    
    if (self.conversationDataRepository.count > 0) {
        [self.conversationDataRepository removeAllObjects];
        [self.conversationMessageCollectionView reloadData];
    }
}

/**
 *  判断消息是否在collectionView的底部
 *
 *  @return 是否在底部
 */
- (BOOL)isAtTheBottomOfTableView {
    if (self.conversationMessageCollectionView.contentSize.height <= self.conversationMessageCollectionView.frame.size.height) {
        return YES;
    }
    if(self.conversationMessageCollectionView.contentOffset.y +200 >= (self.conversationMessageCollectionView.contentSize.height - self.conversationMessageCollectionView.frame.size.height)) {
        return YES;
    }else{
        return NO;
    }
}

/**
 *  更新底部新消息提示显示状态
 */
- (void)updateUnreadMsgCountLabel{
    if (self.unreadNewMsgCount == 0) {
        //        self.unreadButtonView.hidden = YES;
    }
    else{
        //        self.unreadButtonView.hidden = NO;
        //        self.unReadNewMessageLabel.text = @"底部有新消息";
    }
}

/**
 *  检查是否更新新消息提醒
 */
- (void) checkVisiableCell{
    NSIndexPath *lastPath = [self getLastIndexPathForVisibleItems];
    if (lastPath.row >= self.conversationDataRepository.count - self.unreadNewMsgCount || lastPath == nil || [self isAtTheBottomOfTableView] ) {
        self.unreadNewMsgCount = 0;
        [self updateUnreadMsgCountLabel];
    }
}

/**
 *  获取显示的最后一条消息的indexPath
 *
 *  @return indexPath
 */
- (NSIndexPath *)getLastIndexPathForVisibleItems
{
    NSArray *visiblePaths = [self.conversationMessageCollectionView indexPathsForVisibleItems];
    if (visiblePaths.count == 0) {
        return nil;
    }else if(visiblePaths.count == 1) {
        return (NSIndexPath *)[visiblePaths firstObject];
    }
    NSArray *sortedIndexPaths = [visiblePaths sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSIndexPath *path1 = (NSIndexPath *)obj1;
        NSIndexPath *path2 = (NSIndexPath *)obj2;
        return [path1 compare:path2];
    }];
    return (NSIndexPath *)[sortedIndexPaths lastObject];
}

/**
 *  如果当前会话没有这个消息id，把消息加入本地数组
 */
- (BOOL)appendMessageModel:(RCCRMessageModel *)model {
    
    if (!model.content) {
        return NO;
    }
    //这里可以根据消息类型来决定是否显示，如果不希望显示直接return NO
    
    //数量不可能无限制的大，这里限制收到消息过多时，就对显示消息数量进行限制。
    //用户可以手动下拉更多消息，查看更多历史消息。
    if (self.conversationDataRepository.count>100) {
        //                NSRange range = NSMakeRange(0, 1);
        RCCRMessageModel *message = self.conversationDataRepository[0];
        [[RCIMClient sharedRCIMClient]deleteMessages:@[@(message.messageId)]];
        [self.conversationDataRepository removeObjectAtIndex:0];
        [self.conversationMessageCollectionView reloadData];
    }
    
    [self.conversationDataRepository addObject:model];
    return YES;
}

/**
 发言按钮事件
 */
- (void)commentBtnPressed:(id)sender {
    //  判断是否登录了
    if ([[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] isLogin]) {
        //判断是否禁言
        if ([RCCRManager sharedRCCRManager].isBan) {
            [self insertNotificationMessage:banNotifyContent];
        } else {
            [_inputBar setHidden:NO];
            [_inputBar setInputBarStatus:RCCRBottomBarStatusKeyboard];
        }
    } else {
        CGRect frame = self.loginView.frame;
        frame.origin.y -= frame.size.height;
        [self.loginView setHidden:NO];
        __weak __typeof(&*self)weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            [weakSelf.loginView setFrame:frame];
        } completion:nil];
    }
}

#pragma mark - RCCRLoginViewDelegate
- (void)clickLoginBtn:(UIButton *)loginBtn {
    // 登录成功
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] setIsLogin:YES];
    RCChatroomWelcome *joinChatroomMessage = [[RCChatroomWelcome alloc]init];
    [joinChatroomMessage setId:[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId];
    [self sendMessage:joinChatroomMessage pushContent:nil success:nil error:nil];
    //    [self setDefaultBottomViewStatus];
}


#pragma mark - ban and block notice

- (void)presentAlert:(NSString *)content {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:content message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)insertNotificationMessage:(NSString *)content {
    RCChatroomNotification *notify = [RCChatroomNotification new];
    notify.content = content;
    RCMessage *message = [[RCMessage alloc] initWithType:self.conversationType
                                                targetId:self.targetId
                                               direction:MessageDirection_SEND
                                               messageId:-1
                                                 content:notify];
    message.senderUserId = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId;
    [self appendAndDisplayMessage:message];
}

#pragma mark - views init
/**
 *  注册cell
 *
 *  @param cellClass  cell类型
 *  @param identifier cell标示
 */
- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier {
    [self.conversationMessageCollectionView registerClass:cellClass
                               forCellWithReuseIdentifier:identifier];
}

- (void)initializedSubViews {
    
    CGFloat height = SCREEN_HEIGHT-170-59-FWSafeBottom;
    CGSize size = CGSizeMake(SCREEN_WIDTH, height);
    
    
    //  消息展示界面和输入框
    [self.view addSubview:self.messageContentView];
    [_messageContentView setFrame:CGRectMake(0, height, size.width-60, 170)];
    
    [_messageContentView addSubview:self.conversationMessageCollectionView];
    [_conversationMessageCollectionView setFrame:CGRectMake(0, 0, SCREEN_WIDTH,CGRectGetHeight(self.messageContentView.frame))];
    
    UICollectionViewFlowLayout *customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    customFlowLayout.minimumLineSpacing = 2;
    customFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0.0f,0.f, 0.0f);
    customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [_conversationMessageCollectionView setCollectionViewLayout:customFlowLayout animated:NO completion:nil];
    
    [self registerClass:[FWHotLiveCollectionCell class]forCellWithReuseIdentifier:textCellIndentifier];
    [self registerClass:[FWHotLiveCollectionCell class]forCellWithReuseIdentifier:startAndEndCellIndentifier];
}

- (UIView *)messageContentView {
    if (!_messageContentView) {
        _messageContentView = [[UIView alloc] init];
        [_messageContentView setBackgroundColor: [UIColor clearColor]];
    }
    return _messageContentView;
}

- (UICollectionView *)conversationMessageCollectionView {
    if (!_conversationMessageCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        _conversationMessageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [_conversationMessageCollectionView setDelegate:self];
        [_conversationMessageCollectionView setDataSource:self];
        [_conversationMessageCollectionView setBackgroundColor: [UIColor clearColor]];
        [_conversationMessageCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:ConversationMessageCollectionViewCell];
    }
    return _conversationMessageCollectionView;
}

- (FWHotLiveBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[FWHotLiveBottomView alloc] init];
        _bottomView.textView.delegate = self;
        _bottomView.delegate = self;
    }
    return _bottomView;
}


#pragma mark - > 检测是否登录
- (void)cheackLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        /* 登录 */
        [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        
        return;
    }
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.player.currentPlayerManager pause];
    [self.playerManager pause];
    
    [self.view endEditing:YES];
}

#pragma mark - > 监听当前网络状态
- (void)monitorLiveNet{
    
    [self checkWifi];
}


- (void)checkWifi{
    
    if ([[GFStaticData getObjectForKey:Current_Network] isEqualToString:@"WIFI"]) {
        
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            [self requestLiveInfo];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }else{
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            if (![[GFStaticData getObjectForKey:Allow_4G_Play] boolValue]) {
                [self requestLiveInfo];

                self.tipView.hidden = NO;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.player.currentPlayerManager pause];
                    [self.playerManager pause];
                });
            }else{
                self.tipView.hidden = YES;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }
}

#pragma mark - > 4G下 点击继续观看
- (void)continueLiveMatch{
    
    self.tipView.hidden = YES;
    
    self.lastNetWork = [GFStaticData getObjectForKey:Last_Network];
    
    if ([self.lastNetWork isEqualToString:@"WIFI"] && ![self.lastNetWork isEqualToString:[GFStaticData getObjectForKey:Current_Network]]) {
        // 由wifi切换到4G ，继续播放
        [self.player.currentPlayerManager play];
    }else{
        // 进来直接是4G
        [self requestLiveInfo];
    }
    
    [GFStaticData saveObject:@"WIFI" forKey:Current_Network];
    [GFStaticData saveObject:@"1" forKey:Allow_4G_Play];
}
@end
