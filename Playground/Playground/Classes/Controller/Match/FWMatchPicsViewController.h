//
//  FWMatchPicsViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 赛事图集
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchPicsViewController : FWBaseViewController<UITextViewDelegate>

@property (nonatomic, strong) NSString * tuiji_id;
@property (nonatomic, strong) NSString * nav_id;
@property (nonatomic, strong) FWCollectionView * picsCollectionView;

@end

NS_ASSUME_NONNULL_END
