//
//  FWNewShareScoreViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/12.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 *
 */
#import "FWBaseViewController.h"
#import "FWScoreModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWNewShareScoreViewController : FWBaseViewController<UIGestureRecognizerDelegate>

/* 1: 单人  2: 双人  */
@property (nonatomic, strong) NSString * shareType;

@property (nonatomic, strong) FWScoreListModel * listModel;
@property (nonatomic, strong) FWScoreModel * scoreModel;

@end

NS_ASSUME_NONNULL_END
