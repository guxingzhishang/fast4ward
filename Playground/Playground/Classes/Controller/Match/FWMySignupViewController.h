//
//  FWMySignupViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWPlayerSignupModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMySignupViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
