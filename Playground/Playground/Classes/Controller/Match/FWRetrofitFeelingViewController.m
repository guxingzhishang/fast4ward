//
//  FWRetrofitFeelingViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/13.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRetrofitFeelingViewController.h"
#import "FWRetrofitSegementScrollView.h"
#import "FWRetrofitFeelingCell.h"
#import "FWRefrofitFeelingModel.h"

@interface FWRetrofitFeelingViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,FWRetrofitSegementScrollViewDelegate>
@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) FWRetrofitSegementScrollView * segementScrollView;
@property (nonatomic, strong) NSArray * iconArray;
@property (nonatomic, strong) NSArray * titleArray;
@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWRefrofitFeelingModel * feelingModel;

@property (nonatomic, strong) NSString * currentCatID;
@end

@implementation FWRetrofitFeelingViewController
@synthesize infoTableView;
@synthesize segementScrollView;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)requestCategoryData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"cat_id":self.currentCatID,
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_category_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            self.feelingModel = [FWRefrofitFeelingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (self.segementScrollView.titles.count <= 0) {
                [self.segementScrollView deliverTitles:self.feelingModel.list_category];
            }
            
            if(self.feelingModel.list_tag.count == 0 &&
               self.pageNum > 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                NSMutableArray * hotTags = @[].mutableCopy;
                NSMutableArray * allTags = @[].mutableCopy;
                
                if(self.dataSource.count > 0){
                    if (self.feelingModel.list_hot_tag.count > 0) {
                        
                        if ([(NSMutableArray *)self.dataSource[0] count] > 0) {
                            hotTags = self.dataSource[0];
                        }
                        [hotTags addObjectsFromArray:self.feelingModel.list_hot_tag];
                    }
                    
                    if (self.feelingModel.list_tag.count > 0) {
                        
                        if ([(NSMutableArray *)self.dataSource[1] count] > 0) {
                            hotTags = self.dataSource[0];
                        }
                        [allTags addObjectsFromArray:self.feelingModel.list_tag];
                    }
                }else{
                    
                    [hotTags addObjectsFromArray:self.feelingModel.list_hot_tag];
                    [allTags addObjectsFromArray:self.feelingModel.list_tag];
                }
                
                [self.dataSource addObject:hotTags];
                [self.dataSource addObject:allTags];

                [self.infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"改装灵感页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"改装灵感页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"改装灵感";
    self.currentCatID = @"0";
    
    self.iconArray = @[@"retrofit_hot",@"retrofit_all"];
    self.titleArray = @[@"热门",@"全部"];
    
    [self setupSubViews];
    [self requestCategoryData:NO];
}

- (void)setupSubViews{
    
    UIView * upLineView = [[UIView alloc] init];
    upLineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
    [self.view addSubview:upLineView];
    upLineView.frame = CGRectMake(0, 0, SCREEN_WIDTH,1);
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 54;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.mas_equalTo(self.view);
        make.left.mas_equalTo(97);
        make.top.mas_equalTo(upLineView.mas_bottom);
    }];
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestCategoryData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestCategoryData:YES];
    }];
    
    segementScrollView = [[FWRetrofitSegementScrollView alloc] init];
    segementScrollView.showsVerticalScrollIndicator = NO;
    segementScrollView.itemDelegate = self;
    [self.view addSubview:segementScrollView];
    segementScrollView.frame = CGRectMake(0,CGRectGetMinY(infoTableView.frame)+2 , 97, SCREEN_HEIGHT-64-FWCustomeSafeTop);
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWViewBackgroundColor_E5E5E5;
    [self.view addSubview:lineView];
    lineView.frame = CGRectMake(CGRectGetMaxX(segementScrollView.frame), 0, 1,SCREEN_HEIGHT);
}

#pragma mark - > 点击推荐item
- (void)segementItemClickTap:(NSInteger)index{
    self.currentCatID = self.feelingModel.list_category[index].cat_id;
    [self requestCategoryData:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [(NSMutableArray *)self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"petrofitFeelingID";
    
    FWRetrofitFeelingCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWRetrofitFeelingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if ([(NSMutableArray *)self.dataSource[indexPath.section] count] > 0) {
        
        NSMutableArray * tempArr = self.dataSource[indexPath.section];
        if (tempArr.count > indexPath.row) {
            FWRefrofitFeelingTagListModel * model = [tempArr objectAtIndex:indexPath.row];
            cell.nameLabel.text = model.tag_name;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([(NSMutableArray *)self.dataSource[indexPath.section] count] > 0) {
        
        NSMutableArray * tempArr = self.dataSource[indexPath.section];
        if (tempArr.count > indexPath.row) {
            FWRefrofitFeelingTagListModel * model = [tempArr objectAtIndex:indexPath.row];
            
            FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
            TVC.traceType = 5;
            TVC.tags_name = model.tag_name;
            TVC.tags_id = model.tag_id;
            [self.navigationController pushViewController:TVC animated:YES];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.infoTableView.frame.size.width, 64)];
    headerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 25, 25)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(icon.frame)+12, 0, self.infoTableView.frame.size.width-50, 64)];
    label.textColor = FWTextColor_12101D;
    label.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    label.textAlignment = NSTextAlignmentLeft;
    
    if (section <= self.iconArray.count-1) {
        icon.image = [UIImage imageNamed:self.iconArray[section]];
        label.text = self.titleArray[section];
    }
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    lineView.frame = CGRectMake(0, 63.5, CGRectGetWidth(self.infoTableView.frame)-15, 0.5);
    
    [headerView addSubview:icon];
    [headerView addSubview:label];
    [headerView addSubview:lineView];

    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 64;
}

@end
