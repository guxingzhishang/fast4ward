//
//  FWReportViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/7.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWReportViewController.h"
#import "FWFeedBackRequest.h"

#define CELLHEIGHT 40

@implementation FWReportViewController
@synthesize titleArray;

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"举报页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"举报页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"举报原因";
    self.view.backgroundColor = FWViewBackgroundColor_EEEEEE;

    titleArray = @[@"色情低俗",
                   @"政治敏感",
                   @"违法犯罪",
                   @"发布垃圾广告",
                   @"造谣传播，涉嫌欺诈",
                   @"盗用他人作品",
                   @"疑似自我伤害",
                   @"侮辱谩骂"];
    
    [self setupSubviews];
}

- (void)bindViewModel{
    [super bindViewModel];
}

#pragma mark - > 视图初始化
- (void)setupSubviews{
    
    for (int i = 0; i< titleArray.count; i++) {
        
        FWBaseCellView * cellView = [[FWBaseCellView alloc] initWithTitle:titleArray[i] WithImage:@""];
        cellView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        cellView.iconImageView.hidden = YES;
        cellView.arrowImageView.hidden = YES;
        cellView.tag = 20000+i;
        [cellView addTarget:self action:@selector(cellViewClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:cellView];
        
        [cellView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(i*CELLHEIGHT+20);
            make.left.right.mas_equalTo(self.view);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, CELLHEIGHT));
        }];
        
        [cellView.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cellView).mas_offset(15);
            make.centerY.mas_equalTo(cellView);
            make.height.mas_equalTo(25);
        }];
        
        [cellView.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(cellView).mas_offset(-15);
            make.left.mas_equalTo(cellView).mas_offset(15);
            make.bottom.mas_equalTo(cellView).mas_offset(-0.3);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-30, 0.5));
        }];
    }
}

#pragma mark - > 举报
- (void)cellViewClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 20000;
    
    FWFeedBackRequest * request = [[FWFeedBackRequest alloc] init];
    
    NSDictionary * dict = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                            @"feed_id":@"0",
                            @"content":[NSString stringWithFormat:@"举报_%@",titleArray[index]],
                            @"f_uid":self.f_uid,
                            };
    
    [request startWithParameters:dict WithAction:Submit_feedback  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        if ([code isEqualToString:NetRespondCodeSuccess]){
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"举报成功,平台将会在24小时之内给出回复" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
}

@end
