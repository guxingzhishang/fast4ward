//
//  FW04ListViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 04总榜
 */
#import "FWBaseViewController.h"
#import "FWMyScoreView.h"
#import "FWGroupScoreCell.h"
#import "FWPlayerScoreDetailViewController.h"
#import "FWTotalListSegementView.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^list04Block)(NSString * top_img_url);

@interface FW04ListViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource,FWTotalListSegementViewDelegate>

@property (nonatomic, copy) list04Block myBlock;

@property (nonatomic, strong) NSString * year;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * group_id;

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWMyScoreView * mineView ;

@property (nonatomic, strong) FWPlayerRankModel * rankModel;
@property (nonatomic, assign) NSInteger  pageNum;

@property (nonatomic, strong) FWTotalListSegementView * totalSegement;

@end

NS_ASSUME_NONNULL_END
