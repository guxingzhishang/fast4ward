//
//  FWLiveViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/5.
//  Copyright © 2018 孤星之殇. All rights reserved.
//
/**
 * 直播
 */
#import "FWBaseAutoTextView.h"
#import "WMPageController.h"

//NS_ASSUME_NONNULL_BEGIN

@interface FWLiveViewController :WMPageController <UITextViewDelegate,FWBaseAutoTextViewDelegate>
 
@property (nonatomic, strong) NSURL * playURL;
@property (nonatomic, strong) FWBaseAutoTextView * bottomView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * shareButton;

@property (nonatomic, strong) UIImageView * adImageView;


@property (nonatomic, strong) NSString * roomId;//直播室的id
@property (nonatomic, strong) NSString * sport_id;//直播室的id
@property (nonatomic, strong) FWLiveInfoModel * finalModel;//直播模型
@property (nonatomic, strong) FWF4WMatchLiveListModel * liveListModel;//回放模型
@property (nonatomic, assign) BOOL isLive;/* YES 直播   NO 回看 */

@property (nonatomic, assign) int  currentIndex;

@property (nonatomic, strong) FWLiveADView * adView;


@end

//NS_ASSUME_NONNULL_END
