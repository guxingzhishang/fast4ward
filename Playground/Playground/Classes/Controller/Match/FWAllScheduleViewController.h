//
//  FWAllScheduleViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 赛程
 */
#import "FWBaseViewController.h"
#import "FWAllScheduleModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^AllScheduleBlock)(NSString * top_img_url);

@interface FWAllScheduleViewController : FWBaseViewController<UIScrollViewDelegate>

@property (nonatomic, copy) AllScheduleBlock myBlock;

@end

@interface FWScheduleView : UIView

@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIImageView * nearestImageView;
@property (nonatomic, strong) UIImageView * picImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * carLabel;
@property (nonatomic, strong) UILabel * ETLabel;
@property (nonatomic, strong) FWScheduleListModel * subListModel;
@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, assign) CGFloat  sectionIndex;
@property (nonatomic, assign) CGFloat  rowIndex;

- (void)configForView:(id)model WithSection:(NSInteger)section WithRow:(NSInteger)row;

@end

NS_ASSUME_NONNULL_END
