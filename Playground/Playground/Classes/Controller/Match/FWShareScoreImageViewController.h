//
//  FWShareScoreImageViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/12.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWScoreModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWShareScoreImageViewController : FWBaseViewController<UIGestureRecognizerDelegate>

/* 1: 单人  2: 双人  3: 比赛海报 */
@property (nonatomic, strong) NSString * shareType;

@property (nonatomic, strong) FWScoreListModel * listModel;
@property (nonatomic, strong) FWScoreModel * scoreModel;

@property (nonatomic, strong) UIImage * postImage;

@end

NS_ASSUME_NONNULL_END
