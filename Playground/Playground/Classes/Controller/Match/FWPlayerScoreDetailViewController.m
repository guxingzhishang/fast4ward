//
//  FWPlayerScoreDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerScoreDetailViewController.h"
#import "FWPlayerScoreDetailView.h"
#import "FWPlayerInfoShareViewController.h"

@interface FWPlayerScoreDetailViewController ()
@property (nonatomic, strong) FWPlayerScoreDetailView * detailView;
@property (nonatomic, strong) UIView * navigtionView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) FWScoreDetailModel * model;

@end

@implementation FWPlayerScoreDetailViewController

- (void)requestData{
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if (!self.ScoreModel.car_no ||
        !self.ScoreModel.sport_id ||
        !self.ScoreModel.driver_id ||
        !self.ScoreModel.match_log_id) {
        return;
    }
   
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"car_no":self.ScoreModel.car_no,
                              @"sport_id":self.ScoreModel.sport_id,
                              @"match_log_id":self.ScoreModel.match_log_id,
                              @"driver_id":self.ScoreModel.driver_id,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_rank_score_detail WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.model= [FWScoreDetailModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.detailView configForView:self.model];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"成绩详情"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"成绩详情"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    self.navigtionView = [[UIView alloc] init];
    self.navigtionView.backgroundColor = FWTextColor_252527;
    [self.view addSubview:self.navigtionView];
    [self.navigtionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, FWCustomeSafeTop +64));
        make.top.left.right.mas_equalTo(self.view);
    }];
    
    self.backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    self.backButton.titleLabel.font = DHSystemFontOfSize_16;
    self.backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.navigtionView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(55, 30));
        make.left.mas_equalTo(self.navigtionView).mas_offset(10);
        make.bottom.mas_equalTo(self.navigtionView).mas_offset(-10);
    }];
    
    self.shareButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    [self.shareButton setImage:[UIImage imageNamed:@"new_white_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.navigtionView addSubview:self.shareButton];
    [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(45, 30));
        make.right.mas_equalTo(self.navigtionView).mas_offset(-5);
        make.centerY.mas_equalTo(self.backButton);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"成绩详情";
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigtionView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 30));
        make.centerY.mas_equalTo(self.backButton);
        make.centerX.mas_equalTo(self.navigtionView);
    }];

    self.detailView = [[FWPlayerScoreDetailView alloc] init];
    self.detailView.vc = self;
    self.detailView.delegate = self;
    [self.view addSubview:self.detailView];
    [self.detailView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.navigtionView.mas_bottom);
    }];
    
    [self requestData];
}

- (void)shareBtnClick{
    
    FWPlayerInfoShareViewController * vc = [[FWPlayerInfoShareViewController alloc] init];
    vc.vcType = 1;
    vc.detailModel = self.model;
    vc.share_url = self.model.qrcode_url;
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = self.detailView.contentOffset;
    if (offset.y <= 0) {
        offset.y = 0;
    }
    self.detailView.contentOffset = offset;
}
@end
