//
//  FWMoreListViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMoreListViewController.h"
#import "FWGroupScoreViewController.h"
#import "FWScoreSearchViewController.h"

@implementation FWRankSortListModel
@end

@implementation FWRankSortModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list" : NSStringFromClass([FWRankSortListModel class]),
             };
}
@end

@interface FWMoreListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) UIImageView * headerImageView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * searchButton;
@property (nonatomic, strong) FWRankSortModel * sortModel;
@property (nonatomic, strong) FWPlayerRankModel * categaryRankModel;
@property (nonatomic, strong) NSMutableArray * firstArray;

@end

@implementation FWMoreListViewController
@synthesize infoTableView;

- (NSMutableArray *)dataSource{
    
    if(!_dataSource){
        _dataSource = @[].mutableCopy;
    }
    return _dataSource;
}


- (void)requestLogList:(BOOL)isRequestMoreData{
    
    
    if (self.dataSource.count) {
        [self.dataSource removeAllObjects];
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              };
    
    [request startWithParameters:params WithAction:Get_rank_sort_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [infoTableView.mj_header endRefreshing];
            
            self.sortModel = [FWRankSortModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:self.sortModel.top_img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.headerImageView.clipsToBounds = YES;
            
            [self.dataSource addObjectsFromArray: self.sortModel.list];
            
            NSMutableArray * tempArr = @[].mutableCopy;
            NSMutableArray * titleArr = @[].mutableCopy;
            
            self.categaryRankModel = [[FWPlayerRankModel alloc] init];

            for (FWRankSortListModel * model in self.sortModel.list) {
                
                FWPlayerRankListModel * listModel = [[FWPlayerRankListModel alloc] init];
                listModel.type = model.type;
                listModel.year = model.year;
                listModel.sport_id = @"0";
                listModel.sport_name = model.name;
                [tempArr addObject:listModel];
                [titleArr addObject:model.name];
            }
            
            self.firstArray = [titleArr mutableCopy];
            self.categaryRankModel.category_list = [tempArr mutableCopy];
            
            [infoTableView reloadData];
        }else{
            
            [infoTableView.mj_header endRefreshing];
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"更多榜单页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"更多榜单页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    
    [self setupSubViews];
    [self requestLogList:NO];
}

- (void)setupSubViews{
    
    self.headerImageView = [[UIImageView alloc] init];
    self.headerImageView.clipsToBounds = YES;
    self.headerImageView.userInteractionEnabled = YES;
    self.headerImageView.image = [UIImage imageNamed:@"placeholder"];
    self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.headerImageView];
    [self.headerImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 154*SCREEN_WIDTH/375));
    }];
    
    self.backButton = [[UIButton alloc] init];
    self.backButton.alpha = 0.5;
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.headerImageView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.left.mas_equalTo(self.headerImageView);
        make.top.mas_equalTo(self.headerImageView).mas_offset(FWCustomeSafeTop+20);
    }];
    
    self.searchButton = [[UIButton alloc] init];
    [self.searchButton setImage:[UIImage imageNamed:@"rank_search"] forState:UIControlStateNormal];
    [self.searchButton addTarget:self action:@selector(searchButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.headerImageView addSubview:self.searchButton];
    [self.searchButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.right.mas_equalTo(self.headerImageView);
        make.centerY.mas_equalTo(self.backButton);
    }];
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.estimatedRowHeight = 100;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.headerImageView.mas_bottom);
        make.height.mas_greaterThanOrEqualTo(100);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    infoTableView.tableFooterView = footerView;
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestLogList:NO];
    }];
}

#pragma mark - > 返回
- (void)backButtonOnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 搜索
- (void)searchButtonOnClick{
    
    FWScoreSearchViewController * SVC = [[FWScoreSearchViewController alloc] init];
    [self.navigationController pushViewController:SVC animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"moreListCellID";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.dataSource.count) {
        FWRankSortListModel * model = self.dataSource[indexPath.row];
        
        UIImageView * imageView = [[UIImageView alloc] init];
        imageView.clipsToBounds = YES;
        [imageView sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        imageView.layer.cornerRadius = 5;
        imageView.layer.masksToBounds = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [cell.contentView addSubview:imageView];
        [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cell.contentView).mas_offset(20);
            make.right.mas_equalTo(cell.contentView).mas_offset(-20);
            make.top.mas_equalTo(cell.contentView).mas_offset(15);
            make.bottom.mas_equalTo(cell.contentView).mas_offset(0);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-40, 80*(SCREEN_WIDTH-40)/335));
        }];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.dataSource.count) {
        FWRankSortListModel * model = self.dataSource[indexPath.row];
        
        FWGroupScoreViewController * VDVC = [[FWGroupScoreViewController alloc] init];
        VDVC.sport_id = @"0";
        VDVC.year = model.year;
        VDVC.type = model.type;
        VDVC.rankType = @"list";
        VDVC.categaryRankModel = self.categaryRankModel;
        VDVC.firstArray = self.firstArray;
        VDVC.group_id = @"0";
        VDVC.sport_name = model.name;
        [self.navigationController pushViewController:VDVC animated:YES];
    }
}

@end
