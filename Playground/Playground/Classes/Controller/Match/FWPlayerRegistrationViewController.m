//
//  FWPlayerRegistrationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerRegistrationViewController.h"
#import "FWPlayerRegistrationView.h"
#import "FWPlayerSignupModel.h"
#import "FWPlayerSignupViewController.h"
#import "FWMySignupViewController.h"

@interface FWPlayerRegistrationViewController ()<FWPlayerRegistrationViewDelegate>

@property (nonatomic, strong) FWPlayerRegistrationView * registrationView;
@property (nonatomic, strong) UIView * placeHolderView;

@end

@implementation FWPlayerRegistrationViewController

- (void)requestBaomingInfo{
    
    NSDate * startDare = [NSDate date];

    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"baoming_id":self.baoming_id,
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_baoming_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        double durationDate = [[NSDate date] timeIntervalSinceDate:startDare];
        NSLog(@"耗时：%f 秒",durationDate);
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWBaomingInfoModel * listModel = [FWBaomingInfoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            self.registrationView.infoModel = listModel;
            
////            /* 1.6.2改  进来是否要判断剩余数 */
//            if ([listModel.baoming_num_remain integerValue] > 0) {
                self.placeHolderView.hidden = YES;
                self.registrationView.hidden = NO;
//            }else{
//                self.placeHolderView.hidden = NO;
//                self.registrationView.hidden = YES;
//            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手报名页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手报名页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"车手报名";
    
    [self setupPlaceHolderView];

    self.registrationView = [[FWPlayerRegistrationView alloc] init];
    self.registrationView.vc = self;
    self.registrationView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.registrationView.registrationDelegate = self;
    self.registrationView.userInteractionEnabled = YES;
    [self.view addSubview:self.registrationView];
    [self.registrationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.placeHolderView.hidden = YES;
    self.registrationView.hidden = YES;

    [self requestBaomingInfo];
}

- (void)setupPlaceHolderView{
    
    self.placeHolderView = [[UIView alloc] init];
    self.placeHolderView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.placeHolderView];
    [self.placeHolderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    UIImageView * imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.image = [UIImage imageNamed:@"player_registration_placeholder"];
    [self.placeHolderView addSubview:imageView];
    [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.placeHolderView);
        make.top.mas_equalTo(self.placeHolderView).mas_offset(20);
        make.width.mas_equalTo(101);
        make.height.mas_equalTo(127);
    }];
    
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"名额已满";
    titleLabel.font = [UIFont fontWithName:@"PingFang SC" size: 19.9];
    titleLabel.textColor = FWColor(@"#030303");
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.placeHolderView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.placeHolderView);
        make.top.mas_equalTo(imageView.mas_bottom).mas_offset(10);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(25);
    }];
    
    UILabel * descLabel = [[UILabel alloc] init];
    descLabel.numberOfLines = 0;
    descLabel.text = @"本场比赛名额已抢光，若有用户超时未支付，名额将重新开放。";
    descLabel.font = [UIFont fontWithName:@"PingFang SC" size: 15];
    descLabel.textColor = FWTextColor_9EA3AB;
    descLabel.textAlignment = NSTextAlignmentCenter;
    [self.placeHolderView addSubview:descLabel];
    [descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.placeHolderView);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(35);
        make.width.mas_equalTo(SCREEN_WIDTH-100);
        make.height.mas_greaterThanOrEqualTo(25);
    }];
}

- (void)commitButtonClick{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"real_name":self.registrationView.realNameTF.text,
                              @"idcard":self.registrationView.cardNumTF.text,
                              @"idcard_type":self.registrationView.type,
                              @"mobile":self.registrationView.phoneTF.text,
                              @"baoming_id":self.baoming_id,
                              @"extra_id":self.registrationView.extra_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_get_baoming WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWPlayerSignupModel * signupModel = [FWPlayerSignupModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if([signupModel.if_pay isEqualToString:@"1"]){
                /* 支付页面 */
                FWPlayerSignupViewController * psvc = [[FWPlayerSignupViewController alloc] init];
                psvc.baoming_user_id = signupModel.baoming_user_id;
                psvc.isShowAlert = YES;
                [self.navigationController pushViewController:psvc animated:YES];
            }else{
                /* 我的报名页面 */
                FWMySignupViewController * MSVC = [[FWMySignupViewController alloc] init];
                [self.navigationController pushViewController:MSVC animated:YES];
            }
        }else{
            
            if ([back[@"errno"] isEqualToString:@"-1001"]) {
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:back[@"errmsg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"了解肆放会员" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
                    VVC.webTitle = @"会员权益";
                    [self.navigationController pushViewController:VVC animated:YES];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                return ;
            }else if ([back[@"errno"] isEqualToString:@"-1002"]) {

                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:back[@"errmsg"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                return ;
            }else if ([back[@"errno"] isEqualToString:@"-1003"]) {
                self.registrationView.hidden = YES;
                self.placeHolderView.hidden = NO;
                return ;
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }
    }];
}
@end
