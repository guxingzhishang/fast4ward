//
//  FWGroupScoreViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 成绩组别
 */
#import "FWBaseViewController.h"
#import "FWPlayerRankModel.h"
#import "FWMyScoreView.h"
#import "FWGroupScoreCell.h"
#import "FWPlayerScoreDetailViewController.h"
#import "FWTotalListSegementView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGroupScoreViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource,FWTotalListSegementViewDelegate>

@property (nonatomic, strong) NSString * year;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * group_id;
@property (nonatomic, strong) NSString * group_name;

@property (nonatomic, strong) NSString * sport_name;

@property (nonatomic, strong) NSString * rankType;// allSchedule ：全部赛程    list 榜单

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWMyScoreView * mineView ;

@property (nonatomic, strong) FWPlayerRankModel * rankModel;
@property (nonatomic, strong) FWPlayerRankModel * categaryRankModel;
@property (nonatomic, strong) NSMutableArray * firstArray;
@property (nonatomic, assign) NSInteger  pageNum;

@property (nonatomic, strong) FWTotalListSegementView * totalSegement;

@end

NS_ASSUME_NONNULL_END
