//
//  FWGroupScoreViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGroupScoreViewController.h"
#import "FWGroupScoreCell.h"
#import "FWPlayerScoreDetailViewController.h"
#import "FWGroupScoreView.h"
#import "FWScoreSearchViewController.h"
#import "UIBarButtonItem+Item.h"

@interface FWGroupScoreViewController ()<UITableViewDelegate,UITableViewDataSource,UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, assign) NSInteger  lastIndex;// 最后一次点击的segement
@property (nonatomic, strong) FWPlayerRankModel * scoreModel;
@property (nonatomic, strong) NSMutableArray * groupArray;

@property (nonatomic, strong) IQTextView * showTextView;
@property (nonatomic, strong) UIButton * chooseButton;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UIPickerView * pickerView;

@end

@implementation FWGroupScoreViewController
@synthesize infoTableView;
@synthesize mineView;
@synthesize totalSegement;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)requestGroupData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    [request startWithParameters:params WithAction:Get_sport_rank_category WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.categaryRankModel = [FWPlayerRankModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            NSMutableArray * firstTemp = @[].mutableCopy;
            
            for (FWPlayerRankListModel * firstModel in self.categaryRankModel.category_list) {
                if (![firstTemp containsObject:firstModel.sport_name]) {
                    [firstTemp addObject:firstModel.sport_name];
                }
            }
            self.firstArray = [firstTemp mutableCopy];
            
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求分组
- (void)requestTeam{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"year":self.year,
                              @"sport_id":self.sport_id,
                              @"type":self.type,
                              @"group_id":self.group_id,
                              @"group_name":self.group_name,
                              @"page_size":@"50",
                              @"page":@"1",
                              };
    
    [request startWithParameters:params WithAction:Get_sport_rank_group WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.scoreModel = [FWPlayerRankModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            NSMutableArray * titleArr = @[].mutableCopy;
            
            if (self.groupArray.count > 0) {
                [self.groupArray removeAllObjects];
            }
            
            for (FWGroupListModel * listModel  in self.scoreModel.group_list) {
                [self.groupArray addObject:listModel.group_info];
                [titleArr addObject:listModel.group_info.group_name];
            }
            [self.totalSegement deliverTitles:titleArr];
            self.totalSegement.lastIndex = self.lastIndex;
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 根据筛选获取数据
- (void)requestData:(BOOL)isLoadMoreData{
    
    if (isLoadMoreData) {
        self.pageNum += 1;
    }else{
        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
        self.pageNum = 1;
    }
    
    if ([self.group_id isEqualToString:@"0"]) {
        self.group_name = @"全部组别";
        self.group_id = @"cfcd208495d565ef66e7dff9f98764da";
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"year":self.year,
                              @"sport_id":self.sport_id,
                              @"type":self.type,
                              @"group_id":self.group_id,
                              @"group_name":self.group_name,
                              @"page_size":@"50",
                              @"page":@(self.pageNum).stringValue,
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_sport_rank_group WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.rankModel = [FWPlayerRankModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            NSArray * scoreList = self.rankModel.group_list[0].score_list;
            [self.dataSource addObjectsFromArray:scoreList];
            
            if (isLoadMoreData) {
                [self.infoTableView.mj_footer endRefreshing];
            }else{
                [self.infoTableView.mj_header endRefreshing];
            }
            
            if (self.rankModel.driver_rank_item) {
                
                self.mineView.hidden = NO;
                [self.mineView configViewForModel:self.rankModel.driver_rank_item];
                
                [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(self.view);
                    make.top.mas_equalTo(self.totalSegement.mas_bottom);
                    make.bottom.mas_equalTo(-77);
                }];
                [self.mineView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.left.right.mas_equalTo(self.view);
                    make.size.mas_equalTo(CGSizeMake(ScreenWidth, 77));
                    make.top.mas_equalTo(self.infoTableView.mas_bottom);
                }];
                
                self.mineView.numberLabel.textAlignment = NSTextAlignmentRight;
                [self.mineView.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(self.mineView).mas_offset(0);
                    make.centerY.mas_equalTo(self.mineView);
                    make.height.mas_equalTo(40);
                    make.width.mas_equalTo(40);
                }];
                
            }else{
                self.mineView.hidden = YES;
                [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(self.view);
                    make.top.mas_equalTo(self.totalSegement.mas_bottom);
                    make.bottom.mas_equalTo(self.view);
                }];
            }
            
            if([scoreList count] == 0 &&
               self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [self.infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"02总榜页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"02总榜页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 禁止屏幕点击
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5* NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 20s后允许点击，防止网络不好，一直定屏
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    });
    
    self.title = @"车手排名";
    
    if ([self.rankType isEqualToString:@"list"]) {
       /* 榜单 */
    }else{
       
       /* 全部赛程 */
       self.firstArray = @[].mutableCopy;
       [self requestGroupData];
    }
    
    self.pageNum = 1;
    self.groupArray = @[].mutableCopy;
    
    self.year = self.year;
    self.type = self.type;
    self.sport_id = self.sport_id;
    self.group_id = self.group_id;
    
    [self setupNaviView];
    [self setupSubViews];
    
    self.nameLabel.text = self.sport_name;

    [self requestTeam];
    [self requestData:NO];
}

- (void)setupNaviView{
    
    UIView * topView = [[UIView alloc] init];
    topView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:topView];
    topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 45);
    
    self.chooseButton = [[UIButton alloc] init];
    self.chooseButton.frame = CGRectMake(SCREEN_WIDTH - 70, 0, 70, 45);
    self.chooseButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.chooseButton setTitle:@"筛选" forState:UIControlStateNormal];
    [self.chooseButton setTitleColor:FWTextColor_66739A forState:UIControlStateNormal];
    [self.chooseButton setImage:[UIImage imageNamed:@"sport_choose"] forState:UIControlStateNormal];
    [self.chooseButton addTarget:self action:@selector(chooseButtonClick) forControlEvents:UIControlEventTouchUpInside];
    CGFloat leftlab = [@"筛选" sizeWithAttributes:@{NSFontAttributeName : self.chooseButton.titleLabel.font}].width;
    [self.chooseButton setImageEdgeInsets:UIEdgeInsetsMake(0, leftlab +10, 0, 0)];
    [self.chooseButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
    [topView addSubview:self.chooseButton];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.nameLabel.textColor = FWTextColor_272727;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [topView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView);
        make.left.mas_equalTo(topView).mas_offset(14);
        make.height.mas_equalTo(45);
        make.right.mas_equalTo(self.chooseButton.mas_left).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(200);
    }];
    
    self.showTextView = [[IQTextView alloc] init];
    self.showTextView.editable = NO;
    self.showTextView.backgroundColor = FWClearColor;
    [topView addSubview:self.showTextView];
    [self.showTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView);
        make.left.mas_equalTo(topView).mas_offset(14);
        make.height.mas_equalTo(42);
        make.right.mas_equalTo(self.chooseButton.mas_left).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(200);
    }];
    
    
    self.pickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
    self.pickerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.pickerView.backgroundColor=[UIColor clearColor];
    
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    bar.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    UIBarButtonItem *doneButton = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar.items = @[cancelButton,flexItem, fixItem, doneButton];
    
    self.showTextView.inputView = self.pickerView;
    self.showTextView.inputAccessoryView = bar;
    
    totalSegement = [[FWTotalListSegementView alloc] init];
    totalSegement.itemDelegate = self;
    totalSegement.frame = CGRectMake(0,CGRectGetMaxY(topView.frame), SCREEN_WIDTH, 40);
    [self.view addSubview:self.totalSegement];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 77;
    infoTableView.backgroundColor = FWTextColor_F8F8F8;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(totalSegement.mas_bottom);
        make.bottom.mas_equalTo(self.view);
    }];
    
    DHWeakSelf;
    self.infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestData:NO];
    }];
    self.infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestData:YES];
    }];
    
    mineView = [[FWMyScoreView alloc] init];
    mineView.backgroundColor = FWColor(@"ffffff");
    [self.view addSubview:mineView];
    [mineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(infoTableView.mas_bottom);
    }];
    mineView.hidden = YES;
    
    UITapGestureRecognizer * Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mineViewClick)];
    [mineView addGestureRecognizer:Tap];
}

#pragma mark - > tableview`s delegate & datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWGroupScoreCellID";
    
    FWGroupScoreCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWGroupScoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.numberLabel.text =@(indexPath.row+1).stringValue;
    
    if (self.dataSource.count > indexPath.row) {
        [cell configCellForModel:self.dataSource[indexPath.row] withIndex:indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.showTextView resignFirstResponder];

    if (indexPath.row < self.dataSource.count) {
        FWPlayerScoreDetailViewController * DVC = [[FWPlayerScoreDetailViewController alloc] init];
        DVC.ScoreModel = self.dataSource[indexPath.row];
        [self.navigationController pushViewController:DVC animated:YES];
    }
}

- (void)mineViewClick{
    
    [self.showTextView resignFirstResponder];

    FWPlayerScoreDetailViewController * DVC = [[FWPlayerScoreDetailViewController alloc] init];
    DVC.ScoreModel = self.rankModel.driver_rank_item;
    [self.navigationController pushViewController:DVC animated:YES];
}

#pragma mark - > 点击推荐item
- (void)segementItemClickTap:(NSInteger)index{
    
    if (self.lastIndex == index) {
        return;
    }
    
    [self.showTextView resignFirstResponder];

    self.lastIndex = index;
    
    if (self.groupArray.count > index) {
        
        FWGroupInfoModel * infoModel = self.groupArray[index];
        self.group_id = infoModel.group_id;
        self.group_name = infoModel.group_name;
        
        [self requestData:NO];
    }
}

#pragma mark - > 搜索成绩
- (void)toSearchOnClick{
    
    [self.showTextView resignFirstResponder];
    
    FWScoreSearchViewController * SVC = [[FWScoreSearchViewController alloc] init];
    [self.navigationController pushViewController:SVC animated:YES];
}

#pragma mark - > 筛选（弹出选择器）
- (void)chooseButtonClick{
    
    [self.showTextView becomeFirstResponder];
}

#pragma mark - > pickerview数据源及代理方法
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

#pragma mark 数据源  numberOfRowsInComponent
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.firstArray.count;
}

#pragma delegate 显示信息方法
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.firstArray objectAtIndex:row];
}

#pragma mark 选中行信息
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
}

#pragma mark 显示行高
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.00;
}

#pragma mark - pickerView 每列宽度
- (CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    return SCREEN_WIDTH;
}

#pragma mark - > 选中
- (void)doneButtonClick:(UIBarButtonItem *)button{
    [self.view endEditing:YES];
    
    NSInteger onerow=[self.pickerView selectedRowInComponent:0];
    
    if (self.firstArray.count > onerow) {
        
        self.year = self.categaryRankModel.category_list[onerow].year;
        self.type = self.categaryRankModel.category_list[onerow].type;
        self.sport_id = self.categaryRankModel.category_list[onerow].sport_id;
        self.group_id = @"0";
        self.nameLabel.text = self.categaryRankModel.category_list[onerow].sport_name;
        
        [self requestTeam];
        [self requestData:NO];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = (UILabel *)view;
    if (lbl == nil) {
        lbl = [[UILabel alloc]init];
        //在这里设置字体相关属性
        lbl.font = [UIFont systemFontOfSize:16];
        lbl.textColor = FWTextColor_000000;
        [lbl setTextAlignment:1];
        [lbl setBackgroundColor:[UIColor clearColor]];
    }
    //重新加载lbl的文字内容
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return lbl;
}
#pragma mark - > 取消
- (void)cancelButtonClick:(UIBarButtonItem *)button{
    [self.view endEditing:YES];
}

@end
