//
//  FWNewShareScoreViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/12.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWNewShareScoreViewController.h"

@interface FWNewShareScoreViewController ()

@property (nonatomic, strong) UIImageView * containerView;

@property (nonatomic, strong) UIImageView * mainView;

@property (nonatomic, strong) UILabel * titleOneLabel;

@property (nonatomic, strong) UIButton    * pengyouquanButton;
@property (nonatomic, strong) UIButton    * friendButton;

@property (nonatomic, strong) UILabel * numberOneLabel;
@property (nonatomic, strong) UILabel * numberTwoLabel;

@property (nonatomic, strong) UIImageView * leftImageView;
@property (nonatomic, strong) UIImageView * middleImageView;
@property (nonatomic, strong) UIImageView * rightImageView;

@property (nonatomic, strong) UIImageView * photoOneImageView;
@property (nonatomic, strong) UIImageView * photoTwoImageView;

@property (nonatomic, strong) UILabel * nameOneLabel;
@property (nonatomic, strong) UILabel * nameTwoLabel;

@property (nonatomic, strong) UILabel * carOneLabel;
@property (nonatomic, strong) UILabel * carTwoLabel;

@property (nonatomic, strong) UILabel * ETTimeOneLabel;
@property (nonatomic, strong) UILabel * RTTimeOneLabel;
@property (nonatomic, strong) UILabel * WeisuTimeOneLabel;

@property (nonatomic, strong) UILabel * ETTimeTwoLabel;
@property (nonatomic, strong) UILabel * RTTimeTwoLabel;
@property (nonatomic, strong) UILabel * WeisuTimeTwoLabel;

@property (nonatomic, strong) UIImageView * winnerOneImageView;
@property (nonatomic, strong) UIImageView * winnerTwoImageView;

@property (nonatomic, strong) UIImageView * codeImageView;

@end

@implementation FWNewShareScoreViewController
@synthesize containerView;
@synthesize mainView;
@synthesize titleOneLabel;
@synthesize pengyouquanButton;
@synthesize friendButton;
@synthesize numberOneLabel;
@synthesize numberTwoLabel;
@synthesize leftImageView;
@synthesize middleImageView;
@synthesize rightImageView;
@synthesize photoOneImageView;
@synthesize photoTwoImageView;
@synthesize nameOneLabel;
@synthesize nameTwoLabel;
@synthesize carOneLabel;
@synthesize carTwoLabel;
@synthesize ETTimeOneLabel;
@synthesize RTTimeOneLabel;
@synthesize WeisuTimeOneLabel;
@synthesize ETTimeTwoLabel;
@synthesize RTTimeTwoLabel;
@synthesize WeisuTimeTwoLabel;
@synthesize shareType;
@synthesize codeImageView;
@synthesize winnerOneImageView;
@synthesize winnerTwoImageView;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self trackPageBegin:@"比赛分享页"];

    [[NSNotificationCenter defaultCenter] postNotificationName:MonitorLiveNetworking object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"比赛分享页"];
}

/**
 * 一人成绩和双人对决 有分享按钮，没有二维码视图
 * 宣传图片没有分享按钮，有二维码视图
 */
- (void)viewDidLoad{
    [super viewDidLoad];

    [self setupSubviews];
    [self setupShareButton];

    if ([shareType isEqualToString:@"1"]){
        /* 单人 */
        [self setupSingleBettleSubviews];
    }else if ([shareType isEqualToString:@"2"]){
        /* 双人 */
        [self setupDoubleBettleSubviews];
    }
}

#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{

    if (sender == pengyouquanButton) {

        [self actionForScreenShotWith:containerView savePhoto:NO withSence:WXSceneTimeline];
    }else{

        [self actionForScreenShotWith:containerView savePhoto:NO withSence:WXSceneSession];
    }
}


- (void)WXSendImage:(UIImage *)image withShareScene:(enum WXScene)scene {
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [documentPath stringByAppendingPathComponent:@"ocr.jpg"];
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        [imageData writeToFile:filePath atomically:NO];

        UIImage *thumbImage = [self compressImage:image toByte:32768];

        WXImageObject *ext = [WXImageObject object];
        // 小于10MB
        ext.imageData = imageData;

        WXMediaMessage *message = [WXMediaMessage message];
        message.mediaObject = ext;
        // 缩略图 小于32KB
        [message setThumbImage:thumbImage];

        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.scene = scene;
        req.message = message;
        [WXApi sendReq:req];
    }else {
        // 提示用户安装微信
        [[FWHudManager sharedManager] showErrorMessage:@"您还没装微信哦~" toController:self];
    }
}

#pragma mark - > 截屏
- (void)actionForScreenShotWith:(UIView *)aimView savePhoto:(BOOL)savePhoto withSence:(enum WXScene)scene {

    if (!aimView) return;

    UIGraphicsBeginImageContextWithOptions(aimView.bounds.size, NO, 0.0f);
    [aimView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    if (savePhoto) {
        UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }else{
        [self WXSendImage:viewImage withShareScene:scene];
    }
}

#pragma mark - >  保存到本地相册
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    if (error) {
        NSLog(@"保存失败，请重试");
    } else {
        NSLog(@"保存成功");
    }
}

#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {

    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;

    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;

    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }

    return resultImage;
}

- (void)tapNothing{

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - > 初始化主视图
- (void)setupSubviews{

    UIView * bgView = [[UIView alloc] init];
    bgView.userInteractionEnabled= YES;
    bgView.backgroundColor = FWClearColor;
    [self.view addSubview:bgView];
    [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNothing)];
    [bgView addGestureRecognizer:tap];

    containerView = [[UIImageView alloc] init];
    [self.view addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        if(SCREEN_HEIGHT < 667){
            make.top.mas_equalTo(self.view).mas_offset(48);
        }else{
            make.top.mas_equalTo(self.view).mas_offset(100);
        }
        make.width.mas_equalTo(295);
        make.height.mas_equalTo(503);
    }];

    mainView = [[UIImageView alloc] init];
    mainView.contentMode = UIViewContentModeScaleAspectFill;
    mainView.clipsToBounds = YES;
    [mainView sd_setImageWithURL:[NSURL URLWithString:self.scoreModel.share_bg_url] placeholderImage:[UIImage imageNamed:@"new_share_bg"]];
    //    mainView.userInteractionEnabled = YES;
    [containerView addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(containerView);
    }];

    CGAffineTransform matrix =CGAffineTransformMake(1, 0, tanf(10 * (CGFloat)M_PI / 180), 1, 0, 0);
    //设置反射。倾斜角度。
    UIFontDescriptor *desc = [UIFontDescriptor fontDescriptorWithName:@"HElvetica-BoldOblique" matrix:matrix];
    //取得系统字符并设置反射。
    
    
   NSString * topTitle = [NSString stringWithFormat:@"%@%@",self.scoreModel.img_title_1,self.scoreModel.img_title_2];

//    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:topTitle];
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.alignment = NSTextAlignmentJustified; //设置两端对齐显示
//    [attributedStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedStr.length)];
//    [attributedStr addAttribute:NSFontAttributeName value:[UIFont fontWithDescriptor :desc size :12] range:NSMakeRange(0, attributedStr.length)];
//
    titleOneLabel = [UILabel new];
    titleOneLabel.font = [UIFont fontWithDescriptor :desc size :14];
    titleOneLabel.text = topTitle;
    titleOneLabel.numberOfLines = 2;
    titleOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
//    titleOneLabel.attributedText = attributedStr;
    titleOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:titleOneLabel];
    [titleOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mainView);
        make.top.mas_equalTo(mainView).mas_offset(63);
        make.height.mas_greaterThanOrEqualTo(20);
        make.left.right.mas_equalTo(mainView);
        make.width.mas_greaterThanOrEqualTo(100);
    }];

    codeImageView = [[UIImageView alloc] init];
    codeImageView.image = [UIImage imageNamed:@"match_QRCode"];
    codeImageView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [mainView addSubview:codeImageView];
    [codeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(34, 34));
        make.bottom.mas_equalTo(mainView).mas_offset(-16);
        make.right.mas_equalTo(mainView).mas_offset(-20);
    }];
}

#pragma mark - > 初始化分享按钮
- (void)setupShareButton{

    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {

        CGFloat buttonHeight = 47;

        friendButton = [[UIButton alloc] init];
        self.friendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.friendButton setImage:[UIImage imageNamed:@"share_weixinhaoyou"] forState:UIControlStateNormal];
        [self.friendButton setTitle:@"   微信好友" forState:UIControlStateNormal];
        [self.friendButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        self.friendButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        [friendButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:friendButton];
        [friendButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(140);
            make.left.mas_equalTo(containerView).mas_offset(10);
            make.height.mas_equalTo(buttonHeight);
            if (SCREEN_HEIGHT<667 && ![self.shareType isEqualToString:@"3"]) {
                make.top.mas_equalTo(containerView.mas_bottom).mas_offset(5);
            }else{
                make.top.mas_equalTo(containerView.mas_bottom).mas_offset(33);
            }
        }];
        
        pengyouquanButton = [[UIButton alloc] init];
        pengyouquanButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [pengyouquanButton setImage:[UIImage imageNamed:@"share_pengyouquan"] forState:UIControlStateNormal];
        [pengyouquanButton setTitle:@"   朋友圈" forState:UIControlStateNormal];
        [pengyouquanButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
        pengyouquanButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        [pengyouquanButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pengyouquanButton];
        [pengyouquanButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(friendButton);
            make.right.mas_equalTo(containerView).mas_offset(-10);
            make.height.mas_equalTo(friendButton);
            make.top.mas_equalTo(friendButton);
        }];
    }
}

#pragma mark - > 初始化两人对决视图
- (void)setupDoubleBettleSubviews{
    

    leftImageView = [[UIImageView alloc] init];
    leftImageView.image = [UIImage imageNamed:@"share_bg_left"];
    [mainView addSubview:leftImageView];
    [leftImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleOneLabel.mas_bottom).mas_offset(60);
        make.left.mas_equalTo(mainView).mas_offset(23);
        make.size.mas_equalTo(CGSizeMake(49, 55));
    }];
    
    numberOneLabel = [[UILabel alloc] init];
    numberOneLabel.font = [UIFont fontWithName:@"Arial-BoldItalicMT" size: 41];
    numberOneLabel.textColor = FWTextColor_000000;
    numberOneLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    numberOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:numberOneLabel];
    numberOneLabel.frame = CGRectMake(46, 156.5, 109, 57);

    UIBezierPath * numberOnePath = [[UIBezierPath alloc] init];
    [numberOnePath moveToPoint:CGPointMake(25, 00)];
    [numberOnePath addLineToPoint:CGPointMake(109, 00)];
    [numberOnePath addLineToPoint:CGPointMake(99, 57)];
    [numberOnePath addLineToPoint:CGPointMake(15, 57)];
    [numberOnePath closePath];
    
    CAShapeLayer * numberOneShapeLayer = [CAShapeLayer layer];
    numberOneShapeLayer.path = numberOnePath.CGPath;
    numberOneLabel.layer.mask = numberOneShapeLayer;
    
    photoOneImageView = [UIImageView new];
    photoOneImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoOneImageView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [mainView addSubview:photoOneImageView];
    photoOneImageView.frame = CGRectMake(46, 212, 109, 82);

    
    UIBezierPath * photoOnePath = [[UIBezierPath alloc] init];
    [photoOnePath moveToPoint:CGPointMake(15, 00)];
    [photoOnePath addLineToPoint:CGPointMake(99, 00)];
    [photoOnePath addLineToPoint:CGPointMake(85, 82)];
    [photoOnePath addLineToPoint:CGPointMake(0, 82)];
    [photoOnePath closePath];
    
    CAShapeLayer * photoOneLayer = [CAShapeLayer layer];
    photoOneLayer.path = photoOnePath.CGPath;
    photoOneImageView.layer.mask = photoOneLayer;
    
    winnerOneImageView = [[UIImageView alloc] init];
    winnerOneImageView.image = [UIImage imageNamed:@"share_winner"];
    [mainView addSubview:winnerOneImageView];
    winnerOneImageView.frame = CGRectMake(0, 0, 80, 80);
    winnerOneImageView.center = CGPointMake(46, 294);
    winnerOneImageView.hidden = YES;

    numberTwoLabel = [[UILabel alloc] init];
    numberTwoLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    numberTwoLabel.font = [UIFont fontWithName:@"Arial-BoldItalicMT" size: 41];
    numberTwoLabel.textColor = FWTextColor_000000;
    numberTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:numberTwoLabel];
    numberTwoLabel.frame = CGRectMake(155, 156.5, 109, 57);
    
    UIBezierPath * numberTwoPath = [[UIBezierPath alloc] init];
    [numberTwoPath moveToPoint:CGPointMake(25, 00)];
    [numberTwoPath addLineToPoint:CGPointMake(109, 00)];
    [numberTwoPath addLineToPoint:CGPointMake(99, 57)];
    [numberTwoPath addLineToPoint:CGPointMake(15, 57)];
    [numberTwoPath closePath];
    
    CAShapeLayer * numberTwoShapeLayer = [CAShapeLayer layer];
    numberTwoShapeLayer.path = numberTwoPath.CGPath;
    numberTwoLabel.layer.mask = numberTwoShapeLayer;
    

    photoTwoImageView = [UIImageView new];
    photoTwoImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoTwoImageView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [mainView addSubview:photoTwoImageView];
    photoTwoImageView.frame = CGRectMake(155, 212, 109, 82);
    
    
    UIBezierPath * photoTwoPath = [[UIBezierPath alloc] init];
    [photoTwoPath moveToPoint:CGPointMake(15, 00)];
    [photoTwoPath addLineToPoint:CGPointMake(99, 00)];
    [photoTwoPath addLineToPoint:CGPointMake(85, 82)];
    [photoTwoPath addLineToPoint:CGPointMake(0, 82)];
    [photoTwoPath closePath];
    
    CAShapeLayer * photoTwoLayer = [CAShapeLayer layer];
    photoTwoLayer.path = photoTwoPath.CGPath;
    photoTwoImageView.layer.mask = photoTwoLayer;

    winnerTwoImageView = [[UIImageView alloc] init];
    winnerTwoImageView.image = [UIImage imageNamed:@"share_winner"];
    [mainView addSubview:winnerTwoImageView];
    winnerTwoImageView.frame = CGRectMake(0, 0, 80, 80);
    winnerTwoImageView.center = CGPointMake(295-45, 294);
    winnerTwoImageView.hidden = YES;
    
    rightImageView = [[UIImageView alloc] init];
    rightImageView.image = [UIImage imageNamed:@"share_bg_right"];
    [mainView addSubview:rightImageView];
    [rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.photoTwoImageView);
        make.left.mas_equalTo(self.photoTwoImageView.mas_right).mas_offset(-24.2);
        make.size.mas_equalTo(CGSizeMake(33, 42));
    }];
    
    middleImageView = [[UIImageView alloc] init];
    middleImageView.image = [UIImage imageNamed:@"share_bg_middle"];
    [mainView addSubview:middleImageView];
    [middleImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.photoTwoImageView);
        make.left.mas_equalTo(self.photoTwoImageView).mas_offset(-26);
        make.size.mas_equalTo(CGSizeMake(52, 151));
    }];
    
    [mainView sendSubviewToBack:middleImageView];
    
    nameOneLabel = [[UILabel alloc] init];
    nameOneLabel.font =  [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    nameOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:nameOneLabel];
    [nameOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(photoOneImageView).mas_offset(-12);
        make.top.mas_equalTo(photoOneImageView.mas_bottom).mas_offset(9);
        make.height.mas_equalTo(21);
        make.width.mas_equalTo(110);
    }];
//    [nameOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(mainView);
//        make.width.mas_greaterThanOrEqualTo(10);
//        make.top.mas_equalTo(photoOneImageView.mas_bottom).mas_offset(9);
//        make.centerX.mas_equalTo(photoOneImageView).mas_offset(-12);
//        make.height.mas_equalTo(20);
//    }];

    nameTwoLabel = [[UILabel alloc] init];
    nameTwoLabel.font =  [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    nameTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:nameTwoLabel];
    [nameTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(photoTwoImageView).mas_offset(-12);
        make.top.mas_equalTo(photoTwoImageView.mas_bottom).mas_offset(8);
        make.height.mas_equalTo(21);
        make.width.mas_equalTo(110);
    }];
//    [nameTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(mainView);
//        make.top.mas_equalTo(nameOneLabel);
//        make.centerX.mas_equalTo(photoTwoImageView).mas_offset(-12);
//        make.width.mas_greaterThanOrEqualTo(10);
//        make.height.mas_equalTo(nameOneLabel);
//    }];

    carOneLabel = [[UILabel alloc] init];
    carOneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    carOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    carOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:carOneLabel];
    [carOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView);
        make.top.mas_equalTo(nameOneLabel.mas_bottom).mas_offset(3);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(nameOneLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];

    carTwoLabel = [[UILabel alloc] init];
    carTwoLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    carTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    carTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:carTwoLabel];
    [carTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(mainView);
        make.top.mas_equalTo(nameOneLabel.mas_bottom).mas_offset(3);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(nameTwoLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];

    UIImage * bgImage = [UIImage imageNamed:@"share_value_bg"];
    
    UIImageView * ETTimeOneIV = [[UIImageView alloc] init];
    ETTimeOneIV.image = bgImage;
    [mainView addSubview:ETTimeOneIV];
    [ETTimeOneIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mainView).mas_offset(-11/2-55);
        make.top.mas_equalTo(carOneLabel.mas_bottom).mas_offset(8);
        make.height.mas_equalTo(21);
        make.width.mas_equalTo(110);
    }];
    
    ETTimeOneLabel = [[UILabel alloc] init];
    ETTimeOneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    ETTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    ETTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [ETTimeOneIV addSubview:ETTimeOneLabel];
    [ETTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(ETTimeOneIV);
    }];

    UIImageView * ETTimeTwoIV = [[UIImageView alloc] init];
    ETTimeTwoIV.image = bgImage;
    [mainView addSubview:ETTimeTwoIV];
    [ETTimeTwoIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mainView).mas_offset(11/2+55);
        make.top.mas_equalTo(ETTimeOneLabel);
        make.height.mas_equalTo(ETTimeOneLabel);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    ETTimeTwoLabel = [[UILabel alloc] init];
    ETTimeTwoLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    ETTimeTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    ETTimeTwoLabel.textAlignment = NSTextAlignmentCenter;
    [ETTimeTwoIV addSubview:ETTimeTwoLabel];
    [ETTimeTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(ETTimeTwoIV);
    }];

    UIImageView * RTTimeOneIV = [[UIImageView alloc] init];
    RTTimeOneIV.image = bgImage;
    [mainView addSubview:RTTimeOneIV];
    [RTTimeOneIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(ETTimeOneLabel);
        make.top.mas_equalTo(ETTimeOneLabel.mas_bottom).mas_offset(6);
        make.height.mas_equalTo(ETTimeOneLabel);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    RTTimeOneLabel = [[UILabel alloc] init];
    RTTimeOneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    RTTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    RTTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [RTTimeOneIV addSubview:RTTimeOneLabel];
    [RTTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(RTTimeOneIV);
    }];

    UIImageView * RTTimeTwoIV = [[UIImageView alloc] init];
    RTTimeTwoIV.image = bgImage;
    [mainView addSubview:RTTimeTwoIV];
    [RTTimeTwoIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(ETTimeTwoLabel);
        make.top.mas_equalTo(ETTimeTwoLabel.mas_bottom).mas_offset(6);
        make.height.mas_equalTo(ETTimeTwoLabel);
        make.width.mas_equalTo(ETTimeTwoLabel);
    }];
    
    RTTimeTwoLabel = [[UILabel alloc] init];
    RTTimeTwoLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    RTTimeTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    RTTimeTwoLabel.textAlignment = NSTextAlignmentCenter;
    [RTTimeTwoIV addSubview:RTTimeTwoLabel];
    [RTTimeTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(RTTimeTwoIV);
    }];

    UIImageView * WeisuTimeOneIV = [[UIImageView alloc] init];
    WeisuTimeOneIV.image = bgImage;
    [mainView addSubview:WeisuTimeOneIV];
    [WeisuTimeOneIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(ETTimeOneLabel);
        make.top.mas_equalTo(RTTimeOneLabel.mas_bottom).mas_offset(6);
        make.height.mas_equalTo(RTTimeOneLabel);
        make.width.mas_greaterThanOrEqualTo(110);
    }];
    
    WeisuTimeOneLabel = [[UILabel alloc] init];
    WeisuTimeOneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    WeisuTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    WeisuTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [WeisuTimeOneIV addSubview:WeisuTimeOneLabel];
    [WeisuTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(WeisuTimeOneIV);
    }];

    UIImageView * WeisuTimeTwoIV = [[UIImageView alloc] init];
    WeisuTimeTwoIV.image = bgImage;
    [mainView addSubview:WeisuTimeTwoIV];
    [WeisuTimeTwoIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(RTTimeTwoLabel);
        make.top.mas_equalTo(RTTimeTwoLabel.mas_bottom).mas_offset(6);
        make.height.mas_equalTo(RTTimeTwoLabel);
        make.width.mas_greaterThanOrEqualTo(110);
    }];
    
    WeisuTimeTwoLabel = [[UILabel alloc] init];
    WeisuTimeTwoLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    WeisuTimeTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    WeisuTimeTwoLabel.textAlignment = NSTextAlignmentCenter;
    [WeisuTimeTwoIV addSubview:WeisuTimeTwoLabel];
    [WeisuTimeTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(WeisuTimeTwoIV);
    }];

    numberOneLabel.text = [NSString stringWithFormat:@" %@",self.listModel.car_1_info.car_no];
    numberTwoLabel.text = [NSString stringWithFormat:@" %@",self.listModel.car_2_info.car_no];

    nameOneLabel.text = self.listModel.car_1_info.player_name;
    nameTwoLabel.text = self.listModel.car_2_info.player_name;

    carOneLabel.text = self.listModel.car_1_info.car_type;
    carTwoLabel.text = self.listModel.car_2_info.car_type;

    ETTimeOneLabel.text = [NSString stringWithFormat:@"ET:%@ s",self.listModel.car_1_info.elapsed_time];
    RTTimeOneLabel.text = [NSString stringWithFormat:@"RT:%@ s",self.listModel.car_1_info.reaction_time];
    WeisuTimeOneLabel.text = [NSString stringWithFormat:@"尾速:%@ ",self.listModel.car_1_info.vehicle_speed];

    ETTimeTwoLabel.text = [NSString stringWithFormat:@"ET:%@ s",self.listModel.car_2_info.elapsed_time];
    RTTimeTwoLabel.text = [NSString stringWithFormat:@"RT:%@ s",self.listModel.car_2_info.reaction_time];
    WeisuTimeTwoLabel.text = [NSString stringWithFormat:@"尾速:%@ ",self.listModel.car_2_info.vehicle_speed];

    [photoOneImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.car_1_info.header] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (error) {
            photoOneImageView.image = [UIImage imageNamed:@"placeholder_photo"];
        }
    }];
    [photoTwoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.car_2_info.header] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (error) {
            photoOneImageView.image = [UIImage imageNamed:@"placeholder_photo"];
        }
    }];
    
    // 暂时注释
//    if ([self.listModel.winner_car_no isEqualToString:self.listModel.car_1_info.car_no]) {
//        winnerOneImageView.hidden = NO;
//        winnerTwoImageView.hidden = YES;
//        [self.mainView bringSubviewToFront:winnerOneImageView];
//    }else if ([self.listModel.winner_car_no isEqualToString:self.listModel.car_2_info.car_no]){
//        winnerOneImageView.hidden = YES;
//        winnerTwoImageView.hidden = NO;
//        [self.mainView bringSubviewToFront:winnerTwoImageView];
//    }
}

#pragma mark - > 初始化单人视图
- (void)setupSingleBettleSubviews{

    numberOneLabel = [[UILabel alloc] init];
    numberOneLabel.font = [UIFont fontWithName:@"Arial-BoldItalicMT" size: 41];
    numberOneLabel.textColor = FWTextColor_000000;
    numberOneLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    numberOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:numberOneLabel];
    numberOneLabel.frame = CGRectMake(102, 155, 109, 57);
    
    UIBezierPath * numberOnePath = [[UIBezierPath alloc] init];
    [numberOnePath moveToPoint:CGPointMake(25, 00)];
    [numberOnePath addLineToPoint:CGPointMake(109, 00)];
    [numberOnePath addLineToPoint:CGPointMake(99, 57)];
    [numberOnePath addLineToPoint:CGPointMake(15, 57)];
    [numberOnePath closePath];
    
    CAShapeLayer * numberOneShapeLayer = [CAShapeLayer layer];
    numberOneShapeLayer.path = numberOnePath.CGPath;
    numberOneLabel.layer.mask = numberOneShapeLayer;
    
    photoOneImageView = [UIImageView new];
    photoOneImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoOneImageView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [mainView addSubview:photoOneImageView];
    photoOneImageView.frame = CGRectMake(102, 212, 109, 82);
    
    
    UIBezierPath * photoOnePath = [[UIBezierPath alloc] init];
    [photoOnePath moveToPoint:CGPointMake(15, 00)];
    [photoOnePath addLineToPoint:CGPointMake(99, 00)];
    [photoOnePath addLineToPoint:CGPointMake(85, 82)];
    [photoOnePath addLineToPoint:CGPointMake(0, 82)];
    [photoOnePath closePath];
    
    CAShapeLayer * photoOneLayer = [CAShapeLayer layer];
    photoOneLayer.path = photoOnePath.CGPath;
    photoOneImageView.layer.mask = photoOneLayer;
    
    winnerOneImageView = [[UIImageView alloc] init];
    winnerOneImageView.image = [UIImage imageNamed:@"share_winner"];
    [mainView addSubview:winnerOneImageView];
    winnerOneImageView.frame = CGRectMake(0, 0, 80, 80);
    winnerOneImageView.center = CGPointMake(102, 294);
    winnerOneImageView.hidden = YES;
    
    leftImageView = [[UIImageView alloc] init];
    leftImageView.image = [UIImage imageNamed:@"share_bg_left"];
    [mainView addSubview:leftImageView];
    [leftImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleOneLabel.mas_bottom).mas_offset(58.5);
        make.left.mas_equalTo(numberOneLabel).mas_offset(-49/2+0.5);
        make.size.mas_equalTo(CGSizeMake(49, 55));
    }];
    
    rightImageView = [[UIImageView alloc] init];
    rightImageView.image = [UIImage imageNamed:@"share_bg_right"];
    [mainView addSubview:rightImageView];
    [rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.photoOneImageView);
        make.left.mas_equalTo(self.photoOneImageView.mas_right).mas_offset(-24.5);
        make.size.mas_equalTo(CGSizeMake(33, 42));
    }];
    
    nameOneLabel = [[UILabel alloc] init];
    nameOneLabel.font =  [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    nameOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:nameOneLabel];
    [nameOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(photoOneImageView.mas_bottom).mas_offset(3);
        make.centerX.mas_equalTo(photoOneImageView).mas_offset(-15);
        make.height.mas_equalTo(20);
    }];
    
    carOneLabel = [[UILabel alloc] init];
    carOneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    carOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    carOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:carOneLabel];
    [carOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView);
        make.top.mas_equalTo(nameOneLabel.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(nameOneLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    
    UIImage * bgImage = [UIImage imageNamed:@"share_value_bg"];
    
    UIImageView * ETTimeOneIV = [[UIImageView alloc] init];
    ETTimeOneIV.image = bgImage;
    [mainView addSubview:ETTimeOneIV];
    [ETTimeOneIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mainView);
        make.top.mas_equalTo(carOneLabel.mas_bottom).mas_offset(8);
        make.height.mas_equalTo(27);
        make.width.mas_equalTo(144);
    }];
    
    ETTimeOneLabel = [[UILabel alloc] init];
    ETTimeOneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    ETTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    ETTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [ETTimeOneIV addSubview:ETTimeOneLabel];
    [ETTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(ETTimeOneIV);
    }];
    
    UIImageView * RTTimeOneIV = [[UIImageView alloc] init];
    RTTimeOneIV.image = bgImage;
    [mainView addSubview:RTTimeOneIV];
    [RTTimeOneIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(ETTimeOneLabel);
        make.top.mas_equalTo(ETTimeOneLabel.mas_bottom).mas_offset(6);
        make.height.mas_equalTo(ETTimeOneLabel);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    RTTimeOneLabel = [[UILabel alloc] init];
    RTTimeOneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    RTTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    RTTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [RTTimeOneIV addSubview:RTTimeOneLabel];
    [RTTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(RTTimeOneIV);
    }];
    
    UIImageView * WeisuTimeOneIV = [[UIImageView alloc] init];
    WeisuTimeOneIV.image = bgImage;
    [mainView addSubview:WeisuTimeOneIV];
    [WeisuTimeOneIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(ETTimeOneLabel);
        make.top.mas_equalTo(RTTimeOneLabel.mas_bottom).mas_offset(6);
        make.height.mas_equalTo(RTTimeOneLabel);
        make.width.mas_equalTo(144);
    }];
    
    WeisuTimeOneLabel = [[UILabel alloc] init];
    WeisuTimeOneLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 14.4];
    WeisuTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    WeisuTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [WeisuTimeOneIV addSubview:WeisuTimeOneLabel];
    [WeisuTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(WeisuTimeOneIV);
    }];
    
    
    [self.mainView bringSubviewToFront:winnerOneImageView];
    
    FWCarInfoModel * carInfo ;

    if (nil == self.listModel.car_1_info.player_name ||
        self.listModel.car_1_info.player_name.length <= 0) {
        carInfo = self.listModel.car_2_info;
    }else{
        carInfo = self.listModel.car_1_info;
    }

    numberOneLabel.text = [NSString stringWithFormat:@" %@",carInfo.car_no];
    nameOneLabel.text = carInfo.player_name;
    carOneLabel.text = carInfo.car_type;
    ETTimeOneLabel.text = [NSString stringWithFormat:@"ET:%@ s",carInfo.elapsed_time];
    RTTimeOneLabel.text = [NSString stringWithFormat:@"RT:%@ s",carInfo.reaction_time];
    WeisuTimeOneLabel.text = [NSString stringWithFormat:@"尾速:%@ ",carInfo.vehicle_speed];

    [photoOneImageView sd_setImageWithURL:[NSURL URLWithString:carInfo.header] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (error) {
            photoOneImageView.image = [UIImage imageNamed:@"placeholder_photo"];
        }
    }];
}

@end
