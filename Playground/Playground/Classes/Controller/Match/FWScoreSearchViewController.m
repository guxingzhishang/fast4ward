//
//  FWScoreSearchViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWScoreSearchViewController.h"
#import "FWGroupScoreCell.h"
#import "FWPlayerRankModel.h"
#import "FWPlayerScoreDetailViewController.h"

@interface FWScoreSearchViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (nonatomic, strong) NSMutableArray * dataSource;

@property (nonatomic, strong) UITableView * infoTableView;
/* 取消按钮 */
@property (nonatomic, strong) UIButton * cancelBtn;
/* 搜索 */
@property (nonatomic, strong) UISearchBar * searchBar;
@property (nonatomic, strong) NSString * query;
@property (nonatomic, assign) NSInteger  pageNum;


@end

@implementation FWScoreSearchViewController
@synthesize infoTableView;
@synthesize searchBar;
@synthesize cancelBtn;

- (NSMutableArray *)dataSource{
    
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"成绩搜索"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"成绩搜索"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.query = @"";
    self.pageNum = 1;
    self.fd_prefersNavigationBarHidden = YES;
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault;
    searchBar.placeholder = @"可按车手名、品牌、车型搜索";
    searchBar.tintColor = FWTextColor_A6A6A6;
    searchBar.barTintColor = [UIColor whiteColor];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    [self.view addSubview:searchBar];
    [searchBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).mas_offset(13/5);
        make.top.mas_equalTo(self.view).mas_offset(15+FWCustomeSafeTop);
        make.right.mas_equalTo(self.view.mas_right).mas_offset(-58.5);
    }];
    
    [self removeBorder:searchBar];
    
    cancelBtn = [[UIButton alloc] init];
    cancelBtn.titleLabel.font = DHSystemFontOfSize_15;
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBtn];
    [cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view ).mas_offset(-10);
        make.centerY.mas_equalTo(searchBar);
        make.size.mas_equalTo(CGSizeMake(40, 30));
    }];
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    infoTableView.rowHeight = 77;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(searchBar.mas_bottom).mas_offset(10);
    }];
    
    DHWeakSelf;
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestSearchText:YES];
    }];
    
    [searchBar becomeFirstResponder];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString * cellID = @"FWPlayerRankCellID";

    FWGroupScoreCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWGroupScoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }

    cell.numberLabel.text =@(indexPath.row+1).stringValue;
    
    if (self.dataSource.count > indexPath.row) {
        [cell configCellForModel:self.dataSource[indexPath.row] withIndex:indexPath.row];
        cell.rankImageView.hidden = YES;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.dataSource.count) {
        FWPlayerScoreDetailViewController * DVC = [[FWPlayerScoreDetailViewController alloc] init];
        DVC.ScoreModel = self.dataSource[indexPath.row];
        [self.navigationController pushViewController:DVC animated:YES];
    }
}

#pragma mark - > searchBar delegate 实时搜索标签
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([self.searchBar.text length] > 20){
        self.searchBar.text = [self.searchBar.text substringToIndex:20];
    }
}

#pragma mark - > 搜索
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar endEditing:YES];
    
    self.query = searchBar.text;
    [self requestSearchText:NO];
}

#pragma mark - > 搜索请求
- (void)requestSearchText:(BOOL)isLoadMore{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if (isLoadMore) {
        self.pageNum += 1;
    }else{
        self.pageNum = 1;
        [self.infoTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];

        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"query":self.query,
                              @"page":@(self.pageNum).stringValue,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_rank_score_search WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWScoreSearchModel * searchModel = [FWScoreSearchModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.infoTableView.mj_footer endRefreshing];
            
            if([searchModel.score_list count] == 0 &&
               self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.dataSource addObjectsFromArray:searchModel.score_list];
                self.infoTableView.hidden = NO;
                [self.infoTableView reloadData];
            }
        }else{
            [self.infoTableView.mj_footer endRefreshing];
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 取消搜索
- (void)cancelBtnOnClick:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 去掉输入框边框
- (void)removeBorder:(UISearchBar * )searchBar{
    
    if (@available(iOS 13.0, *)) {

        UITextField *textField = (UITextField *)self.searchBar.searchTextField;
        
        textField.font = DHSystemFontOfSize_14;
        // 设置 Search 按钮可用
        textField.enablesReturnKeyAutomatically = NO;
        
        UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];
        
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
        [button setTitle:@"收起" forState:UIControlStateNormal];
        [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
        [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
        [bar setItems:buttonsArray];
        textField.inputAccessoryView = bar;
        
        
        // 改变输入框背景色
        textField.subviews[0].backgroundColor = [UIColor clearColor];
        textField.layer.cornerRadius = 2;
        textField.layer.masksToBounds = YES;
    }else{
        // 去掉搜索框边框
        for(int i =  0 ;i < searchBar.subviews.count;i++){
            UIView * backView = searchBar.subviews[i];
            if ([backView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                [backView removeFromSuperview];
                [searchBar setBackgroundColor:[UIColor clearColor]];
                
                break;
            }else{
                NSArray * arr = searchBar.subviews[i].subviews;
                for(int j = 0;j<arr.count;j++   ){
                    UIView * barView = arr[i];
                    if ([barView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                        
                        [barView removeFromSuperview];
                        [searchBar setBackgroundColor:[UIColor clearColor]];
                        break;
                    }
                }
            }
        }
        
        
        // 获取 TextField 并设置
        for (UIView *subView in self.searchBar.subviews) {
            for (UIView *textFieldSubView in subView.subviews) {
                if ([textFieldSubView isKindOfClass:[UITextField class]]) {
                    UITextField *textField = (UITextField *)textFieldSubView;
                    
                    textField.font = DHSystemFontOfSize_14;
                    // 设置 Search 按钮可用
                    textField.enablesReturnKeyAutomatically = NO;
                    
                    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];
                    
                    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
                    [button setTitle:@"收起" forState:UIControlStateNormal];
                    [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
                    [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
                    
                    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
                    
                    UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
                    NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
                    [bar setItems:buttonsArray];
                    textField.inputAccessoryView = bar;
                    
                    break;
                }
            }
        }
        
        // 改变UISearchBar内部输入框样式
        UIView *searchTextField = nil;
        searchTextField = [[[self.searchBar.subviews firstObject] subviews] lastObject];
        // 改变输入框背景色
        searchTextField.subviews[0].backgroundColor = [UIColor clearColor];
        searchTextField.layer.cornerRadius = 2;
        searchTextField.layer.masksToBounds = YES;
    }
}

- (void)hideKeyBoard{
    
    [self.searchBar endEditing:YES];
}

@end
