//
//  FWRegistrationDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWRegistrationHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWRegistrationDetailViewController : FWBaseViewController<UIScrollViewDelegate>
@property (nonatomic, strong) UIButton * blackBackButton;
@property (nonatomic, strong) UIButton * blackShareButton;

@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UIImageView * topView;
@property (nonatomic, strong) UIImageView * coverImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * areaLabel;
@property (nonatomic, strong) UILabel * sumLabel;
@property (nonatomic, strong) UILabel * typeLabel;
@property (nonatomic, strong) UILabel * statusLabel;

@property (nonatomic, strong) UILabel * descLabel;
@property (nonatomic, strong) UIWebView * detailWebView;

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIButton * contectButton;
@property (nonatomic, strong) UIButton * signupButton;

@property (nonatomic, strong) NSString * baoming_id;

@end

NS_ASSUME_NONNULL_END
