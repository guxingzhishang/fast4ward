//
//  FWAllScheduleViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWAllScheduleViewController.h"
#import "FWGroupScoreViewController.h"

@interface FWAllScheduleViewController ()
@property (nonatomic, strong) FWAllScheduleModel * allScheduleModel;

@end

@implementation FWAllScheduleViewController

#pragma mark - > 请求全部赛程
- (void)requestAllSchedule{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    NSDate * startDare = [NSDate date];

    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_sport_schedule_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

//        NSLog(@"耗时：%f 秒",(double)[[NSDate date] timeIntervalSinceDate:startDare]);

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.allScheduleModel = [FWAllScheduleModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self setupSubViews];
            
            if (self.allScheduleModel.top_img_url.length > 0) {
                if (self.myBlock) {
                    self.myBlock(self.allScheduleModel.top_img_url);
                }
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"全部赛程页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"全部赛程页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupSubViews];
    [self requestAllSchedule];
}

- (void)setupSubViews{
    
    UIScrollView * mainView = [[UIScrollView alloc] init];
    mainView.delegate = self;
    mainView.clipsToBounds = YES;
    mainView.backgroundColor = FWTextColor_F8F8F8;
    [self.view addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    CGFloat currentY = 0;
    
    UIView * bgView = [[UIView alloc] init];
    [mainView addSubview:bgView];
    [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(mainView);
    }];
    
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWColorWihtAlpha(@"000000", 0.2);
    [bgView addSubview:lineView];
    lineView.frame = CGRectMake(20, 33, 6, 5000);
    lineView.hidden = YES;
    
    for (int i = 0; i < self.allScheduleModel.schedule_list.count; i++) {
        
        FWAllScheduleListModel * listModel = self.allScheduleModel.schedule_list[i];
        
        UIImageView * iconImageView = [[UIImageView alloc] init];
        iconImageView.image = [UIImage imageNamed:@"Schedule_cycle"];
        iconImageView.frame = CGRectMake(12.5, currentY+18, 18, 18);
        [bgView addSubview:iconImageView];
        
        UILabel * yearLabel = [[UILabel alloc] init];
        yearLabel.text = listModel.year;
        yearLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
        yearLabel.textColor = FWColorWihtAlpha(@"000000",0.4);
        yearLabel.textAlignment = NSTextAlignmentLeft;
        [bgView addSubview:yearLabel];
        [yearLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(iconImageView.mas_right).mas_offset(10);
            make.centerY.mas_equalTo(iconImageView);
            make.width.mas_greaterThanOrEqualTo(10);
            make.height.mas_equalTo(22);
        }];
        
        currentY = CGRectGetMaxY(iconImageView.frame) +12;
        
        for (int j = 0;j<listModel.list.count; j++){
            
            FWScheduleListModel * subListModel = listModel.list[j];
            
            FWScheduleView * scheduleView = [[FWScheduleView alloc] init];
            scheduleView.vc = self;
            [scheduleView configForView:subListModel WithSection:i WithRow:j];
            scheduleView.frame = CGRectMake(31, currentY, SCREEN_WIDTH-45, 157);
            [bgView addSubview:scheduleView];
            
            
            NSString * sport_id = [GFStaticData getObjectForKey:@"sport_id"];

            if (i == 0 && j == 0) {
                // 永远判断第一个
                if (sport_id.length >0 && ![sport_id isEqualToString:subListModel.sport_id]) {

                    scheduleView.nearestImageView.hidden = NO;
                }else{
                    if (sport_id.length <= 0) {
                        scheduleView.nearestImageView.hidden = NO;
                    }else{
                        scheduleView.nearestImageView.hidden = YES;
                    }
                }
            }
           
            if (i == self.allScheduleModel.schedule_list.count-1 && j == listModel.list.count- 1) {
                [scheduleView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(bgView).mas_offset(31);
                    make.right.mas_equalTo(bgView).mas_offset(-14);
                    make.width.mas_equalTo(SCREEN_WIDTH-45);
                    make.height.mas_equalTo(157);
                    make.top.mas_equalTo(bgView).mas_offset(currentY);
                    make.bottom.mas_equalTo(bgView).mas_offset(-40);
                }];
                
                lineView.hidden = NO;
            }
            
            if(j == listModel.list.count - 1 ){
                currentY = CGRectGetMaxY(scheduleView.frame);
            }else{
                currentY = CGRectGetMaxY(scheduleView.frame)+10;
            }
        }
    }
    
    [bgView layoutIfNeeded];
    
    lineView.frame = CGRectMake(20, 36, 3, CGRectGetHeight(bgView.frame)-40-36);
}

@end


@implementation FWScheduleView
@synthesize subListModel;

- (id)init{
    self = [super init];
    if (self) {
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    [self addSubview:self.bgView];
    self.bgView.frame = CGRectMake(0, 0, SCREEN_WIDTH-45, 157);
    [self.bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgViewClick)]];

    
    self.picImageView = [[UIImageView alloc] init];
    self.picImageView.layer.cornerRadius = 5;
    self.picImageView.layer.masksToBounds = YES;
    self.picImageView.clipsToBounds = YES;
    self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.picImageView.image = [UIImage imageNamed:@"placeholder"];
    [self.bgView addSubview:self.picImageView];
    self.picImageView.frame = CGRectMake(10, 12, 115, 75);

    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.font = [UIFont fontWithName:@"PingFang SC" size: 17];
    self.titleLabel.textColor = FWTextColor_252527;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.picImageView.mas_right).mas_offset(10);
        make.right.mas_equalTo(self.bgView).mas_offset(-10);
        make.top.mas_equalTo(self.picImageView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];

    self.nearestImageView = [[UIImageView alloc]init];
    self.nearestImageView.image = [UIImage imageNamed:@"match_new"];
    [self.bgView addSubview:self.nearestImageView];
    [self.nearestImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(self.bgView);
        make.size.mas_equalTo(CGSizeMake(29, 29));
    }];
    self.nearestImageView.hidden = YES;
    
    self.bottomView = [[UIView alloc] init];
    self.bottomView.backgroundColor = FWColor(@"e8e8e8");
    [self.bgView addSubview:self.bottomView];
    self.bottomView.frame = CGRectMake(0, CGRectGetHeight(self.bgView.frame)-58, CGRectGetWidth(self.bgView.frame), 58);

    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.image = [UIImage imageNamed:@"Schedule_rank"];
    [self.bottomView addSubview:self.iconImageView];
    self.iconImageView.frame = CGRectMake(14, 21, 16, 16);

    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 36/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.clipsToBounds = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder"];
    [self.bottomView addSubview:self.photoImageView];
    self.photoImageView.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame)+9, 12, 36, 36);

    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.ETLabel.textColor = FWColor(@"#38A4F8");
    self.ETLabel.textAlignment = NSTextAlignmentRight;
    [self.bottomView addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bottomView).mas_offset(-10);
        make.centerY.mas_equalTo(self.photoImageView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(17);
    }];

    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.nameLabel.textColor = FWColor(@"121212");
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bottomView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(10);
        make.right.mas_equalTo(self.ETLabel.mas_left).mas_offset(-10);
        make.bottom.mas_equalTo(self.photoImageView).mas_offset(-41/2);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];

    self.carLabel = [[UILabel alloc] init];
    self.carLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.carLabel.textColor = FWColor(@"858585");
    self.carLabel.textAlignment = NSTextAlignmentLeft;
    [self.bottomView addSubview:self.carLabel];
    [self.carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.right.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(0);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(20);
    }];
    
}

- (void)configForView:(id)model WithSection:(NSInteger)section WithRow:(NSInteger)row{
    
    self.sectionIndex = section;
    self.rowIndex = row;
    
    subListModel = (FWScheduleListModel *)model;

    self.titleLabel.text = subListModel.sport_name;

    NSString * carGroup ;

    if (subListModel.best_et.car_group.length > 0) {
        carGroup = subListModel.best_et.car_group;
    }else{
        carGroup = @"个人";
    }
    self.carLabel.text = [NSString stringWithFormat:@"座驾：%@",subListModel.best_et.car_type];
    self.nameLabel.text = [NSString stringWithFormat:@"%@ (%@)",subListModel.best_et.nickname,carGroup];
    self.ETLabel.text = [NSString stringWithFormat:@"ET：%@",subListModel.best_et.et];

    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:subListModel.logo] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:subListModel.best_et.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
}

#pragma mark - > 点击进入比赛
- (void)bgViewClick{
    
    if (self.rowIndex == 0 && self.sectionIndex == 0) {
        self.nearestImageView.hidden = YES;
        [GFStaticData saveObject:subListModel.sport_id forKey:@"sport_id"];
    }

    FWGroupScoreViewController * VDVC = [[FWGroupScoreViewController alloc] init];
    VDVC.sport_id = subListModel.sport_id;
    VDVC.year = subListModel.year;
    VDVC.type = subListModel.type;
    VDVC.rankType = @"allSchedule";
    VDVC.group_id = @"0";
    VDVC.sport_name = subListModel.sport_name;
    [self.vc.navigationController pushViewController:VDVC animated:YES];
    
}
@end
