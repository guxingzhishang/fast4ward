//
//  FWGuessingBettingViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGuessingBettingViewController.h"

@interface FWGuessingBettingViewController ()<UITextViewDelegate>

@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UILabel * currentMatchLabel;
@property (nonatomic, strong) UILabel * currentPeilvLabel;
@property (nonatomic, strong) UILabel * hasSelectLabel;
@property (nonatomic, strong) UILabel * enableGoldenLabel;
@property (nonatomic, strong) UILabel * winGoldenLabel;
@property (nonatomic, strong) IQTextView * touzhuTF;
@property (nonatomic, strong) UILabel * tipLabel;

@property (nonatomic, strong) UIButton * closeButton;
@property (nonatomic, strong) UIButton * confirmButton;

@end

@implementation FWGuessingBettingViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"投主页"];
    [IQKeyboardManager sharedManager].enable = YES;

    self.view.backgroundColor = FWColorWihtAlpha(@"000000", 0.6);
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"投主页"];
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupViews];
}

- (void)setupViews{

    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = FWTextColor_FAFAFA;
    [self.view addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 250+FWSafeBottom));
        make.bottom.left.right.mas_equalTo(self.view);
    }];
    
    self.currentMatchLabel = [[UILabel alloc] init];
    self.currentMatchLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.currentMatchLabel.textColor = FWTextColor_222222;
    self.currentMatchLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.currentMatchLabel];
    [self.currentMatchLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(18);
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-40);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(19);
    }];

    self.closeButton = [[UIButton alloc] init];
    [self.closeButton setImage:[UIImage imageNamed:@"new_search_close"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.closeButton];
    [self.closeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(50, 50));
        make.top.right.mas_equalTo(self.bgView);
    }];
    
    self.currentPeilvLabel = [[UILabel alloc] init];
    self.currentPeilvLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.currentPeilvLabel.textColor = FWTextColor_222222;
    self.currentPeilvLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.currentPeilvLabel];
    [self.currentPeilvLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.currentMatchLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-40);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(19);
    }];
    
    self.hasSelectLabel = [[UILabel alloc] init];
    self.hasSelectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.hasSelectLabel.textColor = FWTextColor_222222;
    self.hasSelectLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.hasSelectLabel];
    [self.hasSelectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.currentPeilvLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-40);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(19);
    }];
    
    
    self.enableGoldenLabel = [[UILabel alloc] init];
    self.enableGoldenLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.enableGoldenLabel.textColor = FWTextColor_BCBCBC;
    self.enableGoldenLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.enableGoldenLabel];
    [self.enableGoldenLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.hasSelectLabel.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-40);
        make.width.mas_equalTo(210);
        make.height.mas_equalTo(19);
    }];
    
    
    self.winGoldenLabel = [[UILabel alloc] init];
    self.winGoldenLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.winGoldenLabel.textColor = FWTextColor_222222;
    self.winGoldenLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.winGoldenLabel];
    [self.winGoldenLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.enableGoldenLabel).mas_offset(0);
        make.right.mas_equalTo(self.bgView).mas_offset(-13);
        make.width.mas_equalTo(170);
        make.height.mas_equalTo(19);
    }];
    
    self.touzhuTF = [[IQTextView alloc] init];
    self.touzhuTF.textContainerInset = UIEdgeInsetsMake(5,5,0,0);
    self.touzhuTF.delegate = self;
    self.touzhuTF.layer.cornerRadius = 2;
    self.touzhuTF.layer.masksToBounds = YES;
    self.touzhuTF.keyboardType = UIKeyboardTypeNumberPad;
    self.touzhuTF.textColor = FWTextColor_222222;
    self.touzhuTF.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.touzhuTF.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.bgView addSubview:self.touzhuTF];
    [self.touzhuTF mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-13);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-26, 31));
        make.top.mas_equalTo(self.enableGoldenLabel.mas_bottom).mas_offset(10);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"*最终会根据车手封盘赔率计算获得金币数量";
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.tipLabel.textColor = FWTextColor_BCBCBC;
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.touzhuTF.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.right.mas_equalTo(self.bgView).mas_offset(-40);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(19);
    }];
    
    self.confirmButton = [[UIButton alloc] init];
    self.confirmButton.layer.cornerRadius = 2;
    self.confirmButton.layer.masksToBounds = YES;
    self.confirmButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.confirmButton.backgroundColor = FWColor(@"ff6f00");
    [self.confirmButton setTitle:@"确认竞猜" forState:UIControlStateNormal];
    [self.confirmButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.confirmButton addTarget:self action:@selector(confirmButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.confirmButton];
    [self.confirmButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 36));
        make.centerX.mas_equalTo(self.bgView);
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(10);
    }];
    
    if ([self.type isEqualToString:@"left"]) {
        self.currentInfo = self.listModel.car_1_info;
    }else{
        self.currentInfo = self.listModel.car_2_info;
    }
    
    if (self.listModel) {
            
        self.touzhuTF.placeholder = [NSString stringWithFormat:@"投注限额%@~%@",self.listModel.min_gold,self.listModel.max_gold];
        self.currentMatchLabel.attributedText = [self attributedString:[NSString stringWithFormat:@"当前竞猜  %@ %@",self.listModel.group_name,self.listModel.phase_name] WithLength:4];
        self.currentPeilvLabel.attributedText =  [self attributedString:[NSString stringWithFormat:@"当前赔率  %@",self.currentInfo.odds] WithLength:4];
        self.hasSelectLabel.attributedText =  [self attributedString:[NSString stringWithFormat:@"已选  %@",self.currentInfo.nickname] WithLength:2];
        self.enableGoldenLabel.text =  [NSString stringWithFormat:@"可用金币  %@",self.gold_balance_value];
        self.winGoldenLabel.attributedText =  [self attributedString:[NSString stringWithFormat:@"预计可得  %@",@"--"] WithLength:4];
    }
}

#pragma mark - > 提交
- (void)confirmButtonClick{
    
    self.confirmButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.confirmButton.enabled = YES;
    });
    
    if (self.currentInfo.car_no.length < 0 ||
        self.currentInfo.index.length < 0 ||
        self.listModel.guess_id.length < 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"缺少相关数据，请退出直播间，重新进入" toController:self];
        return;
    }
    
    if(![self isPureInt:self.touzhuTF.text]){
        [[FWHudManager sharedManager] showErrorMessage:@"含非法字符，请输入纯数字" toController:self];
        return;
    }
    
    if ([self.touzhuTF.text length] <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入投注数量" toController:self];
        return;
    }
    
    
    if ([self.touzhuTF.text intValue] < [self.listModel.min_gold intValue]) {
        [[FWHudManager sharedManager] showErrorMessage:[NSString stringWithFormat:@"最少投注%@个金币",self.listModel.min_gold] toController:self];
        return;
    }
    
    if ([self.touzhuTF.text intValue] > [self.listModel.max_gold intValue]) {
        [[FWHudManager sharedManager] showErrorMessage:[NSString stringWithFormat:@"最多投注%@个金币",self.listModel.max_gold] toController:self];
        return;
    }
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"guess_id":self.listModel.guess_id,
                                @"gold_value":self.touzhuTF.text,
                                @"index":self.currentInfo.index,
                                @"car_no":self.currentInfo.car_no,
                            };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Submit_do_sport_guess WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [[FWHudManager sharedManager] showErrorMessage:@"已投注" toController:self];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.myBlock(self.listModel.guess_id);
                [self dismissViewControllerAnimated:YES completion:nil];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (NSMutableAttributedString *)attributedString:(NSString *)text WithLength:(NSInteger)length{
    
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attrStr addAttribute: NSForegroundColorAttributeName value: FWColor(@"ff6f00") range: NSMakeRange(length, attrStr.length-length)];
    return attrStr;
}

- (void)closeButtonClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
   
    if ([text isEqualToString:@""]) {
        return YES;
    }
    
    if(![self isPureInt:text]) {
        return NO;
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{

    if ([textView.text isEqualToString:@""]) {
        return;
    }
    
    if(![self isPureInt:textView.text] ){
        [[FWHudManager sharedManager] showErrorMessage:@"含非法字符，请输入纯数字" toController:self];
        return;
    }
    
    if ([textView.text intValue] < [self.listModel.min_gold intValue]) {
        [[FWHudManager sharedManager] showErrorMessage:[NSString stringWithFormat:@"最少投注%@个金币",self.listModel.min_gold] toController:self];
        return;
    }
    
    if ([textView.text intValue] > [self.listModel.max_gold intValue]) {
        [[FWHudManager sharedManager] showErrorMessage:[NSString stringWithFormat:@"最多投注%@个金币",self.listModel.max_gold] toController:self];
        return;
    }
    if ([textView.text integerValue] > 0) {
        self.winGoldenLabel.attributedText =  [self attributedString:[NSString stringWithFormat:@"预计可得  %@",@(floorf([textView.text  integerValue] * [self.currentInfo.odds floatValue])).stringValue] WithLength:4];
    }
}

//判断是否为整形：
- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

@end
