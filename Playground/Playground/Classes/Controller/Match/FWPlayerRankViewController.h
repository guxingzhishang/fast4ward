//
//  FWPlayerRankViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 车手排名
 */
#import <UIKit/UIKit.h>
#import "WMPageController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerRankViewController : WMPageController<UIPickerViewDataSource,UIPickerViewDelegate>

@end

NS_ASSUME_NONNULL_END
