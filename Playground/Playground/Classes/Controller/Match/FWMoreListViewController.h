//
//  FWMoreListViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 更多榜单
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@interface FWRankSortListModel : NSObject

@property (nonatomic, strong) NSString * year;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * img;
@property (nonatomic, strong) NSString * name;

@end

@interface FWRankSortModel : NSObject

@property (nonatomic, strong) NSString * top_img_url;
@property (nonatomic, strong) NSMutableArray<FWRankSortListModel *> * list;

@end

@interface FWMoreListViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
