//
//  FW02ListViewController.m
//  
//
//  Created by 孤星之殇 on 2019/9/26.
//

#import "FW02ListViewController.h"
#import "FWPlayerRankModel.h"

@interface FW02ListViewController ()

@property (nonatomic, assign) NSInteger  lastIndex;// 最后一次点击的segement
@property (nonatomic, strong) FWPlayerRankModel * scoreModel;
@property (nonatomic, strong) NSMutableArray * groupArray;

@end

@implementation FW02ListViewController
@synthesize infoTableView;
@synthesize mineView;
@synthesize totalSegement;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 请求分组
- (void)requestTeam{

    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"year":self.year,
                              @"sport_id":self.sport_id,
                              @"type":self.type,
                              @"group_id":self.group_id,
                              @"page_size":@"50",
                              @"page":@"1",
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_sport_rank_group WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {

            self.scoreModel = [FWPlayerRankModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            NSMutableArray * titleArr = @[].mutableCopy;

            if (self.groupArray.count > 0) {
                [self.groupArray removeAllObjects];
            }
            
            for (FWGroupListModel * listModel  in self.scoreModel.group_list) {
                [self.groupArray addObject:listModel.group_info];
                [titleArr addObject:listModel.group_info.group_name];
            }
            [self.totalSegement deliverTitles:titleArr];
            self.totalSegement.lastIndex = self.lastIndex;
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 根据筛选获取数据
- (void)requestData:(BOOL)isLoadMoreData{
    
    if (isLoadMoreData) {
        self.pageNum += 1;
    }else{
        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
        self.pageNum = 1;
    }
    
    if ([self.group_id isEqualToString:@"0"]) {
       self.group_id = @"cfcd208495d565ef66e7dff9f98764da";
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"year":self.year,
                              @"sport_id":self.sport_id,
                              @"type":self.type,
                              @"group_id":self.group_id,
                              @"page_size":@"50",
                              @"page":@(self.pageNum).stringValue,
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_sport_rank_group WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.rankModel = [FWPlayerRankModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            NSArray * scoreList = self.rankModel.group_list[0].score_list;
            [self.dataSource addObjectsFromArray:scoreList];
            
            if (isLoadMoreData) {
                [self.infoTableView.mj_footer endRefreshing];
            }else{
                [self.infoTableView.mj_header endRefreshing];
            }
            
            if (self.rankModel.driver_rank_item) {
                
                self.mineView.hidden = NO;
                [self.mineView configViewForModel:self.rankModel.driver_rank_item];
                
                [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(self.view);
                    make.top.mas_equalTo(self.totalSegement.mas_bottom);
                    make.bottom.mas_equalTo(-77);
                }];
                [self.mineView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.left.right.mas_equalTo(self.view);
                    make.size.mas_equalTo(CGSizeMake(ScreenWidth, 77));
                    make.top.mas_equalTo(self.infoTableView.mas_bottom);
                }];
                
                self.mineView.numberLabel.textAlignment = NSTextAlignmentRight;
                [self.mineView.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                   make.left.mas_equalTo(self.mineView).mas_offset(0);
                   make.centerY.mas_equalTo(self.mineView);
                   make.height.mas_equalTo(40);
                   make.width.mas_equalTo(40);
                }];
            }else{
                self.mineView.hidden = YES;
                [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(self.view);
                    make.top.mas_equalTo(self.totalSegement.mas_bottom);
                    make.bottom.mas_equalTo(self.view);
                }];
            }
            
            if([scoreList count] == 0 &&
               self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [self.infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"02总榜页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"02总榜页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageNum = 1;
    self.groupArray = @[].mutableCopy;

    self.year = @"0";
    self.type = @"02";
    self.sport_id = @"0";
    self.group_id = @"0";
    
    [self setupNaviView];
    [self setupSubViews];
    
    [self requestTeam];
    [self requestData:NO];
}

- (void)setupNaviView{
    
    totalSegement = [[FWTotalListSegementView alloc] init];
    totalSegement.itemDelegate = self;
    totalSegement.frame = CGRectMake(0,0, SCREEN_WIDTH, 40);
    [self.view addSubview:self.totalSegement];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 77;
    infoTableView.backgroundColor = FWTextColor_F8F8F8;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(totalSegement.mas_bottom);
        make.bottom.mas_equalTo(self.view);
    }];
    
    DHWeakSelf;
    self.infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestData:NO];
    }];
    self.infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestData:YES];
    }];
    
    mineView = [[FWMyScoreView alloc] init];
    mineView.backgroundColor = FWColor(@"FFFFFF");
    [self.view addSubview:mineView];
    [mineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(infoTableView.mas_bottom);
    }];
    mineView.hidden = YES;
    
    UITapGestureRecognizer * Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mineViewClick)];
    [mineView addGestureRecognizer:Tap];
}

#pragma mark - > tableview`s delegate & datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWGroupScoreCellID";
    
    FWGroupScoreCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWGroupScoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.numberLabel.text =@(indexPath.row+1).stringValue;
    
    if (self.dataSource.count > indexPath.row) {
        [cell configCellForModel:self.dataSource[indexPath.row] withIndex:indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.dataSource.count) {
        FWPlayerScoreDetailViewController * DVC = [[FWPlayerScoreDetailViewController alloc] init];
        DVC.ScoreModel = self.dataSource[indexPath.row];
        [self.navigationController pushViewController:DVC animated:YES];
    }
}

- (void)mineViewClick{
    
    FWPlayerScoreDetailViewController * DVC = [[FWPlayerScoreDetailViewController alloc] init];
    DVC.ScoreModel = self.rankModel.driver_rank_item;
    [self.navigationController pushViewController:DVC animated:YES];
}

#pragma mark - > 点击推荐item
- (void)segementItemClickTap:(NSInteger)index{
    
    if (self.lastIndex == index) {
        return;
    }
    
    self.lastIndex = index;
    
    if (self.groupArray.count > index) {
        
        FWGroupInfoModel * infoModel = self.groupArray[index];
        self.group_id = infoModel.group_id;
        [self requestData:NO];
    }
}

@end
