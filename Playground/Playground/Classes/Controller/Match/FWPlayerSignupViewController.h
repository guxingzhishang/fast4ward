//
//  FWPlayerSignupViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWPlayerSignupModel.h"
#import "FWRegistrationHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerSignupViewController : FWBaseViewController

@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, strong) NSString * goods_id;
@property (nonatomic, strong) FWPlayerSignupModel * signupModel;
@property (nonatomic, assign) BOOL isShowAlert;
@end

NS_ASSUME_NONNULL_END
