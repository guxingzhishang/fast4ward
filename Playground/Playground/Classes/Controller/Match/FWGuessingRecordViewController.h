//
//  FWGuessingRecordViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 精彩记录
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGuessingRecordViewController : FWBaseViewController

@property (nonatomic, strong) NSString * sport_id;


@end

NS_ASSUME_NONNULL_END
