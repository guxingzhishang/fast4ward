//
//  FWPlayerWonderfulVideoViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerWonderfulVideoViewController.h"
#import "ZFPlayer.h"
#import "ZFAVPlayerManager.h"
#import "ZFIJKPlayerManager.h"
#import "KSMediaPlayerManager.h"
#import "UIImageView+ZFCache.h"
#import "ZFUtilities.h"
#import <AVFoundation/AVFoundation.h>
#import "ZFPlayerControlView.h"
#import "FWHotLiveShareViewController.h"
#import "FWLiveTipView.h"
#import "XLCircleProgress.h"

@interface FWPlayerWonderfulVideoViewController ()<NSURLSessionDelegate>

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) UIImageView *containerView;
@property (nonatomic, strong) ZFPlayerControlView *controlView;
@property (nonatomic, strong) UIButton *playBtn;
@property (nonatomic, strong) NSArray <NSURL *>*assetURLs;
@property (nonatomic, strong) ZFAVPlayerManager *playerManager;
/* 最近一次的网络状态  wifi 还是 4G */
@property (nonatomic, strong) NSString * lastNetWork;
@property (nonatomic, strong) FWLiveTipView * tipView;

@property (nonatomic, strong) UIButton * closeButton;
@property (nonatomic, strong) UIButton * downloadButton;
@property (nonatomic, strong) XLCircleProgress *progressView;
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;

@end

@implementation FWPlayerWonderfulVideoViewController

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.player.viewControllerDisappear = NO;
    [self trackPageBegin:@"车手精彩视频页"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.player.viewControllerDisappear = YES;
     [self trackPageEnd:@"车手精彩视频页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.containerView];
    
    [self.containerView addSubview:self.playBtn];
    
    [self setupPlayer];
    [self setupTopView];
    
    [self checkWifi];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(monitorLiveNet) name:MonitorLiveNetworking object:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat w = CGRectGetWidth(self.view.frame);
    CGFloat h = CGRectGetHeight(self.view.frame);
    self.containerView.frame = CGRectMake(x, y, w, h);
    
    w = 44;
    h = w;
    x = (CGRectGetWidth(self.containerView.frame)-w)/2;
    y = (CGRectGetHeight(self.containerView.frame)-h)/2;
    self.playBtn.frame = CGRectMake(x, y, w, h);
}

#pragma mark - > 播放器相关配置
- (void)setupPlayer{
    self.playerManager= [[ZFAVPlayerManager alloc] init];
    self.playerManager.scalingMode = ZFPlayerScalingModeAspectFit;
    
    /// 播放器相关
    self.player = [ZFPlayerController playerWithPlayerManager:self.playerManager containerView:self.containerView];
    self.player.controlView = self.controlView;
    /// 设置退到后台继续播放
    self.player.pauseWhenAppResignActive = NO;
    
    self.player.customAudioSession = YES;
    self.player.playerReadyToPlay = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSURL * _Nonnull assetURL) {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:nil];
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
    };
}

#pragma mark - > 设置顶部视图
- (void)setupTopView{
    
    self.progressView = [[XLCircleProgress alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    self.progressView.center = self.view.center;
    [DHWindow addDHCustomSubView:self.progressView priority:10000];
    self.progressView.hidden = YES;
    
    self.closeButton = [[UIButton alloc] init];
    self.closeButton.frame = CGRectMake(0, FWCustomeSafeTop +14, 50, 50);
    [self.closeButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
    
    self.downloadButton = [[UIButton alloc] init];
    [self.downloadButton setImage:[UIImage imageNamed:@"download"] forState:UIControlStateNormal];
    [self.downloadButton addTarget:self action:@selector(downloadButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.downloadButton];
    [self.downloadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.closeButton);
        make.size.mas_equalTo(CGSizeMake(50, 50));
        make.right.mas_equalTo(self.view).mas_offset(0);
    }];
}



#pragma mark - > 关闭当前页面
- (void)closeButtonClick{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
        [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] setIsLogin:NO];
    });
}

#pragma mark - > 用户单机屏幕，收键盘
- (void)singleTap{
    [self.view endEditing:YES];
}

- (void)playClick:(UIButton *)sender {
    [self.player playTheIndex:0];
    [self.controlView showTitle:@"" coverURLString:self.videoCover fullScreenMode:ZFFullScreenModeAutomatic];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
        _controlView.prepareShowLoading = YES;
        _controlView.fullScreenOnly = NO;
        _controlView.fastViewAnimated = YES;
        _controlView.autoHiddenTimeInterval = 5;
        _controlView.autoFadeTimeInterval = 0.5;
        _controlView.prepareShowLoading = YES;
    }
    return _controlView;
}

- (UIImageView *)containerView {
    if (!_containerView) {
        _containerView = [UIImageView new];
        [_containerView setImageWithURLString:self.videoCover placeholder:[ZFUtilities imageWithColor:[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1] size:CGSizeMake(1, 1)]];
    }
    return _containerView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setImage:[UIImage imageNamed:@"new_allPlay_44x44_"] forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(playClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playBtn;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.player.currentPlayerManager pause];
    [self.playerManager pause];
    
    [self.view endEditing:YES];
}

#pragma mark - > 监听当前网络状态
- (void)monitorLiveNet{
    
//    [self checkWifi];
}

- (void)checkWifi{
    
    if ([[GFStaticData getObjectForKey:Current_Network] isEqualToString:@"WIFI"]) {
        
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            [self playVideo];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }else{
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            if (![[GFStaticData getObjectForKey:Allow_4G_Play] boolValue]) {
                [self playVideo];
                
                self.tipView.hidden = NO;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.player.currentPlayerManager pause];
                    [self.playerManager pause];
                });
            }else{
                self.tipView.hidden = YES;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }
}

#pragma mark - > 4G下 点击继续观看
- (void)continueLiveMatch{
    
    self.tipView.hidden = YES;
    
    self.lastNetWork = [GFStaticData getObjectForKey:Last_Network];
    
    if ([self.lastNetWork isEqualToString:@"WIFI"] && ![self.lastNetWork isEqualToString:[GFStaticData getObjectForKey:Current_Network]]) {
        // 由wifi切换到4G ，继续播放
        [self.player.currentPlayerManager play];
    }else{
        // 进来直接是4G
        [self playVideo];
    }
    
    [GFStaticData saveObject:@"WIFI" forKey:Current_Network];
    [GFStaticData saveObject:@"1" forKey:Allow_4G_Play];
}

- (void)playVideo{
    self.assetURLs = @[[NSURL URLWithString:self.videoURL]];
    self.player.assetURLs = self.assetURLs;
    [self.player playTheIndex:0];
}

#pragma mark - > 下载
- (void)downloadButtonClick{

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    self.downloadTask = [session downloadTaskWithURL:[NSURL URLWithString:self.videoURL]];
    self.progressView.hidden = NO;
    
    [self.downloadTask resume];
}


#pragma mark NSSessionUrlDelegate
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    //下载进度
    CGFloat progress = totalBytesWritten / (double)totalBytesExpectedToWrite;
    dispatch_async(dispatch_get_main_queue(), ^{
        //进行UI操作  设置进度条
        self.progressView.progress = progress;
    });
}
//下载完成 保存到本地相册
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    //1.拿到cache文件夹的路径
    NSString *cache=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject];
    //2,拿到cache文件夹和文件名
    NSString *file=[cache stringByAppendingPathComponent:downloadTask.response.suggestedFilename];
    
    [[NSFileManager defaultManager] moveItemAtURL:location toURL:[NSURL fileURLWithPath:file] error:nil];
    //3，保存视频到相册
    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(file)) {
        //保存相册核心代码
        UISaveVideoAtPathToSavedPhotosAlbum(file, self, nil, nil);
    }
    
    [self progressOverAndChangeViewContents];
}

//控件本身的代理方法  更新控件样子
- (void)progressOverAndChangeViewContents {
    
    self.progressView.hidden = YES;
    [[FWHudManager sharedManager] showSuccessMessage:@"视频已成功保存至相册" toController:self];
    
    [DHWindow removeSubView:self.progressView];
}

@end
