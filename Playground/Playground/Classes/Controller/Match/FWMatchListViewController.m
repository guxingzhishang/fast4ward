//
//  FWMatchListViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//


#import "FWMatchListViewController.h"
#import "FWMatchListCell.h"
#import "FWLiveViewController.h"
#import "FWRealScroeAndRankViewController.h"

@interface FWMatchListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger pageNum;

@property (nonatomic, strong) FWF4WMatchLiveModel * matchModel;
@end

@implementation FWMatchListViewController
@synthesize infoTableView;
@synthesize pageNum;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 请求数据
- (void)requestLoadMoredData:(BOOL)loadMoredData{
   
    if (loadMoredData) {
        self.pageNum += 1;
    }else{
        
        self.pageNum = 1;

        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page_size":@"20",
                              @"page":@(self.pageNum).stringValue,
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_sport_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            NSLog(@"self.dataSource==%@",self.dataSource);
            
            if (loadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            self.matchModel = [FWF4WMatchLiveModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.dataSource addObjectsFromArray:self.matchModel.list];
            
            if([self.matchModel.list count] == 0 && self.pageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [infoTableView.mj_footer endRefreshing];
            [infoTableView.mj_header endRefreshing];

            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"赛事列表"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"赛事列表"];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.pageNum = 1;
    self.title = @"赛事列表";
    [self setupSubViews];
    [self requestLoadMoredData:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 86;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    infoTableView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_FFFFFF;

    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestLoadMoredData:YES];
    }];
    
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
    infoTableView.tableFooterView = footView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count?self.dataSource.count:0;;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"ListCellID";
    
    FWMatchListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWMatchListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if (self.dataSource.count > 0) {
        [cell configForCell:self.dataSource[indexPath.row]];
    
        if (indexPath.row == self.dataSource.count-1) {
            cell.lineView.hidden =YES;
        }else{
            cell.lineView.hidden =NO;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.dataSource.count <= indexPath.row) {
        return;
    }
    
    FWF4WMatchLiveListModel * liveModel = self.dataSource[indexPath.row];
    

    if (liveModel.list_replay[0].replay_url.length > 0) {
        /* 有直播 */
        FWLiveViewController * LVC = [[FWLiveViewController alloc] init];
        LVC.isLive = NO;
        LVC.liveListModel = liveModel;
        LVC.sport_id = liveModel.sport_id;
        LVC.roomId = liveModel.list_replay[0].chatroom_id;
        LVC.currentIndex = 0;
        [self.navigationController pushViewController:LVC animated:YES];
    }else{
        FWRealScroeAndRankViewController * RARVC = [[FWRealScroeAndRankViewController alloc] init];
        RARVC.sport_id = liveModel.sport_id;
        RARVC.share_image = liveModel.list_replay[0].img_url;
        [self.navigationController pushViewController:RARVC animated:YES];
    }
}

@end
