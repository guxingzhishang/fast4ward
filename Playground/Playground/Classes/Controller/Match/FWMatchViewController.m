//
//  FWMatchViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchViewController.h"
#import "FWMatchView.h"
#import "FWCustomNavigationView.h"

@interface FWMatchViewController ()

@property (nonatomic, strong) FWMatchView * matchView;
@property (nonatomic, strong) FWF4WMatchModel * matchModel;
@property (nonatomic, strong) NSMutableArray * allDataSource;
@property (nonatomic, assign) NSInteger   allPageNum;
@property (nonatomic, strong) FWCustomNavigationView * navigationView;

@end

@implementation FWMatchViewController
@synthesize allDataSource;
@synthesize allPageNum;

#pragma mark - > 数据请求
- (void)requestAllListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        allPageNum = 1;
        
        if (self.allDataSource.count > 0 ) {
            [self.allDataSource removeAllObjects];
        }
    }else{
        allPageNum +=1;
    }
    
    FWMatchRequest * request = [[FWMatchRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(allPageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_sport_index WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.matchModel = [FWF4WMatchModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.matchView.mj_header endRefreshing];
            
            [self.allDataSource addObjectsFromArray: self.matchModel.feed_list];
            
            [self.matchView congifViewWithModel:self.matchModel withRefitListModel:self.matchModel.refit_list_keys];
        }else{
            [self.matchView.mj_header endRefreshing];
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = FWTextColor_FAFAFA;
    [self trackPageBegin:@"赛事主页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"赛事主页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;
    self.fd_prefersNavigationBarHidden = YES;
    
    allPageNum = 0;
    allDataSource = @[].mutableCopy;
    
    [self setupTopView];
    [self setupSubViews];
    [self requestAllListWithLoadMoreData:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PushToPublishViewController) name:kTagPushPublish object:nil];
}

#pragma mark - > 初始化视图
- (void)setupTopView{
    
    self.navigationView = [[FWCustomNavigationView alloc] init];
    self.navigationView.navigationImageView.image = [UIImage imageNamed:@"match_fast4ward"];
    self.navigationView.navigationLabel.hidden = YES;
    self.navigationView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    [self.view addSubview:self.navigationView];
    
    [self.navigationView.navigationImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(136, 15));
        make.left.mas_equalTo(self.navigationView).mas_offset(14);
        make.bottom.mas_equalTo(self.navigationView).mas_offset(-14);
    }];
}
#pragma mark - > 初始化视图
- (void)setupSubViews{

    self.matchView = [[FWMatchView alloc] init];
    self.matchView.vc = self;
    [self.view addSubview:self.matchView];
    [self.matchView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(self.navigationView.mas_bottom);
        make.bottom.mas_equalTo(self.view).mas_offset(-49-FWSafeBottom);
    }];

    DHWeakSelf;
    self.matchView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestAllListWithLoadMoreData:NO];
    }];
//
//        @weakify(self);
//        self.allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            @strongify(self);
//            [self requestAllListWithLoadMoreData:YES];
//        }];
//    }
}


#pragma mark - > 跳转到发布页
- (void)PushToPublishViewController{
    
    if (![((UINavigationController *)self.tabBarController.selectedViewController).topViewController isEqual:self]
        ) {
        return;
    }
    

    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self];
        }
    }else {
        [self cheackLogin];
        
        if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
            ![@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
            
            FWPublishViewController * vc = [[FWPublishViewController alloc] init];
            vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
            vc.fd_prefersNavigationBarHidden = YES;
            vc.navigationController.navigationBar.hidden = YES;
            
            CATransition *animation = [CATransition animation];
            [animation setDuration:0.4];
            [animation setType: kCATransitionMoveIn];
            [animation setSubtype: kCATransitionFromTop];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            [self.navigationController pushViewController:vc animated:NO];
            [self.navigationController.view.layer addAnimation:animation forKey:nil];
        }
    }
}

#pragma mark - > 检测是否登录
- (void)cheackLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        /* 登录 */
        [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        
        return;
    }
}
@end
