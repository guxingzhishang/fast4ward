
//
//  FWRankViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/8.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWRankViewController.h"
#import "FWMatchModel.h"
#import "FWMatchRequest.h"
#import "FWPreRankCell.h"
#import "FWFinalsRankView.h"
#import "FWPreMyScoreView.h"


@interface FWRankViewController ()

@property (nonatomic, strong) FWFinalsRankView * finalsView;
@property (nonatomic, strong) NSMutableArray * twoTitleArray;

@property (nonatomic, assign) NSInteger firstSelect;
@property (nonatomic, assign) NSInteger secondSelect;

@property (nonatomic, strong) FWPreMyScoreView * myScoreView;


@end

@implementation FWRankViewController
@synthesize matchModel;
@synthesize preRankModel;
@synthesize finalRankModel;
@synthesize infoTableView;
@synthesize finalsView;

- (NSMutableArray *)twoTitleArray{
    if (!_twoTitleArray) {
        _twoTitleArray = [[NSMutableArray alloc] init];
    }
    
    return _twoTitleArray;
}

#pragma mark - > 获取比赛分组
- (void)requestTeam{
    
    FWMatchRequest * request = [[FWMatchRequest alloc] init];
    
    if (!self.sport_id) return;
        
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"sport_id":self.sport_id,
                              };
    
    [request startWithParameters:params WithAction:Get_sport_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            matchModel = [FWMatchModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            if (matchModel.category[0].list <= 0) {
                /* 预赛没有分组 */
                return ;
            }

            NSMutableArray * tempArr = @[].mutableCopy;
            // 获取第0列数据
            for (FWMatchCategoryModel * model in matchModel.category) {
                NSString * ScheduleItemName = model.info.ScheduleItemName?model.info.ScheduleItemName:@"";
                [tempArr addObject:ScheduleItemName];
            }

            /* 预赛，第0列 index = 0
               决赛  第0列 index = 1 */
            
//            if (matchModel.category[1].list <= 0) {
//                /* 决赛没有分组 */
//                return ;
//            }

            NSInteger index = 0;
//            if ([matchModel.current_type isEqualToString:@"F"]) {
//                index = 1;
//            }
            
            /********* 获取第1列数据(从0开始) *******/
            FWMatchCategoryModel * categoryModel = matchModel.category[index];
            NSMutableArray * tempTowArr = @[].mutableCopy;
            for (FWMatchListModel * listModel in categoryModel.list) {
                NSString * ScheduleItemName = listModel.ScheduleItemName?listModel.ScheduleItemName:@"";
                [tempTowArr addObject:ScheduleItemName];
            }
            /********* 获取第1列数据 *******/
            
            if (tempArr.count > 0){
                self.pickerView.oneTitleArray = [NSMutableArray arrayWithArray:tempArr];
            
                NSString * selectString = [NSString stringWithFormat:@"%@",tempTowArr[0]];
                [self selectType:selectString withFirstRow:index withSecondRow:0];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 预赛和决赛排名
- (void)requestMatchSortWithDictionary:(NSDictionary *)dict WithType:(NSString *)type{
    
    FWMatchRequest * request = [[FWMatchRequest alloc] init];
    
    [request startWithParameters:dict WithAction:Get_match_sort WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (self.twoTitleArray.count > 0) {
                [self.twoTitleArray removeAllObjects];
            }
            
            for (int i =0; i<matchModel.category.count; i++) {
                
                FWMatchCategoryModel * categoryModel = matchModel.category[i];
                NSMutableArray * tempArr = @[].mutableCopy;
                
                for (FWMatchListModel * listModel in categoryModel.list) {
                    NSString * ScheduleItemName = listModel.ScheduleItemName?listModel.ScheduleItemName:@"";
                    [tempArr addObject:ScheduleItemName];
                }
                
                [self.twoTitleArray addObject:tempArr];
            }
            
            if ([type isEqualToString:@"P"]) {
                // 预赛
                finalsView.hidden = YES;
                self.infoTableView.hidden = NO;
                
                preRankModel = [FWPreRankModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                if (preRankModel.live_user_count.length > 0) {
                    self.myBlock(preRankModel.live_user_count);
                }
                
                if ([preRankModel.show_my_score isEqualToString:@"1"]) {
                    self.myScoreView.hidden = NO;
                    [self.myScoreView configForView:preRankModel  withMyCarNo:preRankModel.my_car_no];
                    [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.mas_equalTo(self.view);
                        make.bottom.mas_equalTo(self.view).mas_offset(-(72+FWSafeBottom));
                        make.top.mas_equalTo(self.titleButton.mas_bottom).mas_offset(10);
                    }];
                }else{
                    self.myScoreView.hidden = YES;
                    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.bottom.mas_equalTo(self.view);
                        make.top.mas_equalTo(self.titleButton.mas_bottom).mas_offset(10);
                    }];
                }
                
                [self.infoTableView reloadData];
            }else{
                finalsView.hidden = NO;
                self.myScoreView.hidden = YES;
                self.infoTableView.hidden = YES;
                
                finalRankModel = [FWFinalRankModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                if (finalRankModel.live_user_count.length > 0) {
                    self.myBlock(finalRankModel.live_user_count);
                }
                [self.finalsView configForViewWithData:finalRankModel];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手比赛排名页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手比赛排名页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // 禁止屏幕点击
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        // 允许屏幕点击
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    });
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.fd_prefersNavigationBarHidden = YES;
    
    [self setupPickerView];
    [self setupTableView];
    
    self.firstSelect = 0;
    self.secondSelect = 0;
    
    [self requestTeam];
}

#pragma mark - > 视图初始化
- (void)setupPickerView{

    self.pickerView = [[FWPickerTypeView alloc] init];
    self.pickerView.pickerType = FWDoublePickerViewType;
    self.pickerView.delegate = self;
    
    self.titleButton = [[UIButton alloc] init];
    self.titleButton.layer.cornerRadius = 20;
    self.titleButton.layer.masksToBounds = YES;
    self.titleButton.backgroundColor = FWBgColor_Lightgray_F7F8FA;
    self.titleButton.titleLabel.font = DHSystemFontOfSize_14;
    [self.titleButton setTitle:@"请选择小组" forState:UIControlStateNormal];
    [self.titleButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    [self.titleButton addTarget:self action:@selector(pickViewShow) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.titleButton];

    [self.titleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(270, 40));
    }];
    
    UIImageView * downImageView = [[UIImageView alloc] init];
    downImageView.image = [UIImage imageNamed:@"rank_down"];
    [self.titleButton addSubview:downImageView];
    [downImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.titleButton);
        make.size.mas_equalTo(CGSizeMake(10, 6));
        make.right.mas_equalTo(self.titleButton).mas_offset(-10);
    }];
    
    
    finalsView = [[FWFinalsRankView alloc] init];
    finalsView.vc = self;
    [self.view addSubview:finalsView];
    [finalsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.titleButton.mas_bottom).mas_offset(10);
    }];
    finalsView.hidden = YES;
}

- (void)setupTableView{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 80;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.titleButton.mas_bottom).mas_offset(10);
    }];
    infoTableView.isNeedEmptyPlaceHolder = YES;
    infoTableView.verticalOffset = -50;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    
    infoTableView.hidden = YES;
    
    self.myScoreView = [[FWPreMyScoreView alloc] init];
    self.myScoreView.vc = self;
    self.myScoreView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.myScoreView];
    [self.myScoreView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 72+FWSafeBottom));
    }];
    self.myScoreView.hidden = YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return preRankModel.list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWPreRankCellID";
    
    FWPreRankCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWPreRankCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.vc = self;

    if (preRankModel.list.count > 0) {
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        [cell configForCell:preRankModel.list[indexPath.row] WithIndexRow:indexPath.row];
    }
    
    return cell;
}

#pragma mark - > 弹出选择窗
- (void)pickViewShow{

    [self.pickerView show];
    
    [self.pickerView.lzPickerView selectRow:self.firstSelect inComponent:0 animated:YES];
    [self.pickerView.lzPickerView selectRow:self.secondSelect inComponent:1 animated:YES];
}

- (void)FWPickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row component:(NSInteger)component{

    if (component == 0) {
        
        if (self.twoTitleArray.count >0) {
            self.pickerView.twoTitleArray = [NSMutableArray arrayWithArray:self.twoTitleArray[row]];
            [pickerView reloadComponent:1];
        }
    }
}

- (void)selectType:(NSString *)selectTitle withFirstRow:(NSInteger)firstRow withSecondRow:(NSInteger)secondRow{
   
    self.firstSelect =  firstRow;
    self.secondSelect = secondRow;
    
    FWMatchCategoryModel *categoryModel = self.matchModel.category[firstRow];
    FWMatchListModel *model = categoryModel.list[secondRow];

    NSString *sport_id = self.matchModel.sport_id;
    NSString * type = categoryModel.type?categoryModel.type:@"";
    NSString * group_id = model.group_id?model.group_id:@"";
    NSString * father_phase_id = categoryModel.info.PhaseID?categoryModel.info.PhaseID:@"";
    NSString * phase_id = model.PhaseID?model.PhaseID:@"";


    NSDictionary * dict = @{
                            @"sport_id":sport_id,
                            @"father_phase_id":father_phase_id,
                            @"phase_id":phase_id,
                            @"page":@"1",
                            @"page_size":@"100",
                            @"group_id":group_id,
                            };
    
    if (self.twoTitleArray.count > 0){
        NSArray * titleArray = self.twoTitleArray[firstRow];

        selectTitle = titleArray[secondRow];
    }
    
    [self.titleButton setTitle:selectTitle forState:UIControlStateNormal];
    
    if ([type isEqualToString:@"P"]) {
        
        // 预赛
        finalsView.hidden = YES;
        self.infoTableView.hidden = NO;
    }else if ([type isEqualToString:@"F"]){
        
        //决赛
        finalsView.hidden = NO;
        self.infoTableView.hidden = YES;
    }
    
    [self requestMatchSortWithDictionary:dict WithType:type];
}


@end
