//
//  FWMySignupViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMySignupViewController.h"
#import "FWPlayerSignupModel.h"
#import "FWPerfectInfoModel.h"
#import "FWRegistrationHomeModel.h"
#import "FWMySignupCell.h"
#import "FWRegistrationListModel.h"
#import "FWJoinADPlanWebViewController.h"
#import "FWCarPlayerADPlanViewController.h"

@interface FWMySignupViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWWechatOrderModel * wechatModel ;

@property (nonatomic, strong) NSMutableArray * dataSource;

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, strong) UIView * headerView;
@property (nonatomic, strong) UIImageView * adView;

@property (nonatomic, strong) FWRegistrationListModel * listModel;
@property (nonatomic, assign) NSInteger  pageNum;

@end

@implementation FWMySignupViewController

- (void)requestData:(BOOL)isLoadMoreData{
    
    if (isLoadMoreData) {
        self.pageNum += 1;
    }else{
        self.pageNum = 1;
        
        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_my_baoming_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
     
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if (isLoadMoreData) {
            [self.infoTableView.mj_footer endRefreshing];
        }else{
            self.infoTableView.mj_footer.hidden = NO;
            [self.infoTableView.mj_header endRefreshing];
        }
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.listModel = [FWRegistrationListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.dataSource addObjectsFromArray:self.listModel.list];
            
            if (self.dataSource.count > 0 && self.pageNum == 1) {
                self.baoming_user_id = ((FWRegistrationHomeModel *)self.dataSource[0]).baoming_user_id;
            }
            
            if (self.listModel.ad_plan_img.length > 0) {
                self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 20+ 66*(SCREEN_WIDTH-28)/347);
                self.adView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28,  66*(SCREEN_WIDTH-28)/347);
                self.infoTableView.tableHeaderView= self.headerView;
                
                [self.adView sd_setImageWithURL:[NSURL URLWithString:self.listModel.ad_plan_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            }else{
                self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.01);
                self.adView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28,  0.01);
            }
            
            if([self.listModel.list count] == 0 && self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.infoTableView reloadData];
            }
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"我的报名页"];
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    if (self.baoming_user_id.length > 0) {
        [self requestData:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"我的报名页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"我的报名";
    self.pageNum = 1;

    self.dataSource = @[].mutableCopy;
    
    [self setupSubViews];
    
    [self requestData:NO];
}

- (void)setupSubViews{
    
    self.infoTableView = [[FWTableView alloc] init];
    self.infoTableView.delegate = self;
    self.infoTableView.dataSource = self;
    self.infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.infoTableView.showsVerticalScrollIndicator = NO;
    self.infoTableView.estimatedRowHeight = 100;
    self.infoTableView.rowHeight = UITableViewAutomaticDimension;
    self.infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.infoTableView];
    [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    
    self.headerView = [[UIView alloc] init];
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.01);

    self.adView = [[UIImageView alloc] init];
    self.adView.clipsToBounds = YES;
    self.adView.userInteractionEnabled = YES;
    self.adView.layer.cornerRadius = 2;
    self.adView.layer.masksToBounds = YES;
    self.adView.contentMode = UIViewContentModeScaleAspectFill;
    self.adView.frame = CGRectMake(14, 10, SCREEN_WIDTH-28, 0.01);
    [self.headerView addSubview:self.adView];
    [self.adView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerClick)]];

    self.infoTableView.tableHeaderView= self.headerView;

    self.infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"尚无报名信息~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    self.infoTableView.emptyDescriptionString = attributeString;
    
    DHWeakSelf;
    self.infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestData:NO];
    }];
    
    self.infoTableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        [weakSelf requestData:YES];
    }];
}

#pragma mark - > 加入广告计划
- (void)headerClick{
    
    if ([self.listModel.join_status isEqualToString:@"1"]) {
        [self.navigationController pushViewController:[FWCarPlayerADPlanViewController new] animated:YES];
    }else{
        NSDictionary * dict = @{@"share_desc":self.listModel.share_desc,
                                @"share_title":self.listModel.share_title,
                                @"share_url":self.listModel.share_url,
                                @"share_img":self.listModel.share_img,
        };

        FWJoinADPlanWebViewController * CPAVC = [[FWJoinADPlanWebViewController alloc] init];
        CPAVC.showBottom = YES;
        //            WVC.pageType = WebViewTypeURL;
        CPAVC.htmlStr = self.listModel.h5_plan;
        CPAVC.infodict = dict;
        [self.navigationController pushViewController:CPAVC animated:YES];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWMySignupCellID";
    
    FWMySignupCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWMySignupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.dataSource.count) {
        cell.vc = self;
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

- (void)backBtnClick{
    
    for (UIViewController * item in self.navigationController.viewControllers) {
        if ([item isKindOfClass:[FWRegistrationHomeViewController class]]) {
            [self.navigationController popToViewController:item animated:YES];
            return ;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
