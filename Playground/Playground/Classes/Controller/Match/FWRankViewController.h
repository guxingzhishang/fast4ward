//
//  FWRankViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/8.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 * 排名
 */
#import "FWBaseViewController.h"
#import "FWPickerTypeView.h"

NS_ASSUME_NONNULL_BEGIN
@class FWMatchModel;

typedef void(^countlBlock)(NSString * countString);

@interface FWRankViewController : FWBaseViewController<SelectPickerTypeDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIButton * titleButton;

@property (nonatomic, strong) FWPickerTypeView * pickerView;

@property (nonatomic, strong) FWMatchModel * matchModel;

@property (nonatomic, strong) FWPreRankModel * preRankModel;

@property (nonatomic, strong) FWFinalRankModel * finalRankModel;

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) NSString * sport_id;

@property (nonatomic, copy) countlBlock myBlock;

@end

NS_ASSUME_NONNULL_END
