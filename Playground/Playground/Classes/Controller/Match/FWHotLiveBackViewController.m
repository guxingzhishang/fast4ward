//
//  FWHotLiveBackViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHotLiveBackViewController.h"
#import "ZFPlayer.h"
#import "ZFAVPlayerManager.h"
#import "ZFIJKPlayerManager.h"
#import "KSMediaPlayerManager.h"
#import "UIImageView+ZFCache.h"
#import "ZFUtilities.h"
#import <AVFoundation/AVFoundation.h>
#import "ZFPlayerControlView.h"
#import "FWHotLiveShareViewController.h"
#import "FWLiveTipView.h"

@interface FWHotLiveBackViewController ()< UIGestureRecognizerDelegate,FWLiveTipViewDelegate>

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) UIImageView *containerView;
@property (nonatomic, strong) ZFPlayerControlView *controlView;
@property (nonatomic, strong) UIButton *playBtn;
@property (nonatomic, strong) NSArray <NSURL *>*assetURLs;
@property (nonatomic, strong) ZFAVPlayerManager *playerManager;
/* 最近一次的网络状态  wifi 还是 4G */
@property (nonatomic, strong) NSString * lastNetWork;
@property (nonatomic, strong) FWLiveTipView * tipView;

@property (nonatomic, strong) UIButton * closeButton;

@property (nonatomic, strong) UIView * userView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * numLabel;
@property (nonatomic, strong) UIButton * attentionButton;

@property (nonatomic, strong) UIButton * shareButton;

@end

@implementation FWHotLiveBackViewController

- (void)requestLiveInfo{
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"live_id":self.liveListModel.live_id,
                              @"type":@"huifang",
                              };
    
    [request startWithParameters:params WithAction:Get_zhibo_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.liveListModel = [FWLiveListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            self.assetURLs = @[[NSURL URLWithString:self.liveListModel.replay_url]];
            self.player.assetURLs = self.assetURLs;
            [self.player playTheIndex:0];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.player.viewControllerDisappear = NO;
    
    [self trackPageBegin:@"直播回放页"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.player.viewControllerDisappear = YES;
    
    [self trackPageEnd:@"直播回放页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.containerView];
    
    [self.containerView addSubview:self.playBtn];
    
    [self setupPlayer];
    [self setupTopView];
    
    [self checkWifi];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(monitorLiveNet) name:MonitorLiveNetworking object:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat w = CGRectGetWidth(self.view.frame);
    CGFloat h = CGRectGetHeight(self.view.frame);
    self.containerView.frame = CGRectMake(x, y, w, h);
    
    w = 44;
    h = w;
    x = (CGRectGetWidth(self.containerView.frame)-w)/2;
    y = (CGRectGetHeight(self.containerView.frame)-h)/2;
    self.playBtn.frame = CGRectMake(x, y, w, h);
}

#pragma mark - > 播放器相关配置
- (void)setupPlayer{
    self.playerManager= [[ZFAVPlayerManager alloc] init];
    self.playerManager.scalingMode = ZFPlayerScalingModeAspectFill;
    
    /// 播放器相关
    self.player = [ZFPlayerController playerWithPlayerManager:self.playerManager containerView:self.containerView];
    self.player.controlView = self.controlView;
    /// 设置退到后台继续播放
    self.player.pauseWhenAppResignActive = NO;
    
    self.player.customAudioSession = YES;
    self.player.playerReadyToPlay = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSURL * _Nonnull assetURL) {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:nil];
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
    };
}

#pragma mark - > 设置顶部视图
- (void)setupTopView{
    
    self.tipView = [[FWLiveTipView alloc] initWithFrame:self.view.frame];
    self.tipView.delegate = self;
    [self.tipView.bgImageView sd_setImageWithURL:[NSURL URLWithString:self.liveListModel.live_cover]];
    [self.view addSubview:self.tipView];
    self.tipView.hidden = YES;
    
    self.closeButton = [[UIButton alloc] init];
    self.closeButton.frame = CGRectMake(SCREEN_WIDTH-64, FWCustomeSafeTop +14, 64, 66);
    [self.closeButton setImage:[UIImage imageNamed:@"hot_live_close"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
    
    self.userView = [[UIView alloc] init];
    self.userView.layer.cornerRadius=45/2;
    self.userView.layer.masksToBounds = YES;
    self.userView.backgroundColor = FWColorWihtAlpha(@"000000", 0.4);
    [self.view addSubview:self.userView];
    [self.userView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(self.view).mas_offset(14);
        make.centerY.mas_equalTo(self.closeButton);
        make.width.mas_lessThanOrEqualTo(199);
    }];
    UITapGestureRecognizer * photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoClick)];
    [self.userView addGestureRecognizer:photoTap];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius=39/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.userView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.userView);
        make.size.mas_equalTo(CGSizeMake(39, 39));
        make.left.mas_equalTo(self.userView).mas_offset(3);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 13];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.userView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.photoImageView);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(4);
        make.width.mas_lessThanOrEqualTo(105);
    }];
    
    self.attentionButton = [[UIButton alloc] init];
    self.attentionButton.layer.cornerRadius=2;
    self.attentionButton.layer.masksToBounds = YES;
    self.attentionButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.attentionButton.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.4);
    [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    [self.attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.attentionButton addTarget:self action:@selector(attentionButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.userView addSubview:self.attentionButton];
    
    self.shareButton = [[UIButton alloc] init];
    [self.shareButton setImage:[UIImage imageNamed:@"hot_live_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.shareButton];
    [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).mas_offset(-14);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.bottom.mas_equalTo(self.view).mas_offset(-FWSafeBottom-40);
    }];
    
    CGFloat attentionWH= 39;
    if ([self.liveListModel.user_info.follow_status isEqualToString:@"1"]) {
        // 已关注
        attentionWH = 0.1;
    }
    [self.attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.userView);
        make.size.mas_equalTo(CGSizeMake(attentionWH, attentionWH));
        make.right.mas_equalTo(self.userView).mas_offset(-3);
        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(8);
    }];
    
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.liveListModel.user_info.header_url]];
    
    self.nameLabel.text = self.liveListModel.user_info.nickname;
    self.numLabel.text = [NSString stringWithFormat:@"%@人观看",self.liveListModel.user_total];
}

#pragma mark - > 分享
- (void)shareButtonOnClick{
    
    FWHotLiveShareViewController * vc = [[FWHotLiveShareViewController alloc] init];
    vc.listModel = self.liveListModel;
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - > 关闭当前页面
- (void)closeButtonClick{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
        [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] setIsLogin:NO];
    });
}

#pragma mark - > 进入个人页
- (void)photoClick{
    
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = self.liveListModel.user_info.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 关注主播
- (void)attentionButtonOnClick{
    
    NSString * action = Submit_follow_users;
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":self.liveListModel.user_info.uid,
                              };
    
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([action isEqualToString: Submit_follow_users]) {
                
                [self.attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.mas_equalTo(self.userView);
                    make.size.mas_equalTo(CGSizeMake(0.1, 0.1));
                    make.right.mas_equalTo(self.userView).mas_offset(-3);
                    make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(8);
                }];
                
                [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 用户单机屏幕，收键盘
- (void)singleTap{
    [self.view endEditing:YES];
}

- (void)playClick:(UIButton *)sender {
    [self.player playTheIndex:0];
    [self.controlView showTitle:@"" coverURLString:self.liveListModel.live_cover fullScreenMode:ZFFullScreenModeAutomatic];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
        _controlView.prepareShowLoading = YES;
        _controlView.fullScreenOnly = NO;
        _controlView.fastViewAnimated = YES;
        _controlView.autoHiddenTimeInterval = 5;
        _controlView.autoFadeTimeInterval = 0.5;
        _controlView.prepareShowLoading = YES;
    }
    return _controlView;
}

- (UIImageView *)containerView {
    if (!_containerView) {
        _containerView = [UIImageView new];
        [_containerView setImageWithURLString:self.liveListModel.live_cover placeholder:[ZFUtilities imageWithColor:[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1] size:CGSizeMake(1, 1)]];
    }
    return _containerView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setImage:[UIImage imageNamed:@"new_allPlay_44x44_"] forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(playClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playBtn;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.player.currentPlayerManager pause];
    [self.playerManager pause];
    
    [self.view endEditing:YES];
}

#pragma mark - > 监听当前网络状态
- (void)monitorLiveNet{
    
    [self checkWifi];
}

- (void)checkWifi{
    
    if ([[GFStaticData getObjectForKey:Current_Network] isEqualToString:@"WIFI"]) {
        
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            [self requestLiveInfo];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }else{
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            if (![[GFStaticData getObjectForKey:Allow_4G_Play] boolValue]) {
                [self requestLiveInfo];
                
                self.tipView.hidden = NO;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.player.currentPlayerManager pause];
                    [self.playerManager pause];
                });
            }else{
                self.tipView.hidden = YES;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }
}

#pragma mark - > 4G下 点击继续观看
- (void)continueLiveMatch{
    
    self.tipView.hidden = YES;
    
    self.lastNetWork = [GFStaticData getObjectForKey:Last_Network];
    
    if ([self.lastNetWork isEqualToString:@"WIFI"] && ![self.lastNetWork isEqualToString:[GFStaticData getObjectForKey:Current_Network]]) {
        // 由wifi切换到4G ，继续播放
        [self.player.currentPlayerManager play];
    }else{
        // 进来直接是4G
        [self requestLiveInfo];
    }
    
    [GFStaticData saveObject:@"WIFI" forKey:Current_Network];
    [GFStaticData saveObject:@"1" forKey:Allow_4G_Play];
}
@end
