//
//  FWPlayerRegistrationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWRegistrationHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerRegistrationViewController : FWBaseViewController

@property (nonatomic, strong) NSString * baoming_id;

@end

NS_ASSUME_NONNULL_END
