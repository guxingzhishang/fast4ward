//
//  FWPlayerSignupViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerSignupViewController.h"
#import "FWPlayerSignupView.h"
#import "FWMySignupViewController.h"
#import "FWRegistrationHomeViewController.h"

@interface FWPlayerSignupViewController ()<FWPlayerSignupViewDelegate>
@property (nonatomic, strong) FWPlayerSignupView * signupView;
@property (nonatomic, strong) FWWechatOrderModel * wechatModel ;

@end

@implementation FWPlayerSignupViewController

- (void)requestData{
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];

    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"baoming_user_id":self.baoming_user_id,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_baoming_pay_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.signupModel = [FWPlayerSignupModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.signupView configForView:self.signupModel];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手报名页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手报名页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"车手报名";
    [self setupSubViews];
    
    [self requestData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySucceed) name:FWWXReturnSussessPayNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payFailed) name:FWWXReturnFailedPayNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selfPayFailed) name:FWSelfControlFailedPayNotification object:nil];
}

- (void)setupSubViews{
    
    self.signupView = [[FWPlayerSignupView alloc] init];
    self.signupView.vc = self;
    self.signupView.signupDelegate = self;
    [self.view addSubview:self.signupView];
    [self.signupView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - > 去支付
- (void)payButtonClick:(NSString *)payMethod{
    
    if ([payMethod isEqualToString:@"wechat"]) {
        [self requestPay:@"weixin"];
    }else if ([payMethod isEqualToString:@"alipay"]){
        [self requestPay:@"alipay"];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:payMethod forKey:@"支付返回"];
}

#pragma mark - > 调起微信支付
- (void)payrequestWithModel:(FWWechatOrderModel *)model {
    
    NSString * timeStampStr = model.timestamp;
    UInt32 timesta = (UInt32)[timeStampStr intValue];
    
    PayReq *request = [[PayReq alloc] init];
    request.sign      = model.sign;
    request.package   = @"Sign=WXPay";
    request.nonceStr  = model.noncestr;
    request.prepayId  = model.prepayid;
    request.partnerId = model.partnerid;
    request.timeStamp = timesta;
    
    /* 调起支付 */
    if ([WXApi sendReq:request]) {
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"支付失败" message:@"未安装微信客户端,请使用其他支付方式" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - > 微信pay成功
- (void)paySucceed{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"订单号"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"支付返回"];
    [[NSUserDefaults standardUserDefaults] synchronize];
 
    FWMySignupViewController * MSVC = [[FWMySignupViewController alloc] init];
    [self.navigationController pushViewController:MSVC animated:YES];
}

#pragma mark - > 微信pay失败
- (void)payFailed{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付取消" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 自行操作返回pay失败
- (void)selfPayFailed{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付失败，请重试" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 提交支付(微信/支付宝)
- (void)requestPay:(NSString *)type{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    if (self.baoming_user_id.length < 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"未查询到用户报名ID，请联系工作人员咨询" toController:self];
        return;
    }
    
    if ([self.signupModel.goods.goods_status_sale isEqualToString:@"1"] && self.signupView.selectButton.isSelected) {
        self.goods_id = self.signupModel.goods.goods_id;
    }else{
        self.goods_id = @"0";
    }
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"pay_service":type,
                              @"baoming_user_id":self.baoming_user_id,
                              @"goods_id":self.goods_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_baoming_pay WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([type isEqualToString:@"weixin"]) {
                /* 微信支付 */
                NSDictionary * result = [[back objectForKey:@"data"] objectForKey:@"result_weixin"];
                NSDictionary * pay_order_id = [[back objectForKey:@"data"] objectForKey:@"pay_order_id"];

                self.wechatModel = [FWWechatOrderModel mj_objectWithKeyValues:result];
                [self payrequestWithModel:self.wechatModel];
                
                [[NSUserDefaults standardUserDefaults] setObject:pay_order_id forKey:@"订单号"];
            }else if ([type isEqualToString:@"alipay"]){
                /* 支付宝支付 */
//                [[NSUserDefaults standardUserDefaults] setObject:[[back objectForKey:@"data"] objectForKey:@"pay_order_id"] forKey:@"订单号"];
//                [self alipayRequestWithResult:[[back objectForKey:@"data"] objectForKey:@"result"]];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)backBtnClick{
    
    
    if (self.isShowAlert) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"确认离开?" message:@"离开此页面后，您可在规定时间内到【赛事->车手报名】完成缴费。" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"继续支付" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确认离开" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            for (UIViewController * item in self.navigationController.viewControllers) {
                if ([item isKindOfClass:[FWRegistrationHomeViewController class]]) {
                    [self.navigationController popToViewController:item animated:YES];
                    return ;
                }
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
