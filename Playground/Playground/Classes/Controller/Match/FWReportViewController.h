//
//  FWReportViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/7.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 * 举报原因
 */

#import "FWBaseViewController.h"
#import "FWBaseCellView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWReportViewController : FWBaseViewController

@property (nonatomic, strong) NSArray * titleArray;
@property (nonatomic, strong) NSString * f_uid;

@end

NS_ASSUME_NONNULL_END
