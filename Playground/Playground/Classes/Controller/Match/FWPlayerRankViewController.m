//
//  FWPlayerRankViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerRankViewController.h"
#import "FWPlayerRankCell.h"
#import "FWGroupScoreViewController.h"
#import "FWPlayerRankModel.h"
#import "UIBarButtonItem+Item.h"
#import "FWScoreSearchViewController.h"
#import "FWAllScheduleViewController.h"
#import "FWMoreListViewController.h"
#import "FW02ListViewController.h"
#import "FW04ListViewController.h"


@interface FWPlayerRankViewController()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) NSArray * titleData;
@property (nonatomic, assign) CGFloat  menuHeight;
@property (nonatomic, assign) CGFloat  wmMenuY;

@property (nonatomic, strong) UIImageView * headerImageView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * searchButton;
@property (nonatomic, strong) UIButton * moreButton;

@end

@implementation FWPlayerRankViewController

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        self.selectIndex = 1;
        self.menuViewStyle = WMMenuViewStyleLine;
        self.progressHeight = 2.0f;
        self.titleSizeSelected = 14.0f;
        self.titleSizeNormal = 14.0f;
        self.scrollEnable = YES;
        self.titleColorSelected = FWTextColor_222222;
        self.titleColorNormal = FWTextColor_BCBCBC;
        self.menuViewLayoutMode = WMMenuViewLayoutModeScatter;
        self.menuHeight = 44.0f;
        self.wmMenuY =  154*SCREEN_WIDTH/375;
    }
    return self;
}

#pragma mark - > 标题数组
- (NSArray *)titleData {
    if (!_titleData) {
        _titleData = @[ @"赛程",@"04总榜",@"02总榜"];
    }
    return _titleData;
}

#pragma mark - > WMPageController 相关配置
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController{
    return self.titleData.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.titleData[index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    
    if (index == 0){
        FWAllScheduleViewController * RVC = [[FWAllScheduleViewController alloc] init];
        RVC.myBlock = ^(NSString * _Nonnull top_img_url) {};
        return RVC;
    }else if (index == 1){
        FW04ListViewController * LVC = [[FW04ListViewController alloc] init];
        LVC.myBlock = ^(NSString * _Nonnull top_img_url) {
            [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:top_img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        };
        return LVC;
    }else{
        FW02ListViewController * LVC = [[FW02ListViewController alloc] init];
        return LVC;
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView
{
    return CGRectMake(0,self.wmMenuY, self.view.frame.size.width-46, self.menuHeight);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView
{
    return CGRectMake(0, self.wmMenuY+self.menuHeight, self.view.frame.size.width, self.view.frame.size.height - self.menuHeight-self.wmMenuY);
}

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.fd_prefersNavigationBarHidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.fd_prefersNavigationBarHidden = YES;
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.headerImageView = [[UIImageView alloc] init];
    self.headerImageView.clipsToBounds = YES;
    self.headerImageView.userInteractionEnabled = YES;
    self.headerImageView.image = [UIImage imageNamed:@"placeholder"];
    self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.headerImageView];
    [self.headerImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 154*SCREEN_WIDTH/375));
    }];
    
    self.backButton = [[UIButton alloc] init];
    self.backButton.alpha = 0.5;
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.headerImageView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.left.mas_equalTo(self.headerImageView);
        make.top.mas_equalTo(self.headerImageView).mas_offset(FWCustomeSafeTop+20);
    }];
    
    self.searchButton = [[UIButton alloc] init];
    [self.searchButton setImage:[UIImage imageNamed:@"rank_search"] forState:UIControlStateNormal];
    [self.searchButton addTarget:self action:@selector(searchButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.headerImageView addSubview:self.searchButton];
    [self.searchButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.right.mas_equalTo(self.headerImageView);
        make.centerY.mas_equalTo(self.backButton);
    }];
    
    self.moreButton = [[UIButton alloc] init];
    [self.moreButton setImage:[UIImage imageNamed:@"rank_more"] forState:UIControlStateNormal];
    [self.moreButton addTarget:self action:@selector(moreButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.moreButton];
    [self.moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(46, 44));
        make.right.mas_equalTo(self.view).mas_offset(0);
        make.top.mas_equalTo(self.headerImageView.mas_bottom).mas_offset(0);
    }];
}

#pragma mark - > 返回
- (void)backButtonOnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 搜索
- (void)searchButtonOnClick{
    
    FWScoreSearchViewController * SVC = [[FWScoreSearchViewController alloc] init];
    [self.navigationController pushViewController:SVC animated:YES];
}

#pragma mark - > 更多
- (void)moreButtonOnClick{
    [self.navigationController pushViewController:[FWMoreListViewController new] animated:YES];
}

@end
