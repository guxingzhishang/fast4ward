//
//  FWScoreSearchViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 成绩搜索
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWScoreSearchViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
