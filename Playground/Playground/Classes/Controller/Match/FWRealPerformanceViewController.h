//
//  FWRealPerformanceViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/7.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 * 实时成绩
 */
#import "FWBaseViewController.h"
#import "FWPickerTypeView.h"
#import "FWScoreModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^countlBlock)(NSString * countString);

@interface FWRealPerformanceViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource,SelectPickerTypeDelegate>

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) FWPickerTypeView *pickView;

@property (nonatomic, strong) FWMatchModel * matchModel;
@property (nonatomic, strong) FWScoreModel * scoreModel;

@property (nonatomic, strong) NSString * sport_id;

@property (nonatomic, copy) countlBlock myBlock;

@end

NS_ASSUME_NONNULL_END
