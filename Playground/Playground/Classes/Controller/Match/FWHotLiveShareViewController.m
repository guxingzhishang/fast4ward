//
//  FWHotLiveShareViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHotLiveShareViewController.h"

@interface FWHotLiveShareViewController ()

@property (nonatomic, strong) UIButton * friendButton;
@property (nonatomic, strong) UIButton * sessionButton;

@property (nonatomic, strong) UIView * mainView;
@property (nonatomic, strong) UIImageView * bgView;
@property (nonatomic, strong) UIImageView * topIconView;
@property (nonatomic, strong) UIImageView * picView;
@property (nonatomic, strong) UIImageView * qrcodeView;

@property (nonatomic, strong) UILabel * quanqiuqicheLabel;
@property (nonatomic, strong) UILabel * conetentLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UILabel * zhibozhongLabel;

@end

@implementation FWHotLiveShareViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    [self trackPageBegin:@"直播分享页"];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"直播分享页"];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNothing)];
    [self.view addGestureRecognizer:tap];
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.mainView = [[UIView alloc] init];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.layer.masksToBounds = YES;
    [self.view addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset((SCREEN_HEIGHT-(505*(SCREEN_WIDTH-48)/327))/3);
        make.left.mas_equalTo(self.view).mas_offset(24);
        make.width.mas_equalTo(SCREEN_WIDTH-48);
        make.height.mas_equalTo(505*(SCREEN_WIDTH-48)/327);
    }];
    
    self.bgView = [[UIImageView alloc] init];
    self.bgView.image = [UIImage imageNamed:@"hot_live_share_bg"];
    [self.mainView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.mainView);
    }];
    
    self.topIconView = [[UIImageView alloc] init];
    self.topIconView.image = [UIImage imageNamed:@"hot_live_share_topicon"];
    [self.bgView addSubview:self.topIconView];
    [self.topIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(16);
        make.left.mas_equalTo(self.bgView).mas_offset(21);
        make.width.mas_equalTo(64);
        make.height.mas_equalTo(30);
    }];
    
    UIView * lineOneView = [[UIView alloc] init];
    lineOneView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.bgView addSubview:lineOneView];
    [lineOneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topIconView.mas_right).mas_offset(7);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(23);
        make.centerY.mas_equalTo(self.topIconView);
    }];
    
    self.quanqiuqicheLabel = [[UILabel alloc] init];
    self.quanqiuqicheLabel.text = @"全球汽车文化引领者";
    self.quanqiuqicheLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
    self.quanqiuqicheLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.quanqiuqicheLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.quanqiuqicheLabel];
    [self.quanqiuqicheLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topIconView);
        make.size.mas_equalTo(CGSizeMake(150, 20));
        make.left.mas_equalTo(self.topIconView.mas_right).mas_offset(14);
    }];
    
    self.picView = [[UIImageView alloc] init];
    [self.picView sd_setImageWithURL:[NSURL URLWithString:self.listModel.live_share_image] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.picView.contentMode = UIViewContentModeScaleAspectFill;
    self.picView.clipsToBounds = YES;
    self.picView.layer.cornerRadius = 5;
    self.picView.layer.masksToBounds =YES;
    [self.bgView addSubview:self.picView];
    [self.picView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(56);
        make.centerX.mas_equalTo(self.bgView);
        make.width.height.mas_equalTo(SCREEN_WIDTH-48-42);
    }];
    
    self.conetentLabel = [[UILabel alloc] init];
    self.conetentLabel.text = self.listModel.live_title;
    self.conetentLabel.numberOfLines = 2;
    self.conetentLabel.lineBreakMode = NSLineBreakByCharWrapping;
    self.conetentLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.conetentLabel.textColor = FWColor(@"0C091A");
    self.conetentLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.conetentLabel];
    [self.conetentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.picView.mas_bottom).mas_equalTo(10);
        make.height.mas_equalTo(40);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.picView).mas_offset(8);
        make.right.mas_equalTo(self.picView).mas_offset(-8);
    }];
    
    UIView * lineTwoView = [[UIView alloc] init];
    lineTwoView.backgroundColor = FWColor(@"E0DFDF");
    [self.bgView addSubview:lineTwoView];
    [lineTwoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.picView);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.picView.mas_bottom).mas_offset(52);
    }];
    
    self.qrcodeView = [[UIImageView alloc] init];
    [self.qrcodeView sd_setImageWithURL:[NSURL URLWithString:self.listModel.live_share_qrcode] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.bgView addSubview:self.qrcodeView];
    [self.qrcodeView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineTwoView.mas_bottom).mas_offset(20);
        make.right.mas_equalTo(self.picView);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(70);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.text = self.listModel.user_info.nickname;
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.nameLabel.textColor = FWColor(@"151515");
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineTwoView.mas_bottom).mas_equalTo(12);
        make.height.mas_equalTo(15);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.conetentLabel);
        make.right.mas_equalTo(self.qrcodeView.mas_left).mas_offset(-10);
    }];
    
    self.zhibozhongLabel = [[UILabel alloc] init];
    self.zhibozhongLabel.text = @"直播中...";
    self.zhibozhongLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 21];
    self.zhibozhongLabel.textColor = FWColor(@"FF5555");
    self.zhibozhongLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.zhibozhongLabel];
    [self.zhibozhongLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_equalTo(5);
        make.height.mas_equalTo(26);
        make.width.mas_greaterThanOrEqualTo(80);
        make.left.mas_equalTo(self.nameLabel);
        make.right.mas_equalTo(self.nameLabel);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"扫描右侧二维码\r\n打开「肆放」看直播";
    self.tipLabel.numberOfLines = 0;
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.tipLabel.textColor = FWColor(@"151515");
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.zhibozhongLabel.mas_bottom).mas_equalTo(0);
        make.height.mas_equalTo(40);
        make.width.mas_greaterThanOrEqualTo(80);
        make.left.mas_equalTo(self.zhibozhongLabel);
        make.right.mas_equalTo(self.zhibozhongLabel);
    }];
    
    
    self.friendButton = [[UIButton alloc] init];
    self.friendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.friendButton setImage:[UIImage imageNamed:@"share_weixinhaoyou"] forState:UIControlStateNormal];
    [self.friendButton setTitle:@"   微信好友" forState:UIControlStateNormal];
    [self.friendButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    self.friendButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.view addSubview:self.friendButton];
    [self.friendButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.friendButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_bottom).mas_offset(27);
        make.height.mas_equalTo(47);
        make.width.mas_equalTo(140);
        make.left.mas_equalTo(self.bgView).mas_offset(14);
    }];
    

    self.sessionButton = [[UIButton alloc] init];
    self.sessionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.sessionButton setImage:[UIImage imageNamed:@"share_pengyouquan"] forState:UIControlStateNormal];
    [self.sessionButton setTitle:@"   朋友圈" forState:UIControlStateNormal];
    [self.sessionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    self.sessionButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.view addSubview:self.sessionButton];
    [self.sessionButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.sessionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.friendButton);
        make.height.mas_equalTo(self.friendButton);
        make.width.mas_equalTo(self.friendButton);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
    }];
}


#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{
    
    if (sender == self.sessionButton) {
        
        [self actionForScreenShotWith:self.bgView savePhoto:NO withSence:WXSceneTimeline];
    }else{
        
        [self actionForScreenShotWith:self.bgView savePhoto:NO withSence:WXSceneSession];
    }
}


- (void)WXSendImage:(UIImage *)image withShareScene:(enum WXScene)scene {
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
       
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        WXImageObject *ext = [WXImageObject object];
        // 小于10MB
        ext.imageData = imageData;
        
        WXMediaMessage *message = [WXMediaMessage message];
        message.mediaObject = ext;
        
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.scene = scene;
        req.message = message;
        [WXApi sendReq:req];
        
        [self tapNothing];
    }else {
        // 提示用户安装微信
        [[FWHudManager sharedManager] showErrorMessage:@"您还没装微信哦~" toController:self];
    }
}

#pragma mark - > 截屏
- (void)actionForScreenShotWith:(UIView *)aimView savePhoto:(BOOL)savePhoto withSence:(enum WXScene)scene {
    
    if (!aimView) return;
    
    UIGraphicsBeginImageContextWithOptions(aimView.bounds.size, NO, 0.0f);
    [aimView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (savePhoto) {
        UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }else{
        [self WXSendImage:viewImage withShareScene:scene];
    }
}

#pragma mark - >  保存到本地相册
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    if (error) {
        NSLog(@"保存失败，请重试");
    } else {
        NSLog(@"保存成功");
    }
}

#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {
    
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}

- (void)tapNothing{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
