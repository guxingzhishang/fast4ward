//
//  FWPlayerShareViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWRegistrationHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerShareViewController : FWBaseViewController

@property (nonatomic, strong) FWPlayerSignupDriverCardModel * driver_info;

@end

NS_ASSUME_NONNULL_END
