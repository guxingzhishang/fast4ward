//
//  FWGuessingBettingViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 竞猜投注
 */
#import "FWBaseViewController.h"
#import "FWGuessingModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^guessBlock)(NSString * guess_id);
@interface FWGuessingBettingViewController : FWBaseViewController

@property (nonatomic, strong) FWGuessingListModel * listModel;
@property (nonatomic, strong) FWGuessingRecordUserInfoModel * currentInfo;
@property (nonatomic, strong) NSString * type; // 押的左边，还是右边
@property (nonatomic, strong) NSString * gold_balance_value;

@property (nonatomic, copy) guessBlock myBlock;


@end

NS_ASSUME_NONNULL_END
