//
//  FWOfficialVideoViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/9.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWOfficialVideoViewController.h"
#import "FWOfficialVideoCell.h"
#import "FWRefitCaseSegement.h"
#import "FWOfficialVideoModel.h"

@interface FWOfficialVideoViewController ()<UITableViewDelegate,UITableViewDataSource,FWRefitCaseSegementViewDelegate>

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, strong) FWRefitCaseSegement * segement;
@property (nonatomic, strong) FWOfficialVideoModel * videoModel;
@property (nonatomic, strong) NSMutableArray * groupArray;
@property (nonatomic, assign) NSInteger  lastIndex;

@end

@implementation FWOfficialVideoViewController
@synthesize infoTableView;

- (NSMutableArray *)groupArray{
    if (!_groupArray) {
        _groupArray = [[NSMutableArray alloc] init];
    }
    return _groupArray;
}

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return  _dataSource;
}

#pragma mark - > 获取官方视频列表
- (void)requestVideoList:(BOOL)isLoadMoredData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    NSDictionary * params  = @{
                               @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                               @"page":@(self.pageNum).stringValue,
                               @"page_size":@"20",
                               @"group_id":self.group_id,
                               };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_sport_officialvod  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }

            self.videoModel = [FWOfficialVideoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            if (self.groupArray.count <= 0) {
                for (FWGroupInfoModel * listModel in self.videoModel.group_list) {
                    if (listModel.group_name.length > 0) {
                        [self.groupArray addObject:listModel.group_name];
                    }
                }
                [self.segement deliverTitles:[self.groupArray copy]];
                self.segement.lastIndex = self.lastIndex;
            }

            [self.dataSource addObjectsFromArray: self.videoModel.vod_list];

            if([self.videoModel.vod_list count] == 0 &&
               self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;

                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                [[FWHudManager sharedManager] showErrorMessage:@"您还未登录，请先登录才能查看关注哦~" toController:self];
                return ;
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"官方视频页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"官方视频页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"FAST4WARD官方视频";
    self.group_id = @"0";
    self.lastIndex = 0;

    [self setupSubViews];
    
    [self requestVideoList:NO];
}

- (void)setupSubViews{
    
    self.segement = [[FWRefitCaseSegement alloc]init];
    self.segement.itemDelegate = self;
    [self.view addSubview:self.segement];
    self.segement.frame = CGRectMake(0, 0, SCREEN_WIDTH, 48);

    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 210;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.segement.mas_bottom).mas_offset(0);
    }];
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestVideoList:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestVideoList:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"officialVideoID";
    
    FWOfficialVideoCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWOfficialVideoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.dataSource.count) {
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.videoModel.vod_list[indexPath.row].is_long_video isEqualToString:@"1"]) {
        /* 长视频 */
        FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
        LPVC.feed_id = self.videoModel.vod_list[indexPath.row].feed_id;
        LPVC.myBlock = ^(FWFeedListModel *listModel) {};
        [self.navigationController pushViewController:LPVC animated:YES];
    }else{
        // 视频流
        FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
        controller.currentIndex = 0;
        controller.feed_id = self.videoModel.vod_list[indexPath.row].feed_id;
        controller.requestType = FWPushMessageRequestType;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)segementItemClickTap:(NSInteger)index{
    
    if (self.lastIndex == index) {
        return;
    }
//    [self.infoTableView layoutIfNeeded]; //这句是关键

    self.lastIndex = index;

    self.group_id = self.videoModel.group_list[index].group_id;
    [self requestVideoList:NO];
}

@end
