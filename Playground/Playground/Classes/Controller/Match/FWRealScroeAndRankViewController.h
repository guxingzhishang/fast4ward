//
//  FWRealScroeAndRankViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/8.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 不含直播的实时成绩 + 排行
 */

#import "WMPageController.h"
#import "WMPageController.h"
#import "FWShareScoreImageViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface FWRealScroeAndRankViewController : WMPageController

@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * share_image;

@end

NS_ASSUME_NONNULL_END
