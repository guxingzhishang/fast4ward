//
//  FWRealPerformanceViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/7.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWRealPerformanceViewController.h"
#import "FWRealPerformanceCell.h"
#import "FWMatchRequest.h"
#import "FWMatchModel.h"
#import "FWRealPerformanceView.h"

static NSString * tipString = @"当前无比赛成绩!";

@interface FWRealPerformanceViewController()<FWRealPerformanceCellDelegate>

@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) BOOL  showMore;

@end

@implementation FWRealPerformanceViewController
@synthesize infoTableView;
@synthesize scoreModel;
@synthesize pageNum;

- (NSMutableArray * )dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

- (void)dealloc{
    
}

- (void)setSport_id:(NSString *)sport_id{
    _sport_id = sport_id;

    [self requestMatchLogWithLoadMoredData:NO];
}

#pragma mark - > 获取比赛实时成绩
- (void)requestMatchLogWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (self.sport_id.length > 0) {
        
        if (isLoadMoredData) {
            pageNum += 1;
        }else{
            pageNum = 1;
            
            if (self.dataSource.count > 0 ) {
                [self.dataSource removeAllObjects];
            }
        }
        
        FWMatchRequest * request = [[FWMatchRequest alloc] init];
        
        if (!self.sport_id) return;
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"sport_id":self.sport_id,
                                  @"phase_id":@"0",
                                  @"page":@(pageNum).stringValue,
                                  @"page_size":@"20",
                                  };
        
        [request startWithParameters:params WithAction:Get_match_log WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
               
                if (isLoadMoredData) {
                    [infoTableView.mj_footer endRefreshing];
                }else{
                    [infoTableView.mj_header endRefreshing];
                }
                
                scoreModel = [FWScoreModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                
                if (scoreModel.live_user_count.length > 0) {
                    self.myBlock(scoreModel.live_user_count);
                }
                
                if(scoreModel.list.count == 0 &&
                   pageNum > 1){
                    [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
                }else{
                    
                    NSArray * dayList = [scoreModel.day_list copy];
                
                    NSMutableArray  * tempArr = @[].mutableCopy;
                    
                    for(int i = 0; i < dayList.count ;i++){
                        NSString * day = dayList[i];
                        
                        NSMutableArray * everyDay = @[].mutableCopy;

                        /* 分页后走这个遍历，判断下面的逻辑，新请求回来的数据放入哪组 */
                        for (NSArray * listArray in self.dataSource) {
                            
                            if ([((FWScoreListModel *)listArray[0]).day length] <= 0) {
                                continue;
                            }else{
                                if ([day isEqualToString:((FWScoreListModel *)listArray[0]).day]) {
                                    everyDay = [listArray mutableCopy];
                                }
                            }
                        }
                        
                        for (FWScoreListModel * model in scoreModel.list) {
                            if ([model.day isEqualToString:day]) {
                                [everyDay addObject:model];
                            }
                        }
                        if (everyDay.count > 0) {
                            [tempArr addObject:everyDay];
                        }
                    }
                    self.dataSource = [tempArr mutableCopy];
                    
                
                    if (scoreModel.my_car_no && scoreModel.my_car_no.length > 0 ) {
                        if ([scoreModel.my_car_no isEqualToString:@"0"] ||
                            [scoreModel.my_car_no isEqualToString:@""]) {
                            /* 如果没参加过本次比赛，直接刷新退出 */
                            [self.infoTableView reloadData];
                            return ;
                        }
                        /* 当前uid报名本次比赛 ，并且不是上拉加载更多*/
                        tempArr = [self.dataSource mutableCopy];
                        
                        NSMutableArray * my_scoreArr = @[].mutableCopy;

                        if (scoreModel.my_score_list.count > 0) {
                            /* 有成绩 */
                            
                            for (FWScoreListModel * model in scoreModel.my_score_list) {
                                model.currentStatus = @"有成绩";
                                [my_scoreArr addObject:model];
                            }
                            
                            [tempArr insertObject:my_scoreArr atIndex:0];
                        }else{
                            /* 没有成绩，显示当前无比赛成绩(构造个模型装到数组中) */
                            FWScoreListModel * model = [[FWScoreListModel alloc] init];
                            model.currentStatus = tipString;
                            [my_scoreArr addObject:model];
                            [tempArr insertObject:my_scoreArr atIndex:0];
                        }
                        
                        self.dataSource = [tempArr mutableCopy];
                    }
                    
                    [self.infoTableView reloadData];
                }
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"实时成绩页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"实时成绩页"];
}
- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;

    pageNum = 1;
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.estimatedRowHeight = 100;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.verticalOffset = -20;
    infoTableView.emptyDescriptionString = attributeString;
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestMatchLogWithLoadMoredData:NO];
    }];
    
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestMatchLogWithLoadMoredData:YES];
    }];
}

#pragma mark - > UITableView delegate & dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (scoreModel.my_car_no && scoreModel.my_car_no.length > 0 && ![scoreModel.my_car_no isEqualToString:@"0"]) {
        if (section == 0) {
            if (self.showMore) {
                return [(NSMutableArray *)self.dataSource[section] count];
            }else{
                return 1;
            }
        }
    }
    return [(NSMutableArray *)self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"RealPerformanceID";
    
    FWRealPerformanceCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWRealPerformanceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }    
    
    cell.delegate = self;
    
    if (scoreModel.my_car_no && scoreModel.my_car_no.length > 0) {
        /* 报名了 */
        if (indexPath.section == 0) {
            NSArray<FWScoreListModel *> * dataArray  = self.dataSource[indexPath.section];
            
            if (dataArray.count > 0 &&
                dataArray.count > indexPath.row &&
                [dataArray[0].currentStatus isEqualToString:@"有成绩"]) {
                
                [cell allViewIsHidden:NO];
                
                cell.vc = self;
                cell.scoreModel = scoreModel;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell configForModel:dataArray[indexPath.row]];
            
                if (self.showMore) {
                    /* 已经展示更多，将按钮隐藏，并显示所有个人成绩 */
                    cell.moreButton.enabled = NO;
                    [cell.moreButton setTitle:@"" forState:UIControlStateNormal];
                    [cell.moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.centerX.mas_equalTo(cell.shareButton);
                        make.bottom.mas_equalTo(cell.contentView).mas_offset(-25);
                        make.size.mas_equalTo(CGSizeMake(60, 0.01));
                        make.top.mas_equalTo(cell.shareButton.mas_bottom).mas_offset(0);
                    }];
                }else{
                    /* 未展示更多，将按钮显示，并显示查看更多按钮 */
                    cell.moreButton.enabled = YES;
                    [cell.moreButton setTitle:@"查看更多" forState:UIControlStateNormal];
                    [cell.moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.centerX.mas_equalTo(cell.shareButton);
                        make.bottom.mas_equalTo(cell.contentView).mas_offset(-5);
                        make.size.mas_equalTo(CGSizeMake(60, 30));
                        make.top.mas_equalTo(cell.shareButton.mas_bottom).mas_offset(10);
                    }];
                }
            }else{
                // 当前无比赛成绩
                FWScoreListModel * model = ((NSMutableArray *)self.dataSource[indexPath.section])[0];
                if ([model.currentStatus isEqualToString:tipString]) {
                    
                    [cell allViewIsHidden:YES];
                    cell.moreButton.enabled = NO;
                    [cell.moreButton setTitle:tipString forState:UIControlStateNormal];
                    [cell.moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.edges.mas_equalTo(cell.contentView);
                    }];
                }
            }
        }else{
            if (self.dataSource.count > 1) {
                
                NSArray<FWScoreListModel *> * dataArray  = self.dataSource[indexPath.section];
                
                if (dataArray.count > 0 &&
                    dataArray.count > indexPath.row) {
                    cell.vc = self;
                    cell.scoreModel = scoreModel;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell configForModel:dataArray[indexPath.row]];
                    
                    [cell allViewIsHidden:NO];
                    cell.moreButton.enabled = NO;
                    [cell.moreButton setTitle:@"" forState:UIControlStateNormal];
                    [cell.moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.centerX.mas_equalTo(cell.shareButton);
                        make.bottom.mas_equalTo(cell.contentView).mas_offset(-25);
                        make.size.mas_equalTo(CGSizeMake(60, 0.01));
                        make.top.mas_equalTo(cell.shareButton.mas_bottom).mas_offset(0);
                    }];
                }
            }
        }
    }else{
        
        if (self.dataSource.count > 0) {
            
            NSArray<FWScoreListModel *> * dataArray  = self.dataSource[indexPath.section];
            
            if (dataArray.count > 0 &&
                dataArray.count > indexPath.row) {
                cell.vc = self;
                cell.scoreModel = scoreModel;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell configForModel:dataArray[indexPath.row]];
                
                cell.moreButton.enabled = NO;
                [cell.moreButton setTitle:@"" forState:UIControlStateNormal];
                [cell.moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(cell.shareButton);
                    make.bottom.mas_equalTo(cell.contentView).mas_offset(-25);
                    make.size.mas_equalTo(CGSizeMake(60, 0.01));
                    make.top.mas_equalTo(cell.shareButton.mas_bottom).mas_offset(0);
                }];
            }
        }
    }
    
    
    return cell;
}

-(void)moreButtonClick{
    self.showMore = YES;
    [self.infoTableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    FWRealPerformanceView * view = [FWRealPerformanceView headerViewWithTableView:self.infoTableView];
    view.backgroundColor = FWTextColor_FAFAFA;
    
    if (section < self.dataSource.count) {
        NSArray * dataArray  = self.dataSource[section];
        
        if (scoreModel.my_car_no && scoreModel.my_car_no.length > 0 && section == 0 && ![scoreModel.my_car_no isEqualToString:@"0"]) {
            [view configForModel:@"我的成绩"];
            return view;
        }
        
        [view configForModel:dataArray[0]];
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 31;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (scoreModel.my_car_no && scoreModel.my_car_no.length > 0 && ![scoreModel.my_car_no isEqualToString:@"0"]) {
        if (indexPath.section == 0) {
            if (self.showMore) {
                return 114;
            }else{

                FWScoreListModel * model = ((NSMutableArray *)self.dataSource[indexPath.section])[0];
                if ([model.currentStatus isEqualToString:tipString]) {
                    return 60;
                }
                return 126;
            }
        }
    }
    return 114;
}

@end
