//
//  FWGuessingRecordViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGuessingRecordViewController.h"
#import "FWGuessingRecordCell.h"
#import "FWGuessingRecordModel.h"

@interface FWGuessingRecordViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) FWGuessingRecordModel * recordModel;
@property (nonatomic, assign) NSInteger pageNum;
@end

@implementation FWGuessingRecordViewController
@synthesize infoTableView;

- (void)requestRecord:(BOOL)isreloadMore{
    
    if (!self.sport_id) {
        [[FWHudManager sharedManager] showErrorMessage:@"没有比赛id，请重新退出重试" toController:self];
        return;
    }
    
    if (isreloadMore == NO) {
        
        self.pageNum = 1;
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum += 1;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"sport_id":self.sport_id,
                                @"page":@(self.pageNum).stringValue,
                                @"page_size":@"20",

    };
    
    [request startWithParameters:params WithAction:Get_sport_guess_log WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if (isreloadMore) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            self.recordModel = [FWGuessingRecordModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.dataSource addObjectsFromArray:self.recordModel.list];
            
            if([self.recordModel.list count] == 0 &&
               self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
            
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return  _dataSource;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"竞猜记录页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"竞猜记录页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageNum = 1;
    
    self.title = @"竞猜记录";
    [self setupSubViews];
    [self requestRecord:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 205;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"当前无竞猜~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    infoTableView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_FFFFFF;
    
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    infoTableView.tableFooterView = footView;
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestRecord:NO];
    }];
    infoTableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        [weakSelf requestRecord:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWGuessingRecordCellID";
    
    FWGuessingRecordCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWGuessingRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.dataSource.count > indexPath.row) {
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    return cell;
}

@end
