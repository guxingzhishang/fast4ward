//
//  FWPlayerShareViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerShareViewController.h"

@interface FWPlayerShareViewController ()
@property (nonatomic, strong) UIButton * friendButton;
@property (nonatomic, strong) UIButton * sessionButton;

@property (nonatomic, strong) UIView * mainView;
@property (nonatomic, strong) UIImageView * bgView;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;

@end

@implementation FWPlayerShareViewController

#pragma mark - > 直播分享
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self trackPageBegin:@"直播分享页"];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"直播分享页"];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNothing)];
    [self.view addGestureRecognizer:tap];
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.mainView = [[UIView alloc] init];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.layer.masksToBounds = YES;
    [self.view addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset((SCREEN_HEIGHT-(408*(SCREEN_WIDTH-82)/294))/3);
        make.left.mas_equalTo(self.view).mas_offset(41);
        make.width.mas_equalTo(SCREEN_WIDTH-82);
        make.height.mas_equalTo(408*(SCREEN_WIDTH-82)/294);
    }];
    
    self.bgView = [[UIImageView alloc] init];
    self.bgView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgView.clipsToBounds = YES;
    [self.bgView sd_setImageWithURL:[NSURL URLWithString:self.driver_info.bg_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.mainView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.mainView);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 2;
    self.photoImageView.layer.masksToBounds = YES;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.driver_info.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
    [self.bgView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29, 29));
        make.centerY.mas_equalTo(self.bgView);
        make.right.mas_equalTo(self.bgView).mas_offset(-22);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.text = self.driver_info.nickname;
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 11];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.photoImageView.mas_bottom).mas_equalTo(6);
        make.height.mas_equalTo(15);
        make.width.mas_greaterThanOrEqualTo(10);
        make.right.mas_equalTo(self.photoImageView);
    }];
    
    self.friendButton = [[UIButton alloc] init];
    self.friendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.friendButton setImage:[UIImage imageNamed:@"share_weixinhaoyou"] forState:UIControlStateNormal];
    [self.friendButton setTitle:@"   微信好友" forState:UIControlStateNormal];
    [self.friendButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    self.friendButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.view addSubview:self.friendButton];
    [self.friendButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.friendButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_bottom).mas_offset(27);
        make.height.mas_equalTo(47);
        make.width.mas_equalTo(140);
        make.left.mas_equalTo(self.bgView).mas_offset(14);
    }];
    
    
    self.sessionButton = [[UIButton alloc] init];
    self.sessionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.sessionButton setImage:[UIImage imageNamed:@"share_pengyouquan"] forState:UIControlStateNormal];
    [self.sessionButton setTitle:@"   朋友圈" forState:UIControlStateNormal];
    [self.sessionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    self.sessionButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.view addSubview:self.sessionButton];
    [self.sessionButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.sessionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.friendButton);
        make.height.mas_equalTo(self.friendButton);
        make.width.mas_equalTo(self.friendButton);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
    }];
}

#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{
    
    if (sender == self.sessionButton) {
        [self actionForScreenShotWith:self.bgView savePhoto:NO withSence:WXSceneTimeline];
    }else{
        [self actionForScreenShotWith:self.bgView savePhoto:NO withSence:WXSceneSession];
    }
}


- (void)WXSendImage:(UIImage *)image withShareScene:(enum WXScene)scene {
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        WXImageObject *ext = [WXImageObject object];
        // 小于10MB
        ext.imageData = imageData;
        
        WXMediaMessage *message = [WXMediaMessage message];
        message.mediaObject = ext;
        
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.scene = scene;
        req.message = message;
        [WXApi sendReq:req];
        
        [self tapNothing];
    }else {
        // 提示用户安装微信
        [[FWHudManager sharedManager] showErrorMessage:@"您还没装微信哦~" toController:self];
    }
}

#pragma mark - > 截屏
- (void)actionForScreenShotWith:(UIView *)aimView savePhoto:(BOOL)savePhoto withSence:(enum WXScene)scene {
    
    if (!aimView) return;
    
    UIGraphicsBeginImageContextWithOptions(aimView.bounds.size, NO, 0.0f);
    [aimView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (savePhoto) {
        UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }else{
        [self WXSendImage:viewImage withShareScene:scene];
    }
}

#pragma mark - >  保存到本地相册
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    if (error) {
        NSLog(@"保存失败，请重试");
    } else {
        NSLog(@"保存成功");
    }
}

- (void)tapNothing{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
