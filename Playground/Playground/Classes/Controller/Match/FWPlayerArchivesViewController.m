//
//  FWPlayerArchivesViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerArchivesViewController.h"
#import "FWPlayerArchivesHeaderView.h"
#import "FWPlayerArchivesCell.h"
#import "FWPlayerArchivesModel.h"
#import "FWPlayerInfoShareViewController.h"
#import "FWPlayerScoreDetailViewController.h"

@interface FWPlayerArchivesViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) FWPlayerArchivesHeaderView * headerView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWPlayerArchivesModel * archivesModel;
@end

@implementation FWPlayerArchivesViewController
@synthesize infoTableView;

- (NSMutableArray *)dataSource{
    
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

- (void)requestData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if (!self.driver_id) {
        return;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"driver_id":self.driver_id,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_driver_rank_history WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.archivesModel = [FWPlayerArchivesModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.headerView configForView:self.archivesModel];
            
            [self.dataSource addObjectsFromArray:self.archivesModel.sport_list];
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"车手档案"];
    
    self.view.backgroundColor = FWTextColor_F8F8F8;
    
    [self.navigationController.navigationBar setTranslucent:NO];
    // 设置导航栏的样式
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    // 设置导航栏的背景渲染色
    [self.navigationController.navigationBar setBarTintColor:FWTextColor_252527];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    // 设置文字属性
    NSDictionary *dic = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:FWViewBackgroundColor_FFFFFF, DHSystemFontOfSize_18, nil] forKeys:[NSArray arrayWithObjects:NSForegroundColorAttributeName, NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTitleTextAttributes:dic];
    
    UIButton* backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,60,30)];
    backButton.titleLabel.font = DHSystemFontOfSize_16;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    UIButton* shareButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    shareButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    [shareButton setImage:[UIImage imageNamed:@"new_white_share"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手档案"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"车手档案";
    
    [self setupSubViews];
    [self requestData];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.estimatedRowHeight = 138;
    infoTableView.backgroundColor = FWTextColor_F8F8F8;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
//        make.top.mas_equalTo(self.view).mas_offset(-20);
        make.top.mas_equalTo(self.view).mas_offset(0);
    }];
    
    self.headerView = [[FWPlayerArchivesHeaderView alloc] init];
    self.headerView.vc = self;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 170);
    infoTableView.tableHeaderView = self.headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    FWPlayerSportModel * sportModel = (FWPlayerSportModel *)self.dataSource[section];
    return sportModel.list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWPlayerArchivesCellID";
    
    FWPlayerArchivesCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWPlayerArchivesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
   
    cell.vc = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (indexPath.section < self.dataSource.count) {
        FWPlayerSportModel * sportModel = (FWPlayerSportModel *)self.dataSource[indexPath.section];
        
        if (indexPath.row < sportModel.list.count) {
            [cell configForCell:sportModel.list[indexPath.row]];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section < self.dataSource.count) {
        FWPlayerSportModel * sportModel = (FWPlayerSportModel *)self.dataSource[indexPath.section];
        
        if (indexPath.row < sportModel.list.count) {
            FWPlayerScoreDetailViewController * SDVC = [[FWPlayerScoreDetailViewController alloc] init];
            SDVC.ScoreModel = sportModel.list[indexPath.row];
            [self.navigationController pushViewController:SDVC animated:YES];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    view.backgroundColor =FWTextColor_F8F8F8;
    
    UIImageView * quanImageView = [[UIImageView alloc] init];
    quanImageView.image = [UIImage imageNamed:@"quan"];
    [view addSubview:quanImageView];
    [quanImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(14, 14));
        make.left.mas_equalTo(view).mas_offset(12);
        make.centerY.mas_equalTo(view);
    }];
    
    UIView * verView = [[UIView alloc] init];
    verView.backgroundColor = FWTextColor_BCBCBC;
    [view addSubview:verView];
    [verView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.width.mas_equalTo(1.5);
        if (section == 0) {
            make.top.mas_equalTo(quanImageView);
            make.bottom.mas_equalTo(view);
        }else{
            make.top.bottom.mas_equalTo(view);
        }
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    [view bringSubviewToFront:quanImageView];
    
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    titleLabel.textColor = FWTextColor_BCBCBC;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:titleLabel];
    titleLabel.frame  =CGRectMake(30, 0, 200, 30);
    
    if (section < self.archivesModel.sport_list.count && self.archivesModel.sport_list[section].year.length > 0) {
        titleLabel.text = [NSString stringWithFormat:@"%@                                 ",self.archivesModel.sport_list[section].year];
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30.f;
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)shareButtonClick{
    
    FWPlayerSportModel * sportModel = (FWPlayerSportModel *)self.dataSource[0];
    
    FWPlayerInfoShareViewController * vc = [[FWPlayerInfoShareViewController alloc] init];
    vc.vcType = 2;
    vc.share_url = self.archivesModel.qrcode_url;
    vc.partString = self.archivesModel.sport_count;
    vc.detailModel = sportModel.list[0];
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = self.infoTableView.contentOffset;
    if (offset.y <= 0) {
        offset.y = 0;
    }
    self.infoTableView.contentOffset = offset;
}
@end
