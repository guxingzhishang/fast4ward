//
//  FWBaomingQrcodeViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWBaomingQrcodeViewController : FWBaseViewController

@property (nonatomic, strong) NSString * qrcode;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * titleString;
@property (nonatomic, assign) NSInteger  exp_time;

@end

NS_ASSUME_NONNULL_END
