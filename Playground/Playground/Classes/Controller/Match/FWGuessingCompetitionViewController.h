//
//  FWGuessingCompetitionViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^countlBlock)(NSString * countString);

@interface FWGuessingCompetitionViewController : FWBaseViewController
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * guess_id;
@property (nonatomic, copy) countlBlock myBlock;

@end

NS_ASSUME_NONNULL_END
