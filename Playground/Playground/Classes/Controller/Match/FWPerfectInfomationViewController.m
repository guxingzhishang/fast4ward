//
//  FWPerfectInfomationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPerfectInfomationViewController.h"
#import "FWPerfectInfomationView.h"

@interface FWPerfectInfomationViewController ()
@property (nonatomic, strong) FWPerfectInfomationView * infomationView;
@property (nonatomic, strong) NSString * errmsg;

@end

@implementation FWPerfectInfomationViewController

#pragma mark - > 请求完善信息页数据
- (void)requestSelectInfo{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"baoming_user_id":self.baoming_user_id,
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_baoming_select_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.errmsg = @"";
            
            self.arrayModel = [FWPerfectInfoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.infomationView configForViewWithArrayModel:self.arrayModel];

        }else{
            self.errmsg = [back objectForKey:@"errmsg"];
            
            if (self.errmsg.length > 0) {
                [[FWHudManager sharedManager] showErrorMessage:self.errmsg toController:self];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
                return ;
            }
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"完善信息页"];
    [IQKeyboardManager sharedManager].enable = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"完善信息页"];
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)setCanEdited:(BOOL)canEdited{
    _canEdited = canEdited;
    
    if (canEdited) {
        self.title = @"完善信息";
    }else{
        self.title = @"我的报名信息";
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_interactivePopDisabled = YES;
    
    [self requestSelectInfo];
    
    self.infomationView = [[FWPerfectInfomationView alloc] init];
    self.infomationView.vc = self;
    [self.view addSubview:self.infomationView];
    [self.infomationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    self.infomationView.baoming_user_id = self.baoming_user_id;
    self.infomationView.canEdited = self.canEdited;
}

- (void)backBtnClick{
    
    if (self.canEdited) {
        /* 可编辑情况下走逻辑 */
        [self.infomationView endEditing:YES];
        [self.infomationView cancelButtonClick];
        
        if ([self.infomationView.originalDict isEqual:[self.infomationView getParams]]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"是否保存修改信息？" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"暂不" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.infomationView saveButtonClick];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }else{
        /* 查看信息下，直接返回 */
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
