//
//  FWPlayerInfoShareViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerInfoShareViewController.h"

@interface FWPlayerInfoShareViewController ()

@property (nonatomic, strong) UIView * mainView;
@property (nonatomic, strong) UIImageView * bgView;
@property (nonatomic, strong) UIImageView * typeIconView;
@property (nonatomic, strong) UIImageView * topIconView;
@property (nonatomic, strong) UILabel * quanqiuqicheLabel;
@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * partLabel;
@property (nonatomic, strong) UILabel * scoreLabel;
@property (nonatomic, strong) UILabel * latestSroeLabel;
@property (nonatomic, strong) UILabel * groupLabel;
@property (nonatomic, strong) UILabel * carInfo;
@property (nonatomic, strong) UILabel * groupInfo;
@property (nonatomic, strong) UILabel * rubangLabel;
@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UIButton * friendButton;
@property (nonatomic, strong) UIButton * sessionButton;

@end

@implementation FWPlayerInfoShareViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.vcType == 1) {
        [self trackPageBegin:@"成绩详情分享页"];
    }else{
        [self trackPageBegin:@"车手档案分享页"];
    }
   
    self.view.backgroundColor = FWColorWihtAlpha(@"000000", 0.6);
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    if (self.vcType == 1) {
        [self trackPageEnd:@"成绩详情分享页"];
    }else{
        [self trackPageEnd:@"车手档案分享页"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideVC)];
    [self.view addGestureRecognizer:tap];
    
    self.mainView = [[UIView alloc] init];
    self.mainView.layer.cornerRadius = 4;
    self.mainView.layer.masksToBounds = YES;
    [self.view addSubview:self.mainView];
    [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset((SCREEN_HEIGHT-(408*(SCREEN_WIDTH-48)/327))/3);
        make.left.mas_equalTo(self.view).mas_offset(24);
        make.width.mas_equalTo(SCREEN_WIDTH-48);
        make.height.mas_equalTo(408*(SCREEN_WIDTH-48)/327);
    }];
    
    
    self.bgView = [[UIImageView alloc] init];
    self.bgView.image = [UIImage imageNamed:@"playerinfo_share_bg"];
    [self.mainView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.mainView);
    }];
    
    self.topIconView = [[UIImageView alloc] init];
    self.topIconView.image = [UIImage imageNamed:@"hot_live_share_topicon"];
    [self.bgView addSubview:self.topIconView];
    [self.topIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView).mas_offset(16);
        make.left.mas_equalTo(self.bgView).mas_offset(21);
        make.width.mas_equalTo(64);
        make.height.mas_equalTo(30);
    }];
    
    UIView * lineOneView = [[UIView alloc] init];
    lineOneView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.bgView addSubview:lineOneView];
    [lineOneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topIconView.mas_right).mas_offset(7);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(23);
        make.centerY.mas_equalTo(self.topIconView);
    }];
    
    self.quanqiuqicheLabel = [[UILabel alloc] init];
    self.quanqiuqicheLabel.text = @"全球汽车文化引领者";
    self.quanqiuqicheLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
    self.quanqiuqicheLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.quanqiuqicheLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.quanqiuqicheLabel];
    [self.quanqiuqicheLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topIconView);
        make.size.mas_equalTo(CGSizeMake(150, 20));
        make.left.mas_equalTo(self.topIconView.mas_right).mas_offset(14);
    }];
    
    self.typeIconView = [[UIImageView alloc] init];
    [self.bgView addSubview:self.typeIconView];
    [self.typeIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(28, 41));
        make.centerY.mas_equalTo(self.quanqiuqicheLabel);
        make.right.mas_equalTo(self.bgView.mas_right).mas_offset(-23);
    }];
    if ([_detailModel.type isEqualToString:@"04"]) {
        self.typeIconView.image = [UIImage imageNamed:@"white_type_04"];
    }else{
        self.typeIconView.image = [UIImage imageNamed:@"white_type_02"];
    }
    
    
    UIView * lineTwoView = [[UIView alloc] init];
    lineTwoView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.bgView addSubview:lineTwoView];
    [lineTwoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(14);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.width.mas_greaterThanOrEqualTo(100);
        make.height.mas_equalTo(1);
        make.centerX.mas_equalTo(self.bgView);
        make.top.mas_equalTo(self.topIconView.mas_bottom).mas_offset(15);
    }];
    
    UIView * photoBgView = [[UIView alloc] init];
    photoBgView.layer.cornerRadius = 34/2;
    photoBgView.layer.masksToBounds = YES;
    photoBgView.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.37);
    [self.bgView addSubview:photoBgView];
    [photoBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(34, 34));
        make.top.mas_equalTo(lineTwoView.mas_bottom).mas_offset(13);
        make.left.mas_equalTo(self.bgView).mas_offset(26);
    }];
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 29/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [photoBgView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29, 29));
        make.centerX.mas_equalTo(photoBgView);
        make.centerY.mas_equalTo(photoBgView);
    }];
    
    
    self.partLabel = [[UILabel alloc] init];
    self.partLabel.layer.borderColor = FWViewBackgroundColor_FFFFFF.CGColor;
    self.partLabel.layer.borderWidth = 1;
    self.partLabel.layer.cornerRadius = 5;
    self.partLabel.layer.masksToBounds = YES;
    self.partLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    self.partLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.partLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.partLabel];
    [self.partLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(photoBgView);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(80);
        make.right.mas_equalTo(self.bgView).mas_offset(-29);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(photoBgView);
        make.height.mas_equalTo(20);
        make.width.mas_greaterThanOrEqualTo(100);
        make.left.mas_equalTo(photoBgView.mas_right).mas_offset(4);
        make.right.mas_equalTo(self.partLabel.mas_left).mas_offset(-10);
    }];
    
    self.latestSroeLabel = [[UILabel alloc] init];
    self.latestSroeLabel.text = @"最新战绩";
    self.latestSroeLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.latestSroeLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.latestSroeLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.latestSroeLabel];
    [self.latestSroeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(photoBgView);
        make.top.mas_equalTo(photoBgView.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(250, 22));
    }];
    
    self.scoreLabel = [[UILabel alloc] init];
    self.scoreLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    self.scoreLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.scoreLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.scoreLabel];
    [self.scoreLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.centerY.mas_equalTo(self.latestSroeLabel);
        make.height.mas_equalTo(22);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    self.groupLabel = [[UILabel alloc] init];
    self.groupLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.groupLabel.textColor = FWColorWihtAlpha(@"ffffff", 0.4);
    self.groupLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.groupLabel];
    [self.groupLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.latestSroeLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(17);
        make.width.mas_greaterThanOrEqualTo(100);
        make.left.mas_equalTo(self.latestSroeLabel);
    }];
    
    self.carInfo = [[UILabel alloc] init];
    self.carInfo.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.carInfo.textColor = FWColorWihtAlpha(@"ffffff", 0.4);
    self.carInfo.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.carInfo];
    [self.carInfo mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.groupLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(17);
        make.width.mas_greaterThanOrEqualTo(100);
        make.left.mas_equalTo(self.latestSroeLabel);
    }];
    
    self.groupInfo = [[UILabel alloc] init];
    self.groupInfo.text = @"";
    self.groupInfo.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.groupInfo.textColor = FWColorWihtAlpha(@"ffffff", 0.4);
    self.groupInfo.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.groupInfo];
    [self.groupInfo mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.carInfo.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(17);
        make.width.mas_greaterThanOrEqualTo(100);
        make.left.mas_equalTo(self.latestSroeLabel);
    }];
    
    self.rubangLabel = [[UILabel alloc] init];
    self.rubangLabel.text = @"入榜成绩";
    self.rubangLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    self.rubangLabel.textColor = FWColorWihtAlpha(@"ffffff", 1);
    self.rubangLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.rubangLabel];
    [self.rubangLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.groupInfo.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(17);
        make.width.mas_greaterThanOrEqualTo(100);
        make.left.mas_equalTo(self.latestSroeLabel);
    }];
    
    NSArray * titleArray = @[@"ET时间",@"RT时间",@"尾速"];
    NSArray * imageArray = @[@"player_share_et",@"playerinfo_share_rt",@"playerinfo_share_vspeed"];

    for (int i = 0; i<3; i++) {
        UIView * partView = [[UIView alloc] init];
        [self.bgView addSubview:partView];
        [partView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.rubangLabel.mas_bottom).mas_offset(7);
            make.height.mas_equalTo(40);
            make.width.mas_equalTo((SCREEN_WIDTH-120)/3);
            make.left.mas_equalTo(self.bgView).mas_offset(24+((SCREEN_WIDTH-120)/3+12)*i);
        }];
        
        UILabel * timeLabel = [[UILabel alloc] init];
        timeLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
        if (i == 0)      timeLabel.textColor = FWColor(@"#53C0FF");
        else if (i == 1) timeLabel.textColor = FWColor(@"#3960EE");
        else if (i == 2) timeLabel.textColor = FWColor(@"#FFA445");
        timeLabel.textAlignment = NSTextAlignmentLeft;
        timeLabel.text = titleArray[i];
        [partView addSubview:timeLabel];
        [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(partView);
            make.size.mas_equalTo(CGSizeMake(40, 13));
        }];
        
        UIImageView * timeImageView = [[UIImageView alloc] init];
        timeImageView.image = [UIImage imageNamed:imageArray[i]];
        [partView addSubview:timeImageView];
        [timeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.mas_equalTo(partView);
            make.height.mas_equalTo(8);
            make.width.mas_equalTo(85);
        }];
        
        UILabel * timeValueLabel = [[UILabel alloc] init];
        timeValueLabel.tag = 10086+i;
        timeValueLabel.font =  [UIFont fontWithName:@"PingFangSC-Medium" size: 20];
        if (i == 0){
            timeValueLabel.textColor = FWColor(@"#53C0FF");
            timeValueLabel.text = self.detailModel.et;
        }else if (i == 1){
            timeValueLabel.textColor = FWColor(@"#3960EE");
            timeValueLabel.text = self.detailModel.rt;
        }else if (i == 2){
            timeValueLabel.textColor = FWColor(@"#FFA445");
            timeValueLabel.text = self.detailModel.vspeed;
        }
        timeValueLabel.textAlignment = NSTextAlignmentLeft;
        [partView addSubview:timeValueLabel];
        [timeValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(partView);
            make.height.mas_equalTo(30);
            make.width.mas_greaterThanOrEqualTo(10);
            make.left.mas_equalTo(partView).mas_offset(FWAdaptive(40));
        }];
    }
//
//    UIView * lineThreeView = [[UIView alloc] init];
//    lineThreeView.backgroundColor = FWViewBackgroundColor_FFFFFF;
//    [self.bgView addSubview:lineThreeView];
//    [lineThreeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.bgView).mas_offset(14);
//        make.right.mas_equalTo(self.bgView).mas_offset(-14);
//        make.width.mas_greaterThanOrEqualTo(100);
//        make.height.mas_equalTo(1);
//        make.centerX.mas_equalTo(self.bgView);
//        make.top.mas_equalTo(self.groupInfo.mas_bottom).mas_offset(69);
//    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"打开【肆放】APP\r\n即刻查看更多车手成绩";
    self.tipLabel.numberOfLines = 0;
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    self.tipLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.groupInfo.mas_bottom).mas_offset(70);
        make.height.mas_greaterThanOrEqualTo(40);
        make.width.mas_equalTo(140);
        make.left.mas_equalTo(self.bgView).mas_offset(33);
        make.bottom.mas_equalTo(self.bgView);
    }];
    
    UIImageView * qrcodeView = [[UIImageView alloc] init];
    qrcodeView.contentMode = UIViewContentModeScaleAspectFill;
    qrcodeView.clipsToBounds = YES;
    qrcodeView.layer.cornerRadius = 5;
    qrcodeView.layer.masksToBounds = YES;
    [qrcodeView sd_setImageWithURL:[NSURL URLWithString:self.share_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self.bgView addSubview:qrcodeView];
    [qrcodeView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.tipLabel);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.size.mas_equalTo(CGSizeMake(79, 79));
    }];
    
    self.friendButton = [[UIButton alloc] init];
    self.friendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.friendButton setImage:[UIImage imageNamed:@"share_weixinhaoyou"] forState:UIControlStateNormal];
    [self.friendButton setTitle:@"   微信好友" forState:UIControlStateNormal];
    [self.friendButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    self.friendButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.view addSubview:self.friendButton];
    [self.friendButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.friendButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_bottom).mas_offset(35);
        make.height.mas_equalTo(47);
        make.width.mas_equalTo(140);
        make.left.mas_equalTo(self.bgView).mas_offset(14);
    }];
    
    self.sessionButton = [[UIButton alloc] init];
    self.sessionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.sessionButton setImage:[UIImage imageNamed:@"share_pengyouquan"] forState:UIControlStateNormal];
    [self.sessionButton setTitle:@"   朋友圈" forState:UIControlStateNormal];
    [self.sessionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    self.sessionButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    [self.view addSubview:self.sessionButton];
    [self.sessionButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.sessionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.friendButton);
        make.height.mas_equalTo(self.friendButton);
        make.width.mas_equalTo(self.friendButton);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
    }];

    self.partLabel.text = [NSString stringWithFormat:@"参与比赛%@次",self.partString];
    self.groupLabel.text = self.detailModel.car_group;
    
    if (self.detailModel.club_name.length > 0) {
        self.groupInfo.text = [NSString stringWithFormat:@"车队：%@",self.detailModel.club_name];
    }
    
    if (self.vcType == 1) {
        /* 成绩详情 */
        self.partLabel.hidden = YES;
        self.nameLabel.text = self.detailModel.nick;
        self.latestSroeLabel.text = self.detailModel.sport_name;
        self.carInfo.text = [NSString stringWithFormat:@"车型：%@ %@",self.detailModel.brand,self.detailModel.model];
        [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.detailModel.header]];
    }else{
        /* 车手档案 */
        self.partLabel.hidden = NO;
        self.nameLabel.text = self.detailModel.nickname;
        self.latestSroeLabel.text = @"最新战绩";
        self.scoreLabel.text = self.detailModel.sport_name;
        self.carInfo.text = [NSString stringWithFormat:@"车型：%@ %@",self.detailModel.car_brand,self.detailModel.car_type];
        [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:self.detailModel.header_url]];
    }
}

- (void)setDetailModel:(FWScoreDetailModel *)detailModel{
    _detailModel = detailModel;
}

#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{
    
    if (sender == self.sessionButton) {
        [self actionForScreenShotWith:self.bgView savePhoto:NO withSence:WXSceneTimeline];
    }else{
        [self actionForScreenShotWith:self.bgView savePhoto:NO withSence:WXSceneSession];
    }
}

- (void)WXSendImage:(UIImage *)image withShareScene:(enum WXScene)scene {
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        WXImageObject *ext = [WXImageObject object];
        // 小于10MB
        ext.imageData = imageData;
        
        WXMediaMessage *message = [WXMediaMessage message];
        message.mediaObject = ext;
        
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.scene = scene;
        req.message = message;
        [WXApi sendReq:req];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }else {
        // 提示用户安装微信
        [[FWHudManager sharedManager] showErrorMessage:@"您还没装微信哦~" toController:self];
    }
}

#pragma mark - > 截屏
- (void)actionForScreenShotWith:(UIView *)aimView savePhoto:(BOOL)savePhoto withSence:(enum WXScene)scene {
    
    if (!aimView) return;
    
    UIGraphicsBeginImageContextWithOptions(aimView.bounds.size, NO, 0.0f);
    [aimView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (savePhoto) {
        UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }else{
        [self WXSendImage:viewImage withShareScene:scene];
    }
}

#pragma mark - >  保存到本地相册
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    if (error) {
        NSLog(@"保存失败，请重试");
    } else {
        NSLog(@"保存成功");
    }
}

#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {
    
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}
- (void)hideVC{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
