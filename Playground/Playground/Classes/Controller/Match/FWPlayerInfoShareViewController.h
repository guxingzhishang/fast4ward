//
//  FWPlayerInfoShareViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 车手档案 / 车手成绩详情分享
 */
#import "FWBaseViewController.h"
#import "FWPlayerRankModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerInfoShareViewController : FWBaseViewController

@property (nonatomic, assign) NSInteger  vcType; // 1 :成绩详情 ； 2：车手档案
@property (nonatomic, strong) FWScoreDetailModel * detailModel;
@property (nonatomic, strong) NSString * partString;
@property (nonatomic, strong) NSString * share_url;

@end

NS_ASSUME_NONNULL_END
