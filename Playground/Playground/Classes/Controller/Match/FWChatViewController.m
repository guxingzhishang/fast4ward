//
//  FWChatViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/8.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWChatViewController.h"

#import "RCCRInputBarControl.h"
#import "RCCRMessageModel.h"
#import "RCCRUtilities.h"
#import "RCCRMessageBaseCell.h"
#import "RCCRHostInformationView.h"
#import "RCCRgiftListView.h"
#import "RCCRLoginView.h"
#import "RCCRRongCloudIMManager.h"
#import "RCCRTextMessageCell.h"
#import "UIView+RCDDanmaku.h"
#import "RCDDanmaku.h"
#import "RCDDanmakuManager.h"

#define kRandomColor [UIColor colorWithRed:arc4random_uniform(256) / 255.0 green:arc4random_uniform(256) / 255.0 blue:arc4random_uniform(256) / 255.0 alpha:1]

#import "RCChatroomWelcome.h"
#import "RCChatroomGift.h"
#import "RCCRManager.h"
#import "RCChatroomLike.h"
#import "RCChatroomBarrage.h"
#import "RCChatroomFollow.h"
#import "RCChatroomUserQuit.h"
#import "RCCRGiftNumberLabel.h"
#import "RCChatroomStart.h"
#import "RCChatroomEnd.h"
#import "RCChatroomUserBan.h"
#import "RCChatroomUserUnBan.h"
#import "RCChatroomUserBlock.h"
#import "RCChatroomUserUnBlock.h"
#import "RCChatroomNotification.h"
static NSString * const banNotifyContent = @"您已被管理员禁言";

#define kRandomColor [UIColor colorWithRed:arc4random_uniform(256) / 255.0 green:arc4random_uniform(256) / 255.0 blue:arc4random_uniform(256) / 255.0 alpha:1]

static NSString * const ConversationMessageCollectionViewCell = @"ConversationMessageCollectionViewCell";

/**
 *  文本cell标示
 */
static NSString *const textCellIndentifier = @"textCellIndentifier";

static NSString *const startAndEndCellIndentifier = @"startAndEndCellIndentifier";

@interface FWChatViewController () <UICollectionViewDelegate, UICollectionViewDataSource, RCCRInputBarControlDelegate, UIGestureRecognizerDelegate, RCCRLoginViewDelegate, RCCRGiftViewDelegate, RCCRHostInformationViewDelegate>

/*!
 消息列表CollectionView和输入框都在这个view里
 */
@property(nonatomic, strong) UIView *messageContentView;

/*!
 会话页面的CollectionView
 */
@property(nonatomic, strong) UICollectionView *conversationMessageCollectionView;

/**
 输入工具栏
 */
@property(nonatomic,strong) RCCRInputBarControl *inputBar;

/**
 底部按钮容器，底部的四个按钮都添加在此view上
 */
@property(nonatomic, strong) UIView *bottomBtnContentView;

/**
 *  评论按钮
 */
@property(nonatomic,strong)UIButton *commentBtn;

/**
 登录的界面
 */
@property(nonatomic,strong)RCCRLoginView *loginView;


/**
 *  是否需要滚动到底部
 */
@property(nonatomic, assign) BOOL isNeedScrollToButtom;

/**
 *  滚动条不在底部的时候，接收到消息不滚动到底部，记录未读消息数
 */
@property (nonatomic, assign) NSInteger unreadNewMsgCount;

/*!
 聊天内容的消息Cell数据模型的数据源
 
 @discussion 数据源中存放的元素为消息Cell的数据模型，即RCDLiveMessageModel对象。
 */
@property(nonatomic, strong) NSMutableArray<RCCRMessageModel *> *conversationDataRepository;

@end

@implementation FWChatViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self rcinit];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self rcinit];
    }
    return self;
}

- (void)rcinit {
    self.conversationDataRepository = [[NSMutableArray alloc] init];
    self.conversationMessageCollectionView = nil;
    [self registerNotification];
    self.defaultHistoryMessageCountOfChatRoom = 0;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.view.backgroundColor = FWViewBackgroundColor_F0F0F0;

    [self initializedSubViews];
    
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] setIsLogin:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendAndShowMessage:) name:RongYunRecieveNofi object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteMessage:) name:RongYunChangeRoomNofi object:nil];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self trackPageBegin:@"比赛直播聊天页"];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    
    if (![self.currentID isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        [self requestRongYunTokenChatroom];
    }
}

- (void)requestRongYunTokenChatroom{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_chatroom_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            
                                            [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo = userInfo;
                                            
                                            [[RCIM sharedRCIM]refreshUserInfoCache:userInfo withUserId:userId];
                                            [self joinChatRoom];
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"聊天室登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            /* token过期或者不正确。
                                             * 如果设置了token有效期并且token过期，请重新请求您的服务器获取新的token
                                             * 如果没有设置token有效期却提示token错误，请检查您客户端和服务器的appkey是否匹配，还有检查您获取token的流程。*/
                                            NSLog(@"聊天室token错误");
                                        }];
        }
    }];
}


- (void)joinChatRoom{
    
    //聊天室类型进入时需要调用加入聊天室接口，退出时需要调用退出聊天室接口
    DHWeakSelf;
    [[RCIMClient sharedRCIMClient]
     joinChatRoom:self.targetId
     messageCount:-1
     success:^{
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"] &&
                 [[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
                 
                 RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                 
                 [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo = userInfo;
                 
                 RCChatroomWelcome *message = [[RCChatroomWelcome alloc] init];
                 message.id = userInfo.userId;
                 message.extra = userInfo.name;
                 message.senderUserInfo = userInfo;
                 [weakSelf sendMessage:message pushContent:nil success:nil error:nil];
             }
         });
         weakSelf.currentID = [GFStaticData getObjectForKey:kTagUserKeyID];
     }
     error:^(RCErrorCode status) {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (status != KICKED_FROM_CHATROOM) {
                 // 提示错误信息
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [weakSelf joinChatRoom];
                 });
             }
         });
     }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self trackPageEnd:@"比赛直播聊天页"];
    [[RCCRManager sharedRCCRManager] setUserUnban];
}

/**
 *  注册监听Notification
 */
- (void)registerNotification {
    //注册接收消息
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didReceiveMessageNotification:)
     name:RCCRKitDispatchMessageNotification
     object:nil];
}

#pragma mark <UIScrollViewDelegate,UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {

    return self.conversationDataRepository.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
 
    RCCRMessageModel *model =
    [self.conversationDataRepository objectAtIndex:indexPath.row];
    RCMessageContent *messageContent = model.content;
    RCCRMessageBaseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ConversationMessageCollectionViewCell forIndexPath:indexPath];;
    if ([messageContent isMemberOfClass:[RCTextMessage class]] ||
        [messageContent isMemberOfClass:[RCChatroomWelcome class]] ||
        [messageContent isMemberOfClass:[RCChatroomFollow class]] ||
        [messageContent isMemberOfClass:[RCChatroomLike class]] ||
        [messageContent isMemberOfClass:[RCChatroomStart class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserBan class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserUnBan class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserBlock class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserUnBlock class]] ||
        [messageContent isMemberOfClass:[RCChatroomNotification class]] ||
        [messageContent isMemberOfClass:[RCChatroomEnd class]] ||
        [messageContent isMemberOfClass:[RCChatroomUserQuit class]]){
        RCCRTextMessageCell *__cell = nil;
        NSString *indentifier = nil;
        if ([messageContent isMemberOfClass:[RCChatroomStart class]] ||
            [messageContent isMemberOfClass:[RCChatroomEnd class]]) {
            indentifier = startAndEndCellIndentifier;
        } else {
            indentifier = textCellIndentifier;
        }
        __cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
        [__cell setDataModel:model];
        cell = __cell;
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RCCRMessageModel *model = self.conversationDataRepository[indexPath.row];
    if ([model.content isKindOfClass:[RCChatroomStart class]] || [model.content isKindOfClass:[RCChatroomEnd class]]) {
        return CGSizeMake(SCREEN_WIDTH-20,30);
    }else{
        if ([model.content isMemberOfClass:[RCTextMessage class]] &&
            model.senderUserId ) {
            RCTextMessage *textMessage = (RCTextMessage *)model.content;
           
            NSString *userName = textMessage.senderUserInfo.name;
            NSString *str =[NSString stringWithFormat:@"%@:   %@",userName,textMessage.content];
            
            NSMutableAttributedString * messageStringAttr = [[NSMutableAttributedString alloc] initWithString:str];
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.paragraphSpacing = 10;// 字体的行间距
            
            messageStringAttr.yy_font = DHSystemFontOfSize_14;
            messageStringAttr.yy_paragraphStyle = paragraphStyle;
            
            CGSize  textLimitSize = CGSizeMake(SCREEN_WIDTH - 40, MAXFLOAT);
            CGFloat textHeight = [YYTextLayout layoutWithContainerSize:textLimitSize text:messageStringAttr].textBoundingSize.height;

            return CGSizeMake(SCREEN_WIDTH-20,textHeight+10);
        }
        return CGSizeMake(SCREEN_WIDTH-20,30);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HideKeyboard object:nil];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
        return 0.f;
}

/**
 *  消息滚动到底部
 *
 *  @param animated 是否开启动画效果
 */
- (void)scrollToBottomAnimated:(BOOL)animated {
    if ([self.conversationMessageCollectionView numberOfSections] == 0) {
        return;
    }
    NSUInteger finalRow = MAX(0, [self.conversationMessageCollectionView numberOfItemsInSection:0] - 1);
    if (0 == finalRow) {
        return;
    }
    NSIndexPath *finalIndexPath =
    [NSIndexPath indexPathForItem:finalRow inSection:0];
    [self.conversationMessageCollectionView scrollToItemAtIndexPath:finalIndexPath
                                                   atScrollPosition:UICollectionViewScrollPositionTop
                                                           animated:animated];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 是否显示右下未读icon
    if (self.unreadNewMsgCount != 0) {
        [self checkVisiableCell];
    }
}

//  发送消息
- (void)onTouchSendButton:(NSString *)text {
    //判断是否禁言
    if ([RCCRManager sharedRCCRManager].isBan) {
        [self insertNotificationMessage:banNotifyContent];
    } else {
        [self touristSendMessage:text];
    }
}

- (void)touristSendMessage:(NSString *)text {
   
    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:text];
    rcTextMessage.senderUserInfo = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo;
    [self sendMessage:rcTextMessage pushContent:nil success:nil error:nil];
}

#pragma mark sendMessage/showMessage
/**
 发送消息
 
 @param messageContent 消息
 @param pushContent pushContent
 */
- (void)sendMessage:(RCMessageContent *)messageContent
        pushContent:(NSString *)pushContent
            success:(void (^)(long messageId))successBlock
              error:(void (^)(RCErrorCode nErrorCode, long messageId))errorBlock {
    if (_targetId == nil) {
        return;
    }
    messageContent.senderUserInfo = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo;
    if (messageContent == nil) {
        return;
    }
    
    __weak typeof(&*self) __weakself = self;
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] sendMessage:self.conversationType
                                                              targetId:self.targetId
                                                               content:messageContent
                                                           pushContent:pushContent
                                                              pushData:nil success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            RCMessage *message = [[RCMessage alloc] initWithType:__weakself.conversationType
                                                        targetId:__weakself.targetId
                                                       direction:MessageDirection_SEND
                                                       messageId:messageId
                                                         content:messageContent];
            //  过滤礼物消息，弹幕消息,退出聊天室消息不插入数据源中；
            if ([messageContent isMemberOfClass:[RCChatroomGift class]] || [messageContent isMemberOfClass:[RCChatroomBarrage class]] || [messageContent isMemberOfClass:[RCChatroomUserQuit class]]) {
            } else {
                message.senderUserId = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId;
                
                [__weakself appendAndDisplayMessage:message];
                [__weakself.inputBar clearInputView];
            }
        });
        if (successBlock) {
            successBlock(messageId);
        }
    } error:^(RCErrorCode nErrorCode, long messageId) {
        [__weakself.inputBar clearInputView];
        NSLog(@"发送失败，errorcode is: %ld",(long)nErrorCode);
        if (errorBlock) {
            errorBlock(nErrorCode, messageId);
        }
    }];
    
}

/**
 *  接收到消息的回调
 */
- (void)didReceiveMessageNotification:(NSNotification *)notification {
    __block RCMessage *rcMessage = notification.object;
    RCCRMessageModel *model = [[RCCRMessageModel alloc] initWithMessage:rcMessage];
    
    if (model.conversationType == self.conversationType &&
        [model.targetId isEqual:self.targetId]) {
        __weak typeof(&*self) __blockSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            //  对礼物消息,赞消息进行拦截，展示动画，不插入到数据源中,对封禁消息，弹出alert
            if (rcMessage) {
                if ([rcMessage.content isMemberOfClass:[RCChatroomWelcome class]]) {
                    //  过滤自己发送的欢迎消息
                    if ([rcMessage.senderUserId isEqualToString:[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId]) {
                        return;
                    }
                } else if ([rcMessage.content isMemberOfClass:[RCChatroomUserBlock class]]) {
                    RCChatroomUserBlock *blockMessage = (RCChatroomUserBlock*)rcMessage.content;
                    if ([blockMessage.id isEqualToString:[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId]) {
                        [__blockSelf presentAlert:@"您被管理员踢出聊天室"];
                    }
                }
                
                NSDictionary *leftDic = notification.userInfo;
                if (leftDic && [leftDic[@"left"] isEqual:@(0)]) {
                    __blockSelf.isNeedScrollToButtom = YES;
                }
                [__blockSelf appendAndDisplayMessage:rcMessage];
                UIMenuController *menu = [UIMenuController sharedMenuController];
                menu.menuVisible=NO;
                //如果消息不在最底部，收到消息之后不滚动到底部，加到列表中只记录未读数
                if (![__blockSelf isAtTheBottomOfTableView]) {
                    __blockSelf.unreadNewMsgCount ++ ;
                    [__blockSelf updateUnreadMsgCountLabel];
                }
            }
        });
    }
}

/**
 *  将消息加入本地数组
 */
- (void)appendAndDisplayMessage:(RCMessage *)rcMessage {
    if (!rcMessage) {
        return;
    }
    RCCRMessageModel *model = [[RCCRMessageModel alloc] initWithMessage:rcMessage];
//    model.userInfo = [[RCCRManager sharedRCCRManager]getUserInfo:rcMessage.senderUserId];
    if ([self appendMessageModel:model]) {
        NSIndexPath *indexPath =
        [NSIndexPath indexPathForItem:self.conversationDataRepository.count - 1
                            inSection:0];
        if ([self.conversationMessageCollectionView numberOfItemsInSection:0] !=
            self.conversationDataRepository.count - 1) {
            return;
        }
        //  view刷新
        [self.conversationMessageCollectionView
         insertItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        if ([self isAtTheBottomOfTableView] || self.isNeedScrollToButtom) {
            [self scrollToBottomAnimated:YES];
            self.isNeedScrollToButtom=NO;
        }
    }
    return;
}

- (void)appendAndShowMessage:(NSNotification *)noti {
    
    NSDictionary * dict  = (NSDictionary *)noti.userInfo;
    
    RCMessage * rcMessage = [dict objectForKey:@"rcMessage"];
    
    if (!rcMessage) {
        return;
    }
    RCCRMessageModel *model = [[RCCRMessageModel alloc] initWithMessage:rcMessage];
    //    model.userInfo = [[RCCRManager sharedRCCRManager]getUserInfo:rcMessage.senderUserId];
    if ([self appendMessageModel:model]) {
        NSIndexPath *indexPath =
        [NSIndexPath indexPathForItem:self.conversationDataRepository.count - 1
                            inSection:0];
        if ([self.conversationMessageCollectionView numberOfItemsInSection:0] !=
            self.conversationDataRepository.count - 1) {
            return;
        }
        //  view刷新
        [self.conversationMessageCollectionView
         insertItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        if ([self isAtTheBottomOfTableView] || self.isNeedScrollToButtom) {
            [self scrollToBottomAnimated:YES];
            self.isNeedScrollToButtom=NO;
        }
    }
    return;
}

#pragma mark - > 每次切换聊天室时删除聊天记录
- (void)deleteMessage:(NSNotification *)noti {
    
    if (self.conversationDataRepository.count > 0) {
        [self.conversationDataRepository removeAllObjects];
        [self.conversationMessageCollectionView reloadData];
    }
}

/**
 *  判断消息是否在collectionView的底部
 *
 *  @return 是否在底部
 */
- (BOOL)isAtTheBottomOfTableView {
    if (self.conversationMessageCollectionView.contentSize.height <= self.conversationMessageCollectionView.frame.size.height) {
        return YES;
    }
    if(self.conversationMessageCollectionView.contentOffset.y +200 >= (self.conversationMessageCollectionView.contentSize.height - self.conversationMessageCollectionView.frame.size.height)) {
        return YES;
    }else{
        return NO;
    }
}

/**
 *  更新底部新消息提示显示状态
 */
- (void)updateUnreadMsgCountLabel{
    if (self.unreadNewMsgCount == 0) {
        //        self.unreadButtonView.hidden = YES;
    }
    else{
        //        self.unreadButtonView.hidden = NO;
        //        self.unReadNewMessageLabel.text = @"底部有新消息";
    }
}

/**
 *  检查是否更新新消息提醒
 */
- (void) checkVisiableCell{
    NSIndexPath *lastPath = [self getLastIndexPathForVisibleItems];
    if (lastPath.row >= self.conversationDataRepository.count - self.unreadNewMsgCount || lastPath == nil || [self isAtTheBottomOfTableView] ) {
        self.unreadNewMsgCount = 0;
        [self updateUnreadMsgCountLabel];
    }
}

/**
 *  获取显示的最后一条消息的indexPath
 *
 *  @return indexPath
 */
- (NSIndexPath *)getLastIndexPathForVisibleItems
{
    NSArray *visiblePaths = [self.conversationMessageCollectionView indexPathsForVisibleItems];
    if (visiblePaths.count == 0) {
        return nil;
    }else if(visiblePaths.count == 1) {
        return (NSIndexPath *)[visiblePaths firstObject];
    }
    NSArray *sortedIndexPaths = [visiblePaths sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSIndexPath *path1 = (NSIndexPath *)obj1;
        NSIndexPath *path2 = (NSIndexPath *)obj2;
        return [path1 compare:path2];
    }];
    return (NSIndexPath *)[sortedIndexPaths lastObject];
}

/**
 *  如果当前会话没有这个消息id，把消息加入本地数组
 */
- (BOOL)appendMessageModel:(RCCRMessageModel *)model {
    
    if (!model.content) {
        return NO;
    }
    //这里可以根据消息类型来决定是否显示，如果不希望显示直接return NO
    
    //数量不可能无限制的大，这里限制收到消息过多时，就对显示消息数量进行限制。
    //用户可以手动下拉更多消息，查看更多历史消息。
    if (self.conversationDataRepository.count>100) {
        //                NSRange range = NSMakeRange(0, 1);
        RCCRMessageModel *message = self.conversationDataRepository[0];
        [[RCIMClient sharedRCIMClient]deleteMessages:@[@(message.messageId)]];
        [self.conversationDataRepository removeObjectAtIndex:0];
        [self.conversationMessageCollectionView reloadData];
    }
    
    [self.conversationDataRepository addObject:model];
    return YES;
}


/**
 返回按钮事件
 */
- (void)backButtonItemPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 发言按钮事件
 */
- (void)commentBtnPressed:(id)sender {
    //  判断是否登录了
    if ([[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] isLogin]) {
        //判断是否禁言
        if ([RCCRManager sharedRCCRManager].isBan) {
            [self insertNotificationMessage:banNotifyContent];
        } else {
            [_inputBar setHidden:NO];
            [_inputBar setInputBarStatus:RCCRBottomBarStatusKeyboard];
        }
    } else {
        CGRect frame = self.loginView.frame;
        frame.origin.y -= frame.size.height;
        [self.loginView setHidden:NO];
        __weak __typeof(&*self)weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            [weakSelf.loginView setFrame:frame];
        } completion:nil];
    }
}

#pragma mark - RCCRLoginViewDelegate
- (void)clickLoginBtn:(UIButton *)loginBtn {
    // 登录成功
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] setIsLogin:YES];
    RCChatroomWelcome *joinChatroomMessage = [[RCChatroomWelcome alloc]init];
    [joinChatroomMessage setId:[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId];
    [self sendMessage:joinChatroomMessage pushContent:nil success:nil error:nil];
//    [self setDefaultBottomViewStatus];
}


#pragma mark - ban and block notice

- (void)presentAlert:(NSString *)content {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:content message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)insertNotificationMessage:(NSString *)content {
    RCChatroomNotification *notify = [RCChatroomNotification new];
    notify.content = content;
    RCMessage *message = [[RCMessage alloc] initWithType:self.conversationType
                                                targetId:self.targetId
                                               direction:MessageDirection_SEND
                                               messageId:-1
                                                 content:notify];
    message.senderUserId = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId;
    [self appendAndDisplayMessage:message];
}

#pragma mark - views init
/**
 *  注册cell
 *
 *  @param cellClass  cell类型
 *  @param identifier cell标示
 */
- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier {
    [self.conversationMessageCollectionView registerClass:cellClass
                               forCellWithReuseIdentifier:identifier];
}

- (void)initializedSubViews {
    
    CGFloat wmMenuY = SCREEN_WIDTH *9/16+FWCustomeSafeTop+FWLiveTopPadding+49;
    CGFloat height = SCREEN_HEIGHT -44-(wmMenuY)-50-FWSafeBottom;
    CGSize size = CGSizeMake(SCREEN_WIDTH, height);
    
    
    //  消息展示界面和输入框
    [self.view addSubview:self.messageContentView];
    [_messageContentView setFrame:CGRectMake(0, 0, size.width, height-10)];
    
    [_messageContentView addSubview:self.conversationMessageCollectionView];
    [_conversationMessageCollectionView setFrame:CGRectMake(0, 0, SCREEN_WIDTH,CGRectGetHeight(self.messageContentView.frame))];
    UICollectionViewFlowLayout *customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    customFlowLayout.minimumLineSpacing = 2;
    customFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0.0f,0.f, 0.0f);
    customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [_conversationMessageCollectionView setCollectionViewLayout:customFlowLayout animated:NO completion:nil];
    
    
    [self.view  addSubview:self.inputBar];
    [_inputBar setBackgroundColor: [UIColor whiteColor]];
    [_inputBar setFrame:CGRectMake(0, height-60, size.width , 50)];
    [_inputBar setHidden:YES];

    //  底部按钮
    [self.view addSubview:self.bottomBtnContentView];
    [_bottomBtnContentView setFrame:CGRectMake(0, height+20, size.width, 50)];
    [_bottomBtnContentView setBackgroundColor:[UIColor clearColor]];

    [_bottomBtnContentView addSubview:self.commentBtn];
    [_commentBtn setFrame:CGRectMake(10, 10, 35, 35)];
    
    
    [self.view addSubview:self.loginView];
    [_loginView setFrame:CGRectMake(10, size.height, size.width - 20, ISX ? 214 : 180)];
    [_loginView setBackgroundColor:[UIColor blackColor]];
    [_loginView setHidden:YES];
    
    
    [self registerClass:[RCCRTextMessageCell class]forCellWithReuseIdentifier:textCellIndentifier];
    [self registerClass:[RCCRTextMessageCell class]forCellWithReuseIdentifier:startAndEndCellIndentifier];
}

- (UIView *)messageContentView {
    if (!_messageContentView) {
        _messageContentView = [[UIView alloc] init];
        [_messageContentView setBackgroundColor: [UIColor clearColor]];
    }
    return _messageContentView;
}

- (UICollectionView *)conversationMessageCollectionView {
    if (!_conversationMessageCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        _conversationMessageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [_conversationMessageCollectionView setDelegate:self];
        [_conversationMessageCollectionView setDataSource:self];
        [_conversationMessageCollectionView setBackgroundColor: [UIColor clearColor]];
        [_conversationMessageCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:ConversationMessageCollectionViewCell];
    }
    return _conversationMessageCollectionView;
}

- (RCCRInputBarControl *)inputBar {
    if (!_inputBar) {
        _inputBar = [[RCCRInputBarControl alloc] initWithStatus:RCCRBottomBarStatusDefault];
        [_inputBar setDelegate:self];
    }
    return _inputBar;
}

- (UIView *)bottomBtnContentView {
    if (!_bottomBtnContentView) {
        _bottomBtnContentView = [[UIView alloc] init];
        [_bottomBtnContentView setBackgroundColor:[UIColor clearColor]];
    }
    return _bottomBtnContentView;
}

- (UIButton *)commentBtn {
    if (!_commentBtn) {
        _commentBtn = [[UIButton alloc] init];
        [_commentBtn addTarget:self
                        action:@selector(commentBtnPressed:)
              forControlEvents:UIControlEventTouchUpInside];
        [_commentBtn setImage:[UIImage imageNamed:@"feedback"] forState:UIControlStateNormal];
    }
    return _commentBtn;
}


- (RCCRLoginView *)loginView {
    if (!_loginView) {
        _loginView = [[RCCRLoginView alloc] init];
        [_loginView setDelegate:self];
    }
    return _loginView;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
