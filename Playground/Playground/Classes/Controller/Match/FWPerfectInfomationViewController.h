//
//  FWPerfectInfomationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/1.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWRegistrationHomeModel.h"
#import "FWPerfectInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPerfectInfomationViewController : FWBaseViewController

@property (nonatomic, strong) FWPlayerSignupPerfectInfoModel * perfect_info;
@property (nonatomic, strong) FWPerfectInfoModel * arrayModel;
@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, assign) BOOL  canEdited;

@end

NS_ASSUME_NONNULL_END
