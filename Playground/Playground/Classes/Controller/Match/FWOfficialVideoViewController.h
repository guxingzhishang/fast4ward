//
//  FWOfficialVideoViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/9.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWOfficialVideoViewController : FWBaseViewController

@property (nonatomic, strong) NSString * group_id;


@end

NS_ASSUME_NONNULL_END
