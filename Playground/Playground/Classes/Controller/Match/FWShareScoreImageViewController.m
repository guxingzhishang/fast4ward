//
//  FWShareScoreImageViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/12.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWShareScoreImageViewController.h"

@interface FWShareScoreImageViewController()

@property (nonatomic, strong) UIImageView * containerView;

@property (nonatomic, strong) UIImageView * mainView;

@property (nonatomic, strong) UILabel * titleOneLabel;
@property (nonatomic, strong) UILabel * titleTwoLabel;

@property (nonatomic, strong) UIButton    * pengyouquanButton;
@property (nonatomic, strong) UIButton    * friendButton;

@property (nonatomic, strong) UIImageView * quanOneImageView;
@property (nonatomic, strong) UIImageView * quanTwoImageView;

@property (nonatomic, strong) UIImageView * photoOneImageView;
@property (nonatomic, strong) UIImageView * photoTwoImageView;

@property (nonatomic, strong) UIImageView * yibiaopanImageView;

@property (nonatomic, strong) UILabel * nameOneLabel;
@property (nonatomic, strong) UILabel * nameTwoLabel;

@property (nonatomic, strong) UILabel * carOneLabel;
@property (nonatomic, strong) UILabel * carTwoLabel;

@property (nonatomic, strong) UILabel * ETOneLabel;
@property (nonatomic, strong) UILabel * RTOneLabel;
@property (nonatomic, strong) UILabel * WeisuOneLabel;
@property (nonatomic, strong) UILabel * ETTimeOneLabel;
@property (nonatomic, strong) UILabel * RTTimeOneLabel;
@property (nonatomic, strong) UILabel * WeisuTimeOneLabel;

@property (nonatomic, strong) UILabel * ETTwoLabel;
@property (nonatomic, strong) UILabel * RTTwoLabel;
@property (nonatomic, strong) UILabel * WeisuTwoLabel;
@property (nonatomic, strong) UILabel * ETTimeTwoLabel;
@property (nonatomic, strong) UILabel * RTTimeTwoLabel;
@property (nonatomic, strong) UILabel * WeisuTimeTwoLabel;

@property (nonatomic, strong) UIImageView * ETImageView;
@property (nonatomic, strong) UIImageView * RTImageView;
@property (nonatomic, strong) UIImageView * WeisuImageView;

@property (nonatomic, strong) UIImageView * bettleOneImageView;
@property (nonatomic, strong) UIImageView * bettleTwoImageView;
@property (nonatomic, strong) UIImageView * bettleThreeImageView;

@property (nonatomic, strong) UIImageView * announceView;
@property (nonatomic, strong) UIImageView * codeImageView;
@property (nonatomic, strong) UILabel * tipLabel;

@end

@implementation FWShareScoreImageViewController
@synthesize containerView;
@synthesize mainView;
@synthesize titleOneLabel;
@synthesize titleTwoLabel;
@synthesize pengyouquanButton;
@synthesize friendButton;
@synthesize quanOneImageView;
@synthesize quanTwoImageView;
@synthesize photoOneImageView;
@synthesize photoTwoImageView;
@synthesize yibiaopanImageView;
@synthesize nameOneLabel;
@synthesize nameTwoLabel;
@synthesize carOneLabel;
@synthesize carTwoLabel;
@synthesize ETOneLabel;
@synthesize RTOneLabel;
@synthesize WeisuOneLabel;
@synthesize ETTimeOneLabel;
@synthesize RTTimeOneLabel;
@synthesize WeisuTimeOneLabel;
@synthesize ETTwoLabel;
@synthesize RTTwoLabel;
@synthesize WeisuTwoLabel;
@synthesize ETTimeTwoLabel;
@synthesize RTTimeTwoLabel;
@synthesize WeisuTimeTwoLabel;
@synthesize ETImageView;
@synthesize RTImageView;
@synthesize WeisuImageView;
@synthesize bettleOneImageView;
@synthesize bettleTwoImageView;
@synthesize bettleThreeImageView;
@synthesize shareType;
@synthesize announceView;
@synthesize codeImageView;
@synthesize tipLabel;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [self trackPageBegin:@"比赛分享页"];

    [[NSNotificationCenter defaultCenter] postNotificationName:MonitorLiveNetworking object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"比赛分享页"];
}

/**
 * 一人成绩和双人对决 有分享按钮，没有二维码视图
 * 宣传图片没有分享按钮，有二维码视图
 */
- (void)viewDidLoad{
    [super viewDidLoad];

    [self setupSubviews];
    [self setupShareButton];

    if ([shareType isEqualToString:@"1"]){
        /* 单人 */
        [self setupSingleBettleSubviews];
    }else if ([shareType isEqualToString:@"2"]){
        /* 双人 */
        [self setupDoubleBettleSubviews];
    }else if ([shareType isEqualToString:@"3"]){
        /* 比赛海报 */
        [self setupMatchPostSubviews];
    }
}

#pragma mark - > 分享
- (void)shareButtonOnClick:(UIButton *)sender{
    
    if (sender == pengyouquanButton) {
        
        [self actionForScreenShotWith:containerView savePhoto:NO withSence:WXSceneTimeline];
    }else{
        
        [self actionForScreenShotWith:containerView savePhoto:NO withSence:WXSceneSession];
    }
}


- (void)WXSendImage:(UIImage *)image withShareScene:(enum WXScene)scene {
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [documentPath stringByAppendingPathComponent:@"ocr.jpg"];
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        [imageData writeToFile:filePath atomically:NO];
        
        UIImage *thumbImage = [self compressImage:image toByte:32768];
        
        WXImageObject *ext = [WXImageObject object];
        // 小于10MB
        ext.imageData = imageData;
        
        WXMediaMessage *message = [WXMediaMessage message];
        message.mediaObject = ext;
        // 缩略图 小于32KB
        [message setThumbImage:thumbImage];
        
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.scene = scene;
        req.message = message;
        [WXApi sendReq:req];
    }else {
        // 提示用户安装微信
        [[FWHudManager sharedManager] showErrorMessage:@"您还没装微信哦~" toController:self];
    }
}

#pragma mark - > 截屏
- (void)actionForScreenShotWith:(UIView *)aimView savePhoto:(BOOL)savePhoto withSence:(enum WXScene)scene {
    
    if (!aimView) return;
    
    UIGraphicsBeginImageContextWithOptions(aimView.bounds.size, NO, 0.0f);
    [aimView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (savePhoto) {
        UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }else{
        [self WXSendImage:viewImage withShareScene:scene];
    }
}

#pragma mark - >  保存到本地相册
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    if (error) {
        NSLog(@"保存失败，请重试");
    } else {
        NSLog(@"保存成功");
    }
}

#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {

    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}

- (void)tapNothing{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - > 初始化主视图
- (void)setupSubviews{
    
    UIView * bgView = [[UIView alloc] init];
    bgView.userInteractionEnabled= YES;
    bgView.backgroundColor = FWClearColor;
    [self.view addSubview:bgView];
    [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNothing)];
    [bgView addGestureRecognizer:tap];
    
    containerView = [[UIImageView alloc] init];
    [self.view addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).mas_offset(15);
        make.right.mas_equalTo(self.view).mas_offset(-15);
        make.top.mas_equalTo(self.view).mas_offset((SCREEN_HEIGHT-(SCREEN_WIDTH-30)*283/345)/2-55);
        make.width.mas_equalTo(SCREEN_WIDTH-30);
        make.height.mas_greaterThanOrEqualTo((SCREEN_WIDTH-30)*283/345);
    }];
    
    mainView = [[UIImageView alloc] init];
//    mainView.contentMode = UIViewContentModeScaleAspectFill;
    mainView.clipsToBounds = YES;
    mainView.image = [UIImage imageNamed:@"bettleBG"];
//    mainView.userInteractionEnabled = YES;
    [containerView addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(containerView);
        make.width.mas_equalTo(containerView);
        make.height.mas_equalTo((SCREEN_WIDTH-30)*283/193);
    }];
    
    titleOneLabel = [UILabel new];
    titleOneLabel.font = DHBoldFont(25);
    titleOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    titleOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:titleOneLabel];
    [titleOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mainView);
        make.top.mas_equalTo(mainView).mas_offset(60);
        make.height.mas_equalTo(30);
    }];
    
    titleTwoLabel = [UILabel new];
    titleTwoLabel.font = DHBoldFont(18);
    titleTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    titleTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:titleTwoLabel];
    [titleTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mainView);
        make.top.mas_equalTo(titleOneLabel.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(25);
    }];
    
    announceView = [[UIImageView alloc] init];
    announceView.backgroundColor = FWTextColor_180942;
    [containerView addSubview:announceView];
    [announceView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(90);
        make.left.right.mas_equalTo(containerView);
        make.bottom.mas_equalTo(containerView);
        make.top.mas_equalTo(mainView.mas_bottom);
//        if (SCREEN_HEIGHT>667) {
//            make.top.mas_equalTo(mainView.mas_bottom).mas_equalTo(-5);
//        }else{
//            make.top.mas_equalTo(mainView.mas_bottom).mas_equalTo(-10);
//        }
    }];
    
    codeImageView = [[UIImageView alloc] init];
    codeImageView.image = [UIImage imageNamed:@"match_QRCode"];
    codeImageView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [announceView addSubview:codeImageView];
    [codeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(55, 55));
        make.top.mas_equalTo(announceView).mas_offset(17);
        make.left.mas_equalTo(announceView).mas_offset(57);
    }];
    
    tipLabel = [[UILabel alloc] init];
    tipLabel.text = @"长按图中二维码\n下载【肆放】APP观看现场直播";
    tipLabel.numberOfLines = 0;
    tipLabel.font = DHSystemFontOfSize_12;
    tipLabel.textColor = FWViewBackgroundColor_FFFFFF;
    tipLabel.textAlignment = NSTextAlignmentLeft;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:tipLabel.text];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [tipLabel.text length])];
    tipLabel.attributedText = attributedString;
    [announceView addSubview:tipLabel];
    [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(codeImageView.mas_right).mas_offset(13);
        make.top.mas_equalTo(announceView).mas_offset(0);
        make.height.mas_equalTo(announceView);
        make.width.mas_equalTo(200);
    }];
    
    titleOneLabel.text = self.scoreModel.img_title_1;
    titleTwoLabel.text = self.scoreModel.img_title_2;
}

#pragma mark - > 初始化分享按钮
- (void)setupShareButton{
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        
        CGFloat buttonHeight = 37;
        
        pengyouquanButton = [[UIButton alloc] init];
        pengyouquanButton.layer.cornerRadius = buttonHeight/2;
        pengyouquanButton.layer.masksToBounds = YES;
        [pengyouquanButton setImage:[UIImage imageNamed:@"live_pengyouquan"] forState:UIControlStateNormal];
        [pengyouquanButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pengyouquanButton];
        [pengyouquanButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(80);
            make.left.mas_equalTo((SCREEN_WIDTH-160-27)/2);
            make.height.mas_equalTo(buttonHeight);
            if (SCREEN_HEIGHT<=667 && ![self.shareType isEqualToString:@"3"]) {
                make.top.mas_equalTo(announceView.mas_bottom).mas_offset(5);
            }else{
                make.top.mas_equalTo(announceView.mas_bottom).mas_offset(33);
            }
        }];
        
        friendButton = [[UIButton alloc] init];
        [friendButton setImage:[UIImage imageNamed:@"live_friend"] forState:UIControlStateNormal];
        friendButton.layer.cornerRadius = buttonHeight/2;
        friendButton.layer.masksToBounds = YES;
        [friendButton addTarget:self action:@selector(shareButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:friendButton];
        [friendButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(pengyouquanButton);
            make.right.mas_equalTo(-(SCREEN_WIDTH-160-27)/2);
            make.height.mas_equalTo(pengyouquanButton);
            make.top.mas_equalTo(pengyouquanButton);
        }];
    }
}

#pragma mark - > 初始化两人对决视图
- (void)setupDoubleBettleSubviews{
    
    quanOneImageView = [UIImageView new];
    quanOneImageView.image = [UIImage imageNamed:@"live_touxiang"];
    [mainView addSubview:quanOneImageView];
    [quanOneImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(mainView).mas_offset(171);
        make.left.mas_equalTo(((SCREEN_WIDTH-30)-240-43)/2);
        make.size.mas_equalTo(CGSizeMake(120, 120));
    }];
    
    quanTwoImageView = [UIImageView new];
    quanTwoImageView.image = [UIImage imageNamed:@"live_touxiang"];
    [mainView addSubview:quanTwoImageView];
    [quanTwoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(quanOneImageView);
        make.right.mas_equalTo(-((SCREEN_WIDTH-30)-240-43)/2);
        make.size.mas_equalTo(quanOneImageView);
    }];
    
    photoOneImageView = [UIImageView new];
    photoOneImageView.layer.cornerRadius = 50;
    photoOneImageView.layer.masksToBounds = YES;
    photoOneImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoOneImageView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [quanOneImageView addSubview:photoOneImageView];
    [photoOneImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(quanOneImageView);
        make.centerY.mas_equalTo(quanOneImageView);
        make.size.mas_equalTo(CGSizeMake(100, 100));
    }];
    
    photoTwoImageView = [UIImageView new];
    photoTwoImageView.layer.cornerRadius = 50;
    photoTwoImageView.layer.masksToBounds = YES;
    photoTwoImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoTwoImageView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [quanTwoImageView addSubview:photoTwoImageView];
    [photoTwoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(quanTwoImageView);
        make.centerY.mas_equalTo(quanTwoImageView);
        make.size.mas_equalTo(photoOneImageView);
    }];
    
    nameOneLabel = [[UILabel alloc] init];
    nameOneLabel.font = DHSystemFontOfSize_18;
    nameOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:nameOneLabel];
    [nameOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.top.mas_equalTo(quanOneImageView.mas_bottom).mas_offset(9);
        make.centerX.mas_equalTo(photoOneImageView);
        make.height.mas_equalTo(25);
    }];
    
    nameTwoLabel = [[UILabel alloc] init];
    nameTwoLabel.font = DHSystemFontOfSize_18;
    nameTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:nameTwoLabel];
    [nameTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(mainView);
        make.top.mas_equalTo(nameOneLabel);
        make.centerX.mas_equalTo(quanTwoImageView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(nameOneLabel);
    }];
    
    carOneLabel = [[UILabel alloc] init];
    carOneLabel.font = DHSystemFontOfSize_13;
    carOneLabel.textColor = FWTextColor_B8B8B8;
    carOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:carOneLabel];
    [carOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView);
        make.top.mas_equalTo(nameOneLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(nameOneLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    carTwoLabel = [[UILabel alloc] init];
    carTwoLabel.font = DHSystemFontOfSize_13;
    carTwoLabel.textColor = FWTextColor_B8B8B8;
    carTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:carTwoLabel];
    [carTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(mainView);
        make.top.mas_equalTo(nameOneLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(18);
        make.centerX.mas_equalTo(nameTwoLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    yibiaopanImageView = [[UIImageView alloc] init];
    yibiaopanImageView.image = [UIImage imageNamed:@"live_yibiaopan"];
    [mainView addSubview:yibiaopanImageView];
    [yibiaopanImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(carOneLabel.mas_bottom).mas_offset(5);
        make.centerX.mas_equalTo(mainView);
        make.size.mas_equalTo(CGSizeMake(25, 20));
    }];
    
    ETImageView = [[UIImageView alloc] init];
    ETImageView.image = [UIImage imageNamed:@"live_pk"];
    [mainView addSubview:ETImageView];
    [ETImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake((SCREEN_WIDTH-2*63), 30));
        make.centerX.mas_equalTo(mainView).mas_offset(20);
        make.left.mas_equalTo(83);
        make.top.mas_equalTo(yibiaopanImageView.mas_bottom).mas_offset(10);
    }];
    
    RTImageView = [[UIImageView alloc] init];
    RTImageView.image = [UIImage imageNamed:@"live_pk"];
    [mainView addSubview:RTImageView];
    [RTImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(ETImageView);
        make.centerX.mas_equalTo(mainView);
        make.left.mas_equalTo(63);
        make.top.mas_equalTo(ETImageView.mas_bottom).mas_offset(11);
    }];
    
    WeisuImageView = [[UIImageView alloc] init];
    WeisuImageView.image = [UIImage imageNamed:@"live_pk"];
    [mainView addSubview:WeisuImageView];
    [WeisuImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(ETImageView);
        make.centerX.mas_equalTo(mainView).mas_offset(-20);
        make.left.mas_equalTo(43);
        make.top.mas_equalTo(RTImageView.mas_bottom).mas_offset(11);
    }];
    
    NSArray * titleArray = @[@"ET",@"ET",@"RT",@"RT",@"尾速",@"尾速"];
    NSArray * viewArray = @[ETImageView,ETImageView,RTImageView,RTImageView,WeisuImageView,WeisuImageView];
    for (int i =0; i< 6; i++) {
        
        UIImageView * imageView = (UIImageView *)viewArray[i];
        
        UILabel * label = [[UILabel alloc] init];
        label.font = DHSystemFontOfSize_9;
        label.text = titleArray[i];
        label.textColor = FWViewBackgroundColor_FFFFFF;
        if (i%2==0) {
            label.textAlignment = NSTextAlignmentRight;
        }else{
            label.textAlignment = NSTextAlignmentLeft;
        }
        [mainView addSubview:label];
        [label mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (i%2==0) {
                make.right.mas_equalTo(imageView.mas_left);
            }else{
                make.left.mas_equalTo(imageView.mas_right);
            }
            make.centerY.mas_equalTo(imageView);
            make.height.mas_equalTo(13);
            make.width.mas_equalTo(30);
        }];
    }
    
    ETTimeOneLabel = [[UILabel alloc] init];
    ETTimeOneLabel.font = DHSystemFontOfSize_18;
    ETTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    ETTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:ETTimeOneLabel];
    [ETTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(ETImageView);
        make.top.mas_equalTo(ETImageView);
        make.height.mas_equalTo(ETImageView);
        make.width.mas_equalTo((SCREEN_WIDTH-2*63)/2);
    }];
    
    ETTimeTwoLabel = [[UILabel alloc] init];
    ETTimeTwoLabel.font = DHSystemFontOfSize_18;
    ETTimeTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    ETTimeTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:ETTimeTwoLabel];
    [ETTimeTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(ETImageView);
        make.top.mas_equalTo(ETTimeOneLabel);
        make.height.mas_equalTo(ETTimeOneLabel);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    RTTimeOneLabel = [[UILabel alloc] init];
    RTTimeOneLabel.font = DHSystemFontOfSize_18;
    RTTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    RTTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:RTTimeOneLabel];
    [RTTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(RTImageView);
        make.top.mas_equalTo(RTImageView);
        make.height.mas_equalTo(RTImageView);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    RTTimeTwoLabel = [[UILabel alloc] init];
    RTTimeTwoLabel.font = DHSystemFontOfSize_18;
    RTTimeTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    RTTimeTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:RTTimeTwoLabel];
    [RTTimeTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(RTImageView);
        make.top.mas_equalTo(RTTimeOneLabel);
        make.height.mas_equalTo(ETTimeOneLabel);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    WeisuTimeOneLabel = [[UILabel alloc] init];
    WeisuTimeOneLabel.font = DHSystemFontOfSize_18;
    WeisuTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    WeisuTimeOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:WeisuTimeOneLabel];
    [WeisuTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(WeisuImageView);
        make.top.mas_equalTo(WeisuImageView);
        make.height.mas_equalTo(WeisuImageView);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    WeisuTimeTwoLabel = [[UILabel alloc] init];
    WeisuTimeTwoLabel.font = DHSystemFontOfSize_18;
    WeisuTimeTwoLabel.textColor = FWViewBackgroundColor_FFFFFF;
    WeisuTimeTwoLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:WeisuTimeTwoLabel];
    [WeisuTimeTwoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(WeisuImageView);
        make.top.mas_equalTo(WeisuImageView);
        make.height.mas_equalTo(WeisuImageView);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    nameOneLabel.text = self.listModel.car_1_info.player_name;
    nameTwoLabel.text = self.listModel.car_2_info.player_name;
    
    carOneLabel.text = self.listModel.car_1_info.car_type;
    carTwoLabel.text = self.listModel.car_2_info.car_type;
    
    ETTimeOneLabel.text = self.listModel.car_1_info.elapsed_time;
    RTTimeOneLabel.text = self.listModel.car_1_info.reaction_time;
    WeisuTimeOneLabel.text = self.listModel.car_1_info.vehicle_speed;
    
    ETTimeTwoLabel.text = self.listModel.car_2_info.elapsed_time;
    RTTimeTwoLabel.text = self.listModel.car_2_info.reaction_time;
    WeisuTimeTwoLabel.text = self.listModel.car_2_info.vehicle_speed;
    
    [photoOneImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.car_1_info.header] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (error) {
            photoOneImageView.image = [UIImage imageNamed:@"placeholder_photo"];
        }
    }];
    [photoTwoImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.car_2_info.header] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (error) {
            photoOneImageView.image = [UIImage imageNamed:@"placeholder_photo"];
        }
    }];
}

#pragma mark - > 初始化单人视图
- (void)setupSingleBettleSubviews{
    
    quanOneImageView = [UIImageView new];
    quanOneImageView.image = [UIImage imageNamed:@"live_touxiang"];
    [mainView addSubview:quanOneImageView];
    [quanOneImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(mainView).mas_offset(171);
        make.centerX.mas_equalTo(mainView);
        make.size.mas_equalTo(CGSizeMake(120, 120));
    }];
    
    photoOneImageView = [UIImageView new];
    photoOneImageView.layer.cornerRadius = 50;
    photoOneImageView.layer.masksToBounds = YES;
    photoOneImageView.contentMode = UIViewContentModeScaleAspectFill;
    photoOneImageView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [quanOneImageView addSubview:photoOneImageView];
    [photoOneImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(quanOneImageView);
        make.centerY.mas_equalTo(quanOneImageView);
        make.size.mas_equalTo(CGSizeMake(100, 100));
    }];
    
    nameOneLabel = [[UILabel alloc] init];
    nameOneLabel.font = DHSystemFontOfSize_18;
    nameOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    nameOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:nameOneLabel];
    [nameOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(mainView);
        make.top.mas_equalTo(quanOneImageView.mas_bottom).mas_offset(9);
        make.height.mas_equalTo(25);
    }];
    
    carOneLabel = [[UILabel alloc] init];
    carOneLabel.font = DHSystemFontOfSize_13;
    carOneLabel.textColor = FWTextColor_B8B8B8;
    carOneLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:carOneLabel];
    [carOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(mainView);
        make.top.mas_equalTo(nameOneLabel.mas_bottom).mas_offset(5);
        make.height.mas_equalTo(18);
    }];
    
    yibiaopanImageView = [[UIImageView alloc] init];
    yibiaopanImageView.image = [UIImage imageNamed:@"live_yibiaopan"];
    [mainView addSubview:yibiaopanImageView];
    [yibiaopanImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(carOneLabel.mas_bottom).mas_offset(16);
        make.centerX.mas_equalTo(mainView);
        make.size.mas_equalTo(CGSizeMake(25, 20));
    }];
    
    NSArray * titleArray = @[@"ET时间",@"RT时间",@"尾速"];
    for (int i = 0; i<3; i++) {
        UILabel * label = [[UILabel alloc] init];
        label.font = DHSystemFontOfSize_11;
        label.text = titleArray[i];
        label.textColor = FWViewBackgroundColor_FFFFFF;
        label.textAlignment = NSTextAlignmentLeft;
        [mainView addSubview:label];
        [label mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(mainView).mas_offset(15+i*(SCREEN_WIDTH-45)/3);
            make.top.mas_equalTo(yibiaopanImageView.mas_bottom).mas_offset(28);
            make.height.mas_equalTo(13);
            make.width.mas_equalTo((SCREEN_WIDTH-45)/3);
        }];
    }
    
    ETTimeOneLabel = [[UILabel alloc] init];
    ETTimeOneLabel.font = DHSystemFontOfSize_20;
    ETTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    ETTimeOneLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:ETTimeOneLabel];
    [ETTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView).mas_offset(15);
        make.top.mas_equalTo(yibiaopanImageView.mas_bottom).mas_offset(50);
        make.height.mas_equalTo(22);
        make.width.mas_equalTo((SCREEN_WIDTH-45)/3);
    }];
    
    RTTimeOneLabel = [[UILabel alloc] init];
    RTTimeOneLabel.font = DHSystemFontOfSize_20;
    RTTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    RTTimeOneLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:RTTimeOneLabel];
    [RTTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView).mas_offset(15+(SCREEN_WIDTH-45)/3);
        make.top.mas_equalTo(ETTimeOneLabel);
        make.height.mas_equalTo(ETTimeOneLabel);
        make.width.mas_equalTo(ETTimeOneLabel);
    }];
    
    WeisuTimeOneLabel = [[UILabel alloc] init];
    WeisuTimeOneLabel.font = DHSystemFontOfSize_20;
    WeisuTimeOneLabel.textColor = FWViewBackgroundColor_FFFFFF;
    WeisuTimeOneLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:WeisuTimeOneLabel];
    [WeisuTimeOneLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView).mas_offset(15+2*(SCREEN_WIDTH-45)/3);
        make.top.mas_equalTo(RTTimeOneLabel);
        make.height.mas_equalTo(RTTimeOneLabel);
        make.width.mas_equalTo(RTTimeOneLabel);
    }];
    
    NSArray * imageArray = @[@"live_ET",@"live_RT",@"live_weisu"];
    
    for (int i = 0; i<imageArray.count; i++) {
        UIImageView * imageView = [UIImageView new];
        imageView.image = [UIImage imageNamed:imageArray[i]];
        [mainView addSubview:imageView];
        [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(i*(SCREEN_WIDTH-45)/3);
            make.bottom.mas_equalTo(mainView).mas_offset(-61);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo((SCREEN_WIDTH)/3);
        }];
    }
    
    FWCarInfoModel * carInfo ;
    
    if (nil == self.listModel.car_1_info.player_name ||
        self.listModel.car_1_info.player_name.length <= 0) {
        carInfo = self.listModel.car_2_info;
    }else{
        carInfo = self.listModel.car_1_info;
    }
    
    nameOneLabel.text = carInfo.player_name;
    carOneLabel.text = carInfo.car_no;
    ETTimeOneLabel.text = carInfo.elapsed_time;
    RTTimeOneLabel.text = carInfo.reaction_time;
    WeisuTimeOneLabel.text = carInfo.vehicle_speed;
    
    [photoOneImageView sd_setImageWithURL:[NSURL URLWithString:carInfo.header] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (error) {
            photoOneImageView.image = [UIImage imageNamed:@"placeholder_photo"];
        }
    }];
}

#pragma mark - > 比赛海报
- (void)setupMatchPostSubviews{
 
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).mas_offset(15);
        make.right.mas_equalTo(self.view).mas_offset(-15);
        make.top.mas_equalTo(self.view).mas_offset((SCREEN_HEIGHT-(SCREEN_WIDTH-30)*283/345)/2-55);
        make.width.mas_equalTo(SCREEN_WIDTH-30);
        make.height.mas_equalTo((SCREEN_WIDTH-30)*283/345);
    }];
    
    mainView.image = self.postImage;
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(containerView);
        make.width.mas_equalTo(containerView);
        make.height.mas_equalTo((SCREEN_WIDTH-30)*283/193);
    }];
    
    tipLabel.textColor = FWTextColor_000000;
    announceView.backgroundColor = FWViewBackgroundColor_FFFFFF;
}

- (void)setPostImage:(UIImage *)postImage{
    
    _postImage = postImage;
    [self setupMatchPostSubviews];
}

@end
