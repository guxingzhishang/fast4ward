//
//  FWMatchPicsViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchPicsViewController.h"
#import "FWMatchPicsCollectionCell.h"
#import "FWMatchPicsModel.h"
#import "FWMatchPicsSegementView.h"
#import "UIBarButtonItem+Item.h"

static NSString * rankCellID = @"FWMatchPicsCollectionCellID";

@interface FWMatchPicsViewController ()<FWMatchPicsSegementViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIPickerViewDataSource,UIPickerViewDelegate>
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) NSMutableArray * originalDataSource;

@property(nonatomic, strong)  FWMatchPicsSegementView * segement;
@property(nonatomic, strong)  FWMatchPicsModel * tujiModel;
@property (nonatomic, assign) NSInteger  lastIndex;// 最后一次点击的segement
@property (nonatomic, assign) NSInteger  lastChooseRow;// 最后一次筛选选择的第几行
@property (nonatomic, strong) NSString *  currenTujiID;


@property (nonatomic, strong) IQTextView * showTextView;
@property (nonatomic, strong) UIView * chooseView;
@property (nonatomic, strong) UIButton * chooseButton;
@property (nonatomic, strong) UIPickerView * pickerView;
@property (nonatomic, strong) NSMutableArray * firstArray;

@property (nonatomic, assign) NSInteger pageNum;


@end

@implementation FWMatchPicsViewController
@synthesize segement;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

- (NSMutableArray *)originalDataSource{
    if (!_originalDataSource) {
        _originalDataSource = [[NSMutableArray alloc] init];
    }
    
    return _originalDataSource;
}

- (void)requestData:(BOOL)isLoadMore{
    
    if (isLoadMore) {
        self.pageNum += 1;
    }else{
        [self.picsCollectionView scrollRectToVisible:CGRectMake(0, 0, 0.1, 0.1) animated:NO];

        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
        
        if (self.originalDataSource.count > 0) {
            [self.originalDataSource removeAllObjects];
        }
        self.pageNum = 1;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"tuji_id":self.tuiji_id,
                              @"nav_id":self.nav_id,
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"1000",
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_tuji_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.tujiModel = [FWMatchPicsModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            /* 筛选的名称 */
            if (self.firstArray.count <= 0) {
//                [self.firstArray removeAllObjects];
                
                NSMutableArray * tempArr = @[].mutableCopy;
                for (FWMatchTujiIDModel * model in self.tujiModel.list_tuji_id) {
                    if (![tempArr containsObject:model.tuji_name]) {
                        [tempArr addObject:model.tuji_name];
                    }
                }
                self.firstArray = [tempArr mutableCopy];
            }
            
            FWMatchTujiIDModel * model = self.tujiModel.list_tuji_id[self.lastChooseRow];

            for (FWMatchTujiIDModel * idModel in self.tujiModel.list_tuji_id) {
                if ([idModel.tuji_id isEqualToString:self.tuiji_id]) {
                    self.showTextView.text = idModel.tuji_name;
                }
            }
            
            if (self.showTextView.text.length <= 0) {
                /* 默认tuji_id是0时，tuji_name取第一项的name */
                self.showTextView.text = model.tuji_name;
            }
            
            /* 只有在图集ID 不相同时，才重置segement */
            if (![self.currenTujiID isEqualToString:model.tuji_id]) {
                /* segement名称 */
                NSMutableArray * tempSegementArr = @[].mutableCopy;
                
                for (FWMatchTujiNavModel * model in self.tujiModel.list_nav) {
                    if (![tempSegementArr containsObject:model.nav_title]) {
                        [tempSegementArr addObject:model.nav_title];
                    }
                }
                [segement deliverTitles:tempSegementArr];
                segement.lastIndex = self.lastIndex;
            }
            
            self.currenTujiID = model.tuji_id;
            
            /* 图集分类 */
            if (isLoadMore) {
                [self.picsCollectionView.mj_footer endRefreshing];
            }else{
                [self.picsCollectionView.mj_header endRefreshing];
            }
            
            if (self.pageNum != 1 && self.tujiModel.list_small.count == 0) {
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.dataSource addObjectsFromArray:self.tujiModel.list_small];
                [self.originalDataSource addObjectsFromArray:self.tujiModel.list_original];
                
                [self.picsCollectionView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self trackPageBegin:@"赛事图集"];
    self.view.backgroundColor = FWTextColor_F8F8F8;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"赛事图集"];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"赛事图集";
    self.firstArray = @[].mutableCopy;
    self.lastIndex = 0;
//    self.lastChooseRow = 0;
    
    self.currenTujiID = @"";
    self.nav_id = @"0";
    
    [self setupSubViews];
    [self requestData:NO];
}

- (void)setupSubViews{
    
    UIView * topView = [[UIView alloc] init];
    topView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:topView];
    topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 30);
    
    self.chooseView = [[UIView alloc] init];
    self.chooseView.frame = CGRectMake(SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, 30);
    [topView addSubview:self.chooseView];
    
    UIImageView * shaixuanImageView = [[UIImageView alloc] init];
    shaixuanImageView.image = [UIImage imageNamed:@"sport_choose"];
    [self.chooseView addSubview:shaixuanImageView];
    [shaixuanImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.chooseView).mas_offset(-0);
        make.right.mas_equalTo(self.chooseView).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    self.showTextView = [[IQTextView alloc] init];
    self.showTextView.editable = NO;
    self.showTextView.delegate = self;
    self.showTextView.scrollEnabled = NO;
    self.showTextView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.showTextView.contentInset = UIEdgeInsetsMake(-4, 0, 0, 0);
    self.showTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.showTextView.textColor = FWTextColor_66739A;
    self.showTextView.textAlignment = NSTextAlignmentRight;
    [topView addSubview:self.showTextView];
    [self.showTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.chooseView);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(shaixuanImageView.mas_left).mas_offset(-5);
        make.width.mas_greaterThanOrEqualTo(200);
    }];
    
    self.chooseButton = [[UIButton alloc] init];
    self.chooseButton.frame = CGRectMake(SCREEN_WIDTH-250, 0, 250, 30);
    [self.chooseButton addTarget:self action:@selector(chooseButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:self.chooseButton];
    [topView bringSubviewToFront:self.chooseButton];
    
    self.pickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
    self.pickerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.pickerView.backgroundColor=[UIColor clearColor];
    
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    
    UIBarButtonItem *doneButton = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar.items = @[cancelButton,flexItem, fixItem, doneButton];
    
    self.showTextView.inputView = self.pickerView;
    self.showTextView.inputAccessoryView = bar;
    
    segement = [[FWMatchPicsSegementView alloc]init];
    segement.itemDelegate = self;
    segement.frame = CGRectMake(0,CGRectGetMaxY(topView.frame), SCREEN_WIDTH, 40);
    [self.view addSubview:self.segement];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(5,14,5,14);
    flowLayout.minimumLineSpacing = 5;// cell的横向间距
    flowLayout.minimumInteritemSpacing = 5; // cell的纵向间距
    
    self.picsCollectionView = [[FWCollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(segement.frame), SCREEN_WIDTH,SCREEN_HEIGHT-CGRectGetMaxY(segement.frame)-64-FWCustomeSafeTop) collectionViewLayout:flowLayout];
    self.picsCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.picsCollectionView.dataSource = self;
    self.picsCollectionView.delegate = self;
    self.picsCollectionView.showsHorizontalScrollIndicator = NO;
    [self.picsCollectionView registerClass:[FWMatchPicsCollectionCell class] forCellWithReuseIdentifier:rankCellID];
    self.picsCollectionView.isNeedEmptyPlaceHolder = YES;
    NSString * attetionTitle = @"还没有关注的内容哦~";
    NSDictionary *attetionAttributes = @{
                                         NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                         NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attentionString = [[NSMutableAttributedString alloc] initWithString:attetionTitle attributes:attetionAttributes];
    self.picsCollectionView.emptyDescriptionString = attentionString;
    self.picsCollectionView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.picsCollectionView];
    
    DHWeakSelf;
    self.picsCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestData:NO];
    }];
    
    self.picsCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestData:YES];
    }];
}

#pragma mark - > 点击推荐item
- (void)segementItemClickTap:(NSInteger)index{
    
    if (self.lastIndex == index) {
        return;
    }
    
//    [self.infoTableView layoutIfNeeded]; //这句是关键

    self.lastIndex = index;

    self.nav_id = self.tujiModel.list_nav[index].nav_id;
    [self requestData:NO];
}

#pragma mark - >  Delegate & DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWMatchPicsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:rankCellID forIndexPath:indexPath];
    cell.backgroundColor = FWViewBackgroundColor_FFFFFF;
    if (indexPath.row < self.dataSource.count) {
        [cell.picImageView sd_setImageWithURL:[NSURL URLWithString:self.dataSource[indexPath.row]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.showTextView resignFirstResponder];
    
    if (indexPath.row < self.dataSource.count) {
        
        FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
        browser.isFullWidthForLandScape = YES;
        browser.isNeedLandscape = YES;
        browser.currentImageIndex = (int)indexPath.item;
        browser.imageArray = [self.dataSource mutableCopy];
        browser.smallArray = [self.dataSource copy];
        browser.originalImageArray = self.originalDataSource;
        browser.vc = self;
        browser.fromType = 3;
        [browser show];
    }
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake((SCREEN_WIDTH-38)/3, (SCREEN_WIDTH-38)/3);
}


#pragma mark - > 筛选（弹出选择器）
- (void)chooseButtonClick{
    
    [self.showTextView becomeFirstResponder];
}

#pragma mark - > pickerview数据源及代理方法
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

#pragma mark 数据源  numberOfRowsInComponent
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.firstArray.count;
}

#pragma delegate 显示信息方法
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.firstArray objectAtIndex:row];
}

#pragma mark 选中行信息
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
}

#pragma mark 显示行高
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.00;
}

#pragma mark - pickerView 每列宽度
- (CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    return SCREEN_WIDTH;
}

#pragma mark - > 选中
- (void)doneButtonClick:(UIBarButtonItem *)button{
    [self.view endEditing:YES];
    
    NSInteger onerow=[self.pickerView selectedRowInComponent:0];
    
    if (self.tujiModel.list_tuji_id.count > onerow) {
        
        FWMatchTujiIDModel * model = self.tujiModel.list_tuji_id[onerow];
        self.showTextView.text = model.tuji_name;
        
        self.tuiji_id = model.tuji_id;
        self.lastChooseRow = onerow;
        
        [self requestData:NO];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = (UILabel *)view;
    if (lbl == nil) {
        lbl = [[UILabel alloc]init];
        //在这里设置字体相关属性
        lbl.font = [UIFont systemFontOfSize:18];
        lbl.textColor = FWTextColor_000000;
        [lbl setTextAlignment:1];
        [lbl setBackgroundColor:[UIColor clearColor]];
    }
    //重新加载lbl的文字内容
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return lbl;
}

#pragma mark - > 取消
- (void)cancelButtonClick:(UIBarButtonItem *)button{
    [self.view endEditing:YES];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    //    if (action == @selector(paste:))//禁止粘贴
    //        return NO;
    //    if (action == @selector(select:))// 禁止选择
    //        return NO;
    //    if (action == @selector(selectAll:))// 禁止全选
    //        return NO;
    //    return [super canPerformAction:action withSender:sender];
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController)
    {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}
@end
