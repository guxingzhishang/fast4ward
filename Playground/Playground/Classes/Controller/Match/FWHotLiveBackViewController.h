//
//  FWHotLiveBackViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 直播回放
 */

#import "FWBaseViewController.h"
#import "FWLiveModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWHotLiveBackViewController : FWBaseViewController
@property (nonatomic, strong) FWLiveListModel * liveListModel;
@end

NS_ASSUME_NONNULL_END
