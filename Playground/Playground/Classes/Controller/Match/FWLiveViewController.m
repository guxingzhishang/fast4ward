//
//  FWLiveViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/5.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#define kRandomColor [UIColor colorWithRed:arc4random_uniform(256.0)/255.0 green:arc4random_uniform(256.0)/255.0 blue:arc4random_uniform(256.0)/255.0 alpha:1.0]


#import "FWLiveViewController.h"

#import "ZFPlayer.h"
#import "ZFAVPlayerManager.h"
#import "KSMediaPlayerManager.h"
#import "ZFPlayerControlView.h"
#import "ZFIJKPlayerManager.h"
#import "UIView+ZFFrame.h"

#import "FWLiveModel.h"
#import "FWLiveTipView.h"

#import "FWRankViewController.h"
#import "FWChatViewController.h"
#import "FWRealPerformanceViewController.h"
#import "FWShareScoreImageViewController.h"

#import "RCCRMessageModel.h"
#import "RCCRUtilities.h"
#import "RCCRMessageBaseCell.h"
#import "RCCRRongCloudIMManager.h"
#import "RCCRTextMessageCell.h"
#import "RCChatroomGift.h"
#import "RCChatroomBarrage.h"
#import "RCChatroomUserQuit.h"
#import "RCChatroomWelcome.h"
#import "FWGuessingCompetitionViewController.h"

static NSInteger adViewTag = 123456789;

@interface FWLiveViewController ()<FWLiveTipViewDelegate>

@property (nonatomic, strong) UIButton * playBtn;
@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) ZFPlayerControlView *controlView;

@property (nonatomic, strong) ZFAVPlayerManager *playerManager;
@property (nonatomic, strong) FWLiveTipView * tipView;

@property (nonatomic, strong) NSArray *titleData;
@property (nonatomic, assign) CGFloat menuHeight;
@property (nonatomic, assign) CGFloat wmMenuY;

@property (nonatomic, strong) NSString * targetId;
@property (nonatomic, assign) RCConversationType conversationType;

@property (nonatomic, strong) NSTimer * timer;

@property (nonatomic, strong) FWLiveModel * liveModel;
@property (nonatomic, strong) FWLiveModel * statusLiveModel;

@property (nonatomic, strong) NSString * lastStatus;// 上一次状态
@property (nonatomic, assign) BOOL isRefresh;//是否是重新请求了
/* 非直播状态，请求了几次， 用于wifi切4G使用 */
@property (nonatomic, assign) NSInteger notLiveCount;
/* 最近一次的网络状态  wifi 还是 4G */
@property (nonatomic, strong) NSString * lastNetWork;

@property (nonatomic, strong) UIView * blackView;//黑色背景
@property (nonatomic, strong) UILabel * countLabel;// 观看人数
@property (nonatomic, strong) UIImageView * countImage;// 观看人数前面的图标
@property (nonatomic, strong) UIView * replayView; // 回放视图（有几个视频）
@property (nonatomic, strong) NSDictionary * liveStatusDict;// 状态
@property (nonatomic, assign) NSInteger lastClick;// 上一次点击的视频（回放用）

@property (nonatomic, strong) FWMatchModel * matchModel;

@end

@implementation FWLiveViewController
@synthesize playerManager;
@synthesize countLabel;
@synthesize countImage;

#pragma mark - > 标题数组
- (NSArray *)titleData {
    if (!_titleData) {
        _titleData = @[@"聊天", @"实时成绩", @"排名"];
    }
    return _titleData;
}

#pragma mark - > 请求观看人数
- (void)requestChatRoomUserCount{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"room_id":self.targetId,
                              };
    
    [request startWithParameters:params WithAction:Get_chatroom_user_count WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([[[back objectForKey:@"data"] objectForKey:@"msg"] length] > 0) {
                countImage.hidden = YES;
                countLabel.frame = CGRectMake(20, CGRectGetMaxY(self.blackView.frame), SCREEN_WIDTH-40, 49);

                self.countLabel.text = [[back objectForKey:@"data"] objectForKey:@"msg"];
            }else{
                countImage.hidden = NO;
                countImage.frame = CGRectMake(20, CGRectGetMaxY(self.blackView.frame)+14, 18, 20);
                countLabel.frame = CGRectMake(CGRectGetMaxX(countImage.frame) +5, CGRectGetMaxY(self.blackView.frame), 200, 49);

                self.countLabel.text = [NSString stringWithFormat:@"%@人观看",[[back objectForKey:@"data"] objectForKey:@"user_total"]];
            }
        }
    }];
}

#pragma mark - > 请求直播状态 (正常请求接口 )
- (void)requestData{
   
    if (self.isLive) {
        
        FWFollowRequest * request = [[FWFollowRequest alloc] init];

        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  };

        [request startWithParameters:params WithAction:Get_live_status_v2 WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

            if ([code isEqualToString:NetRespondCodeSuccess]) {

                self.liveStatusDict = [back objectForKey:@"data"];

                [self dealwithStatus];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }else{
        
        [self playLiveWithURL:self.liveListModel.list_replay[self.lastClick].replay_url];
    }
}

// 判断各种状态
- (void)dealwithStatus{
    
    if (![[GFStaticData getObjectForKey:Current_Network] isEqualToString:@"WIFI"] &&
        [[GFStaticData getObjectForKey:Network_Available] boolValue]){
        // 4G 返回
        return;
    }
    
    if ([[self.liveStatusDict objectForKey:@"live_status"] isEqualToString:@"1"]) {
        
        // 预赛未直播
        [self playLiveWithURL:@" "];

        self.controlView.activity.hidden = YES;
        self.controlView.failBtn.hidden = NO;
        self.controlView.failBtn.enabled = NO;
        [self.controlView.failBtn setTitle:[self.liveStatusDict objectForKey:@"live_status_desc"] forState:UIControlStateNormal];
    }else if ([[self.liveStatusDict objectForKey:@"live_status"] isEqualToString:@"2"]) {
        
        // 预赛直播中
        if (![self.lastStatus isEqualToString:[self.liveStatusDict objectForKey:@"live_status"]] ||self.isRefresh) {
            // 直播状态改变，或 需要刷新，才会重新请求
            [self playLiveWithURL:self.finalModel.list_url.hls_url];
        }
        self.controlView.activity.hidden = YES;

        self.controlView.failBtn.hidden = YES;
        self.controlView.failBtn.enabled = YES;
        [self.controlView.failBtn setTitle:@"刷新" forState:UIControlStateNormal];
        
        [self.timer invalidate];
    }else if ([[self.liveStatusDict objectForKey:@"live_status"] isEqualToString:@"3"]) {
        /* 直播结束 */
        [self playLiveWithURL:@" "];
        
        self.controlView.activity.hidden = YES;
        self.controlView.failBtn.hidden = NO;
        self.controlView.failBtn.enabled = NO;
        [self.controlView.failBtn setTitle:[self.liveStatusDict objectForKey:@"live_status_desc"] forState:UIControlStateNormal];
        [self.timer invalidate];
    }
    
    self.isRefresh = NO;
    self.lastStatus = [self.liveStatusDict objectForKey:@"live_status"];
}

- (void)checkWifi{
    
    if ([[GFStaticData getObjectForKey:Current_Network] isEqualToString:@"WIFI"]) {
        
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
        
            /* 回放， 4G切WIFI 会从头开始,所以只有直播时才直接请求最新数据 */
            if ([[self.liveStatusDict objectForKey:@"live_status"] isEqualToString:@"2"]) {
                
                [self requestData];
            }else{
                /* 回播，只在wifi转4G后请求一次 */
                if (self.notLiveCount == 1) {
                    self.notLiveCount +=1;
                    [self requestData];
                }
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }else{
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            if (![[GFStaticData getObjectForKey:Allow_4G_Play] boolValue]) {
                [self requestData];

                self.tipView.hidden = NO;

                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.player.currentPlayerManager pause];
                    [self.playerManager pause];
                });
            }else{
                self.tipView.hidden = YES;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }
}

#pragma mark - > 4G下 点击继续观看
- (void)continueLiveMatch{
 
    self.tipView.hidden = YES;
    
    self.lastNetWork = [GFStaticData getObjectForKey:Last_Network];
    
    if ([self.lastNetWork isEqualToString:@"WIFI"] && ![self.lastNetWork isEqualToString:[GFStaticData getObjectForKey:Current_Network]]) {
        // 由wifi切换到4G ，继续播放
        [self.player.currentPlayerManager play];
    }else{
        // 进来直接是4G
        self.notLiveCount +=1;
        [self requestData];
    }
    
    [GFStaticData saveObject:@"WIFI" forKey:Current_Network];
    [GFStaticData saveObject:@"1" forKey:Allow_4G_Play];
}

#pragma mark - > 播放视频
- (void)playLiveWithURL:(NSString *)urlstring{
    
    NSString *URLString = [urlstring stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    playerManager.assetURL = [NSURL URLWithString:URLString];
    
    if ([urlstring stringByReplacingOccurrencesOfString:@" " withString:@""].length<= 0) {
        
        [playerManager pause];
        return;
    }
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        self.menuViewStyle = WMMenuViewStyleLine;
        self.progressHeight = 2.0f;
        self.titleFontName = @"PingFangSC-Semibold";
        self.titleSizeSelected = 14.0f;
        self.titleSizeNormal = 14.0f;
        self.scrollEnable = NO;
        self.titleColorSelected = FWTextColor_222222;
        self.titleColorNormal = FWTextColor_BCBCBC;
        self.menuViewLayoutMode = WMMenuViewLayoutModeScatter;
        self.menuHeight = 44.0f;
        self.wmMenuY = SCREEN_WIDTH *9/16+FWCustomeSafeTop+FWLiveTopPadding+49;
    }
    return self;
}

#pragma mark - > 生命周期
- (void)dealloc{
    NSLog(@"-----------FWLiveViewController销毁了-----------");
    
    self.timer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self checkWifi];
    [self requestChatRoomUserCount];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];

    [self.player.currentPlayerManager pause];
    [self.timer invalidate];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;

    self.notLiveCount = 1;
    
    self.liveStatusDict = @{};
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.containerView];
    
    /* 防止刘海屏，刘海位置挡住视频，所以视频下移后，刘海位置会有空白，设置成黑色 */
    self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,  SCREEN_WIDTH*9/16+FWCustomeSafeTop+FWLiveTopPadding)];
    self.blackView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.blackView];
    [self.view sendSubviewToBack:self.blackView];
    
    [self.containerView addSubview:self.playBtn];
    [self.view addSubview:self.bottomView];
    
    [self setupCustomSubViews];
    [self setupPlayer];
    [self setupPlayerSelectView];
    [self setupADView];

    self.targetId = self.roomId;
    self.conversationType = ConversationType_CHATROOM;
  
    [self registNotification];
    [self checkWifi];
    [self requestTeam];
    
    if (self.isLive) {
        /* 直播中 */
        self.timer = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:@selector(requestData) userInfo:nil repeats:YES];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.selectIndex = self.currentIndex;
    });
}


#pragma mark - > 请求金币浮层图片
- (void)requestTeam{
    
    FWMatchRequest * request = [[FWMatchRequest alloc] init];
    
    if (!self.sport_id) return;
        
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"sport_id":self.sport_id,
                              };
    
    [request startWithParameters:params WithAction:Get_sport_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.matchModel = [FWMatchModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if ([self.matchModel.guess_status isEqualToString:@"1"]) {
                self.titleData = @[@"聊天", @"实时成绩", @"排名",@"竞猜"];
                [self reloadData];
                [self.view addSubview:self.bottomView];
            }
            
            if (self.adView) {
                [self.adView.adImageView sd_setImageWithURL:[NSURL URLWithString:self.matchModel.gold_img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if (!image) {
                         [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];

                        if (self.adView) {
                            for (UIView * view in self.adView.subviews) {
                                [view removeFromSuperview];
                            }
                        }

                    }
                }];
            }
            
            if (self.isLive) {
               [self requestGoldStatus];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 查询金币领取状态
- (void)requestGoldStatus{

    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];

    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"sport_id":self.sport_id,
                             };

    [request startWithParameters:params WithAction:Get_gold_get_status WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {

            if (![[[back objectForKey:@"data"] objectForKey:@"status"] isEqualToString:@"1"]) {
                self.adView.hidden = NO;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

-(void)setupADView{

    self.adView = [[FWLiveADView alloc] init];
    self.adView.tag = adViewTag;
    self.adView.frame = CGRectMake(0, 0, SCREEN_WIDTH, ScreenHeight);
    [[UIApplication sharedApplication].keyWindow addSubview:self.adView];
    
    self.adView.hidden = YES;

    [self.adView.adImageView sd_setImageWithURL:[NSURL URLWithString:self.matchModel.gold_img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];

    UITapGestureRecognizer * adTap = [[UITapGestureRecognizer alloc] init];
    [self.adView.adImageView addGestureRecognizer:adTap];
    [self.adView.adImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adTap)]];

    [self.adView.closeView addTarget:self action:@selector(closeTap) forControlEvents:UIControlEventTouchUpInside];
}

- (void)adTap{

     [self checkLogin];

     if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];
         /* 广告 */
         FWNetworkRequest * request = [[FWNetworkRequest alloc] init];

         NSDictionary * params = @{
                                     @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                     @"sport_id":self.sport_id,
         };

         request.isNeedShowHud = YES;
         [request startWithParameters:params WithAction:Submit_get_guess_gold WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

             NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

             if ([code isEqualToString:NetRespondCodeSuccess]) {
                 NSString *handledStr = [[[back objectForKey:@"data"] objectForKey:@"notice_str"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];


                 UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:handledStr preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"立刻竞猜" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                     
                     if (self.adView) {
                         [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];
                     }
                     
                     if (self.titleData.count > 0) {
                        self.selectIndex = (int)(self.titleData.count -1);
                     }
                 }];
                 [alertController addAction:okAction];
                 [self presentViewController:alertController animated:YES completion:nil];
             }else{
                 if (self.adView && [[back objectForKey:@"errmsg"] isEqualToString:@"您已经领取过本场比赛的赠送金币"]) {
                     [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];
                     return ;
                 }

                 [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
             }
         }];
     }
}

#pragma mark - > 关闭金币浮窗
- (void)closeTap{
    [[[UIApplication sharedApplication].keyWindow viewWithTag:adViewTag] removeFromSuperview];
}

- (void)registNotification{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HidekeyboardHide:) name:HideKeyboard object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:LiveRequestNoti object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(monitorLiveNet) name:MonitorLiveNetworking object:nil];
}

#pragma mark - > 初始化播放器
- (void)setupPlayer{
    
    playerManager = [[ZFAVPlayerManager alloc] init];
    
    self.player = [[ZFPlayerController alloc] initWithPlayerManager:playerManager containerView:self.containerView];
    self.player.controlView = self.controlView;
    @weakify(self)
    self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
        @strongify(self)
        [self.textField resignFirstResponder];
        [self setNeedsStatusBarAppearanceUpdate];
    };
  
    if (self.isLive) {
        [self.controlView showTitle:self.finalModel.live_title coverURLString:self.finalModel.live_image fullScreenMode:ZFFullScreenModeLandscape];
    }else{
        [self.controlView showTitle:self.liveListModel.list_replay[0].title coverURLString:self.liveListModel.list_replay[0].img_url fullScreenMode:ZFFullScreenModeLandscape];
    }
}

#pragma mark - > 返回 、回看/直播切换 、4G提示
- (void)setupCustomSubViews{
    
    CGFloat y = FWLiveTopPadding;
    CGFloat h = SCREEN_WIDTH*9/16;
    
    CGRect tipFrame = CGRectMake(0, y, SCREEN_WIDTH, h);
    
    self.tipView = [[FWLiveTipView alloc] initWithFrame:tipFrame];
    self.tipView.delegate = self;
    [self.tipView.bgImageView sd_setImageWithURL:[NSURL URLWithString:self.finalModel.live_image]];
    [self.view addSubview:self.tipView];
    self.tipView.hidden = YES;
    
    self.backButton = [[UIButton alloc] init];
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.backButton];
    self.backButton.frame = CGRectMake(0, SafeTop, 60, 30);
    [self.view bringSubviewToFront:self.backButton];
    
    self.shareButton = [[UIButton alloc] init];
    [self.shareButton setImage:[UIImage imageNamed:@"white_more"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareMatchPost) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.shareButton];
    self.shareButton.frame = CGRectMake(SCREEN_WIDTH-60, SafeTop, 60, 30);
    [self.view bringSubviewToFront:self.shareButton];
}

#pragma mark - > 初始化直播/回放播放器
- (void)setupPlayerSelectView{
    
    if(self.isLive){
        /* 直播 */
        self.adImageView = [[UIImageView alloc] init];
        self.adImageView.userInteractionEnabled = YES;
        self.adImageView.clipsToBounds = YES;
        self.adImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:self.adImageView];

        if ([self.finalModel.live_ad_status_show isEqualToString:@"1"]) {
            /* 展示广告 */
            self.adImageView.hidden = NO;
            self.adImageView.frame = CGRectMake(0, CGRectGetMaxY(self.blackView.frame), SCREEN_WIDTH, 49);
            [self.adImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adImageClick)]];
            [self.adImageView sd_setImageWithURL:[NSURL URLWithString:self.finalModel.live_ad_img]];
        }else{
            self.adImageView.hidden = YES;
        }
        
        countImage = [[UIImageView alloc] init];
        countImage.image = [UIImage imageNamed:@"match_photo"];
        [self.view addSubview:countImage];
        countImage.frame = CGRectMake(20, CGRectGetMaxY(self.blackView.frame)+14, 18, 20);
        
        countLabel = [[UILabel alloc] init];
        countLabel.font = DHSystemFontOfSize_14;
        countLabel.textColor = FWTextColor_222222;
        countLabel.backgroundColor = FWClearColor;
        countLabel.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:countLabel];
        countLabel.frame = CGRectMake(CGRectGetMaxX(countImage.frame) +5, CGRectGetMaxY(self.blackView.frame), 200, 49);
        
        UIView * lineView = [[UIView alloc] init];
        lineView.frame = CGRectMake(0, CGRectGetMaxY(countLabel.frame), SCREEN_WIDTH, 0.5);
        lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
        [self.view addSubview:lineView];
    }else{
        /* 回放 */
        self.replayView = [UIView new];
        self.replayView.frame = CGRectMake(0, CGRectGetMaxY(self.blackView.frame), SCREEN_WIDTH, 50);
        [self.view addSubview:self.replayView];
        
        UIView * lineView = [[UIView alloc] init];
        lineView.frame = CGRectMake(0, 49.4, SCREEN_WIDTH, 0.5);
        lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
        [self.replayView addSubview:lineView];
        
        self.lastClick = 0;
        
        NSInteger frameX = 0;
        for (int i = 0; i < self.liveListModel.list_replay.count; i++) {
            FWF4WMatchReplayListModel * model = self.liveListModel.list_replay[i];
            
            NSString * titleString = [NSString stringWithFormat:@"%@.%@",@(i+1).stringValue,model.title];
            
            UIButton * playButton = [[UIButton alloc] init];
            playButton.tag = 33333+i;
            playButton.titleLabel.font = DHSystemFontOfSize_12;
            playButton.layer.borderWidth = 1;
            playButton.layer.cornerRadius = 2;
            playButton.layer.masksToBounds = YES;
            [playButton setTitle:titleString forState:UIControlStateNormal];
            [playButton addTarget:self action:@selector(playButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            
            NSInteger length = [titleString length] *12;
            playButton.frame = CGRectMake(20+frameX, 8, length, 33);
            if (i == 0) {
                playButton.layer.borderColor = FWViewBackgroundColor_FF6F00.CGColor;
                [playButton setTitleColor:FWViewBackgroundColor_FF6F00 forState:UIControlStateNormal];
            }else{
                playButton.layer.borderColor = FWViewBackgroundColor_E5E5E5.CGColor;
                [playButton setTitleColor:FWViewBackgroundColor_E5E5E5 forState:UIControlStateNormal];
            }
            [self.replayView addSubview:playButton];
            
            frameX += length+10;
        }
    }
}

#pragma mark - > 切换回放视频
- (void)playButtonClick:(UIButton *)sender{
   
    /* 先退出聊天室 */
    [self quitChatRoom];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RongYunChangeRoomNofi object:nil userInfo:nil];

    
    NSInteger index = sender.tag - 33333;
    
    if (self.lastClick == index) {
        /* 点击当前视频，不做处理 */
        return;
    }else{
        /* 不是同一个视频，标记下来，下次使用 */
        self.lastClick = index;
    }
    
    FWF4WMatchReplayListModel * model = self.liveListModel.list_replay[index];
    
    self.targetId = model.chatroom_id;
    NSLog(@"model.chatroo_id==%@",self.targetId);
    
    /* 切换title,和封面图 */
    [self.controlView showTitle:self.liveListModel.list_replay[index].title coverURLString:self.liveListModel.list_replay[index].img_url fullScreenMode:ZFFullScreenModeLandscape];

    [self playLiveWithURL:model.replay_url];
    
    /* 防止4G下切换预赛、决赛会自动播放 */
    [self checkWifi];
    
    for (UIButton * button in self.replayView.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            
            if (button.tag == sender.tag) {
                button.layer.borderColor = FWViewBackgroundColor_FF6F00.CGColor;
                [button setTitleColor:FWViewBackgroundColor_FF6F00 forState:UIControlStateNormal];
            }else{
                button.layer.borderColor = FWViewBackgroundColor_E5E5E5.CGColor;
                [button setTitleColor:FWViewBackgroundColor_E5E5E5 forState:UIControlStateNormal];
            }
        }
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self joinChatRoom];
    });
}

#pragma mark - > 加入聊天室
- (void)joinChatRoom{
    
    //聊天室类型进入时需要调用加入聊天室接口，退出时需要调用退出聊天室接口
    DHWeakSelf;
    [[RCIMClient sharedRCIMClient]
     joinChatRoom:self.targetId
     messageCount:-1
     success:^{
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"] &&
                 [[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
                 
                 RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                 
                 [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo = userInfo;
                 
                 RCChatroomWelcome *message = [[RCChatroomWelcome alloc] init];
                 message.id = userInfo.userId;
                 message.extra = userInfo.name;
                 message.senderUserInfo = userInfo;
                 [weakSelf sendMessage:message pushContent:nil success:nil error:nil];
             }
         });
     }
     error:^(RCErrorCode status) {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (status != KICKED_FROM_CHATROOM) {
                 // 提示错误信息
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [weakSelf joinChatRoom];
                 });
             }
         });
     }];
}

#pragma mark - > 退出聊天室
- (void)quitChatRoom{
 
    if (self.conversationType == ConversationType_CHATROOM) {
        
        [[RCIMClient sharedRCIMClient] quitChatRoom:self.targetId
                                            success:^{
                                                NSLog(@"退出聊天室~");
                                            } error:^(RCErrorCode status) {
                                                NSLog(@"退出失败~");
                                            }];
    }
}

#pragma mark - > 返回
- (void)backButtonClick{
    
    [self quitChatRoom];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
        [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] setIsLogin:NO];
    });
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    CGFloat x = 0;
    CGFloat y = FWCustomeSafeTop+FWLiveTopPadding;
    CGFloat w = SCREEN_WIDTH;
    CGFloat h = w*9/16;
    self.containerView.frame = CGRectMake(x, y, w, h);
    
    w = 200;
    h = 35;
    x = (self.containerView.width - w)/2;
    y = (self.containerView.height - h);
    self.textField.frame = CGRectMake(x, y, w, h);
    
    w = 44;
    h = w;
    x = (CGRectGetWidth(self.containerView.frame)-w)/2;
    y = (CGRectGetHeight(self.containerView.frame)-h)/2;
    self.playBtn.frame = CGRectMake(x, y, w, h);
    
    w = SCREEN_WIDTH;
    h = 50+FWCustomeSafeTop;
    x = 0;
    y = SCREEN_HEIGHT - 60;
    self.bottomView.frame = CGRectMake(x, y, w, h);
    
    [self updateTextViewFrame];
}

- (UIStatusBarStyle)preferredStatusBarStyle {

    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return self.player.isStatusBarHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (BOOL)shouldAutorotate {
    return self.player.shouldAutorotate;
}

- (void)adImageClick{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"点击“离开”按钮，即离开直播页面。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"离开" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dealWith];
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"看直播" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.player.currentPlayerManager play];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)dealWith{
    if ([self.finalModel.live_ad_type isEqualToString:@"h5_inner"]) {
        /* 内部打开h5 */
        [self openWebView:self.finalModel.live_ad_val];
    }else if ([self.finalModel.live_ad_type isEqualToString:@"h5_outer"]) {
        /* 外部打开h5 */
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.finalModel.live_ad_val] options:@{} completionHandler:nil];
    }else if ([self.finalModel.live_ad_type isEqualToString:@"tianmao"]) {
        /* 天猫 */
        NSURL * tamllURL = [NSURL URLWithString:[tmallDetail(self.finalModel.live_ad_goods_id) stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        if ([[UIApplication sharedApplication] canOpenURL:tamllURL]){
            
            [[UIApplication sharedApplication] openURL:tamllURL options:@{} completionHandler:nil];
        }else{
            [self openWebView:self.finalModel.live_ad_val];
        }
    }else if ([self.finalModel.live_ad_type isEqualToString:@"jingdong"]) {
        /* 京东 */
        NSURL * jdURL = [NSURL URLWithString:[JDDetail(self.finalModel.live_ad_goods_id) stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        if ([[UIApplication sharedApplication] canOpenURL:jdURL]){
            
            [[UIApplication sharedApplication] openURL:jdURL options:@{} completionHandler:nil];
        }else{
            [self openWebView:self.finalModel.live_ad_val];
        }
    }else if ([self.finalModel.live_ad_type isEqualToString:@"taobao"]) {
        /* 淘宝 */
        NSURL * taobaoURL = [NSURL URLWithString:taobaoDetail(self.finalModel.live_ad_goods_id)];
        if ([[UIApplication sharedApplication] canOpenURL:taobaoURL]) {
            
            [[UIApplication sharedApplication] openURL:taobaoURL options:@{} completionHandler:nil];
        }else{
            [self openWebView:self.finalModel.live_ad_val];
        }
    }else if ([self.finalModel.live_ad_type isEqualToString:@"weidian"]) {
        /* 微店 */
        NSURL * weidianURL = [NSURL URLWithString:weidianDetail(self.finalModel.live_ad_goods_id)];

        if ([[UIApplication sharedApplication] canOpenURL:weidianURL]) {
            
            [[UIApplication sharedApplication] openURL:weidianURL options:@{} completionHandler:nil];
        }else{
            [self openWebView:self.finalModel.live_ad_val];
        }
    }
}

#pragma mark - > 如果打不开走H5
- (void)openWebView:(NSString *)urlString{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.htmlStr = urlString;
    WVC.pageType = WebViewTypeURL;
    [self.navigationController pushViewController:WVC animated:YES];
}
#pragma mark - about keyboard orientation

/// 键盘支持横屏，这里必须设置支持多个方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if (self.player.isFullScreen) {
        return UIInterfaceOrientationMaskLandscape;
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
    }
    return _controlView;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.placeholder = @"说点什么吧";
    }
    return _textField;
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [UIView new];
    }
    return _containerView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setImage:[UIImage imageNamed:@"new_allPlay_44x44_"] forState:UIControlStateNormal];
    }
    return _playBtn;
}

- (FWBaseAutoTextView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[FWBaseAutoTextView alloc] init];
        _bottomView.textView.delegate = self;
        _bottomView.delegate = self;

    }
    return _bottomView;
}

#pragma mark - > 输入框跟随键盘动
- (void)updateTextViewFrame {
    CGFloat textViewHeight = self.bottomView.keyboardHeight > self.bottomView.commentViewBgHeight ? self.bottomView.textHeight + 2*TOP_BOTTOM_INSET-9 : ceilf(self.bottomView.textView.font.lineHeight) + 2*TOP_BOTTOM_INSET-9;
    self.bottomView.textView.layer.cornerRadius = 2;
    self.bottomView.textView.frame = CGRectMake(15, 12, CGRectGetMinX(self.bottomView.sendButton.frame)-30, textViewHeight);
    self.bottomView.container.frame = CGRectMake(0, 0, SCREEN_WIDTH, textViewHeight + self.bottomView.keyboardHeight);
    self.bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-textViewHeight - self.bottomView.keyboardHeight, SCREEN_WIDTH, textViewHeight + self.bottomView.keyboardHeight);
}

//keyboard notification
- (void)keyboardWillShow:(NSNotification *)notification {
    self.bottomView.keyboardHeight = [notification keyBoardHeight]+25;
    [self updateTextViewFrame];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.bottomView.keyboardHeight = self.bottomView.commentViewBgHeight;
    [self updateTextViewFrame];
}

- (void)HidekeyboardHide:(NSNotification *)notification{
    
    [self.view endEditing:YES];
}

//textView delegate
-(void)textViewDidChange:(UITextView *)textView {
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:textView.attributedText];
    
    if(!textView.hasText) {
        self.bottomView.textHeight = ceilf(self.bottomView.textView.font.lineHeight);
    }else {
        self.bottomView.textHeight = [attributedText multiLineSize:SCREEN_WIDTH - LEFT_INSET - RIGHT_INSET].height;
    }
    [self updateTextViewFrame];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@""]) {
        return YES;
    }
    
    NSString * tempString = [textView.text stringByAppendingString:text];
    if (tempString.length >=50) {
        tempString = [tempString substringToIndex:50];
        
        self.bottomView.textView.text = tempString;
        return NO;
    }
    
    
    if([text isEqualToString:@"\n"]) {
        [self sendMessage];
    }
    return YES;
}

#pragma mark - > 发送消息
- (void)sendMessage{
    
    [self onSendText:self.bottomView.textView.text];
    self.bottomView.textView.text = @"";
    self.bottomView.textHeight = ceilf(self.bottomView.textView.font.lineHeight);
    [self.bottomView.textView resignFirstResponder];
}

#pragma mark - > 发送消息
- (void)onSendText:(NSString *)string{
    
    [self cheackLogin];
    
    if ([[string stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0) {
        
        [[FWHudManager sharedManager] showErrorMessage:@"写点什么吧~" toController:self];
        return;
    }
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]) {
        RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:string];
        rcTextMessage.senderUserInfo = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo;
        [self sendMessage:rcTextMessage pushContent:nil success:nil error:nil];
    }
}

//保存成功调用的方法
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    
    [[FWHudManager sharedManager] showSuccessMessage:@"保存成功" toController:self];
}
#pragma mark - > 分享海报
- (void)shareMatchPost{
    
    NSString *urlString ;
    if (self.isLive) {
        urlString = self.finalModel.live_image;
    }else{
        urlString = self.liveListModel.logo;
    }
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    UIImage *image = [UIImage imageWithData:data];
    
    FWShareScoreImageViewController * vc = [[FWShareScoreImageViewController alloc] init];
    vc.shareType = @"3";
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    vc.postImage = image;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - > WMPageController 相关配置
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController
{
    return self.titleData.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    
    return self.titleData[index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    
    DHWeakSelf;
    
    if (index == 0){
        FWChatViewController * chatRoomVC = [[FWChatViewController alloc] init];
        chatRoomVC.conversationType = ConversationType_CHATROOM;
        chatRoomVC.targetId = self.roomId;
        chatRoomVC.defaultHistoryMessageCountOfChatRoom = -1;
        return chatRoomVC;
    }else if(index == 1){
        FWRealPerformanceViewController * RPVC = [[FWRealPerformanceViewController alloc] init];
        RPVC.sport_id = self.sport_id;
        RPVC.myBlock = ^(NSString * _Nonnull countString) {
            weakSelf.countLabel.text = [NSString stringWithFormat:@"%@人观看",countString];
        };
        return RPVC;
    }else if(index == 2){
        FWRankViewController * rankVC = [[FWRankViewController alloc] init];
        rankVC.sport_id = self.sport_id;
        rankVC.myBlock = ^(NSString * _Nonnull countString) {
            weakSelf.countLabel.text = [NSString stringWithFormat:@"%@人观看",countString];
        };
        return rankVC;
    }else{
        FWGuessingCompetitionViewController * guessVC = [[FWGuessingCompetitionViewController alloc] init];
        guessVC.sport_id = self.sport_id;
        guessVC.myBlock = ^(NSString * _Nonnull countString) {
            weakSelf.countLabel.text = [NSString stringWithFormat:@"%@人观看",countString];
        };
        return guessVC;
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView
{
    return CGRectMake(0,self.wmMenuY, self.view.frame.size.width, self.menuHeight);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView
{
    return CGRectMake(0, self.wmMenuY+self.menuHeight, self.view.frame.size.width, self.view.frame.size.height - self.menuHeight-self.wmMenuY);
}

-(void)pageController:(WMPageController *)pageController didEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info{
    
    NSInteger index = [[info objectForKey:@"index"] integerValue]?[[info objectForKey:@"index"] integerValue]:0;
    [self.view endEditing:YES];

    if (index == 0) {

        self.bottomView.hidden = NO;
        self.menuView.frame = CGRectMake(0, self.wmMenuY+self.menuHeight, self.view.frame.size.width, self.view.frame.size.height - self.menuHeight-self.wmMenuY-FWCustomeSafeTop-50-FWSafeBottom);
    }else if (index == 1){
        
        self.bottomView.hidden = YES;
        self.menuView.frame = CGRectMake(0, self.wmMenuY+self.menuHeight, self.view.frame.size.width, self.view.frame.size.height - self.menuHeight-self.wmMenuY);
    }else if (index == 2){
        
        self.bottomView.hidden = YES;
        self.menuView.frame = CGRectMake(0, self.wmMenuY+self.menuHeight, self.view.frame.size.width, self.view.frame.size.height - self.menuHeight-self.wmMenuY);
    }else if (index == 3){
        
        self.bottomView.hidden = YES;
        self.menuView.frame = CGRectMake(0, self.wmMenuY+self.menuHeight, self.view.frame.size.width, self.view.frame.size.height - self.menuHeight-self.wmMenuY);
    }
}

#pragma mark sendMessage/showMessage
/**
 发送消息
 
 @param messageContent 消息
 @param pushContent pushContent
 */
- (void)sendMessage:(RCMessageContent *)messageContent
        pushContent:(NSString *)pushContent
            success:(void (^)(long messageId))successBlock
              error:(void (^)(RCErrorCode nErrorCode, long messageId))errorBlock {
    if (_targetId == nil) {
        return;
    }
    messageContent.senderUserInfo = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo;
    if (messageContent == nil) {
        return;
    }
    
    __weak typeof(&*self) __weakself = self;
    [[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager] sendMessage:self.conversationType targetId:self.targetId content:messageContent pushContent:pushContent pushData:nil success:^(long messageId) {
        dispatch_async(dispatch_get_main_queue(), ^{
            RCMessage *message = [[RCMessage alloc] initWithType:__weakself.conversationType
                                                        targetId:__weakself.targetId
                                                       direction:MessageDirection_SEND
                                                       messageId:messageId
                                                         content:messageContent];
            //  过滤礼物消息，弹幕消息,退出聊天室消息不插入数据源中；
            if ([messageContent isMemberOfClass:[RCChatroomGift class]] || [messageContent isMemberOfClass:[RCChatroomBarrage class]] || [messageContent isMemberOfClass:[RCChatroomUserQuit class]]) {
            } else {
                message.senderUserId = [RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId;
                
                [__weakself.bottomView.textView endEditing:YES];
                [__weakself.bottomView.textView setText:@""];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:RongYunRecieveNofi object:nil userInfo:@{@"rcMessage":message}];
            }
        });
        if (successBlock) {
            successBlock(messageId);
        }
    } error:^(RCErrorCode nErrorCode, long messageId) {
        [__weakself.bottomView.textView setText:@""];
        NSLog(@"发送失败，errorcode is: %ld",(long)nErrorCode);
        if (errorBlock) {
            errorBlock(nErrorCode, messageId);
        }
    }];
    
}

/**
 *  接收到消息的回调
 */
- (void)didReceiveMessageNotification:(NSNotification *)notification {
    __block RCMessage *rcMessage = notification.object;
    RCCRMessageModel *model = [[RCCRMessageModel alloc] initWithMessage:rcMessage];
    
    if (model.conversationType == self.conversationType &&
        [model.targetId isEqual:self.targetId]) {
        __weak typeof(&*self) __blockSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            //  对礼物消息,赞消息进行拦截，展示动画，不插入到数据源中,对封禁消息，弹出alert
            if (rcMessage) {
                if ([rcMessage.content isMemberOfClass:[RCChatroomWelcome class]]) {
                    //  过滤自己发送的欢迎消息
                    if ([rcMessage.senderUserId isEqualToString:[RCCRRongCloudIMManager sharedRCCRRongCloudIMManager].currentUserInfo.userId]) {
                        return;
                    }
                }
            }
        });
    }
}

#pragma mark - > 检测是否登录
- (void)cheackLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        /* 登录 */
        [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        
        return;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
   
    [self.player.currentPlayerManager pause];
    [self.playerManager pause];
    
    [self.view endEditing:YES];
}

#pragma mark - > 监听当前网络状态
- (void)monitorLiveNet{
    
    self.isRefresh = YES;
    
    
    if ([[self.liveStatusDict objectForKey:@"live_status"] isEqualToString:@"1"] ||
        [[self.liveStatusDict objectForKey:@"live_status"] isEqualToString:@"3"]) {
        
        self.controlView.failBtn.hidden = NO;
        self.controlView.failBtn.enabled = NO;
    }else{
        self.controlView.failBtn.hidden = YES;
        self.controlView.failBtn.enabled = NO;
    }
    
    
    [self checkWifi];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

@end

