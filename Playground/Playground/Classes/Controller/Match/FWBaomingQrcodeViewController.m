//
//  FWBaomingQrcodeViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaomingQrcodeViewController.h"

@interface FWBaomingQrcodeViewController ()

@property (nonatomic, strong) UIImageView * qrcodeImageView ;
@property (nonatomic, strong) NSTimer * timer;

@end

@implementation FWBaomingQrcodeViewController
#pragma mark - > 生命周期
- (void)dealloc {
    NSLog(@"销毁了");
    self.timer = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    [self trackPageBegin:@"二维码页"];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"二维码页"];
    
    [self.timer invalidate];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupSubviews];
}

- (void)setupSubviews{
    
    UIView * mainView = [[UIView alloc] init];
    mainView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    mainView.layer.cornerRadius = 5;
    mainView.layer.masksToBounds = YES;
    [self.view addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset((SCREEN_HEIGHT-(319*(SCREEN_WIDTH-118)/261))*2/5);
        make.left.mas_equalTo(self.view).mas_offset(58);
        make.width.mas_equalTo(SCREEN_WIDTH-118);
        make.height.mas_equalTo(319*(SCREEN_WIDTH-118)/261);
    }];
    
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.text = self.titleString;
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    titleLabel.textColor = FWTextColor_222222;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(mainView).mas_offset(16);
        make.centerX.mas_equalTo(mainView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(18);
    }];

    UIButton * closeButton = [[UIButton alloc] init];
    [closeButton setImage:[UIImage imageNamed:@"upload_close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeCurrentVC) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:closeButton];
    [closeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(44, 44));
        make.top.right.mas_equalTo(mainView).mas_offset(0);
    }];
    
    UILabel * nameLabel = [[UILabel alloc] init];
    nameLabel.text = self.name;
    nameLabel.numberOfLines = 0;
    nameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    nameLabel.textColor = FWTextColor_222222;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    [mainView addSubview:nameLabel];
    [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(15);
        make.centerX.mas_equalTo(mainView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(mainView).mas_offset(14);
        make.right.mas_equalTo(mainView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(18);
    }];
    
    self.qrcodeImageView = [[UIImageView alloc] init];
    [self.qrcodeImageView sd_setImageWithURL:[NSURL URLWithString:self.qrcode]];
    [mainView addSubview:self.qrcodeImageView];
    [self.qrcodeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(mainView);
        make.width.height.mas_equalTo(218);
        make.bottom.mas_equalTo(mainView).mas_offset(-15);
    }];
    [mainView bringSubviewToFront:nameLabel];
    
    if (self.exp_time > 0 ) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.exp_time target:self selector:@selector(reloadImage) userInfo:nil repeats:YES];
    }
}

- (void)closeCurrentVC{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)reloadImage{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    [request startWithParameters:params WithAction:Get_gold_writeoff_code WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [self.qrcodeImageView sd_setImageWithURL:[NSURL URLWithString:[[back objectForKey:@"data"] objectForKey:@"url"]]];
        }
    }];
}


@end
