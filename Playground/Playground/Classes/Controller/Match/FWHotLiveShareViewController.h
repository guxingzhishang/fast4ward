//
//  FWHotLiveShareViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWLiveModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWHotLiveShareViewController : FWBaseViewController

@property (nonatomic, strong) UIImage * postImage;
@property (nonatomic, strong) FWLiveListModel * listModel;

@end

NS_ASSUME_NONNULL_END
