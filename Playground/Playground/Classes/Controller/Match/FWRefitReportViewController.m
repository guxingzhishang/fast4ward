//
//  FWRefitReportViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWRefitReportViewController.h"
#import "FWRefitReportCell.h"
#import "FWRefitReportModel.h"
#import "FWRefitReportDetailViewController.h"

@interface FWRefitReportViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) FWRefitReportModel * reportModel;
@end

@implementation FWRefitReportViewController
@synthesize infoTableView;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 获取改装情报列表
- (void)requestRefitList:(BOOL)isLoadMoredData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    NSDictionary * params  = @{
                               @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                               @"page":@(self.pageNum).stringValue,
                               @"page_size":@"20",
                               };
    
    [request startWithParameters:params WithAction:Get_refit_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            self.reportModel = [FWRefitReportModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            [self.dataSource addObjectsFromArray: self.reportModel.list];

            if([self.reportModel.list count] == 0 &&
               self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;

                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                [[FWHudManager sharedManager] showErrorMessage:@"您还未登录，请先登录才能查看关注哦~" toController:self];
                return ;
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手改装情报页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手改装情报页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"车手改装情报";
    
    [self setupSubViews];
    [self requestRefitList:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 185;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestRefitList:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestRefitList:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWRefitReportCellID";
    
    FWRefitReportCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWRefitReportCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.vc = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.dataSource.count > indexPath.row) {
        [cell configForCell:self.dataSource[indexPath.row] WithRefitListKeysModel:self.reportModel.refit_list_keys];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < self.dataSource.count) {
        FWRefitReportListModel * listModel = self.dataSource[indexPath.row];
        
        FWRefitReportDetailViewController * RRDVC = [[FWRefitReportDetailViewController alloc] init];
        RRDVC.feed_id = listModel.feed_info.feed_id;
        RRDVC.bestet_id = listModel.driver_info.bestet_id;
        [self.navigationController pushViewController:RRDVC animated:YES];
    }
}
@end
