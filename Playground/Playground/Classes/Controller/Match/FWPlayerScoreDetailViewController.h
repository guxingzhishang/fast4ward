//
//  FWPlayerScoreDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 成绩详情
 */
#import "FWBaseViewController.h"
#import "FWPlayerRankModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerScoreDetailViewController : FWBaseViewController<UIScrollViewDelegate>
@property (nonatomic, strong) FWScoreDetailModel * ScoreModel;
@end

NS_ASSUME_NONNULL_END
