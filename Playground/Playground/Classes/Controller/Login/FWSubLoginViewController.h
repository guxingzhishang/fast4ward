//
//  FWSubLoginViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 游客的登录（页面没有游客登录了，登录成功返回之前页面）
 */
#import "FWBaseViewController.h"
#import "FWLoginView.h"
#import "WXApi.h"
#import "WXApiObject.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWSubLoginDelegate <NSObject>

- (void)logininSuccess;

@end

@interface FWSubLoginViewController : FWBaseViewController<FWLoginViewDelegate>

@property (nonatomic, strong) FWLoginView * loginView;

@property (nonatomic, strong) NSString * lastIndex;

/* 登录或注册成功后返回到登录页 */
@property (nonatomic, assign) BOOL  isBack;

@property (nonatomic, weak) id<FWSubLoginDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
