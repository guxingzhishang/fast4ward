//
//  FWLoginViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWLoginViewController.h"
#import "FWWXLoginRequest.h"
#import "FWLoginModel.h"
#import "FWPhoneViewController.h"
#import "FWInformationViewController.h"
#import "FWChangePhoneViewController.h"

#import "FWAttentionRefitTopicViewController.h"

// 临时的
#import "FWPublishPreviewViewController.h"
#import "FWPublishPreviewGraphViewController.h"

@interface FWLoginViewController ()
@property (nonatomic, strong) NSDictionary * dataDict;
@end

@implementation FWLoginViewController
@synthesize loginView;

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"登录页-主"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"登录页-主"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WXLoginFinished:) name:kTagWXShareFinished object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WXLoginRefresh:) name:kTagWXLoginRefresh object:nil];

    self.dataDict = @{};
    [self requestStsData];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    [self setupSubViews];
    
    if (![[GFStaticData getObjectForKey:is_Show_AD] boolValue]) {
        
        [GFStaticData saveObject:@"YES" forKey:is_Show_AD];
        
        FWADViewController *vc = [[FWADViewController alloc] init];
        vc.navi = self.navigationController;
        [DHWindow addDHViewController:vc priority:4000];
    }
}

- (void)bindViewModel{
    [super bindViewModel];
}

- (void)backBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupSubViews{
    
    loginView = [[FWLoginView alloc] init];
    loginView.delegate = self;
    [self.view addSubview:loginView];
    [loginView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        //安装微信不做任何操作
    }else{
        loginView.wechatLoginButton.hidden = YES;
        [loginView.phoneButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(70, 25));
            make.top.mas_equalTo(loginView.ballImageView.mas_bottom).mas_offset(105);
            make.left.mas_equalTo(loginView).mas_offset(58);
        }];
    }
}

- (void)agreementClick:(NSInteger)index{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    
    if (index == 0) {
        WVC.htmlStr = HTMLAPI_URL(StandardHTML);
        WVC.webTitle = @"社区规范";
        
    }else{
        WVC.htmlStr = HTMLAPI_URL(AgreementHTML);
        WVC.webTitle = @"隐私政策";
    }
    
    WVC.pageType = WebViewTypeURL;
    [self.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 手机登录
- (void)phoneButtonClick{
    
    FWPhoneViewController * PVC = [[FWPhoneViewController alloc] init];
    PVC.type = 1;
    PVC.fromLogin = 1;
    [self.navigationController pushViewController:PVC animated:YES];
}

#pragma mark - > 游客访问
- (void)visitorButtonClick{

    [GFStaticData saveObject:@"0" forKey:kTagUserKeyID];
    [GFStaticData saveObject:@"0" forKey:kTagUserKeyName];

    [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
}

#pragma mark - > 微信登录
-(void) wechatLoginButtonClick{
    
    [GFStaticData saveObject:Submit_weixin_login forKey:@"WXOperation"];
    
    SendAuthReq * req = [[SendAuthReq alloc] init];
    req.openID = WXAppKey;
    req.scope = @"snsapi_userinfo";
    req.state = @"wechat_sdk_demo";
    [WXApi sendReq:req];
}

// 微信请求结束
- (void)WXLoginFinished:(NSNotification *)noti{
    
    BaseResp * resp = [noti object];
    if (resp.errCode == 0)
    {
        if ([resp isKindOfClass:[SendAuthResp class]])
        {
            SendAuthResp * sendAuthResp = (SendAuthResp *)resp;
            if (sendAuthResp.code)
            {
                [self getAccessTokenWithCode:[NSString stringWithFormat:@"%@",sendAuthResp.code]];
            }
        }
    }
}

- (void)getAccessTokenWithCode:(NSString *)code{
    
    FWWXLoginRequest * request = [[FWWXLoginRequest alloc] init];
    [request getAccessTokenWithCode:code WithAction:WXLogin];
}


- (void)WXLoginRefresh:(NSNotification *)noti{

    [self requestMineInfo];
    
    NSDictionary * dict = (NSDictionary *)[noti object];
    FWLoginModel * model = [FWLoginModel mj_objectWithKeyValues:dict];
    
    if ([model.next_page isEqualToString:@"bind_mobile"]) {
       
        /* 展示绑定手机的页面 */
        FWPhoneViewController * PVC = [[FWPhoneViewController alloc] init];
        PVC.fromLogin = 1;
        PVC.type = 2;
        [self.navigationController pushViewController:PVC animated:YES];
    }else if ([model.next_page isEqualToString:@"follow_users"]){
        
        /* 展示推荐关注的用户 */
        FWAttentionRefitTopicViewController * ARTVC = [[FWAttentionRefitTopicViewController alloc] init];
        ARTVC.fromLogin = 1;
        [self.navigationController pushViewController:ARTVC animated:YES];
    }else if ([model.next_page isEqualToString:@"follow_tags"]){
        
        /* 展示推荐关注的标签 */
        FWAttentionRefitTopicViewController * ARTVC = [[FWAttentionRefitTopicViewController alloc] init];
        ARTVC.fromLogin = 1;
        [self.navigationController pushViewController:ARTVC animated:YES];
    }else if ([model.next_page isEqualToString:@"fill_profile"]){
       
        /* 展示设置头像和昵称的页面 */
        FWInformationViewController * IVC = [[FWInformationViewController alloc] init];
        IVC.fromLogin = 1;
        [self.navigationController pushViewController:IVC animated:YES];
    }else if ([model.next_page isEqualToString:@"home"]){
        /* 首页 */
        
        if ([model.sync_weixin_header isEqualToString:@"1"]) {
            
            NSDictionary * weixinDict = [self dictionaryWithJsonString:[GFStaticData getObjectForKey:kTagWeiXinUserInfo]];

            if ([weixinDict isKindOfClass:[NSDictionary class]]) {
                
                //异步并列执行
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSString * headerURL = weixinDict[@"headimgurl"];
                    NSData *data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:headerURL]];
                    UIImage *avatar = [UIImage imageWithData:data];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [ALiOssUploadTool asyncUploadImage:avatar WithBucketName:[self.dataDict objectForKey:@"bucket"] WithObject_key:[self.dataDict objectForKey:@"object_key"] WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
                            if (UploadImageSuccess == state) {
//                                NSLog(@"成功了");
                                NSString * header_url = [self.dataDict objectForKey:@"object_key"];
                                [self savePhotoInfo:header_url];
                            }
                        }];
                    });
                });
            }
        }
        
        [GFStaticData saveObject:@"2" forKey:Push_Home_Segement];

        [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
    }
}

#pragma mark - > 请求个人信息
- (void)requestMineInfo{
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]){
        
        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
        NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
        
        [request startWithParameters:params WithAction:Get_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                NSDictionary * infoDict = [[back objectForKey:@"data"] objectForKey:@"user_info"];
                
                [GFStaticData saveLoginData:infoDict];
                
                [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
                [[FWUserCenter sharedManager] saveUserInfoToDiskWithJson:back];
                [FWUserCenter sharedManager].user_IsLogin = YES;
            }
        }];
    }
}

#pragma mark - > 请求阿里临时验证
- (void)requestStsData{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    [request requestStsTokenWithType:@"1"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestUploadName];
    });
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"user_header",
                              @"number":@"1",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.dataDict = [back objectForKey:@"data"];
        }
    }];
}

#pragma mark - > 保存头像
- (void)savePhotoInfo:(NSString *)header_url{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"user_header":header_url,
                              };
    [request startWithParameters:params WithAction:Submit_update_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
//        NSLog(@"kTagWeiXinUserInfo-----%@",responseObject);
    }];
}

#pragma mark - NSString 转 字典NSDictionary
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {/*JSON解析失败*/
        
        return nil;
    }
    return dic;
}
@end
