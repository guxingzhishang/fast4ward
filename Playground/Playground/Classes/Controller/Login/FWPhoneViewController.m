//
//  FWPhoneViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWPhoneViewController.h"
#import "FWChangePhoneViewController.h"
#import "FWLoginGetCodeRequest.h"


@interface FWPhoneViewController ()

@end

@implementation FWPhoneViewController
@synthesize phoneView;

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    if (self.type != 3) {
        [self trackPageEnd:@"手机号页"];
    }else{
        [self trackPageEnd:@"更换手机号页面"];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if (self.type != 3) {
        [self trackPageBegin:@"手机号页"];
    }else{
        [self trackPageBegin:@"更换手机号页"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    phoneView = [[FWPhoneView alloc] init];
    phoneView.type = self.type;
    phoneView.delegate = self;
    [self.view addSubview:phoneView];
    [phoneView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [phoneView.phoneTextFeild becomeFirstResponder];
    });
}

#pragma mark - > 下一步
- (void)nextButtonClick:(UIButton *)sender{
    
    if (phoneView.phoneTextFeild.text.length<11 ) {
        [[FWHudManager sharedManager] showErrorMessage:@"手机号格式不正确" toController:self];
        return;
    }
    
    [self.view endEditing:YES];
    
    self.nextButton = sender;
    sender.enabled = NO;
    // 防止重复点击
    [self performSelector:@selector(changeButtonStatus) withObject:nil afterDelay:1];
    
    NSDictionary * dict = @{@"mobile":phoneView.phoneTextFeild.text};

    NSString * action ;

    if (self.type == 1) {
        action = Get_reglogin_sms;
    }else if (self.type == 2||
              self.type == 3) {
        action = Get_mobile_bind_sms;
    }

    //先收键盘，在请求网络
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        FWLoginGetCodeRequest * request = [[FWLoginGetCodeRequest alloc] init];
        [request startWithParameters:dict WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject,nil);

            if ([code isEqualToString:NetRespondCodeSuccess]) {

                FWChangePhoneViewController * PVC = [[FWChangePhoneViewController alloc] init];
                PVC.codeNumber = [back objectForKey:@""];
                PVC.phoneNumber = phoneView.phoneTextFeild.text;
                PVC.type = self.type;
                PVC.fromLogin = self.fromLogin;
                [self.navigationController pushViewController:PVC animated:YES];
            }else{

                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    });
}

- (void)changeButtonStatus{
    
    self.nextButton.enabled = YES;
}
@end
