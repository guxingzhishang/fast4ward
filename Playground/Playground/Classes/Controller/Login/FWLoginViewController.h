//
//  FWLoginViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 登录页面
 */

#import "FWBaseViewController.h"
#import "FWLoginView.h"
#import "WXApi.h"
#import "WXApiObject.h"

@interface FWLoginViewController : FWBaseViewController<FWLoginViewDelegate>

@property (nonatomic, strong) FWLoginView * loginView;

@end
