//
//  FWAttentionRefitTopicViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/12.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWAttentionRefitTopicViewController.h"
#import "FWAttentionRefitTopicCell.h"
#import "FWAttentionRefitTopicModel.h"

@interface FWAttentionRefitTopicViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) NSString * componentTags;
@property (nonatomic, strong) FWAttentionRefitTopicModel * topicModel;

@end

@implementation FWAttentionRefitTopicViewController

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

-(void)requestData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_reg_recommand WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.topicModel = [FWAttentionRefitTopicModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.dataSource addObjectsFromArray:self.topicModel.tag_list];
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"推荐改装话题页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"推荐改装话题页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UIButton * skipButton = [[UIButton alloc] init];
    skipButton.frame = CGRectMake(0, 0, 45, 40);
    skipButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [skipButton setTitle:@"跳过" forState:UIControlStateNormal];
    [skipButton setTitleColor:FWColor(@"999999") forState:UIControlStateNormal];
    [skipButton addTarget:self action:@selector(skipButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:skipButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    
    [self setupSubViews];
    [self requestData];
}

- (void)setupSubViews{
    
    self.tipLable = [[UILabel alloc] init];
    self.tipLable.text = @"关注感兴趣的改装话题";
    self.tipLable.font = DHBoldFont(22);
    self.tipLable.textColor = FWTextColor_222222;
    self.tipLable.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.tipLable];
    self.tipLable.frame = CGRectMake(0, 5, SCREEN_WIDTH, 30);
    
    self.subTipLabel = [[UILabel alloc] init];
    self.subTipLabel.text = @"为你推荐用户，查看他们发布的精彩改装案例";
    self.subTipLabel.font = DHFont(14);
    self.subTipLabel.textColor = FWTextColor_222222;
    self.subTipLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.subTipLabel];
    self.subTipLabel.frame = CGRectMake(0, CGRectGetMaxY(self.tipLable.frame)+5, SCREEN_WIDTH, 20);
    
    self.nextButton = [[UIButton alloc] init];
    self.nextButton.titleLabel.font = DHFont(14);
    self.nextButton.backgroundColor = FWTextColor_222222;
    [self.nextButton setTitle:@"进入肆放APP" forState:UIControlStateNormal];
    [self.nextButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.nextButton addTarget:self action:@selector(nextButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.nextButton];
    self.nextButton.frame = CGRectMake(18, SCREEN_HEIGHT-36-FWSafeBottom-30-64-FWCustomeSafeTop, SCREEN_WIDTH-36, 36);

    
    self.infoTableView = [[UITableView alloc] init];
    self.infoTableView.delegate = self;
    self.infoTableView.dataSource = self;
    self.infoTableView.rowHeight = 160;
    self.infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.infoTableView];
    [self.infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.subTipLabel.mas_bottom).mas_offset(20);
        make.bottom.mas_equalTo(self.nextButton.mas_top).mas_offset(-20);
        make.left.right.mas_equalTo(self.view);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    FWAttentionRefitTopicListModel * listModel = self.dataSource[section];
    if (listModel.isShowAll) {
        return 1;
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWAttentionRefitTopicCellID";
    
    FWAttentionRefitTopicCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWAttentionRefitTopicCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section < self.dataSource.count) {
        FWAttentionRefitTopicListModel * listModel = self.dataSource[indexPath.section];

        if (indexPath.row < listModel.user_list.count) {
            [cell configForCell:listModel.user_list];
        }
    }

    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    if (self.dataSource.count > 0) {
        
        FWAttentionRefitTopicListModel * listModel = self.dataSource[section];

        UIView * shadowView = [[UIView alloc] init];
        shadowView.userInteractionEnabled = YES;
        shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        shadowView.userInteractionEnabled = YES;
        shadowView.layer.cornerRadius = 5;
        shadowView.layer.shadowOffset = CGSizeMake(0, 0);
        shadowView.layer.shadowOpacity = 0.7;
        shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
        [view addSubview:shadowView];
        shadowView.frame = CGRectMake(18, 10, SCREEN_WIDTH-36, 55);

        
        UIView *bgView = [[UIView alloc] init];
        bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        bgView.frame = CGRectMake(0, 0, SCREEN_WIDTH-36, 55);
        [shadowView addSubview:bgView];
        
        UIImageView * iconImageView = [[UIImageView alloc] init];
        iconImageView.frame = CGRectMake(12, 12, 31, 31);
        [iconImageView sd_setImageWithURL:[NSURL URLWithString:listModel.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        [bgView addSubview:iconImageView];
        
        UIImageView * shapeImageView = [[UIImageView alloc] init];
        shapeImageView.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, 22, 13, 12);
        shapeImageView.image = [UIImage imageNamed:@"shape_question"];
        [bgView addSubview:shapeImageView];
        
        UILabel * titleLabel = [[UILabel alloc] init];
        titleLabel.text = listModel.tag_name;
        titleLabel.font = DHFont(14);
        titleLabel.frame = CGRectMake(CGRectGetMaxX(shapeImageView.frame)+10, 0, 120, 55);
        titleLabel.textColor = FWTextColor_222222;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [bgView addSubview:titleLabel];
        
        UIButton * selectButton = [[UIButton alloc] init];
        selectButton.tag = 999+section;
        [selectButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
        [selectButton setImage:[UIImage imageNamed:@"new_select"]  forState:UIControlStateSelected];
        [selectButton addTarget:self action:@selector(selectButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:selectButton];
        selectButton.frame = CGRectMake(CGRectGetWidth(bgView.frame)-14-20, 17.50, 20, 20);
        if (listModel.isShowAll) {
            selectButton.selected = YES;
        }else{
            selectButton.selected = NO;
        }
        
        UILabel * contentLabel = [[UILabel alloc] init];
        contentLabel.text = listModel.count_text;
        contentLabel.font = DHFont(12);
        contentLabel.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame)+10, 0, CGRectGetWidth(bgView.frame)-CGRectGetMaxX(titleLabel.frame)-20-14-20, 55);
        contentLabel.textColor = DHLineHexColor_LightGray_999999;
        contentLabel.textAlignment = NSTextAlignmentRight;
        [bgView addSubview:contentLabel];
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 75;
}

- (void)selectButtonClick:(UIButton *)sender{
    NSInteger index = sender.tag - 999;
    
    FWAttentionRefitTopicListModel * listModel = self.dataSource[index];
    if (listModel.isShowAll) {
        listModel.isShowAll = NO;
        
        for (FWAttentionRefitTopicListModel * model in self.dataSource) {
            
            for (int i =0; i<model.user_list.count; i++) {
                FWAttentionRefitTopicUserModel * userModel = model.user_list[i];
                userModel.isSelectUser = NO;
            }
        }
    }else{
        listModel.isShowAll = YES;
        
        for (FWAttentionRefitTopicListModel * model in self.dataSource) {
            
            for (int i =0; i<model.user_list.count; i++) {
                FWAttentionRefitTopicUserModel * userModel = model.user_list[i];
                userModel.isSelectUser = YES;
            }
        }
    }
    [self.infoTableView reloadData];
}

#pragma mark - > 跳过
- (void)skipButtonClick{
    [self requestSkipWithType:@"2"];
}

#pragma mark - > 请求跳过 (1为点击 我选好了；2为点击跳过)
- (void)requestSkipWithType:(NSString *)type{
    
    NSDictionary * params = @{ @"jump_status":type};
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    [request startWithParameters:params WithAction:Submit_jump_followv2 WithDelegate:self  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back  = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (self.fromLogin == 2) {
                // 游客模式登录，返回
                __block FWSubLoginViewController * loginVC;
                [self.navigationController.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj isMemberOfClass:[FWSubLoginViewController class]])
                    {
                        loginVC = obj;
                    }
                }];
                
                if (loginVC != nil)
                {
                    //返回到登录页面
                    loginVC.isBack = YES;
                    [self.navigationController popToViewController:loginVC animated:NO];
                } else {
                    //正常登录，跳转至首页
                    [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
                }
            }else{
                // 正常登录
                [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


#pragma mark - > 关注标签
- (void)nextButtonClick{

    //提取所有关注的UID
    NSMutableArray * tempArr =@[].mutableCopy;
    for (FWAttentionRefitTopicListModel * model in self.dataSource) {
        
        for (int i =0; i<model.user_list.count; i++) {
            FWAttentionRefitTopicUserModel * userModel = model.user_list[i];
            if (userModel.isSelectUser) {
                [tempArr addObject:userModel.uid];
            }
        }
    }
    
    // 获得用户选取的数据
    NSMutableArray *result = @[].mutableCopy;
    for (FWAttentionRefitTopicListModel *model in self.dataSource) {
        if (model.isShowAll) {
            [result addObject:model.tag_id];
        }
    }
    
    NSString * follow = @"";
    if (tempArr.count >0) {
        follow = [tempArr componentsJoinedByString:@","];
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"请选择要关注的大咖" toController:self];
        return;
    }
    
    if (result.count >0) {
        self.componentTags = [result componentsJoinedByString:@","];
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"请选择标签" toController:self];
        return;
    }
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"tag_id":self.componentTags,
                              @"f_uid":follow,
    };

    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Submit_follow_user_tags  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back  = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [self requestSkipWithType:@"1"];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

@end
