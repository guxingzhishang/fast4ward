//
//  FWPhoneViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 首次绑定手机 或 手机登录
 */


#import "FWBaseViewController.h"
#import "FWPhoneView.h"

@interface FWPhoneViewController : FWBaseViewController<FWPhoneViewDelegate>

@property (nonatomic, strong) FWPhoneView * phoneView;

/* 1、手机号登录  2、微信登录绑定手机号  3、更换新手机号 */
@property (nonatomic, assign) NSInteger type;

@property (nonatomic, strong) UIButton * nextButton;
/* 1、正常登录   2、游客模式下的登录（成功后要返回登录之前停留页面）*/
@property (nonatomic, assign) NSInteger  fromLogin;


@end
