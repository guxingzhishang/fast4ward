//
//  FWAttentionRefitTopicViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/12.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWAttentionRefitTopicViewController : FWBaseViewController

@property (nonatomic, strong) UILabel * tipLable;
@property (nonatomic, strong) UILabel * subTipLabel;
@property (nonatomic, strong) UIButton * nextButton;
@property (nonatomic, strong) UITableView * infoTableView;

@property (nonatomic, assign) NSInteger  fromLogin;

@end

NS_ASSUME_NONNULL_END
