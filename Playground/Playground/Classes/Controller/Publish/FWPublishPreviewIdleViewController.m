//
//  FWPublishPreviewIdleViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/18.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWPublishPreviewIdleViewController.h"
#import "FWSearchTagsViewController.h"
#import "FWPublishRequest.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import "CZHTool.h"
#import "Header.h"
#import "CZHChooseCoverController.h"

#import "FWUpdateInfomationRequest.h"

#import "FWUploadManager.h"
#import "FWChooseBradeViewController.h"

@interface FWPublishPreviewIdleViewController ()

@property (nonatomic, assign) BOOL isUpload;
@property (nonatomic, strong) NSString * carType;

@end

@implementation FWPublishPreviewIdleViewController
@synthesize previewView;
@synthesize tagsArray;
@synthesize feed_id;
@synthesize isUploadVideo;
@synthesize nextButton;

- (void)setPhotos:(NSMutableArray *)photos{
    _photos = photos;
}

#pragma mark - > 请求阿里云临时凭证
- (void)requsetStsData{
    FWPublishRequest * requst = [[FWPublishRequest alloc] init];
    [requst requestStsTokenWithType:@"1"];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [GFStaticData saveObject:nil forKey:ChooseCarTypeBackToPublish];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"图文发布页"];

    [[UIApplication sharedApplication] setStatusBarHidden:NO];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [IQKeyboardManager sharedManager].enable = YES;
    });
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"图文发布页"];

    [IQKeyboardManager sharedManager].enable = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.title = @"发布预览";
    self.fd_interactivePopDisabled = YES;

    self.imagesArray = @[].mutableCopy;
    tagsArray = @[].mutableCopy;
    self.objectDict = @{}.mutableCopy;

    /* 获取临时凭证 */
    [self requsetStsData];
    
    [self setupSubViews];
}

- (void)bindViewModel{
    [super bindViewModel];
}

#pragma mark - > 视图初始化
- (void)setupSubViews{
    
    previewView = [[FWPublishPreviewIdleView alloc] init];
    previewView.publishDelegate = self;
    previewView.vc = self;
    [self.view addSubview:previewView];
    [previewView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    if (self.editType == 2) {

        [previewView.deleteButton setTitle:@"删除草稿" forState:UIControlStateNormal];
        
        NSMutableArray * array = [[GFStaticData getObjectForKey:self.feed_id] mutableCopy];
       
        NSString *path  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;

        NSMutableArray<UIImage *> * imageTempArr = @[].mutableCopy;
        for (NSString * imageName in array) {
            
            NSString * imagePath = [path stringByAppendingFormat:@"%@",imageName];

            UIImage * getImage =[[UIImage alloc]initWithContentsOfFile:imagePath];
            [imageTempArr addObject:getImage];
        }
        [self.imagesArray addObjectsFromArray:imageTempArr];
        
        previewView.images = imageTempArr;
        
        [previewView configForView:self.listModel];
    }else{
        previewView.images = self.photos;
        [previewView configForView:nil];
    }
}

#pragma mark - > 是否删除草稿
- (void)deleteButtonClick:(UIButton *)sender {
    
    self.nextButton = sender;
    self.nextButton.enabled = NO;
    
    // 防止重复点击
    [self performSelector:@selector(changeButtonStatus) withObject:nil afterDelay:3];
    
    if (self.editType == 2) {
        
        // 删除草稿，直接请求接口
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除草稿" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self requestDeleteDraft];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        // 保存草稿
        self.is_draft = @"1";
        /* 获取上传资源的文件名 */
        [self requestUploadName:@"2"];
    }
}

#pragma mark - > 删除草稿
- (void)requestDeleteDraft{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.draft_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_delete_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"删除成功" toController:self];
            
            NSMutableArray * array = [[GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]] mutableCopy];
            
            if ([array containsObject:self.draft_id]) {
                [array removeObject:self.draft_id];
            }

            [GFStaticData saveObject:array forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];

            [self popToPreViewController];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


#pragma mark - > 选择车系
- (void)carTypeButtonClick{
    
    FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
    CBVC.chooseFrom = @"publish";
    [self.navigationController pushViewController:CBVC animated:YES];
}

#pragma mark - > 发布
- (void)publishClick:(UIButton *)sender{
    
    self.nextButton = sender;
    sender.enabled = NO;
    
    // 防止重复点击
    [self performSelector:@selector(changeButtonStatus) withObject:nil afterDelay:3];
    
    self.is_draft = @"2";
    [self.view endEditing:YES];
    
    /* 获取上传资源的文件名 */
    [self requestUploadName:@"1"];
}


- (void)changeButtonStatus{
    
    self.nextButton.enabled = YES;
}

#pragma mark - > 返回
- (void)backBtnClick{
    
    self.is_draft = @"1";
    [self.view endEditing:YES];

    if ([[previewView.addImageView getImagesArray] count] <= 0) {
        [self popToPreViewController];
        return;
    }
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否保存到草稿箱" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"不保存" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

        if (self.editType == 1) {
            for (UIViewController * item in self.navigationController.viewControllers) {
                if ([item isKindOfClass:[FWTagsViewController class]]) {
                    [self.navigationController popToViewController:item animated:YES];
                    return ;
                }
            }
            [self popToPreViewController];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        /* 获取上传资源的文件名 */
        [self requestUploadName:@"2"];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName:(NSString *)type{
    
    self.imagesArray = [previewView.addImageView getImagesArray];
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"image_feed",
                              @"number":@(self.imagesArray.count).stringValue,
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.objectDict = [back objectForKey:@"data"];
            
            [self requestCheckWithType:type];
        }
    }];
}


#pragma mark - > 预校验
- (void)requestCheckWithType:(NSString *)type{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSMutableDictionary * params = [[self getParams] mutableCopy];
    [params setObject:@"1" forKey:@"flag_check"];
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params.copy WithAction:Submit_add_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            // 发布
            [self uploadFile:type];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

// 上传文件（保存/发布）
- (void)uploadFile:(NSString *)type{
    
    FWUploadManager * defaultManager  = [FWUploadManager sharedRunLoopWorkDistribution];
    defaultManager.objectDict = self.objectDict;
    defaultManager.params = [[self getParams] mutableCopy];
    defaultManager.imagesArray = self.imagesArray;
    [defaultManager requestUploadFengmianImageWithType:@"2"];
   
    [self popToPreViewController];
}

- (NSDictionary *)getParams{
    
    NSMutableArray * tempImageArr = @[].mutableCopy;
    
    for (int i =0; i < self.imagesArray.count ; i++) {
        UIImage * image = self.imagesArray[i];
        
        CGFloat fixelW = CGImageGetWidth(image.CGImage);
        CGFloat fixelH = CGImageGetHeight(image.CGImage);
        
        NSDictionary * dict = @{
                                @"wh":@"",
                                @"img_width":@(fixelW).stringValue,
                                @"img_height":@(fixelH).stringValue,
                                @"path":[self.objectDict objectForKey:@"object_keys"][i],
                                };
        [tempImageArr addObject:dict];
    }
    
    NSString * images = [tempImageArr mj_JSONString];
    
    if (self.draft_id.length <= 0) {
        self.draft_id = @"";
    }

    NSString * if_quanxin = @"2";
    if (self.previewView.otherDictionary[@"if_quanxin"] &&
        [self.previewView.otherDictionary[@"if_quanxin"] length] > 0) {
        if_quanxin = self.previewView.otherDictionary[@"if_quanxin"];
    }
    
    NSString * if_ziti = @"2";
    if (self.previewView.otherDictionary[@"if_ziti"] &&
        [self.previewView.otherDictionary[@"if_ziti"] length] > 0) {
        if_ziti = self.previewView.otherDictionary[@"if_ziti"];
    }
    
    NSString * tags = @"";
    NSMutableArray * tempArr = @[].mutableCopy;

    if (self.previewView.otherDictionary[@"tags"] &&
        [self.previewView.otherDictionary[@"tags"] length] > 0) {
        [tempArr addObject:self.previewView.otherDictionary[@"tags"]];
        tags = [tempArr mj_JSONString];
    }
    
    NSString * car_type = @"";
    if (self.previewView.otherDictionary[@"car_type"] &&
        [self.previewView.otherDictionary[@"car_type"] length] > 0) {
        car_type = self.previewView.otherDictionary[@"car_type"];
    }
    
    NSString * freight = @"";
    if ([self.previewView.priceDictionary[@"baoyou"] length] > 0){
        if([self.previewView.priceDictionary[@"baoyou"] isEqualToString:@"包邮"]) {
            freight = @"2";
        }else{
            freight = @"1";
        }
    }
    
    NSString * price_buy = @"";
    if (self.previewView.priceDictionary[@"yuanjia"] &&
        [self.previewView.priceDictionary[@"yuanjia"] integerValue] > 0 ) {
        price_buy = self.previewView.priceDictionary[@"yuanjia"];
    }
    
    NSString * price_sell = @"";
    if (self.previewView.priceDictionary[@"chushoujia"] &&
        [self.previewView.priceDictionary[@"chushoujia"] integerValue] > 0 ) {
        price_sell = self.previewView.priceDictionary[@"chushoujia"];
    }
    
    NSString * status_niming = @"1";
    if (previewView.nimingSwitch.isOn) {
        status_niming = @"2";
    }
    
    NSString * provice_code = @"";
    if (self.previewView.provice.length > 0 ) {
        provice_code = self.previewView.provice;
    }
    
    NSString * city_code = @"";
    if (self.previewView.city.length > 0 ) {
        city_code = self.previewView.city;
    }
    
    NSString * county_code = @"";
    if (self.previewView.country.length > 0 ) {
        county_code = self.previewView.country;
    }
    
    NSString * xianzhi_category_id = @"";
    if (self.previewView.otherDictionary[@"xianzhi_category_id"]) {
        if ([self.previewView.otherDictionary[@"xianzhi_category_id"] length] > 0 ) {
            xianzhi_category_id = self.previewView.otherDictionary[@"xianzhi_category_id"];
        }
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"is_draft":self.is_draft,
                              @"draft_id":self.draft_id,
                              @"feed_type":@"6",
                              @"video_id":@"",
                              @"feed_id":@"",
                              @"video_name":@"",
                              @"video_cover":@"",
                              @"video_cover_wh":@"",
                              @"video_cover_width":@"",
                              @"video_cover_height":@"",
                              @"imgs":images,
                              @"feed_title":previewView.contentTextView.text,
                              @"flag_check":@"2",
                              @"province_code":provice_code,
                              @"city_code":city_code,
                              @"county_code":county_code,
                              @"price_buy":price_buy,
                              @"price_sell":price_sell,
                              @"freight":freight,
                              @"tags":tags,
                              @"car_type":car_type,
                              @"if_quanxin":if_quanxin,
                              @"if_ziti":if_ziti,
                              @"status_niming":status_niming,
                              @"xianzhi_category_id":xianzhi_category_id,
                              };
    return params;
}

#pragma mark - > 发布 or 存草稿箱 (1为保存草稿，2为发布帖子)
- (void)requestSaveDraft{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSDictionary * parmas = [self getParams];
    
    [request startWithParameters:parmas WithAction:Submit_add_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([self.is_draft isEqualToString:@"1"]) {
                
                [[FWHudManager sharedManager] showErrorMessage:@"保存成功" toController:self];
                
                // 保存草稿
                DHWeakSelf;
                
                dispatch_async(dispatch_queue_create(0,0), ^{
                    NSMutableArray * tempArr = @[].mutableCopy;
                    
                    for (int i =0;i < weakSelf.imagesArray.count;i++) {
                        
                        UIImage * image = weakSelf.imagesArray[i];
                        
                        //设置一个图片的存储路径
                        NSDate *date = [NSDate date];
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
                        NSString *dataString = [dateFormatter stringFromDate:date];
                        
                        //设置一个图片的存储路径
                        
                        NSString *path  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
                        
                        NSString * imageName = [NSString stringWithFormat:@"/%@_%d.png",dataString,i];
                        NSString * imagePath = [path stringByAppendingFormat:@"%@",imageName];
                        
                        /*
                         把图片直接保存到指定的路径（同时应该把图片的名称存起来，在取的时候，拼上前面的路径，因为每次路径是变的。）
                         */
                        [UIImagePNGRepresentation(image) writeToFile:imagePath atomically:YES];
                        [tempArr addObject:imageName];
                    }
                    
                    [GFStaticData saveObject:tempArr forKey:[[back objectForKey:@"data"] objectForKey:@"feed_id"]];
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
                        });
                    });
                });
            }else{
                [[FWHudManager sharedManager] showSuccessMessage:@"发布成功" toController:self];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)popToPreViewController{
    
    if ([self.draft_id isEqualToString:@"0"]) {
        /* 不是草稿，回首页 */
        __weak FWPublishPreviewIdleViewController * weakSelf = self;
        
        weakSelf.navigationController.tabBarController.selectedIndex = 0;
        [weakSelf.navigationController popToRootViewControllerAnimated:NO];
    }else{
        /* 草稿，回草稿箱 */
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
