//
//  FWPublishPreviewViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 视频发布预览
 */

#import "FWBaseViewController.h"
#import "FWPublishPreviewView.h"

@interface FWPublishPreviewViewController : FWBaseViewController<FWPublishPreviewViewDelegate>

@property (nonatomic, strong) FWPublishPreviewView * previewView;

@property (nonatomic, strong) UIButton * nextButton;

/* 封面相关数据 */
@property (nonatomic, strong) NSDictionary * coverImageDict;

/**
 * 用来展示的url，asset
 */
@property (nonatomic, strong) NSURL * pathURL;

/**
 * 实际存储的路径  eg：/var/xxxx/xxxx.mp4
 */
@property (nonatomic, strong) NSString * filePath;

/**
 * 存储标签
 */
@property (nonatomic, strong) NSMutableArray * tagsArray;

/**
 * 存储视频封面的图片地址，和实际路径
 */
@property (nonatomic, strong) NSMutableDictionary * infoDict;

/**
 * 存储获取的上传资源的文件名（bucket，object-key）
 */
@property (nonatomic, strong) NSMutableDictionary * objectDict;

@property (nonatomic, strong) NSString * feed_id;

@property (nonatomic, strong) NSString * video_id;

/**
 * 如果是从草稿箱发布帖子，则传草稿ID,如果不是，传0
 */
@property (nonatomic, strong) NSString * draft_id;

/**
 * 1为保存草稿，2为发布帖子
 */
@property (nonatomic, strong) NSString * is_draft;

/**
 * 是否已经传过阿里云
 */
@property (nonatomic, assign) BOOL  isUploadVideo;

/**
 * 1为发布流程，2为编辑草稿
 */
@property (nonatomic, assign) NSInteger editType;

@property (nonatomic, strong) FWFeedListModel * listModel;

@end
