//
//  FWPublishPreviewIdleViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/18.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWPublishPreviewIdleView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPublishPreviewIdleViewController : FWBaseViewController<FWPublishPreviewIdleViewDelegate>

@property (nonatomic, strong) FWPublishPreviewIdleView * previewView;

@property (nonatomic, strong) NSMutableArray * tagsArray;

@property (nonatomic, strong) NSMutableArray * imagesArray;

@property (nonatomic, strong) NSMutableArray * photos;

@property (nonatomic, strong) UIButton * nextButton;

/**
 * 存储获取的上传资源的文件名（bucket，object-key）
 */
@property (nonatomic, strong) NSMutableDictionary * objectDict;

@property (nonatomic, strong) NSString * feed_id;

@property (nonatomic, strong) NSString * video_id;

/**
 * 如果是从草稿箱发布帖子，则传草稿ID,如果不是，传0
 */
@property (nonatomic, strong) NSString * draft_id;

/**
 * 1为保存草稿，2为发布帖子
 */
@property (nonatomic, strong) NSString * is_draft;

/**
 * 是否已经传过阿里云
 */
@property (nonatomic, assign) BOOL  isUploadVideo;

/**
 * 1为发布流程，2为编辑草稿
 */
@property (nonatomic, assign) NSInteger editType;


@property (nonatomic, strong) FWFeedListModel * listModel;


@end

NS_ASSUME_NONNULL_END
