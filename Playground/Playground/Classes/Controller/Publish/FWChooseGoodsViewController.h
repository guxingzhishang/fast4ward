//
//  FWChooseGoodsViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 选择商品
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^valueBlock)(NSString *goods_id);

@interface FWChooseGoodsViewController : FWBaseViewController

@property (nonatomic, strong) NSString * goods_id;

@property (nonatomic, copy) valueBlock myBlock;

@end

NS_ASSUME_NONNULL_END
