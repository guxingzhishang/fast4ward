//
//  FWThirdViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWPublishViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AliyunVideoRecordParam.h"
#import "AliyunVideoUIConfig.h"
#import "AliyunVideoBase.h"
#import "AliyunVideoCropParam.h"

#import "FWPublishPreviewViewController.h"
#import "TZImagePickerController.h"

#import "FWPublishPreviewGraphViewController.h"
#import "FWArticalViewController.h"

#import "CZHTool.h"
#import "CZHChooseCoverController.h"

#import "FWDraftViewController.h"
#import "FWPublishPreviewAskViewController.h"
#import "FWPublishPreviewRefitViewController.h"
#import "FWPublishPreviewIdleViewController.h"

@interface FWPublishViewController ()<TZImagePickerControllerDelegate>

@property (nonatomic, assign) BOOL suspend;

@property (nonatomic, strong) AliyunVideoRecordParam *quVideo;

@property (nonatomic, assign) CGFloat videoOutputWidth;
@property (nonatomic, assign) CGFloat videoOutputRatio;
@property (assign, nonatomic) BOOL isUnActive;
@property (assign, nonatomic) BOOL isPhotoToRecord;
@property (assign, nonatomic) NSInteger maxTime;

@end

@implementation FWPublishViewController
@synthesize cameraView;
@synthesize mainView;

- (void)setScreenshotImage:(UIImage *)screenshotImage{
    _screenshotImage = screenshotImage;
    
    cameraView.mainView.image = screenshotImage;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"发布选择类型页"];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.navigationController.navigationBar.hidden = YES;
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"发布选择类型页"];

    self.navigationController.navigationItem.title = @"测试";
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.fd_prefersNavigationBarHidden = YES;

    cameraView = [[FWPublishCameraView alloc] init];
    cameraView.vc = self;
    cameraView.delegate = self;
    [self.view addSubview:cameraView];
    cameraView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
//    [cameraView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(self.view);
//    }];
    
    [self setupSDKBaseVersionUI];
    
    //阿里云设置
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    
    _quVideo = [[AliyunVideoRecordParam alloc] init];
    _quVideo.ratio = AliyunVideoVideoRatio3To4;
    _quVideo.size = AliyunVideoVideoSize540P;
    _quVideo.minDuration = 3;

    _maxTime = [[GFStaticData getObjectForKey:kTagUserMaxVideoDuration] integerValue]?[[GFStaticData getObjectForKey:kTagUserMaxVideoDuration] integerValue]:60;
    _quVideo.maxDuration = _maxTime;
    _quVideo.position = AliyunCameraPositionBack;
    _quVideo.beautifyStatus = YES;
    _quVideo.beautifyValue = 100;
    _quVideo.torchMode = AliyunCameraTorchModeOff;
    _quVideo.outputPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/record_save.mp4"];

    self.videoOutputRatio = 0.75;//0.5625;
    self.videoOutputWidth = 540;
}

#pragma mark - > 提问
- (void)askButtonClick{

     FWPublishPreviewAskViewController * PVC = [[FWPublishPreviewAskViewController alloc] init];
     PVC.draft_id = @"0";
     PVC.editType = 1;
     
     CATransition *animation1 = [CATransition animation];
     [animation1 setDuration:0.4];
     [animation1 setType: kCATransitionMoveIn];
     [animation1 setSubtype: kCATransitionFromTop];
     [animation1 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
     [self.navigationController pushViewController:PVC animated:NO];
     [self.navigationController.view.layer addAnimation:animation1 forKey:nil];
}

#pragma mark - > 改装分享
- (void)refitButtonClick{
    
    FWPublishPreviewRefitViewController * PVC = [[FWPublishPreviewRefitViewController alloc] init];
    PVC.draft_id = @"0";
    PVC.editType = 1;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.4];
    [animation setType: kCATransitionMoveIn];
    [animation setSubtype: kCATransitionFromTop];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    [self.navigationController pushViewController:PVC animated:NO];
    [self.navigationController.view.layer addAnimation:animation forKey:nil];
}

#pragma mark - > 草稿箱
- (void)draftButtonClick{
    [self.navigationController pushViewController:[FWDraftViewController new] animated:YES];
}

#pragma mark - > 拍视频
- (void)videoButtonClick{
    
    if (self.videoOutputRatio == 0.5625) {
        _quVideo.ratio = AliyunVideoVideoRatio9To16;
    }else if (self.videoOutputRatio == 0.75) {
        _quVideo.ratio = AliyunVideoVideoRatio3To4;
    } else {
        _quVideo.ratio = AliyunVideoVideoRatio1To1;
    }

    if (_quVideo.maxDuration == 0) {
        _quVideo.maxDuration = 30;
    }
    if (_quVideo.minDuration == 0) {
        _quVideo.minDuration = 3;
    }
    if (_quVideo.maxDuration <= _quVideo.minDuration) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"最大时长不得小于最小时长" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }

    UIViewController *recordViewController = [[AliyunVideoBase shared] createRecordViewControllerWithRecordParam:_quVideo];
    [AliyunVideoBase shared].delegate = (id)self;
    [self.navigationController pushViewController:recordViewController animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

#pragma mark - > 照相
- (void)photographButtonClick{
        
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowTakePicture = YES;
    
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {

        FWPublishPreviewGraphViewController * PVC = [[FWPublishPreviewGraphViewController alloc] init];
        PVC.photos = photos.mutableCopy;
        PVC.draft_id = @"0";
        PVC.editType = 1;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.4];
        [animation setType: kCATransitionMoveIn];
        [animation setSubtype: kCATransitionFromTop];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        [self.navigationController pushViewController:PVC animated:NO];
        [self.navigationController.view.layer addAnimation:animation forKey:nil];
    }];

    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

#pragma mark - > 文章
- (void)articalButtonClick{
    
    [self.navigationController pushViewController:[FWArticalViewController new] animated:YES];
}

#pragma mark - > 发布闲置
- (void)idleButtonClick{
            
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowTakePicture = YES;
    
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {

        FWPublishPreviewIdleViewController * PVC = [[FWPublishPreviewIdleViewController alloc] init];
        PVC.photos = photos.mutableCopy;
        PVC.draft_id = @"0";
        PVC.editType = 1;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.4];
        [animation setType: kCATransitionMoveIn];
        [animation setSubtype: kCATransitionFromTop];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        [self.navigationController pushViewController:PVC animated:NO];
        [self.navigationController.view.layer addAnimation:animation forKey:nil];
    }];

    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)setupSDKBaseVersionUI {
    AliyunVideoUIConfig *config = [[AliyunVideoUIConfig alloc] init];

    config.backgroundColor = RGBToColor(35, 42, 66);
    config.timelineBackgroundCollor = RGBToColor(35, 42, 66);
    config.timelineDeleteColor = [UIColor redColor];
    config.timelineTintColor = RGBToColor(239, 75, 129);
    config.durationLabelTextColor = [UIColor redColor];
    config.cutTopLineColor = [UIColor redColor];
    config.cutBottomLineColor = [UIColor redColor];
    config.noneFilterText = @"无滤镜";
    config.hiddenDurationLabel = NO;
    config.hiddenFlashButton = NO;
    config.hiddenBeautyButton = NO;
    config.hiddenCameraButton = NO;
    config.hiddenImportButton = NO;
    config.hiddenDeleteButton = NO;
    config.hiddenFinishButton = NO;
//    config.hiddenRatioButton = YES;
    config.recordOnePart = NO;
    config.filterArray = @[@"炽黄",@"粉桃",@"海蓝",@"红润",@"灰白",@"经典",@"麦茶",@"浓烈",@"柔柔",@"闪耀",@"鲜果",@"雪梨",@"阳光",@"优雅",@"朝阳",@"波普",@"光圈",@"海盐",@"黑白",@"胶片",@"焦黄",@"蓝调",@"迷糊",@"思念",@"素描",@"鱼眼",@"马赛克",@"模糊"];
    config.imageBundleName = @"QPSDK";
    config.filterBundleName = @"FilterResource";
    config.recordType = AliyunVideoRecordTypeCombination;
    config.showCameraButton = YES;

    [[AliyunVideoBase shared] registerWithAliyunIConfig:config];
}

- (void)appWillResignActive:(id)sender{
    self.isUnActive = YES;
}
- (void)appDidBecomeActive:(id)sender{
    self.isUnActive = NO;
}

#pragma mark - AliyunVideoBaseDelegate
-(void)videoBaseRecordVideoExit {
    NSLog(@"退出录制");
    [GFStaticData saveObject:@"YES" forKey:@"ALI"];
    [self.navigationController popViewControllerAnimated:YES];

    self.navigationController.navigationBar.hidden = YES;
}

- (void)videoBase:(AliyunVideoBase *)base recordCompeleteWithRecordViewController:(UIViewController *)recordVC videoPath:(NSString *)videoPath {
    NSLog(@"录制完成  %@", videoPath);
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:videoPath]
                                completionBlock:^(NSURL *assetURL, NSError *error) {

                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [recordVC.navigationController popViewControllerAnimated:NO];

                                        
                                        if (assetURL.absoluteString.length>0) {
                                            
                                            //计算相册视频时长
                                            NSDictionary *videoDic = [CZHTool getLocalVideoSizeAndTimeWithSourcePath:assetURL.absoluteString];
                                            
                                            int videoTime = [[videoDic valueForKey:@"duration"] intValue];
                                            
                                            NSUInteger limitTime = 30;
                                            if (videoTime > limitTime) {
                                                return;
                                            }
                                            
                                            CZHChooseCoverController *chooseCover = [[CZHChooseCoverController alloc] init];
                                            chooseCover.isEdit = NO;
                                            chooseCover.videoPath = assetURL;
                                            chooseCover.filePath = videoPath;
                                            [self.navigationController pushViewController:chooseCover animated:YES];
                                        }
                                        
//                                        FWPublishPreviewViewController * PVC = [FWPublishPreviewViewController new];
//                                        PVC.filePath = videoPath;
//                                        PVC.pathURL = assetURL;
//                                        PVC.draft_id = @"0";
//                                        PVC.editType = 1;
//                                        [self.navigationController pushViewController:PVC animated:YES];
                                    });
                                }];
}

- (AliyunVideoCropParam *)videoBaseRecordViewShowLibrary:(UIViewController *)recordVC {

    [UIApplication sharedApplication].keyWindow.rootViewController.navigationController.navigationBar.hidden = YES;
    NSLog(@"录制页跳转Library");
    // 可以更新相册页配置
    AliyunVideoCropParam *mediaInfo = [[AliyunVideoCropParam alloc] init];
    mediaInfo.minDuration = 3.0;
    mediaInfo.maxDuration = _maxTime;
    mediaInfo.fps = _quVideo.fps;
    mediaInfo.gop = _quVideo.gop;
    mediaInfo.videoQuality = _quVideo.videoQuality;
    mediaInfo.size = _quVideo.size;
    mediaInfo.ratio = _quVideo.ratio;
    mediaInfo.cutMode = AliyunVideoCutModeScaleAspectFill;
    mediaInfo.videoOnly = YES;
    mediaInfo.outputPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/cut_save.mp4"];
    return mediaInfo;

}

// 裁剪
- (void)videoBase:(AliyunVideoBase *)base cutCompeleteWithCropViewController:(UIViewController *)cropVC videoPath:(NSString *)videoPath {

    NSLog(@"裁剪完成  %@", videoPath);
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:videoPath]
                                completionBlock:^(NSURL *assetURL, NSError *error) {

                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [cropVC.navigationController popViewControllerAnimated:NO];

                                        NSURL *url = assetURL;

                                        if (url.absoluteString.length>0) {
                                            
                                            //计算相册视频时长
                                            NSDictionary *videoDic = [CZHTool getLocalVideoSizeAndTimeWithSourcePath:url.absoluteString];
                                            
                                            int videoTime = [[videoDic valueForKey:@"duration"] intValue];
                                            
                                            NSUInteger limitTime = 30;
                                            if (videoTime > limitTime) {
                                                return;
                                            }
                                            
                                            CZHChooseCoverController *chooseCover = [[CZHChooseCoverController alloc] init];
                                            chooseCover.isEdit = NO;
                                            chooseCover.videoPath = url;
//                                            chooseCover.videoPath = [NSURL URLWithString:videoPath];
                                            chooseCover.filePath = videoPath;
                                            [self.navigationController pushViewController:chooseCover animated:YES];
                                        }
                                    });
                                }];
}



- (AliyunVideoRecordParam *)videoBasePhotoViewShowRecord:(UIViewController *)photoVC {

    NSLog(@"跳转录制页");
    return nil;
}

- (void)videoBasePhotoExitWithPhotoViewController:(UIViewController *)photoVC {

    NSLog(@"退出相册页");
    [photoVC.navigationController popViewControllerAnimated:YES];
}


@end
