//
//  FWArticalLinkGoodsViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/11.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWArticalLinkGoodsViewController.h"
#import "FWChooseGoodsViewController.h"

@interface FWArticalLinkGoodsViewController ()
@property (nonatomic, strong) NSString * tempGoods_id;

@end

@implementation FWArticalLinkGoodsViewController


#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"文章关联商品页"];

    self.view.backgroundColor =  FWTextColor_F6F8FA;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"文章关联商品页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tempGoods_id = @"0";
    self.title = @"关联商品";
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.upView = [UIView new];
    self.upView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.upView];
    self.upView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 290);
    
    self.bgImageView = [[UIImageView alloc] init];
    self.bgImageView.frame = CGRectMake(4, 0, SCREEN_WIDTH-8, 265);
    self.bgImageView.image = [UIImage imageNamed:@"home_bg"];
    [self.upView addSubview:self.bgImageView];
    
    self.picImageView = [[UIImageView alloc] init];
    self.picImageView.frame = CGRectMake(10, 0, CGRectGetWidth(self.bgImageView.frame)-20, 195);
    self.picImageView.image = [UIImage imageNamed:@"placeholder"];
    self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.picImageView.clipsToBounds = YES;
    [self.bgImageView addSubview:self.picImageView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.titleLabel.textColor = FWTextColor_272727;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgImageView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.picImageView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.picImageView).mas_offset(10);
        make.right.mas_equalTo(self.picImageView).mas_offset(-10);
        make.width.mas_equalTo(CGRectGetWidth(self.picImageView.frame)-20);
        make.height.mas_greaterThanOrEqualTo(20);
    }];
    
    self.downView = [UIView new];
    self.downView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.downView];
    self.downView.frame = CGRectMake(0, CGRectGetMaxY(self.upView.frame)+10, SCREEN_WIDTH, 42);
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"关联商品";
    self.tipLabel.frame = CGRectMake(25, 0, 100, 42);
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.tipLabel.textColor = FWTextColor_272727;
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.downView addSubview:self.tipLabel];
    
    self.linkGoodsButton = [[UIButton alloc] init];
    [self.linkGoodsButton addTarget:self action:@selector(linkGoodsButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.downView addSubview:self.linkGoodsButton];
    [self.linkGoodsButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.downView);
    }];
    
    self.linkArrowView = [[UIImageView alloc] init];
    self.linkArrowView.image = [UIImage imageNamed:@"right_arrow"];
    [self.linkGoodsButton addSubview:self.linkArrowView];
    [self.linkArrowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.linkGoodsButton);
        make.right.mas_equalTo(self.linkGoodsButton).mas_offset(-20);
        make.size.mas_equalTo(CGSizeMake(6, 11));
    }];
    
    self.hasLinkLabel = [[UILabel alloc] init];
    self.hasLinkLabel.textAlignment = NSTextAlignmentRight;
    self.hasLinkLabel.text = @"已关联";
    self.hasLinkLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    self.hasLinkLabel.textColor = FWTextColor_67769E;
    [self.linkGoodsButton addSubview:self.hasLinkLabel];
    [self.hasLinkLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.linkArrowView.mas_left).mas_offset(-10);
        make.centerY.mas_equalTo(self.linkGoodsButton);
        make.size.mas_equalTo(CGSizeMake(100, 40));
    }];
    self.hasLinkLabel.hidden = YES;
    
    
    self.saveButton = [[UIButton alloc] init];
    self.saveButton.layer.cornerRadius = 2;
    self.saveButton.layer.masksToBounds = YES;
    self.saveButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.saveButton.backgroundColor = FWTextColor_222222;
    [self.saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [self.saveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.saveButton addTarget:self action:@selector(saveButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveButton];
    [self.saveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-56, 48));
        make.centerX.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-20-FWSafeBottom);
    }];
    
    [self configForView];
}

#pragma mark - > 返回
- (void)backBtnClick{
    
    if (!self.listModel.goods_info.goods_id) {
        self.listModel.goods_info.goods_id = @"0";
    }
    
    if ([self.listModel.goods_info.goods_id isEqualToString:self.tempGoods_id]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"您有信息修改未保存，直接返回吗？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self saveButtonOnClick];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 关联商品
- (void)linkGoodsButtonClick{
    
    FWChooseGoodsViewController * CGVC = [[FWChooseGoodsViewController alloc] init];
    CGVC.goods_id = self.tempGoods_id ;
    CGVC.myBlock = ^(NSString * _Nonnull goods_id) {
        self.tempGoods_id = goods_id;
        if (goods_id.length > 0 && ![goods_id isEqualToString:@"0"]) {
            self.hasLinkLabel.hidden = NO;
        }else{
            self.hasLinkLabel.hidden = YES;
        }
    };
    [self.navigationController pushViewController:CGVC animated:YES];
}

#pragma mark - > 保存
- (void)saveButtonOnClick{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.listModel.feed_id,
                              @"goods_id":self.tempGoods_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_relate_goods WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [[FWHudManager sharedManager] showErrorMessage:@"关联成功" toController:self];

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)setListModel:(FWFeedListModel *)listModel{
    _listModel = listModel;
}

- (void)configForView{
    
    self.titleLabel.text = self.listModel.feed_title;
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.feed_cover]];
    
    if (self.listModel.goods_info.goods_id && ![self.listModel.goods_info.goods_id isEqualToString:@"0"]) {
        self.tempGoods_id = self.listModel.goods_info.goods_id;
        self.hasLinkLabel.hidden = NO;
    }else{
        self.tempGoods_id = @"0";
        self.hasLinkLabel.hidden = YES;
    }
}
@end
