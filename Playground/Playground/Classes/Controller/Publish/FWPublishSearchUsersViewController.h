//
//  FWPublishSearchUsersViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/4.
//  Copyright © 2020 孤星之殇. All rights reserved.
//
/**
 * 各种类型发布页 - > 提醒谁看
 */
#import "FWBaseViewController.h"
#import "FWSearchResultNavigationView.h"

NS_ASSUME_NONNULL_BEGIN

@class FWPublishSearchBarView;
@class FWSearchUserCell;

typedef void(^chooseUserBlock)(FWMineInfoModel * userInfo);

@interface FWPublishSearchUsersViewController : FWBaseViewController

@property (nonatomic, strong) FWPublishSearchBarView * navigationView;

@property (nonatomic, strong) NSString * query;

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, copy) chooseUserBlock chooseBlock;

@property (nonatomic, strong) NSMutableArray * atUserMutableArray;


@end

@protocol FWPublishSearchBarViewDelegate <NSObject>

- (void)backClick;
- (void)searchResultWithText:(NSString *)text;
- (void)selectClearButton;

@end

@interface FWPublishSearchBarView : UIView<UISearchBarDelegate>

@property (nonatomic, strong) UISearchBar * resultSearchBar;

@property (nonatomic, strong) UIButton * backButton;

@property (nonatomic, weak) id<FWPublishSearchBarViewDelegate>delegate ;


@end


@interface FWPublishSearchUserCell : UITableViewCell

@property (nonatomic, strong) FWMineInfoModel * userModel;

@property (nonatomic, strong) UIImageView * photoImageView;
@property (nonatomic, strong) UILabel * nameLabel;

- (void)configForCell:(id)model;

@end
NS_ASSUME_NONNULL_END
