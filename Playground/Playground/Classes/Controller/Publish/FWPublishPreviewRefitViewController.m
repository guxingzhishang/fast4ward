//
//  FWPublishPreviewRefitViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/10.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWPublishPreviewRefitViewController.h"

#import "FWSearchTagsViewController.h"
#import "FWPublishRequest.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import "CZHTool.h"
#import "Header.h"
#import "CZHChooseCoverController.h"

#import "FWUpdateInfomationRequest.h"

#import "FWUploadManager.h"
#import "FWChooseBradeViewController.h"

#import "FWRefitCategaryModel.h"

@interface FWPublishPreviewRefitViewController ()

@property (nonatomic, assign) BOOL isUpload;
@property (nonatomic, strong) NSString * carType;
@property (nonatomic, strong) FWRefitCategaryModel * categaryModel;

@end

@implementation FWPublishPreviewRefitViewController
@synthesize previewView;
@synthesize tagsArray;
@synthesize feed_id;
@synthesize isUploadVideo;
@synthesize nextButton;

#pragma mark - > 请求改装参数
- (void)requestGaizhuangCategary{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_gaizhuang_category WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.categaryModel = [FWRefitCategaryModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            self.previewView.categaryModel = self.categaryModel;
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)setPhotos:(NSMutableArray *)photos{
    _photos = photos;
}

#pragma mark - > 请求阿里云临时凭证
- (void)requsetStsData{
    FWPublishRequest * requst = [[FWPublishRequest alloc] init];
    [requst requestStsTokenWithType:@"1"];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [GFStaticData saveObject:nil forKey:ChooseCarTypeBackToPublish];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"改装发布页"];

    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [IQKeyboardManager sharedManager].enable = YES;
    });
    
    
    NSString * tempString = [GFStaticData getObjectForKey:ChooseCarTypeBackToPublish];
    
    if (tempString && tempString.length > 0) {
        self.carType = tempString;
        [self.previewView setupCarTypeView:tempString];
    }
    
    if (self.subListModel.car_style_id.length > 0) {
        self.previewView.carLabel.text = self.subListModel.style;
        self.previewView.toChooseLabel.text = @"";
    }else{
        self.previewView.toChooseLabel.text = @"去选择";
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"改装发布页"];

    [IQKeyboardManager sharedManager].enable = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.title = @"改装发布";
    self.fd_interactivePopDisabled = YES;

    self.imagesArray = @[].mutableCopy;
    tagsArray = @[].mutableCopy;
    self.objectDict = @{}.mutableCopy;

    /* 获取临时凭证 */
    [self requsetStsData];
    
    [self setupSubViews];
    
    if (self.editType == 1) {
        [self requestGaizhuangCategary];
    }
}

- (void)bindViewModel{
    [super bindViewModel];
}

#pragma mark - > 视图初始化
- (void)setupSubViews{
    
    previewView = [[FWPublishPreviewRefitView alloc] init];
    previewView.publishDelegate = self;
    previewView.vc = self;
    [self.view addSubview:previewView];
    [previewView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    if (self.editType == 2) {

        [previewView.deleteButton setTitle:@"删除草稿" forState:UIControlStateNormal];

        [tagsArray addObjectsFromArray:self.listModel.tags];
        
        NSMutableArray * array = [[GFStaticData getObjectForKey:self.feed_id] mutableCopy];
       
        NSString *path  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;

        NSMutableArray<UIImage *> * imageTempArr = @[].mutableCopy;
        for (NSString * imageName in array) {
            
            NSString * imagePath = [path stringByAppendingFormat:@"%@",imageName];

            UIImage * getImage =[[UIImage alloc]initWithContentsOfFile:imagePath];
            [imageTempArr addObject:getImage];
        }
        [self.imagesArray addObjectsFromArray:imageTempArr];
        
        previewView.images = imageTempArr;
        
        [previewView configForView:self.listModel];
        
        if (self.listModel.gaizhuang_list.length > 0) {
            NSData *jsonData = [self.listModel.gaizhuang_list dataUsingEncoding:NSUTF8StringEncoding];
            NSError *err;
            NSArray *arr = [NSJSONSerialization JSONObjectWithData: jsonData options:NSJSONReadingMutableContainers error:&err];
            
            NSDictionary * dict = @{@"list_gaizhuang_category":arr};
            self.categaryModel = [FWRefitCategaryModel mj_objectWithKeyValues:dict];
        }

        /* 改装清单 */
        for(int i = 0; i< self.categaryModel.list_gaizhuang_category.count;i++){
            
            /* 第一层数据 */
            FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[i];
            for (int j = 0; j <firstModel.sub.count; j++) {
            
                /* 第二层数据 */
                FWRefitCategarySubListModel * secondModel = firstModel.sub[j];
                
                if (secondModel.val.length > 0) {
                    firstModel.firstLevelType = 1;
                    
                    if ([secondModel.val containsString:@"原厂"]) {
                        secondModel.secondLevelType = 1;
                        secondModel.tempName = [NSString stringWithFormat:@"%@-原厂",secondModel.name];
                    }
                }else{
                    for (int k = 0; k < secondModel.list.count; k++) {
                        /* 有编辑 */
                        FWRefitCategarySubDetailModel * detailModel = secondModel.list[k];
                        
                        if (detailModel.val.length > 0) {
                            
                            firstModel.firstLevelType = 1;
                            secondModel.secondLevelType = 2;
                            
                            NSString * pinpai = @"";
                            NSString * xinghao = @"";
                            NSString * guige = @"";
                            NSString * feiyong = @"";
                            
                            for (int k = 0;k < secondModel.list.count; k++) {
                                if ([secondModel.list[k].name isEqualToString:@"品牌"]) {
                                    pinpai = secondModel.list[k].val;
                                }else  if ([secondModel.list[k].name isEqualToString:@"型号"]) {
                                     xinghao = secondModel.list[k].val;
                                 }else  if ([secondModel.list[k].name isEqualToString:@"规格"]) {
                                     guige = secondModel.list[k].val;
                                 }else  if ([secondModel.list[k].name isEqualToString:@"费用"]) {
                                     feiyong = secondModel.list[k].val;
                                 }
                            }
                            
                            if (pinpai.length > 0) {
                                pinpai = [NSString stringWithFormat:@"-%@",pinpai];
                            }
                            
                            if (xinghao.length > 0) {
                                xinghao = [NSString stringWithFormat:@"-%@",xinghao];
                            }
                            
                            if (guige.length > 0) {
                                guige = [NSString stringWithFormat:@"-%@",guige];
                            }
                            
                            if (feiyong.length > 0) {
                                feiyong = [NSString stringWithFormat:@"-%@",feiyong];
                            }
                            NSString * moduleString = [NSString stringWithFormat:@"%@%@%@%@",pinpai,xinghao,guige,feiyong];
                            if ([[moduleString substringToIndex:1] isEqualToString:@"-"]) {
                                /* 如果第一个是- ，先给去掉 */
                                moduleString = [moduleString substringFromIndex:1];
                            }

                            secondModel.tempName = [NSString stringWithFormat:@"%@\n%@",secondModel.name,moduleString];
                        }
                    }
                }
            }
        }
        
        self.previewView.categaryModel = self.categaryModel;
        
        if (self.listModel.car_type &&
            self.listModel.car_type.length > 0) {
            self.carType = self.listModel.car_type;
        }else{
            self.carType = @"";
        }
    }else{
        previewView.images = self.photos;
        
        [previewView configForView:nil];
        
        for (UIViewController * item in self.navigationController.viewControllers) {
            if ([item isKindOfClass:[FWTagsViewController class]]) {
                
                NSDictionary *dict = [GFStaticData getObjectForKey:CurrentTagsDictionary];
                
                FWSearchTagsSubListModel * tagModel = [[FWSearchTagsSubListModel alloc] init];
                tagModel.tag_name = dict[@"tag_name"];
                tagModel.tag_id = dict[@"tag_id"];
                
                [tagsArray addObject:tagModel];
                
                [previewView setupTagsView:tagsArray];
            }
        }
    }
}

#pragma mark - > 是否删除草稿
- (void)deleteButtonClick:(UIButton *)sender {
    
    self.nextButton = sender;
    self.nextButton.enabled = NO;
    
    // 防止重复点击
    [self performSelector:@selector(changeButtonStatus) withObject:nil afterDelay:3];
    
    if (self.editType == 2) {
        
        // 删除草稿，直接请求接口
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除草稿" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self requestDeleteDraft];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        // 保存草稿
        self.is_draft = @"1";
        /* 获取上传资源的文件名 */
        [self requestUploadName:@"2"];
    }
}

#pragma mark - > 删除草稿
- (void)requestDeleteDraft{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.draft_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_delete_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"删除成功" toController:self];
            
            NSMutableArray * array = [[GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]] mutableCopy];
            
            if ([array containsObject:self.draft_id]) {
                [array removeObject:self.draft_id];
            }

            [GFStaticData saveObject:array forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];

            [self popToPreViewController];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 删除标签
- (void)deleteTagsClick:(NSInteger)index{
    
    if (index == 0) {
        if (tagsArray.count > 0) {
            [tagsArray removeObjectAtIndex:index];
        }
        
        [previewView setupTagsView:tagsArray];
    }else{
        [GFStaticData saveObject:nil forKey:ChooseCarTypeBackToPublish];
        [previewView setupCarTypeView:@""];
    }
}

#pragma mark - > 选择车系
- (void)carTypeButtonClick{
    
    FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
    CBVC.chooseFrom = @"publish";
    [self.navigationController pushViewController:CBVC animated:YES];
}

#pragma mark - > 选择标签
- (void)tagsButtonClick {
    
    FWSearchTagsViewController * STVC = [[FWSearchTagsViewController alloc] init];
    STVC.myBlock = ^(FWSearchTagsSubListModel * tagModel) {
        
        if (tagsArray.count >0 ) {
            [tagsArray removeAllObjects];
        }
        
        [tagsArray addObject:tagModel];
        [previewView setupTagsView:tagsArray];
    };
    [self.navigationController pushViewController:STVC animated:YES];
}

#pragma mark - > 发布
- (void)publishClick:(UIButton *)sender{
    
    self.nextButton = sender;
    sender.enabled = NO;
    
    // 防止重复点击
    [self performSelector:@selector(changeButtonStatus) withObject:nil afterDelay:3];
    
    self.is_draft = @"2";
    [self.view endEditing:YES];
    
    /* 获取上传资源的文件名 */
    [self requestUploadName:@"1"];
}


- (void)changeButtonStatus{
    
    self.nextButton.enabled = YES;
}

#pragma mark - > 返回
- (void)backBtnClick{
    
    self.is_draft = @"1";
    [self.view endEditing:YES];

    if ([[previewView.addImageView getImagesArray] count] <= 0) {
        [self popToPreViewController];
        return;
    }
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否保存到草稿箱" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"不保存" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

        if (self.editType == 1) {
            for (UIViewController * item in self.navigationController.viewControllers) {
                if ([item isKindOfClass:[FWTagsViewController class]]) {
                    [self.navigationController popToViewController:item animated:YES];
                    return ;
                }
            }
            [self popToPreViewController];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        /* 获取上传资源的文件名 */
        [self requestUploadName:@"2"];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName:(NSString *)type{
    
    self.imagesArray = [previewView.addImageView getImagesArray];
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"image_feed",
                              @"number":@(self.imagesArray.count).stringValue,
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.objectDict = [back objectForKey:@"data"];
            
            [self requestCheckWithType:type];
        }
    }];
}


#pragma mark - > 预校验
- (void)requestCheckWithType:(NSString *)type{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSMutableDictionary * params = [[self getParams] mutableCopy];
    [params setObject:@"1" forKey:@"flag_check"];
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params.copy WithAction:Submit_add_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            // 发布
            [self uploadFile:type];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

// 上传文件（保存/发布）
- (void)uploadFile:(NSString *)type{
    
    FWUploadManager * defaultManager  = [FWUploadManager sharedRunLoopWorkDistribution];
    defaultManager.objectDict = self.objectDict;
    defaultManager.params = [[self getParams] mutableCopy];
    defaultManager.imagesArray = self.imagesArray;
    [defaultManager requestUploadFengmianImageWithType:@"2"];
   
    [self popToPreViewController];
}

- (NSDictionary *)getParams{
    
    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (FWSearchTagsSubListModel *  model in tagsArray) {
        [tempArr addObject:model.tag_id];
    }
    NSString * tags = [tempArr mj_JSONString];
    
    NSMutableArray * tempImageArr = @[].mutableCopy;
    
    
    for (int i =0; i < self.imagesArray.count ; i++) {
        UIImage * image = self.imagesArray[i];
        
        CGFloat fixelW = CGImageGetWidth(image.CGImage);
        CGFloat fixelH = CGImageGetHeight(image.CGImage);
        
        NSDictionary * dict = @{
                                @"wh":@"",
                                @"img_width":@(fixelW).stringValue,
                                @"img_height":@(fixelH).stringValue,
                                @"path":[self.objectDict objectForKey:@"object_keys"][i],
                                };
        [tempImageArr addObject:dict];
    }
    
    NSString * images = [tempImageArr mj_JSONString];
    
    if (self.draft_id.length <= 0) {
        self.draft_id = @"";
    }
    
    if ([previewView.tempGoods_id isEqualToString:@"0"] || !previewView.tempGoods_id) {
        previewView.tempGoods_id = @"0";
    }
    
    /* 改装简要 */
    NSArray * jianyaoArray = @[@"改装后马力（HP）",
                                @"改装后扭矩（NM）",
                                @"0-100km/h加速（S）",
                                @"改装总花费（￥）",
                                @"改装服务商",
                                @"获得荣誉（多条荣誉请换行）"];
    NSMutableArray * jianyaoTempArray = @[].mutableCopy;

    for ( int i =0; i<6; i++) {
        IQTextView * textView = [self.previewView.refitJianyaoView viewWithTag:333+i];
        self.previewView.jianyaoString = textView.text;

        NSDictionary * dict = @{
                                @"name":jianyaoArray[i],
                                @"val":self.previewView.jianyaoString,
                                };
        [jianyaoTempArray addObject:dict];
    }
    self.previewView.jianyaoString = [jianyaoTempArray mj_JSONString];

    /* 改装故事 */
    IQTextView * gushiTextView = [self.previewView.refitStoryView viewWithTag:555];
    self.previewView.gushiString = gushiTextView.text;


    /* 改装清单 */
    NSMutableArray * qingdanArray = @[].mutableCopy;
    for(int i = 0; i< self.categaryModel.list_gaizhuang_category.count;i++){
        
        /* 第一层数据 */
        NSMutableArray * subArray = @[].mutableCopy;
        FWRefitCategaryListModel * firstModel = self.categaryModel.list_gaizhuang_category[i];
        
        for (int j = 0; j <firstModel.sub.count; j++) {
        
            /* 第二层数据 */
            NSMutableArray * listArray = @[].mutableCopy;
            FWRefitCategarySubListModel * secondModel = firstModel.sub[j];
            
            if (secondModel.tempName.length > 0) {
                if ([secondModel.tempName containsString:@"原厂"]) {
                    /* 原厂 */
                    secondModel.val = @"原厂";
                    
                    for (int k = 0; k<secondModel.list.count; k++) {
                         
                        NSDictionary * thirdDict = @{
                            @"name":secondModel.list[k].name,
                            @"val":@"",
                            @"type":secondModel.list[k].type,
                         };
                        
                        [listArray addObject:thirdDict];
                    }
                }else {
                    /* 编辑了 */
                    secondModel.val = @"";
                    
                     for (int k = 0; k<secondModel.list.count; k++) {
                        
                         NSDictionary * thirdDict = @{
                             @"name":secondModel.list[k].name,
                             @"val":secondModel.list[k].val,
                             @"type":secondModel.list[k].type,
                          };
                         
                         [listArray addObject:thirdDict];
                     }
                }
            }else{
//                 改装清单没填写完 
//                [[FWHudManager sharedManager] showErrorMessage:@"请完善改装清单" toController:self];

                for (int k = 0; k<secondModel.list.count; k++) {
                   
                    NSDictionary * thirdDict = @{
                        @"name":secondModel.list[k].name,
                        @"val":secondModel.list[k].val,
                        @"type":secondModel.list[k].type,
                     };
                    
                    [listArray addObject:thirdDict];
                }
            }
            
            NSDictionary * secondDict = @{
                @"name":secondModel.name,
                @"type":secondModel.type,
                @"val":secondModel.val,
                @"list":listArray,
            };
            
            [subArray addObject:secondDict];
        }
        
        NSDictionary * firstDict = @{
            @"name":firstModel.name,
            @"sub":subArray,
        };
        
        [qingdanArray addObject:firstDict];
    }

    self.previewView.qingdanString = [qingdanArray mj_JSONString];
    

    if (self.subListModel.car_style_id.length <= 0) {
        self.subListModel.car_style_id = @"";
    }
    
    NSString * invite_uids = @"";
    NSMutableArray * uidTempArray = @[].mutableCopy;
    if (self.previewView.atUserMutableArray.count > 0) {
        for (FWMineInfoModel * userInfo in self.previewView.atUserMutableArray) {
            [uidTempArray addObject:userInfo.uid];
        }
        
        invite_uids = [uidTempArray mj_JSONString];
    }

    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"is_draft":self.is_draft,
                              @"draft_id":self.draft_id,
                              @"feed_type":@"5",
                              @"video_id":@"",
                              @"feed_id":@"",
                              @"video_name":@"",
                              @"video_cover":@"",
                              @"video_cover_wh":@"",
                              @"video_cover_width":@"",
                              @"video_cover_height":@"",
                              @"imgs":images,
                              @"car_style_id":self.subListModel.car_style_id,
                              @"feed_title":previewView.contentTextView.text,//
                              @"gaizhuang_content":previewView.gushiString ,//
                              @"gaizhuang_list":previewView.qingdanString,
                              @"gaizhuang_jianyao":previewView.jianyaoString,
                              @"tags":tags,
                              @"flag_check":@"2",
                              @"goods_id":previewView.tempGoods_id,
                              @"car_type":self.carType,
//                              @"invite_uids":invite_uids,
                              };
    
    return params;
}

#pragma mark - > 发布 or 存草稿箱 (1为保存草稿，2为发布帖子)
- (void)requestSaveDraft{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSDictionary * parmas = [self getParams];
    
    [request startWithParameters:parmas WithAction:Submit_add_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([self.is_draft isEqualToString:@"1"]) {
                
                [[FWHudManager sharedManager] showErrorMessage:@"保存成功" toController:self];
                
                // 保存草稿
                DHWeakSelf;
                
                dispatch_async(dispatch_queue_create(0,0), ^{
                    NSMutableArray * tempArr = @[].mutableCopy;
                    
                    for (int i =0;i < weakSelf.imagesArray.count;i++) {
                        
                        UIImage * image = weakSelf.imagesArray[i];
                        
                        //设置一个图片的存储路径
                        NSDate *date = [NSDate date];
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
                        NSString *dataString = [dateFormatter stringFromDate:date];
                        
                        //设置一个图片的存储路径
                        
                        NSString *path  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
                        
                        NSString * imageName = [NSString stringWithFormat:@"/%@_%d.png",dataString,i];
                        NSString * imagePath = [path stringByAppendingFormat:@"%@",imageName];
                        
                        /*
                         把图片直接保存到指定的路径（同时应该把图片的名称存起来，在取的时候，拼上前面的路径，因为每次路径是变的。）
                         */
                        [UIImagePNGRepresentation(image) writeToFile:imagePath atomically:YES];
                        [tempArr addObject:imageName];
                    }
                    
                    [GFStaticData saveObject:tempArr forKey:[[back objectForKey:@"data"] objectForKey:@"feed_id"]];
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
                        });
                    });
                });
            }else{
                [[FWHudManager sharedManager] showSuccessMessage:@"发布成功" toController:self];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)popToPreViewController{
    
    if ([self.draft_id isEqualToString:@"0"]) {
        /* 不是草稿，回首页 */
        __weak FWPublishPreviewRefitViewController * weakSelf = self;
        
        weakSelf.navigationController.tabBarController.selectedIndex = 0;
        [weakSelf.navigationController popToRootViewControllerAnimated:NO];
    }else{
        /* 草稿，回草稿箱 */
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


-(BOOL)prefersStatusBarHidden{

    return YES;//隐藏
//  return NO;//显示
}



@end
