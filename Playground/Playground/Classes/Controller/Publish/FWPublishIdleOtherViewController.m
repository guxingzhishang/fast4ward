//
//  FWPublishIdleOtherViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/20.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWPublishIdleOtherViewController.h"
#import "UIBarButtonItem+Item.h"
#import "FWChooseBradeViewController.h"
#import "FWSearchTagsViewController.h"
#import "FWIdleModel.h"

@interface FWPublishIdleOtherViewController ()
@property (nonatomic, strong) NSString * carType;
@property (nonatomic, strong) NSString * topic;
@property (nonatomic, strong) NSString * fenlei;
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * xianzhi_category_id;



@property (nonatomic, strong) FWIdleModel * idleModel;

@end

@implementation FWPublishIdleOtherViewController

- (void)requestData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_xianzhi_category WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.idleModel = [FWIdleModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            for (FWIdleListModel * listModel in self.idleModel.list) {
                [self.paixuArray addObject:listModel.category_name];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [GFStaticData saveObject:nil forKey:ChooseCarTypeBackToPublish];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSString * tempString = [GFStaticData getObjectForKey:ChooseCarTypeBackToPublish];
    
    if (tempString && tempString.length > 0) {
        self.carType = tempString;
        [self setupCarTypeView:self.carType];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"其他";
    [self requestData];
    
    self.paixuArray = @[].mutableCopy;
    
    [self setupSubviews];
    
}

- (void)setupSubviews{
    
    UIButton * saveButton = [[UIButton alloc] init];
    saveButton.frame = CGRectMake(0, 0, 45, 25);
    saveButton.layer.cornerRadius = 2;
    saveButton.layer.masksToBounds = YES;
    saveButton.backgroundColor = FWTextColor_222222;
    saveButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
    [saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [saveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];

    /* 所属分类 */
    self.fenleiView = [[UITextField alloc] init];
    self.fenleiView.userInteractionEnabled = YES;
    [self.view addSubview:self.fenleiView];
    [self.fenleiView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset(20);
        make.right.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view).mas_offset(-50);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH+50, 50));
    }];

    
    self.fenleiLabel = [[UILabel alloc] init];
    self.fenleiLabel.font = DHFont(14);
    self.fenleiLabel.text =@"所属分类";
    self.fenleiLabel.textColor = FWTextColor_222222;
    self.fenleiLabel.textAlignment = NSTextAlignmentLeft;
    [self.fenleiView addSubview:self.fenleiLabel];
    [self.fenleiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.fenleiView);
        make.width.mas_equalTo(100);
        make.left.mas_equalTo(self.fenleiView).mas_offset(64);
        make.height.mas_equalTo(self.fenleiView);
    }];
    
    self.fenleiArrowImageView = [[UIImageView alloc] init];
    self.fenleiArrowImageView.image = [UIImage imageNamed:@"right_arrow"];;
    [self.fenleiView addSubview:self.fenleiArrowImageView];
    [self.fenleiArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.right.mas_equalTo(self.fenleiView).mas_offset(-14);
         make.centerY.mas_equalTo(self.fenleiView);
         make.width.mas_equalTo(6);
         make.height.mas_equalTo(11);
    }];
    
    self.fenleiRightLabel = [[UILabel alloc] init];
    self.fenleiRightLabel.font = DHFont(12);
    self.fenleiRightLabel.text = @"去关联";
    self.fenleiRightLabel.textColor = FWColor(@"BCBCBC");
    self.fenleiRightLabel.textAlignment = NSTextAlignmentRight;
    [self.fenleiView addSubview:self.fenleiRightLabel];
    [self.fenleiRightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.fenleiArrowImageView.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(self.fenleiView);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(30);
    }];
    
    /* 适用车系 */
    self.carTypeView = [[UIView alloc] init];
    self.carTypeView.userInteractionEnabled = YES;
    [self.view addSubview:self.carTypeView];
    [self.carTypeView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.fenleiView.mas_bottom).mas_offset(0);
        make.left.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 50));
    }];
    [self.carTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(carTypeViewClick)]];

    self.carTypeLabel = [[UILabel alloc] init];
    self.carTypeLabel.font = DHFont(14);
    self.carTypeLabel.text =@"适用车系";
    self.carTypeLabel.textColor = FWTextColor_222222;
    self.carTypeLabel.textAlignment = NSTextAlignmentLeft;
    [self.carTypeView addSubview:self.carTypeLabel];
    [self.carTypeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.carTypeView);
        make.width.mas_equalTo(100);
        make.left.mas_equalTo(self.carTypeView).mas_offset(14);
        make.height.mas_equalTo(self.carTypeView);
    }];
    
    self.carTypeArrowImageView = [[UIImageView alloc] init];
    self.carTypeArrowImageView.image = [UIImage imageNamed:@"right_arrow"];;
    [self.carTypeView addSubview:self.carTypeArrowImageView];
    [self.carTypeArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.right.mas_equalTo(self.carTypeView).mas_offset(-14);
         make.centerY.mas_equalTo(self.carTypeView);
         make.width.mas_equalTo(6);
         make.height.mas_equalTo(11);
    }];
    
    self.carTypeRightLabel = [[UILabel alloc] init];
    self.carTypeRightLabel.font = DHFont(12);
    self.carTypeRightLabel.text = @"去关联";
    self.carTypeRightLabel.textColor = FWColor(@"BCBCBC");
    self.carTypeRightLabel.textAlignment = NSTextAlignmentRight;
    [self.carTypeView addSubview:self.carTypeRightLabel];
    [self.carTypeRightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.carTypeArrowImageView.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(self.carTypeView);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(30);
    }];
    
    /* 添加话题 */
    self.topicView = [[UIView alloc] init];
    [self.view addSubview:self.topicView];
    [self.topicView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.carTypeView.mas_bottom).mas_offset(0);
        make.left.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 50));
    }];
    [self.topicView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topicViewClick)]];

    self.topicLabel = [[UILabel alloc] init];
    self.topicLabel.font = DHFont(14);
    self.topicLabel.text =@"添加话题";
    self.topicLabel.textColor = FWTextColor_222222;
    self.topicLabel.textAlignment = NSTextAlignmentLeft;
    [self.topicView addSubview:self.topicLabel];
    [self.topicLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.topicView);
        make.width.mas_equalTo(100);
        make.left.mas_equalTo(self.topicView).mas_offset(14);
        make.height.mas_equalTo(self.topicView);
    }];
    
    self.topicArrowImageView = [[UIImageView alloc] init];
    self.topicArrowImageView.image = [UIImage imageNamed:@"right_arrow"];;
    [self.topicView addSubview:self.topicArrowImageView];
    [self.topicArrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
         make.right.mas_equalTo(self.topicView).mas_offset(-14);
         make.centerY.mas_equalTo(self.topicView);
         make.width.mas_equalTo(6);
         make.height.mas_equalTo(11);
    }];
    
    self.topicRightLabel = [[UILabel alloc] init];
    self.topicRightLabel.font = DHFont(12);
    self.topicRightLabel.text = @"去关联";
    self.topicRightLabel.textColor = FWColor(@"BCBCBC");
    self.topicRightLabel.textAlignment = NSTextAlignmentRight;
    [self.topicView addSubview:self.topicRightLabel];
    [self.topicRightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.topicArrowImageView.mas_left).mas_offset(-5);
        make.centerY.mas_equalTo(self.topicView);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(30);
    }];
    
    self.otherLabel = [[UILabel alloc] init];
    self.otherLabel.font = DHFont(14);
    self.otherLabel.text =@"其他（可多选）";
    self.otherLabel.textColor = FWTextColor_222222;
    self.otherLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.otherLabel];
    [self.otherLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topicView.mas_bottom).mas_offset(20);
        make.left.mas_equalTo(self.view).mas_offset(14);
        make.size.mas_equalTo(CGSizeMake(100, 50));
    }];
    
    self.allNewButton = [[UIButton alloc] init];
    self.allNewButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
    self.allNewButton.layer.borderWidth = 1;
    self.allNewButton.layer.cornerRadius = 2;
    self.allNewButton.layer.masksToBounds = YES;
    self.allNewButton.titleLabel.font = DHFont(12);
    [self.allNewButton setTitle:@"全新" forState:UIControlStateNormal];
    [self.allNewButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.allNewButton addTarget:self action:@selector(allNewButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.allNewButton];
    [self.allNewButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.otherLabel);
        make.size.mas_equalTo(CGSizeMake(45, 25));
        make.left.mas_equalTo(self.otherLabel.mas_right).mas_offset(20);
    }];
    
    self.zitiButton = [[UIButton alloc] init];
    self.zitiButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
    self.zitiButton.layer.borderWidth = 1;
    self.zitiButton.layer.cornerRadius = 2;
    self.zitiButton.layer.masksToBounds = YES;
    self.zitiButton.titleLabel.font = DHFont(12);
    [self.zitiButton setTitle:@"自提" forState:UIControlStateNormal];
    [self.zitiButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.zitiButton addTarget:self action:@selector(zitiButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.zitiButton];
    [self.zitiButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.otherLabel);
        make.size.mas_equalTo(CGSizeMake(45, 25));
        make.left.mas_equalTo(self.allNewButton.mas_right).mas_offset(20);
    }];
    
    
    self.zonghePickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220, SCREEN_WIDTH, 220)];
    self.zonghePickerView.delegate=self;
    self.zonghePickerView.dataSource=self;
    self.zonghePickerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.zonghePickerView.backgroundColor=[UIColor clearColor];
    
    UIToolbar *bar1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-220-44, SCREEN_WIDTH, 44)];
    
    UIBarButtonItem *doneButton1 = [UIBarButtonItem barButtonItemWithTitle:@"完成" target:self action:@selector(doneButtonOneClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelButton1 = [UIBarButtonItem barButtonItemWithTitle:@"取消" target:self action:@selector(cancelButtonOneClick) forControlEvents:UIControlEventTouchUpInside];
    
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:16], NSFontAttributeName,nil]  forState:UIControlStateNormal];
    
    UIBarButtonItem *fixItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixItem1.width = 20;//用来设置宽度的属性width
    UIBarButtonItem *flexItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];//自动把按钮推到最右边
    bar1.items = @[cancelButton1,flexItem1, fixItem1, doneButton1];
    
    self.fenleiView.inputView = self.zonghePickerView;
    self.fenleiView.inputAccessoryView = bar1;
    

    if (self.otherDict.allKeys.count > 0) {
        NSArray * keys = self.otherDict.allKeys;
        
        for (int i = 0; i<keys.count; i++) {
            if ([keys[i] isEqualToString:@"xianzhi_category_name"] &&
                [keys[i] length] > 0) {
                self.fenlei = self.otherDict[@"xianzhi_category_name"];
                self.xianzhi_category_id = self.otherDict[@"xianzhi_category_id"];
                [self setupFenleiView:self.fenlei];
            }else if ([keys[i] isEqualToString:@"car_type"] &&
                      [keys[i] length] > 0) {
                self.carType = self.otherDict[@"car_type"];
                [self setupCarTypeView:self.carType];
            }else if ([keys[i] isEqualToString:@"tags_show"] &&
                      [keys[i] length] > 0) {
                self.topic = self.otherDict[@"tags_show"];
                self.tag_id = self.otherDict[@"tags"];
                [self setupTagsView:self.topic];
            }else if ([keys[i] isEqualToString:@"if_ziti"] &&
                      [self.otherDict[@"if_ziti"] isEqualToString:@"1"]) {
                self.zitiButton.selected = YES;
                self.zitiButton.backgroundColor = FWTextColor_BCBCBC;
                self.zitiButton.layer.borderColor = FWClearColor.CGColor;
                [self.zitiButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
            }else if ([keys[i] isEqualToString:@"if_quanxin"] &&
                      [self.otherDict[@"if_quanxin"] isEqualToString:@"1"]) {
                self.allNewButton.selected = YES;
                self.allNewButton.backgroundColor = FWTextColor_BCBCBC;
                self.allNewButton.layer.borderColor = FWClearColor.CGColor;
                [self.allNewButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
            }
        }
    }
}

#pragma mark - > 全新
- (void)allNewButtonClick{
    
    self.allNewButton.selected = !self.allNewButton.selected;
    
    if (self.allNewButton.isSelected) {
        self.allNewButton.backgroundColor = FWTextColor_BCBCBC;
        self.allNewButton.layer.borderColor = FWClearColor.CGColor;
        [self.allNewButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    }else{
        self.allNewButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.allNewButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
        [self.allNewButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    }
}

#pragma mark - > 自提
- (void)zitiButtonClick{
    
    self.zitiButton.selected = !self.zitiButton.selected;
    
    if (self.zitiButton.isSelected) {
        self.zitiButton.backgroundColor = FWTextColor_BCBCBC;
        self.zitiButton.layer.borderColor = FWClearColor.CGColor;
        [self.zitiButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    }else{
        self.zitiButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.zitiButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
        [self.zitiButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    }
}

#pragma mark - > pickerview数据源及代理方法
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

#pragma mark 数据源  numberOfRowsInComponent
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.paixuArray.count;
}

#pragma delegate 显示信息方法
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.paixuArray objectAtIndex:row];
}

#pragma mark 选中行信息
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
}

#pragma mark 显示行高
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.00;
}

#pragma mark - pickerView 每列宽度
- (CGFloat)pickerView:(UIPickerView*)pickerView widthForComponent:(NSInteger)component {
    return SCREEN_WIDTH;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = (UILabel *)view;
    if (lbl == nil) {
        lbl = [[UILabel alloc]init];
        //在这里设置字体相关属性
        lbl.font = [UIFont systemFontOfSize:18];
        lbl.textColor = FWTextColor_000000;
        [lbl setTextAlignment:1];
        [lbl setBackgroundColor:[UIColor clearColor]];
    }
    //重新加载lbl的文字内容
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return lbl;
}

#pragma mark - > 选中所属分类
- (void)doneButtonOneClick{
    
    NSInteger onerow=[self.zonghePickerView selectedRowInComponent:0];

    if (self.paixuArray.count > onerow) {
        self.fenlei = self.paixuArray[onerow];
        [self setupFenleiView:self.fenlei];
        self.xianzhi_category_id = self.idleModel.list[onerow].category_id;
    }
    
    [self cancelButtonOneClick];
}

- (void)cancelButtonOneClick{
    [self.view endEditing:YES];
}

#pragma mark - > 隐藏
- (void)topViewClick{
    [self.view endEditing:YES];
}

#pragma mark - > 车系
- (void)carTypeViewClick{
    
    FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
    CBVC.chooseFrom = @"publish";
    [self.navigationController pushViewController:CBVC animated:YES];
}

#pragma mark - > 话题
- (void)topicViewClick{
    
    DHWeakSelf;
    
    FWSearchTagsViewController * STVC = [[FWSearchTagsViewController alloc] init];
    STVC.myBlock = ^(FWSearchTagsSubListModel * tagModel) {
        weakSelf.tag_id = tagModel.tag_id;
        weakSelf.topic = tagModel.tag_name;
        [weakSelf setupTagsView:weakSelf.topic];
    };
    [self.navigationController pushViewController:STVC animated:YES];
}

#pragma mark - > 保存
- (void)saveButtonClick{
    
    NSString * zitiString = @"";
    NSString * ifZiti = @"2";
    
    NSString * allNewString = @"";
    NSString * ifQuanxin = @"2";
    

    NSString * fenleiNameString = @"";
    NSString * fenleiString = @"";
    NSString * topicString = @"";
    NSString * carTypeString = @"";

    if (self.zitiButton.isSelected) {
        zitiString = @"自提";
        ifZiti = @"1";
    }
    

    if (self.allNewButton.isSelected) {
        allNewString = @"全新";
        ifQuanxin = @"1";
    }
    
    
    if ([self.fenleiRightLabel.text isEqualToString:@"去关联"]) {
        fenleiString = @"";
    }else{
        fenleiString = self.xianzhi_category_id;
        fenleiNameString = self.fenlei;
    }

    NSString * tags = self.tag_id?self.tag_id:@"";
    
    if ([self.topicRightLabel.text isEqualToString:@"去关联"]) {
        topicString = @"";
    }else{
        topicString = self.topic;
    }
    
    if ([self.carTypeRightLabel.text isEqualToString:@"去关联"]) {
        carTypeString = @"";
    }else{
        carTypeString = self.carType;
    }
    
    NSDictionary * dict = @{
        @"xianzhi_category_name":fenleiNameString,
        @"xianzhi_category_id":fenleiString,
        @"car_type":carTypeString,
        @"tags":tags,
        @"tags_show":topicString,
        @"ziti":zitiString,
        @"quanxin":allNewString,
        @"if_ziti":ifZiti,
        @"if_quanxin":ifQuanxin,
    };
    
    if (self.otherBlock) {
        self.otherBlock(dict);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 添加分类
- (void)setupFenleiView:(NSString *)fenlei{
    
    if (fenlei.length <= 0) {
        for (UIView * view in self.fenleiRightLabel.subviews) {
            [view removeFromSuperview];
        }
        return;
    }
    if(nil == self.fenleiRightLabel){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self refreshContainer:self.fenleiRightLabel WithText:fenlei];
        });
    }else{
        [self refreshContainer:self.fenleiRightLabel WithText:fenlei];
    }
}


#pragma mark - > 添加话题
- (void)setupTagsView:(NSString *)topic{
    
    if (topic.length <= 0) {
        for (UIView * view in self.topicRightLabel.subviews) {
            [view removeFromSuperview];
        }
        return;
    }
    if(nil == self.topicRightLabel){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self refreshContainer:self.topicRightLabel WithText:topic];
        });
    }else{
        [self refreshContainer:self.topicRightLabel WithText:topic];
    }
}

#pragma mark - > 选择车系后显示
- (void)setupCarTypeView:(NSString *)carType{
    
    if (carType.length <= 0) {
        for (UIView * view in self.carTypeRightLabel.subviews) {
            [view removeFromSuperview];
        }
        return;
    }
    
    if(nil == self.carTypeRightLabel){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self refreshContainer:self.carTypeRightLabel WithText:carType];
        });
    }else{
        [self refreshContainer:self.carTypeRightLabel WithText:carType];
    }
}

- (void) refreshContainer:(UILabel *)container WithText:(NSString *)text{
    
    if (nil == container) {
        return;
    }
    for (UIView * view in container.subviews) {
        [view removeFromSuperview];
    }

    if (text.length > 0 ) {

        container.text = @"";
        container.userInteractionEnabled = YES;
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat width = 0.0;
        CGFloat height = 28;

        NSMutableAttributedString * attribute = [[NSMutableAttributedString alloc] initWithString:text];
        CGSize  textLimitSize = CGSizeMake(MAXFLOAT, height);
        width = [YYTextLayout layoutWithContainerSize:textLimitSize text:attribute].textBoundingSize.width+20;

        UIView * bgView = [[UIView alloc] init];
        bgView.backgroundColor = FWTextColor_222222;
        bgView.userInteractionEnabled = YES;
        bgView.layer.cornerRadius = 2;
        bgView.layer.masksToBounds = YES;
        bgView.frame = CGRectMake(x,y, width+30, height);
        [container addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(container).mas_offset(-5);
            make.centerY.mas_equalTo(container);
            make.width.mas_equalTo( width+30);
            make.height.mas_equalTo(height);
        }];

        UILabel * titleLable = [[UILabel alloc] init];
        titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
        titleLable.textAlignment = NSTextAlignmentCenter;
        titleLable.text = [NSString stringWithFormat:@" %@",text];
        titleLable.textColor = FWViewBackgroundColor_FFFFFF;
        [bgView addSubview:titleLable];
        titleLable.frame = CGRectMake(5,0, width, height);

        UIButton * btn = [[UIButton alloc] init];
        if ([container isEqual:self.fenleiRightLabel]) {
            btn.tag = 10000;
        }else if ([container isEqual:self.carTypeRightLabel]) {
            btn.tag = 10001;
        }else{
            btn.tag = 10002;
        }

        [btn setImage:[UIImage imageNamed:@"delete_tag_new"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(deleteTagsOnClick:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:btn];
        btn.frame = CGRectMake(width+2,0, height, height);
    }else{
        container.text = @"去关联";
        container.textColor = FWColor(@"BCBCBC");
    }
}

#pragma mark - > 删除选择的标签
- (void)deleteTagsOnClick:(UIButton *)sender{
    
    NSInteger btnTag = sender.tag - 10000;

    if (btnTag == 0) {
        self.fenlei = @"";
        self.xianzhi_category_id = @"";
        
        [self setupFenleiView:self.fenlei];
        [self refreshContainer:self.fenleiRightLabel WithText:self.fenlei];
    }else if (btnTag == 1) {
        self.carType = @"";
        [GFStaticData saveObject:nil forKey:ChooseCarTypeBackToPublish];
        [self setupCarTypeView:self.carType];
        [self refreshContainer:self.carTypeRightLabel WithText:self.carType];
    }else{
        self.topic = @"";
        self.tag_id = @"";
        [self setupTagsView:self.topic];
        [self refreshContainer:self.topicRightLabel WithText:self.topic];
    }
}
@end
