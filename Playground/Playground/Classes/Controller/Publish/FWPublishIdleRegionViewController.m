//
//  FWPublishIdleRegionViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/21.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWPublishIdleRegionViewController.h"
#import "FWArearModel.h"

@interface FWPublishIdleRegionViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView * headerView;
@property (nonatomic, strong) UIView * topView;
@property (nonatomic, strong) UIView * selectView;

@property (nonatomic, strong) NSString * status;


@end

@implementation FWPublishIdleRegionViewController
@synthesize infoTableView;

- (void)requestProvice{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_province_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWRegionModel * regionModel = [FWRegionModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            if (self.dataSource.count > 0) {
                [self.dataSource removeAllObjects];
            }
            [self.dataSource addObjectsFromArray:regionModel.list];
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)requestCity{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
        @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
        @"p_name":self.provice,
    };
    
    [request startWithParameters:params WithAction:Get_city_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWRegionModel * regionModel = [FWRegionModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            if (self.dataSource.count > 0) {
                [self.dataSource removeAllObjects];
            }
            [self.dataSource addObjectsFromArray:regionModel.list];
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)requestCounty{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
        @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
        @"p_name":self.city,
    };
    
    [request startWithParameters:params WithAction:Get_county_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWRegionModel * regionModel = [FWRegionModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            if (self.dataSource.count > 0) {
                [self.dataSource removeAllObjects];
            }
            [self.dataSource addObjectsFromArray:regionModel.list];
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (NSMutableArray*)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.status = @"provice";
    
    [self setupSubViews];
    [self requestProvice];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 44;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];

    self.headerView = [[UIView alloc] init];
    
    self.topView = [[UIView alloc] init];
    [self.headerView addSubview:self.topView];
    
    if (self.isDeny == 1) {
        /* 已开启定位 */
        self.topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 44);
        
        UIView * lineView = [[UIView alloc] init];
        lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
        [self.topView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.topView);
            make.height.mas_equalTo(1);
            make.top.mas_equalTo(self.topView.mas_bottom);
        }];
        
        UIImageView * dingweiImageView = [[UIImageView alloc] init];
        dingweiImageView.image = [UIImage imageNamed:@""];
        [self.topView addSubview:dingweiImageView];
        [dingweiImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.topView);
            make.size.mas_equalTo(CGSizeMake(8, 12));
            make.left.mas_equalTo(self.topView).mas_offset(14);
        }];
        
        UILabel * currentLabel = [[UILabel alloc] init];
        currentLabel.text = @"当前定位";
        currentLabel.font = DHFont(12);
        currentLabel.textColor = FWTextColor_222222;
        currentLabel.textAlignment = NSTextAlignmentLeft;
        [self.topView addSubview:currentLabel];
        [currentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(dingweiImageView.mas_right).mas_offset(5);
            make.centerY.mas_equalTo(self.topView);
            make.size.mas_equalTo(CGSizeMake(60, 20));
        }];
        
        UILabel * currentValueLabel = [[UILabel alloc] init];
        currentValueLabel.text = self.currentRegion;
        currentValueLabel.font = DHBoldFont(14);
        currentValueLabel.textColor = FWTextColor_222222;
        currentValueLabel.textAlignment = NSTextAlignmentLeft;
        [self.topView addSubview:currentValueLabel];
        [currentValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(currentLabel.mas_right).mas_offset(10);
            make.centerY.mas_equalTo(self.topView);
            make.size.mas_equalTo(CGSizeMake(200, 20));
        }];
    }else{
        /* 没开启定位 */
        self.topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.01);
    }
    
    self.selectView = [[UIView alloc] init];
    [self.headerView addSubview:self.selectView];
    self.selectView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), SCREEN_WIDTH, 0.01);
    
    self.selectLabel = [[UILabel alloc] init];
    self.selectLabel.font = DHFont(12);
    self.selectLabel.textColor = FWTextColor_222222;
    self.selectLabel.textAlignment = NSTextAlignmentLeft;
    [self.selectView addSubview:self.selectLabel];
    [self.selectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.selectView).mas_offset(14);
        make.centerY.mas_equalTo(self.selectView);
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];
    
    self.selectValueLabel = [[UILabel alloc] init];
    self.selectValueLabel.font = DHBoldFont(14);
    self.selectValueLabel.textColor = FWTextColor_222222;
    self.selectValueLabel.textAlignment = NSTextAlignmentLeft;
    [self.selectView addSubview:self.selectValueLabel];
    [self.selectValueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.selectLabel.mas_right).mas_offset(0);
        make.centerY.mas_equalTo(self.selectView);
        make.size.mas_equalTo(CGSizeMake(300, 20));
    }];
    
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetMaxY(self.selectView.frame));
    self.infoTableView.tableHeaderView = self.headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"regionID";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if (indexPath.row < self.dataSource.count) {
        cell.textLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        cell.textLabel.textColor = FWTextColor_222222;
        cell.textLabel.text = ((FWRegionListModel *)self.dataSource[indexPath.row]).region_name;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.status isEqualToString:@"provice"]) {
        if (indexPath.row < self.dataSource.count) {
            
            if (self.isDeny == 1) {
                /* 已开启定位 */
                self.topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 44);
               
            }else{
                /* 没开启定位 */
                self.topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.01);
            }
            
            self.selectView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), SCREEN_WIDTH, 44);
            
            self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetMaxY(self.selectView.frame));
            self.infoTableView.tableHeaderView = self.headerView;

            self.status = @"city";
            self.selectLabel.text = @"已选：";
            self.selectValueLabel.text = ((FWRegionListModel *)self.dataSource[indexPath.row]).region_name;
            self.provice = self.selectValueLabel.text;
            
            [self requestCity];
            
        }
    }else if ([self.status isEqualToString:@"city"]){
        if (indexPath.row < self.dataSource.count) {
            self.city = ((FWRegionListModel *)self.dataSource[indexPath.row]).region_name;
            
            self.status = @"county";
            
            self.selectValueLabel.text = [NSString stringWithFormat:@"%@-%@",self.selectValueLabel.text,self.city];
            
            [self requestCounty];
        }
    }else if ([self.status isEqualToString:@"county"]){
        if (indexPath.row < self.dataSource.count) {
            self.county = ((FWRegionListModel *)self.dataSource[indexPath.row]).region_name;
            
            self.status = @"";

            self.selectValueLabel.text = [NSString stringWithFormat:@"%@-%@",self.selectValueLabel.text,self.county];
            
            if (self.regionBlock) {
                self.regionBlock (self.provice,self.city,self.county);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
@end

