//
//  FWArticalViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWArticalViewController.h"
#import "FWScanArticalView.h"

@interface FWArticalViewController ()

@property (nonatomic, strong) FWScanArticalView * articalView;

@end

@implementation FWArticalViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"网页编辑器页"];
    self.view.backgroundColor = FWViewBackgroundColor_F1F1F1;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"网页编辑器页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"网页编辑器";
    self.fd_interactivePopDisabled = YES;
    
    self.articalView = [[NSBundle mainBundle] loadNibNamed:@"FWScanArticalView" owner:self options:nil].lastObject;
    self.articalView.vc = self;
    [self.view addSubview:self.articalView];
    [self.articalView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.articalView.upValueLabel.text = [GFStaticData getObjectForKey:kTagPC_qrcode_url]?[GFStaticData getObjectForKey:kTagPC_qrcode_url]:@"";
}

@end
