//
//  FWThirdViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWPublishCameraView.h"
@class AliyunMediaConfig;

//@protocol AliyunConfigureViewControllerDelegate <NSObject>
//
//- (void)configureDidFinishWithMedia:(AliyunMediaConfig *)mediaConfig;
//
//@end

@interface FWPublishViewController : FWBaseViewController<FWPublishCameraViewDelegate>

@property (nonatomic, strong) FWPublishCameraView * cameraView;
@property (nonatomic, strong) UIImage * screenshotImage;
@property (nonatomic, strong) UIImageView * mainView;

@property (assign, nonatomic) BOOL isClipConfig;
//@property (nonatomic, weak) id<AliyunConfigureViewControllerDelegate> delegate;


@end
