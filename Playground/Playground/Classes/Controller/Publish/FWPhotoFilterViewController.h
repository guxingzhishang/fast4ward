//
//  FWPhotoFilterViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/11.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWFilterView.h"

typedef void(^valueBlock)(UIImage *image);


@interface FWPhotoFilterViewController : FWBaseViewController<FWFilterViewDelegate>

@property (nonatomic, strong) UIImageView * editImageView;

/* 滤镜种类 */
@property (nonatomic, strong) NSArray *theaArr;

/* 原图 */
@property (nonatomic, strong) UIImage *originImage;

/* 下一步 */
@property (nonatomic, strong) UIButton * nextButton;

@property (nonatomic, strong) FWFilterView * filterView;

@property (nonatomic, copy) valueBlock myBlock;

@end
