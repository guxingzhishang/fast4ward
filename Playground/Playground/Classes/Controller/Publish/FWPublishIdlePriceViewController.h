//
//  FWPublishIdlePriceViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/20.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^idlePriceBlock)(NSDictionary * dict);

@interface FWPublishIdlePriceViewController : FWBaseViewController<UITextViewDelegate>

@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UILabel * tianxieLabel;
@property (nonatomic, strong) UIButton * saveButton;
@property (nonatomic, strong) UILabel * yuanjiaLabel;
@property (nonatomic, strong) UILabel * rushouLabel;
@property (nonatomic, strong) IQTextView * rushouTextView;
@property (nonatomic, strong) IQTextView * yuanjiaTextView;
@property (nonatomic, strong) UILabel * yunfeiLabel;
@property (nonatomic, strong) UIButton * juliButton;
@property (nonatomic, strong) UIButton * baoyouButton;

@property (nonatomic, copy) idlePriceBlock priceBlock;
@property (nonatomic, strong) NSDictionary * priceDictionary;
@end

NS_ASSUME_NONNULL_END
