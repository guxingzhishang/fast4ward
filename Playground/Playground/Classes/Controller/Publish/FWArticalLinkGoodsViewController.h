//
//  FWArticalLinkGoodsViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/11.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 文章 - > 关联商品
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWArticalLinkGoodsViewController : FWBaseViewController

@property (nonatomic, strong) UIView * upView;
@property (nonatomic, strong) UIImageView * bgImageView;
@property (nonatomic, strong) UIImageView * picImageView;
@property (nonatomic, strong) UILabel * titleLabel;

@property (nonatomic, strong) UIView * downView;
@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UIButton * linkGoodsButton;
@property (nonatomic, strong) UIImageView * linkArrowView;
@property (nonatomic, strong) UILabel * hasLinkLabel;

@property (nonatomic, strong) UIButton * saveButton;

@property (nonatomic, strong) FWFeedListModel * listModel;

- (void)configForView;
@end

NS_ASSUME_NONNULL_END
