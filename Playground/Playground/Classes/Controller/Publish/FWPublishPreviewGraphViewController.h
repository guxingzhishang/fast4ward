//
//  FWPublishPreviewGraphViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/10.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWPublishPreviewGraphView.h"

@interface FWPublishPreviewGraphViewController : FWBaseViewController<FWPublishPreviewGraphViewDelegate>

@property (nonatomic, strong) FWPublishPreviewGraphView * previewView;

@property (nonatomic, strong) NSURL * pathURL;

@property (nonatomic, strong) NSMutableArray * tagsArray;

@property (nonatomic, strong) NSMutableArray * imagesArray;

@property (nonatomic, strong) NSMutableArray * photos;

@property (nonatomic, strong) UIButton * nextButton;

/**
 * 存储获取的上传资源的文件名（bucket，object-key）
 */
@property (nonatomic, strong) NSMutableDictionary * objectDict;

@property (nonatomic, strong) NSString * feed_id;

@property (nonatomic, strong) NSString * video_id;

/**
 * 如果是从草稿箱发布帖子，则传草稿ID,如果不是，传0
 */
@property (nonatomic, strong) NSString * draft_id;

/**
 * 1为保存草稿，2为发布帖子
 */
@property (nonatomic, strong) NSString * is_draft;

/**
 * 是否已经传过阿里云
 */
@property (nonatomic, assign) BOOL  isUploadVideo;

/**
 * 1为发布流程，2为编辑草稿
 */
@property (nonatomic, assign) NSInteger editType;


@property (nonatomic, strong) FWFeedListModel * listModel;

@end
