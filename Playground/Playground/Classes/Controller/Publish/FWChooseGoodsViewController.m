//
//  FWChooseGoodsViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWChooseGoodsViewController.h"
#import "FWChooseGoodsCell.h"

static NSString * chooseGoodsID = @"chooseGoodsID";

@interface FWChooseGoodsViewController ()<UITableViewDelegate,UITableViewDataSource,FWChooseGoodsCellDelegate>
@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSouce;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) FWBussinessShopModel * shopModel;

@end

@implementation FWChooseGoodsViewController
@synthesize infoTableView;

- (NSMutableArray *)dataSouce{
    if (!_dataSouce) {
        _dataSouce = [[NSMutableArray alloc] init];
    }
    return _dataSouce;
}

#pragma mark - > 请求列表
- (void)requestSelectGoodsWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (!self.goods_id || self.goods_id.length <= 0) {
        self.goods_id = @"0";
    }
    
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSouce.count > 0 ) {
            [self.dataSouce removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"20",
                              @"goods_id":self.goods_id,
                              };
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    [request startWithParameters:params WithAction:Select_goods WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                infoTableView.mj_footer = nil;
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            self.shopModel = [FWBussinessShopModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"选择商品页"];
    self.view.backgroundColor = FWTextColor_F5F5F5;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"选择商品页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"选择商品";
    self.pageNum = 1;
    
    [self setupSubViews];
    [self requestSelectGoodsWithLoadMoredData:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 101;
    infoTableView.backgroundColor = FWTextColor_F5F5F5;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestSelectGoodsWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestSelectGoodsWithLoadMoredData:YES];
    }];
}

#pragma mark - > tableview`s Delegate && DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.shopModel.goods_info_selected.count >0)
        return 2;
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.shopModel.goods_info_selected.count > 0) {
        if (section == 0) {
            return self.shopModel.goods_info_selected.count;
        }else{
            return self.shopModel.good_list.count;
        }
    }else{
        return self.shopModel.good_list.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWChooseGoodsCell * cell = [tableView dequeueReusableCellWithIdentifier:chooseGoodsID];
    if (!cell) {
        cell = [[FWChooseGoodsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:chooseGoodsID];
    }
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.shopModel.goods_info_selected.count > 0) {
        if (indexPath.section == 0) {
            FWBussinessShopGoodsListModel * model = self.shopModel.goods_info_selected[indexPath.row];
            [cell configForShopCell:model];
            [cell resetChooseButton:YES];
        }else{
            FWBussinessShopGoodsListModel * model = self.shopModel.good_list[indexPath.row];
            [cell resetChooseButton:NO];
            [cell configForShopCell:model];
//            if ([model.goods_id isEqualToString:self.goods_id]) {
//                [cell resetChooseButton:YES];
//            }else{
//                [cell resetChooseButton:NO];
//            }
        }
    }else{
        FWBussinessShopGoodsListModel * model = self.shopModel.good_list[indexPath.row];
        [cell configForShopCell:model];
        [cell resetChooseButton:NO];
//            if ([model.goods_id isEqualToString:self.goods_id]) {
//                [cell resetChooseButton:YES];
//            }else{
//                [cell resetChooseButton:NO];
//            }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    NSArray *titles = @[@"   已关联商品", @"   全部商品"];

    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.font = DHSystemFontOfSize_16;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.backgroundColor = FWTextColor_F5F5F5;
    
    if (self.shopModel.goods_info_selected.count > 0) {
        if (section == 0) {
            titleLabel.text = titles[0];
        }else{
            titleLabel.text = titles[1];
        }
    }else{
        titleLabel.text = titles[1];
    }
    return titleLabel;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.shopModel.goods_info_selected.count > 0) {
        if (indexPath.section == 0) {
            [self chooseButtonClick:@"0"];
        }else{
            FWBussinessShopGoodsListModel * model = self.shopModel.good_list[indexPath.row];
            [self chooseButtonClick:model.goods_id];
        }
    }else{
        FWBussinessShopGoodsListModel * model = self.shopModel.good_list[indexPath.row];
        [self chooseButtonClick:model.goods_id];
    }
}

#pragma mark - > 选择商品
- (void)chooseButtonClick:(NSString *)goods_id {
    
    self.myBlock(goods_id);
    [self.navigationController popViewControllerAnimated:YES];
}

@end
