//
//  FWPublishSearchUsersViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/4.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWPublishSearchUsersViewController.h"
#import "FWFollowModel.h"

#import "FWSearchUserModel.h"
#import "FWSearchRequest.h"

@interface FWPublishSearchUsersViewController ()<UITableViewDelegate,UITableViewDataSource,FWPublishSearchBarViewDelegate>

@property (nonatomic, strong) NSMutableArray * userList;
@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, strong) FWSearchUserModel * userModel;

@end

@implementation FWPublishSearchUsersViewController
@synthesize infoTableView;
@synthesize pageNum;
@synthesize userModel;

- (NSMutableArray *)userList{
    if (!_userList) {
        _userList = [[NSMutableArray alloc] init];
    }
    
    return _userList;
}

#pragma mark - > 请求搜索的用户列表
- (void)requestFollowsListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        [self.infoTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];

        if (self.userList.count > 0 ) {
            [self.userList removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"query":self.query,
                              @"page":@(pageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_search_users  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            userModel = [FWSearchUserModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }
            
            NSMutableArray * tempArr = userModel.user_list;
            
            [self.userList addObjectsFromArray:tempArr];
            
            if([tempArr count] == 0 && pageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"搜索用户页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"搜索用户页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    
    pageNum = 0;
    self.view.backgroundColor = FWTextColor_FAFAFA;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self setupNaviView];
    
    [self.navigationView.resultSearchBar becomeFirstResponder];
}

- (void)setupNaviView{
    self.navigationView = [[FWPublishSearchBarView alloc] init];
    self.navigationView.delegate = self;
    [self.view addSubview:self.navigationView];
    self.navigationView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40+20+FWCustomeSafeTop);

    infoTableView = [[FWTableView alloc] init];
    infoTableView.rowHeight = 56;
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.backgroundColor = FWTextColor_FAFAFA;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    infoTableView.frame = CGRectMake(0, CGRectGetMaxY(self.navigationView.frame)+10, SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.navigationView.frame)-10-FWSafeBottom);
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    @weakify(self);
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestFollowsListWithLoadMoredData:YES];
    }];
}

#pragma mark - > 返回
- (void)backClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 搜索
- (void)searchResultWithText:(NSString *)content{
    
    if (content.length <= 0) {
        return;
    }
        
    self.query = content;
    
    [self requestFollowsListWithLoadMoredData:NO];
}

#pragma mark - > 关闭输入框
- (void)selectClearButton {
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - > tableView的代理方法和数据源
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"searchUserCellID";
    
    FWPublishSearchUserCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWPublishSearchUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if (self.userList.count > 0) {
        [cell configForCell:self.userList[indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    FWMineInfoModel * userModel = self.userList[indexPath.row];
    
    if (nil == userModel.uid) {
        return;
    }
    
    if ([self.atUserMutableArray containsObject:userModel.uid]) {
        [[FWHudManager sharedManager] showErrorMessage:@"已经选过此用户，请选择其他用户" toController:self];
        return;
    }
    
    if ([self.atUserMutableArray count] >= 5) {
        [[FWHudManager sharedManager] showErrorMessage:@"最多选择5名用户" toController:self];
        return;
    }
    
    if (self.chooseBlock) {
        self.chooseBlock(userModel);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark keyboardNotification
-(void)keyboardShow:(NSNotification *)note{
    
    CGRect keyBoardRect=[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat deltaY=keyBoardRect.size.height;
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        
        infoTableView.frame = CGRectMake(0, CGRectGetMaxY(self.navigationView.frame)+10, SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.navigationView.frame)-10-FWSafeBottom-deltaY);
    }];
}

-(void)keyboardHide:(NSNotification *)note{
    
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
    } completion:^(BOOL finished) {
        infoTableView.frame = CGRectMake(0, CGRectGetMaxY(self.navigationView.frame)+10, SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(self.navigationView.frame)-10-FWSafeBottom);
    }];
}
@end

@implementation FWPublishSearchBarView

- (id)init{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, 55+40+20+FWCustomeSafeTop);
        self.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self setupSubviews];
    }
    
    return self;
}

- (void)setupSubviews{
    
    self.resultSearchBar = [[UISearchBar alloc] init];
    self.resultSearchBar.delegate = self;
    self.resultSearchBar.tintColor = [UIColor darkGrayColor];
    self.resultSearchBar.barTintColor = [UIColor whiteColor];
    self.resultSearchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.resultSearchBar.placeholder = @"搜索用户";
    [self addSubview:self.resultSearchBar];
    [self.resultSearchBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(7);
        make.top.mas_equalTo(self).mas_offset(20+FWCustomeSafeTop);
        make.right.mas_equalTo(self).mas_offset(-62);
        make.height.mas_equalTo(40);
    }];
        
    if (@available(iOS 13.0, *)) {
        UITextField *textField = (UITextField *)self.resultSearchBar.searchTextField;
        
        textField.font = DHSystemFontOfSize_14;
        // 设置 Search 按钮可用
        textField.enablesReturnKeyAutomatically = NO;
        
//        UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];
//
//        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
//        [button setTitle:@"收起" forState:UIControlStateNormal];
//        [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
//
//        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//
//        UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//        NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
//        [bar setItems:buttonsArray];
//        textField.inputAccessoryView = bar;
        
        
        // 改变输入框背景色
        textField.subviews[0].backgroundColor = [UIColor clearColor];
    }else{
        // 去掉搜索框边框
         for(int i =  0 ;i < self.resultSearchBar.subviews.count;i++){
             UIView * backView = self.resultSearchBar.subviews[i];
             if ([backView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                 [backView removeFromSuperview];
                 [self.resultSearchBar setBackgroundColor:[UIColor clearColor]];

                 break;
             }else{
                 NSArray * arr = self.resultSearchBar.subviews[i].subviews;
                 for(int j = 0;j<arr.count;j++   ){
                     UIView * barView = arr[i];
                     if ([barView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {

                         [barView removeFromSuperview];
                         [self.resultSearchBar setBackgroundColor:[UIColor clearColor]];
                         break;
                     }
                 }
             }
         }
        

        // 改变UISearchBar内部输入框样式
        UIView *searchTextField = nil;
        searchTextField = [[[self.resultSearchBar.subviews firstObject] subviews] lastObject];
        // 改变输入框背景色
        searchTextField.subviews[0].backgroundColor = [UIColor clearColor];
    }

    // 获取 TextField 并设置
    for (UIView *subView in self.resultSearchBar.subviews) {
        for (UIView *textFieldSubView in subView.subviews) {
            if ([textFieldSubView isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)textFieldSubView;

                textField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
                
                // 设置 Search 按钮可用
                textField.enablesReturnKeyAutomatically = NO;

//                UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];
//
//                UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
//                [button setTitle:@"收起" forState:UIControlStateNormal];
//                [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
//                [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
//
//                UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//
//                UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//                NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
//                [bar setItems:buttonsArray];
//                textField.inputAccessoryView = bar;

                break;
            }
        }
    }
    
    self.backButton = [[UIButton alloc] init];
    self.backButton.titleLabel.font = DHSystemFontOfSize_16;
    [self.backButton setTitle:@"取消" forState:UIControlStateNormal];
    [self.backButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-8);
        make.left.mas_equalTo(self.resultSearchBar.mas_right).mas_offset(14);
        make.height.mas_equalTo(40);
        make.width.mas_greaterThanOrEqualTo(40);
        make.centerY.mas_equalTo(self.resultSearchBar);
    }];
}

#pragma mark - > 返回
- (void)backButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(backClick)]) {
        [self.delegate backClick];
    }
}

#pragma mark - > searchBar delegate 实时检测
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([self.delegate respondsToSelector:@selector(searchResultWithText:)]) {
        [self.delegate searchResultWithText:searchBar.text];
    }
}

#pragma mark - > 搜索
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar endEditing:YES];
    
    if ([self.delegate respondsToSelector:@selector(searchResultWithText:)]) {
        [self.delegate searchResultWithText:searchBar.text];
    }
}

#pragma mark - > 去掉输入框边框
- (void)removeBorder:(UISearchBar * )searchBar{
    
    if (@available(iOS 13.0, *)) {

    }else{
        for(int i =  0 ;i < searchBar.subviews.count;i++){
            UIView * backView = searchBar.subviews[i];
            if ([backView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                [backView removeFromSuperview];
                [searchBar setBackgroundColor:[UIColor clearColor]];
                
                break;
            }else{
                NSArray * arr = searchBar.subviews[i].subviews;
                for(int j = 0;j<arr.count;j++   ){
                    UIView * barView = arr[i];
                    if ([barView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                        
                        [barView removeFromSuperview];
                        [searchBar setBackgroundColor:[UIColor clearColor]];
                        break;
                    }
                }
            }
        }
    }
}

- (void)clearTextClick{
    
    self.resultSearchBar.text = @"";
    if ([self.delegate respondsToSelector:@selector(selectClearButton)]) {
        [self.delegate selectClearButton];
    }
}

#pragma mark - > 收起键盘
- (void)hideKeyBoard{
    
    [self.resultSearchBar endEditing:YES];
}

@end

@implementation FWPublishSearchUserCell
@synthesize userModel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.photoImageView = [[UIImageView alloc] init];
    self.photoImageView.layer.cornerRadius = 36/2;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.image = [UIImage imageNamed:@""];
    [self.contentView addSubview:self.photoImageView];
    [self.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(36, 36));
        make.left.mas_equalTo(self.contentView).mas_offset(14);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.nameLabel.textColor = FWTextColor_222222;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageView.mas_right).mas_offset(14);
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
    }];
}

- (void)configForCell:(id)model{
    
    userModel = (FWMineInfoModel *)model;

    self.nameLabel.text = userModel.nickname;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:userModel.header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
}


@end
