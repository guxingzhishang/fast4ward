//
//  FWPublishIdleOtherViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/20.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^idleOtherBlock)(NSDictionary * dict);

@interface FWPublishIdleOtherViewController : FWBaseViewController<UIPickerViewDelegate,UIPickerViewDataSource>


@property (nonatomic, strong) UILabel * titleLabel;

@property (nonatomic, strong) UITextField * fenleiView;
@property (nonatomic, strong) UILabel * fenleiLabel;
@property (nonatomic, strong) UIView * fenleiLineLabel;
@property (nonatomic, strong) UILabel * fenleiRightLabel;
@property (nonatomic, strong) UIImageView * fenleiArrowImageView;

@property (nonatomic, strong) UIView * carTypeView;
@property (nonatomic, strong) UILabel * carTypeLabel;
@property (nonatomic, strong) UIView * carTypeLineLabel;
@property (nonatomic, strong) UILabel * carTypeRightLabel;
@property (nonatomic, strong) UIImageView * carTypeArrowImageView;

@property (nonatomic, strong) UIView * topicView;
@property (nonatomic, strong) UILabel * topicLabel;
@property (nonatomic, strong) UIView * topicLineLabel;
@property (nonatomic, strong) UILabel * topicRightLabel;
@property (nonatomic, strong) UIImageView * topicArrowImageView;

@property (nonatomic, strong) UIPickerView * zonghePickerView;

@property (nonatomic, strong) UILabel * otherLabel;
@property (nonatomic, strong) UIButton * allNewButton;
@property (nonatomic, strong) UIButton * zitiButton;

@property (nonatomic, strong) NSMutableArray * paixuArray;
@property (nonatomic, strong) NSDictionary * otherDict;

@property (nonatomic, copy) idleOtherBlock otherBlock;



@end

NS_ASSUME_NONNULL_END
