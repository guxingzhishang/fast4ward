//
//  FWPublishIdleRegionViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/21.
//  Copyright © 2020 孤星之殇. All rights reserved.
//
/**
 * 发货地
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN


typedef void(^idleRegionBlock)(NSString * provice,NSString * city, NSString * county);

@interface FWPublishIdleRegionViewController : FWBaseViewController

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;

@property (nonatomic, assign) NSInteger isDeny;
@property (nonatomic, strong) NSString * currentRegion;

@property (nonatomic, strong) UILabel * selectLabel;
@property (nonatomic, strong) UILabel * selectValueLabel;

@property (nonatomic, strong) NSString * provice;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * county;

@property (nonatomic, copy) idleRegionBlock regionBlock;

@end

NS_ASSUME_NONNULL_END
