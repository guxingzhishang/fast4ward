//
//  FWPublishPreviewViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/*
 * 视频发布流程：
 * 1、申请sts图片的临时凭证（上传封面用）（get_sts_token）
 * 2、申请sts视频的临时凭证（上传视频用）（get_sts_token）
 * 3、获取上传资源的文件名（get_upload_object_name）
 * 4、创建feed_id（submit_create_feed_id）
 * 5、上传封面到阿里云
 * 6、上传视频到阿里云
 * 7、将所有参数上传到服务器（submit_add_feed - 发布帖子&加入草稿箱）
 *
 * ps:1、2可以在其他页面请求，然后判断时效，第一版先上线，后期修改
 */

#import "FWPublishPreviewViewController.h"
#import "FWSearchTagsViewController.h"
#import "FWPublishRequest.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import "CZHTool.h"
#import "Header.h"
#import "CZHChooseCoverController.h"

#import "FWUpdateInfomationRequest.h"
#import "FWUploadManager.h"
#import "FWTagsViewController.h"
#import "FWChooseBradeViewController.h"

@interface FWPublishPreviewViewController ()

@property (nonatomic, strong) NSString * carType;
@property (nonatomic, assign) BOOL isUpload;

@end

@implementation FWPublishPreviewViewController
@synthesize previewView;
@synthesize tagsArray;
@synthesize isUpload;
@synthesize infoDict;
@synthesize filePath;
@synthesize feed_id;
@synthesize isUploadVideo;
@synthesize objectDict;
@synthesize nextButton;

- (void)setPathURL:(NSURL *)pathURL{
    _pathURL = pathURL;
}

#pragma mark - > 请求阿里云临时凭证
- (void)requsetImageStsData{

    // 用来请求获取上传视频封面时的临时凭证
    FWPublishRequest * requst = [[FWPublishRequest alloc] init];
    [requst requestStsTokenWithType:@"1"];
}

- (void)requsetVideoStsData{
    
    // 用来请求获取上传视频时的临时凭证
    FWPublishRequest * requst = [[FWPublishRequest alloc] init];
    [requst requestStsTokenWithType:@"2"];
}

#pragma mark - > 获取feed_id
- (void)requsetCreateFeedID{
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Submit_create_feed_id  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            NSDictionary * data = HYGET_OBJECT_FROMDIC(responseObject, @"data");
            feed_id = [data objectForKey:@"feed_id"];
        }
    }];
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"video_feed_cover",
                              @"number":@"1",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.objectDict = [[back objectForKey:@"data"] mutableCopy];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"视频发布页"];

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [IQKeyboardManager sharedManager].enable = YES;
    });
    
    NSString * tempString = [GFStaticData getObjectForKey:ChooseCarTypeBackToPublish];
    
    if (tempString && tempString.length > 0) {
        self.carType = tempString;
        [self.previewView setupCarTypeView:tempString];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"视频发布页"];
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"发布预览";
    self.fd_interactivePopDisabled = YES;

    infoDict  = @{}.mutableCopy;
    tagsArray = @[].mutableCopy;
    objectDict = @{}.mutableCopy;
    isUploadVideo = NO;
    
    [self setupSubViews];
   
    if(self.editType == 1){
        /* 获取临时凭证 */
        [self requsetImageStsData];
        [self requsetVideoStsData];

        /* 创建feed_id */
        [self requsetCreateFeedID];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            /* 获取上传文件的路径和bukect */
            [self requestUploadName];
        });
    }
}

#pragma mark - > 视图初始化
- (void)setupSubViews{
    
    UIImage * getImage = [UIImage imageWithContentsOfFile:[self.coverImageDict objectForKey:@"UIImagePath"]];
    
    previewView = [[FWPublishPreviewView alloc] init];
    previewView.fengmianView.image = getImage;
    previewView.publishDelegate = self;
    previewView.vc = self;
    [self.view addSubview:previewView];
    [previewView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [previewView configForView:self.listModel];
    
    if (self.listModel.car_type &&
        self.listModel.car_type.length > 0) {
        self.carType = self.listModel.car_type;
    }else{
        self.carType = @"";
    }
    
    if(self.editType == 2){
        // 编辑草稿，隐藏编辑封面的按钮
        previewView.editButton.hidden = YES;
        [previewView.deleteButton setTitle:@"删除草稿" forState:UIControlStateNormal];
        [tagsArray addObjectsFromArray:self.listModel.tags];
    }else{
        
        NSArray * viewControllers = self.navigationController.viewControllers;
        
        for (UIViewController * item in viewControllers) {
            if ([item isKindOfClass:[FWTagsViewController class]]) {
                
                NSDictionary *dict = [GFStaticData getObjectForKey:CurrentTagsDictionary];
                
                FWSearchTagsSubListModel * tagModel = [[FWSearchTagsSubListModel alloc] init];
                tagModel.tag_name = dict[@"tag_name"];
                tagModel.tag_id = dict[@"tag_id"];
                
                [tagsArray addObject:tagModel];
                
                [previewView setupTagsView:tagsArray];
            }
        }
    }
    
    NSString *paths = NSHomeDirectory();
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dataString = [dateFormatter stringFromDate:date];
    
    //设置一个图片的存储路径
    NSString *imagePath = [NSString stringWithFormat:@"%@/Documents/%@.png",paths,dataString];
    [UIImagePNGRepresentation(previewView.fengmianView.image) writeToFile:imagePath atomically:YES];

    [infoDict setValue:imagePath forKey:@"UIImagePath"];
}

#pragma mark - > 删除草稿
- (void)requestDeleteDraft{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.draft_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_delete_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"删除成功" toController:self];
            
            NSMutableArray * array = [[GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]] mutableCopy];

            if ([array containsObject:self.draft_id]) {
                [array removeObject:self.draft_id];
            }
            
            [GFStaticData saveObject:array forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];

            [self popToPreViewController];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 是否删除草稿
- (void)deleteButtonClick:(UIButton *)sender {
    
    self.nextButton = sender;
    self.nextButton.enabled = NO;
    
    // 防止重复点击
    [self performSelector:@selector(changeButtonStatus) withObject:nil afterDelay:3];
    
    if (self.editType == 2) {
        
        // 删除草稿，直接请求接口
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除草稿" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self requestDeleteDraft];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        // 保存草稿
        self.is_draft = @"1";
        [self requestCheckWithType:@"2"];
    }
}

#pragma mark - > 删除标签
- (void)deleteTagsClick:(NSInteger)index{

    if (index == 0) {
        if (tagsArray.count > 0) {
            [tagsArray removeObjectAtIndex:index];
        }
        
        [previewView setupTagsView:tagsArray];
    }else{
        [GFStaticData saveObject:nil forKey:ChooseCarTypeBackToPublish];
        [previewView setupCarTypeView:@""];
    }
}

#pragma mark - > 选择车系
- (void)carTypeButtonClick{
    
    FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
    CBVC.chooseFrom = @"publish";
    [self.navigationController pushViewController:CBVC animated:YES];
}

#pragma mark - > 编辑封面
- (void)editButtonClick {
    
    if (self.pathURL.absoluteString.length>0) {
        NSURL *url = self.pathURL;
        
        //计算相册视频时长
        NSDictionary *videoDic = [CZHTool getLocalVideoSizeAndTimeWithSourcePath:url.absoluteString];
        
        int videoTime = [[videoDic valueForKey:@"duration"] intValue];
        
        NSUInteger limitTime = 30;
        if (videoTime > limitTime) {
            return;
        }
        
        CZHChooseCoverController *chooseCover = [[CZHChooseCoverController alloc] init];
        chooseCover.videoPath = self.pathURL;
        chooseCover.isEdit = YES;
        chooseCover.coverImageBlock = ^(NSDictionary *imageDict) {
            infoDict = [imageDict mutableCopy];
            
            UIImage * getImage = [UIImage imageWithContentsOfFile:[imageDict objectForKey:@"UIImagePath"]];

            previewView.fengmianView.image = getImage;
        };
        [self.navigationController pushViewController:chooseCover animated:YES];
    }
}

#pragma mark - > 保存到本地
- (void)saveButtonClick {}

#pragma mark - > 选择标签
- (void)tagsButtonClick {
    
    FWSearchTagsViewController * STVC = [[FWSearchTagsViewController alloc] init];
    STVC.myBlock = ^(FWSearchTagsSubListModel * tagModel) {
        
        if (tagsArray.count >0 ) {
            [tagsArray removeAllObjects];
        }
        
        [tagsArray addObject:tagModel];
        [previewView setupTagsView:tagsArray];
    };
    [self.navigationController pushViewController:STVC animated:YES];
}


#pragma mark - > 上传视频到阿里云
- (void)publishButtonClick:(UIButton *)sender{
    
    self.nextButton = sender;
    self.nextButton.enabled = NO;
    
    // 防止重复点击
    [self performSelector:@selector(changeButtonStatus) withObject:nil afterDelay:3];

    self.is_draft = @"2";
    [self.view endEditing:YES];

    if (self.editType == 2) {
        // 编辑草稿，直接请求接口
        self.draft_id = self.listModel.feed_id;
        self.video_id = self.listModel.video_id;
        [self.objectDict setObject:self.listModel.feed_cover forKey:@"object_key"];
    }
    
    [self requestCheckWithType:@"1"];
}

- (void)changeButtonStatus{
    
    self.nextButton.enabled = YES;
}

#pragma mark - > 返回
- (void)backBtnClick{
    
    self.is_draft = @"1";
    [self.view endEditing:YES];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否保存到草稿箱" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"不保存" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        if (self.editType == 1) {
            
            for (UIViewController * item in self.navigationController.viewControllers) {
                if ([item isKindOfClass:[FWTagsViewController class]]) {
                    [self.navigationController popToViewController:item animated:YES];
                    return ;
                }
            }
            [self popToPreViewController];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (self.editType == 2) {
            // 编辑草稿，直接请求接口
            self.video_id = self.listModel.video_id;
            [self.objectDict setObject:self.listModel.feed_cover forKey:@"object_key"];
        }

        [self requestCheckWithType:@"2"];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 预校验
- (void)requestCheckWithType:(NSString *)type{
    
    if (![[GFStaticData getObjectForKey:Network_Available] boolValue]) {
        [[FWHudManager sharedManager] showErrorMessage:@"当前网络不可用" toController:self];
        return ;
    }
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSMutableDictionary * params = [[self getParams] mutableCopy];
    if (nil == params) {
        return;
    }
    
    [params setObject:@"1" forKey:@"flag_check"];
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params.copy WithAction:Submit_add_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (self.editType == 1) {
                if ([type isEqualToString:@"1"]) {
                    // 发布
                    [self uploadFile:@"1"];
                }else{
                    // 执行上传的单例
                    [self uploadFile:@"2"];
                }
            }else{
                
                [self requestSaveDraft];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 发布 or 存草稿箱 (1为保存草稿，2为发布帖子)
- (void)requestSaveDraft{
    
    FWPublishRequest * request = [[FWPublishRequest alloc] init];
    
    NSMutableDictionary * params = [[self getParams] mutableCopy];
    if (nil == params) {
        return;
    }
    
    [params setObject:self.video_id forKey:@"video_id"];
    [params setObject:@"2" forKey:@"flag_check"];
    
    [request startWithParameters:params.copy WithAction:Submit_add_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([self.is_draft isEqualToString:@"2"]) {
                [self deleteLocalFeedID:params];
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 发布或删除草稿成功后，如果本地有这个feed_id,就删除
- (void)deleteLocalFeedID:(NSMutableDictionary *)params{
    
    NSMutableArray * array = [GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
    
    NSArray * tempArr = [NSArray arrayWithArray:array];
    NSSet *set = [NSSet setWithArray:tempArr];
    [array removeAllObjects];
    for (NSString * str in set){
        [array addObject:str];
    }
    
    if ([array containsObject:params[@"feed_id"]]) {
        [array removeObject:params[@"feed_id"]];
        
        [GFStaticData saveObject:array forKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
    }
}

/* type 1:上传 ，2：保存 */
- (void)uploadFile:(NSString *)type{
    
    FWUploadManager * defaultManager  = [FWUploadManager sharedRunLoopWorkDistribution];
    defaultManager.infoDict = self.infoDict;
    defaultManager.objectDict = self.objectDict;
    defaultManager.filePath = self.filePath;
    defaultManager.fengmianImage = previewView.fengmianView.image;
    defaultManager.params = [[self getParams] mutableCopy];
    [defaultManager requestUploadFengmianImageWithType:@"1"];

    UIImage * getImage = [UIImage imageWithContentsOfFile:[self.infoDict objectForKey:@"UIImagePath"]];
    NSData * imageData = UIImagePNGRepresentation(getImage);
    
    if (!imageData) {
        return;
    }
    
    NSDictionary * tempDict = @{
                                @"uploadProgress":@"0",
                                @"feed_type":[[self getParams] objectForKey:@"feed_type"],
                                @"imageData":imageData,
                                @"is_draft":[[self getParams] objectForKey:@"is_draft"],
                                };
    
    [GFStaticData saveObject:upLoadStatusUploading forKey:upLoadStatus];
    [GFStaticData saveObject:tempDict forKey:upLoadStatusUploading];
    
    [GFStaticData saveObject:@"YES" forKey:@"isUploading"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedUpLoad" object:nil userInfo:tempDict];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTab" object:nil userInfo:nil];


    [self popToPreViewController];
}

- (void)popToPreViewController{
    
    if ([self.draft_id isEqualToString:@"0"]) {
        /* 不是草稿，回首页 */
        __weak FWPublishPreviewViewController * weakSelf = self;
        
        weakSelf.navigationController.tabBarController.selectedIndex = 0;
        [weakSelf.navigationController popToRootViewControllerAnimated:NO];
    }else{
        /* 草稿，回草稿箱 */
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (NSDictionary *)getParams{
    
    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (FWSearchTagsSubListModel *  model in tagsArray) {
        [tempArr addObject:model.tag_id];
    }
    NSString * tags = [tempArr mj_JSONString];
    
    CGFloat fixelW = CGImageGetWidth(previewView.fengmianView.image.CGImage);
    CGFloat fixelH = CGImageGetHeight(previewView.fengmianView.image.CGImage);
    
    if (!previewView.tempGoods_id || [previewView.tempGoods_id isEqualToString:@"0"]) {
        previewView.tempGoods_id = @"0";
    }
    
    if (fixelW <= 0 || fixelH <= 0) {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"视频处理失败，请返回重新录制" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"明白了" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self popToPreViewController];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return nil;
    }
    
    NSString * invite_uids = @"";
    NSMutableArray * uidTempArray = @[].mutableCopy;
    if (self.previewView.atUserMutableArray.count > 0) {
        for (FWMineInfoModel * userInfo in self.previewView.atUserMutableArray) {
            [uidTempArray addObject:userInfo.uid];
        }
        
        invite_uids = [uidTempArray mj_JSONString];
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"is_draft":self.is_draft,
                              @"draft_id":self.draft_id,
                              @"feed_type":@"1",
                              @"feed_id":self.feed_id,
                              @"video_name":@"",
                              @"video_cover":[self.objectDict objectForKey:@"object_key"],
                              @"video_cover_wh":@"",
                              @"video_cover_width":@(fixelW).stringValue,
                              @"video_cover_height":@(fixelH).stringValue,
                              @"imgs":@"",
                              @"feed_title":previewView.contentTextView.text,
                              @"tags":tags,
                              @"flag_check":@"1",
                              @"goods_id":previewView.tempGoods_id,
                              @"car_type":self.carType,
//                              @"invite_uids":invite_uids,
                              };
    return params;
}


@end
