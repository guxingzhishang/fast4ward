//
//  FWSearchTagsViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWSearchTagsViewController.h"
#import "FWTagsListCell.h"
#import <Accelerate/Accelerate.h>
#import "FWSearchTagsListRequest.h"
#import "FWSearchTagsListModel.h"
#import "FWTagsViewController.h"

@interface FWSearchTagsViewController ()

@property (nonatomic, strong) UIImageView * mainView;

@property (nonatomic, strong) NSMutableArray * searchArray;

@property (nonatomic, strong) NSMutableArray * tagsList;

@end

@implementation FWSearchTagsViewController
@synthesize headerView;
@synthesize cancelBtn;
@synthesize searchBar;
@synthesize tagsView;
@synthesize listTableView;
@synthesize mainView;

- (NSMutableArray *)searchArray{
    if (!_searchArray) {
        _searchArray = [[NSMutableArray alloc] init];
    }
    return _searchArray;
}

- (NSMutableArray *)tagsList{
    if (!_tagsList) {
        _tagsList = [[NSMutableArray alloc] init];
    }
    
    return _tagsList;
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"搜索标签页"];
    [self requestData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"搜索标签页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    self.fd_prefersNavigationBarHidden = YES;

    self.fd_interactivePopDisabled = YES;
}

#pragma mark - > 视图初始化
- (void)addSubviews{
    
    mainView = [[UIImageView alloc] init];
    mainView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [self.view addSubview:mainView];
    
    [self setupNavigationView];
    [self setupTableView];
}

- (void)setupNavigationView{
    
    headerView = [[UIView alloc] init];
    [self.view addSubview:headerView];
    [headerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 84+FWCustomeSafeTop));
    }];
    
    searchBar = [[FWSearchBar alloc] initWithFrame:CGRectMake(10, 25+FWCustomeSafeTop, SCREEN_WIDTH-70, 40)];
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault;
    searchBar.barTintColor = [UIColor whiteColor];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.tintColor = FWTextColor_A6A6A6;
    searchBar.placeholder = @"请输入车系、活动或位置";
    [headerView addSubview:searchBar];
    
    [self removeBorder:searchBar];

    cancelBtn = [[UIButton alloc] init];
    cancelBtn.titleLabel.font = DHSystemFontOfSize_14;
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:cancelBtn];
    [cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(headerView).mas_offset(-10);
        make.centerY.mas_equalTo(searchBar);
        make.size.mas_equalTo(CGSizeMake(40, 30));
    }];
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(headerView);
        make.height.mas_equalTo(10);
        make.bottom.mas_equalTo(headerView.mas_bottom);
    }];
}

- (void)setupTableView{
    
    tagsView = [[FWTagsListView alloc] init];
    tagsView.tagsDelegate = self;
    tagsView.backgroundColor = [UIColor clearColor];
    [self.view  addSubview:tagsView];
    [tagsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(headerView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop));
    }];
    
    [tagsView configViewWithModel:nil];
    
    listTableView = [[UITableView alloc] init];
    listTableView.delegate = self;
    listTableView.dataSource = self;
    listTableView.rowHeight = 60;
    listTableView.backgroundColor = [UIColor clearColor];
    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view  addSubview:listTableView];
    [listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(headerView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop));
    }];
    listTableView.hidden = YES;
}

#pragma mark - > 选中标签
-(void)tagsBtnOnClickWithSuperIndex:(NSInteger)superIndex WithSubIndex:(NSInteger)subIndex{
    
    [self.searchBar endEditing:YES];
       
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        FWSearchTagsSubListModel * model ;
        if (superIndex == 321) {
           
           NSData * data = [GFStaticData getObjectForKey:Tags_History_Array];
           NSMutableArray *tagsArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
           
           model = tagsArray[subIndex];
        }else if (superIndex == 322) {
           
           model  = self.tagsList[subIndex];
        }

        // 将标签存起来
        [self saveTagsCacheModel:model];

        //推荐标签
        self.myBlock(model);

        [self.navigationController popViewControllerAnimated:YES];
    });
}

#pragma mark - > 将标签存为缓存
- (void)saveTagsCacheModel:(FWSearchTagsSubListModel *)model{
    
    /* 如果有，则取出历史标签，
     加入新元素后，插在第0个位置
     如果大于5个，将最后一个删除 */
    NSData * data = [GFStaticData getObjectForKey:Tags_History_Array];
    NSMutableArray *tagsArray = @[].mutableCopy;
    NSMutableArray * tempArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (tempArray.count > 0) {
        
        NSInteger index = -1;
        for (int i =0; i< tempArray.count; i++) {
            FWSearchTagsSubListModel * submodel = tempArray[i];
            if ([submodel.tag_id isEqualToString:model.tag_id]) {
                index = i;
                break;
            }
        }
        /* 如果有相同id存在的标签，先删除原标签，再重新加入
         始终让每次查询的在最前面 */
        
        tagsArray =  tempArray;
        
        if (index >= 0) {
            [tagsArray removeObjectAtIndex:index];
        }
        [tagsArray insertObject:model atIndex:0];
    }else{
        [tagsArray addObject:model];
    }
    
    if (tagsArray.count>5) {
        [tagsArray removeLastObject];
    }
    
    data = [NSKeyedArchiver archivedDataWithRootObject:tagsArray];
    [GFStaticData saveObject:data forKey:Tags_History_Array];
}

#pragma mark - > tableView  Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.searchArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"tagsListID";

    FWTagsListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWTagsListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = [UIColor clearColor];
    
    [cell cellConfigureFor:self.searchArray[indexPath.row]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.searchBar endEditing:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        FWSearchTagsSubListModel * model = self.searchArray[indexPath.row];
        FWFollowRequest * request = [[FWFollowRequest alloc] init];
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"tag_name":model.tag_name,
                                  };
        // 检测标签是否合法
        [request startWithParameters:params WithAction:Submit_check_tags  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                model.tag_id = [[back objectForKey:@"data"] objectForKey:@"tag_id"];

                // 将标签存起来
                [self saveTagsCacheModel:model];
                
                self.myBlock(model);
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    });
}


#pragma mark - > searchBar delegate 实时搜索标签
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([self.searchBar.text length] > 12)
    {
        self.searchBar.text = [self.searchBar.text substringToIndex:12];
    }
    
    FWSearchTagsListRequest * request = [[FWSearchTagsListRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"query":searchBar.text
                              };
    
    if (searchBar.text.length >0) {
        tagsView.hidden = YES;
        listTableView.hidden = NO;
        
        [request startWithParameters:params WithAction:Get_search_tags  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if (self.searchArray.count >0) {
                    [self.searchArray removeAllObjects];
                }
                
                FWSearchTagsListModel * model = [FWSearchTagsListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                self.searchArray = model.tag_list;
                [listTableView reloadData];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }else{
        tagsView.hidden = NO;
        listTableView.hidden = YES;
    }
}

#pragma mark - > 请求推荐标签
- (void)requestData{

    FWSearchTagsListRequest * request = [[FWSearchTagsListRequest alloc] init];
    
    NSDictionary * param =@{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_pub_reco_tags  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (self.tagsList.count > 0) {
                [self.tagsList removeAllObjects];
            }
            
            FWSearchTagsListModel * model = [FWSearchTagsListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            self.tagsList = model.tag_list;
            [tagsView configViewWithModel:self.tagsList];
            
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 取消搜索
- (void)cancelBtnOnClick:(UIButton *)sender{
   
    [self.searchBar endEditing:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}


#pragma mark - > 去掉输入框边框
- (void)removeBorder:(UISearchBar * )searchBar{
    
    if (@available(iOS 13.0, *)) {
        UITextField *textField = (UITextField *)self.searchBar.searchTextField;
        
        textField.font = DHSystemFontOfSize_14;
        // 设置 Search 按钮可用
        textField.enablesReturnKeyAutomatically = NO;
        
        // 改变输入框背景色
        textField.subviews[0].backgroundColor = [UIColor clearColor];
        textField.layer.cornerRadius = 2;
        textField.layer.masksToBounds = YES;
    }else{
        //设置背景图是为了去掉上下黑线
        self.searchBar.backgroundImage = [[UIImage alloc] init];
        // 设置SearchBar的颜色主题为白色
        self.searchBar.barTintColor = FWViewBackgroundColor_F1F1F1;

        UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
        if (searchField) {
            [searchField setBackgroundColor:FWViewBackgroundColor_F1F1F1];
            searchField.layer.cornerRadius = 2;
            searchField.layer.masksToBounds = YES;
        }
    }
}

@end
