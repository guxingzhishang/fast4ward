//
//  FWSearchTagsViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWTagsListView.h"
#import "FWSearchTagsListModel.h"
#import "FWSearchBar.h"

typedef void(^valueBlock)(FWSearchTagsSubListModel *tagModel);

@interface FWSearchTagsViewController : FWBaseViewController<FWTagsListViewDelegate,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>

/**
 * 做navigation的view
 */
@property (nonatomic, strong) UIView * headerView;

/**
 * 取消按钮
 */
@property (nonatomic, strong) UIButton * cancelBtn;

/**
 * 搜索
 */
@property (nonatomic, strong) FWSearchBar * searchBar;

/**
 * 列表的tableview
 */
@property (nonatomic, strong) UITableView * listTableView;

@property (nonatomic, strong) FWTagsListView * tagsView;

@property (nonatomic, copy) valueBlock myBlock;


@end
