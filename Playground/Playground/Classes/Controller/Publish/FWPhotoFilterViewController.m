//
//  FWPhotoFilterViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/11.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWPhotoFilterViewController.h"
#import "ColorFliterAuxiliary.h"

@interface FWPhotoFilterViewController ()

@end

@implementation FWPhotoFilterViewController
@synthesize editImageView;
@synthesize theaArr;
@synthesize nextButton;
@synthesize filterView;

- (void)setOriginImage:(UIImage *)originImage{
    _originImage = originImage;
}

- (void)dealloc{
    for (UIView * view in filterView.subviews) {
        [view removeAllSubviews];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"编辑照片页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"编辑照片页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"编辑照片";

    theaArr = [[NSArray alloc]initWithObjects:@"原图",@"LOMO",@"黑白",@"复古",@"哥特",@"锐化",@"淡雅",@"酒红",@"青柠",@"浪漫",@"光晕",@"蓝调",@"梦幻",@"夜色", nil];
    
    [self setupSubviews];
}

- (void)setupSubviews{

    nextButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
    nextButton.titleLabel.font = DHSystemFontOfSize_16;
    nextButton.titleLabel.textAlignment = NSTextAlignmentRight;
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClick) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];

    editImageView = [[UIImageView alloc] init];
    editImageView.contentMode = UIViewContentModeScaleAspectFit;
    editImageView.image = self.originImage;
    [self.view addSubview:editImageView];
    [editImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.view).mas_offset(-200);
    }];

    UIView * bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:bottomView];
    [bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(self.view);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.top.mas_equalTo(editImageView.mas_bottom);
    }];

    UILabel * tipLabel = [[UILabel alloc] init];
    tipLabel.text = @"编辑这张图片可以获得更多曝光哦";
    tipLabel.font = DHSystemFontOfSize_14;
    tipLabel.textColor = FWTextColor_969696;
    tipLabel.textAlignment = NSTextAlignmentCenter;
    [bottomView addSubview:tipLabel];
    [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(bottomView);
        make.top.mas_equalTo(bottomView).mas_offset(20);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(20);
    }];

    UIButton * filtButton = [[UIButton alloc] init];
    filtButton.titleLabel.font = DHSystemFontOfSize_16;
    [filtButton setImage:[UIImage imageNamed:@"publish_filter"] forState:UIControlStateNormal];
    [filtButton setTitle:@"滤镜" forState:UIControlStateNormal];
    [filtButton setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
    [filtButton addTarget:self action:@selector(filterButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    filtButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [filtButton setTitleEdgeInsets:UIEdgeInsetsMake(filtButton.imageView.frame.size.height+80 ,-filtButton.imageView.frame.size.width-30, 0.0,0.0)];
    [filtButton setImageEdgeInsets:UIEdgeInsetsMake(-10, 0.0,0.0, -filtButton.titleLabel.bounds.size.width)];
    [bottomView addSubview:filtButton];
    [filtButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bottomView);
        make.size.mas_equalTo(CGSizeMake(100, 100));
        make.top.mas_equalTo(tipLabel).mas_offset(50);
    }];

    filterView = [[FWFilterView alloc] init];
    filterView.contentSize = CGSizeMake(theaArr.count * 100+40, 0);
    filterView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 200);
    filterView.filterDelegate = self;
    filterView.vc = self;
    filterView.originImage = self.originImage;
    [self.view addSubview:filterView];
}

#pragma mark - > 滤镜选择弹窗
- (void)filterButtonOnClick{

    [filterView showView];

    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];

    [animation setType: kCATransitionMoveIn];

    [animation setSubtype: kCATransitionFromTop];

    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];

    filterView.frame = CGRectMake(0, SCREEN_HEIGHT-200-64, SCREEN_WIDTH, 200);

    [filterView.layer addAnimation:animation forKey:nil];
}

- (void)settingFilterClick:(NSInteger)index{

    editImageView.image = [ColorFliterAuxiliary changeImage:index imageView:self.originImage];
}

#pragma mark - > 下一步保存图片
- (void)nextClick{

    UIImageWriteToSavedPhotosAlbum(editImageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}


//保存成功调用的方法
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    //保存成功
    [[FWHudManager sharedManager] showSuccessMessage:@"保存成功" toController:self];
    DHWeakSelf;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.myBlock(weakSelf.editImageView.image);

        [weakSelf.navigationController popViewControllerAnimated:YES];
    });
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [filterView hideView];
}

@end
