//
//  FWPublishIdlePriceViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/20.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWPublishIdlePriceViewController.h"
#import "FWChooseBradeViewController.h"

@interface FWPublishIdlePriceViewController ()
@property (nonatomic, assign) BOOL  isBaoyou;
@end

@implementation FWPublishIdlePriceViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    self.view.backgroundColor = FWColorWihtAlpha(@"000000", 0.6);
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.isBaoyou = NO;
    [self setupSubViews];
}

- (void)topViewClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setupSubViews{
    
    UIView * topView = [[UIView alloc] init];
    topView.userInteractionEnabled = YES;
    [self.view addSubview:topView];
    [topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-300);
    }];
    [topView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topViewClick)]];

    
    self.bgView = [[UIView alloc] init];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 300));
    }];

    self.tianxieLabel = [[UILabel alloc] init];
    self.tianxieLabel.font = DHBoldFont(14);
    self.tianxieLabel.text =@"填写价格";
    self.tianxieLabel.textColor = FWTextColor_222222;
    self.tianxieLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.tianxieLabel];
    [self.tianxieLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(self.bgView).mas_offset(14);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(20);
    }];
    
    self.saveButton = [[UIButton alloc] init];
    self.saveButton.titleLabel.font = DHFont(12);
    self.saveButton.layer.cornerRadius = 2;
    self.saveButton.layer.masksToBounds = YES;
    self.saveButton.backgroundColor = FWTextColor_222222;
    [self.saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [self.saveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.saveButton addTarget:self action:@selector(saveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.saveButton];
    [self.saveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.tianxieLabel);
        make.right.mas_equalTo(self.bgView).mas_offset(-14);
        make.size.mas_equalTo(CGSizeMake(40, 25));
    }];
    
    self.yuanjiaLabel = [[UILabel alloc] init];
    self.yuanjiaLabel.font = DHFont(14);
    self.yuanjiaLabel.text =@"原价:";
    self.yuanjiaLabel.textColor = FWTextColor_222222;
    self.yuanjiaLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.yuanjiaLabel];
    [self.yuanjiaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.tianxieLabel);
        make.top.mas_equalTo(self.tianxieLabel.mas_bottom).mas_equalTo(30);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(40);
    }];
    
    self.yuanjiaTextView = [[IQTextView alloc] init];
    self.yuanjiaTextView.textContainerInset = UIEdgeInsetsMake(8,5,0,0);
    self.yuanjiaTextView.delegate = self;
    self.yuanjiaTextView.placeholder = @"单位（元）";
    self.yuanjiaTextView.layer.cornerRadius = 2;
    self.yuanjiaTextView.layer.masksToBounds = YES;
    self.yuanjiaTextView.keyboardType = UIKeyboardTypeNumberPad;
    self.yuanjiaTextView.textColor = FWTextColor_222222;
    self.yuanjiaTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.yuanjiaTextView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.bgView addSubview:self.yuanjiaTextView];
    [self.yuanjiaTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.yuanjiaLabel.mas_right).mas_offset(5);
        make.right.mas_equalTo(self.bgView).mas_offset(-13);
        make.height.mas_equalTo(35);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(self.yuanjiaLabel);
    }];
    
    self.rushouLabel = [[UILabel alloc] init];
    self.rushouLabel.font = DHFont(14);
    self.rushouLabel.text =@"出手价:";
    self.rushouLabel.textColor = FWTextColor_222222;
    self.rushouLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.rushouLabel];
    [self.rushouLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.tianxieLabel);
        make.top.mas_equalTo(self.yuanjiaLabel.mas_bottom).mas_equalTo(20);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(40);
    }];
    
    self.rushouTextView = [[IQTextView alloc] init];
    self.rushouTextView.textContainerInset = UIEdgeInsetsMake(8,5,0,0);
    self.rushouTextView.delegate = self;
    self.rushouTextView.placeholder = @"单位（元）";
    self.rushouTextView.layer.cornerRadius = 2;
    self.rushouTextView.layer.masksToBounds = YES;
    self.rushouTextView.keyboardType = UIKeyboardTypeNumberPad;
    self.rushouTextView.textColor = FWTextColor_222222;
    self.rushouTextView.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.rushouTextView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.bgView addSubview:self.rushouTextView];
    [self.rushouTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.rushouLabel.mas_right).mas_offset(5);
        make.right.mas_equalTo(self.bgView).mas_offset(-13);
        make.height.mas_equalTo(35);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(self.rushouLabel);
    }];
    
    self.yunfeiLabel = [[UILabel alloc] init];
    self.yunfeiLabel.font = DHFont(14);
    self.yunfeiLabel.text =@"运费:";
    self.yunfeiLabel.textColor = FWTextColor_222222;
    self.yunfeiLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.yunfeiLabel];
    [self.yunfeiLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.tianxieLabel);
        make.top.mas_equalTo(self.rushouLabel.mas_bottom).mas_equalTo(20);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(40);
    }];
    
    self.juliButton = [[UIButton alloc] init];
    self.juliButton.titleLabel.font = DHFont(14);
    self.juliButton.selected = YES;
    [self.juliButton setTitle:@" 按距离计算" forState:UIControlStateNormal];
    [self.juliButton setImage:[UIImage imageNamed:@"new_select"] forState:UIControlStateNormal];
    [self.juliButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.juliButton addTarget:self action:@selector(juliButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.juliButton];
    [self.juliButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.yunfeiLabel);
        make.left.mas_equalTo(self.yunfeiLabel.mas_right).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    self.baoyouButton = [[UIButton alloc] init];
    self.baoyouButton.titleLabel.font = DHFont(14);
    [self.baoyouButton setTitle:@" 包邮" forState:UIControlStateNormal];
    [self.baoyouButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
    [self.baoyouButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.baoyouButton addTarget:self action:@selector(baoyouButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.baoyouButton];
    [self.baoyouButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.yunfeiLabel);
        make.left.mas_equalTo(self.juliButton.mas_right).mas_offset(30);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    /*
     
     NSDictionary * dict = @{
         @"yuanjia":self.yuanjiaTextView.text,
         @"chushoujia":self.rushouTextView.text,
         @"baoyou":baoyou,
     };
     */
    
    if (self.priceDictionary.allKeys.count > 0) {
        NSArray * keys = self.priceDictionary.allKeys;
        
        for (int i = 0; i<keys.count; i++) {
            if ([keys[i] isEqualToString:@"yuanjia"] &&
                [keys[i] length] > 0) {
                self.yuanjiaTextView.textColor = FWTextColor_222222;
                self.yuanjiaTextView.text = self.priceDictionary[@"yuanjia"];
            }else if ([keys[i] isEqualToString:@"chushoujia"] &&
                      [keys[i] length] > 0) {
                self.rushouTextView.textColor = FWTextColor_222222;
                self.rushouTextView.text = self.priceDictionary[@"chushoujia"];
            }else if ([keys[i] isEqualToString:@"baoyou"]) {
                
                if([self.priceDictionary[@"baoyou"] isEqualToString:@"包邮"]) {
                    
                    self.isBaoyou = YES;
                    [self.juliButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
                    [self.baoyouButton setImage:[UIImage imageNamed:@"new_select"] forState:UIControlStateNormal];
                }else{
                    self.isBaoyou = NO;
                    [self.juliButton setImage:[UIImage imageNamed:@"new_select"] forState:UIControlStateNormal];
                    [self.baoyouButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
                }
            }
        }
    }
}

#pragma mark - > 按距离计算
- (void)juliButtonClick{
    
    if (self.isBaoyou) {
        self.isBaoyou = NO;
        
        [self.juliButton setImage:[UIImage imageNamed:@"new_select"] forState:UIControlStateNormal];
        [self.baoyouButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
    }
}

#pragma mark - > 包邮
- (void)baoyouButtonClick{
    
    if (!self.isBaoyou) {
        self.isBaoyou = YES;
        
        [self.juliButton setImage:[UIImage imageNamed:@"new_unselect"] forState:UIControlStateNormal];
        [self.baoyouButton setImage:[UIImage imageNamed:@"new_select"] forState:UIControlStateNormal];
    }
}

#pragma mark - > 保存
- (void)saveButtonClick{
    
    if (self.yuanjiaTextView.text.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入原价" toController:self];
        return;
    }
    
    if ([self.yuanjiaTextView.text intValue] <= 0 ||
        [self.rushouTextView.text intValue] <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入正确的价格" toController:self];
        return;
    }

    if (self.rushouTextView.text.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入出手价" toController:self];
        return;
    }
    
    NSString * baoyou = @"按距离计算";
    if (self.isBaoyou) {
        baoyou = @"包邮";
    }
    
    NSDictionary * dict = @{
        @"yuanjia":self.yuanjiaTextView.text,
        @"chushoujia":self.rushouTextView.text,
        @"baoyou":baoyou,
    };
    
    if (self.priceBlock) {
        self.priceBlock(dict);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
