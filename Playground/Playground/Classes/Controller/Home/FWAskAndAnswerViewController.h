//
//  FWAskAndAnswerViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWAskAndAnswerView.h"
#import "FWSearchTagsListModel.h"
#import "FWAskAndAnswerModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWAskAndAnswerViewController : FWBaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FWBaseCollectionLayoutDalegate,FWAskAndAnswerViewDelegate>

@property (nonatomic, strong) FWAskAndAnswerView * askAnswerView;

/* 承载collectionView的背景Scrollview */
@property (nonatomic, strong) UIScrollView * collectionScr;

/* 全部列表 */
@property (nonatomic, strong) FWCollectionView * allCollectionView;

@property (nonatomic, strong) NSString * order_type;
@property (nonatomic, strong) NSString * car_type;

@property (nonatomic, strong) NSString * tags_id;
@property (nonatomic, strong) NSString * category_id;
@property (nonatomic, strong) NSString * category_name;

@property(nonatomic, strong) FWBaseCollectionLayout *allLayout;


@end

NS_ASSUME_NONNULL_END
