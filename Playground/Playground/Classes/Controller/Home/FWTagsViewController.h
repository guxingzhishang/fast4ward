//
//  FWTagsViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWTagsView.h"
#import "FWSearchTagsListModel.h"

typedef NS_ENUM(NSUInteger, FWTagsCollectionViewType) {
    FWTagsAllCollectionType = 1,// 所有列表
    FWTagsVideoCollectionType,  // 视频列表
    FWTagsPicsCollectionType,   // 图片列表
};

@interface FWTagsViewController : FWBaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FWBaseCollectionLayoutDalegate,FWTagsViewDelegate>

@property (nonatomic, strong) FWTagsView * tagsView;

/* 承载collectionView的背景Scrollview */
@property (nonatomic, strong) UIScrollView * collectionScr;

/* 全部列表 */
@property (nonatomic, strong) FWCollectionView * allCollectionView;

/* 视频列表 */
@property (nonatomic, strong) FWCollectionView * videoCollectionView;

/* 图片列表 */
@property (nonatomic, strong) FWCollectionView * picsCollectionView;

/* 参与活动 */
@property (nonatomic, strong) UIButton * participateButton;

/* 标记是哪一个列表 */
@property (nonatomic, assign) FWTagsCollectionViewType collectionType;

/* 上一页面传来的id */
@property (nonatomic, strong) FWSearchTagsSubListModel * tagsModel;

@property (nonatomic, strong) NSString * order_type;

@property (nonatomic, strong) NSString * tags_id;
@property (nonatomic, strong) NSString * tags_name;

@property(nonatomic, strong) FWBaseCollectionLayout *allLayout;
@property(nonatomic, strong) FWBaseCollectionLayout *videoLayout;
@property(nonatomic, strong) FWBaseCollectionLayout *picsLayout;

/* 用作TD事件分析 1：发现页跳转 2：帖子页跳转 3：搜索推荐页跳转 4：搜索结果页跳转 */
@property (nonatomic, assign) NSInteger traceType;


@end
