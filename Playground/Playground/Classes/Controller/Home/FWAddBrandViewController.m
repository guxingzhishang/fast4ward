//
//  FWAddBrandViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/10.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWAddBrandViewController.h"

@interface FWAddBrandViewController ()

@end

@implementation FWAddBrandViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = FWColorWihtAlpha(@"000000", 0.6);
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonClick)];
    [self.view addGestureRecognizer:tap];
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.centerView = [[UIView alloc] init];
    self.centerView.layer.cornerRadius = 2;
    self.centerView.layer.masksToBounds = YES;
    self.centerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.centerView];
    self.centerView.frame = CGRectMake(47, SCREEN_HEIGHT/2-77-10, SCREEN_WIDTH-94, 155);
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"品牌名称";
    self.titleLabel.font = DHBoldFont(14);
    self.titleLabel.textColor = FWTextColor_222222;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.centerView addSubview:self.titleLabel];
    self.titleLabel.frame  =CGRectMake(0, 0, CGRectGetWidth(self.centerView.frame), 44);
    
    self.closeButton = [[UIButton alloc] init];
    [self.closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.centerView addSubview:self.closeButton];
    [self.closeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.centerView);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.centerY.mas_equalTo(self.titleLabel);
    }];
    
    self.shuruTF = [[UITextField alloc] init];
    self.shuruTF.delegate = self;
    self.shuruTF.placeholder = @" 请输入品牌名称";
    self.shuruTF.font = DHFont(14);
    self.shuruTF.textColor = FWTextColor_222222;
    self.shuruTF.layer.borderColor = FWTextColor_BCBCBC.CGColor;
    self.shuruTF.layer.borderWidth = 1;
    self.shuruTF.layer.cornerRadius = 2;
    self.shuruTF.layer.masksToBounds = YES;
    self.shuruTF.frame = CGRectMake(14, CGRectGetMaxY(self.titleLabel.frame)+5, CGRectGetWidth(self.centerView.frame)-28, 40);
    [self.centerView addSubview:self.shuruTF];
    
    self.saveButton = [[UIButton alloc] init];
    self.saveButton.layer.borderColor = FWTextColor_BCBCBC.CGColor;
    self.saveButton.layer.borderWidth = 1;
    self.saveButton.layer.cornerRadius = 2;
    self.saveButton.layer.masksToBounds = YES;
    self.saveButton.titleLabel.font = DHFont(14);
    [self.saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [self.saveButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [self.saveButton addTarget:self action:@selector(saveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.centerView addSubview:self.saveButton];
    self.saveButton.frame = CGRectMake(14, CGRectGetHeight(self.centerView.frame)-50, CGRectGetWidth(self.centerView.frame)-28, 36);
}

#pragma mark - > 输入
- (void)saveButtonClick{
    
    [self.shuruTF resignFirstResponder];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.shuruTF.text.length > 0) {
            self.brandBlock(self.shuruTF.text);
        }
        
        for (UIView * view in self.view.subviews) {
            [view removeFromSuperview];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)closeButtonClick{
    
    for (UIView * view in self.view.subviews) {
        [view removeFromSuperview];
    }
    self.brandBlock(@"");
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
