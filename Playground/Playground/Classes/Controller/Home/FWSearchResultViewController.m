//
//  FWSearchResultViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/20.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWSearchResultViewController.h"
#import "FWFollowModel.h"

#import "FWSearchUserModel.h"
#import "FWSearchUserCell.h"

#import "FWBaseCollectionLayout.h"
#import "FWCollectionBaseModel.h"
#import "FWCollecitonBaseCell.h"
#import "FWCollectionView.h"
#import "FWSearchRequest.h"
#import "FWAttionCollectionCell.h"
#import "FWSearchResultCollectionCell.h"
#import "FWIdleDetailViewController.h"
#import "FWIdleCollectionCell.h"

@interface FWSearchResultViewController ()<FWSearchResultNavigationViewDelegate,UITableViewDelegate,UITableViewDataSource,FWSearchUserCellDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FWBaseCollectionLayoutDalegate,FWSegementScrollViewDelegate,FWCollectionViewEmptyButtonDelegate>

@property (nonatomic, weak) UIViewController * currentVC;

@property (nonatomic, strong) NSMutableArray * userList;
@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, strong) FWSearchUserModel * userModel;

@property (nonatomic, strong) FWFeedModel * resultFeedModel;
@property (nonatomic, strong) NSMutableArray * resultDataSource;
@property (nonatomic, assign) NSInteger   resultPageNum;

@property (nonatomic, strong) FWFeedModel * idleFeedModel;
@property (nonatomic, strong) NSMutableArray * idleDataSource;
@property (nonatomic, assign) NSInteger   idlePageNum;

@property (nonatomic, assign) BOOL isIdleFirstRequest;


@end

static NSString * const resultCellId = @"resultCellId";
static NSString * const idleCellId = @"idleCellId";

@implementation FWSearchResultViewController
@synthesize currentVC;
@synthesize infoTableView;
@synthesize pageNum;
@synthesize userModel;
@synthesize mainScrollView;
@synthesize resultFeedModel;
@synthesize resultDataSource;
@synthesize resultPageNum;
@synthesize idleFeedModel;
@synthesize idleDataSource;
@synthesize idlePageNum;
@synthesize resultCollectionView;
@synthesize idleCollectionView;
@synthesize segement;
@synthesize tagsListArray;
@synthesize isRefresh;

- (NSMutableArray *)userList{
    if (!_userList) {
        _userList = [[NSMutableArray alloc] init];
    }
    
    return _userList;
}

#pragma mark - > 请求搜索的用户列表
- (void)requestFollowsListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        [self.infoTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];

        if (self.userList.count > 0 ) {
            [self.userList removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"query":self.query,
                              @"page":@(pageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_search_users  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            userModel = [FWSearchUserModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }
            
            NSMutableArray * tempArr = userModel.user_list;
            
            [self.userList addObjectsFromArray:tempArr];
            
            if([tempArr count] == 0 && pageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求综合列表
- (void)requestResultListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    FWSearchRequest * request = [[FWSearchRequest alloc] init];
    if (isLoadMoredData == NO) {
        resultPageNum = 1;
        
        [self.resultCollectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];

        if (self.resultDataSource.count > 0 ) {
            [self.resultDataSource removeAllObjects];
        }
    }else{
        resultPageNum +=1;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"query":self.query,
                              @"feed_type":self.feed_type,
                              @"page":@(resultPageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_search_feeds  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [resultCollectionView.mj_footer endRefreshing];
            }
            
            [self dealWithData:back];
        }else{
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                [[FWHudManager sharedManager] showErrorMessage:@"您还未登录，请先登录才能查看关注哦~" toController:self];
                return ;
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求闲置列表
- (void)requestIdleListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    FWSearchRequest * request = [[FWSearchRequest alloc] init];
    if (isLoadMoredData == NO) {
        
        idlePageNum = 1;
        [self.idleCollectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];

        if (self.idleDataSource.count > 0 ) {
            [self.idleDataSource removeAllObjects];
       }
    }else{
        idlePageNum +=1;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"query":self.query,
                              @"feed_type":self.feed_type,
                              @"page":@(resultPageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_search_feeds  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [idleCollectionView.mj_footer endRefreshing];
            }
            
            [self dealWithData:back];
        }else{
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                [[FWHudManager sharedManager] showErrorMessage:@"您还未登录，请先登录才能查看关注哦~" toController:self];
                return ;
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

/* 处理数据 */
- (void)dealWithData:(NSDictionary *)back{
    
    if ([self.feed_type isEqualToString:@"0"]) {
        
        resultFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
        
        if (resultFeedModel.tag_list.count > 0 ) {
            resultCollectionView.frame = CGRectMake(0,CGRectGetMaxY(segement.frame), SCREEN_WIDTH, CGRectGetHeight(self.mainScrollView.frame)-CGRectGetMaxY(segement.frame));
            
            if (self.tagsListArray.count <= 0) {
                
                self.tagsListArray = resultFeedModel.tag_list;
                [segement deliverTitles:tagsListArray];
            }
        }else{
            
            resultCollectionView.frame = CGRectMake(0,0, SCREEN_WIDTH,CGRectGetHeight(self.mainScrollView.frame));
        }
        
        [self.resultDataSource addObjectsFromArray: resultFeedModel.feed_list];
        
        if([resultFeedModel.feed_list count] == 0 &&
           resultPageNum != 1){
            [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
        }else{
            if (self.currentButton == 1) {
                [self.resultCollectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            }else{
                [self.infoTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            }
            
            [resultCollectionView reloadData];
        }
    }else{
        idleFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
        
        [self.idleDataSource addObjectsFromArray: idleFeedModel.feed_list];
        
        if([idleFeedModel.feed_list count] == 0 &&
           idlePageNum != 1){
            [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
        }else{
            [self.idleCollectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            [idleCollectionView reloadData];
        }
    }
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;

    isRefresh = YES;
    self.isIdleFirstRequest = YES;
    
    pageNum = 0;
    resultPageNum = 0;
    idlePageNum = 0;

    self.currentButton = 1;
    self.feed_type = @"0";
    
    self.tagsListArray = @[].mutableCopy;
    self.resultDataSource = @[].mutableCopy;
    self.idleDataSource = @[].mutableCopy;

    self.view.backgroundColor = FWTextColor_FAFAFA;
    
    [self setupSubViews];
    
    [self requestResultListWithLoadMoredData:NO];
    [self requestFollowsListWithLoadMoredData:NO];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"搜索结果页"];

    self.fd_prefersNavigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"搜索结果页"];
}

#pragma mark - > 视图初始化
- (void)setupSubViews{
    
    self.navigationView = [[FWSearchResultNavigationView alloc] init];
    self.navigationView.resultSearchBar.text = self.query;
    self.navigationView.delegate = self;
    [self.view addSubview:self.navigationView];
    self.navigationView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 55+40+20+FWCustomeSafeTop);
    
    self.mainScrollView = [[UIScrollView alloc] init];
    self.mainScrollView.delegate = self;
    self.mainScrollView.backgroundColor = FWTextColor_FAFAFA;
    self.mainScrollView.scrollEnabled = YES;
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.mainScrollView];
    self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.navigationView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetHeight(self.navigationView.frame));
    self.mainScrollView.contentSize = CGSizeMake(3 *SCREEN_WIDTH, 0);

    segement = [[FWSegementScrollView alloc]init];
    segement.backgroundColor = FWTextColor_FAFAFA;
    segement.itemDelegate = self;
    segement.fromHome = 2;
    segement.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
    [self.mainScrollView addSubview:self.segement];
    
    self.resultLayout = [[FWBaseCollectionLayout alloc] init];
    self.resultLayout.columns = 2;
    self.resultLayout.rowMargin = 12;
    self.resultLayout.colMargin = 12;
    self.resultLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.resultLayout.delegate = self;
    [self.resultLayout autuContentSize];
    
    resultCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(segement.frame), SCREEN_WIDTH, SCREEN_HEIGHT-49-FWSafeBottom-64-FWCustomeSafeTop - CGRectGetHeight(segement.frame)) collectionViewLayout:self.resultLayout];        resultCollectionView.dataSource = self;
    resultCollectionView.delegate = self;
    resultCollectionView.backgroundColor = FWTextColor_FAFAFA;
    resultCollectionView.isNeedEmptyPlaceHolder = YES;
    NSString * attetionTitle = @"没有找到相关内容，换个词试试吧";
    NSDictionary *attetionAttributes = @{
                                         NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0f],
                                         NSForegroundColorAttributeName:[UIColor colorWithHexString:@"969696"]};
    NSMutableAttributedString * resultString = [[NSMutableAttributedString alloc] initWithString:attetionTitle attributes:attetionAttributes];
    resultCollectionView.emptyDescriptionString = resultString;
    resultCollectionView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_F0F0F0;
    [resultCollectionView registerClass:[FWSearchResultCollectionCell class] forCellWithReuseIdentifier:resultCellId];
    [self.mainScrollView addSubview:resultCollectionView];
    
    @weakify(self);
    resultCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestResultListWithLoadMoredData:YES];
    }];
    
    /* 右侧列表 */
    infoTableView = [[FWTableView alloc] init];
    infoTableView.rowHeight = 80;
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.backgroundColor = FWTextColor_FAFAFA;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainScrollView addSubview:infoTableView];
    infoTableView.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, CGRectGetHeight(self.mainScrollView.frame));
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * infoTitle = @"没有找到相关用户，换个词试试吧";
    NSDictionary *infoAttributes = @{
                                         NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0f],
                                         NSForegroundColorAttributeName:[UIColor colorWithHexString:@"969696"]};
    NSMutableAttributedString * infoString = [[NSMutableAttributedString alloc] initWithString:infoTitle attributes:infoAttributes];
    infoTableView.emptyDescriptionString = infoString;
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestFollowsListWithLoadMoredData:YES];
    }];
    
    self.idleLayout = [[FWBaseCollectionLayout alloc] init];
    self.idleLayout.columns = 2;
    self.idleLayout.rowMargin = 12;
    self.idleLayout.colMargin = 12;
    self.idleLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.idleLayout.delegate = self;
    [self.idleLayout autuContentSize];
    
    idleCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*2,0, SCREEN_WIDTH, CGRectGetHeight(self.mainScrollView.frame)) collectionViewLayout:self.idleLayout];
    idleCollectionView.dataSource = self;
    idleCollectionView.delegate = self;
    idleCollectionView.backgroundColor = FWTextColor_FAFAFA;
    idleCollectionView.isNeedEmptyPlaceHolder = YES;
    NSString * idleAttetionTitle = @"没有找到相关内容，换个词试试吧";
    NSDictionary *idleAttetionAttributes = @{
                                         NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0f],
                                         NSForegroundColorAttributeName:[UIColor colorWithHexString:@"969696"]};
    NSMutableAttributedString * idleResultString = [[NSMutableAttributedString alloc] initWithString:idleAttetionTitle attributes:idleAttetionAttributes];
    idleCollectionView.emptyDescriptionString = idleResultString;
    idleCollectionView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_F0F0F0;
    [idleCollectionView registerClass:[FWIdleCollectionCell class] forCellWithReuseIdentifier:idleCellId];
    [self.mainScrollView addSubview:idleCollectionView];
    
    idleCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestIdleListWithLoadMoredData:YES];
    }];
}

#pragma mark - > 返回
- (void)backClick{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - > 搜索
- (void)searchResultWithText:(NSString *)content{
    
    if (content.length <= 0) {
        return;
    }
        
    self.query = content;
    
    if (self.tagsListArray.count > 0) {
        [self.tagsListArray removeAllObjects];
        [self.segement clearSubViews];
    }
    
    if ([self.feed_type isEqualToString:@"0"]) {
        [self requestResultListWithLoadMoredData:NO];
        [self requestFollowsListWithLoadMoredData:NO];
    }else if ([self.feed_type isEqualToString:@"6"]){
        [self requestIdleListWithLoadMoredData:NO];
    }
}

#pragma mark - > 保存历史记录
- (void)saveHistoryWithContent:(NSDictionary *)keywordDict{
    
    //如果是空格，就不存储
    NSString * tempKeyword = keywordDict[@"keyword"];
    tempKeyword = [tempKeyword stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (tempKeyword.length <= 0) {
        return;
    }
    /* 如果有，则取出历史标签，
     加入新元素后，插在第0个位置
     如果大于10个，将最后一个删除 */
    NSData * data = [GFStaticData getObjectForKey:Search_History_Array];
    NSMutableArray *tagsArray = @[].mutableCopy;
    NSMutableArray * tempArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString * keyword = keywordDict[@"keyword"];
    if (tempArray.count > 0) {
        
        NSInteger index = -1;
        for (int i =0; i< tempArray.count; i++) {
            
            NSDictionary * dict = tempArray[i];
            NSString * keyWord = dict[@"keyword"];
            
            if ([keyWord isEqualToString:keyword]) {
                index = i;
                break;
            }
        }
        /* 如果有相同id存在的标签，先删除原标签，再重新加入
         始终让每次查询的在最前面 */
        tagsArray =  tempArray;
        
        if (index >= 0) {
            [tagsArray removeObjectAtIndex:index];
        }
        [tagsArray insertObject:keywordDict atIndex:0];
    }else{
        [tagsArray addObject:keywordDict];
    }
    
    if (tagsArray.count>10) {
        [tagsArray removeLastObject];
    }
    
    data = [NSKeyedArchiver archivedDataWithRootObject:tagsArray];
    [GFStaticData saveObject:data forKey:Search_History_Array];
}

- (void)selectClearButton{
    
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - > ********************** 左边列表 **********************
#pragma mark - > 点击item跳转至标签页
- (void)segementItemClickTap:(NSInteger)index{
    
    FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
    TVC.traceType = 4;
    TVC.tags_name = ((FWTagModel *)self.resultFeedModel.tag_list[index]).tag_name;
    TVC.tags_id = ((FWTagModel *)self.resultFeedModel.tag_list[index]).tag_id;
    [self.navigationController pushViewController:TVC animated:YES];
}

#pragma mark - > collectionview delegate & dataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (collectionView == self.resultCollectionView) {
        return self.resultDataSource.count?self.resultDataSource.count:0;
    }else{
        return self.idleDataSource.count?self.idleDataSource.count:0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == self.resultCollectionView) {
            
        FWSearchResultCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:resultCellId forIndexPath:indexPath];
        cell.viewController = self;

        if (self.resultDataSource.count >0) {
            
            FWFeedListModel * model = self.resultDataSource[indexPath.item];
            [cell configForCell:model];

            if([model.user_info.cert_status isEqualToString:@"2"]) {
                
                cell.username.textColor = FWTextColor_FFAF3C;
                cell.authenticationImageView.hidden = NO;
                cell.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
            }else{
                if ([model.user_info.merchant_cert_status isEqualToString:@"3"] &&
                    ![model.user_info.cert_status isEqualToString:@"2"]){
                    cell.username.textColor = FWTextColor_2B98FA;
                    cell.authenticationImageView.hidden = YES;
                }else{
                    cell.username.textColor = FWTextColor_000000;
                }
            }
            
            if (model.h5_url.length > 0) {
                
                cell.activityImageView.hidden = NO;
                
                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);
                
                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }
        
        return cell;
    }else{

        FWIdleCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:idleCellId forIndexPath:indexPath];
        cell.viewController = self;

        if (self.idleDataSource.count >0) {

            FWFeedListModel * model = self.idleDataSource[indexPath.item];
            [cell configForCell:model];

            if([model.user_info.cert_status isEqualToString:@"2"]) {

                cell.username.textColor = FWTextColor_FFAF3C;
                cell.authenticationImageView.hidden = NO;
                cell.authenticationImageView.image = [UIImage imageNamed:@"middle_bussiness"];
            }else{
                if ([model.user_info.merchant_cert_status isEqualToString:@"3"] &&
                    ![model.user_info.cert_status isEqualToString:@"2"]){
                    cell.username.textColor = FWTextColor_2B98FA;
                    cell.authenticationImageView.hidden = YES;
                }else{
                    cell.username.textColor = FWTextColor_000000;
                }
            }

            if (model.h5_url.length > 0) {

                cell.activityImageView.hidden = NO;

                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);

                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }

        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr;
    
    if ([self.feed_type isEqualToString:@"0"]) {
        tempArr = self.resultDataSource;
    }else{
        tempArr = self.idleDataSource;
    }
    
    FWFeedListModel * model = tempArr[indexPath.row];
    NSMutableArray * tempDataSource = @[].mutableCopy;
    
    NSInteger requestType = FWSearchResultListRequestType;
    
    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }
    
    if ([model.feed_type isEqualToString:@"1"]) {
        
        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            DHWeakSelf;
            
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.listModel = model;
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {
                if ([weakSelf.feed_type isEqualToString:@"0"]) {
                    weakSelf.resultDataSource[indexPath.row] = listModel;
                }else{
                    weakSelf.idleDataSource[indexPath.row] = listModel;
                }
            };
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            for (NSInteger i = indexPath.row; i<tempArr.count; i++) {
                FWFeedListModel * model = tempArr[i];
                
                if ([model.feed_type isEqualToString:@"1"]) {
                    [tempDataSource addObject:model];
                }
            }
            
            // 视频流 由于筛选过，所以每次点击的都是数据源第一个。
            FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
            controller.currentIndex = 0;
            controller.listModel = tempDataSource;
            controller.requestType = requestType;
            controller.flag_return = @"2";
            controller.feed_id = model.feed_id;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        for (NSInteger i = indexPath.row; i<tempArr.count; i++) {
            FWFeedListModel * model = tempArr[i];
            
            if ([model.feed_type isEqualToString:@"2"]) {
                [tempDataSource addObject:model];
            }
        }
        
        // 图文详情
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.listModel = model;
        TVC.feed_id = model.feed_id;
        TVC.myBlock = ^(FWFeedListModel *awemeModel) {};
        [self.navigationController pushViewController:TVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"4"]) {
        // 问答
        FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        // 改装
        FWRefitCaseDetailViewController * RCDVC = [[FWRefitCaseDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"6"]) {
        // 闲置
        FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }
}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr;
    
    if ([self.feed_type isEqualToString:@"0"]) {
        tempArr = self.resultDataSource;
    }else{
        tempArr = self.idleDataSource;
    }
    
    if (indexPath.row >= tempArr.count) {
        return 0;
    }
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"2"]) {
        // 图文贴 固定比例3：4
        height = (cover_width *4)/3;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if([model.feed_type isEqualToString:@"3"]){
        // 文章帖 固定比例5：4
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if ([model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 20;
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 25;
        }
    }else if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}
#pragma mark - > ********************** 左边列表 **********************

#pragma mark - > ********************** 用户列表 **********************
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"searchUserCellID";
    
    FWSearchUserCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWSearchUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.delegate = self;
    cell.followButton.tag = 6000+indexPath.row;
    if (self.userList.count > 0) {
        [cell cellConfigureFor:self.userList[indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    FWMineInfoModel * userModel = self.userList[indexPath.row];
    
    if (nil == userModel.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = userModel.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 关注
- (void)followButtonForIndex:(NSInteger)index withCell:(FWSearchUserCell *)cell {
    
    NSString * action ;
    
    FWMineInfoModel * listModel = self.userList[index];
  
    if ([listModel.follow_status isEqualToString:@"2"]) {
        action = Submit_follow_users;
    }else{
        action = Submit_cancel_follow_users;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":listModel.uid,
                              };
    
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([action isEqualToString: Submit_follow_users]) {
                
                listModel.follow_status_str = [[back objectForKey:@"data"] objectForKey:@"follow_status_str"];
                listModel.follow_status = [[back objectForKey:@"data"] objectForKey:@"follow_status"];
                
                [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self];
            }else{
                listModel.follow_status = @"2";
            }
            [cell cellConfigureFor:listModel];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > ********************** 用户列表 **********************

- (void)selectButtonClick:(UIButton *)sender{
    
    if (sender == self.navigationView.comprehensiveButton) {
        self.currentButton = 1;
        self.feed_type = @"0";
        
        self.navigationView.comprehensiveButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.navigationView.userButton.backgroundColor = FWViewBackgroundColor_F3F3F3;
        self.navigationView.idleButton.backgroundColor = FWViewBackgroundColor_F3F3F3;

        [self.navigationView.comprehensiveButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
        [self.navigationView.userButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
        [self.navigationView.idleButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
        
    }else if (sender == self.navigationView.userButton){
        self.currentButton = 2;
        self.feed_type = @"0";

        self.navigationView.comprehensiveButton.backgroundColor = FWViewBackgroundColor_F3F3F3;
        self.navigationView.userButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
        self.navigationView.idleButton.backgroundColor = FWViewBackgroundColor_F3F3F3;

        [self.navigationView.comprehensiveButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
        [self.navigationView.userButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
        [self.navigationView.idleButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
    }else if (sender == self.navigationView.idleButton){
        self.currentButton = 3;
        self.feed_type = @"6";

        self.navigationView.comprehensiveButton.backgroundColor = FWViewBackgroundColor_F3F3F3;
        self.navigationView.userButton.backgroundColor = FWViewBackgroundColor_F3F3F3;
        self.navigationView.idleButton.backgroundColor = FWViewBackgroundColor_FFFFFF;

        [self.navigationView.comprehensiveButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
        [self.navigationView.userButton setTitleColor:FWTextColor_A6A6A6 forState:UIControlStateNormal];
        [self.navigationView.idleButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
        
        if (self.isIdleFirstRequest) {
            self.isIdleFirstRequest = NO;
            [self requestIdleListWithLoadMoredData:NO];
        }
    }
    
    //点击btn的时候scrollowView的contentSize发生变化
    [self.mainScrollView setContentOffset:CGPointMake((self.currentButton-1) *SCREEN_WIDTH, 0) animated:YES];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self.view endEditing:YES];
    
    if (self.mainScrollView == scrollView){
        
        float scrollViewX = scrollView.contentOffset.x;
        if (scrollViewX == 0) {
            [self selectButtonClick:self.navigationView.comprehensiveButton];
        }else if (scrollViewX == SCREEN_WIDTH){
            [self selectButtonClick:self.navigationView.userButton];
        }else if (scrollViewX == 2*SCREEN_WIDTH){
            [self selectButtonClick:self.navigationView.idleButton];
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}
@end
