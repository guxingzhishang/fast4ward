//
//  FWHomeBussinessViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeBussinessViewController.h"
#import "FWHomeBussinessCell.h"
#import "FWPickerTypeView.h"
#import "FWShopInfoDetailViewController.h"

@interface FWHomeBussinessViewController ()<UITableViewDelegate,UITableViewDataSource,SelectPickerTypeDelegate>

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) UIButton * selectButton;
@property (nonatomic, strong) UIImageView * arrowImageView;
@property (nonatomic, strong) FWPickerTypeView * pickerView;
@property (nonatomic, assign) NSInteger firstSelect;

@property (nonatomic, strong) NSMutableArray * bussinessDataSource;
@property (nonatomic, assign) NSInteger  bussinessPageNum;

@property (nonatomic, strong) NSString * city_id;
@property (nonatomic, strong) FWHomeModel * bussinessModel;
@end

@implementation FWHomeBussinessViewController
@synthesize infoTableView;
@synthesize selectButton;
@synthesize arrowImageView;
@synthesize bussinessPageNum;
@synthesize bussinessDataSource;
@synthesize bussinessModel;

#pragma mark - > 获取店铺列表
- (void)requestBussinessList:(BOOL)isLoadMoredData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    if (isLoadMoredData == NO) {
        bussinessPageNum = 1;
        
        if (self.bussinessDataSource.count > 0 ) {
            [self.bussinessDataSource removeAllObjects];
        }
    }else{
        bussinessPageNum +=1;
    }
    
    NSDictionary * params  = @{
                               @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                               @"page":@(bussinessPageNum).stringValue,
                               @"feed_id":@"0",
                               @"page_size":@"20",
                               @"city_id":self.city_id,
                               };
    
    [request startWithParameters:params WithAction:Get_shop_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            bussinessModel = [FWHomeModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.bussinessDataSource addObjectsFromArray: bussinessModel.shop_list];
            
            if([bussinessModel.shop_list count] == 0 &&
               bussinessPageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                [[FWHudManager sharedManager] showErrorMessage:@"您还未登录，请先登录才能查看关注哦~" toController:self];
                return ;
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"商家页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"商家页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"商家列表";
    
    self.city_id = @"-1";
    bussinessPageNum = -1;
    bussinessDataSource = @[].mutableCopy;
    
    
//    [self setupPickerView];
    [self setupSubViews];
    [self requestBussinessList:NO];
}

- (void)setupPickerView{
    
    self.pickerView = [[FWPickerTypeView alloc] init];
    self.pickerView.pickerType = FWSignlePickerViewType;
    self.pickerView.delegate = self;
    
    self.pickerView.oneTitleArray = @[@"全部商家"].mutableCopy;
   
    UIView * henLineView = [[UIView alloc] init];
    henLineView.backgroundColor = FWTextColor_B6BCC4;
    [self.view addSubview:henLineView];
    [henLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.top.mas_equalTo(self.view).mas_offset(2);
    }];
    
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWTextColor_F8F8F8;
    [self.view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).mas_offset(-120);
        make.height.mas_equalTo(14);
        make.width.mas_equalTo(1);
        make.top.mas_equalTo(self.view).mas_offset(19);
    }];
    
    selectButton = [[UIButton alloc] init];
    selectButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    selectButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [selectButton setTitle:@"全部商家" forState:UIControlStateNormal];
    [selectButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
    [selectButton addTarget:self action:@selector(selectButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectButton];
    [selectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(lineView);
        make.left.mas_equalTo(lineView.mas_right).mas_offset(10);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(30);
    }];
    
    arrowImageView = [[UIImageView alloc] init];
    arrowImageView.image = [UIImage imageNamed:@"select_up"];
    [self.view addSubview:arrowImageView];
    [arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(lineView);
        make.left.mas_equalTo(selectButton.mas_right).mas_offset(10);
        make.width.mas_equalTo(12);
        make.height.mas_equalTo(9);
    }];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.estimatedRowHeight = 100;
    infoTableView.backgroundColor = FWTextColor_F8F8F8;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view).mas_offset(0);
    }];
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestBussinessList:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestBussinessList:YES];
    }];
}

#pragma mark - > 筛选商家
- (void)selectButtonOnClick{
    
    [self.pickerView show];
    
    arrowImageView.image = [UIImage imageNamed:@"select_down"];
    [self.pickerView.lzPickerView selectRow:self.firstSelect inComponent:0 animated:YES];
}

- (void)cancelShow{
    arrowImageView.image = [UIImage imageNamed:@"select_up"];
}

- (void)FWPickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row component:(NSInteger)component{
    
    if (component == 0) {
        
    }
}

- (void)selectType:(NSString *)selectTitle withFirstRow:(NSInteger)firstRow withSecondRow:(NSInteger)secondRow{

    arrowImageView.image = [UIImage imageNamed:@"select_up"];
    [selectButton setTitle:self.pickerView.oneTitleArray[firstRow] forState:UIControlStateNormal];
    
    self.city_id = self.bussinessModel.shop_city[firstRow].city_id;
    [self requestBussinessList:NO];
}


#pragma mark - > 列表 delegate && dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bussinessDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"HomeBussinessCellID";
    FWHomeBussinessCell * cell =  [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (nil == cell) {
        cell = [[FWHomeBussinessCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.bussinessDataSource.count) {
        cell.vc = self;
        [cell configForCell:self.bussinessDataSource[indexPath.row]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [infoTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.bussinessDataSource.count) {

        FWHomeShopListModel * shopList = self.bussinessDataSource[indexPath.row];
        
        if ([shopList.user_info.cert_status isEqualToString:@"2"]) {
            /* 肆放东家 */
            FWCustomerShopViewController * CSVC = [[FWCustomerShopViewController alloc] init];
            CSVC.user_id = shopList.user_info.uid;
            CSVC.isUserPage = NO;
            [self.navigationController pushViewController:CSVC animated:YES];
        }else{
            FWShopInfoDetailViewController  * UIVC = [[FWShopInfoDetailViewController alloc] init];
            UIVC.user_id = shopList.user_info.uid;
            [self.navigationController pushViewController:UIVC animated:YES];
        }
    }
}
@end
