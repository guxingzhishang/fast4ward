//
//  FWLongVideoPlayerViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWLongVideoPlayerViewController.h"
#import "FWFollowRequest.h"
#import "FWCommentCell.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWCommentMessageRequest.h"
#import "FWLikeRequest.h"
#import "FWCommentModel.h"
#import "SharePopView.h"
#import "FWEmptyView.h"
#import "FWCommentHeaderView.h"
#import "FWCommentDetailViewController.h"

#import "ZFPlayer.h"
#import "ZFAVPlayerManager.h"
#import "ZFIJKPlayerManager.h"
#import "KSMediaPlayerManager.h"
#import "UIImageView+ZFCache.h"
#import "ZFUtilities.h"
#import <AVFoundation/AVFoundation.h>
#import "FWLiveTipView.h"
#import "FWLongVideoPlayerControlView.h"

@interface FWLongVideoPlayerViewController ()<ShareViewDelegate,FWCommentCellDelegate,FWCommentHeaderViewDelegate,FWLiveTipViewDelegate>

@property (nonatomic, assign) CGFloat alphaValue;
@property (nonatomic, strong) UIButton * blackBackButton;
@property (nonatomic, strong) FWCommentModel * commentModel;
@property (nonatomic, strong) NSString * hostComment_id;
/** 选中的话题尺寸模型 */
@property (nonatomic , assign) NSInteger selectedSection;

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) UIImageView *containerView;
@property (nonatomic, strong) FWLongVideoPlayerControlView *controlView;
@property (nonatomic, strong) UIButton *playBtn;
@property (nonatomic, strong) NSArray <NSURL *>*assetURLs;
@property (nonatomic, strong) ZFAVPlayerManager *playerManager;
/* 最近一次的网络状态  wifi 还是 4G */
@property (nonatomic, strong) NSString * lastNetWork;
@property (nonatomic, strong) FWLiveTipView * tipView;

@property (nonatomic, strong) UIButton * closeButton;
@property (nonatomic, assign) CGFloat picHight; // 视频播放器的高度
@property (nonatomic, assign) BOOL  isRePlay;

@end


@implementation FWLongVideoPlayerViewController
@synthesize headerView;
@synthesize alphaValue;
@synthesize blackBackButton;
@synthesize infoTableView;
@synthesize commentView;
@synthesize feed_id;
@synthesize commentModel;
@synthesize pageNum;
@synthesize commentDataSource;
@synthesize picHight;

#pragma mark - > 请求头部个人信息
- (void)requestHeaderInfo{
    
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.feed_id,
                              @"watermark":@"1",
                              };
    
    [request startWithParameters:params WithAction:Get_feed_info_by_id  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.listModel = [FWFeedListModel mj_objectWithKeyValues:[[back objectForKey:@"data"] objectForKey:@"feed_info"]];
            
            if ([self.listModel.flag_hidden isEqualToString:@"2"]) {
                
                CGFloat k = [self.listModel.feed_cover_width floatValue]/[self.listModel.feed_cover_height floatValue];
                
                picHight = SCREEN_WIDTH;
                CGFloat picWidth = SCREEN_WIDTH;

                if (k < 1) {
                    picHight = picWidth;
                    //ZFPlayerScalingModeAspectFit
                }if (k == 1) {
                    picHight = picWidth;
                    //ZFPlayerScalingModeAspectFit
                }else if (k >1 && k < 1.77){
                    picHight = picWidth /k;
                     //ZFPlayerScalingModeAspectFill
                }else if( k >= 1.77 && k<= 1.78){
                    picHight = picWidth * 0.5625;
                    //ZFPlayerScalingModeAspectFill
                }else if( k > 1.78){
                    picHight = picWidth * 0.5625;
                    //ZFPlayerScalingModeAspectFit
                }
                
                self.containerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, picHight);
                self.tipView.frame = CGRectMake(0, 0, SCREEN_WIDTH, picHight);

                [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(self.view);
                    make.top.mas_equalTo(self.containerView.mas_bottom);
                    make.bottom.mas_equalTo(self.view).mas_offset(-50-FWSafeBottom);
                }];
                
                self.assetURLs = @[[NSURL URLWithString:self.listModel.video_info.playURL]];
                self.player.assetURLs = self.assetURLs;
                [self.player playTheIndex:0];
                
                [self configWithModel];
            }else if ([self.listModel.flag_hidden isEqualToString:@"1"]){
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:self.listModel.flag_hidden_msg preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求评论列表
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    if (isLoadMoredData == NO) {
        pageNum = 1;
        
        if (self.commentDataSource.count > 0 ) {
            [self.commentDataSource removeAllObjects];
        }
    }else{
        pageNum +=1;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.feed_id,
                              @"p_comment_id":self.p_comment_id,
                              @"page":@(pageNum).stringValue,
                              @"page_size":@"10",
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_comment_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            [self dealWithData:back];
            
        }else{
            if ([[back objectForKey:@"errno"] isEqualToString:@"-2"] &&
                [[back objectForKey:@"errmsg"] isEqualToString:@"当前帖子状态不正常,没有审核通过"]) {
                return ;
            }
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 处理数据
- (void)dealWithData:(NSDictionary *)back{
    
    commentModel = [FWCommentModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
    
    if (commentModel.comment_list.count > 0) {
        
        infoTableView.tableFooterView = nil;
    }else{
        if (pageNum == 1) {
            
            FWEmptyView * footerView = [[FWEmptyView alloc] init];
            footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 400);
            [footerView settingWithImageName:@"empty" WithTitle:@"还没有相关的内容哦~"];
            infoTableView.tableFooterView = footerView;
        }
    }
    
    self.total_count = commentModel.all_comment_count?commentModel.all_comment_count:@"0";
    
    for (FWCommentListModel * model in commentModel.comment_list) {
        [self.commentDataSource addObject:[self _topicFrameWithTopic:model]];
    }
    
    if([commentModel.comment_list count] == 0 &&
       pageNum != 1){
        [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
    }else{
        [infoTableView reloadData];
    }
}

#pragma mark - > 删除消息
- (void)requestDeleteMessageWithSection:(NSInteger)section WithRow:(NSInteger)row{
    
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.commentModel.feed_id,
                              @"comment_id":self.comment_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_del_comment WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            
            FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)self.commentDataSource[section];
            
            if (row >= 0) {
                [topicFrame.commentFrames removeObjectAtIndex:row];
                [UIView performWithoutAnimation:^{
                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
                    [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                }];
                
                self.total_count = @([self.total_count integerValue] -1).stringValue;
            }else{
                self.total_count = @([self.total_count integerValue] -[topicFrame.topic.reply_total_count integerValue]-1).stringValue;
                
                [self.commentDataSource removeObjectAtIndex:section];
                [self.infoTableView reloadData];
            }
            
            [self showCommentCount:self.total_count];
            
            [[FWHudManager sharedManager] showSuccessMessage:@"删除成功" toController:self];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 显示共多少评论
- (void)showCommentCount:(NSString *)count{
    
    [headerView.commentNumberButton setTitle:self.total_count forState:UIControlStateNormal];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    alphaValue = 0;
    self.pageNum = 0;
    self.p_comment_id = @"0";
    self.selectedSection = -1;
    self.currentRow = -1;
    self.currentSection = -1;
    picHight = 200;
    self.isRePlay = YES;
    
    self.hostComment_id = self.p_comment_id;
    
    self.commentDataSource = @[].mutableCopy;
    self.backDataSource = @[].mutableCopy;
    
    self.title = @"";
    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.containerView];
    
    [self.containerView addSubview:self.playBtn];
    
    [self setupPlayer];
    [self setupTopView];
    
    [self checkWifi];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(monitorLiveNet) name:MonitorLiveNetworking object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayFinished) name:VideoPlayFinished object:nil];

    [self requestHeaderInfo];
    
    [self setupSubViews];
    
    [self requestDataWithLoadMoredData:NO];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"图文详情页"];
    self.player.viewControllerDisappear = NO;

    self.navigationController.navigationBar.alpha = self.alphaValue;
    
    
    if(self.listModel.uid.length > 0 && self.listModel.feed_id.length > 0){
        // 访问记录
        [self requestVisitorWithUserID:self.listModel.uid  WithType:@"2" WithID:self.listModel.feed_id];
    }
    
    if (self.selectedSection>=0) {
        [self _reloadSelectedSectin];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"图文详情页"];
    self.player.viewControllerDisappear = YES;

    self.navigationController.navigationBar.alpha = 1;
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    infoTableView.tag = CommentListTag;
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.containerView.mas_bottom);
        make.bottom.mas_equalTo(self.view).mas_offset(-50-FWSafeBottom);
    }];
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    commentView = [[FWCommentView alloc] init];
    commentView.delegate = self;
    commentView.type = 2;
    commentView.feed_type = @"1";
    [self.view addSubview:commentView];
    [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.view).mas_offset(0);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_greaterThanOrEqualTo(50+FWSafeBottom);
    }];
    
    headerView = [[FWLongVideoPlayerHeaderView alloc] init];
    headerView.vc = self;
    headerView.headerDelegate = self;
    CGFloat height = [headerView getCurrentViewHeight];
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
    self.infoTableView.tableHeaderView = headerView;
    
    //    [self configWithModel];
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:NO];
    }];
}

- (void)configWithModel{
    
    NSString * likeCount = self.listModel.count_realtime.like_count_format?self.listModel.count_realtime.like_count_format:@"0";
    NSString * shareCount = self.listModel.count_realtime.share_count?self.listModel.count_realtime.share_count:@"0";
    
    [commentView.likesButton setTitle:[NSString stringWithFormat:@"   %@",likeCount] forState:UIControlStateNormal];
    [commentView.shareButton setTitle:[NSString stringWithFormat:@"   %@",shareCount] forState:UIControlStateNormal];
    
    if ([self.listModel.is_liked isEqualToString:@"1"]) {
        [commentView.likesButton setImage:[UIImage imageNamed:@"car_detail_like"] forState:UIControlStateNormal];
    }else{
        [commentView.likesButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
    }
    
    [headerView configViewForModel:self.listModel];
    CGFloat height = [headerView getCurrentViewHeight];
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
    self.infoTableView.tableHeaderView = headerView;
}

#pragma mark - > UITableView delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.commentDataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.commentFrames.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id model = self.commentDataSource[indexPath.section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentCell *cell = [FWCommentCell cellWithTableView:tableView WithID:@"CommentID"];
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
        cell.commentFrame = commentFrame;
        
        cell.nameLabel.textColor = FWTextColor_000000;
        cell.likesLabel.textColor = FWTextColor_000000;
//        cell.contentLabel.textColor = FWTextColor_000000;
        
        cell.viewcontroller = self;
        cell.delegate = self;
        
        if (indexPath.row > 2) {
            cell.photoImageView.image = [UIImage imageNamed:@""];
        }
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    
    self.selectedSection = indexPath.section;
    
    id model = self.commentDataSource[indexPath.section];
    
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
    FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
    
    if (indexPath.row == topicFrame.commentFrames.count-1  &&
        [commentFrame.comment.content isEqualToString:MoreReply]) {
        FWCommentDetailViewController * CDVC = [[FWCommentDetailViewController alloc] init];
        CDVC.pageType = @"2";
        CDVC.feed_id = commentFrame.comment.feed_id?commentFrame.comment.feed_id:@"";
        /*就是要取top_comment_id(因为cell最后一行显示的固定文案，
         所以最后一行comment_id赋值了别的) */
        CDVC.p_comment_id = commentFrame.comment.top_comment_id?commentFrame.comment.top_comment_id:@"0";
        CDVC.top_comment_id = commentFrame.comment.top_comment_id?commentFrame.comment.top_comment_id:@"0";
        DHWeakSelf;
        CDVC.returnDataSource = ^(NSMutableArray * _Nonnull backDataSource) {
            weakSelf.backDataSource = backDataSource;
        };
        [self.navigationController pushViewController:CDVC animated:YES];
    }else{
        
        if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:commentFrame.comment.uid]) {
            /* 是本人，提醒删除 */
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.comment_id = commentFrame.comment.comment_id;
                [self requestDeleteMessageWithSection:indexPath.section WithRow:indexPath.row];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }else{
            /* 不是本人，回复 */
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.p_comment_id = topicFrame.topic.comment_id;
                self.reply_comment_id = commentFrame.comment.comment_id;
                
                [self.commentView.contentView becomeFirstResponder];
                self.commentView.contentView.placeholder = [NSString stringWithFormat:@"回复:%@",commentFrame.comment.user_info.nickname];
                /* 回复 */
                self.reply_userInfo = commentFrame.comment.user_info;
                self.currentSection = indexPath.section;
                self.currentRow = indexPath.row;
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentHeaderView *headerView = [FWCommentHeaderView headerViewWithTableView:tableView];
        headerView.commentButton.tag = section +30000;
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        headerView.topicFrame = topicFrame;
        
        headerView.nicknameLable.textColor = FWTextColor_000000;
        headerView.createTimeLabel.textColor = FWTextColor_000000;
        headerView.thumbNumberLabel.textColor = FWTextColor_000000;
        headerView.contentLabel.textColor = FWTextColor_000000;
        
        headerView.viewcontroller = self;
        headerView.delegate = self;
        return headerView;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    if ([self.total_count integerValue] >10 &&
        section == self.commentDataSource.count-1) {
        UIButton * moreButton = [[UIButton alloc] init];
        moreButton.titleLabel.font = DHSystemFontOfSize_14;
        [moreButton setTitle:@"查看更多评论" forState:UIControlStateNormal];
        [moreButton setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
        [moreButton addTarget:self action:@selector(moreButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:moreButton];
        view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
        moreButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetHeight(view.frame));
    }
    
    return view;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id model = self.commentDataSource[indexPath.section];
    if ([model isKindOfClass:[FWTopicCommentFrame class]]) {
        FWTopicCommentFrame *videoTopicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = videoTopicFrame.commentFrames[indexPath.row];
        
        if (indexPath.row == videoTopicFrame.commentFrames.count-1 &&
            self.commentDataSource.count != 0) {
            return commentFrame.cellHeight+10;
        }
        return commentFrame.cellHeight;
    }
    
    return .1f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([self.total_count integerValue] >10 &&
        section == self.commentDataSource.count -1) {
        return 40;
    }
    return 0.5f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.height;
    }
    
    return .1f;
}

- (void)moreButtonClick{
    
    FWCommentViewController * VC = [[FWCommentViewController alloc] init];
    VC.feed_id = self.listModel.feed_id;
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark - > 关注
- (void)attentionButtonClick {
    
    NSString * action ;
    
    if ([self.listModel.is_followed isEqualToString:@"2"]) {
        action = Submit_follow_users;
    }else{
        action = Submit_cancel_follow_users;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":self.listModel.uid,
                              };
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if ([action isEqualToString:Submit_follow_users]) {
                self.listModel.is_followed = @"1";
                headerView.attentionButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
                [headerView.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
                [headerView.attentionButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
                
                [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self];
            }else{
                self.listModel.is_followed = @"2";
                headerView.attentionButton.backgroundColor = FWTextColor_222222;
                [headerView.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
                [headerView.attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 返回
- (void)backButtonClick{
    
    if (self.requestType == FWPushMessageRequestType ||
        self.requestType == FWADRequestType) {
        
    }else{
        self.myBlock(self.listModel);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 分享
- (void)shareButtonClick{
    
    NSDictionary * params = @{
                              @"aweme":self.listModel,
                              @"type":@"1",
                              };
    
    SharePopView *popView = [[SharePopView alloc] initWithParams:params];
    popView.viewcontroller = self;
    popView.aweme = self.listModel;
    popView.myBlock = ^(FWFeedListModel *awemeModel) {
        self.listModel = awemeModel;
    };
    [popView show];
}

- (void)sendMessageClick{
    
    [self checkLogin];
    
    self.reply_comment_id = self.reply_comment_id?self.reply_comment_id:@"0";
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSString *contentString = commentView.contentView.text;
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":commentModel.feed_id,
                                  @"content":contentString,
                                  @"p_comment_id":self.p_comment_id,
                                  @"reply_comment_id":self.reply_comment_id,
                                  };
        
        
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        request.isNeedShowHud = YES;
        [request startWithParameters:params WithAction:Submit_add_comment  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                
                self.total_count = @([self.total_count integerValue] +1).stringValue;
                [self showCommentCount:self.total_count];
                
                FWMineInfoModel * infoModel = [[FWMineInfoModel alloc] init];
                infoModel.nickname = [GFStaticData getObjectForKey:kTagUserKeyName];
                infoModel.uid = [GFStaticData getObjectForKey:kTagUserKeyID];
                infoModel.header_url = [GFStaticData getObjectForKey:kTagUserKeyImageUrl];
                
                FWCommentListModel * model = [[FWCommentListModel alloc] init];
                model.comment_id = [[back objectForKey:@"data"] objectForKey:@"comment_id"];
                model.content = contentString;
                model.user_info = infoModel;
                model.uid = infoModel.uid;
                model.feed_type = @"2";
                model.is_liked = @"2";
                model.like_count_format = @"0";
                model.format_create_time = @"0秒前";
                
                if (self.commentDataSource.count > 0) {
                    if (self.currentSection >=0) {
                        /* 评论（回复）评论（一、二级评论） */
                        
                        NSString * comment_id = [[back objectForKey:@"data"] objectForKey:@"comment_id"];
                        
                        FWReplyComment * comment = [[FWReplyComment alloc] init];
                        comment.comment_id = comment_id;
                        comment.content = contentString;
                        comment.user_info = infoModel;
                        comment.reply_user_info = self.reply_userInfo;
                        comment.uid = infoModel.uid;
                        comment.feed_type = @"2";
                        comment.is_liked = @"2";
                        comment.like_count_format = @"0";
                        comment.format_create_time = @"刚刚";
                        comment.is_reply = @"1";
                        
                        model.reply_list = [NSMutableArray arrayWithObject:comment];
                        NSMutableArray * tempArr = @[].mutableCopy;
                        
                        FWTopicCommentFrame *topic = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                        topic.topic = model;
                        
                        [tempArr addObject:topic];
                        
                        FWTopicCommentFrame * topicCommentFrame = (FWTopicCommentFrame *)self.commentDataSource[self.currentSection];
                        
                        NSMutableArray * commentFrames = topicCommentFrame.commentFrames.mutableCopy;
                        
                        FWTopicCommentFrame * addCommentFrame = (FWTopicCommentFrame *)tempArr.firstObject;
                        for (FWCommentFrame * comment in addCommentFrame.commentFrames) {
                            
                            if (commentFrames.count >= 3) {
                                [commentFrames insertObject:comment atIndex:commentFrames.count-1];
                            }else {
                                [commentFrames addObject:comment];
                            }
                        }
                        topicCommentFrame.commentFrames = commentFrames.mutableCopy;
                        
                        [UIView performWithoutAnimation:^{
                            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:self.currentSection];
                            [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
                        }];
                    }else{
                        /* 评论了该帖子 */
                        
                        FWTopicCommentFrame *topic = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                        topic.topic = model;
                        
                        [self.commentDataSource insertObject:topic atIndex:0];
                        
                        [UIView performWithoutAnimation:^{
                            [self.infoTableView insertSection:0 withRowAnimation:UITableViewAutomaticDimension];
                        }];
                        [self.infoTableView setContentOffset:CGPointZero animated:NO];
                    }
                }else{
                    [self requestDataWithLoadMoredData:NO];
                }
                
                commentView.contentView.text = @"";
                commentView.contentView.placeholder = @"点赞都是套路，评论才是真情";
                [commentView refreshContentView:commentView.contentView];
                [self.view endEditing:YES];
                
                /* 发送成功后，都置为-1 */
                self.currentSection = -1;
                self.currentRow = -1;
                
                [[FWHudManager sharedManager] showErrorMessage:@"评论成功" toController:self];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 评论主评论
- (void)commentButtonClick:(NSInteger)index{
    
    [self.view endEditing:YES];
    
    id model = self.commentDataSource[index];
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
    
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:topicFrame.topic.uid]) {
        /* 是本人，提醒删除 */
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.comment_id = topicFrame.topic.comment_id;
            [self requestDeleteMessageWithSection:index WithRow:-1];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.p_comment_id = topicFrame.topic.comment_id;
            self.reply_comment_id = topicFrame.topic.comment_id;
            
            [self.commentView.contentView becomeFirstResponder];
            self.commentView.contentView.placeholder = [NSString stringWithFormat:@"回复:%@",topicFrame.topic.user_info.nickname];
            
            self.currentSection = index;
            self.currentRow = -1;
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 喜欢该帖子
- (void)likesButtonClick{
    
    FWLikeRequest * request = [[FWLikeRequest alloc] init];
    
    NSString * likeType;
    
    if([self.listModel.is_liked isEqualToString:@"2"]){
        likeType = @"add";
    }else{
        likeType = @"cancel";
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.listModel.feed_id,
                              @"like_type":likeType,
                              };
    [request startWithParameters:params WithAction:Submit_like_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if ([likeType isEqualToString:@"add"]) {
                [commentView.likesButton setImage:[UIImage imageNamed:@"car_detail_like"] forState:UIControlStateNormal];
                
                self.listModel.is_liked = @"1";
                
                if ([self.listModel.count_realtime.like_count_format integerValue] ||
                    [self.listModel.count_realtime.like_count_format integerValue] == 0) {
                    
                    self.listModel.count_realtime.like_count_format = @([self.listModel.count_realtime.like_count_format integerValue] +1).stringValue;
                }
            }else{
                self.listModel.is_liked = @"2";
                
                [commentView.likesButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
                
                if ([self.listModel.count_realtime.like_count_format integerValue] ||
                    [self.listModel.count_realtime.like_count_format integerValue] == 0)  {
                    
                    self.listModel.count_realtime.like_count_format = @([self.listModel.count_realtime.like_count_format integerValue] -1).stringValue;
                }
            }
            
            [commentView.likesButton setTitle:[NSString stringWithFormat:@"   %@",self.listModel.count_realtime.like_count_format] forState:UIControlStateNormal];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 发送评论
-(void)sendContentText:(NSString *)content{
    
    if (content.length<=0) {
        [[FWHudManager sharedManager] showErrorMessage:@"说点什么吧~" toController:self];
        [self.view endEditing:YES];
        return;
    }
    
    [self sendMessageClick];
}

#pragma mark - > 点击回复的姓名
- (void)commentCell:(FWCommentCell *)commentCell didClickedUser:(FWMineInfoModel *)user{
    
    if (nil == user.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = user.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark keyboardNotification
-(void)keyboardShow:(NSNotification *)note{
    
    CGRect keyBoardRect=[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat deltaY=keyBoardRect.size.height;
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.view).mas_offset(0);
            make.left.right.mas_equalTo(self.view);
            if (SCREEN_HEIGHT >= 812) {
                make.height.mas_greaterThanOrEqualTo(60);
            }else{
                make.height.mas_greaterThanOrEqualTo(50);
            }
        }];
        
        [commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            if (SCREEN_HEIGHT >= 812) {
                make.left.top.mas_equalTo(commentView).mas_offset(15);
            }else{
                make.left.top.mas_equalTo(commentView).mas_offset(10);
            }
            
            make.right.mas_equalTo(commentView.likesButton.mas_left).mas_offset(-20);
            make.height.mas_greaterThanOrEqualTo (30);
        }];
        
        commentView.transform=CGAffineTransformMakeTranslation(0, -deltaY);
    }];
}

-(void)keyboardHide:(NSNotification *)note{
    
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        commentView.transform=CGAffineTransformIdentity;
        self.p_comment_id = self.hostComment_id;
        
        [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.view).mas_offset(0);
            make.left.right.mas_equalTo(self.view);
            make.height.mas_greaterThanOrEqualTo(50+FWSafeBottom);
        }];
        
        [self.commentView.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.width.mas_greaterThanOrEqualTo(30);
            make.right.mas_equalTo(self.commentView).mas_offset(-10);
            if (SCREEN_HEIGHT >= 812) {
                make.top.mas_equalTo(self.commentView).mas_offset(15);
//                make.bottom.mas_equalTo(self.commentView).mas_offset(-15);
            }else{
                make.centerY.mas_equalTo(self.commentView);
//                make.bottom.mas_equalTo(self.commentView).mas_offset(-10);
            }
        }];
        
        [commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (SCREEN_HEIGHT >=812){
                make.left.mas_equalTo(commentView).mas_offset(15);
                make.top.mas_equalTo(commentView).mas_offset(15);
                make.right.mas_equalTo(commentView.likesButton.mas_left).mas_offset(-20);
                make.height.mas_equalTo (30);
            }
            else{
                make.left.top.mas_equalTo(commentView).mas_offset(10);
                make.right.mas_equalTo(commentView.likesButton.mas_left).mas_offset(-20);
                make.height.mas_equalTo (30);
            }
        }];
        
        commentView.contentView.text=@"";
        if(commentView.contentView.text.length<=0){
            commentView.contentView.placeholder = @"点赞都是套路，评论才是真情";
        }
    } completion:^(BOOL finished) {
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [self.view endEditing:YES];
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

#pragma mark - 辅助方法
/** topic --- topicFrame */
- (FWTopicCommentFrame *)_topicFrameWithTopic:(FWCommentListModel *)topic
{
    // 这里要判断评论个数大于3 显示全部评论数
    NSInteger count = [topic.reply_total_count integerValue]?[topic.reply_total_count integerValue]:0;
    if (count > 3) {
        FWReplyComment *comment = [[FWReplyComment alloc] init];
        comment.comment_id = FWAllCommentsId;
        comment.feed_id = self.feed_id;
        comment.top_comment_id = topic.comment_id;
        comment.p_comment_id = topic.comment_id;
        comment.content = MoreReply;
        // 添加假数据
        [topic.reply_list addObject:comment];
    }
    
    FWTopicCommentFrame *topicFrame = [[FWTopicCommentFrame alloc] initWithType:@"1"];
    // 传递话题模型数据，计算所有子控件的frame
    
    topicFrame.topic = topic;
    return topicFrame;
}

/** 刷新段  */
- (void)_reloadSelectedSectin
{
    FWTopicCommentFrame * commentFrame = (FWTopicCommentFrame *)self.commentDataSource[self.selectedSection];
    FWTopicCommentFrame * backFrame = (FWTopicCommentFrame *)self.backDataSource.firstObject;
    
    /* cell中评论的点赞数 和 点赞状态 */
    NSInteger count = backFrame.topic.reply_list.count;
    NSMutableArray * tempArr = backFrame.topic.reply_list.mutableCopy ;
    [tempArr removeObjectsInRange:NSMakeRange(3, count-3)];
    
    for (int i = 0; i < commentFrame.topic.reply_list.count; i++) {
        if (i == 3) {
            break;
        }
        FWReplyComment * commment = commentFrame.topic.reply_list[i];
        FWReplyComment * backCommment = tempArr[i];
        commment.like_count_format = backCommment.like_count_format;
        commment.is_liked = backCommment.is_liked;
    }
    
    /* 更新点赞图标 */
    commentFrame.topic.is_liked = backFrame.topic.is_liked;
    /* 更新点赞数 */
    commentFrame.topic.like_count_format = backFrame.topic.like_count_format;
    
    [UIView performWithoutAnimation:^{
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:self.selectedSection];
        [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }];
    
    self.selectedSection = -1;
}


- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat w = CGRectGetWidth(self.view.frame);
    CGFloat h = picHight;
    self.containerView.frame = CGRectMake(x, y, w, h);

    w = 44;
    h = w;
    x = (CGRectGetWidth(self.containerView.frame)-w)/2;
    y = (CGRectGetHeight(self.containerView.frame)-h)/2;
    self.playBtn.frame = CGRectMake(x, y, w, h);
    
    self.tipView.frame = CGRectMake(0, 0, SCREEN_WIDTH, picHight);
    self.closeButton.frame = CGRectMake(8, FWCustomeSafeTop +14, 40, 40);
    self.tipView.bgImageView.frame = self.tipView.frame;
    self.tipView.tipLabel.frame = CGRectMake(0, CGRectGetHeight(self.tipView.frame)/2-10, SCREEN_WIDTH, 20);
    self.tipView.continueButton.frame = CGRectMake(CGRectGetWidth(self.tipView.frame)/2-35, CGRectGetMaxY(self.tipView.tipLabel.frame)+5, 70, 30);
}

#pragma mark - > 播放器相关配置
- (void)setupPlayer{
    self.playerManager= [[ZFAVPlayerManager alloc] init];
    self.playerManager.scalingMode = ZFPlayerScalingModeAspectFit;

    /// 播放器相关
    self.player = [ZFPlayerController playerWithPlayerManager:self.playerManager containerView:self.containerView];
    self.player.controlView = self.controlView;
    /// 设置退到后台继续播放
    self.player.pauseWhenAppResignActive = NO;
    
    self.player.customAudioSession = YES;
    self.player.playerReadyToPlay = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSURL * _Nonnull assetURL) {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:nil];
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
    };
}

#pragma mark - > 设置顶部视图
- (void)setupTopView{
   
    self.tipView = [[FWLiveTipView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, picHight)];
    self.tipView.delegate = self;
    if (!self.listModel.feed_cover) {
        self.listModel.feed_cover = @"";
    }
    [self.tipView.bgImageView sd_setImageWithURL:[NSURL URLWithString:self.listModel.feed_cover]];
    [self.view addSubview:self.tipView];
    self.tipView.hidden = YES;
    
    self.closeButton = [[UIButton alloc] init];
    self.closeButton.frame = CGRectMake(8, FWCustomeSafeTop +14, 40, 40);
    [self.closeButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
}

#pragma mark - > 关闭当前页面
- (void)closeButtonClick{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

- (void)playClick:(UIButton *)sender {
    [self.player playTheIndex:0];
    if (!self.listModel.feed_cover) {
        self.listModel.feed_cover = @"";
    }
    [self.controlView showTitle:@"" coverURLString:self.listModel.feed_cover fullScreenMode:ZFFullScreenModeAutomatic];
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (FWLongVideoPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [FWLongVideoPlayerControlView new];
        _controlView.prepareShowLoading = YES;
        _controlView.fullScreenOnly = NO;
        _controlView.fastViewAnimated = YES;
        _controlView.autoHiddenTimeInterval = 5;
        _controlView.autoFadeTimeInterval = 0.5;
        _controlView.prepareShowLoading = YES;
    }
    return _controlView;
}

- (UIImageView *)containerView {
    if (!_containerView) {
        _containerView = [UIImageView new];
        if (!self.listModel.feed_cover) {
            self.listModel.feed_cover = @"";
        }
        [_containerView setImageWithURLString:self.listModel.feed_cover placeholder:[ZFUtilities imageWithColor:[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1] size:CGSizeMake(1, 1)]];
    }
    return _containerView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setImage:[UIImage imageNamed:@"new_allPlay_44x44_"] forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(playClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playBtn;
}

#pragma mark - > 监听当前网络状态
- (void)monitorLiveNet{
    
    [self checkWifi];
}

- (void)checkWifi{
    
    if ([[GFStaticData getObjectForKey:Current_Network] isEqualToString:@"WIFI"]) {
        
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            [self requestHeaderInfo];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }else{
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            if (![[GFStaticData getObjectForKey:Allow_4G_Play] boolValue]) {
                [self requestHeaderInfo];
                
                self.tipView.hidden = NO;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.player.currentPlayerManager pause];
                    [self.playerManager pause];
                });
            }else{
                self.tipView.hidden = YES;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }
}

#pragma mark - > 4G下 点击继续观看
- (void)continueLiveMatch{
    
    self.tipView.hidden = YES;
    
    self.lastNetWork = [GFStaticData getObjectForKey:Last_Network];
    
    if ([self.lastNetWork isEqualToString:@"WIFI"] && ![self.lastNetWork isEqualToString:[GFStaticData getObjectForKey:Current_Network]]) {
        // 由wifi切换到4G ，继续播放
        [self.player.currentPlayerManager play];
    }else{
        // 进来直接是4G
        [self requestHeaderInfo];
    }
    
    [GFStaticData saveObject:@"WIFI" forKey:Current_Network];
    [GFStaticData saveObject:@"1" forKey:Allow_4G_Play];
}

#pragma mark - > 视频播放完成
- (void)videoPlayFinished{
    
    if (self.isRePlay) {
        self.isRePlay = NO;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.isRePlay = YES;
        });
        
        [self.player playTheIndex:0];
    }
}
@end
