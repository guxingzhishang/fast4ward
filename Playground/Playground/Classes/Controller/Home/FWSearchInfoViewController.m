//
//  FWSearchInfoViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/19.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWSearchInfoViewController.h"
#import "FWTagsListCell.h"
#import "FWTagsViewController.h"
#import <Accelerate/Accelerate.h>
#import "FWSearchResultViewController.h"
#import "FWSearchTagsListRequest.h"

@interface FWSearchInfoViewController ()

@property (nonatomic, strong) UIImageView * mainView;

@property (nonatomic, strong) NSMutableArray * searchArray;

@property (nonatomic, strong) NSMutableArray * tagsList;

@property (nonatomic, strong) NSString * defaultKeyWord;

@end

@implementation FWSearchInfoViewController
@synthesize headerView;
@synthesize cancelBtn;
@synthesize searchBar;
@synthesize infoView;
@synthesize listTableView;
@synthesize mainView;

- (NSMutableArray *)searchArray{
    if (!_searchArray) {
        _searchArray = [[NSMutableArray alloc] init];
    }
    return _searchArray;
}

- (NSMutableArray *)tagsList{
    if (!_tagsList) {
        _tagsList = [[NSMutableArray alloc] init];
    }
    
    return _tagsList;
}

- (void)viewGesture{
    [self.searchBar endEditing:YES];
}

#pragma mark - > 请求热词
- (void)requestHotKeywords{
    
    FWSearchRequest * request = [[FWSearchRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_hot_keywords WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.tagsList = [[back objectForKey:@"data"] objectForKey:@"list"];
            
            [infoView configViewWithModel:self.tagsList];
            
            self.defaultKeyWord = [[back objectForKey:@"data"] objectForKey:@"default_keyword"];
            
            self.searchBar.placeholder = self.defaultKeyWord;
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    [self trackPageBegin:@"首页搜索页"];

    [self.searchBar becomeFirstResponder];
    self.searchBar.text = @"";
    
    [self requestHotKeywords];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"首页搜索页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;

    self.fd_prefersNavigationBarHidden = YES;
    
    self.fd_interactivePopDisabled = YES;
    
    if (@available(iOS 13.0, *)) {
        [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewGesture)]];
    }
}

#pragma mark - > 视图初始化
- (void)addSubviews{
    
    [self setupNavigationView];
    [self setupTableView];
}

- (void)setupNavigationView{
    
    /* 自定义导航栏 */
    headerView = [[UIView alloc] init];
    [self.view addSubview:headerView];
    [headerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 74));
    }];
    
    searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault;
    searchBar.placeholder = @"请输入车系、活动或位置";
    searchBar.tintColor = FWTextColor_BCBCBC;
    searchBar.barTintColor = [UIColor whiteColor];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    [headerView addSubview:searchBar];
    [searchBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(headerView).mas_offset(13/5);
        make.top.mas_equalTo(headerView).mas_offset(15+FWCustomeSafeTop);
        make.right.mas_equalTo(headerView.mas_right).mas_offset(-58.5);
        make.bottom.mas_equalTo(headerView);
    }];

    [self removeBorder:searchBar];
    
    cancelBtn = [[UIButton alloc] init];
    cancelBtn.titleLabel.font = DHSystemFontOfSize_15;
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:cancelBtn];
    [cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(headerView).mas_offset(-10);
        make.centerY.mas_equalTo(searchBar);
        make.size.mas_equalTo(CGSizeMake(40, 30));
    }];
}

- (void)setupTableView{
    
    infoView = [[FWSearchInfoView alloc] init];
    infoView.infoDelegate = self;
    infoView.backgroundColor = [UIColor clearColor];
    [self.view  addSubview:infoView];
    [infoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(headerView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-74-FWCustomeSafeTop));
    }];
    
    listTableView = [[UITableView alloc] init];
    listTableView.delegate = self;
    listTableView.dataSource = self;
    listTableView.rowHeight = 60;
    listTableView.backgroundColor = [UIColor clearColor];
    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view  addSubview:listTableView];
    [listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(headerView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop));
    }];
    listTableView.hidden = YES;
}

#pragma mark - > 跳转至标签页
-(void)tagsBtnOnClickWithSuperIndex:(NSInteger)superIndex WithSubIndex:(NSInteger)subIndex{
    
    NSString * query = @"";
    
    if (superIndex == 325) {
        
        NSData * data = [GFStaticData getObjectForKey:Search_History_Array];
        NSMutableArray *tagsArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        NSDictionary * tempDict = tagsArray[subIndex];
        
        if ([tempDict[@"type"] isEqualToString:@"tag"]) {
            
            [self saveHistoryWithContent:tempDict];
            
            FWTagsViewController * tagsVC = [[FWTagsViewController alloc] init];
            tagsVC.traceType = 3;
            tagsVC.tags_name = tempDict[@"keyword"];
            tagsVC.tags_id = tempDict[@"tag_id"];
            [self.navigationController pushViewController:tagsVC animated:YES];
            return;
        }else{
            query = tempDict[@"keyword"];
        }
    }else if (superIndex == 327) {
        
        NSDictionary * dict = self.tagsList[subIndex];
        if ([dict[@"type"] isEqualToString:@"keyword"]) {
            query  = dict[@"keyword"];
        }else{
            
            [self saveHistoryWithContent:dict];
            
            FWTagsViewController * tagsVC = [[FWTagsViewController alloc] init];
            tagsVC.traceType = 3;
            tagsVC.tags_name = dict[@"keyword"];
            tagsVC.tags_id = dict[@"tag_id"];
            [self.navigationController pushViewController:tagsVC animated:YES];
            return;
        }
    }
    
    [self pushToNextViewController:query];
}

#pragma mark - > tableView  Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.searchArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"tagsListID";
    
    FWTagsListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWTagsListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = [UIColor clearColor];
    
    [cell cellConfigureFor:self.searchArray[indexPath.row]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{}

#pragma mark - > searchBar delegate 实时搜索标签
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([self.searchBar.text length] > 20){
        
        self.searchBar.text = [self.searchBar.text substringToIndex:20];
    }
}

#pragma mark - > 搜索
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [searchBar endEditing:YES];

    if (searchBar.placeholder.length > 0 && searchBar.text.length <= 0) {
        
        [self pushToNextViewController:searchBar.placeholder];
        return;
    }
    
    [self pushToNextViewController:searchBar.text];
}

#pragma mark - > 跳转至搜索结果页
- (void)pushToNextViewController:(NSString *)query{
    
    NSDictionary * tempDict = @{@"keyword":query};
    
    [self saveHistoryWithContent:tempDict];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 0.4秒后跳转
        FWSearchResultViewController * RVC = [[FWSearchResultViewController alloc] init];
        RVC.query = query;
        [self.navigationController pushViewController:RVC animated:NO];
    });
}

#pragma mark - > 保存历史记录
- (void)saveHistoryWithContent:(NSDictionary *)keywordDict{
   
    //如果是空格，就不存储
    NSString * tempKeyword = keywordDict[@"keyword"];
    tempKeyword = [tempKeyword stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (tempKeyword.length <= 0) {
        return;
    }
    /* 如果有，则取出历史标签，
     加入新元素后，插在第0个位置
     如果大于10个，将最后一个删除 */
    NSData * data = [GFStaticData getObjectForKey:Search_History_Array];
    NSMutableArray *tagsArray = @[].mutableCopy;
    NSMutableArray * tempArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
   
    NSString * keyword = keywordDict[@"keyword"];
    if (tempArray.count > 0) {
        
        NSInteger index = -1;
        for (int i =0; i< tempArray.count; i++) {
            
            NSDictionary * dict = tempArray[i];
            NSString * keyWord = dict[@"keyword"];
            
            if ([keyWord isEqualToString:keyword]) {
                index = i;
                break;
            }
        }
        /* 如果有相同id存在的标签，先删除原标签，再重新加入
         始终让每次查询的在最前面 */
        tagsArray =  tempArray;
        
        if (index >= 0) {
            [tagsArray removeObjectAtIndex:index];
        }
        [tagsArray insertObject:keywordDict atIndex:0];
    }else{
        [tagsArray addObject:keywordDict];
    }
    
    if (tagsArray.count>10) {
        [tagsArray removeLastObject];
    }
    
    data = [NSKeyedArchiver archivedDataWithRootObject:tagsArray];
    [GFStaticData saveObject:data forKey:Search_History_Array];
}


#pragma mark - > 取消搜索
- (void)cancelBtnOnClick:(UIButton *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 去掉输入框边框
- (void)removeBorder:(UISearchBar * )searchBar{
    
    if (@available(iOS 13.0, *)) {
        UITextField *textField = (UITextField *)self.searchBar.searchTextField;
        
        textField.font = DHSystemFontOfSize_14;
        // 设置 Search 按钮可用
        textField.enablesReturnKeyAutomatically = NO;
        
        UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];
        
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
        [button setTitle:@"收起" forState:UIControlStateNormal];
        [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
        [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
        [bar setItems:buttonsArray];
        textField.inputAccessoryView = bar;
        
        
        // 改变输入框背景色
        textField.subviews[0].backgroundColor = [UIColor clearColor];
//        textField.layer.cornerRadius = 1;
//        textField.layer.masksToBounds = YES;
    }else{
        // 去掉搜索框边框
         for(int i =  0 ;i < searchBar.subviews.count;i++){
             UIView * backView = searchBar.subviews[i];
             if ([backView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                 [backView removeFromSuperview];
                 [searchBar setBackgroundColor:[UIColor clearColor]];

                 break;
             }else{
                 NSArray * arr = searchBar.subviews[i].subviews;
                 for(int j = 0;j<arr.count;j++   ){
                     UIView * barView = arr[i];
                     if ([barView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {

                         [barView removeFromSuperview];
                         [searchBar setBackgroundColor:[UIColor clearColor]];
                         break;
                     }
                 }
             }
         }
        

        // 改变UISearchBar内部输入框样式
        UIView *searchTextField = nil;
        searchTextField = [[[self.searchBar.subviews firstObject] subviews] lastObject];
        // 改变输入框背景色
        searchTextField.subviews[0].backgroundColor = [UIColor clearColor];
//        searchTextField.layer.cornerRadius = 1;
//        searchTextField.layer.masksToBounds = YES;
    }
    

    // 获取 TextField 并设置
    for (UIView *subView in self.searchBar.subviews) {
        for (UIView *textFieldSubView in subView.subviews) {
            if ([textFieldSubView isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)textFieldSubView;

                // 设置 Search 按钮可用
                textField.enablesReturnKeyAutomatically = NO;

                UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,44)];

                UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 7,50, 30)];
                [button setTitle:@"收起" forState:UIControlStateNormal];
                [button setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
                [button addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];

                UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

                UIBarButtonItem * buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
                NSArray * buttonsArray = [NSArray arrayWithObjects:space,buttonItem,nil];
                [bar setItems:buttonsArray];
                textField.inputAccessoryView = bar;

                break;
            }
        }
    }
}

- (void)hideKeyBoard{
    
    [self.searchBar endEditing:YES];
}

@end
