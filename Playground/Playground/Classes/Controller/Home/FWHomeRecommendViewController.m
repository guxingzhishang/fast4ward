//
//  FWHomeRecommendViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeRecommendViewController.h"
#import "FWHomeRecomendView.h"

@interface FWHomeRecommendViewController ()<UITableViewDelegate,UITableViewDataSource,FWHomeCellDelegate>

@property (nonatomic, assign) NSInteger   recommendPageNum;
@property (nonatomic, strong) NSMutableArray * recommendDataSource;
@property (nonatomic, strong) FWFeedModel * recommendFeedModel;
/* 记录置顶帖的数量 */
@property (nonatomic, assign) NSInteger   topCount;
/* 判断推荐接口第一次请求 */
@property (nonatomic, assign) BOOL   isFirstRequest;

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) FWHomeRecomendView * recommendView;
@property (nonatomic, strong) NSIndexPath * currentIndexPath;

@property (nonatomic, strong) NSString * page3;
@property (nonatomic, strong) NSString * page2_feed_id;

@property (nonatomic, assign) BOOL isFirstLoading; // 第一次启动时不显示loading，否则在开屏广告处有菊花

@property (nonatomic, assign) NSInteger currentRow ;
@property (nonatomic, assign) NSInteger oldOffset; // 偏移量
@property (nonatomic, assign) BOOL  justScroll; // 刚刚滚动过
@end

@implementation FWHomeRecommendViewController
@synthesize recommendFeedModel;
@synthesize recommendPageNum;
@synthesize topCount;
@synthesize isFirstRequest;
@synthesize infoTableView;
@synthesize recommendView;

#pragma mark - > 请求推荐列表
- (void)requestRecommendListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        
        self.page3 = @"0";
        self.page2_feed_id = @"0";
        recommendPageNum = 1;
        if (self.recommendDataSource.count > 0 ) {
            [self.recommendDataSource removeAllObjects];
        }
    }else{
        recommendPageNum += 1;
        [self requestHasReadFeed:self.recommendDataSource.count-1];
    }
    
//    NSDate * startDare = [NSDate date];
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":@"0",
                              @"flag_return":@"2",
                              @"page3":self.page3,
                              @"page2_feed_id":self.page2_feed_id,
                              @"page":@(recommendPageNum).stringValue,
                              @"page_size":@"40",
                              };
    NSString * action = Get_feeds_by_suggestion_v3;
    
    if (self.isFirstLoading) {
        request.isNeedShowHud = YES;
    }else{
        self.isFirstLoading = YES;
    }
    
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
//        NSLog(@"耗时：%f 秒",(double)[[NSDate date] timeIntervalSinceDate:startDare]);
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                // 上拉刷新
                [infoTableView.mj_footer endRefreshing];
            }else{
                // 下拉刷新
                [infoTableView.mj_header endRefreshing];
            }
            recommendFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            self.page2_feed_id = recommendFeedModel.page2_feed_id;
            self.page3 = recommendFeedModel.page3;
            [self.recommendView configViewForModel:recommendFeedModel];
            
            self.recommendView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.recommendView.currentHeight);
            self.infoTableView.tableHeaderView = self.recommendView;
            
            if (![GFStaticData getObjectForKey:@"mengceng4"] && [kAppVersion isEqualToString:@"2.0.3"]) {
                [GFStaticData saveObject:@"YES" forKey:@"mengceng4"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"mengcengChangeTab" object:nil];
            }
            [self.recommendDataSource addObjectsFromArray: recommendFeedModel.feed_list];
            
            if([recommendFeedModel.feed_list count] == 0 &&
               recommendPageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
            
            if (!isLoadMoredData) {
                // 下拉刷新，或者第一次请求，直接滚动到顶部
                [self.infoTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"首页推荐"];
    
    self.recommendView.isPush = NO;
    
    self.fd_prefersNavigationBarHidden = YES;
}


- (void)deleteFeedInfo{
    
    int tempNum = -1;
    
    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {
        
        NSMutableArray * tempArr = self.recommendDataSource;
        
        for (int i = 0; i < tempArr.count; i++) {
            FWFeedListModel * tempModel = tempArr[i];
            
            if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                tempNum = i;
            }
        }
        
        if (tempNum >=0) {
            [tempArr removeObjectAtIndex:tempNum];
            [infoTableView reloadData];
        }
    }
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"首页-推荐"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.recommendDataSource = @[].mutableCopy;
    recommendPageNum = 0;
    topCount = 0;
    isFirstRequest = YES;
    self.page2_feed_id = @"0";
    self.page3 = @"0";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteFeedInfo) name:@"DeleteFeedInfo" object:nil];

    [self requestRecommendListWithLoadMoredData:NO];

    [self setupSubViews];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.estimatedRowHeight = 200;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.backgroundColor = FWTextColor_FAFAFA;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.infoTableView registerClass:[FWHomeCell class]  forCellReuseIdentifier:@"recommendCellID"];
    [self.view addSubview:infoTableView];
    self.infoTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.view.bounds.size.height-(70+FWCustomeSafeTop)-(49+FWSafeBottom));

    if (@available(iOS 11.0, *)) {
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    /* 设置这个后，防止上拉加载更多时，乱弹 */
    self.infoTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];
    
    recommendView = [[FWHomeRecomendView alloc] init];
    recommendView.vc = self;
    recommendView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 99);
    infoTableView.tableHeaderView = recommendView;
    
    DHWeakSelf;
    self.recommendView.myBlock = ^(CGFloat height) {
        weakSelf.recommendView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
        [weakSelf.infoTableView reloadData];
    };

    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestRecommendListWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        [weakSelf requestRecommendListWithLoadMoredData:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.recommendDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWHomeCell * cell =  [tableView dequeueReusableCellWithIdentifier:@"recommendCellID"];
    
    [cell.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cell.contentView).mas_offset(0);
        make.left.mas_equalTo(cell.contentView).mas_offset(0);
        make.right.mas_equalTo(cell.contentView).mas_offset(-0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(100);
        make.bottom.mas_equalTo(cell.contentView).mas_offset(-0);
    }];
    
    cell.delegate            = self;
    cell.vc                  = self;
    cell.bannerImageView.tag  = 1000+indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (indexPath.row < self.recommendDataSource.count) {
        cell.listModel = self.recommendDataSource[indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [infoTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.recommendDataSource.count) {
        [self dealWithOperationWithIndex:indexPath.row];
    }
}

- (void)picImageClick:(NSInteger)index{
    
    [self dealWithOperationWithIndex:index];
}

#pragma mark - > 跳转逻辑
- (void)dealWithOperationWithIndex:(NSInteger)index{
    
    NSMutableArray * tempArr = self.recommendDataSource;
    NSInteger requestType = FWRecommendRequestType;
    
    NSMutableArray * tempDataSource = @[].mutableCopy;
    
    FWFeedListModel * model = self.recommendDataSource[index];

    if (tempArr.count <= 0) {
        return;
    }
    
    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }
    
    if ([model.feed_type isEqualToString:@"1"]) {
        
        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.listModel = model;
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {
                self.recommendDataSource[index] = listModel;
            };
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            for (NSInteger i = index; i<tempArr.count; i++) {
                FWFeedListModel * model = tempArr[i];
                
                if ([model.feed_type isEqualToString:@"1"] && [model.is_long_video isEqualToString:@"2"]) {
                    [tempDataSource addObject:model];
                }
            }
            
            // 视频流 由于筛选过，所以每次点击的都是数据源第一个。
            FWVideoPlayerViewController * VPVC = [[FWVideoPlayerViewController alloc] init];
            VPVC.currentIndex = 0;
            VPVC.listModel = tempDataSource;
            VPVC.requestType = requestType;
            VPVC.flag_return = @"2";
            VPVC.feed_id = model.feed_id;
            VPVC.page2_feed_id = self.page2_feed_id;
            VPVC.page3 = self.page3;
            [self.navigationController pushViewController:VPVC animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        // 图文详情
        FWGraphTextDetailViewController * DVC = [[FWGraphTextDetailViewController alloc] init];
        DVC.listModel = self.recommendDataSource[index];
        DVC.feed_id = ((FWFeedListModel *)self.recommendDataSource[index]).feed_id;
        DVC.myBlock = ^(FWFeedListModel *listModel) {
            self.recommendDataSource[index] = listModel;
        };
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"4"]) {
            
        // 问答页
        FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        
        // 改装页
        FWRefitCaseDetailViewController * RCDVC = [[FWRefitCaseDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    //获取最后一个cell对象
    UITableViewCell *cell ;
    if (scrollView.contentOffset.y >self.oldOffset) {
        cell = self.infoTableView.visibleCells.lastObject;
    }else{
        cell= self.infoTableView.visibleCells.firstObject;
    }
    //获取最后一个cell的indexPath
    NSIndexPath *indexPath = [self.infoTableView indexPathForCell:cell];
    
    if (self.currentRow == indexPath.row) {
        return;
    }

    self.currentIndexPath = indexPath;
    self.currentRow = self.currentIndexPath.row;

    if (self.currentIndexPath.row > 0) {
        if (scrollView.contentOffset.y >self.oldOffset) {
            // 向下
//            NSLog(@"---下--%ld已经展示完全",self.currentIndexPath.row-1);
            [self requestHasReadFeed:self.currentIndexPath.row-1];
        }else{
            // 向上
            if (!self.justScroll) {
//                NSLog(@"---上--%ld已经展示完全",self.currentIndexPath.row+1);
                [self requestHasReadFeed:self.currentIndexPath.row+1];
            }else{
                self.justScroll = !self.justScroll;
            }
        }
    }else{
//        NSLog(@"---平--%ld已经展示完全",self.currentIndexPath.row);
        [self requestHasReadFeed:self.currentIndexPath.row];
    }
   
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    //获取最后一个cell对象
    UITableViewCell *cell = self.infoTableView.visibleCells.lastObject;
    //获取最后一个cell的indexPath
    NSIndexPath *indexPath = [self.infoTableView indexPathForCell:cell];
    
    self.currentIndexPath = indexPath;
    self.currentRow = self.currentIndexPath.row;

    self.justScroll = YES;
//    NSLog(@"---滚--%ld已经展示完全",self.currentIndexPath.row-1);
    [self requestHasReadFeed:self.currentIndexPath.row-1];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    self.oldOffset  = scrollView.contentOffset.y;
}

#pragma mark - > 标记已读帖子
- (void)requestHasReadFeed:(NSInteger)index{
    
    if (index > self.recommendDataSource.count) {
        return;
    }
    FWFeedListModel * model = self.recommendDataSource[index];
    
    if (!model.feed_id || !model.feed_type) {
        return;
    }
    NSArray * feedsArray = @[@{@"feed_id":model.feed_id,@"feed_type":model.feed_type}];
    NSString * feed_ids = [feedsArray mj_JSONString];
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_ids":feed_ids,
                              };
    
    [request startWithParameters:params WithAction:Submit_mark_feed_id WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
//        NSLog(@"responseObject=== %@",responseObject);
    }];
}

#pragma mark - > 没有网2秒后取消刷新
- (void)networkUnAvailable:(NSNotification *)noti{
    
    if (![((UINavigationController *)self.tabBarController.selectedViewController).topViewController isEqual:self]
        ) {
        return;
    }
    [self hideRefresh];
}

- (void)hideRefresh{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 矜持会，3秒后取消
        [infoTableView.mj_footer endRefreshing];
        [infoTableView.mj_header endRefreshing];
    });
    
}


@end
