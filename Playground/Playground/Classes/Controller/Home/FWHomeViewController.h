//
//  FWHomeViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWBaseCollectionLayout.h"
#import "FWHomeNavigationView.h"
#import "WMPageController.h"
#import "FWCollectionBaseModel.h"
#import "FWCollecitonBaseCell.h"
#import "FWUploadView.h"

@interface FWHomeViewController : WMPageController<UIScrollViewDelegate>

@property (nonatomic, strong) FWHomeNavigationView * navigationView;
@property (nonatomic, strong) UIButton * signButton;
@property (nonatomic, strong) UIView * signView;

@end
