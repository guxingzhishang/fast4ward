//
//  FWCommentViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWCommentViewController.h"
#import "FWCommentCell.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWCommentMessageRequest.h"
#import "FWLikeRequest.h"
#import "FWCommentModel.h"
#import "FWCommentHeaderView.h"
#import "FWCommentDetailViewController.h"

@interface FWCommentViewController ()<ShareViewDelegate,FWCommentCellDelegate,FWCommentHeaderViewDelegate>

@property (nonatomic, strong)FWCommentModel * commentModel;
@property (nonatomic, strong)NSString * hostComment_id;
/** 选中的话题尺寸模型 */
@property (nonatomic , assign) NSInteger selectedSection;

@end

@implementation FWCommentViewController
@synthesize infoTableView;
@synthesize commentView;
@synthesize feed_id;
@synthesize commentModel;
@synthesize pageNum;
@synthesize commentDataSource;

#pragma mark - > 请求评论列表
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    if (isLoadMoredData == NO) {
        pageNum = 1;
        
        if (self.commentDataSource.count > 0 ) {
            [self.commentDataSource removeAllObjects];
        }
    }else{
        pageNum +=1;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.feed_id,
                              @"p_comment_id":self.p_comment_id,
                              @"page":@(pageNum).stringValue,
                              @"page_size":@"20",
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_comment_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            [self dealWithData:back];
            
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 处理数据
- (void)dealWithData:(NSDictionary *)back{
    
    commentModel = [FWCommentModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

    
    for (FWCommentListModel * model in commentModel.comment_list) {
        [self.commentDataSource addObject:[self _topicFrameWithTopic:model]];
    }

    self.total_count = commentModel.all_comment_count?commentModel.all_comment_count:@"0";
    [self showCommentCount:self.total_count];
    
    if([commentModel.comment_list count] == 0 &&
       pageNum != 1){
        [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
    }else{
        [infoTableView reloadData];
    }
}

#pragma mark - > 显示共多少评论
- (void)showCommentCount:(NSString *)count{
    self.title = [NSString stringWithFormat:@"共%@条评论",count];
}

#pragma mark - > 删除消息
- (void)requestDeleteMessageWithSection:(NSInteger)section WithRow:(NSInteger)row{
    
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.commentModel.feed_id,
                              @"comment_id":self.comment_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_del_comment WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)self.commentDataSource[section];
            
            if (row >= 0) {
                [topicFrame.commentFrames removeObjectAtIndex:row];
                
                [UIView performWithoutAnimation:^{
                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
                    [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                }];
                
                self.total_count = @([self.total_count integerValue] -1).stringValue;

            }else{
                self.total_count = @([self.total_count integerValue] -[topicFrame.topic.reply_total_count integerValue]-1).stringValue;

                [self.commentDataSource removeObjectAtIndex:section];
                [self.infoTableView reloadData];
            }
            
            [self showCommentCount:self.total_count];

            [[FWHudManager sharedManager] showSuccessMessage:@"删除成功" toController:self];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageNum = 0;
    self.p_comment_id = @"0";
    self.commentDataSource = @[].mutableCopy;
    self.backDataSource = @[].mutableCopy;
    
    self.hostComment_id = self.p_comment_id;
    
    self.selectedSection = -1;
    self.currentRow = -1;
    self.currentSection = -1;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self requestDataWithLoadMoredData:NO];

    [self setupSubViews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if (self.selectedSection>=0) {
        [self _reloadSelectedSectin];
    }

    [self trackPageBegin:@"图文贴评论页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"图文贴评论页"];
}


#pragma mark - > 视图初始化
- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    infoTableView.tag = CommentListTag;
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"暂无评论，快抢沙发";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-49-FWSafeBottom);
    }];

    
    commentView = [[FWCommentView alloc] init];
    commentView.delegate = self;
    commentView.type = 1;
    [self.view addSubview:commentView];
    [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.view).mas_offset(0);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_greaterThanOrEqualTo(50+FWSafeBottom);
    }];
    
    [commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_greaterThanOrEqualTo (30);
        
        if (SCREEN_HEIGHT >=812){
            make.left.top.mas_equalTo(commentView).mas_offset(15);
            make.bottom.mas_equalTo(commentView).mas_offset(-40);
            make.right.mas_equalTo(commentView).mas_offset(-15);
        }else{
            make.left.top.mas_equalTo(commentView).mas_offset(10);
            make.bottom.mas_equalTo(commentView).mas_offset(-10);
            make.right.mas_equalTo(commentView).mas_offset(-10);
        }
    }];
    
    commentView.shareButton.hidden = YES;
    commentView.likesButton.hidden = YES;
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return self.commentDataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.commentFrames.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id model = self.commentDataSource[indexPath.section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentCell *cell = [FWCommentCell cellWithTableView:tableView WithID:@"CommentID"];
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
        cell.commentFrame = commentFrame;
        cell.viewcontroller = self;
        cell.delegate = self;
        
        if (indexPath.row > 2) {
            cell.photoImageView.image = [UIImage imageNamed:@""];
        }
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];

    self.selectedSection = indexPath.section;

    id model = self.commentDataSource[indexPath.section];

    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
    FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
    
    if (indexPath.row == topicFrame.commentFrames.count-1 &&
        [commentFrame.comment.content isEqualToString:MoreReply]) {

        FWCommentDetailViewController * CDVC = [[FWCommentDetailViewController alloc] init];
        CDVC.pageType = @"2";
        CDVC.feed_id = commentFrame.comment.feed_id?commentFrame.comment.feed_id:@"";
        /*就是要取top_comment_id(因为cell最后一行显示的固定文案，
         所以最后一行comment_id赋值了别的) */
        CDVC.p_comment_id = commentFrame.comment.top_comment_id?commentFrame.comment.top_comment_id:@"0";
        CDVC.top_comment_id = commentFrame.comment.top_comment_id?commentFrame.comment.top_comment_id:@"0";
        DHWeakSelf;
        CDVC.returnDataSource = ^(NSMutableArray * _Nonnull backDataSource) {
            weakSelf.backDataSource = backDataSource;
        };
        [self.navigationController pushViewController:CDVC animated:YES];
    }else{

        if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:commentFrame.comment.uid]) {
            /* 是本人，提醒删除 */
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.comment_id = commentFrame.comment.comment_id;
                [self requestDeleteMessageWithSection:indexPath.section WithRow:indexPath.row];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }else{
            /* 不是本人，回复 */
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                self.p_comment_id = topicFrame.topic.comment_id;
                self.reply_comment_id = commentFrame.comment.comment_id;
                
                [self.commentView.contentView becomeFirstResponder];
                self.commentView.contentView.placeholder = [NSString stringWithFormat:@"回复:%@",commentFrame.comment.user_info.nickname];
                /* 回复 */
                self.reply_userInfo = commentFrame.comment.user_info;
                self.currentSection = indexPath.section;
                self.currentRow = indexPath.row;
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentHeaderView *headerView = [FWCommentHeaderView headerViewWithTableView:tableView];
        headerView.commentButton.tag = section +30000;
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        headerView.topicFrame = topicFrame;
        headerView.nicknameLable.textColor = FWTextColor_000000;
        headerView.createTimeLabel.textColor = FWTextColor_000000;
        headerView.thumbNumberLabel.textColor = FWTextColor_000000;
        headerView.contentLabel.textColor = FWTextColor_000000;
        headerView.viewcontroller = self;
        headerView.delegate = self;
        return headerView;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = FWViewBackgroundColor_EEEEEE;
    return view;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id model = self.commentDataSource[indexPath.section];
    if ([model isKindOfClass:[FWTopicCommentFrame class]]) {
        FWTopicCommentFrame *videoTopicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = videoTopicFrame.commentFrames[indexPath.row];
        
        if (indexPath.row == videoTopicFrame.commentFrames.count-1 &&
            self.commentDataSource.count != 0) {
            return commentFrame.cellHeight+10;
        }
        return commentFrame.cellHeight;
    }
    
    return .1f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.5f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.height;
    }
    
    return .1f;
}

#pragma mark - > 分享
- (void)shareButtonClick{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
        [shareView setupQRViewWithModel:self.listModel];
        [shareView showView];
    }
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0){
            
            [[ShareManager defaultShareManager] shareToMinProgramObjectWithModel:self.listModel];
        }else if (index == 1){
            
            UIImageView * imageView = [[ShareView defaultShareView] getCurrenImage];
            [[ShareManager defaultShareManager] shareToPengyouquan:imageView WithFeedID:self.listModel.feed_id WithType:Share_feed_id];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}

#pragma mark - > 发送消息
- (void)sendMessageClick{
    
    [self checkLogin];
    
    self.reply_comment_id = self.reply_comment_id?self.reply_comment_id:@"0";
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSString *contentString = commentView.contentView.text;
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":commentModel.feed_id,
                                  @"content":contentString,
                                  @"p_comment_id":self.p_comment_id,
                                  @"reply_comment_id":self.reply_comment_id,
                                  };
        
        
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        request.isNeedShowHud = YES;
        [request startWithParameters:params WithAction:Submit_add_comment  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                self.total_count = @([self.total_count integerValue] +1).stringValue;
                [self showCommentCount:self.total_count];
                
                FWMineInfoModel * infoModel = [[FWMineInfoModel alloc] init];
                infoModel.nickname = [GFStaticData getObjectForKey:kTagUserKeyName];
                infoModel.uid = [GFStaticData getObjectForKey:kTagUserKeyID];
                infoModel.header_url = [GFStaticData getObjectForKey:kTagUserKeyImageUrl];
                
                FWCommentListModel * model = [[FWCommentListModel alloc] init];
                model.comment_id = [[back objectForKey:@"data"] objectForKey:@"comment_id"];
                model.content = contentString;
                model.user_info = infoModel;
                model.uid = infoModel.uid;
                model.feed_type = @"2";
                model.is_liked = @"2";
                model.like_count_format = @"0";
                model.format_create_time = @"0秒前";
                
                if(self.commentDataSource.count >0){
                    // 有数据插入
                    if (self.currentSection >=0) {
                        /* 评论（回复）评论（一、二级评论） */
                        
                        NSString * comment_id = [[back objectForKey:@"data"] objectForKey:@"comment_id"];
                        
                        FWReplyComment * comment = [[FWReplyComment alloc] init];
                        comment.comment_id = comment_id;
                        comment.content = contentString;
                        comment.user_info = infoModel;
                        comment.reply_user_info = self.reply_userInfo;
                        comment.uid = infoModel.uid;
                        comment.feed_type = @"2";
                        comment.is_liked = @"2";
                        comment.like_count_format = @"0";
                        comment.format_create_time = @"刚刚";
                        comment.is_reply = @"1";

                        model.reply_list = [NSMutableArray arrayWithObject:comment];
                        NSMutableArray * tempArr = @[].mutableCopy;
                        
                        FWTopicCommentFrame *topic = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                        topic.topic = model;
                        
                        [tempArr addObject:topic];
                        
                        FWTopicCommentFrame * topicCommentFrame = (FWTopicCommentFrame *)self.commentDataSource[self.currentSection];
                        
                        NSMutableArray * commentFrames = topicCommentFrame.commentFrames.mutableCopy;
                        
                        FWTopicCommentFrame * addCommentFrame = (FWTopicCommentFrame *)tempArr.firstObject;
                        for (FWCommentFrame * comment in addCommentFrame.commentFrames) {
                            
                            if (commentFrames.count >= 3) {
                                [commentFrames insertObject:comment atIndex:commentFrames.count-1];
                            }else {
                                [commentFrames addObject:comment];
                            }
                        }
                        topicCommentFrame.commentFrames = commentFrames.mutableCopy;
                        
                        [UIView performWithoutAnimation:^{
                            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:self.currentSection];
                            [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
                        }];
                    }else{
                        /* 评论了该帖子 */
                        
                        FWTopicCommentFrame *topic = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                        topic.topic = model;
                        
                        [self.commentDataSource insertObject:topic atIndex:0];
                        
                        [UIView performWithoutAnimation:^{
                            [self.infoTableView insertSection:0 withRowAnimation:UITableViewAutomaticDimension];
                        }];
                        [self.infoTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }
                }else{
                    // 没数据，刷新列表
                    [self requestDataWithLoadMoredData:NO];
                }
                
                
                commentView.contentView.text = @"";
                commentView.contentView.placeholder = @"点赞都是套路，评论才是真情";
                [commentView refreshContentView:commentView.contentView];
                [self.view endEditing:YES];

                /* 发送成功后，都置为-1 */
                self.currentSection = -1;
                self.currentRow = -1;
                
                [[FWHudManager sharedManager] showErrorMessage:@"评论成功" toController:self];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 评论主评论
- (void)commentButtonClick:(NSInteger)index{
    
    [self.view endEditing:YES];

    id model = self.commentDataSource[index];
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;

    if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:topicFrame.topic.uid]) {
        /* 是本人，提醒删除 */
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.comment_id = topicFrame.topic.comment_id;
            [self requestDeleteMessageWithSection:index WithRow:-1];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.p_comment_id = topicFrame.topic.comment_id;
            self.reply_comment_id = topicFrame.topic.comment_id;
            
            [self.commentView.contentView becomeFirstResponder];
            self.commentView.contentView.placeholder = [NSString stringWithFormat:@"回复:%@",topicFrame.topic.user_info.nickname];
            
            self.currentSection = index;
            self.currentRow = -1;
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 喜欢该帖子
- (void)likesButtonClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        
        NSString * likeType;
        
        if([self.listModel.is_liked isEqualToString:@"2"]){
            likeType = @"add";
        }else{
            likeType = @"cancel";
        }
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.listModel.feed_id,
                                  @"like_type":likeType,
                                  };
        [request startWithParameters:params WithAction:Submit_like_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([likeType isEqualToString:@"add"]) {
                    [commentView.likesButton setImage:[UIImage imageNamed:@"car_detail_like"] forState:UIControlStateNormal];
                    
                    self.listModel.is_liked = @"1";
                    
                    if ([self.listModel.count_realtime.like_count_format integerValue] ||
                        [self.listModel.count_realtime.like_count_format integerValue] == 0) {

                        self.listModel.count_realtime.like_count_format = @([self.listModel.count_realtime.like_count_format integerValue] +1).stringValue;
                    }
                }else{
                    self.listModel.is_liked = @"2";

                    [commentView.likesButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
                    
                    if ([self.listModel.count_realtime.like_count_format integerValue] ||
                        [self.listModel.count_realtime.like_count_format integerValue] == 0) {

                        self.listModel.count_realtime.like_count_format = @([self.listModel.count_realtime.like_count_format integerValue] -1).stringValue;
                    }
                }
                
                [commentView.likesButton setTitle:[NSString stringWithFormat:@"   %@",self.listModel.count_realtime.like_count_format] forState:UIControlStateNormal];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 发送评论
-(void)sendContentText:(NSString *)content{
    
    if (content.length<=0) {
        [[FWHudManager sharedManager] showErrorMessage:@"说点什么吧~" toController:self];
        [self.view endEditing:YES];
        return;
    }
    
    [self sendMessageClick];
}

#pragma mark - > 点击回复的姓名
- (void)commentCell:(FWCommentCell *)commentCell didClickedUser:(FWMineInfoModel *)user{
    
    if (nil == user.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = user.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > keyboardNotification
-(void)keyboardShow:(NSNotification *)note{
    
    CGRect keyBoardRect=[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat deltaY=keyBoardRect.size.height;
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        
        [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.view).mas_offset(0);
            make.left.right.mas_equalTo(self.view);
            if (SCREEN_HEIGHT >= 812) {
                make.height.mas_greaterThanOrEqualTo(60);
            }else{
                make.height.mas_greaterThanOrEqualTo(50);
            }
        }];
        
        [commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            if (SCREEN_HEIGHT >= 812) {
                make.left.top.mas_equalTo(commentView).mas_offset(15);
                make.right.mas_equalTo(commentView.mas_right).mas_offset(-15);
            }else{
                make.left.top.mas_equalTo(commentView).mas_offset(10);
                make.right.mas_equalTo(commentView.mas_right).mas_offset(-10);
            }
            
            make.height.mas_greaterThanOrEqualTo (30);
        }];
        
        commentView.transform=CGAffineTransformMakeTranslation(0, -deltaY);
    }];
}

-(void)keyboardHide:(NSNotification *)note{
    
   
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        commentView.transform=CGAffineTransformIdentity;
        
        self.p_comment_id = self.hostComment_id;
        
        [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.view).mas_offset(0);
            make.left.right.mas_equalTo(self.view);
            make.height.mas_greaterThanOrEqualTo(50+FWSafeBottom);
        }];
        
        [commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (SCREEN_HEIGHT >=812){
                make.left.mas_equalTo(commentView).mas_offset(15);
                make.top.mas_equalTo(commentView).mas_offset(15);
                make.right.mas_equalTo(commentView.mas_right).mas_offset(-15);
                make.height.mas_equalTo (30);
            }
            else{
                make.left.top.mas_equalTo(commentView).mas_offset(10);
                make.right.mas_equalTo(commentView.mas_right).mas_offset(-10);
                make.height.mas_equalTo (30);
            }
        }];
        
        commentView.contentView.text=@"";
        if(commentView.contentView.text.length<=0){
            commentView.contentView.placeholder = @"点赞都是套路，评论才是真情";
        }
    } completion:^(BOOL finished) {
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    [self.view endEditing:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [self.view endEditing:YES];

        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

#pragma mark - 辅助方法
- (FWTopicCommentFrame *)_topicFrameWithTopic:(FWCommentListModel *)topic
{
    // 这里要判断评论个数大于3 显示全部评论数
    NSInteger count = [topic.reply_total_count integerValue]?[topic.reply_total_count integerValue]:0;
    if (count > 3) {
        FWReplyComment *comment = [[FWReplyComment alloc] init];
        comment.comment_id = FWAllCommentsId;
        comment.feed_id = self.feed_id;
        comment.top_comment_id = topic.comment_id;
        comment.p_comment_id = topic.comment_id;
        comment.content = MoreReply;
        // 添加假数据
        [topic.reply_list addObject:comment];
    }
    
    FWTopicCommentFrame *topicFrame = [[FWTopicCommentFrame alloc] initWithType:@"1"];
    // 传递话题模型数据，计算所有子控件的frame
    topicFrame.topic = topic;
    return topicFrame;
}

/** 刷新段  */
- (void)_reloadSelectedSectin
{
    FWTopicCommentFrame * commentFrame = (FWTopicCommentFrame *)self.commentDataSource[self.selectedSection];
    FWTopicCommentFrame * backFrame = (FWTopicCommentFrame *)self.backDataSource.firstObject;
    
    /* cell中评论的点赞数 和 点赞状态 */
    NSInteger count = backFrame.topic.reply_list.count;
    NSMutableArray * tempArr = backFrame.topic.reply_list.mutableCopy ;
    [tempArr removeObjectsInRange:NSMakeRange(3, count-3)];
    
    for (int i = 0; i < commentFrame.topic.reply_list.count; i++) {
        if (i == 3) {
            break;
        }
        FWReplyComment * commment = commentFrame.topic.reply_list[i];
        FWReplyComment * backCommment = tempArr[i];
        commment.like_count_format = backCommment.like_count_format;
        commment.is_liked = backCommment.is_liked;
    }
    
    /* 更新点赞图标 */
    commentFrame.topic.is_liked = backFrame.topic.is_liked;
    /* 更新点赞数 */
    commentFrame.topic.like_count_format = backFrame.topic.like_count_format;
   
    
    [UIView performWithoutAnimation:^{
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:self.selectedSection];
        [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }];
    
    self.selectedSection = -1;
}


@end
