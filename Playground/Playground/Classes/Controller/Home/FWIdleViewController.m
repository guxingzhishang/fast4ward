//
//  FWIdleViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/14.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWIdleViewController.h"
#import "FWScrollViewFollowCollectionView.h"
#import "FWMainTouchScrollView.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWAskAndAnswerModel.h"
#import "FWIdleCollectionCell.h"
#import "FWIdleModel.h"
#import "FWIdleDetailViewController.h"
#import "FWIdleFilterViewController.h"
#import "FWScrollViewFollowCollectionView.h"
#import "FWMainTouchScrollView.h"

static NSString * const allCellId = @"IdleCellId";

@interface FWIdleViewController ()
{
    FWMainTouchScrollView *rootScrollView; //rootScrollView要有滑动穿透
    FWScrollViewFollowCollectionView *thFollow; //必须写成属性
    NSInteger  ScrollHeight;//需三处保持一致

    
    FWAskAndAnswerModel * allFeedModel;
    NSMutableArray * allDataSource;
    NSInteger   allPageNum;
}
@property (nonatomic, strong) FWIdleModel * idleModel;

@end

@implementation FWIdleViewController
@synthesize allCollectionView;
@synthesize collectionScr;

#pragma mark - > ******************* 网络请求 *******************
#pragma mark - > 获取该该标签全部列表信息
- (void)requestAllListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        allPageNum = 1;
        
        if (allDataSource.count > 0 ) {
            [allDataSource removeAllObjects];
        }
    }else{
        allPageNum += 1;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"province_code":@"",
                             @"city_code":@"",
                             @"county_code":@"",
                             @"if_quanxin":@"",
                             @"if_ziti":@"",
                             @"freight":@"",
                             @"order_type":@"",
                             @"xianzhi_category_id":@"",
                             @"page":@(allPageNum).stringValue,
                             @"page_size":@"20",
                             };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_xianzhi  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            allFeedModel = [FWAskAndAnswerModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [allDataSource addObjectsFromArray: allFeedModel.feed_list];
            
            if([allFeedModel.feed_list count] == 0 &&
               allPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.allCollectionView reloadData];
            }
            
            if (isLoadMoredData) {
                [allCollectionView.mj_footer endRefreshing];
            }else{
                [allCollectionView scrollToTopAnimated:YES];
                [rootScrollView.mj_header endRefreshing];
            }
            
            [self refreshFrame];

            if (![GFStaticData getObjectForKey:@"mengceng3"] &&
                [kAppVersion isEqualToString:@"2.0.3"]) {
                [GFStaticData saveObject:@"YES" forKey:@"mengceng3"];
                [self setupZhiyin1];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 刷新视图坐标
- (void)refreshFrame{
    
    CGFloat height = 70;
    
    ScrollHeight = 0;

    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+height+FWSafeBottom+5+height);

    self.topScrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
    
    self.collectionScr.frame = CGRectMake(0, CGRectGetMaxY(self.topScrollView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-50-64-FWCustomeSafeTop-49-FWSafeBottom-height);
    self.allCollectionView.frame =CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame));

    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH, 0);

    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView]];
}

- (void)requestCategary{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_xianzhi_category WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.idleModel = [FWIdleModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self setupTopSubviews];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)setupZhiyin1{
    
    UIView * BGView = [[UIView alloc] init];
    BGView.tag = 7789;
    BGView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    BGView.backgroundColor = FWColorWihtAlpha(@"000000", 0.4);
    [[UIApplication sharedApplication].keyWindow addSubview:BGView];
    
    UIView * zhiyinView = [[UIView alloc] init];
    zhiyinView.userInteractionEnabled = YES;
    zhiyinView.frame = CGRectMake(130, FWCustomeSafeTop+45, 200, 300);
    [BGView addSubview:zhiyinView];
    [zhiyinView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zhiyin1ImageViewClick)]];

    UILabel * xianzhiLabel = [[UILabel alloc] init];
    xianzhiLabel.font = DHBoldFont(18);
    xianzhiLabel.text =@"闲置";
    xianzhiLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    xianzhiLabel.layer.cornerRadius = 4;
    xianzhiLabel.layer.masksToBounds = YES;
    xianzhiLabel.textColor = FWTextColor_222222;
    xianzhiLabel.textAlignment = NSTextAlignmentCenter;
    [zhiyinView addSubview:xianzhiLabel];
    xianzhiLabel.frame = CGRectMake(70, 20, 60, 40);
    
    UILabel * xianzhiTitleLabel = [[UILabel alloc] init];
    xianzhiTitleLabel.font = DHFont(14);
    xianzhiTitleLabel.text =@"闲置功能全新上线啦~";
    xianzhiTitleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    xianzhiTitleLabel.textAlignment = NSTextAlignmentCenter;
    [zhiyinView addSubview:xianzhiTitleLabel];
    xianzhiTitleLabel.frame = CGRectMake(0, CGRectGetMaxY(xianzhiLabel.frame)+20, 200, 20);
    
    
    UILabel * knowLabel = [[UILabel alloc] init];
    knowLabel.font = DHFont(14);
    knowLabel.text =@"知道了";
    knowLabel.layer.borderColor = FWViewBackgroundColor_FFFFFF.CGColor;
    knowLabel.layer.borderWidth = 1;
    knowLabel.layer.cornerRadius = 2;
    knowLabel.layer.masksToBounds = YES;
    knowLabel.textColor = FWViewBackgroundColor_FFFFFF;
    knowLabel.textAlignment = NSTextAlignmentCenter;
    [zhiyinView addSubview:knowLabel];
    knowLabel.frame = CGRectMake(CGRectGetMinX(xianzhiLabel.frame)+10, CGRectGetMaxY(xianzhiTitleLabel.frame)+30, CGRectGetWidth(xianzhiLabel.frame), 40);
    
}

/* 去掉蒙层 */
- (void)zhiyin1ImageViewClick{
    
    [[[UIApplication sharedApplication].keyWindow viewWithTag:7789] removeFromSuperview];
    [[NSNotificationCenter defaultCenter] postNotificationName:kTagPushPublish object:nil];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    [self trackPageBegin:@"闲置页"];

    [self.navigationController.navigationBar setShadowImage:[FWClearColor image]];

    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {
        
        int tempNum = -1;
        for (int i = 0; i < allDataSource.count; i++) {
            FWFeedListModel * tempModel = allDataSource[i];
            
            if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                tempNum = i;
            }
        }
        
        if (tempNum >=0) {
            [allDataSource removeObjectAtIndex:tempNum];
            [allCollectionView reloadData];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"闲置页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    allDataSource = @[].mutableCopy;

    allPageNum = 0;
    ScrollHeight = 0;

    [self setupCollectionView];

    [self requestAllListWithLoadMoreData:NO];
    [self requestCategary];
}

#pragma mark - > 初始化collectionview
- (void)setupCollectionView{
    
    rootScrollView = [[FWMainTouchScrollView alloc] init];
    rootScrollView.frame  = CGRectMake(0, 0,SCREEN_WIDTH, SCREEN_HEIGHT);
    rootScrollView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    rootScrollView.delegate=self; //要遵循代理
    rootScrollView.showsVerticalScrollIndicator = NO;
    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);  //滚动大小
    [self.view addSubview:rootScrollView];
    
    self.topScrollView = [[UIScrollView alloc] init];
    self.topScrollView.delegate = self;
    self.topScrollView.showsHorizontalScrollIndicator = NO;
    [rootScrollView addSubview:self.topScrollView];
    self.topScrollView.frame = CGRectMake(0, 20, SCREEN_WIDTH, 70);
    
    [self setupTopSubviews];

    self.collectionScr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.topScrollView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-49-FWSafeBottom-50)];
    self.collectionScr.delegate = self;
    self.collectionScr.pagingEnabled = YES;
    self.collectionScr.showsVerticalScrollIndicator = NO;
    self.collectionScr.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [rootScrollView addSubview:self.collectionScr];
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH, 0);

    self.allLayout = [[FWBaseCollectionLayout alloc] init];
    self.allLayout.columns = 2;
    self.allLayout.rowMargin = 12;
    self.allLayout.colMargin = 12;
    self.allLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.allLayout.delegate = self;
    [self.allLayout autuContentSize];
    
    self.allCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.allLayout];
    self.allCollectionView.dataSource = self;
    self.allCollectionView.delegate = self;
    self.allCollectionView.showsVerticalScrollIndicator = NO;
    self.allCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.allCollectionView registerClass:[FWIdleCollectionCell class] forCellWithReuseIdentifier:allCellId];
    self.allCollectionView.isNeedEmptyPlaceHolder = YES;
    self.allCollectionView.verticalOffset = -150;
    NSString * allTitle = @"还没有发布的作品哦~";
    NSDictionary * allAttributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * allAttributeString = [[NSMutableAttributedString alloc] initWithString:allTitle attributes:allAttributes];
    self.allCollectionView.emptyDescriptionString = allAttributeString;
    [self.collectionScr addSubview:self.allCollectionView];

    //利用initRoot创建 给予底部上下滑动的ScrollView 要滑动的ScrollRange 大小和左右滑动的Table数组
    thFollow = [[FWScrollViewFollowCollectionView alloc]init];
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView]];
    
    {
        @weakify(self);
        rootScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            @strongify(self);
            [self requestAllListWithLoadMoreData:NO];
        }];
        
        self.allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestAllListWithLoadMoreData:YES];
        }];
    }
}

#pragma mark - > collectionView delegate & datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return allDataSource.count?allDataSource.count:0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWIdleCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:allCellId forIndexPath:indexPath];
    cell.viewController = self;
    
    if (allDataSource.count > 0) {
        
        FWFeedListModel * model = allDataSource[indexPath.item];
        [cell configForCell:model];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = allDataSource;
    FWFeedListModel * model = tempArr[indexPath.row];
    
    FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
    RCDVC.listModel = model;
    RCDVC.feed_id = model.feed_id;
    [self.navigationController pushViewController:RCDVC animated:YES];
}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = allDataSource;
    
    if (indexPath.row >= tempArr.count) {
        return 0;
    }
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

- (void)setupTopSubviews{
    
    for (UIView * view in self.topScrollView.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat x = 0;
    
    NSInteger count = self.idleModel.list.count;
    
    for (int i = 0; i < count; i++) {
        
        FWIdleListModel * listModel = self.idleModel.list[i];
        
        FWModuleButton * moduleButton = [[FWModuleButton alloc] init];
        moduleButton.tag = 10000+i;
        [moduleButton addTarget:self action:@selector(btnTagsClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.topScrollView addSubview:moduleButton];
        [moduleButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.topScrollView).mas_offset(x);
            make.top.mas_equalTo(self.topScrollView).mas_offset(0);
            make.height.mas_equalTo(70);
            make.width.mas_equalTo(60);
        }];
        
        [moduleButton setModuleTitle:listModel.category_name];
        [moduleButton.iconImageView sd_setImageWithURL:[NSURL URLWithString:listModel.img]];
        
        moduleButton.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        [moduleButton.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(moduleButton);
            make.centerX.mas_equalTo(moduleButton);
            make.size.mas_equalTo(CGSizeMake(44, 44));
        }];
        
        moduleButton.iconImageView.layer.cornerRadius = 44/2;
        moduleButton.iconImageView.layer.masksToBounds = YES;
        

        moduleButton.nameLabel.font = DHSystemFontOfSize_14;
        moduleButton.nameLabel.textAlignment = NSTextAlignmentCenter;
        moduleButton.nameLabel.textColor = FWColor(@"#39393F");
        [moduleButton.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(moduleButton);
            make.top.mas_equalTo(moduleButton.iconImageView.mas_bottom);
            make.height.mas_equalTo(20);
            make.centerX.mas_equalTo(moduleButton.iconImageView);
            make.width.mas_equalTo(60);
        }];
        
        [self.topScrollView layoutIfNeeded];
        x += CGRectGetWidth(moduleButton.frame);
    }
    
    self.topScrollView.contentSize = CGSizeMake(60*count, 70);
}

#pragma mark - > 点击顶部
- (void)btnTagsClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 10000;
    
    FWIdleListModel * listModel = self.idleModel.list[index];

    FWIdleFilterViewController * IFVC = [[FWIdleFilterViewController alloc] init];
    IFVC.xianzhi_category_id = listModel.category_id;
    [self.navigationController pushViewController:IFVC animated:YES];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [thFollow followScrollViewScrollScrollViewScroll:scrollView];
}

@end
