//
//  FWChooseCarTypeViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWChooseCarTypeViewController.h"
#import "FWChooseCarStyleViewController.h"
#import "FWChooseCarTypeModel.h"
#import "FWRefitCaseViewController.h"
#import "FWPublishPreviewViewController.h"
#import "FWPublishPreviewGraphViewController.h"
#import "FWPublishPreviewAskViewController.h"
#import "FWPublishPreviewRefitViewController.h"
#import "FWPublishIdleOtherViewController.h"

@interface FWChooseCarTypeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, assign) NSInteger   pageNum;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWChooseCarTypeModel * typeModel;
@end

@implementation FWChooseCarTypeViewController
@synthesize infoTableView;
@synthesize pageNum;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)requestList{
    
    if (self.brand.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"获取品牌出现错误，请退出重新操作" toController:self];
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"brand":self.brand,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_car_type  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if (self.dataSource.count > 0 ) {
                [self.dataSource removeAllObjects];
            }
            
            self.typeModel = [FWChooseCarTypeModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.dataSource addObjectsFromArray: self.typeModel.list];
            [infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"选择车系页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"选择车系页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"选择车系";
    
    [self setupSubViews];
    [self requestList];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.sectionIndexColor = FWTextColor_969696;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.dataSource.count > section) {
        FWChooseCarTypeListModel * listModel = self.dataSource[section];
        return listModel.list.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"ChooseCarTypeID";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.dataSource.count > indexPath.section) {
        FWChooseCarTypeListModel * listModel = self.dataSource[indexPath.section];
        if (listModel.list.count > indexPath.row) {
            cell.textLabel.text = listModel.list[indexPath.row];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (self.dataSource.count > indexPath.section) {
        FWChooseCarTypeListModel * listModel = self.dataSource[indexPath.section];
        if (listModel.list.count > indexPath.row) {

            if ([self.chooseFrom isEqualToString:@"anchexi"]) {
                FWRefitCaseViewController * RCVC = [[FWRefitCaseViewController alloc] init];
                RCVC.car_type = listModel.list[indexPath.row];
                [self.navigationController pushViewController:RCVC animated:YES];
            }else if([self.chooseFrom isEqualToString:@"publish"]){
                /* 从发布页跳转过来的，需返回回去 */
                [GFStaticData saveObject:listModel.list[indexPath.row] forKey:ChooseCarTypeBackToPublish];

                for (UIViewController * vc in self.navigationController.viewControllers) {
                    if ([vc isKindOfClass:[FWPublishPreviewViewController class]]) {
                        [self.navigationController popToViewController:vc animated:YES];
                        return ;
                    }
                    if ([vc isKindOfClass:[FWPublishPreviewGraphViewController class]]) {
                        [self.navigationController popToViewController:vc animated:YES];
                        return ;
                    }
                    if ([vc isKindOfClass:[FWPublishPreviewAskViewController class]]) {
                        [self.navigationController popToViewController:vc animated:YES];
                        return ;
                    }
                    if ([vc isKindOfClass:[FWPublishPreviewRefitViewController class]]) {
                        [self.navigationController popToViewController:vc animated:YES];
                        return ;
                    }
                    if ([vc isKindOfClass:[FWPublishIdleOtherViewController class]]) {
                        [self.navigationController popToViewController:vc animated:YES];
                        return ;
                    }
                }
            }else if([self.chooseFrom isEqualToString:@"chooseChexi"]||
                     [self.chooseFrom isEqualToString:@"addCar"]){
                FWChooseCarStyleViewController * CCTVC = [[FWChooseCarStyleViewController alloc] init];
                CCTVC.brand = self.typeModel.brand;
                CCTVC.category = listModel.category;
                CCTVC.type = listModel.list[indexPath.row];
                [self.navigationController pushViewController:CCTVC animated:YES];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
   
    if (self.dataSource.count > section) {
        FWChooseCarTypeListModel * listModel = self.dataSource[section];
        return listModel.category;
    }
    
    return @"";
}

@end
