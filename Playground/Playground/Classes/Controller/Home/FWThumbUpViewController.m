//
//  FWThumbUpViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWThumbUpViewController.h"
#import "FWCommentMessageCell.h"
#import "FWCommentDetailViewController.h"
#import "FWCarDetailViewController.h"

@interface FWThumbUpViewController ()

@property (nonatomic, strong) NSMutableArray<FWUnionMessageModel *> * dataSource;

@property (nonatomic, assign) NSInteger pageNum;

@end

@implementation FWThumbUpViewController
@synthesize listType;
@synthesize infoTableView;

- (void)dealloc{
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if (listType == FWMessageThumbUpList) {
        [self trackPageBegin:@"点赞页"];
    }else{
        [self trackPageBegin:@"评论页"];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    if (listType == FWMessageThumbUpList) {
        [self trackPageEnd:@"点赞页"];
    }else{
        [self trackPageEnd:@"评论页"];
    }
}

- (void)viewDidLoad {
  
    [super viewDidLoad];

    self.pageNum = 0;
    self.dataSource = @[].mutableCopy;

    if (listType == FWMessageThumbUpList) {
        self.title = @"点赞";
    }else{
        self.title = @"评论";
    }
    [self setupSubViews];
    [self requestDataWithLoadMoredData:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    if (listType == FWMessageThumbUpList) {
        infoTableView.rowHeight = 70;
    }else{
        infoTableView.rowHeight = UITableViewAutomaticDimension;
        infoTableView.estimatedRowHeight = 95;
    }
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:YES];
    }];
}

#pragma mark - > 网络请求
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{

    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    FWNetworkRequest * request;
    NSString * msg_type; //1为点赞,2为关注,3为评论
    
    if (listType == FWMessageThumbUpList) {
        msg_type = @"1";
        request = [[FWLikesMessageRequest alloc] init];
    }else{
        msg_type = @"3";
        request = [[FWCommentMessageRequest alloc] init];
    }
  
    NSDictionary * params = @{
                              @"msg_type":msg_type,
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"pagesize":@"20",
                              };
    request.isNeedShowHud = YES;
    
    [request startWithParameters:params WithAction:Get_msg_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSMutableArray * tempArr = [FWUnionMessageModel mj_objectArrayWithKeyValuesArray:[[back objectForKey:@"data"] objectForKey:@"msg_list"]];
            
            [self.dataSource addObjectsFromArray:tempArr];
            
            if([tempArr count] == 0 && self.pageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger number = self.dataSource.count?self.dataSource.count:0;
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (listType == FWMessageThumbUpList) {
        
        static NSString * cellID = @"thumbupID";
        
        FWThumbUpCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[FWThumbUpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.vc = self;
        [cell cellConfigureFor:self.dataSource[indexPath.row]];
        
        return cell;
    }else{

        static NSString * cellID = @"CommentID";
        
        FWCommentMessageCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[FWCommentMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.vc = self;
        [cell cellConfigureFor:self.dataSource[indexPath.row]];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (listType == FWMessageThumbUpList) {
        
        FWThumbUpCell * cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.unreadView.hidden = YES;
        
        FWUnionMessageModel * messageModel = self.dataSource[indexPath.row];
        
        [self pushNextViewControllerWithModel:messageModel];
    }else{
        FWUnionMessageModel * messageModel = self.dataSource[indexPath.row];

        if ([messageModel.jump_to isEqualToString:@"detail"]) {
            
            // 老版本跳转至详情
            [self pushNextViewControllerWithModel:messageModel];
        }else if ([messageModel.jump_to isEqualToString:@"chat"]) {
            
            // 新版本跳转至评论页
            FWCommentDetailViewController * CDVC = [[FWCommentDetailViewController alloc] init];
            CDVC.pageType = @"1";
            /* 消息页进入详情，写死type为2， */
            CDVC.feedType = @"2";
            CDVC.feed_id = messageModel.feed_id?messageModel.feed_id:@"";
            CDVC.p_comment_id = messageModel.p_comment_id?messageModel.p_comment_id:@"";
            CDVC.r_uid = messageModel.r_uid?messageModel.r_uid:@"";
            CDVC.r_comment_id = messageModel.r_comment_id?messageModel.r_comment_id:@"";
            [self.navigationController pushViewController:CDVC animated:YES];
        }
    }
}

#pragma mark - > 跳转至图文或视频
- (void)pushNextViewControllerWithModel:(FWUnionMessageModel *)messageModel{
    
    if ([messageModel.feed_type isEqualToString:@"1"]) {
        if ([messageModel.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.feed_id = messageModel.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {};
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            // 视频流 由于筛选过，所以每次点击的都是数据源第一个。
            FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
            controller.currentIndex = 0;
            controller.requestType = FWPushMessageRequestType;
            controller.tag_id = @"";
            controller.flag_return = @"1";
            controller.feed_id = messageModel.feed_id;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }else if ([messageModel.feed_type isEqualToString:@"2"]) {
        
        // 图文详情
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.feed_id = messageModel.feed_id;
        TVC.myBlock = ^(FWFeedListModel *awemeModel) {};
        [self.navigationController pushViewController:TVC animated:YES];
    }else if ([messageModel.feed_type isEqualToString:@"3"]) {
        
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = messageModel.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if([messageModel.feed_type isEqualToString:@"10"]) {
        
        // 座驾详情
        FWCarDetailViewController * DVC = [[FWCarDetailViewController alloc] init];
        DVC.user_car_id = messageModel.user_car_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }
}


@end

