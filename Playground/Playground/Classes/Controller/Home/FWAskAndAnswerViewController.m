//
//  FWAskAndAnswerViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWAskAndAnswerViewController.h"
#import "FWRefitCaseCollectionViewCell.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWTagsRequest.h"
#import "FWRefitCaseModel.h"
#import "FWScrollViewFollowCollectionView.h"
#import "FWMainTouchScrollView.h"
#import "FWAskAndAnswerDetailViewController.h"

static NSString * const allCellId = @"allCellId";
@interface FWAskAndAnswerViewController ()
{
    FWMainTouchScrollView *rootScrollView; //rootScrollView要有滑动穿透
    FWScrollViewFollowCollectionView *thFollow; //必须写成属性
    NSInteger  ScrollHeight;//需三处保持一致

    FWAskAndAnswerModel * allFeedModel;
    NSMutableArray * allDataSource;
    NSInteger   allPageNum;
}

@end

@implementation FWAskAndAnswerViewController
@synthesize askAnswerView;
@synthesize allCollectionView;
@synthesize collectionScr;

#pragma mark - > ******************* 网络请求 *******************
#pragma mark - > 获取该该标签全部列表信息
- (void)requestAllListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        allPageNum = 1;
        
        if (allDataSource.count > 0 ) {
            [allDataSource removeAllObjects];
        }
    }else{
        allPageNum += 1;
    }
    
    FWTagsRequest * request = [[FWTagsRequest alloc] init];
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"order_type":self.order_type,
                             @"page":@(allPageNum).stringValue,
                             @"page_size":@"20",
                             };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_wenda  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            allFeedModel = [FWAskAndAnswerModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if ([self.order_type isEqualToString:@"new"]) {
                askAnswerView.isTimeSort = YES;
            }else{
                askAnswerView.isTimeSort = NO;
            }
            
            [askAnswerView congifViewWithModel:allFeedModel];

            [allDataSource addObjectsFromArray: allFeedModel.feed_list];
            
            if([allFeedModel.feed_list count] == 0 &&
               allPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.allCollectionView reloadData];
            }

            
            if (isLoadMoredData) {
                [allCollectionView.mj_footer endRefreshing];
            }else{
                [allCollectionView scrollToTopAnimated:YES];
                [rootScrollView.mj_header endRefreshing];
            }
            
            [self refreshFrame];

        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    [self trackPageBegin:@"问答页"];

    [self.navigationController.navigationBar setShadowImage:[FWClearColor image]];

    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {
        
        int tempNum = -1;
        for (int i = 0; i < allDataSource.count; i++) {
            FWFeedListModel * tempModel = allDataSource[i];
            
            if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                tempNum = i;
            }
        }
        
        if (tempNum >=0) {
            [allDataSource removeObjectAtIndex:tempNum];
            [allCollectionView reloadData];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"问答页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    allDataSource = @[].mutableCopy;

    self.order_type = @"new";

    allPageNum = 0;
    ScrollHeight = 180;
    
    [self setupCollectionView];

    [self requestAllListWithLoadMoreData:NO];
}

#pragma mark - > 刷新视图坐标
- (void)refreshFrame{
    
    CGFloat height = [askAnswerView getCurrentViewHeight];
    
    ScrollHeight = [askAnswerView getBarViewTopHeight];

    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+height+FWSafeBottom+5);

    askAnswerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
    
    self.collectionScr.frame = CGRectMake(0, CGRectGetMaxY(askAnswerView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-49-FWSafeBottom);
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH, 0);

    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView]];
}

#pragma mark - > 初始化collectionview
- (void)setupCollectionView{
    
    rootScrollView = [[FWMainTouchScrollView alloc] init];
    rootScrollView.frame  = CGRectMake(0, 0,SCREEN_WIDTH, SCREEN_HEIGHT);
    rootScrollView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    rootScrollView.delegate=self; //要遵循代理
    rootScrollView.showsVerticalScrollIndicator = NO;
    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);  //滚动大小
    [self.view addSubview:rootScrollView];
    
    askAnswerView = [[FWAskAndAnswerView alloc] init];
    askAnswerView.ankAndAnswerDelegate = self;
    askAnswerView.vc = self;
    askAnswerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 10);
    [rootScrollView addSubview:askAnswerView];
    
    self.collectionScr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(askAnswerView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-49-FWSafeBottom-50)];
    self.collectionScr.delegate = self;
    self.collectionScr.pagingEnabled = YES;
    self.collectionScr.showsVerticalScrollIndicator = NO;
    self.collectionScr.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [rootScrollView addSubview:self.collectionScr];
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH, 0);
    
    self.allLayout = [[FWBaseCollectionLayout alloc] init];
    self.allLayout.columns = 2;
    self.allLayout.rowMargin = 12;
    self.allLayout.colMargin = 12;
    self.allLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.allLayout.delegate = self;
    [self.allLayout autuContentSize];
    
    self.allCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.allLayout];
    self.allCollectionView.dataSource = self;
    self.allCollectionView.delegate = self;
    self.allCollectionView.showsVerticalScrollIndicator = NO;
    self.allCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.allCollectionView registerClass:[FWRefitCaseCollectionViewCell class] forCellWithReuseIdentifier:allCellId];
    self.allCollectionView.isNeedEmptyPlaceHolder = YES;
    self.allCollectionView.verticalOffset = -150;
    NSString * allTitle = @"还没有发布的作品哦~";
    NSDictionary * allAttributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * allAttributeString = [[NSMutableAttributedString alloc] initWithString:allTitle attributes:allAttributes];
    self.allCollectionView.emptyDescriptionString = allAttributeString;
    [self.collectionScr addSubview:self.allCollectionView];

    //利用initRoot创建 给予底部上下滑动的ScrollView 要滑动的ScrollRange 大小和左右滑动的Table数组
    thFollow = [[FWScrollViewFollowCollectionView alloc]init];
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView]];

    {
        @weakify(self);
        rootScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            @strongify(self);
            [self requestAllListWithLoadMoreData:NO];
        }];
        
        self.allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestAllListWithLoadMoreData:YES];
        }];
    }
}

#pragma mark - > 切换排序
- (void)changeSortWithType:(NSString *)sortType{
    self.order_type = sortType;
    [self requestAllListWithLoadMoreData:NO];
}


#pragma mark - > collectionView delegate & datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return allDataSource.count?allDataSource.count:0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWRefitCaseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:allCellId forIndexPath:indexPath];
    cell.viewController = self;
    
    if (allDataSource.count > 0) {
        
        FWFeedListModel * model = allDataSource[indexPath.item];
        [cell configForCell:model];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = allDataSource;
    FWFeedListModel * model = tempArr[indexPath.row];
    
    FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
    RCDVC.listModel = model;
    RCDVC.feed_id = model.feed_id;
//        DVC.myBlock = ^(FWFeedListModel *listModel) {
//            self.dataSource[index] = listModel;
//        };
    [self.navigationController pushViewController:RCDVC animated:YES];}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = allDataSource;
    
    if (indexPath.row >= tempArr.count) {
        return 0;
    }
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"2"]) {
        // 图文贴 固定比例3：4
        height = (cover_width *4)/3;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if([model.feed_type isEqualToString:@"3"]){
        // 文章帖 固定比例5：4
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if ([model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 20;
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 30;
        }
    }else  if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}
#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == rootScrollView) {
        if (scrollView.contentOffset.y >= ScrollHeight-5) {
//            self.title = allFeedModel.tag_info.tag_name;
        }else if (scrollView.contentOffset.y < ScrollHeight-5){
            self.title = @"";
        }
    }
    
    [thFollow followScrollViewScrollScrollViewScroll:scrollView];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}
@end
