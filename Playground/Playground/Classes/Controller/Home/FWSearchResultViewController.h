//
//  FWSearchResultViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/20.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 * 搜索结果页
 */
#import "FWBaseViewController.h"
#import "FWSearchResultNavigationView.h"
#import "FWSegementScrollView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWSearchResultViewController : FWBaseViewController

@property (nonatomic, strong) FWSearchResultNavigationView * navigationView;

@property (nonatomic, strong) NSString * query;

@property (nonatomic, strong) FWTableView * infoTableView;

/* 承载视图的scrollview*/
@property (nonatomic, strong) UIScrollView * mainScrollView;

/* 综合结果 */
@property (nonatomic, strong) FWCollectionView * resultCollectionView;

/* 闲置 */
@property (nonatomic, strong) FWCollectionView * idleCollectionView;

/* segement */
@property(nonatomic, strong) FWSegementScrollView * segement;

/* 首页segement下的标签数组 */
@property (nonatomic, strong) NSMutableArray * tagsListArray;

@property(nonatomic, strong) FWBaseCollectionLayout *resultLayout;

@property(nonatomic, strong) FWBaseCollectionLayout *idleLayout;

/* 记录当前点击的按钮 */
@property (nonatomic, assign) NSInteger  currentButton;

@property (nonatomic, assign) BOOL  isRefresh;

@property (nonatomic, strong) NSString * feed_type;


@end

NS_ASSUME_NONNULL_END
