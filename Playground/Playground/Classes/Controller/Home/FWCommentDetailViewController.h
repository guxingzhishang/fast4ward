//
//  FWCommentDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/15.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWCommentView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCommentDetailViewController : FWBaseViewController <UITableViewDelegate,UITableViewDataSource,FWCommentViewDelegate>

@property(nonatomic, strong) UITableView * infoTableView;

@property (nonatomic, strong) FWCommentView * commentView;

@property (nonatomic, strong) NSString * feed_id;

@property (nonatomic, strong) NSString * comment_id;

/* 是否是从消息页过来 */
@property (nonatomic, strong) NSString * fromMessage;

/**
 * 如果要获取指定ID的回复,可以传p_comment_id,默认为0
 */
@property (nonatomic, strong) NSString * p_comment_id;

@property (nonatomic, strong) NSString * r_comment_id;

/**
 * 要回复的comment_id,默认为0,要回复的任意帖子ID
 */
@property (nonatomic, strong) NSString * reply_comment_id;

/**
 * 要置顶显示的comment_id,从消息列表点击到详情页要用
 */
@property (nonatomic, strong) NSString * top_comment_id;

/**
 * 要置顶显示的comment_id,从消息列表点击到详情页要用
 */
@property (nonatomic, strong) NSString * r_uid;

/**
 * 评论总数
 */
@property (nonatomic, strong) NSString * total_count;

/**
 * 第几页
 */
@property (nonatomic, assign) NSInteger pageNum;

/**
 * 数据源
 */
@property (nonatomic, strong) NSMutableArray * commentDataSource;

@property (nonatomic, strong) FWFeedListModel * listModel;

@property (nonatomic, strong) FWMineInfoModel * replyInfo;

/* 1:消息评论页  2:查看更多评论*/
@property (nonatomic, strong) NSString * pageType;

/* 如果消息页进来，传feedType */
@property (nonatomic, strong) NSString * feedType;

@property(nonatomic,copy)void(^returnDataSource)(NSMutableArray *backDataSource);

@end

NS_ASSUME_NONNULL_END
