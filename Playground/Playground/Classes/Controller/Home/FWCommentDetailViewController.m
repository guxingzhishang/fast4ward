//
//  FWCommentDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/15.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWCommentDetailViewController.h"
#import "FWCommentCell.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWCommentMessageRequest.h"
#import "FWLikeRequest.h"
#import "FWCommentModel.h"
#import "FWCommentHeaderView.h"

@interface FWCommentDetailViewController ()<ShareViewDelegate,FWCommentCellDelegate,FWCommentHeaderViewDelegate>

@property (nonatomic, strong)FWCommentModel * commentModel;


@end

@implementation FWCommentDetailViewController
@synthesize infoTableView;
@synthesize commentView;
@synthesize feed_id;
@synthesize commentModel;
@synthesize pageNum;
@synthesize commentDataSource;

#pragma mark - > 请求评论列表
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    if (isLoadMoredData == NO) {
        pageNum = 1;
        
        if (self.commentDataSource.count > 0 ) {
            [self.commentDataSource removeAllObjects];
        }
    }else{
        pageNum +=1;
    }
    
    NSDictionary * params;
    NSString * action ;
    if ([self.pageType isEqualToString:@"1"]) {
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"feed_id":self.feed_id,
                   @"p_comment_id":self.p_comment_id,
                   @"r_comment_id":self.r_comment_id,
                   @"page":@(pageNum).stringValue,
                   @"page_size":@"20",
                   @"r_uid":self.r_uid,
                   };
        action = Get_comment_chat;
    }else if ([self.pageType isEqualToString:@"2"]){
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"feed_id":self.feed_id,
                   @"p_comment_id":self.top_comment_id,
                   @"page":@(pageNum).stringValue,
                   @"page_size":@"20",
                   @"top_comment_id":@"0",
                   };
        action = Get_comment_list;
    }
    
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            [self dealWithData:back];
            
        }else{
            
            if ([[back objectForKey:@"errmsg"] isEqualToString:@"当前帖子已经被删除"]) {
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"当前帖子已经被删除" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }
    }];
}

#pragma mark - > 处理数据
- (void)dealWithData:(NSDictionary *)back{
    
    commentModel = [FWCommentModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
    
    self.total_count = commentModel.total_count?commentModel.total_count:@"0";
    
    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (FWCommentListModel * model in commentModel.comment_list) {
        
        if (pageNum > 1) {
            [tempArr addObject:[self _topicFrameWithTopic:model]];
        }else{
            [self.commentDataSource addObject:[self _topicFrameWithTopic:model]];
        }
    }

    FWTopicCommentFrame * topicCommentFrame = (FWTopicCommentFrame *)self.commentDataSource.firstObject;

    
    if (pageNum >1 && [commentModel.comment_list count] > 0) {
      
        NSMutableArray * commentFrames = topicCommentFrame.commentFrames.mutableCopy;

        FWTopicCommentFrame * addCommentFrame = (FWTopicCommentFrame *)tempArr.firstObject;
        for (FWCommentFrame * comment in addCommentFrame.commentFrames) {
            
            [commentFrames addObject:comment];
        }
        
        topicCommentFrame.commentFrames = commentFrames.mutableCopy;
    }
    
    if([commentModel.comment_list count] == 0 &&
       pageNum != 1){
        [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
    }else{
        [infoTableView reloadData];
    }
}

#pragma mark - > 删除消息
- (void)requestDeleteMessageWithSection:(NSInteger)section WithRow:(NSInteger)row{
    
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.feed_id,
                              @"comment_id":self.comment_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_del_comment WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)self.commentDataSource[section];
            
            if (row >= 0) {
                [topicFrame.commentFrames removeObjectAtIndex:row];
                [UIView performWithoutAnimation:^{
                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
                    [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                }];
            }else{
                [self.commentDataSource removeObjectAtIndex:section];
                [self.infoTableView reloadData];
            }
            
            [[FWHudManager sharedManager] showSuccessMessage:@"删除成功" toController:self];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"评论详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"评论详情";
    
    self.pageNum = 0;

    self.commentDataSource = @[].mutableCopy;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self setupSubViews];
    [self requestDataWithLoadMoredData:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
     [self trackPageEnd:@"评论详情页"];
    DHWeakSelf;
    if (self.returnDataSource) {
        self.returnDataSource(weakSelf.commentDataSource);
    }
}

#pragma mark - > 视图初始化
- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.tag = CommentListTag;
//    infoTableView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-49-FWSafeBottom);
    }];
    
    commentView = [[FWCommentView alloc] init];
    commentView.delegate = self;
    commentView.type = 1;
    commentView.shareButton.hidden = YES;
    commentView.likesButton.hidden = YES;
    [self.view addSubview:commentView];
    [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.view).mas_offset(0);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_greaterThanOrEqualTo(50+FWSafeBottom);
    }];
    
    [commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(commentView).mas_offset(-15);
        make.height.mas_greaterThanOrEqualTo (30);

        if (SCREEN_HEIGHT >=812){
            make.left.top.mas_equalTo(commentView).mas_offset(15);
            make.bottom.mas_equalTo(commentView).mas_offset(-40);
        }else{
            make.left.top.mas_equalTo(commentView).mas_offset(10);
            make.bottom.mas_equalTo(commentView).mas_offset(-10);
        }
    }];
    
    NSString * likeCount = self.listModel.count_realtime.like_count_format?self.listModel.count_realtime.like_count_format:@"0";
    NSString * shareCount = self.listModel.count_realtime.share_count?self.listModel.count_realtime.share_count:@"0";
    
    [commentView.likesButton setTitle:[NSString stringWithFormat:@"   %@",likeCount] forState:UIControlStateNormal];
    [commentView.shareButton setTitle:[NSString stringWithFormat:@"   %@",shareCount] forState:UIControlStateNormal];
    
    if ([self.listModel.is_liked isEqualToString:@"1"]) {
        [commentView.likesButton setImage:[UIImage imageNamed:@"car_detail_like"] forState:UIControlStateNormal];
    }else{
        [commentView.likesButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
    }
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.commentDataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.commentFrames.count;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];

    id model = self.commentDataSource[indexPath.section];
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
    FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
    
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:commentFrame.comment.uid]) {
        /* 是本人，提醒删除 */
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.comment_id = commentFrame.comment.comment_id;
            [self requestDeleteMessageWithSection:indexPath.section WithRow:indexPath.row];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        /* 不是本人，回复 */
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
            self.replyInfo = commentFrame.comment.reply_user_info;
            self.reply_comment_id = commentFrame.comment.comment_id;

            [self.commentView.contentView becomeFirstResponder];
            self.commentView.contentView.placeholder = [NSString stringWithFormat:@"回复:%@",commentFrame.comment.user_info.nickname];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id model = self.commentDataSource[indexPath.section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentCell *cell = [FWCommentCell cellWithTableView:tableView WithID:@"CommentDetailID"];
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
        cell.commentFrame = commentFrame;
//        cell.backgroundColor = FWViewBackgroundColor_F0F0F0;
//        cell.contentView.backgroundColor = FWViewBackgroundColor_F0F0F0;
        cell.backgroundColor = FWViewBackgroundColor_FFFFFF;
        cell.contentView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        cell.likesButton.tag = indexPath.row + 98765;
        cell.tag = indexPath.section + 87654;
        cell.delegate = self;
        cell.viewcontroller = self;
        
        return cell;
    }
    
    return [[UITableViewCell alloc] initWithFrame:CGRectZero];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id model = self.commentDataSource[indexPath.section];
    if ([model isKindOfClass:[FWTopicCommentFrame class]]) {
        FWTopicCommentFrame *videoTopicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = videoTopicFrame.commentFrames[indexPath.row];
        
        if (indexPath.row == videoTopicFrame.commentFrames.count-1 &&
            self.commentDataSource.count != 0) {
            return commentFrame.cellHeight+10;
        }
        return commentFrame.cellHeight;
    }
    
    return .1f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentHeaderView *headerView = [FWCommentHeaderView headerViewWithTableView:tableView];
        headerView.commentButton.tag = section +30000;
        headerView.thumbBtn.tag = section +66666;
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        headerView.topicFrame = topicFrame;
        headerView.viewcontroller = self;
        headerView.delegate = self;
        
        if (topicFrame.topic.reply_list.count > 0) {
            UILabel * allLabel = [[UILabel alloc] initWithFrame:CGRectMake(14, topicFrame.height + 3, 100, 22)];
            allLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
            allLabel.text = @"全部回复";
            allLabel.textColor = FWTextColor_272727;
            allLabel.textAlignment = NSTextAlignmentLeft;
            [headerView addSubview:allLabel];
        }
        
        return headerView;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [[UIView alloc] init];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        
        if (topicFrame.topic.reply_list.count > 0) {
            return topicFrame.height + 30;
        }
        return topicFrame.height;
    }
    
    return .1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.5;
}

#pragma mark - > 主评论点赞
- (void)_thumbBtnClicked:(FWTopicCommentFrame *)topicCommentFrame WithIndex:(NSInteger)index{

    [self.commentDataSource replaceObjectAtIndex:index withObject:topicCommentFrame];
}

#pragma mark - > 子评论/回复 点赞
- (void)likesButtonClickWithCommentFrame:(FWCommentFrame *)commentFrame WithIndex:(NSInteger)index withcommentCell:(FWCommentCell *)commentCell{
    
    NSInteger section = commentCell.tag - 87654;
    
    FWTopicCommentFrame * topicFarme = self.commentDataSource[section];
    FWReplyComment * comment = topicFarme.topic.reply_list[index];
    comment.is_liked = commentFrame.comment.is_liked;
    comment.like_count_format = commentFrame.comment.like_count_format;

}

#pragma mark - > 分享
- (void)shareButtonClick{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {

        ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
        [shareView setupQRViewWithModel:self.listModel];
        [shareView showView];
    }
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0){
            
            [[ShareManager defaultShareManager] shareToMinProgramObjectWithModel:self.listModel];
        }else if (index == 1){
            
            UIImageView * imageView = [[ShareView defaultShareView] getCurrenImage];
            [[ShareManager defaultShareManager] shareToPengyouquan:imageView WithFeedID:self.listModel.feed_id WithType:Share_feed_id];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}

#pragma mark - > 发送消息
- (void)sendMessageClick{
    
    [self checkLogin];
    
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSString *contentString = commentView.contentView.text;
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.feed_id,
                                  @"content":contentString,
                                  @"p_comment_id":self.p_comment_id,
                                  @"reply_comment_id":self.reply_comment_id,
                                  };
        
        
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        request.isNeedShowHud = YES;
        [request startWithParameters:params WithAction:Submit_add_comment  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                commentView.contentView.text = @"";
                commentView.contentView.placeholder = @"点赞都是套路，评论才是真情";
                [commentView refreshContentView:commentView.contentView];
                [self.view endEditing:YES];
                self.reply_comment_id = @"0";
                [self requestDataWithLoadMoredData:NO];
                
                [[FWHudManager sharedManager] showErrorMessage:@"评论成功" toController:self];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 评论主评论
- (void)commentButtonClick:(NSInteger)index{
    
    [self.view endEditing:YES];
    
    id model = self.commentDataSource[index];
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;

    if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:topicFrame.topic.uid]) {
        /* 是本人，提醒删除 */
        if ([self.pageType isEqualToString:@"1"]) {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.comment_id = topicFrame.topic.comment_id;
                [self requestDeleteMessageWithSection:index WithRow:-1];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }else{
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
         
            self.reply_comment_id = topicFrame.topic.comment_id;
            
            [self.commentView.contentView becomeFirstResponder];
            self.commentView.contentView.placeholder = [NSString stringWithFormat:@"回复:%@",topicFrame.topic.user_info.nickname];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 发送评论
-(void)sendContentText:(NSString *)content{
    
    if (content.length<=0) {
        [[FWHudManager sharedManager] showErrorMessage:@"说点什么吧~" toController:self];
        [self.view endEditing:YES];
        return;
    }
    
    [self sendMessageClick];
}

#pragma mark - > 点击回复的姓名
- (void)commentCell:(FWCommentCell *)commentCell didClickedUser:(FWMineInfoModel *)user{
    
    if (nil == user.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = user.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > keyboardNotification
-(void)keyboardShow:(NSNotification *)note{
    
    if (self.commentDataSource.count <= 0) {
        return;
    }
    CGRect keyBoardRect=[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat deltaY=keyBoardRect.size.height;
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        commentView.transform=CGAffineTransformMakeTranslation(0, -deltaY);
        
        [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.view).mas_offset(0);
            make.left.right.mas_equalTo(self.view);
            if (SCREEN_HEIGHT >= 812) {
                make.height.mas_greaterThanOrEqualTo(60);
            }else{
                make.height.mas_greaterThanOrEqualTo(50);
            }
        }];
        
        [commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            if (SCREEN_HEIGHT >= 812) {
                make.left.top.mas_equalTo(commentView).mas_offset(15);
            }else{
                make.left.top.mas_equalTo(commentView).mas_offset(10);
            }
            
            make.right.mas_equalTo(commentView).mas_offset(-15);
            make.height.mas_greaterThanOrEqualTo (30);
        }];
    }];
}

-(void)keyboardHide:(NSNotification *)note{
    
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        commentView.transform=CGAffineTransformIdentity;
        
        self.reply_comment_id = @"0";
        
        [commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.view).mas_offset(0);
            make.left.right.mas_equalTo(self.view);
            make.height.mas_greaterThanOrEqualTo(50+FWSafeBottom);
        }];
        
        [commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (SCREEN_HEIGHT >=812){
                make.left.mas_equalTo(commentView).mas_offset(15);
                make.top.mas_equalTo(commentView).mas_offset(15);
                make.right.mas_equalTo(commentView).mas_offset(-15);
                make.height.mas_equalTo (30);
            }
            else{
                make.left.top.mas_equalTo(commentView).mas_offset(10);
                make.right.mas_equalTo(commentView).mas_offset(-15);
                make.height.mas_equalTo (30);
            }
        }];
        
        commentView.contentView.text=@"";
        if(commentView.contentView.text.length<=0){
            commentView.contentView.placeholder = @"点赞都是套路，评论才是真情";
        }
    } completion:^(BOOL finished) {
        
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [self.view endEditing:YES];
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

#pragma mark - 辅助方法
/** topic --- topicFrame */
- (FWTopicCommentFrame *)_topicFrameWithTopic:(FWCommentListModel *)topic
{
    if (self.feedType) {
        topic.feed_type = self.feedType;
    }
    
    FWTopicCommentFrame *topicFrame = [[FWTopicCommentFrame alloc] initWithType:@"2"];
    topicFrame.topic = topic;
    
    return topicFrame;
}

- (void)textViewBeginEditing{
    
    for (int i =0; i<self.commentDataSource.count; i++) {
        FWTopicCommentFrame * topicFrame = self.commentDataSource[i];
        
        NSString * user_Id = topicFrame.topic.user_info.uid;
        
        if ([user_Id isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            continue;
        }else{
            self.replyInfo = topicFrame.topic.user_info;
            if (![self.reply_comment_id isEqualToString:@"0"] && self.reply_comment_id.length >0) {
                // do nothing
            }else{
                // 直接回复一级评论
                self.reply_comment_id = topicFrame.topic.comment_id;
            }
            commentView.contentView.placeholder = [NSString stringWithFormat:@"回复: %@",self.replyInfo.nickname];
            return;
        }
    }
}

@end
