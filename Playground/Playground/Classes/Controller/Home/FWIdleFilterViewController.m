//
//  FWIdleFilterViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/17.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWIdleFilterViewController.h"
#import "FWMyworksCollectionCell.h"
#import "FWMyVideoCollectionCell.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWTagsRequest.h"
#import "FWTagsFeedModel.h"
#import "FWScrollViewFollowCollectionView.h"
#import "FWMainTouchScrollView.h"
#import "FWMyPicsCollectionCell.h"
#import "FWIdleDetailViewController.h"

static NSString * const allCellId = @"allCellId";

@interface FWIdleFilterViewController ()<ShareViewDelegate,FWIdleFiterViewDelegate>
{
    FWMainTouchScrollView *rootScrollView; //rootScrollView要有滑动穿透
    FWScrollViewFollowCollectionView *thFollow; //必须写成属性
    NSInteger  ScrollHeight;//需三处保持一致

    FWFeedModel * allFeedModel;

    NSMutableArray * allDataSource;

    NSInteger   allPageNum;
}


@end

@implementation FWIdleFilterViewController
@synthesize tagsView;
@synthesize allCollectionView;
@synthesize collectionScr;

#pragma mark - > ******************* 网络请求 *******************
#pragma mark - > 获取该该标签全部列表信息
- (void)requestAllListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        allPageNum = 1;
        
        if (allDataSource.count > 0 ) {
            [allDataSource removeAllObjects];
        }
    }else{
        allPageNum += 1;
    }
    
    FWTagsRequest * request = [[FWTagsRequest alloc] init];
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"province_code":self.province_code,
                             @"city_code":self.city_code,
                             @"county_code":self.county_code,
                             @"if_quanxin":self.if_quanxin,
                             @"if_ziti":self.if_ziti,
                             @"freight":self.freight,
                             @"order_type":self.order_type,
                             @"xianzhi_category_id":self.xianzhi_category_id,
                             @"page":@(allPageNum).stringValue,
                             @"page_size":@"20",
                             };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_xianzhi  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            allFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [allDataSource addObjectsFromArray: allFeedModel.feed_list];
            
            if([allFeedModel.feed_list count] == 0 &&
               allPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.allCollectionView reloadData];
            }
            
            [tagsView congifViewWithModel:allFeedModel];


            if (isLoadMoredData) {
                [allCollectionView.mj_footer endRefreshing];
            }else{
                [allCollectionView scrollToTopAnimated:YES];
                [rootScrollView.mj_header endRefreshing];
            }

            [self refreshFrame];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    [self trackPageBegin:@"闲置筛选页"];

    [self.navigationController.navigationBar setShadowImage:[FWClearColor image]];

    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {
        
        int tempNum = -1;
        for (int i = 0; i < allDataSource.count; i++) {
            FWFeedListModel * tempModel = allDataSource[i];
            
            if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                tempNum = i;
            }
        }
        
        if (tempNum >=0) {
            [allDataSource removeObjectAtIndex:tempNum];
            [allCollectionView reloadData];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"闲置筛选页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    allDataSource = @[].mutableCopy;
    
    allPageNum = 0;
    ScrollHeight = 80;
    
    self.province_code = @"";
    self.city_code = @"";
    self.county_code = @"";
    self.if_quanxin = @"";
    self.if_ziti = @"";
    self.freight = @"";
    self.order_type = @"";
    
    [self setupSubviews];
    [self setupCollectionView];

    [self requestAllListWithLoadMoreData:NO];
}

#pragma mark - > 视图初始化
- (void)setupSubviews{
    
    UIButton * shareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [shareButton setImage:[UIImage imageNamed:@"new_black_share"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        shareButton.hidden = NO;
    }else{
        shareButton.hidden = YES;
    }
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
}

#pragma mark - > 刷新视图坐标
- (void)refreshFrame{
    
    CGFloat height = [tagsView getCurrentViewHeight];
    
    ScrollHeight = [tagsView getBarViewTopHeight];

    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+height);

    tagsView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
    
    self.collectionScr.frame = CGRectMake(0, CGRectGetMaxY(tagsView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-90);
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH, 0);

    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView]];
}

#pragma mark - > 初始化collectionview
- (void)setupCollectionView{
    
    rootScrollView = [[FWMainTouchScrollView alloc] init];
    rootScrollView.frame  = CGRectMake(0, 0,SCREEN_WIDTH, SCREEN_HEIGHT);
    rootScrollView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    rootScrollView.delegate=self; //要遵循代理
    rootScrollView.showsVerticalScrollIndicator = NO;
    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);  //滚动大小
    [self.view addSubview:rootScrollView];
    
    tagsView = [[FWIdleFiterView alloc] init];
    tagsView.delegate = self;
    tagsView.vc = self;
    tagsView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 230);
    [rootScrollView addSubview:tagsView];
    
    self.collectionScr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(tagsView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-90)];
    self.collectionScr.delegate = self;
    self.collectionScr.pagingEnabled = YES;
    self.collectionScr.showsVerticalScrollIndicator = NO;
    self.collectionScr.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [rootScrollView addSubview:self.collectionScr];
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH*3, 0);
    
    self.allLayout = [[FWBaseCollectionLayout alloc] init];
    self.allLayout.columns = 2;
    self.allLayout.rowMargin = 12;
    self.allLayout.colMargin = 12;
    self.allLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.allLayout.delegate = self;
    [self.allLayout autuContentSize];
    
    self.allCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.allLayout];
    self.allCollectionView.dataSource = self;
    self.allCollectionView.delegate = self;
    self.allCollectionView.showsVerticalScrollIndicator = NO;
    self.allCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.allCollectionView registerClass:[FWMyworksCollectionCell class] forCellWithReuseIdentifier:allCellId];
    self.allCollectionView.isNeedEmptyPlaceHolder = YES;
    self.allCollectionView.verticalOffset = -150;
    NSString * allTitle = @"还没有发布的作品哦~";
    NSDictionary * allAttributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * allAttributeString = [[NSMutableAttributedString alloc] initWithString:allTitle attributes:allAttributes];
    self.allCollectionView.emptyDescriptionString = allAttributeString;
    [self.collectionScr addSubview:self.allCollectionView];
    
    //利用initRoot创建 给予底部上下滑动的ScrollView 要滑动的ScrollRange 大小和左右滑动的Table数组
    thFollow = [[FWScrollViewFollowCollectionView alloc]init];
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView]];

    {
        @weakify(self);
        rootScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            @strongify(self);
            [self requestAllListWithLoadMoreData:NO];
        }];
        
        self.allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestAllListWithLoadMoreData:YES];
        }];
    }
}

#pragma mark - > 分享
- (void)shareBtnOnClick{
    
    ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
    [shareView setupTopicQRViewWithModel:allFeedModel];
    [shareView showView];
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0)
        {
            [[ShareManager defaultShareManager] topicShareToMinProgramObjectWithModel:allFeedModel];
        }else if (index == 1){
            UIImageView * imageView = [[ShareView defaultShareView] getCurrenImage];
            [[ShareManager defaultShareManager] shareToPengyouquan:imageView WithFeedID:allFeedModel.tag_info.tag_id WithType:Share_tag_id];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}

#pragma mark - > collectionView delegate & datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger number = allDataSource.count?allDataSource.count:0;
    return number;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWMyworksCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:allCellId forIndexPath:indexPath];
    cell.viewController = self;
    
    if (allDataSource.count > 0) {
        
        FWFeedListModel * model = allDataSource[indexPath.item];
        [cell configForCell:model];
        
        if (model.h5_url.length > 0 ||
            [model.flag_create isEqualToString:@"1"]) {
            
            cell.activityImageView.hidden = NO;
            
            NSString * imageName = @"activity";
            CGSize imageViewSize = CGSizeMake(30, 15);
            
            if ([model.flag_create isEqualToString:@"1"]){
                // 发起者
                imageName = @"faqizhe";
                imageViewSize = CGSizeMake(50, 21);
            }
            
            cell.activityImageView.image = [UIImage imageNamed:imageName];
            [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                make.size.mas_equalTo(imageViewSize);
            }];
        }else{
            cell.activityImageView.hidden = YES;
        }
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = allDataSource;
    
    FWFeedListModel * model = tempArr[indexPath.row];
    
    FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
    RCDVC.listModel = model;
    RCDVC.feed_id = model.feed_id;
    [self.navigationController pushViewController:RCDVC animated:YES];
}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = allDataSource;
    
    if (indexPath.row >= tempArr.count) {
        return 0;
    }
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == rootScrollView) {
        if (scrollView.contentOffset.y >= ScrollHeight-5) {
            self.title = allFeedModel.tag_info.tag_name;
        }else if (scrollView.contentOffset.y < ScrollHeight-5){
            self.title = @"";
        }
    }
    
    [thFollow followScrollViewScrollScrollViewScroll:scrollView];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

#pragma mark - > ************ 筛选闲置delegate  *******************
-(void)allNewClick:(NSString *)if_quanxin{
    self.if_quanxin = if_quanxin;
    [self requestAllListWithLoadMoreData:NO];
}

- (void)zitiClick:(NSString *)if_ziti{
    self.if_ziti = if_ziti;
    [self requestAllListWithLoadMoreData:NO];
}

- (void)baoyouClick:(NSString *)freight{
    self.freight = freight;
    [self requestAllListWithLoadMoreData:NO];
}

- (void)doneOneClick:(NSString *)order_type{
    self.order_type = order_type;
    [self requestAllListWithLoadMoreData:NO];
}

- (void)doneTwoProvinceCode:(NSString *)province_code CityCode:(NSString *)city_code CountyCode:(NSString *)county_code{
    
    self.province_code = province_code;
    self.city_code = city_code;
    self.county_code = county_code;

    [self requestAllListWithLoadMoreData:NO];

}
@end
