//
//  FWIdleFilterViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/17.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

/*
 *  筛选闲置
 */
#import "FWBaseViewController.h"
#import "FWIdleFiterView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWIdleFilterViewController : FWBaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FWBaseCollectionLayoutDalegate>

@property (nonatomic, strong) FWIdleFiterView * tagsView;

/* 承载collectionView的背景Scrollview */
@property (nonatomic, strong) UIScrollView * collectionScr;

/* 全部列表 */
@property (nonatomic, strong) FWCollectionView * allCollectionView;

/* 视频列表 */
@property (nonatomic, strong) FWCollectionView * videoCollectionView;

/* 图片列表 */
@property (nonatomic, strong) FWCollectionView * picsCollectionView;

/* 参与活动 */
@property (nonatomic, strong) UIButton * participateButton;

/* 标记是哪一个列表 */
@property (nonatomic, assign) FWTagsCollectionViewType collectionType;

/* 上一页面传来的id */
@property (nonatomic, strong) FWSearchTagsSubListModel * tagsModel;

@property(nonatomic, strong) FWBaseCollectionLayout *allLayout;

/* 用作TD事件分析 1：发现页跳转 2：帖子页跳转 3：搜索推荐页跳转 4：搜索结果页跳转 */
@property (nonatomic, assign) NSInteger traceType;

@property (nonatomic, strong) NSString * province_code;
@property (nonatomic, strong) NSString * city_code;
@property (nonatomic, strong) NSString * county_code;

@property (nonatomic, strong) NSString * if_quanxin;
@property (nonatomic, strong) NSString * if_ziti;
@property (nonatomic, strong) NSString * freight;
@property (nonatomic, strong) NSString * order_type;

@property (nonatomic, strong) NSString * xianzhi_category_id;
@end

NS_ASSUME_NONNULL_END
