//
//  FWChooseCarStyleViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWChooseCarStyleViewController.h"
#import "FWChooseCarStyleModel.h"
#import "FWPublishPreviewRefitViewController.h"
#import "FWAddCarViewController.h"

@interface FWChooseCarStyleViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, assign) NSInteger   pageNum;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWChooseCarStyleModel * styleModel;

@end

@implementation FWChooseCarStyleViewController
@synthesize infoTableView;
@synthesize pageNum;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)requestList{
    
    if (self.brand.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"获取品牌出现错误，请退出重新操作" toController:self];
        return;
    }
    
    if (self.category.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"获取车系分类出现错误，请退出重新操作" toController:self];
        return;
    }
    
    if (self.type.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"获取车系名称出现错误，请退出重新操作" toController:self];
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"brand":self.brand,
                              @"category":self.category,
                              @"type":self.type,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_car_style  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if (self.dataSource.count > 0 ) {
                [self.dataSource removeAllObjects];
            }
            
            self.styleModel = [FWChooseCarStyleModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.dataSource addObjectsFromArray: self.styleModel.list];
            [infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"选择车系页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"选择车系页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"选择车系";
    
    [self setupSubViews];
    [self requestList];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.sectionIndexColor = FWTextColor_969696;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.dataSource.count > section) {
        FWChooseCarStyleListModel * listModel = self.dataSource[section];
        return listModel.list.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"ChooseCarTypeID";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.dataSource.count > indexPath.section) {
        FWChooseCarStyleListModel * listModel = self.dataSource[indexPath.section];
        if (listModel.list.count > indexPath.row) {
            FWChooseCarStyleSubListModel * subListModel = listModel.list[indexPath.row];
            cell.textLabel.text = subListModel.style;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (self.dataSource.count > indexPath.section) {
        FWChooseCarStyleListModel * listModel = self.dataSource[indexPath.section];
        if (listModel.list.count > indexPath.row) {
            
            FWChooseCarStyleSubListModel * subListModel = listModel.list[indexPath.row];
            for (UIViewController * vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[FWPublishPreviewRefitViewController class]]) {
                    
                    FWPublishPreviewRefitViewController * prvc = (FWPublishPreviewRefitViewController *)vc;
                    prvc.subListModel = subListModel;
                    [self.navigationController popToViewController:vc animated:YES];
                    return ;
                }
            }
            
            for (UIViewController * vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[FWAddCarViewController class]]) {
                    
                    FWAddCarViewController * prvc = (FWAddCarViewController *)vc;
                    prvc.subListModel = subListModel;
                    [self.navigationController popToViewController:vc animated:YES];
                    return ;
                }
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
   
    if (self.dataSource.count > section) {
        FWChooseCarStyleListModel * listModel = self.dataSource[section];
        return listModel.parameter;
    }
    
    return @"";
}

@end
