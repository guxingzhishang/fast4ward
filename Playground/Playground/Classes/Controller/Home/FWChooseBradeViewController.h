//
//  FWChooseBradeViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWChooseCarBrandModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^brandBlock)(NSString * brandString);

@interface FWChooseBradeViewController : FWBaseViewController
@property (nonatomic, strong) NSString * chooseFrom; // publish: 发布页; anchexi: 按车系查看； gaizhuang:编辑改装；  chooseChexi 选择车系（发布改装页） ;addCar : 添加座驾
@property (nonatomic, strong) NSString * type;
@property (nonatomic, copy) brandBlock myBlock;

@end

@interface FWChooseBradeTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) NSString * chooseFrom; // publish: 发布页; anchexi: 按车系查看； gaizhuang:编辑改装；  chooseChexi 选择车系（发布改装页） ; addCar : 添加座驾

- (void)configforCell:(FWChooseCarBrandSubListModel *)model;
@end

NS_ASSUME_NONNULL_END
