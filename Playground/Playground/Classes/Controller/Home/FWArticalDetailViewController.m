//
//  FWArticalDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWArticalDetailViewController.h"
#import "FWArticalDetailHeaderView.h"
#import "FWCommentCell.h"
#import "FWEmptyView.h"
#import "FWTopicCommentFrame.h"

#import "FWFollowRequest.h"
#import "FWViewPictureViewController.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWCommentMessageRequest.h"
#import "FWCommentModel.h"
#import "SharePopView.h"
#import "FWCommentHeaderView.h"
#import "FWCommentDetailViewController.h"

#import <PassKit/PassKit.h>                                 //用户绑定的银行卡信息
#import <PassKit/PKPaymentAuthorizationViewController.h>    //Apple pay的展示控件
#import <AddressBook/AddressBook.h>                         //用户联系信息相关
#import <StoreKit/StoreKit.h>

@interface FWArticalDetailViewController ()<UITableViewDelegate,UITableViewDataSource,ShareViewDelegate,FWCommentCellDelegate,FWCommentViewDelegate,FWCommentHeaderViewDelegate>

@property (nonatomic, assign) CGFloat alphaValue;
@property (nonatomic, strong) UIView * naviBarView;
@property (nonatomic, strong) UIButton * blackBackButton;

@property (nonatomic, strong) FWArticalDetailHeaderView * headerView;
@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) FWFeedModel * articalModel;
@property (nonatomic, strong) FWCommentView * commentView;

@property (nonatomic, strong) FWCommentModel * commentModel;
@property (nonatomic, strong) NSString * hostComment_id;
/** 选中的话题尺寸模型 */
@property (nonatomic , assign) NSInteger selectedSection;

@end

@implementation FWArticalDetailViewController

#pragma mark - > 请求文章详情
- (void)requestArticalDetail{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.feed_id,
                              @"watermark":@"1",
                              };
    
    [request startWithParameters:params WithAction:Get_feed_info_by_id WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.articalModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            if ([self.articalModel.feed_info.flag_hidden isEqualToString:@"2"]) {
                
                [self configWithModel];
            }else if ([self.articalModel.feed_info.flag_hidden isEqualToString:@"1"]){
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:self.articalModel.feed_info.flag_hidden_msg preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求评论列表
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.commentDataSource.count > 0 ) {
            [self.commentDataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.feed_id,
                              @"p_comment_id":self.p_comment_id,
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"10",
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_comment_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [self.infoTableView.mj_footer endRefreshing];
            }else{
                [self.infoTableView.mj_header endRefreshing];
            }
            
            [self dealWithDatas:back];
            
        }else{
            if ([[back objectForKey:@"errno"] isEqualToString:@"-2"] &&
                [[back objectForKey:@"errmsg"] isEqualToString:@"当前帖子状态不正常,没有审核通过"]) {
                return ;
            }
            if ([[back objectForKey:@"errno"] isEqualToString:@"-2"] &&
                [[back objectForKey:@"errmsg"] isEqualToString:@"当前帖子已经被删除"]) {
                return ;
            }
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 处理数据
- (void)dealWithDatas:(NSDictionary *)back{
    
    self.commentModel = [FWCommentModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
    
    if (self.commentModel.comment_list.count > 0) {
        
        self.infoTableView.tableFooterView = nil;
    }else{
        if (self.pageNum == 1) {
            
            FWEmptyView * footerView = [[FWEmptyView alloc] init];
            footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 400);
            [footerView settingWithImageName:@"empty" WithTitle:@"还没有相关的内容哦~"];
            self.infoTableView.tableFooterView = footerView;
        }
    }
    
    self.total_count = self.commentModel.all_comment_count?self.commentModel.all_comment_count:@"0";
    
    for (FWCommentListModel * model in self.commentModel.comment_list) {
        [self.commentDataSource addObject:[self _topicFrameWithTopic:model]];
    }
    
    if([self.commentModel.comment_list count] == 0 &&
       self.pageNum != 1){
        [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
    }else{
        [self.infoTableView reloadData];
    }
}

#pragma mark - > 删除消息
- (void)requestDeleteMessageWithSection:(NSInteger)section WithRow:(NSInteger)row{
    
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.commentModel.feed_id,
                              @"comment_id":self.comment_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_del_comment WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            
            FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)self.commentDataSource[section];
            
            if (row >= 0) {
                [topicFrame.commentFrames removeObjectAtIndex:row];
                [UIView performWithoutAnimation:^{
                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
                    [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                }];
                
                self.total_count = @([self.total_count integerValue] -1).stringValue;
            }else{
                self.total_count = @([self.total_count integerValue] -[topicFrame.topic.reply_total_count integerValue]-1).stringValue;
                
                [self.commentDataSource removeObjectAtIndex:section];
                [self.infoTableView reloadData];
            }
            
            [self showCommentCount:self.total_count];
            
            [[FWHudManager sharedManager] showSuccessMessage:@"删除成功" toController:self];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 显示共多少评论
- (void)showCommentCount:(NSString *)count{
    
    [self.headerView.numberButton setTitle:[NSString stringWithFormat:@"%@ 条评论",self.total_count] forState:UIControlStateNormal];
}

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"文章详情页"];

    self.navigationController.navigationBar.alpha = self.alphaValue;

    if (self.selectedSection>=0) {
        [self _reloadSelectedSectin];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"文章详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.alphaValue = 0;
    self.pageNum = 0;
    self.p_comment_id = @"0";
    self.selectedSection = -1;
    self.currentRow = -1;
    self.currentSection = -1;
    
    self.hostComment_id = self.p_comment_id;
    
    self.commentDataSource = @[].mutableCopy;
    self.backDataSource = @[].mutableCopy;
    
    self.fd_prefersNavigationBarHidden = YES;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self requestArticalDetail];
    
    [self setupSubViews];

    [self requestDataWithLoadMoredData:NO];
}


- (void)setupSubViews{
    
    self.naviBarView = [[UIView alloc] init];
    self.naviBarView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.naviBarView.alpha = 0;
    self.naviBarView.hidden = YES;
    [self.view addSubview:self.naviBarView];
    self.naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    
    self.blackBackButton = [[UIButton alloc] init];
    [self.blackBackButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [self.blackBackButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.blackBackButton];
    [self.blackBackButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.naviBarView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(self.naviBarView).mas_offset(30+FWCustomeSafeTop);
    }];
    self.blackBackButton.hidden = YES;
    
    self.infoTableView = [[FWTableView alloc] init];
    self.infoTableView.delegate = self;
    self.infoTableView.dataSource = self;
    self.infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.infoTableView.showsVerticalScrollIndicator = NO;
    self.infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.infoTableView];
    [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-49-FWSafeBottom);
    }];
    
    if (@available(iOS 11.0, *)) {
        self.infoTableView.estimatedSectionFooterHeight = 0;
        self.
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self.view bringSubviewToFront:self.naviBarView];

    self.commentView = [[FWCommentView alloc] init];
    self.commentView.delegate = self;
    self.commentView.type = 2;
    self.commentView.feed_type = @"3";
    [self.view addSubview:self.commentView];
    [self.commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.bottom.mas_equalTo(self.view).mas_offset(0);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_greaterThanOrEqualTo(50+FWSafeBottom);
    }];
    
    @weakify(self);
    self.infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:NO];
    }];
    
    self.headerView = [[FWArticalDetailHeaderView alloc] init];
    self.headerView.vc = self;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 1);
    self.infoTableView.tableHeaderView = self.headerView;
    
    [self configWithModel];
}

- (void)configWithModel{
    
    NSString * likeCount = self.articalModel.feed_info.count_realtime.like_count_format?self.articalModel.feed_info.count_realtime.like_count_format:@"0";
    NSString * shareCount = self.articalModel.feed_info.count_realtime.share_count?self.articalModel.feed_info.count_realtime.share_count:@"0";
    
    [self.commentView.likesButton setTitle:[NSString stringWithFormat:@"   %@",likeCount] forState:UIControlStateNormal];
    [self.commentView.shareButton setTitle:[NSString stringWithFormat:@"   %@",shareCount] forState:UIControlStateNormal];
    
    if ([self.articalModel.feed_info.is_liked isEqualToString:@"1"]) {
        [self.commentView.likesButton setImage:[UIImage imageNamed:@"car_detail_like"] forState:UIControlStateNormal];
    }else{
        [self.commentView.likesButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
    }
    
    [self.headerView dealWithData:self.articalModel];
    
    CGFloat viewHeight = self.headerView.viewHeight;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, viewHeight);
    self.infoTableView.tableHeaderView = self.headerView;
}

#pragma mark - > UITableView delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.commentDataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.commentFrames.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id model = self.commentDataSource[indexPath.section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentCell *cell = [FWCommentCell cellWithTableView:tableView WithID:@"CommentID"];
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
        cell.commentFrame = commentFrame;
        cell.viewcontroller = self;
        cell.delegate = self;
        
        if (indexPath.row > 2) {
            cell.photoImageView.image = [UIImage imageNamed:@""];
        }
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    
    self.selectedSection = indexPath.section;
    
    id model = self.commentDataSource[indexPath.section];
    
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
    FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
    
    if (indexPath.row == topicFrame.commentFrames.count-1  &&
        [commentFrame.comment.content isEqualToString:MoreReply]) {
        FWCommentDetailViewController * CDVC = [[FWCommentDetailViewController alloc] init];
        CDVC.pageType = @"2";
        CDVC.feed_id = commentFrame.comment.feed_id?commentFrame.comment.feed_id:@"";
        /*就是要取top_comment_id(因为cell最后一行显示的固定文案，
         所以最后一行comment_id赋值了别的) */
        CDVC.p_comment_id = commentFrame.comment.top_comment_id?commentFrame.comment.top_comment_id:@"0";
        CDVC.top_comment_id = commentFrame.comment.top_comment_id?commentFrame.comment.top_comment_id:@"0";
        DHWeakSelf;
        CDVC.returnDataSource = ^(NSMutableArray * _Nonnull backDataSource) {
            weakSelf.backDataSource = backDataSource;
        };
        [self.navigationController pushViewController:CDVC animated:YES];
    }else{
        
        if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:commentFrame.comment.uid]) {
            /* 是本人，提醒删除 */
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.comment_id = commentFrame.comment.comment_id;
                [self requestDeleteMessageWithSection:indexPath.section WithRow:indexPath.row];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }else{
            /* 不是本人，回复 */
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.p_comment_id = topicFrame.topic.comment_id;
                self.reply_comment_id = commentFrame.comment.comment_id;
                
                [self.commentView.contentView becomeFirstResponder];
                self.commentView.contentView.placeholder = [NSString stringWithFormat:@"回复:%@",commentFrame.comment.user_info.nickname];
                /* 回复 */
                self.reply_userInfo = commentFrame.comment.user_info;
                self.currentSection = indexPath.section;
                self.currentRow = indexPath.row;
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentHeaderView *headerView = [FWCommentHeaderView headerViewWithTableView:tableView];
        headerView.commentButton.tag = section +30000;
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        headerView.topicFrame = topicFrame;
        headerView.viewcontroller = self;
        headerView.delegate = self;
        return headerView;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    if ([self.total_count integerValue] >10 &&
        section == self.commentDataSource.count-1) {
        UIButton * moreButton = [[UIButton alloc] init];
        moreButton.titleLabel.font = DHSystemFontOfSize_14;
        [moreButton setTitle:@"查看更多评论" forState:UIControlStateNormal];
        [moreButton setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
        [moreButton addTarget:self action:@selector(moreButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:moreButton];
        view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
        moreButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetHeight(view.frame));
    }
    
    return view;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id model = self.commentDataSource[indexPath.section];
    if ([model isKindOfClass:[FWTopicCommentFrame class]]) {
        FWTopicCommentFrame *videoTopicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = videoTopicFrame.commentFrames[indexPath.row];
        
        if (indexPath.row == videoTopicFrame.commentFrames.count-1 &&
            self.commentDataSource.count != 0) {
            return commentFrame.cellHeight+10;
        }
        return commentFrame.cellHeight;
    }
    
    return .1f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([self.total_count integerValue] >10 &&
        section == self.commentDataSource.count -1) {
        return 40;
    }
    return 0.5f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.height;
    }
    
    return .1f;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([scrollView isEqual:self.infoTableView]){
        
        if (scrollView.contentOffset.y > 150){
            self.alphaValue = 1;
            self.naviBarView.alpha = self.alphaValue;
        }else{
            self.alphaValue = (scrollView.contentOffset.y/150);
            
            self.naviBarView.alpha = self.alphaValue;
        }
        
        if (scrollView.contentOffset.y >0) {
            self.naviBarView.hidden = NO;
            self.headerView.backButton.hidden = YES;
            self.blackBackButton.hidden = NO;
        }else{
            self.naviBarView.hidden = YES;
            self.headerView.backButton.hidden = NO;
            self.blackBackButton.hidden = YES;
        }
    }
}

- (void)sendMessageClick{
    
    [self checkLogin];
    
    self.reply_comment_id = self.reply_comment_id?self.reply_comment_id:@"0";
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSString *contentString = self.commentView.contentView.text;
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.commentModel.feed_id,
                                  @"content":contentString,
                                  @"p_comment_id":self.p_comment_id,
                                  @"reply_comment_id":self.reply_comment_id,
                                  };
        
        
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        request.isNeedShowHud = YES;
        [request startWithParameters:params WithAction:Submit_add_comment  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                
                self.total_count = @([self.total_count integerValue] +1).stringValue;
                [self showCommentCount:self.total_count];
                
                FWMineInfoModel * infoModel = [[FWMineInfoModel alloc] init];
                infoModel.nickname = [GFStaticData getObjectForKey:kTagUserKeyName];
                infoModel.uid = [GFStaticData getObjectForKey:kTagUserKeyID];
                infoModel.header_url = [GFStaticData getObjectForKey:kTagUserKeyImageUrl];
                
                FWCommentListModel * model = [[FWCommentListModel alloc] init];
                model.comment_id = [[back objectForKey:@"data"] objectForKey:@"comment_id"];
                model.content = contentString;
                model.user_info = infoModel;
                model.uid = infoModel.uid;
                model.feed_type = @"2";
                model.is_liked = @"2";
                model.like_count_format = @"0";
                model.format_create_time = @"0秒前";
                
                if (self.commentDataSource.count > 0) {
                    if (self.currentSection >=0) {
                        /* 评论（回复）评论（一、二级评论） */
                        
                        NSString * comment_id = [[back objectForKey:@"data"] objectForKey:@"comment_id"];
                        
                        FWReplyComment * comment = [[FWReplyComment alloc] init];
                        comment.comment_id = comment_id;
                        comment.content = contentString;
                        comment.user_info = infoModel;
                        comment.reply_user_info = self.reply_userInfo;
                        comment.uid = infoModel.uid;
                        comment.feed_type = @"2";
                        comment.is_liked = @"2";
                        comment.like_count_format = @"0";
                        comment.format_create_time = @"刚刚";
                        comment.is_reply = @"1";
                        
                        model.reply_list = [NSMutableArray arrayWithObject:comment];
                        NSMutableArray * tempArr = @[].mutableCopy;
                        
                        FWTopicCommentFrame *topic = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                        topic.topic = model;
                        
                        [tempArr addObject:topic];
                        
                        FWTopicCommentFrame * topicCommentFrame = (FWTopicCommentFrame *)self.commentDataSource[self.currentSection];
                        
                        NSMutableArray * commentFrames = topicCommentFrame.commentFrames.mutableCopy;
                        
                        FWTopicCommentFrame * addCommentFrame = (FWTopicCommentFrame *)tempArr.firstObject;
                        for (FWCommentFrame * comment in addCommentFrame.commentFrames) {
                            
                            if (commentFrames.count >= 3) {
                                [commentFrames insertObject:comment atIndex:commentFrames.count-1];
                            }else {
                                [commentFrames addObject:comment];
                            }
                        }
                        topicCommentFrame.commentFrames = commentFrames.mutableCopy;
                        
                        [UIView performWithoutAnimation:^{
                            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:self.currentSection];
                            [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
                        }];
                    }else{
                        /* 评论了该帖子 */
                        
                        FWTopicCommentFrame *topic = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                        topic.topic = model;
                        
                        [self.commentDataSource insertObject:topic atIndex:0];
                        
                        [UIView performWithoutAnimation:^{
                            [self.infoTableView insertSection:0 withRowAnimation:UITableViewAutomaticDimension];
                        }];
                        [self.infoTableView setContentOffset:CGPointZero animated:NO];
                    }
                }else{
                    [self requestDataWithLoadMoredData:NO];
                }
                
                self.commentView.contentView.text = @"";
                self.commentView.contentView.placeholder = @"点赞都是套路，评论才是真情";
                [self.commentView refreshContentView:self.commentView.contentView];
                [self.view endEditing:YES];
                
                /* 发送成功后，都置为-1 */
                self.currentSection = -1;
                self.currentRow = -1;
                
                [[FWHudManager sharedManager] showErrorMessage:@"评论成功" toController:self];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 评论主评论
- (void)commentButtonClick:(NSInteger)index{
    
    [self.view endEditing:YES];
    
    id model = self.commentDataSource[index];
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
    
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:topicFrame.topic.uid]) {
        /* 是本人，提醒删除 */
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.comment_id = topicFrame.topic.comment_id;
            [self requestDeleteMessageWithSection:index WithRow:-1];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.p_comment_id = topicFrame.topic.comment_id;
            self.reply_comment_id = topicFrame.topic.comment_id;
            
            [self.commentView.contentView becomeFirstResponder];
            self.commentView.contentView.placeholder = [NSString stringWithFormat:@"回复:%@",topicFrame.topic.user_info.nickname];
            
            self.currentSection = index;
            self.currentRow = -1;
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 分享该帖子
- (void)shareButtonClick{
    
    NSDictionary * params = @{
                              @"aweme":self.articalModel.feed_info,
                              @"type":@"3",
                              @"hideDownload":@"YES",
                              };
    
    SharePopView *popView = [[SharePopView alloc] initWithParams:params];
    popView.viewcontroller = self;
    popView.aweme = self.articalModel.feed_info;
    popView.myBlock = ^(FWFeedListModel *awemeModel) {
        self.articalModel.feed_info = awemeModel;
    };
    [popView show];
}

#pragma mark - > 喜欢该帖子
- (void)likesButtonClick{
    
    FWLikeRequest * request = [[FWLikeRequest alloc] init];
    
    NSString * likeType;
    
    if([self.articalModel.feed_info.is_liked isEqualToString:@"2"]){
        likeType = @"add";
    }else{
        likeType = @"cancel";
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.articalModel.feed_info.feed_id,
                              @"like_type":likeType,
                              };
    [request startWithParameters:params WithAction:Submit_like_feed  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if ([likeType isEqualToString:@"add"]) {
                [self.commentView.likesButton setImage:[UIImage imageNamed:@"car_detail_like"] forState:UIControlStateNormal];
                
                self.articalModel.feed_info.is_liked = @"1";
                
                if ([self.articalModel.feed_info.count_realtime.like_count_format integerValue] ||
                    [self.articalModel.feed_info.count_realtime.like_count_format integerValue] == 0) {
                    
                    self.articalModel.feed_info.count_realtime.like_count_format = @([self.articalModel.feed_info.count_realtime.like_count_format integerValue] +1).stringValue;
                }
            }else{
                self.articalModel.feed_info.is_liked = @"2";
                
                [self.commentView.likesButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
                
                if ([self.articalModel.feed_info.count_realtime.like_count_format integerValue] ||
                    [self.articalModel.feed_info.count_realtime.like_count_format integerValue] == 0)  {
                    
                    self.articalModel.feed_info.count_realtime.like_count_format = @([self.articalModel.feed_info.count_realtime.like_count_format integerValue] -1).stringValue;
                }
            }
            
            [self.commentView.likesButton setTitle:[NSString stringWithFormat:@"   %@",self.articalModel.feed_info.count_realtime.like_count_format] forState:UIControlStateNormal];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 发送评论
-(void)sendContentText:(NSString *)content{
    
    if (content.length<=0) {
        [[FWHudManager sharedManager] showErrorMessage:@"说点什么吧~" toController:self];
        [self.view endEditing:YES];
        return;
    }
    
    [self sendMessageClick];
}

#pragma mark - > 返回
- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 更多评论
- (void)moreButtonClick{
    
    FWCommentViewController * VC = [[FWCommentViewController alloc] init];
    VC.feed_id = self.articalModel.feed_info.feed_id;
    VC.listModel = self.articalModel.feed_info;
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark - > 点击回复的姓名
- (void)commentCell:(FWCommentCell *)commentCell didClickedUser:(FWMineInfoModel *)user{
    
    if (nil == user.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = user.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark keyboardNotification
-(void)keyboardShow:(NSNotification *)note{
    
    CGRect keyBoardRect=[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat deltaY=keyBoardRect.size.height;
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        [self.commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.view).mas_offset(0);
            make.left.right.mas_equalTo(self.view);
            if (SCREEN_HEIGHT >= 812) {
                make.height.mas_greaterThanOrEqualTo(60);
            }else{
                make.height.mas_greaterThanOrEqualTo(50);
            }
        }];
        
        [self.commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            if (SCREEN_HEIGHT >= 812) {
                make.left.top.mas_equalTo(self.commentView).mas_offset(15);
            }else{
                make.left.top.mas_equalTo(self.commentView).mas_offset(10);
            }
            
            make.right.mas_equalTo(self.commentView.likesButton.mas_left).mas_offset(-20);
            make.height.mas_greaterThanOrEqualTo (30);
        }];
        

        self.commentView.transform=CGAffineTransformMakeTranslation(0, -deltaY);
    }];
}

-(void)keyboardHide:(NSNotification *)note{
    
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.commentView.transform=CGAffineTransformIdentity;
        self.p_comment_id = self.hostComment_id;
        
        [self.commentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.bottom.mas_equalTo(self.view).mas_offset(0);
            make.left.right.mas_equalTo(self.view);
            make.height.mas_greaterThanOrEqualTo(50+FWSafeBottom);
        }];
        
                
        [self.commentView.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.width.mas_greaterThanOrEqualTo(30);
            make.right.mas_equalTo(self.commentView).mas_offset(-10);
            if (SCREEN_HEIGHT >= 812) {
                make.top.mas_equalTo(self.commentView).mas_offset(15);
//                make.bottom.mas_equalTo(self.commentView).mas_offset(-15);
            }else{
                make.centerY.mas_equalTo(self.commentView);
//                make.bottom.mas_equalTo(self.commentView).mas_offset(-10);
            }
        }];

        [self.commentView.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (SCREEN_HEIGHT >=812){
                make.left.mas_equalTo(self.commentView).mas_offset(15);
                make.top.mas_equalTo(self.commentView).mas_offset(15);
                make.right.mas_equalTo(self.commentView.likesButton.mas_left).mas_offset(-20);
                make.height.mas_equalTo (30);
            }
            else{
                make.left.top.mas_equalTo(self.commentView).mas_offset(10);
                make.right.mas_equalTo(self.commentView.likesButton.mas_left).mas_offset(-20);
                make.height.mas_equalTo (30);
            }
        }];
        
        
        self.commentView.contentView.text=@"";
        if(self.commentView.contentView.text.length<=0){
            self.commentView.contentView.placeholder = @"点赞都是套路，评论才是真情";
        }
    } completion:^(BOOL finished) {
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [self.view endEditing:YES];
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

#pragma mark - 辅助方法
/** topic --- topicFrame */
- (FWTopicCommentFrame *)_topicFrameWithTopic:(FWCommentListModel *)topic
{
    // 这里要判断评论个数大于3 显示全部评论数
    NSInteger count = [topic.reply_total_count integerValue]?[topic.reply_total_count integerValue]:0;
    if (count > 3) {
        FWReplyComment *comment = [[FWReplyComment alloc] init];
        comment.comment_id = FWAllCommentsId;
        comment.feed_id = self.feed_id;
        comment.top_comment_id = topic.comment_id;
        comment.p_comment_id = topic.comment_id;
        comment.content = MoreReply;
        // 添加假数据
        [topic.reply_list addObject:comment];
    }
    
    FWTopicCommentFrame *topicFrame = [[FWTopicCommentFrame alloc] initWithType:@"1"];
    // 传递话题模型数据，计算所有子控件的frame
    
    topicFrame.topic = topic;
    return topicFrame;
}

/** 刷新段  */
- (void)_reloadSelectedSectin
{
    FWTopicCommentFrame * commentFrame = (FWTopicCommentFrame *)self.commentDataSource[self.selectedSection];
    FWTopicCommentFrame * backFrame = (FWTopicCommentFrame *)self.backDataSource.firstObject;
    
    /* cell中评论的点赞数 和 点赞状态 */
    NSInteger count = backFrame.topic.reply_list.count;
    NSMutableArray * tempArr = backFrame.topic.reply_list.mutableCopy ;
    [tempArr removeObjectsInRange:NSMakeRange(3, count-3)];
    
    for (int i = 0; i < commentFrame.topic.reply_list.count; i++) {
        if (i == 3) {
            break;
        }
        FWReplyComment * commment = commentFrame.topic.reply_list[i];
        FWReplyComment * backCommment = tempArr[i];
        commment.like_count_format = backCommment.like_count_format;
        commment.is_liked = backCommment.is_liked;
    }
    
    /* 更新点赞图标 */
    commentFrame.topic.is_liked = backFrame.topic.is_liked;
    /* 更新点赞数 */
    commentFrame.topic.like_count_format = backFrame.topic.like_count_format;
    
    [UIView performWithoutAnimation:^{
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:self.selectedSection];
        [self.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }];
    
    self.selectedSection = -1;
}


@end
