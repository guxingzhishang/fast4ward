//
//  FWEditRefitModuleViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWEditRefitModuleViewController.h"
#import "FWChooseBradeViewController.h"

@interface FWEditRefitModuleViewController ()

@end

@implementation FWEditRefitModuleViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:[NSString stringWithFormat:@"%@改装页",self.type]];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:[NSString stringWithFormat:@"%@改装页",self.type]];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = self.type;
    
    UIButton * finishButton = [[UIButton alloc] init];
    finishButton.frame = CGRectMake(0, 0, 45, 40);
    finishButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [finishButton setTitle:@"完成" forState:UIControlStateNormal];
    [finishButton setTitleColor:FWTextColor_272727 forState:UIControlStateNormal];
    [finishButton addTarget:self action:@selector(finishButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:finishButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];

    [self setupSubViews];
}

- (void)setupSubViews{
    
//    self.pinpaiTF = [[UITextField alloc] init];
//    self.pinpaiTF.delegate = self;
//    self.pinpaiTF.text = @"选择品牌";
//    self.pinpaiTF.font = DHBoldFont(14);
//    self.pinpaiTF.textColor = FWTextColor_222222;
//    self.pinpaiTF.frame = CGRectMake(14, 20, SCREEN_WIDTH-28, 54);
//    [self.view addSubview:self.xinghaoTF];
    self.pinpaiLabel = [[UILabel alloc] init];
    self.pinpaiLabel.text = @"选择品牌";
    self.pinpaiLabel.font = DHBoldFont(14);
    self.pinpaiLabel.userInteractionEnabled = YES;
    self.pinpaiLabel.textColor = FWTextColor_222222;
    [self.view addSubview:self.pinpaiLabel];
    self.pinpaiLabel.frame = CGRectMake(14, 20, SCREEN_WIDTH-28, 54);
    [self.pinpaiLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toChooseButtonClick)]];

    
//    self.toChooseButton = [[UIButton alloc] init];
//    self.toChooseLabel.enabled = NO;
////    [self.toChooseButton addTarget:self action:@selector(toChooseButtonClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.pinpaiLabel addSubview:self.toChooseButton];
//    self.toChooseButton.frame = CGRectMake(CGRectGetWidth(self.pinpaiLabel.frame)-80, 0, 70, 54);
    
    self.rightArrow = [[UIImageView alloc] init];
    self.rightArrow.image = [UIImage imageNamed:@"right_arrow"];
    [self.view addSubview:self.rightArrow];
    [self.rightArrow mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(8, 11));
        make.right.mas_equalTo(self.pinpaiLabel);
        make.centerY.mas_equalTo(self.pinpaiLabel);
    }];

    self.toChooseLabel = [[UILabel alloc] init];
    self.toChooseLabel.text = @"去选择";
    self.toChooseLabel.font = DHSystemFontOfSize_12;
    self.toChooseLabel.textColor = FWColor(@"bcbcbc");
    self.toChooseLabel.textAlignment = NSTextAlignmentRight;
    [self.pinpaiLabel addSubview:self.toChooseLabel];
    [self.toChooseLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(50, 20));
        make.right.mas_equalTo(self.rightArrow.mas_left).mas_offset(-8);
        make.centerY.mas_equalTo(self.pinpaiLabel);
    }];
    
    if ([self.dict[@"品牌"] length] > 0) {
        self.pinpaiLabel.text = self.dict[@"品牌"];
        self.toChooseLabel.text = @"";
    }else{
        self.pinpaiLabel.text = @"选择品牌";
        self.toChooseLabel.text = @"去选择";
    }
    
    self.pinpaiLine = [[UIView alloc] init];
    self.pinpaiLine.frame = CGRectMake(CGRectGetMinX(self.pinpaiLabel.frame), CGRectGetMaxY(self.pinpaiLabel.frame), CGRectGetWidth(self.pinpaiLabel.frame), 0.5);
    self.pinpaiLine.backgroundColor = FWColor(@"F4F4F4");
    [self.view addSubview:self.pinpaiLabel];
    
    self.xinghaoTF = [[UITextField alloc] init];
    self.xinghaoTF.delegate = self;
    self.xinghaoTF.font = DHBoldFont(14);
    self.xinghaoTF.placeholder = @"请填写型号（非必填）";
    self.xinghaoTF.textColor = FWTextColor_12101D;
    self.xinghaoTF.frame = CGRectMake(CGRectGetMinX(self.pinpaiLabel.frame), CGRectGetMaxY(self.pinpaiLine.frame), CGRectGetWidth(self.pinpaiLabel.frame), CGRectGetHeight(self.pinpaiLabel.frame));
    [self.view addSubview:self.xinghaoTF];

    if ([self.dict[@"型号"] length] > 0) {
        self.xinghaoTF.text = self.dict[@"型号"];
    }else{
        self.xinghaoTF.text = @"";
    }
    
    self.xinghaoLine = [[UIView alloc] init];
    self.xinghaoLine.frame = CGRectMake(CGRectGetMinX(self.xinghaoTF.frame), CGRectGetMaxY(self.xinghaoTF.frame), CGRectGetWidth(self.xinghaoTF.frame), 0.5);
    self.xinghaoLine.backgroundColor = FWColor(@"F4F4F4");
    [self.view addSubview:self.xinghaoLine];
    
    self.guigeTF = [[UITextField alloc] init];
    self.guigeTF.delegate = self;
    self.guigeTF.font = DHBoldFont(14);
    self.guigeTF.placeholder = @"请填写规格（非必填）";
    self.guigeTF.textColor = FWTextColor_12101D;
    self.guigeTF.frame = CGRectMake(CGRectGetMinX(self.pinpaiLabel.frame), CGRectGetMaxY(self.xinghaoLine.frame), CGRectGetWidth(self.pinpaiLabel.frame), CGRectGetHeight(self.pinpaiLabel.frame));
    [self.view addSubview:self.guigeTF];

    if ([self.dict[@"规格"] length] > 0) {
        self.guigeTF.text = self.dict[@"规格"];
    }else{
        self.guigeTF.text = @"";
    }
    
    self.guigeLine = [[UIView alloc] init];
    self.guigeLine.frame = CGRectMake(CGRectGetMinX(self.guigeTF.frame), CGRectGetMaxY(self.guigeTF.frame), CGRectGetWidth(self.guigeTF.frame), 0.5);
    self.guigeLine.backgroundColor = FWColor(@"F4F4F4");
    [self.view addSubview:self.guigeLine];
    
    self.feiyongTF = [[UITextField alloc] init];
    self.feiyongTF.delegate = self;
    self.feiyongTF.font = DHBoldFont(14);
    self.feiyongTF.placeholder = @"请填写改装费用（非必填）";
    self.feiyongTF.textColor = FWTextColor_12101D;
    self.feiyongTF.frame = CGRectMake(CGRectGetMinX(self.guigeTF.frame), CGRectGetMaxY(self.guigeLine.frame), CGRectGetWidth(self.guigeTF.frame), CGRectGetHeight(self.guigeTF.frame));
    [self.view addSubview:self.feiyongTF];

    if ([self.dict[@"费用"] length] > 0) {
        self.feiyongTF.text = self.dict[@"费用"];
    }else{
        self.feiyongTF.text = @"";
    }
    
    self.feiyongLine = [[UIView alloc] init];
    self.feiyongLine.frame = CGRectMake(CGRectGetMinX(self.guigeTF.frame), CGRectGetMaxY(self.feiyongTF.frame), CGRectGetWidth(self.guigeTF.frame), 0.5);
    self.feiyongLine.backgroundColor = FWColor(@"F4F4F4");
    [self.view addSubview:self.feiyongLine];
    
    self.saveButton = [[UIButton alloc] init];
    self.saveButton.titleLabel.font = DHFont(14);
    self.saveButton.backgroundColor = FWTextColor_222222;
    [self.saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [self.saveButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.saveButton addTarget:self action:@selector(saveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveButton];
}

#pragma mark - > 去选择
- (void)toChooseButtonClick{
    
    @weakify(self);
    FWChooseBradeViewController * CBVC = [[FWChooseBradeViewController alloc] init];
    CBVC.chooseFrom = @"gaizhuang";
    CBVC.type = self.type;
    CBVC.myBlock = ^(NSString * _Nonnull brandString) {
        @strongify(self);
        if (brandString.length > 0) {
            self.toChooseLabel.text = @"";
            self.pinpaiLabel.text = brandString;
        }
    };
    [self.navigationController pushViewController:CBVC animated:YES];
}

#pragma mark - > 完成
- (void)finishButtonClick{
    
    [self.view endEditing:YES];
    
    if (self.pinpaiLabel.text.length <=0 ||
        [self.pinpaiLabel.text isEqualToString:@"选择品牌"]) {
        [[FWHudManager sharedManager] showErrorMessage:@"请选择品牌" toController:self];
        return;
    }
    
    NSString * xinghao = @"";
    if (self.xinghaoTF.text.length > 0) {
        xinghao = [NSString stringWithFormat:@"-%@",self.xinghaoTF.text];
    }
    
    NSString * guige = @"";
    if (self.guigeTF.text.length > 0) {
        guige = [NSString stringWithFormat:@"-%@",self.guigeTF.text];
    }
    
    NSString * feiyong = @"";
    if (self.feiyongTF.text.length > 0) {
        feiyong = [NSString stringWithFormat:@"-%@",self.feiyongTF.text];
    }
    
    NSString * moduleString = [NSString stringWithFormat:@"%@%@%@%@",self.pinpaiLabel.text,xinghao,guige,feiyong];
    
    NSDictionary * dict = @{
        @"品牌":self.pinpaiLabel.text,
        @"型号":self.xinghaoTF.text,
        @"规格":self.guigeTF.text,
        @"费用":self.feiyongTF.text,
        @"moduleString":moduleString,
    };
    
    self.moduleBlock(dict);
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 保存
- (void)saveButtonClick{
    [self finishButtonClick];
}

@end
