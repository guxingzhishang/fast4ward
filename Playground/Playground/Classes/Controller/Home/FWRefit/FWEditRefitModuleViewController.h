//
//  FWEditRefitModuleViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^editModuleBlock)(NSDictionary * moduleDict);

@interface FWEditRefitModuleViewController : FWBaseViewController<UITextFieldDelegate>

@property (nonatomic, strong) UILabel * titleLabel;

//@property (nonatomic, strong) UITextField * pinpaiTF;
@property (nonatomic, strong) UILabel * pinpaiLabel;
@property (nonatomic, strong) UIView * pinpaiLine;

@property (nonatomic, strong) UIButton * toChooseButton;
@property (nonatomic, strong) UILabel * toChooseLabel;
@property (nonatomic, strong) UIImageView * rightArrow;


@property (nonatomic, strong) UITextField * xinghaoTF;
@property (nonatomic, strong) UIView * xinghaoLine;

@property (nonatomic, strong) UITextField * guigeTF;
@property (nonatomic, strong) UIView * guigeLine;

@property (nonatomic, strong) UITextField * feiyongTF;
@property (nonatomic, strong) UIView * feiyongLine;

@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UIButton * saveButton;

@property (nonatomic, strong) NSString * type;

@property (nonatomic, copy) editModuleBlock moduleBlock;

@property (nonatomic, strong) NSDictionary * dict;


@end

NS_ASSUME_NONNULL_END
