//
//  FWChooseCarStyleViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWChooseCarStyleViewController : FWBaseViewController
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSString * type;
@end

NS_ASSUME_NONNULL_END
