//
//  FWAddBrandViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/10.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

/**
 * 添加品牌
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^addBrand)(NSString * brand);

@interface FWAddBrandViewController : FWBaseViewController<UITextFieldDelegate>

@property (nonatomic, strong) UIView * centerView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton * closeButton;
@property (nonatomic, strong) UITextField * shuruTF;
@property (nonatomic, strong) UIButton * saveButton;

@property (nonatomic, copy) addBrand brandBlock;

@end

NS_ASSUME_NONNULL_END
