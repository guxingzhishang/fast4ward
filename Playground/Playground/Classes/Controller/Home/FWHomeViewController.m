//
//  FWHomeViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWHomeViewController.h"

#import "FWHomeRequest.h"
#import "FWHomeRecomendView.h"

#import "ShareView.h"
#import "ShareManager.h"

#import "FWSearchInfoViewController.h"
#import "FWFeedModel.h"
#import "FWMessageHomeModel.h"
#import "FWTagsRequest.h"
#import "RCCRRongCloudIMManager.h"
#import "FWUpdateAlertViewController.h"
#import "FWSettingModel.h"
#import "FWMineRequest.h"

#import "FWHomeBussinessViewController.h"

#import "FWActivityAlertViewController.h"
#import "FWMatchViewController.h"
#import "FWPlayerRankViewController.h"
#import "FWHotLiveListViewController.h"

#import "FWHomeRecommendViewController.h"
#import "FWHomeAttentionViewController.h"
#import "FWAskAndAnswerViewController.h"
#import "FWIdleViewController.h"
#import "FWMyGoldenViewController.h"

// 临时的
#import <WXApi.h>
#import <WXApiObject.h>
#import "FWAttentionRefitTopicViewController.h"


@interface FWHomeViewController ()<UIViewControllerTransitioningDelegate,FWActivityAlertDelegate,FWHomeNavigationViewDelegate>

@property (nonatomic, assign) BOOL   isFirstRequest;
@property (nonatomic, strong) FWSettingModel * settingModel;

@property (nonatomic, strong) NSString * lastIndex;

@property (nonatomic, strong) NSArray * titleData;
@property (nonatomic, assign) CGFloat  menuHeight;
@property (nonatomic, assign) CGFloat  wmMenuY;
@property (nonatomic, strong) UILabel * unreandLabel;

@end

@implementation FWHomeViewController
@synthesize settingModel;
@synthesize isFirstRequest;
@synthesize signButton;
@synthesize signView;

#pragma mark - > 请求融云
- (void)requestRongYunTokenIM{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_im_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:userId];
                                            [RCIM sharedRCIM].currentUserInfo = userInfo;
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"IM登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            NSLog(@"IMtoken错误");
                                        }];
        }
    }];
}
#pragma mark - > 请求个人信息
- (void)requestMineInfo{
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]){
        
        FWHomeRequest * request = [[FWHomeRequest alloc] init];
        
        NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
        
        [request startWithParameters:params WithAction:Get_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                NSDictionary * infoDict = [[back objectForKey:@"data"] objectForKey:@"user_info"];
                
                [GFStaticData saveLoginData:infoDict];
                
                [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
                [[FWUserCenter sharedManager] saveUserInfoToDiskWithJson:back];
                [FWUserCenter sharedManager].user_IsLogin = YES;
                
                NSString * selectImage = @"";
                NSString * unselectImage = @"";
                if ([[[back objectForKey:@"data"] objectForKey:@"signin_status"] intValue] == 2) {
                    /* 当日未签到 */
                    selectImage = @"tabbar_me_sign_click";
                    unselectImage = @"tabbar_me_sign";
                    
                    signView.hidden = NO;
                }else if(([[[back objectForKey:@"data"] objectForKey:@"gold_status"] intValue] == 1) &&
                         ([[[back objectForKey:@"data"] objectForKey:@"signin_status"] intValue] == 1)){
                    selectImage = @"tabbar_me_badge_click";
                    unselectImage = @"tabbar_me_badge";
                    signView.hidden = YES;
                }else{
                    /* 当日已签到 */
                    selectImage = @"tabbar_me_click_icon";
                    unselectImage = @"tabbar_me_icon";
                    signView.hidden = YES;
                }
                
                [[self cyl_tabBarController].tabBar.items enumerateObjectsUsingBlock:^(UITabBarItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (idx == 3) {
                        obj.selectedImage = [[UIImage imageNamed:selectImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                        obj.image = [[UIImage imageNamed:unselectImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    }
                }];
            }
        }];
    }
}

#pragma mark - > 请求基本配置参数
- (void)requestSetting{

    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_settings  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
        
            [GFStaticData saveObject:self.settingModel.kefu_uid forKey:kKefuUID];
            [GFStaticData saveObject:self.settingModel.kefu_nickname forKey:kKefuNickname];
            [GFStaticData saveObject:self.settingModel.pub_banner forKey:kPublishIdelImageUrl];

            
            NSString * update_status = self.settingModel.update_status;
            
            NSString * lastVersion ;
            if ([GFStaticData getObjectForKey:HomeUpdateVersion]) {
                /* 如果有上一次存储的版本记录，则判断 */
                lastVersion = [GFStaticData getObjectForKey:HomeUpdateVersion];
            }else{
                /* 如果没有上一次记录，取当前版本号 */
                lastVersion = kAppVersion;
                [GFStaticData saveObject:kAppVersion forKey:HomeUpdateVersion];
            }
            if (self.settingModel.alert_activity_info.act_id) {
                [self showActivity];
            }
            
            if (![update_status isEqualToString:@"1"]) {
                
                if ([update_status isEqualToString:@"3"] &&
                    [lastVersion isEqualToString:self.settingModel.newest_version]) {
                    // 如果非强制更新，并且提示过，就不在显示
                    return ;
                }
                FWUpdateAlertViewController * vc = [FWUpdateAlertViewController new];
                vc.settingModel = self.settingModel;
                vc.view.frame = [UIScreen mainScreen].bounds;
                [DHWindow addDHViewController:vc priority:500];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 展示活动
- (void)showActivity{
    
    NSMutableDictionary * dict = [GFStaticData getObjectForKey:IsShowedActivity];
    NSString * phoneID = [Utility getDeviceID];
    
    if ([dict[phoneID] isEqualToString:self.settingModel.alert_activity_info.act_id]) {
        /* 如果当前设备号对应的value有值， 并等于当前活动id，返回不显示*/
        return;
    }
    
    if (self.settingModel.alert_activity_info.act_id) {
        /* 没展示过*/
        FWActivityAlertViewController * vc = [FWActivityAlertViewController new];
        vc.activityInfoModel = self.settingModel.alert_activity_info;
        vc.delegate = self;
        vc.view.frame = [UIScreen mainScreen].bounds;
        [DHWindow addDHViewController:vc priority:450];
    }
}

#pragma mark - > 跳转到活动页
- (void)activityClick{

    if (self.settingModel.alert_activity_info) {
        FWActivityInfoModel * activityModel = self.settingModel.alert_activity_info;
        
        if (activityModel.act_type && activityModel.act_val) {
            if ([activityModel.act_type isEqualToString:@"1"]){
                /* 赛事主页 */
//                FWMatchViewController * MVC = [[FWMatchViewController alloc] init];
//                [self.navigationController pushViewController:MVC animated:YES];
                self.navigationController.tabBarController.selectedIndex = 1;
            }else if ([activityModel.act_type isEqualToString:@"4"]){
                /* 文章详情页 */
                FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
                DVC.feed_id = activityModel.act_val;
                [self.navigationController pushViewController:DVC animated:YES];
            }else if ([activityModel.act_type isEqualToString:@"5"]){
                /* 图文详情页 */
                FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
                TVC.feed_id = activityModel.act_val;
                TVC.requestType = FWPushMessageRequestType;
                [self.navigationController pushViewController:TVC animated:YES];
            }else if ([activityModel.act_type isEqualToString:@"6"]){
                /* 标签（话题）页 */
                FWTagsViewController *vc = [[FWTagsViewController alloc] init];
                vc.tags_id = activityModel.act_val;
                [self.navigationController pushViewController:vc animated:YES];
            }else if ([activityModel.act_type isEqualToString:@"7"]) {
                /* 商家店铺页*/
                FWCustomerShopViewController * CSVC = [[FWCustomerShopViewController alloc] init];
                CSVC.user_id = activityModel.act_val;;
                CSVC.isUserPage = NO;
                [self.navigationController pushViewController:CSVC animated:YES];
            }else if ([activityModel.act_type isEqualToString:@"8"]) {
                /* 商品详情页 */
                FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
                GDVC.goods_id = activityModel.act_val;;
                [self.navigationController pushViewController:GDVC animated:YES];
            }else if ([activityModel.act_type isEqualToString:@"9"]){
                /* 打开H5活动页 */
                FWWebViewController * WVC = [[FWWebViewController alloc] init];
                WVC.pageType = WebViewTypeURL;
                WVC.htmlStr = activityModel.act_val;
                [self.navigationController pushViewController:WVC animated:YES];
            }else if ([activityModel.act_type isEqualToString:@"13"]){
                /* 视频详情页 */
                FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
                controller.currentIndex = 0;
                controller.feed_id = activityModel.act_val;
                controller.requestType = FWPushMessageRequestType;
                [self.navigationController pushViewController:controller animated:YES];
            }else if ([activityModel.act_type isEqualToString:@"14"]){
                /* 车手排名 */
                FWPlayerRankViewController *controller = [[FWPlayerRankViewController alloc] init];
                [self.navigationController pushViewController:controller animated:YES];
            }else if ([activityModel.act_type isEqualToString:@"15"]){
                /* 直播列表 */
                FWHotLiveListViewController *controller = [[FWHotLiveListViewController alloc] init];
                [self.navigationController pushViewController:controller animated:YES];
            }else if ([activityModel.act_type  isEqualToString:@"16"]){
                /* 长视频详情页 */
                FWLongVideoPlayerViewController *controller = [[FWLongVideoPlayerViewController alloc] init];
                controller.feed_id = activityModel.act_val;
                controller.myBlock = ^(FWFeedListModel *listModel) {};
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
    }
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
            [[GFStaticData getObjectForKey:kTagUserKeyID] length] > 1) {
            self.selectIndex = 0;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.selectIndex = 1;
            });
        }else{
            self.selectIndex = 1;
        }
//        if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
//            [[GFStaticData getObjectForKey:kTagUserKeyID] length] > 1) {
//            self.selectIndex = 1;
//
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                self.selectIndex = 0;
//            });
//        }else{
//            self.selectIndex = 0;
//        }
        self.menuViewStyle = WMMenuViewStyleDefault;
        self.progressHeight = 2.0f;
        self.titleFontName = @"PingFangSC-Semibold";
        self.titleSizeSelected = 22;
        self.titleSizeNormal = 17;
        self.scrollEnable = YES;
        self.titleColorSelected = FWTextColor_222222;
        self.titleColorNormal = FWColorWihtAlpha(@"999999", 0.6);
        self.menuViewLayoutMode = WMMenuViewLayoutModeScatter;
        self.menuHeight = 50;
        self.wmMenuY =  60+FWCustomeSafeTop;
    }
    return self;
}

#pragma mark - > 标题数组
- (NSArray *)titleData {
    if (!_titleData) {
        _titleData = @[ @"关注",@"发现",@"问答",@"闲置"];
    }
    return _titleData;
}


#pragma mark - > WMPageController 相关配置
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController{
    return self.titleData.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    
    return self.titleData[index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    
    if (index == 0){
        FWHomeAttentionViewController * AVC = [[FWHomeAttentionViewController alloc] init];
        return AVC;
    }else if (index == 1){
        FWHomeRecommendViewController * RVC = [[FWHomeRecommendViewController alloc] init];
        return RVC;
    }else if(index == 2){
        FWAskAndAnswerViewController * AVC = [[FWAskAndAnswerViewController alloc] init];
        return AVC;
    }else{
        FWIdleViewController * AVC = [[FWIdleViewController alloc] init];
        return AVC;
    }
}

-(void)pageController:(WMPageController *)pageController didEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info{
    
//    NSInteger index = 0;
    NSInteger index = [[info objectForKey:@"index"] integerValue]?[[info objectForKey:@"index"] integerValue]:0;
    
    if ([[info objectForKey:@"index"] integerValue] > 0) {
        index = [[info objectForKey:@"index"] integerValue];
    }
    
    if (index == 0) {
        
        if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
            [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop,@"last_index":@(index+1).stringValue}];
            return;
        }
    }
    
    self.lastIndex = @(index).stringValue;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView
{
    return CGRectMake(14,self.wmMenuY, 250, self.menuHeight);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView
{
    return CGRectMake(0, self.wmMenuY+self.menuHeight, self.view.frame.size.width, self.view.frame.size.height - self.menuHeight-self.wmMenuY);
}

//#pragma mark - > 未读数
//- (void)requestUnread{
//    FWHomeRequest * request = [[FWHomeRequest alloc] init];
//
//    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
//
//    [request startWithParameters:params WithAction:Get_new_msg_count  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
//
//        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
//
//        if ([code isEqualToString:NetRespondCodeSuccess]) {
//
//            FWMessageHomeModel * model = [FWMessageHomeModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
//            if ([model.unread_like_count integerValue]>0 ||
//                [model.unread_comment_count integerValue]>0 ||
//                [model.unread_follow_count integerValue]>0 ||
//                [model.unread_total_count integerValue]>0 ||
//                [model.unread_at_count integerValue]>0||
//                [[RCIMClient sharedRCIMClient] getTotalUnreadCount] > 0) {
//
//                NSInteger unreandCount = [model.unread_total_count integerValue];
//
//                if ([[RCIMClient sharedRCIMClient] getTotalUnreadCount]>0) {
//                    unreandCount += [[RCIMClient sharedRCIMClient] getTotalUnreadCount];
//                }
//
//                if (unreandCount > 0 && unreandCount < 100) {
//                    self.unreandLabel.hidden = NO;
//                    self.unreandLabel.text = @(unreandCount).stringValue;
//                    self.unreandLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
//                }else if(unreandCount >= 100){
//                    self.unreandLabel.hidden = NO;
//                    self.unreandLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 8];
//                    self.unreandLabel.text = @"99+";
//                }else{
//                    self.unreandLabel.hidden = YES;
//                }
//            }else{
//                self.unreandLabel.hidden = YES;
//            }
//        }else{
//            self.unreandLabel.hidden = YES;
//        }
//    }];
//}

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    self.fd_prefersNavigationBarHidden = YES;
    
    [self requestMineInfo];
    [self requestRongYunTokenIM];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.fd_prefersNavigationBarHidden = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PushToPublishViewController) name:kTagPushPublish object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ScrollToIndex) name:@"ScrollToIndex" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTab) name:@"changeTab" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mengcengChangeTab) name:@"mengcengChangeTab" object:nil];

    if (![[GFStaticData getObjectForKey:is_Show_AD] boolValue]){
    
        [GFStaticData saveObject:@"YES" forKey:is_Show_AD];
        
        FWADViewController *vc = [[FWADViewController alloc] init];
        vc.navi = self.navigationController;
        [DHWindow addDHViewController:vc priority:4000];
    }

    isFirstRequest = YES;
    
    [self setupButtons];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self requestSetting];
    });
}

- (void)setupButtons{
    
    self.navigationView = [[FWHomeNavigationView alloc] init];
    self.navigationView.frame = CGRectMake(0, 0, SCREEN_WIDTH-60, 64+FWCustomeSafeTop);
    self.navigationView.delegate = self;
    [self.view addSubview:self.navigationView];
    
    signButton = [[UIButton alloc] init];
    signButton.tag = 1000900;
    [self.view addSubview:signButton];
    [signButton setImage:[UIImage imageNamed:@"new_black_sign"] forState:UIControlStateNormal];
    [signButton addTarget:self action:@selector(messageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [signButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.navigationView.searchImageView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.right.mas_equalTo(self.view).mas_offset(-10);
    }];

    signView = [[UIView alloc] init];
    signView.layer.cornerRadius = 6.5/2;
    signView.layer.masksToBounds = YES;
    signView.backgroundColor = FWColor(@"ff6f00");
    [self.view addSubview:signView];
    [signView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(6.5, 6.5));
        make.top.mas_equalTo(self.signButton).mas_offset(3);
        make.right.mas_equalTo(self.signButton).mas_offset(-4);
    }];
    signView.hidden = YES;
}

/* 未登录，点击关注，会弹出登录页，点击关闭返回之前选择页面*/
- (void)ScrollToIndex{
    
    self.selectIndex = (int)[self.lastIndex intValue];
}

/* 上传后切换到关注tab */
- (void)changeTab{
    
    if([[GFStaticData getObjectForKey:@"isUploading"] boolValue] && self.selectIndex != 0){
        
//        [GFStaticData saveObject:nil forKey:@"isUploading"];
        self.selectIndex = 0;
    }
}

#pragma mark - > 签到页
- (void)messageButtonClick{
    
    [self cheackLogin];
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
              ![@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        [self.navigationController pushViewController:[FWMyGoldenViewController new] animated:YES];
    }
}

#pragma mark - > 搜索
- (void)searchButtonClick{

//    FWPublishSearchUsersViewController * ARTVC = [[FWPublishSearchUsersViewController alloc] init];
//    [self.navigationController pushViewController:ARTVC animated:YES];
//    return;

    
//    WXLaunchMiniProgramReq *launchMiniProgramReq = [WXLaunchMiniProgramReq object];
//    launchMiniProgramReq.userName = @"gh_8684d0dbafd7";  //拉起的小程序的username
//    launchMiniProgramReq.path = @"";    ////拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
//    launchMiniProgramReq.miniProgramType = WXMiniENVIRONMENT; //拉起小程序的类型
//    [WXApi sendReq:launchMiniProgramReq];
//
//    return;

    [TalkingData trackEvent:@"搜索" label:@"首页" parameters:nil];

    FWSearchInfoViewController * SIVC = [[FWSearchInfoViewController alloc] init];
    [self.navigationController pushViewController:SIVC animated:YES];
}

#pragma mark - > 跳转到发布页
- (void)PushToPublishViewController{
    
    if (![((UINavigationController *)self.tabBarController.selectedViewController).topViewController isEqual:self]
        ) {
        return;
    }

    [self cheackLogin];
    
    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self];
        }
    }else {
        if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
            ![@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
            
            FWPublishViewController * vc = [[FWPublishViewController alloc] init];
            vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
            vc.fd_prefersNavigationBarHidden = YES;
            vc.navigationController.navigationBar.hidden = YES;
            
            CATransition *animation = [CATransition animation];
            [animation setDuration:0.4];
            [animation setType: kCATransitionMoveIn];
            [animation setSubtype: kCATransitionFromTop];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            [self.navigationController pushViewController:vc animated:NO];
            [self.navigationController.view.layer addAnimation:animation forKey:nil];
        }
    }
}

#pragma mark - > 检测是否登录
- (void)cheackLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        /* 登录 */
        [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        
        return;
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

- (void)mengcengChangeTab{
    self.selectIndex = 3;
}
@end
