//
//  FWGraphTextViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 图文流
 */

#import "FWBaseViewController.h"
#import "FWGraphTextCell.h"
#import "FWFeedModel.h"

typedef NS_ENUM(NSUInteger, FWFeedDetailRequestType) {
    FWAttentionRequestType = 1,// 关注列表的请求
    FWRecommendRequestType,    // 推荐列表的请求
    FWNoteRequestType,         // 笔记列表的请求
    FWSearchResultListRequestType, // 搜索结果页的请求
    FWHomeTagRequestType,      // 首页标签的请求
    FWMatchListRequestType,    // 赛事列表的请求
    FWGodListRequestType,    // 大神专区的请求
    FWInfoListRequestType,     // 个人中心所有作品和视频的请求
    FWTagListRequestType,      // 标签页所有作品和视频的请求
    FWCategaryListRequestType, // 改装案例页所有作品和视频的请求
    FWActivityTagsRequestType, // 活动页点击某个作品的请求
    FWFavouriteListRequestType,// 收藏点击某个作品的请求
    FWPushMessageRequestType, // 点击推送进来单独的一个作品
    FWADRequestType, // 开屏广告进来单独的一个作品
};

@interface FWGraphTextViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource,FWGraphTextCellDelegate>

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * listModel;

/* 1:关注  2:推荐  3:标签 */
@property (nonatomic, assign) FWFeedDetailRequestType  requestType;

@property (nonatomic, strong) NSString * tag_id;

@property (nonatomic, strong) NSString * feed_id;

/* 来源的页面类型 nav  topic  fromtopic fromactivity */
@property (nonatomic, strong) NSString * tag_type;

@property (nonatomic, strong) NSString * flag_return;

@end
