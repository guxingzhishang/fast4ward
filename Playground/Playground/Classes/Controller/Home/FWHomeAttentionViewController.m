//
//  FWHomeAttentionViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeAttentionViewController.h"
#import "FWHomeAttentionCell.h"

#import "FWUploadView.h"

@interface FWHomeAttentionViewController ()<UITableViewDelegate,UITableViewDataSource,FWHomeCellDelegate,FWUploadViewDelegate>

@property (nonatomic, assign) NSInteger   attentionPageNum;
@property (nonatomic, strong) NSMutableArray * attentionDataSource;
@property (nonatomic, strong) FWFeedModel * attentionFeedModel;
@property (nonatomic, assign) NSInteger  currentIndex;// 感兴趣模块在第几行
@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) FWUploadView * uploadView;
@property (nonatomic, assign) BOOL isNowLoading; // 正在上传进度
@property (nonatomic, assign) BOOL noShowView;// 当前上传不在显示进度条

@end

@implementation FWHomeAttentionViewController
@synthesize attentionPageNum;
@synthesize attentionDataSource;
@synthesize attentionFeedModel;
@synthesize infoTableView;
@synthesize uploadView;

#pragma mark - > 请求关注列表
- (void)requestAttentionListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    if (isLoadMoredData == NO) {
        attentionPageNum = 1;
        self.currentIndex = -1;
        
        if (self.attentionDataSource.count > 0 ) {
            [self.attentionDataSource removeAllObjects];
        }
    }else{
        attentionPageNum +=1;
    }
    
    NSDictionary * params  = @{
                               @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                               @"page":@(attentionPageNum).stringValue,
                               @"feed_id":@"0",
                               @"page_size":@"20",
                               };
    NSString * action = Get_feeds_by_follow_users;
    
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            attentionFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.attentionDataSource addObjectsFromArray: attentionFeedModel.feed_list];
            
            /* 可能感兴趣模块 */
            if (attentionFeedModel.list_suggestion_user.count > 0 &&
                attentionPageNum == 1) {
                /* 有需要推荐的用户模块，并且不是上拉刷新 */
                
                if (self.attentionDataSource.count <= 4) {
                    /* 如果有小于等于4条的消息，将推荐关注模块放到最后一位 */
                    
                    self.currentIndex = self.attentionDataSource.count;
                    [self.attentionDataSource addObject:attentionFeedModel.list_suggestion_user];
                }else{
                    /* 如果有大于4条的消息，将推荐关注模块放到第5条*/
                    
                    self.currentIndex = 4;
                    [self.attentionDataSource insertObject:attentionFeedModel.list_suggestion_user atIndex:4];
                }
            }
            
            if([attentionFeedModel.feed_list count] == 0 &&
               attentionPageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                [[FWHudManager sharedManager] showErrorMessage:@"您还未登录，请先登录才能查看关注哦~" toController:self];
                return ;
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"首页-关注"];
    
    if (self.attentionDataSource.count <= 0 ) {
        [self requestAttentionListWithLoadMoredData:NO];
    }
}

- (void)deleteFeedInfo{
    
    int tempNum = -1;
    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {
        
        for (int i = 0; i < self.attentionDataSource.count; i++) {
            FWFeedListModel * tempModel = self.attentionDataSource[i];
            
            if (i == self.currentIndex) {
                continue;
            }
            
            if ([tempModel isKindOfClass:[NSArray class]]) {
                return;
            }
            if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                tempNum = i;
            }
        }
        
        if (tempNum >=0) {
            [self.attentionDataSource removeObjectAtIndex:tempNum];
            [infoTableView reloadData];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"首页-关注"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.attentionDataSource = @[].mutableCopy;
    attentionPageNum = 0;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadProgress:) name:@"FeedUpLoad" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteFeedInfo) name:@"DeleteFeedInfo" object:nil];

    [self setupSubViews];
    [self requestAttentionListWithLoadMoredData:NO];
}

- (void)setupSubViews{
    
    uploadView = [[FWUploadView alloc] init];
    uploadView.delegate = self;
    [self.view addSubview:uploadView];
    uploadView.hidden = YES;
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.estimatedRowHeight = 100;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.backgroundColor = FWTextColor_F8F8F8;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    infoTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-(55+FWCustomeSafeTop)-(49+FWSafeBottom));

    /* 设置这个后，防止上拉加载更多时，乱弹 */
    self.infoTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];

    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestAttentionListWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        [weakSelf requestAttentionListWithLoadMoredData:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.attentionDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.currentIndex >= 0 && indexPath.row == self.currentIndex) {
        static NSString * cellID = @"homeAttentionCellID";
        FWHomeAttentionCell * cell =  [tableView dequeueReusableCellWithIdentifier:cellID];
        
        if (nil == cell) {
            cell = [[FWHomeAttentionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        cell.vc = self;
        
        DHWeakSelf;
        cell.attentionBlock = ^(BOOL isShouldDelete) {
            if (isShouldDelete) {
                [self.attentionDataSource removeObjectAtIndex:self.currentIndex];
                self.currentIndex = -1;
                [UIView performWithoutAnimation:^{
                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
                    [weakSelf.infoTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                }];
            }
        };
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell configForCell:self.attentionFeedModel];
        return cell;
        
    }else{
        
        static NSString * cellID = @"attentionCellID";
        FWHomeCell * cell =  [tableView dequeueReusableCellWithIdentifier:cellID];
        
        if (nil == cell) {
            cell = [[FWHomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        cell.delegate            = self;
        cell.vc                  = self;
        cell.bannerImageView.tag  = 1000+indexPath.row;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row < self.attentionDataSource.count && indexPath.row != self.currentIndex) {
            cell.listModel = self.attentionDataSource[indexPath.row];
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [infoTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.attentionDataSource.count) {
        [self dealWithOperationWithIndex:indexPath.row];
    }
}

- (void)picImageClick:(NSInteger)index{
    
    [self dealWithOperationWithIndex:index];
}

- (void)dealWithOperationWithIndex:(NSInteger)index{
    
    if (index == self.currentIndex) {
        return;
    }
    
    NSMutableArray * tempArr = self.attentionDataSource;
    NSInteger requestType = FWAttentionRequestType;
    NSMutableArray * tempDataSource = @[].mutableCopy;

    FWFeedListModel * model = self.attentionDataSource[index];
    
    if (tempArr.count <= 0) {
        return;
    }
    
    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }
    
    if ([model.feed_type isEqualToString:@"1"]) {
        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.listModel = model;
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {
                self.attentionDataSource[index] = listModel;
            };
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            for (NSInteger i = index; i<tempArr.count; i++) {
                FWFeedListModel * model = tempArr[i];
                
                if (i == self.currentIndex) {
                    continue;
                }
                
                if ([model.feed_type isEqualToString:@"1"] && [model.is_long_video isEqualToString:@"2"]) {
                    [tempDataSource addObject:model];
                }
            }
            
            // 视频流
            FWVideoPlayerViewController * VPVC = [[FWVideoPlayerViewController alloc] init];
            VPVC.currentIndex = 0;
            VPVC.requestType = requestType;
            VPVC.flag_return = @"2";
            VPVC.feed_id = model.feed_id;
            VPVC.listModel = tempDataSource;
            [self.navigationController pushViewController:VPVC animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        // 图文详情
        FWGraphTextDetailViewController * DVC = [[FWGraphTextDetailViewController alloc] init];
        DVC.listModel = self.attentionDataSource[index];
        DVC.feed_id = ((FWFeedListModel *)tempArr[index]).feed_id;
        DVC.myBlock = ^(FWFeedListModel *listModel) {
            self.attentionDataSource[index] = listModel;
        };
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"4"]) {
            
        // 问答页
        FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        
        // 改装页
        FWRefitCaseDetailViewController * RCDVC = [[FWRefitCaseDetailViewController alloc] init];
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"6"]) {
    /* 闲置 */
        FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }
}


#pragma mark - > 没有网2秒后取消刷新
- (void)networkUnAvailable:(NSNotification *)noti{
    
    if (![((UINavigationController *)self.tabBarController.selectedViewController).topViewController isEqual:self]
        ) {
        return;
    }
    [self hideRefresh];
}

- (void)hideRefresh{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 矜持会，3秒后取消
        [infoTableView.mj_footer endRefreshing];
        [infoTableView.mj_header endRefreshing];
    });
    
}

#pragma mark - > 帖子上传中
- (void)uploadProgress:(NSNotification *)noti{
    
    if (!self.noShowView) {
        
        NSDictionary * progress = (NSDictionary *)noti.userInfo;
        CGFloat progressValue = [progress[@"uploadProgress"] floatValue];
        //        NSLog(@"uploadProgress===来了老弟，%.2f",progressValue);
        
        if (progressValue >=0 && progressValue < 1) {
            uploadView.hidden = NO;
            uploadView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 80);
            //            NSLog(@"uploadProgress===显示");
            
            if (progress[@"imageData"] && !self.isNowLoading) {
                self.isNowLoading = YES;
                [self requestAttentionListWithLoadMoredData:NO];
                
                [self.infoTableView setContentOffset:CGPointMake(SCREEN_WIDTH, 0) animated:YES];
            }
            
            uploadView.coverImage.image = [[UIImage alloc] initWithData:progress[@"imageData"]];
            
            if ([progress[@"feed_type"] isEqualToString:@"1"]) {
                /* 视频贴 */
                uploadView.iconImage.hidden = NO;
            }else{
                /* 图文贴 */
                uploadView.iconImage.hidden = YES;
            }
            
            if ([progress[@"is_draft"] isEqualToString:@"1"]) {
                uploadView.tipLabel.text = [NSString stringWithFormat:@"保存中（%.0f%%）",progressValue*100];
            }else{
                uploadView.tipLabel.text = [NSString stringWithFormat:@"上传中（%.0f%%）",progressValue*100];
            }
            uploadView.progressView.progress = progressValue;
            self.infoTableView.frame = CGRectMake(0, CGRectGetMaxY(uploadView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(uploadView.frame)-(70+FWCustomeSafeTop)-(49+FWSafeBottom));
        }else {
            uploadView.progressView.progress = 0;
            uploadView.hidden = YES;
            self.infoTableView.frame = CGRectMake(0,0 ,SCREEN_WIDTH, SCREEN_HEIGHT-(70+FWCustomeSafeTop)-(49+FWSafeBottom));
        }
    }
}

- (void)uploadSuccess:(NSNotification *)noti{
    [super uploadSuccess:noti];
    self.noShowView = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestAttentionListWithLoadMoredData:NO];
    });
    
    self.uploadView.hidden = YES;
    self.infoTableView.frame = CGRectMake(0,0 ,SCREEN_WIDTH, SCREEN_HEIGHT-(70+FWCustomeSafeTop)-(49+FWSafeBottom));
    
    [GFStaticData saveObject:nil forKey:@"isUploading"];
}

- (void)uploadFailure:(NSNotification *)noti{
    [super uploadFailure:noti];
    
    self.noShowView = NO;
    self.uploadView.hidden = YES;
    self.uploadView.progressView.progress = 0;
    self.infoTableView.frame = CGRectMake(0,0 ,SCREEN_WIDTH, SCREEN_HEIGHT-(70+FWCustomeSafeTop)-(49+FWSafeBottom));
    
    [GFStaticData saveObject:nil forKey:@"isUploading"];
}

#pragma mark - > 关闭上传进度
- (void)closeButtonOnClick{
    
    uploadView.progressView.progress = 0;
    uploadView.hidden = YES;
    self.noShowView = YES;
    self.infoTableView.frame = CGRectMake(0,0 ,SCREEN_WIDTH, SCREEN_HEIGHT-(70+FWCustomeSafeTop)-(49+FWSafeBottom));
}


@end
