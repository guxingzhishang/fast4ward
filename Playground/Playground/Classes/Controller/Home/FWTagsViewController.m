//
//  FWTagsViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWTagsViewController.h"
#import "FWMyworksCollectionCell.h"
#import "FWMyVideoCollectionCell.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWTagsRequest.h"
#import "FWTagsFeedModel.h"
#import "FWScrollViewFollowCollectionView.h"
#import "FWMainTouchScrollView.h"
#import "FWMyPicsCollectionCell.h"

static NSString * const allCellId = @"allCellId";
static NSString * const videoCellId = @"videoCellId";
static NSString * const picsCellId = @"picsCellId";

@interface FWTagsViewController ()<ShareViewDelegate>
{
    FWMainTouchScrollView *rootScrollView; //rootScrollView要有滑动穿透
    FWScrollViewFollowCollectionView *thFollow; //必须写成属性
    NSInteger  ScrollHeight;//需三处保持一致

    FWFeedModel * allFeedModel;
    FWFeedModel * videoFeedModel;
    FWFeedModel * picsFeedModel;

    NSMutableArray * allDataSource;
    NSMutableArray * videoDataSource;
    NSMutableArray * picsDataSource;

    NSInteger   allPageNum;
    NSInteger   videoPageNum;
    NSInteger   picsPageNum;

    /* 左右划是否刷新（第一次划到视频或者草稿要刷新，其他情况只有上下拉刷新） */
    BOOL    isRefresh;
    BOOL   isVideoRefresh;
    BOOL   isDraftRefresh;
}

@end

@implementation FWTagsViewController
@synthesize tagsView;
@synthesize allCollectionView;
@synthesize videoCollectionView;
@synthesize picsCollectionView;
@synthesize collectionType;
@synthesize collectionScr;
@synthesize tagsModel;

#pragma mark - > ******************* 网络请求 *******************
#pragma mark - > 关注
- (void)attentionButtonClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        NSString * action ;
        
        if ([allFeedModel.follow_status isEqualToString:@"1"]) {
            //已关注
            action = Submit_cancel_follow_tags;
        }else if ([allFeedModel.follow_status isEqualToString:@"2"]){
            //未关注
            action = Submit_follow_tags;
        }
        
        FWTagsRequest * request = [[FWTagsRequest alloc] init];
        
        NSDictionary * param = @{
                                 @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                 @"tag_id":self.tags_id
                                 };
        [request startWithParameters:param WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([action isEqualToString:Submit_cancel_follow_tags]) {
                    //取消关注成功
                    allFeedModel.follow_status = @"2";
                }else{
                    //关注成功
                    allFeedModel.follow_status = @"1";
                    
                    [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self];
                }
                [tagsView congifViewWithModel:allFeedModel];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 获取该该标签全部列表信息
- (void)requestAllListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        allPageNum = 1;
        
        if (allDataSource.count > 0 ) {
            [allDataSource removeAllObjects];
        }
    }else{
        allPageNum += 1;
    }
    
    FWTagsRequest * request = [[FWTagsRequest alloc] init];
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"tag_id":self.tags_id,
                             @"feed_type":@"0",
                             @"pagesize":@"20",
                             @"page":@(allPageNum).stringValue,
                             @"feed_id":@"0",
                             @"order_type":self.order_type,
                             @"type":@"topic",
                             };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_tags_v3  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            allFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [allDataSource addObjectsFromArray: allFeedModel.feed_list];
            
            if([allFeedModel.feed_list count] == 0 &&
               allPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.allCollectionView reloadData];
            }
            
            if ([self.order_type isEqualToString:@"new"]) {
                tagsView.isTimeSort = YES;
            }else{
                tagsView.isTimeSort = NO;
            }
            
            [tagsView congifViewWithModel:allFeedModel];
            
            FWMineModel * mineModel = [[FWMineModel alloc] init];
            [tagsView.barView setTabNmuber:mineModel];
            
            if (isLoadMoredData) {
                [allCollectionView.mj_footer endRefreshing];
            }else{
                [allCollectionView scrollToTopAnimated:YES];
                [rootScrollView.mj_header endRefreshing];
            }
            
            
            NSDictionary * dictionary = @{
                                          @"tag_id":allFeedModel.tag_info.tag_id,
                                          @"tag_name":allFeedModel.tag_info.tag_name,
                                          };
            [GFStaticData saveObject:dictionary forKey:CurrentTagsDictionary];
            [self refreshFrame];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 获取该该标签视频列表信息
- (void)requestVideoListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        
        videoPageNum = 1;
        if (videoDataSource.count > 0 ) {
            [videoDataSource removeAllObjects];
        }
    }else{
        videoPageNum += 1;
    }
    
    FWTagsRequest * request = [[FWTagsRequest alloc] init];
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"tag_id":self.tags_id,
                             @"feed_type":@"1",
                             @"pagesize":@"20",
                             @"page":@(videoPageNum).stringValue,
                             @"feed_id":@"0",
                             @"order_type":self.order_type,
                             @"type":@"topic",
                             };
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_tags_v3  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            videoFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [videoDataSource addObjectsFromArray: videoFeedModel.feed_list];
            
            if([videoFeedModel.feed_list count] == 0 &&
               videoPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.videoCollectionView reloadData];
            }
            
            if (isLoadMoredData) {
                [videoCollectionView.mj_footer endRefreshing];
            }else{
                [videoCollectionView scrollToTopAnimated:YES];
                [rootScrollView.mj_header endRefreshing];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 获取该该标签图文列表信息
- (void)requestPicsListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        
        picsPageNum = 1;
        if (picsDataSource.count > 0 ) {
            [picsDataSource removeAllObjects];
        }
    }else{
        picsPageNum += 1;
    }
    
    FWTagsRequest * request = [[FWTagsRequest alloc] init];
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"tag_id":self.tags_id,
                             @"feed_type":@"2",
                             @"pagesize":@"20",
                             @"page":@(picsPageNum).stringValue,
                             @"feed_id":@"0",
                             @"order_type":self.order_type,
                             @"type":@"topic",
                             };
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_tags_v3  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            picsFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [picsDataSource addObjectsFromArray: picsFeedModel.feed_list];
            
            if([picsFeedModel.feed_list count] == 0 &&
               picsPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.picsCollectionView reloadData];
            }
            
            if (isLoadMoredData) {
                [picsCollectionView.mj_footer endRefreshing];
            }else{
                [picsCollectionView scrollToTopAnimated:YES];
                [rootScrollView.mj_header endRefreshing];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}
#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    [self trackPageBegin:@"话题页"];

    [self.navigationController.navigationBar setShadowImage:[FWClearColor image]];

    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {
        
        if (collectionType == FWTagsAllCollectionType) {
            int tempNum = -1;
            for (int i = 0; i < allDataSource.count; i++) {
                FWFeedListModel * tempModel = allDataSource[i];
                
                if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                    tempNum = i;
                }
            }
            
            if (tempNum >=0) {
                [allDataSource removeObjectAtIndex:tempNum];
                [allCollectionView reloadData];
            }
        }else if (collectionType == FWTagsVideoCollectionType){
            
            int tempNum = -1;
            for (int i = 0; i < videoDataSource.count; i++) {
                FWFeedListModel * tempModel = videoDataSource[i];
                
                if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                    tempNum = i;
                }
            }
            
            if (tempNum >=0) {
                [videoDataSource removeObjectAtIndex:tempNum];
                [videoCollectionView reloadData];
            }
        }else if (collectionType == FWTagsPicsCollectionType){
            
            int tempNum = -1;
            for (int i = 0; i < picsDataSource.count; i++) {
                FWFeedListModel * tempModel = picsDataSource[i];
                
                if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                    tempNum = i;
                }
            }
            
            if (tempNum >=0) {
                [picsDataSource removeObjectAtIndex:tempNum];
                [picsCollectionView reloadData];
            }
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"话题页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    allDataSource = @[].mutableCopy;
    videoDataSource = @[].mutableCopy;
    picsDataSource = @[].mutableCopy;

    isRefresh = YES;
    isVideoRefresh = YES;
    isDraftRefresh = YES;
    
    allPageNum = 0;
    videoPageNum = 0;
    ScrollHeight = 180;
    self.order_type = @"hot";
    
    [self setupSubviews];
    [self setupCollectionView];

    //启动默认是所有列表
    collectionType = FWTagsAllCollectionType;

    [self requestAllListWithLoadMoreData:NO];
    
    NSString * tagName = self.tagsModel.tag_name?self.tagsModel.tag_name:self.tags_name;
    if (self.traceType == 1) {
        [TalkingData trackEvent:@"标签" label:@"发现" parameters:@{@"标签名":tagName}];
    }else if (self.traceType == 2){
        [TalkingData trackEvent:@"标签" label:@"帖子" parameters:@{@"标签名":tagName}];
    }else if (self.traceType == 3){
        [TalkingData trackEvent:@"标签" label:@"搜索推荐" parameters:@{@"标签名":tagName}];
    }else if (self.traceType == 4){
        [TalkingData trackEvent:@"标签" label:@"搜索结果" parameters:@{@"标签名":tagName}];
    }else if (self.traceType == 5){
        [TalkingData trackEvent:@"标签" label:@"改装灵感" parameters:@{@"标签名":tagName}];
    }else{}
}

#pragma mark - > 视图初始化
- (void)setupSubviews{
    
    UIButton * shareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [shareButton setImage:[UIImage imageNamed:@"new_black_share"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        shareButton.hidden = NO;
    }else{
        shareButton.hidden = YES;
    }
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
}

#pragma mark - > 刷新视图坐标
- (void)refreshFrame{
    
    CGFloat height = [tagsView getCurrentViewHeight];
    
    ScrollHeight = [tagsView getBarViewTopHeight];

    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+height);

    tagsView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
    
    self.collectionScr.frame = CGRectMake(0, CGRectGetMaxY(tagsView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-90);
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH*3, 0);

    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView,videoCollectionView,picsCollectionView]];
}

#pragma mark - > 初始化collectionview
- (void)setupCollectionView{
    
    rootScrollView = [[FWMainTouchScrollView alloc] init];
    rootScrollView.frame  = CGRectMake(0, 0,SCREEN_WIDTH, SCREEN_HEIGHT);
    rootScrollView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    rootScrollView.delegate=self; //要遵循代理
    rootScrollView.showsVerticalScrollIndicator = NO;
    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);  //滚动大小
    [self.view addSubview:rootScrollView];
    
    tagsView = [[FWTagsView alloc] init];
    tagsView.tagsDelegate = self;
    tagsView.vc = self;
    tagsView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 225);
    [rootScrollView addSubview:tagsView];
    
    self.collectionScr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(tagsView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-90)];
    self.collectionScr.delegate = self;
    self.collectionScr.pagingEnabled = YES;
    self.collectionScr.showsVerticalScrollIndicator = NO;
    self.collectionScr.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [rootScrollView addSubview:self.collectionScr];
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH*3, 0);
    
    self.allLayout = [[FWBaseCollectionLayout alloc] init];
    self.allLayout.columns = 2;
    self.allLayout.rowMargin = 12;
    self.allLayout.colMargin = 12;
    self.allLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.allLayout.delegate = self;
    [self.allLayout autuContentSize];
    
    self.allCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.allLayout];
    self.allCollectionView.dataSource = self;
    self.allCollectionView.delegate = self;
    self.allCollectionView.showsVerticalScrollIndicator = NO;
    self.allCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.allCollectionView registerClass:[FWMyworksCollectionCell class] forCellWithReuseIdentifier:allCellId];
    self.allCollectionView.isNeedEmptyPlaceHolder = YES;
    self.allCollectionView.verticalOffset = -150;
    NSString * allTitle = @"还没有发布的作品哦~";
    NSDictionary * allAttributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * allAttributeString = [[NSMutableAttributedString alloc] initWithString:allTitle attributes:allAttributes];
    self.allCollectionView.emptyDescriptionString = allAttributeString;
    [self.collectionScr addSubview:self.allCollectionView];
    
    self.videoLayout = [[FWBaseCollectionLayout alloc] init];
    self.videoLayout.columns = 2;
    self.videoLayout.rowMargin = 12;
    self.videoLayout.colMargin = 12;
    self.videoLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.videoLayout.delegate = self;
    [self.videoLayout autuContentSize];
    
    self.videoCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.videoLayout];
    self.videoCollectionView.dataSource = self;
    self.videoCollectionView.delegate = self;
    self.videoCollectionView.showsVerticalScrollIndicator = NO;
    self.videoCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.videoCollectionView registerClass:[FWMyVideoCollectionCell class] forCellWithReuseIdentifier:videoCellId];
    self.videoCollectionView.isNeedEmptyPlaceHolder = YES;
    self.videoCollectionView.verticalOffset = -150;
    NSString * videoTitle = @"还没有发布的视频哦~";
    NSDictionary * videoAttributes = @{
                                     NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                     NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * videoAttributeString = [[NSMutableAttributedString alloc] initWithString:videoTitle attributes:videoAttributes];
    self.videoCollectionView.emptyDescriptionString = videoAttributeString;
    [self.collectionScr addSubview:self.videoCollectionView];
   

    self.picsLayout = [[FWBaseCollectionLayout alloc] init];
    self.picsLayout.columns = 2;
    self.picsLayout.rowMargin = 12;
    self.picsLayout.colMargin = 12;
    self.picsLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.picsLayout.delegate = self;
    [self.picsLayout autuContentSize];
    
    self.picsCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*2,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.picsLayout];
    self.picsCollectionView.dataSource = self;
    self.picsCollectionView.delegate = self;
    self.picsCollectionView.showsVerticalScrollIndicator = NO;
    self.picsCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.picsCollectionView registerClass:[FWMyVideoCollectionCell class] forCellWithReuseIdentifier:picsCellId];
    self.picsCollectionView.isNeedEmptyPlaceHolder = YES;
    self.picsCollectionView.verticalOffset = -150;
    NSString * picsTitle = @"还没有发布的图片哦~";
    NSDictionary * picsAttributes = @{
                                       NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                       NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * picsAttributeString = [[NSMutableAttributedString alloc] initWithString:picsTitle attributes:picsAttributes];
    self.picsCollectionView.emptyDescriptionString = picsAttributeString;
    [self.collectionScr addSubview:self.picsCollectionView];
    
    //利用initRoot创建 给予底部上下滑动的ScrollView 要滑动的ScrollRange 大小和左右滑动的Table数组
    thFollow = [[FWScrollViewFollowCollectionView alloc]init];
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView,videoCollectionView,picsCollectionView]];

    {
        @weakify(self);
        rootScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            @strongify(self);
            if (self.collectionType == FWTagsAllCollectionType) {
                [self requestAllListWithLoadMoreData:NO];
            }else if (self.collectionType == FWTagsVideoCollectionType){
                [self requestVideoListWithLoadMoreData:NO];
            }else if (self.collectionType == FWTagsPicsCollectionType){
                [self requestPicsListWithLoadMoreData:NO];
            }
        }];
        
        self.allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestAllListWithLoadMoreData:YES];
        }];
        self.videoCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestVideoListWithLoadMoreData:YES];
        }];
        self.picsCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestPicsListWithLoadMoreData:YES];
        }];
    }
    
    self.participateButton = [[UIButton alloc] init];
    [self.participateButton setImage:[UIImage imageNamed:@"participate"] forState:UIControlStateNormal];
    [self.participateButton addTarget:self action:@selector(participateButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.participateButton];
    [self.view bringSubviewToFront:self.participateButton];
    [self.participateButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).mas_offset(-4);
        make.size.mas_equalTo(CGSizeMake(52, 52));
        make.bottom.mas_equalTo(self.view).mas_offset(-89-FWSafeBottom);
    }];
}

#pragma mark - > 参与活动
- (void)participateButtonOnClick{
    
    [self checkLogin];
    
    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self];
        }
    }else {
        if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
            
            [TalkingData trackEvent:@"话题-参与" label:@"" parameters:nil];
            
            FWPublishViewController * vc = [[FWPublishViewController alloc] init];
            vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
            vc.fd_prefersNavigationBarHidden = YES;
            vc.navigationController.navigationBar.hidden = YES;
            
            [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
            
            CATransition *animation = [CATransition animation];
            [animation setDuration:0.4];
            [animation setType: kCATransitionMoveIn];
            [animation setSubtype: kCATransitionFromTop];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            [self.navigationController pushViewController:vc animated:NO];
            [self.navigationController.view.layer addAnimation:animation forKey:nil];
        }
    }
}

#pragma mark - > 切换排序
- (void)changeSortWithType:(NSString *)sortType{
    self.order_type = sortType;
    
    if (collectionType == FWTagsAllCollectionType) {
        [self requestAllListWithLoadMoreData:NO];
    }else if (collectionType == FWTagsVideoCollectionType){
        [self requestVideoListWithLoadMoreData:NO];
    }else if (collectionType == FWTagsPicsCollectionType){
        [self requestPicsListWithLoadMoreData:NO];
    }
}

#pragma mark - > 分享
- (void)shareBtnOnClick{
    
    ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
    [shareView setupTopicQRViewWithModel:allFeedModel];
    [shareView showView];
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0)
        {
            [[ShareManager defaultShareManager] topicShareToMinProgramObjectWithModel:allFeedModel];
        }else if (index == 1){
            UIImageView * imageView = [[ShareView defaultShareView] getCurrenImage];
            [[ShareManager defaultShareManager] shareToPengyouquan:imageView WithFeedID:allFeedModel.tag_info.tag_id WithType:Share_tag_id];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}

#pragma mark - > collectionView delegate & datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger number;
    if (collectionView == allCollectionView) {
        number = allDataSource.count?allDataSource.count:0;
    }else if (collectionView == videoCollectionView){
        number = videoDataSource.count?videoDataSource.count:0;
    }else {
        number = picsDataSource.count?picsDataSource.count:0;
    }
    
    return number;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == allCollectionView) {
        FWMyworksCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:allCellId forIndexPath:indexPath];
        cell.viewController = self;
        
        if (allDataSource.count > 0) {
            
            FWFeedListModel * model = allDataSource[indexPath.item];
            [cell configForCell:model];
            
            if (model.h5_url.length > 0 ||
                [model.flag_create isEqualToString:@"1"]) {
                
                cell.activityImageView.hidden = NO;
                
                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);
                
                if ([model.flag_create isEqualToString:@"1"]){
                    // 发起者
                    imageName = @"faqizhe";
                    imageViewSize = CGSizeMake(50, 21);
                }
                
                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }
        return cell;
    }else if (collectionView == videoCollectionView) {
        FWMyVideoCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:videoCellId forIndexPath:indexPath];
        cell.viewController = self;

        if (videoDataSource.count > 0) {
            
            FWFeedListModel * model = videoDataSource[indexPath.item];
            [cell configForCell:model];
            
            if (model.h5_url.length > 0 ||
                [model.flag_create isEqualToString:@"1"]) {
                
                cell.activityImageView.hidden = NO;
                
                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);
                
                if ([model.flag_create isEqualToString:@"1"]){
                    // 发起者
                    imageName = @"faqizhe";
                    imageViewSize = CGSizeMake(50, 21);
                }
                
                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }
        return cell;
    }else {
        FWMyPicsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:picsCellId forIndexPath:indexPath];
        cell.viewController = self;
        
        if (picsDataSource.count > 0) {
            
            FWFeedListModel * model = picsDataSource[indexPath.item];
            [cell configForCell:model];
            
            if (model.h5_url.length > 0 ||
                [model.flag_create isEqualToString:@"1"]) {
                
                cell.activityImageView.hidden = NO;
                
                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);
                
                if ([model.flag_create isEqualToString:@"1"]){
                    // 发起者
                    imageName = @"faqizhe";
                    imageViewSize = CGSizeMake(50, 21);
                }
                
                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr;
    if (collectionView == allCollectionView) {
        tempArr = allDataSource;
    }else if (collectionView == videoCollectionView) {
        tempArr = videoDataSource;
    }else{
        tempArr = picsDataSource;
    }
    
    FWFeedListModel * model = tempArr[indexPath.row];
    NSMutableArray * tempDataSource = @[].mutableCopy;
    
    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }
    
    if ([model.feed_type isEqualToString:@"1"]) {
       
        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {};
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            for (NSInteger i = indexPath.row; i<tempArr.count; i++) {
                FWFeedListModel * model = tempArr[i];
                
                if ([model.feed_type isEqualToString:@"1"]) {
                    [tempDataSource addObject:model];
                }
            }
            
            // 视频流
            FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
            controller.currentIndex = 0;
            controller.tag_id = self.tags_id;
            controller.order_type = self.order_type;// 按热度排序传hot，按时间排序传new
            controller.listModel = tempDataSource;
            controller.requestType = FWTagListRequestType;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        for (FWFeedListModel * model in tempArr) {
            if ([model.feed_type isEqualToString:@"2"]) {
                [tempDataSource addObject:model];
            }
        }
        
        // 图文详情
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.listModel = model;
        TVC.feed_id = model.feed_id;
        TVC.myBlock = ^(FWFeedListModel *awemeModel) {};
        [self.navigationController pushViewController:TVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {
        
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"4"]) {
        // 问答
        FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        // 改装
        FWRefitCaseDetailViewController * RCDVC = [[FWRefitCaseDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"6"]) {
        FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }
}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr;
    if (collectionType == FWTagsAllCollectionType) {
        tempArr = allDataSource;
    }else if (collectionType == FWTagsVideoCollectionType) {
        tempArr = videoDataSource;
    }else if (collectionType == FWTagsPicsCollectionType) {
        tempArr = picsDataSource;
    }
    
    if (indexPath.item >= tempArr.count) {
        return 0;
    }
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"2"]) {
        // 图文贴 固定比例3：4
        height = (cover_width *4)/3;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if([model.feed_type isEqualToString:@"3"]){
        // 文章帖 固定比例5：4
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if ([model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 20;
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 25;
        }
    }else  if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}

#pragma mark - > 展开所有描述
- (void)showAllButtonClick{
    
    [self refreshFrame];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (self.collectionScr == scrollView){
        
        float scrollViewX = scrollView.contentOffset.x;
        if (scrollViewX == 0) {
            [self headerBtnClick:tagsView.barView.firstButton];
        }else if (scrollViewX == SCREEN_WIDTH){
            [self headerBtnClick:tagsView.barView.secondButton];
        }else if (scrollViewX == SCREEN_WIDTH *2){
            [self headerBtnClick:tagsView.barView.thirdButton];
        }
    }
    
    if (scrollView == rootScrollView) {
        if (scrollView.contentOffset.y >= ScrollHeight-5) {
            self.title = allFeedModel.tag_info.tag_name;
        }else if (scrollView.contentOffset.y < ScrollHeight-5){
            self.title = @"";
        }
    }
    
    [thFollow followScrollViewScrollScrollViewScroll:scrollView];
}

#pragma mark -> 点击关注 & 推荐
- (void)headerBtnClick:(UIButton *)sender{
    
    if ([sender isEqual:tagsView.barView.firstButton]) {
        
        collectionType = FWTagsAllCollectionType;
        
        if (isRefresh) {
            isRefresh = NO;
            [self requestAllListWithLoadMoreData:NO];
        }else{
            [allCollectionView reloadData];
        }
        
        tagsView.barView.firstButton.selected = YES;
        tagsView.barView.secondButton.selected =  NO;
        tagsView.barView.thirdButton.selected =  NO;

        tagsView.barView.shadowView.frame = CGRectMake(CGRectGetMinX(tagsView.barView.firstButton.frame)+(CGRectGetWidth(tagsView.barView.firstButton.frame)-32)/2, CGRectGetMaxY(tagsView.barView.firstButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if([sender isEqual:tagsView.barView.secondButton]) {
        
        collectionType = FWTagsVideoCollectionType;
        
        if (isVideoRefresh) {
            isVideoRefresh = NO;
            [self requestVideoListWithLoadMoreData:NO];
        }else{
            [videoCollectionView reloadData];
        }
        
        tagsView.barView.firstButton.selected = NO;
        tagsView.barView.secondButton.selected =  YES;
        tagsView.barView.thirdButton.selected =  NO;

        tagsView.barView.shadowView.frame = CGRectMake(CGRectGetMinX(tagsView.barView.secondButton.frame)+(CGRectGetWidth(tagsView.barView.secondButton.frame)-32)/2, CGRectGetMaxY(tagsView.barView.secondButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if([sender isEqual:tagsView.barView.thirdButton]) {
        
        collectionType = FWTagsPicsCollectionType;
        
        
        if (isDraftRefresh) {
            isDraftRefresh = NO;
            [self requestPicsListWithLoadMoreData:NO];
        }else{
            [picsCollectionView reloadData];
        }
        
        tagsView.barView.firstButton.selected = NO;
        tagsView.barView.secondButton.selected =  NO;
        tagsView.barView.thirdButton.selected = YES;

        tagsView.barView.shadowView.frame = CGRectMake(CGRectGetMinX(tagsView.barView.thirdButton.frame)+(CGRectGetWidth(tagsView.barView.thirdButton.frame)-32)/2, CGRectGetMaxY(tagsView.barView.thirdButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }

    //点击btn的时候scrollowView的contentSize发生变化
    [self.collectionScr setContentOffset:CGPointMake((collectionType-1) *SCREEN_WIDTH, 0) animated:YES];
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

@end
