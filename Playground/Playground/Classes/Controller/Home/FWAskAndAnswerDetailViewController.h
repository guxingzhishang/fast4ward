//
//  FWAskAndAnswerDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/3.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWFeedModel.h"
#import "FWRefitCaseDetailHeaderView.h"
#import "FWCommentView.h"
#import "FWAskAndAnswerDetailHeaderView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWAskAndAnswerDetailViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource,FWCommentViewDelegate,FWAskAndAnswerDetailHeaderViewDelegate>

@property (nonatomic, strong) FWAskAndAnswerDetailHeaderView * headerView;

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) FWCommentView * commentView;

@property (nonatomic, strong) NSString * feed_id;

@property (nonatomic, strong) NSString * comment_id;

/**
 * 如果要获取指定ID的回复,可以传p_comment_id,默认为0
 */
@property (nonatomic, strong) NSString * p_comment_id;

/**
 * 要回复的comment_id,默认为0,要回复的任意帖子ID
 */
@property (nonatomic, strong) NSString * reply_comment_id;

/**
 * 评论总数
 */
@property (nonatomic, strong) NSString * total_count;

/**
 * 第几页
 */
@property (nonatomic, assign) NSInteger pageNum;

/**
 * 数据源
 */
@property (nonatomic, strong) NSMutableArray * commentDataSource;

/**
 * 回调数据源
 */
@property (nonatomic, strong) NSMutableArray * backDataSource;

@property (nonatomic, strong) FWFeedListModel * listModel;

@property (nonatomic, assign) NSInteger  currentSection;
@property (nonatomic, assign) NSInteger  currentRow;

@property (nonatomic, strong) FWMineInfoModel * reply_userInfo;


@property (nonatomic, strong) NSString * sort_type;

//@property (nonatomic, copy) modelBlock myBlock;

@property (nonatomic, assign) FWFeedDetailRequestType requestType;


@end

NS_ASSUME_NONNULL_END
