//
//  FWGraphTextViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWGraphTextViewController.h"
#import "FWCommentViewController.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWLikeRequest.h"
#import "FWGraphTextDetailViewController.h"
#import "FWFollowRequest.h"
#import "FWViewPictureViewController.h"
#import "FWHomeRequest.h"
#import "FWEmptyView.h"

@interface FWGraphTextViewController ()<ShareViewDelegate>

@property (nonatomic, strong) FWFeedModel    * feedModel;
@property (nonatomic, strong) NSMutableArray * feedDataSource;
@property (nonatomic, assign) NSInteger        feedPageNum;
@property (nonatomic, assign) NSInteger        currentIndex;
@property (nonatomic, assign) NSInteger        shareIndex;

@end

@implementation FWGraphTextViewController
@synthesize infoTableView;
@synthesize feedPageNum;
@synthesize feedModel;
@synthesize feedDataSource;

#pragma mark - > 请求列表
- (void)requestListWithLoadMoredData{
    FWHomeRequest * request = [[FWHomeRequest alloc] init];
   
    NSString * action;
    NSDictionary * params;
    
    NSDate * startDare = [NSDate date];

    FWFeedListModel * model = [self.listModel firstObject];
    
    if (self.requestType == FWAttentionRequestType) {
        // 关注列表
        params  = @{
                    @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                    @"page":@(feedPageNum).stringValue,
                    @"feed_id":model.feed_id,
                    @"page_size":@"20",
                    @"watermark":@"1",
                    };
        action = Get_feeds_by_follow_users;
    }else if (self.requestType == FWRecommendRequestType) {
        // 推荐列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"feed_id":self.feed_id,
                   @"flag_return":self.flag_return,
                   @"need_top_feed":@"2",
                   @"page_size":@"20",
                   @"watermark":@"1",
                   };
        action = Get_feeds_by_suggestion_v3;
    }else if (self.requestType == FWSearchResultListRequestType) {
        // 搜索结果页随机列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"pagesize":@"20",
                   @"page":@(feedPageNum).stringValue,
                   @"feed_id":model.feed_id,
                   @"watermark":@"1",
                   };
        action = Get_feeds_by_rand;
    }else if (self.requestType == FWMatchListRequestType){
        // 赛事列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"page":@(feedPageNum).stringValue,
                   @"feed_id":self.feed_id?self.feed_id:model.feed_id,
                   @"page_size":@"20",
                   };
        action = Get_sport_index;
    }else{
        // 标签列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"page":@(feedPageNum).stringValue,
                   @"page_size":@"20",
                   @"feed_id":self.feed_id?self.feed_id:model.feed_id,
                   @"feed_type":@"2",
                   @"tag_id":self.tag_id,
                   @"order_type":@"new",
                   @"type":@"nav",
                   @"watermark":@"1",
                   };
        action = Get_feeds_by_tags_v3;
    }

    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        double durationDate = [[NSDate date] timeIntervalSinceDate:startDare];
        
        NSLog(@"耗时：%f 秒",durationDate);
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            [infoTableView.mj_footer endRefreshing];

            feedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.feedDataSource addObjectsFromArray: feedModel.feed_list];
            
            if([feedModel.feed_list count] == 0 &&
               feedPageNum != 1){

                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
            
            feedPageNum +=1;
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"图文流页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"图文流页"];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    self.feedDataSource = @[].mutableCopy;
    feedPageNum = 1;
    
    /* 推荐：要将传来的数据加入数据源中，因为推荐过的不会再推荐，
       如果不存入，就不会显示点击的那条数据。
       关注：不需要存入数组中，服务器会根据第一条的feed_id,重新返回包含这条数据在内的20条数据*/
    if (self.requestType == FWRecommendRequestType) {
        [self.feedDataSource addObjectsFromArray:self.listModel];
    }

    [self setupSubViews];

    [self requestListWithLoadMoredData];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self requestTrace];
    });
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop);
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.estimatedRowHeight = 100;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.infoTableView registerClass:[FWGraphTextCell class]  forCellReuseIdentifier:@"GraphTextCellID"];

    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    DHWeakSelf;
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestListWithLoadMoredData];
    }];

    [self.view addSubview:infoTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.feedDataSource.count?self.feedDataSource.count:0 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    FWGraphTextCell * cell = [tableView dequeueReusableCellWithIdentifier:@"GraphTextCellID"];
    cell.delegate            = self;
    cell.viewController      = self;
    cell.picImageButton.tag  = 1000+indexPath.row;
    cell.showAllButton.tag   = 2000+indexPath.row;
    cell.likesButton.tag     = 3000+indexPath.row;
    cell.commentButton.tag   = 4000+indexPath.row;
    cell.shareButton.tag     = 5000+indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.listModel = self.feedDataSource[indexPath.row];
    
    self.currentIndex = indexPath.row;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        
    FWGraphTextDetailViewController * DVC = [[FWGraphTextDetailViewController alloc] init];
    DVC.listModel = self.feedDataSource[indexPath.row];
    DVC.feed_id = ((FWFeedListModel *)self.feedDataSource[indexPath.row]).feed_id;
    DVC.myBlock = ^(FWFeedListModel *listModel) {
        self.feedDataSource[indexPath.row] = listModel;
    };
    [self.navigationController pushViewController:DVC animated:YES];
}


#pragma mark - > FWGraphTextCellCellDelegate
#pragma mark - > 关注
-(void)attentionButtonClick:(NSInteger)index WithView:(FWGraphTextCell *)cell{
    /*隐藏关注按钮*/
}

#pragma mark - > 展示全部
-(void)showAllButtonClick:(NSInteger)index WithView:(FWGraphTextCell *)cell{
    
    NSIndexPath *indexPath = [self.infoTableView indexPathForCell:cell];

    FWFeedListModel * model = self.feedDataSource[indexPath.row];
    
    model.isShow = !model.isShow;
    
    cell.listModel = model;
    
    [UIView performWithoutAnimation:^{
        [infoTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationAutomatic)];
    }];
}

#pragma mark - > 查看图片组
- (void)picImageClick:(NSInteger)index{
    
    FWViewPictureViewController * PVC = [[FWViewPictureViewController alloc] init];
    PVC.model = self.feedDataSource[index];
    PVC.currentIndex = index;
    [self.navigationController pushViewController:PVC animated:YES];
}

#pragma mark - > 喜欢
-(void)likesButtonClick:(NSInteger)index WithView:(FWGraphTextCell *)cell{
    /*cell中实现了*/
}

#pragma mark - > 评论
-(void)commentButtonClick:(NSInteger)index{
    
    FWCommentViewController * VC = [[FWCommentViewController alloc] init];
    VC.feed_id = ((FWFeedListModel *)self.feedDataSource[index]).feed_id;
    VC.listModel = self.feedDataSource[index];
    [self.navigationController pushViewController:VC animated:YES];
}

#pragma mark - > 分享
- (void)shareButtonClick:(NSInteger)index{
   
    self.shareIndex = index;
 
    NSDictionary * params = @{
                              @"aweme":self.feedDataSource[self.shareIndex],
                              @"type":@"2",
                              };
    
    SharePopView *popView = [[SharePopView alloc] initWithParams:params];
    popView.viewcontroller = self;
    popView.aweme = self.feedDataSource[self.shareIndex];
    popView.myBlock = ^(FWFeedListModel *awemeModel) {};
    [popView show];
}

#pragma mark - >  ScrollView delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
//    [self requestTrace];
}

#pragma mark - > 统计
- (void)requestTrace{
    
    if (self.feedDataSource.count > 0 && self.currentIndex >=0) {
        FWFeedListModel * model = self.feedDataSource[self.currentIndex];
        NSString * url = Trace_URL(model.feed_id, model.feed_type, model.uid);
        
        FWHomeRequest * request = [[FWHomeRequest alloc] init];
        [request requestTraceWithURL:url];
    }
}

@end
