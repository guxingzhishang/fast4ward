//
//  FWViewPictureViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/16.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWViewPictureViewController.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "SharePicPopView.h"

@interface FWViewPictureViewController ()
@end

@implementation FWViewPictureViewController
@synthesize pictureView;
@synthesize model;

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"图文贴大图浏览"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"图文贴大图浏览"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;

    [self setupSubViews];
}

- (void)setupSubViews{
    
    pictureView = [[FWViewPictureView alloc] init];
    pictureView.delegate = self;
    pictureView.vc = self;
    [self.view addSubview:pictureView];
    [pictureView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [pictureView configViewWithModel:model];
    [pictureView.imageScrollView setContentOffset:CGPointMake(self.currentIndex * SCREEN_WIDTH, 0) animated:NO];
}

#pragma mark - > 关注
- (void)attentionButtonClick {/* 内部实现了 */}

#pragma mark - > 返回
- (void)backButtonClick {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 点击头像
- (void)photoButtonClick {/* 内部实现了 */}

#pragma mark - > 保存图片
- (void)saveButtonClick {/* 内部实现了 */}

#pragma mark - > 分享
- (void)shareButtonClick {

    int index = self.pictureView.imageScrollView.contentOffset.x / self.pictureView.imageScrollView.bounds.size.width;
    
    NSDictionary * params = @{
                              @"aweme":model,
                              @"type":model.feed_type,
                              @"hideDelete":@"YES",
                              @"currentIndex":@(index).stringValue,
                              };
    
    @autoreleasepool {
        SharePopView *popView = [[SharePopView alloc] initWithParams:params];
        popView.viewcontroller = self;
        popView.aweme = model;
        popView.myBlock = ^(FWFeedListModel *awemeModel) {
            self.model = awemeModel;
        };
        [popView show];
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        return;
    }
}

@end
