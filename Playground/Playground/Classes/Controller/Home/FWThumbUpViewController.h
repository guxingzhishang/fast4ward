//
//  FWThumbUpViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 首页 - > 消息 - > 点赞 & 评论
 */

#import "FWBaseViewController.h"
#import "FWThumbUpCell.h"
#import "FWUnionMessageModel.h"
#import "FWLikesMessageRequest.h"
#import "FWCommentMessageRequest.h"
#import "FWTableView.h"

typedef NS_ENUM(NSInteger,FWMessageListType) {
    FWMessageThumbUpList = 1, //点赞列表
    FWMessageCommentList, //评论列表
    FWMessageWendaList, //问答列表
};


@interface FWThumbUpViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource>

/**
 * 消息列表
 */
@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, assign) FWMessageListType listType;


@end
