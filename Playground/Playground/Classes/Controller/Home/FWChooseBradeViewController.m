//
//  FWChooseBradeViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWChooseBradeViewController.h"
#import "FWChooseCarTypeViewController.h"
#import "FWAddBrandViewController.h"


@interface FWChooseBradeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, assign) NSInteger   pageNum;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWChooseCarBrandModel * brandModel;
@end

@implementation FWChooseBradeViewController
@synthesize infoTableView;
@synthesize pageNum;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)requestList{
        
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params ;
    
    request.isNeedShowHud = YES;
    NSString * action ;
    
    if ([self.chooseFrom isEqualToString:@"gaizhuang"]) {
        action = Get_gaizhuang_brand;
        params = @{
                    @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                    @"type":self.type,
                    };
    }else{
        action = Get_car_brand;
        params = @{
                    @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                    };
    }
    
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (self.dataSource.count > 0 ) {
                [self.dataSource removeAllObjects];
            }

            self.brandModel = [FWChooseCarBrandModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.dataSource addObjectsFromArray: self.brandModel.list];
            [infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"选择品牌页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"选择品牌页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"选择品牌";
    
    [self setupSubViews];
    [self requestList];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 50;
    infoTableView.sectionIndexColor = FWTextColor_969696;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        if ([self.chooseFrom isEqualToString:@"gaizhuang"]) {
            make.bottom.mas_equalTo(self.view).mas_offset(-60-FWSafeBottom);
        }else{
            make.bottom.mas_equalTo(self.view);
        }
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    if ([self.chooseFrom isEqualToString:@"gaizhuang"]) {
        UIButton * tianxieButton = [[UIButton alloc] init];
        tianxieButton.layer.borderColor = FWColor(@"2d2d2d").CGColor;
        tianxieButton.layer.borderWidth = 1;
        tianxieButton.layer.cornerRadius = 2;
        tianxieButton.layer.masksToBounds = YES;
        tianxieButton.titleLabel.font = DHSystemFontOfSize_14;
        [tianxieButton setTitle:@"没有我的品牌" forState:UIControlStateNormal];
        [tianxieButton setTitleColor:FWColor(@"2d2d2d") forState:UIControlStateNormal];
        [tianxieButton addTarget:self action:@selector(tianxieButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:tianxieButton];
        tianxieButton.frame = CGRectMake(20, SCREEN_HEIGHT-60-FWSafeBottom-64-FWCustomeSafeTop, SCREEN_WIDTH-40, 36);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    for (UIView *view in [tableView subviews]) {
        if ([view isKindOfClass:[NSClassFromString(@"UITableViewIndex") class]]) {
           // 设置字体大小
            [view setValue:[UIFont systemFontOfSize:13 weight:UIFontWeightRegular] forKey:@"_font"];
           //设置view的大小
            view.bounds = CGRectMake(0, 10, 40, 40);
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.dataSource.count > section) {
        FWChooseCarBrandListModel * listModel = self.dataSource[section];
        return listModel.list.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"chooseBrandID";
    
    FWChooseBradeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWChooseBradeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.chooseFrom = self.chooseFrom;
    
    if (self.dataSource.count > indexPath.section) {
        FWChooseCarBrandListModel * listModel = self.dataSource[indexPath.section];
        if (listModel.list.count > indexPath.row) {
            cell.titleLabel.text = listModel.list[indexPath.row].brand;
            
            [cell configforCell:listModel.list[indexPath.row]];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (self.dataSource.count > indexPath.section) {
        FWChooseCarBrandListModel * listModel = self.dataSource[indexPath.section];
        if (listModel.list.count > indexPath.row) {
            
            if ([self.chooseFrom isEqualToString:@"gaizhuang"]) {
                self.myBlock(listModel.list[indexPath.row].brand);
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                FWChooseCarTypeViewController * CCTVC = [[FWChooseCarTypeViewController alloc] init];
                CCTVC.brand = listModel.list[indexPath.row].brand;
                CCTVC.chooseFrom = self.chooseFrom;
                [self.navigationController pushViewController:CCTVC animated:YES];
            }
        }
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
   
    if (self.dataSource.count > section) {
        FWChooseCarBrandListModel * listModel = self.dataSource[section];
        return listModel.F_C;
    }
    
    return @"";
}

//建立浮动索引
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return [self.brandModel.list_FC copy];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    for (int i = 0; i < [tableView subviews].count; i++) {
        UIView *view = [[tableView subviews] objectAtIndex:i];

        if ([view isKindOfClass:[NSClassFromString(@"UITableViewIndex") class]]) {
            if (i == index) {
                view.backgroundColor = FWTextColor_BCBCBC;
            }else{
                view.backgroundColor = FWClearColor;
            }
        }
    }
    
    return index;
}


#pragma mark - > 填写自己的品牌
- (void)tianxieButtonClick{
 
    @weakify(self);
    FWAddBrandViewController * vc = [[FWAddBrandViewController alloc] init];
    vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    vc.brandBlock = ^(NSString * _Nonnull brand) {
        @strongify(self);
        if (brand.length > 0) {
            self.myBlock(brand);
        }
        [self.navigationController popViewControllerAnimated:NO];
    };
    [self presentViewController:vc animated:YES completion:nil];
}
@end


@implementation FWChooseBradeTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.iconImageView = [[UIImageView alloc] init];
//    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = DHSystemFontOfSize_15;
    self.titleLabel.textColor = FWTextColor_222222;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(10);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(25);
    }];
}

- (void)configforCell:(FWChooseCarBrandSubListModel *)model{
    
    if ([self.chooseFrom isEqualToString:@"anchexi"] ||
        [self.chooseFrom isEqualToString:@"chooseChexi"] ||
        [self.chooseFrom isEqualToString:@"publish"] ||
        [self.chooseFrom isEqualToString:@"addcar"]) {
        /* 显示图标 */
        [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(30, 30));
            make.centerY.mas_equalTo(self.contentView);
            make.left.mas_equalTo(self.contentView).mas_offset(14);
        }];
        
        self.iconImageView.clipsToBounds = YES;
        self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.logo] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }else{
        /* 不显示图标 */
        [self.iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(0.01, 30));
            make.centerY.mas_equalTo(self.contentView);
            make.left.mas_equalTo(self.contentView).mas_offset(0);
        }];
    }
}
@end
