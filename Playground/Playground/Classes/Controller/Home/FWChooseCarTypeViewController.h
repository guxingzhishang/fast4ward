//
//  FWChooseCarTypeViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWChooseCarTypeViewController : FWBaseViewController

@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * chooseFrom; // publish 发布页； anchexi 按车系查看； gaizhuang 编辑改装 chooseChexi 选择车系（发布改装页）  addcar 添加座驾

@end

NS_ASSUME_NONNULL_END
