//
//  FWGraphTextDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/4.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 图文贴详情
 */

#import "FWCommentViewController.h"
#import "FWFeedModel.h"
#import "FWGraphTextDetailHeaderView.h"
#import "FWCommentView.h"
#import "FWGraphTextViewController.h"

typedef void(^modelBlock)(FWFeedListModel * listModel);

@interface FWGraphTextDetailViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource,FWCommentViewDelegate,FWGraphTextDetailHeaderViewDelegate>

@property (nonatomic, strong) FWGraphTextDetailHeaderView * headerView;

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) FWCommentView * commentView;

@property (nonatomic, strong) NSString * feed_id;

@property (nonatomic, strong) NSString * comment_id;

/**
 * 如果要获取指定ID的回复,可以传p_comment_id,默认为0
 */
@property (nonatomic, strong) NSString * p_comment_id;

/**
 * 要回复的comment_id,默认为0,要回复的任意帖子ID
 */
@property (nonatomic, strong) NSString * reply_comment_id;

/**
 * 评论总数
 */
@property (nonatomic, strong) NSString * total_count;

/**
 * 第几页
 */
@property (nonatomic, assign) NSInteger pageNum;

/**
 * 数据源
 */
@property (nonatomic, strong) NSMutableArray * commentDataSource;

/**
 * 回调数据源
 */
@property (nonatomic, strong) NSMutableArray * backDataSource;

@property (nonatomic, strong) FWFeedListModel * listModel;

@property (nonatomic, assign) NSInteger  currentSection;
@property (nonatomic, assign) NSInteger  currentRow;

@property (nonatomic, strong) FWMineInfoModel * reply_userInfo;

@property (nonatomic, copy) modelBlock myBlock;

@property (nonatomic, assign) FWFeedDetailRequestType requestType;

@end
