//
//  FWGodDistrictViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/11.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGodDistrictViewController.h"
#import "FWGodDistrictView.h"

@interface FWGodDistrictViewController ()<UITableViewDelegate,UITableViewDataSource,FWHomeCellDelegate>
@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) FWFeedModel * godModel;

@property (nonatomic, strong) FWGodDistrictView * headerView;

@end

@implementation FWGodDistrictViewController
@synthesize infoTableView;
@synthesize dataSource;
@synthesize pageNum;
@synthesize godModel;
@synthesize headerView;

#pragma mark - > 请求大神专区数据
- (void)requestGodListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    if (isLoadMoredData == NO) {
        pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        pageNum +=1;
    }
    
    NSDictionary * params  = @{
                               @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                               @"page":@(pageNum).stringValue,
                               @"feed_id":@"0",
                               @"page_size":@"20",
                               @"feed_type":@"0",
                               };
    
    [request startWithParameters:params WithAction:Get_feeds_by_dashen  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            godModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [headerView configForView:godModel];
            
            [self.dataSource addObjectsFromArray: godModel.feed_list];
            
            if([godModel.feed_list count] == 0 &&
               pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                [[FWHudManager sharedManager] showErrorMessage:@"您还未登录，请先登录才能查看关注哦~" toController:self];
                return ;
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    [self trackPageBegin:@"大神专区页"];
    
    int tempNum = -1;
    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {
        
        for (int i = 0; i < self.dataSource.count; i++) {
            FWFeedListModel * tempModel = self.dataSource[i];
            
            if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                tempNum = i;
            }
        }
        
        if (tempNum >=0) {
            [self.dataSource removeObjectAtIndex:tempNum];
            [infoTableView reloadData];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"大神专区页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"大神专区";
    
    pageNum = -1;
    dataSource = @[].mutableCopy;
    
    [self setupSubViews];
    [self requestGodListWithLoadMoredData:NO];
}


- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.estimatedRowHeight = 100;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.backgroundColor = FWTextColor_F8F8F8;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
        make.left.bottom.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view).mas_offset(10);
    }];
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestGodListWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestGodListWithLoadMoredData:YES];
    }];
    
    headerView = [[FWGodDistrictView alloc] init];
    headerView.vc = self;
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH,200);
    
    infoTableView.tableHeaderView = headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"GodDistrictCellID";
    FWHomeCell * cell =  [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (nil == cell) {
        cell = [[FWHomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    [cell.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cell.contentView).mas_offset(0);
        make.left.mas_equalTo(cell.contentView).mas_offset(14);
        make.right.mas_equalTo(cell.contentView).mas_offset(-14);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_greaterThanOrEqualTo(100);
        make.bottom.mas_equalTo(cell.contentView).mas_offset(-10);
    }];
    
    cell.delegate            = self;
    cell.vc                  = self;
    cell.bannerImageView.tag  = 1000+indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.attentionButton.hidden = NO;
    if (indexPath.row < self.dataSource.count) {
        cell.listModel = self.dataSource[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [infoTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.dataSource.count) {
        [self dealWithOperationWithIndex:indexPath.row];
    }
}

- (void)picImageClick:(NSInteger)index{
    
    [self dealWithOperationWithIndex:index];
}

- (void)dealWithOperationWithIndex:(NSInteger)index{
    
    NSMutableArray * tempArr = self.dataSource;
    NSInteger requestType = FWGodListRequestType;
    NSMutableArray * tempDataSource = @[].mutableCopy;
    
    FWFeedListModel * model = tempArr[index];
    
    if (tempArr.count <= 0) {
        return;
    }
    
    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }
    
    if ([model.feed_type isEqualToString:@"1"]) {
        
        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.listModel = model;
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {
                self.dataSource[index] = listModel;
            };
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            for (NSInteger i = index; i<tempArr.count; i++) {
                FWFeedListModel * model = tempArr[i];
                
                if ([model.feed_type isEqualToString:@"1"]) {
                    [tempDataSource addObject:model];
                }
            }
            
            // 视频流
            FWVideoPlayerViewController * VPVC = [[FWVideoPlayerViewController alloc] init];
            VPVC.currentIndex = 0;
            VPVC.requestType = requestType;
            VPVC.flag_return = @"2";
            VPVC.feed_id = model.feed_id;
            VPVC.listModel = tempDataSource;
            [self.navigationController pushViewController:VPVC animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        // 图文详情
        FWGraphTextDetailViewController * DVC = [[FWGraphTextDetailViewController alloc] init];
        DVC.listModel = tempArr[index];
        DVC.feed_id = ((FWFeedListModel *)tempArr[index]).feed_id;
        DVC.myBlock = ^(FWFeedListModel *listModel) {
            self.dataSource[index] = listModel;
        };
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }
}

@end
