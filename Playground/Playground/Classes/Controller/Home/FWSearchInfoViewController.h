//
//  FWSearchInfoViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/19.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 * 首页搜索页
 */
#import "FWBaseViewController.h"
#import "FWSearchInfoView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWSearchInfoViewController : FWBaseViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,FWSearchInfoViewDelegate>


/**
 * 做navigation的view
 */
@property (nonatomic, strong) UIView * headerView;

/**
 * 取消按钮
 */
@property (nonatomic, strong) UIButton * cancelBtn;

/**
 * 搜索
 */
@property (nonatomic, strong) UISearchBar * searchBar;

/**
 * 列表的tableview
 */
@property (nonatomic, strong) UITableView * listTableView;

@property (nonatomic, strong) FWSearchInfoView * infoView;

@end

NS_ASSUME_NONNULL_END
