//
//  FWIdleViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/14.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWIdleViewController : FWBaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FWBaseCollectionLayoutDalegate>

@property (nonatomic, strong) UIScrollView * topScrollView;

@property (nonatomic, strong) UIScrollView * collectionScr;

/* 全部列表 */
@property (nonatomic, strong) FWCollectionView * allCollectionView;

@property(nonatomic, strong) FWBaseCollectionLayout *allLayout;

@end

NS_ASSUME_NONNULL_END
