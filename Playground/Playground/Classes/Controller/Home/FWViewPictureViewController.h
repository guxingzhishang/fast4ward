//
//  FWViewPictureViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/16.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 图文贴点击图片浏览图组
 */

#import "FWBaseViewController.h"
#import "FWViewPictureView.h"

@interface FWViewPictureViewController : FWBaseViewController<FWViewPictureViewDelegate>

@property (nonatomic, strong) FWViewPictureView * pictureView;
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) FWFeedListModel * model;


@end
