//
//  FWBaseViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWBaseViewModel.h"

@interface FWBaseViewController : UIViewController

@property (nonatomic, strong) FWBaseViewModel * viewModel;

@property (nonatomic, strong) NSDictionary * backDict;

@property (nonatomic, strong) UILabel * badgeLabel;

@property (nonatomic, assign) NSInteger unreandCount ;


- (instancetype)initWithViewModel:(FWBaseViewModel *)viewModel;

//绑定数据模型
- (void)bindViewModel;
- (void)addSubviews;
//- (void)layoutNavigation;

-(void)backBtnClick;

- (UINavigationController *)currentSelectedNavigationController;

- (UIViewController *)currentViewController;

- (void)didSelectedTabBarControllerForClass:(Class)selectedClass;

- (void)pushViewControllerWtihAppModuleModel:(id)model;

- (void)cheackLogin;

- (void)checkTabbarLogin;

- (void)monitorNetworking;

- (void)networkUnAvailable:(NSNotification *)noti;

- (void)setUpNavigationBarTheme_White;
- (void)setUpNavigationBarTheme_Black;

/* 统计 */
- (void)trackPageBegin:(NSString *)pageName;
- (void)trackPageEnd:(NSString *)pageName;

/* 上传结束后，在关注页调用这个方法 */
- (void)uploadProgress:(NSNotification *)noti;

- (void)uploadSuccess:(NSNotification *)noti;

- (void)uploadFailure:(NSNotification *)noti;

/* 请求未读消息数 */
- (void)requestUnreadMessage;

/**
 进行访问记录
 @param f_uid 被访问者uid,必须传
 @param visitor_type 访问类型1为个人主页2为帖子
 @param feed_id 帖子ID，如果类型不是帖子，传0
 */
- (void)requestVisitorWithUserID:(NSString*)f_uid WithType:(NSString*)visitor_type WithID:(NSString *)feed_id;

@end
