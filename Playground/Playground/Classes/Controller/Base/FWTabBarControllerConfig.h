//
//  FWTabBarControllerConfig.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import <CYLTabBarController/CYLTabBarController.h>
#import "FWTabBarViewModel.h"

@interface FWTabBarControllerConfig : FWBaseViewController<CYLTabBarControllerDelegate>

@property (nonatomic, strong) CYLTabBarController *tabBarController;
@property (nonatomic, copy) NSString *context;
@property (nonatomic, strong) NSArray * viewControllers;

@end
