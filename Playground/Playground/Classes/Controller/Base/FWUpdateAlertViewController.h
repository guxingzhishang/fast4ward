//
//  FWUpdateAlertViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWUpdateAlertViewController : FWBaseViewController<UIScrollViewDelegate>

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIView * mainView;
@property (nonatomic, strong) UIView * containerView;
@property (nonatomic, strong) UIView * noticeView;
@property (nonatomic, strong) UIButton * updateButton;
@property (nonatomic, strong) UIButton * cancelButton;

@property (nonatomic, strong) FWSettingModel * settingModel;

@end

NS_ASSUME_NONNULL_END
