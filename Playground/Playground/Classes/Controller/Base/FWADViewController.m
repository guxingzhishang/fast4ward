//
//  FWADViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/29.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWADViewController.h"
#import "FWHomeRequest.h"
#import "FWSettingModel.h"
#import "FWMatchViewController.h"
#import <SDWebImageDownloader.h>

#define CancelTouchHeight (SCREEN_HEIGHT * 110 / 568)

#define CountDown   5

@interface FWADViewController (){
    CAShapeLayer *shapeLayer;
    NSTimer *timer;
    NSString * img_id;
    NSInteger showTime;//展示过了几张
    CGFloat skipBottomHeight; // 广告页
}


@property (nonatomic, strong) UIImageView *loadingPage;
@property (nonatomic, strong) UILabel *timeCounting;
@property (nonatomic, strong) UIButton *skip;// 跳过广告按钮

@property (nonatomic, strong) UILabel *adLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *appIconImageView;

@property (nonatomic, strong) NSDictionary *json;// 服务器返回的Json串
// 当下载完营销页广告isNeedRemovePage会变为NO。这个属性是用来移除ADViewController
@property (nonatomic, assign) BOOL isNeedRemovePage;
// 当营销广告展示出来的时候。点击这个就可以移除ADViewController，同时跳转到相应的界面。
@property (nonatomic, assign) BOOL isCanSkip;

@property (nonatomic, strong) NSTimer *timer;// 营销页显示计时器
@property (nonatomic, assign) NSInteger leftTime;// 展示营销页面的时间。3秒
@property (nonatomic, strong) FWSettingModel *settingModel;

@property (nonatomic, assign) NSInteger  adCount;//数据返回的广告数量

@end

@implementation FWADViewController

#pragma mark - > Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView * bgImageView = [[UIImageView alloc] init];
    NSString * iamgeNamed = @"";
    if(DH_isiPhone4){
        iamgeNamed = @"Default-568h";
    }else if(DH_isiPhone5){
        iamgeNamed = @"Default-568h";
    }else if(DH_isiPhone6){
        iamgeNamed = @"Default-667h";
    }else if(DH_isiPhone6Plus){
        iamgeNamed = @"Default-736h";
    }else if(DH_isiPhoneX){
        iamgeNamed = @"Default-812h";
    }else if(DH_isiPhoneXR){
        iamgeNamed = @"Default-896";
    }else if(DH_isiPhoneXMax){
        iamgeNamed = @"Default-896h";
    }
    bgImageView.image = [UIImage imageNamed:iamgeNamed];
    [self.view addSubview:bgImageView];
    bgImageView.frame = self.view.bounds;
    
    
    showTime = 0;
    self.adCount = -1;
    self.isNeedRemovePage = YES;
    self.isCanSkip = NO;
    self.view.frame = [UIScreen mainScreen].bounds;
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self setupUI];
    
    [self requestLaunchInfo];
    
    if(![[GFStaticData getObjectForKey:Network_Available] boolValue]) {
        // 没网直接销毁
        [self removeADVCWithModifyScale:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self trackPageBegin:@"广告页"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(CountDown * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (self.isNeedRemovePage == YES)
        {
            [DHWindow removeDHViewController:self];
        }
    });
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"广告页"];
}

- (void)dealloc{
    
    NSLog(@"销毁了");
    [timer invalidate];
}

#pragma mark - > Custom Method

- (void)setupUI
{
    /* 先用启动页图片顶一会，防止在启动页消失后，广告出现前显示空白页面 */
    self.loadingPage = [[UIImageView alloc] init];
    self.loadingPage.backgroundColor = FWTextColor_D8D8D8;
    self.loadingPage.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.loadingPage];
  
    CGFloat bottomHeight = 0;
    /*
     * ios (4)750*1102、(7)1242*1825、(8) 1242*1825  就目前看 4（6、6s）、7（plus） 、8（齐刘海系列）
     */
    if(DH_isiPhone4){
        img_id = @"4";
        bottomHeight = 95;
        skipBottomHeight = 60;
    }else if(DH_isiPhone5){
        img_id = @"4";
        bottomHeight = 98;
        skipBottomHeight = 60;
    }else if(DH_isiPhone6){
        img_id = @"4";
        bottomHeight = 96;
        skipBottomHeight = 60;
    }else if(DH_isiPhone6Plus){
        img_id = @"7";
        bottomHeight = 128;
        skipBottomHeight = 80;
    }else if(DH_isiPhoneX){
        img_id = @"8";
        bottomHeight = 116;
        skipBottomHeight = 80;
    }else if(DH_isiPhoneXR){
        img_id = @"8";
        bottomHeight = 140;
        skipBottomHeight = 100;
    }else if(DH_isiPhoneXMax){
        img_id = @"8";
        bottomHeight = 140;
        skipBottomHeight = 100;
    }
    
    [self.loadingPage mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view).mas_offset(-bottomHeight);
        make.right.left.top.mas_equalTo(self.view);
    }];
    
    self.loadingPage.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLinks:)];
    [self.loadingPage addGestureRecognizer:tap];
    
    [self didFinishLoadADImage];
    
    self.loadingPage.hidden = YES;
}

- (void)setupFinishView{
    
    self.adLabel = [[UILabel alloc] init];
    self.adLabel.text = @"广告";
    self.adLabel.font = DHSystemFontOfSize_12;
    self.adLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.adLabel.textAlignment = NSTextAlignmentCenter;
    self.adLabel.layer.cornerRadius = 5;
    self.adLabel.layer.masksToBounds = YES;
    [self.loadingPage addSubview:self.adLabel];
    [self.adLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.loadingPage).mas_offset(15);
        make.top.mas_equalTo(self.loadingPage).mas_offset(15+SafeTop);
        make.size.mas_equalTo(CGSizeMake(50, 23));
    }];
    
    
    self.skip = [UIButton buttonWithType:UIButtonTypeCustom];
    self.skip.layer.cornerRadius = 15;
    self.skip.layer.masksToBounds = YES;
    self.skip.titleLabel.font = DHSystemFontOfSize_12;
    self.skip.backgroundColor = FWColorWihtAlpha(@"#000000", 0.4);
    [self.skip setTitle:@"跳过" forState:UIControlStateNormal];
    [self.skip setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.skip addTarget:self action:@selector(skipOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.loadingPage addSubview:self.skip];
    [self.skip mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.adLabel);
        make.right.mas_equalTo(self.loadingPage).mas_offset(-15);
        make.size.mas_equalTo(CGSizeMake(60, 30));
    }];
    
    UIImage * bgimage;
    if(DH_isiPhone4){
        bgimage = [UIImage imageNamed:@"Default-568h"];
    }else if(DH_isiPhone5){
        bgimage = [UIImage imageNamed:@"Default-568h"];
    }else if(DH_isiPhone6){
        bgimage = [UIImage imageNamed:@"Default-667h"];
    }else if(DH_isiPhone6Plus){
        bgimage = [UIImage imageNamed:@"Default-736h"];
    }else if(DH_isiPhoneX){
        bgimage = [UIImage imageNamed:@"Default-812h"];
    }else if(DH_isiPhoneXR){
        bgimage = [UIImage imageNamed:@"Default-896"];
    }else if(DH_isiPhoneXMax){
        bgimage = [UIImage imageNamed:@"Default-896h"];
    }
    
    self.appIconImageView = [[UIImageView alloc] init];
    self.appIconImageView.userInteractionEnabled = YES;
    self.appIconImageView.image = bgimage;
    [self.view addSubview:self.appIconImageView];
    [self.appIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    
    [self.view bringSubviewToFront:self.loadingPage];
}

- (void)didFinishLoadADImage
{
    self.leftTime = CountDown;

    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCounting) userInfo:nil repeats:YES];
}

#pragma mark - > 点击广告
/**
 *1:赛事主页   2:直播页  3:回放页   4:文章详情页  5:图文详情页   6:话题页  7:商家店铺页  8:商品详情页   9:H5 10:商家排行榜   11：车友排行榜   12:无跳转  13：视频贴
 */
- (void)tapLinks:(UITapGestureRecognizer *)gesture{
    
    //点击是否有操作判断
    if (self.isCanSkip == NO) return;

    NSInteger index = [[GFStaticData getObjectForKey:LastADIndex] integerValue]?[[GFStaticData getObjectForKey:LastADIndex] integerValue]:0;
    NSArray * adlistArray = self.settingModel.list_ad;
    
    if(adlistArray.count <= 0) return;

    if(adlistArray.count <= index){
        index = 0;
    }
    
    FWListADModel * adModel = adlistArray[index];

    if (adModel.ad_id && [adModel.ad_id integerValue] > 0) {
        /* 广告统计（点击）*/
        [self requestADWithAction:@"click" withID:adModel.ad_id];
    }
    
    if ([adModel.ad_type isEqualToString:@"1"]){
        /* 赛事主页 */
//        FWMatchViewController * MVC = [[FWMatchViewController alloc] init];
//        MVC.fd_prefersNavigationBarHidden = NO;
//        [self.navi pushViewController:MVC animated:YES];
        self.navi.tabBarController.selectedIndex = 1;
        [self removeADVCWithModifyScale:NO];
    }else if ([adModel.ad_type isEqualToString:@"4"]){
        /* 文章详情页 */
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id =  adModel.ad_val;
        [self.navi pushViewController:DVC animated:YES];
        [self removeADVCWithModifyScale:NO];
    }else if ([adModel.ad_type isEqualToString:@"5"]){
        /* 图文详情页 */
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.feed_id = adModel.ad_val;
        TVC.requestType = FWPushMessageRequestType;
        [self.navi pushViewController:TVC animated:YES];
        [self removeADVCWithModifyScale:NO];
    }else if ([adModel.ad_type isEqualToString:@"6"]){
        /* 标签（话题）页 */
        FWTagsViewController * PVC = [[FWTagsViewController alloc] init];
        PVC.tags_id = adModel.ad_val;
        [self.navi pushViewController:PVC animated:YES];
        [self removeADVCWithModifyScale:NO];
    }else if ([adModel.ad_type isEqualToString:@"7"]) {
        /* 商家店铺页*/
        FWCustomerShopViewController * CSVC = [[FWCustomerShopViewController alloc] init];
        CSVC.user_id = adModel.ad_val;
        CSVC.isUserPage = NO;
        [self.navi pushViewController:CSVC animated:YES];
        [self removeADVCWithModifyScale:NO];
    }else if ([adModel.ad_type isEqualToString:@"8"]) {
        /* 商品详情页 */
        FWGoodsDetailViewController * GDVC = [[FWGoodsDetailViewController alloc] init];
        GDVC.goods_id = adModel.ad_val;
        [self.navi pushViewController:GDVC animated:YES];
        [self removeADVCWithModifyScale:NO];
    }else if ([adModel.ad_type isEqualToString:@"9"]){
        /* 打开H5活动页 */
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.htmlStr = adModel.ad_val;
        WVC.pageType = WebViewTypeURL;
        [self.navi pushViewController:WVC animated:NO];
        [self removeADVCWithModifyScale:NO];
    }else if ([adModel.ad_type isEqualToString:@"13"]){
        /* 视频详情页 */
        FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
        controller.currentIndex = 0;
        controller.feed_id = adModel.ad_val;
        controller.requestType = FWPushMessageRequestType;
        [self.navi pushViewController:controller animated:YES];
        [self removeADVCWithModifyScale:NO];
    }else if ([adModel.ad_type  isEqualToString:@"16"]){
        /* 长视频详情页 */
        FWLongVideoPlayerViewController *controller = [[FWLongVideoPlayerViewController alloc] init];
        controller.feed_id = adModel.ad_val;
        controller.myBlock = ^(FWFeedListModel *listModel) {};
        [self.navi pushViewController:controller animated:YES];
        [self removeADVCWithModifyScale:NO];
    }
}

- (void)removeADVCWithModifyScale:(BOOL)isNeedModifyScale
{
    [UIView animateWithDuration:0.4 animations:^{
        self.view.alpha = 0.0f;
        if (isNeedModifyScale == YES){
            
            self.view.transform = CGAffineTransformMakeScale(1.7, 1.7);
        }
    } completion:^(BOOL finished) {
        
        [self.timer invalidate];
        [DHWindow removeDHViewController:self];
    }];
}

#pragma mark - > Networking
- (void)requestADWithAction:(NSString *)ad_action withID:(NSString *)ad_id{
    
    if (ad_id.length <= 0 && !ad_id) {
        return;
    }
    
    FWHomeRequest * request = [[FWHomeRequest alloc] init];
    NSString * uid = [GFStaticData getObjectForKey:kTagUserKeyID]?[GFStaticData getObjectForKey:kTagUserKeyID]:@"0";
    
    NSDictionary * params = @{
                              @"uid":uid,
                              @"ad_id":ad_id,
                              @"ad_action":ad_action
                              };
    
    [request startWithParameters:params WithAction:Submit_ad_action WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
//        NSLog(@"back==%@",back);
    }];
}

- (void)requestLaunchInfo{
 
    FWHomeRequest * request = [[FWHomeRequest alloc] init];
    
    NSString * uid = [GFStaticData getObjectForKey:kTagUserKeyID]?[GFStaticData getObjectForKey:kTagUserKeyID]:@"0";
    
    NSDictionary * params = @{@"uid":uid,
                              @"img_id":img_id,
                              };
    
    [request startWithParameters:params WithAction:Get_settings WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
//        NSLog(@"back===%@",back);
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            self.adCount = self.settingModel.list_ad.count;
            
            if (self.adCount <= 0) {
                // 如果服务器返回的没有广告，不显示
                [GFStaticData saveObject:nil forKey:ADStore];
                [self removeADVCWithModifyScale:NO];
                return ;
            }else{
                
                if ([self allowShowLocationAlert]) {
                    
                    NSMutableArray * tempArr = [[GFStaticData getObjectForKey:ADStore] mutableCopy];
                    if (tempArr.count > 0) {
                        /* 每天第一次启动，删除本地缓存更新数据 */
                        [tempArr removeAllObjects];
                        [GFStaticData saveObject:nil forKey:ADStore];
                    }
                }
            }
            
            dispatch_async(dispatch_queue_create(0,0), ^{
                
                /* 先取出上次展示的第几条广告 ,如果已经是最后一条，就直接显示第一条，如果不是最后一条，就往后+1 进行展示*/
                NSInteger index = [[GFStaticData getObjectForKey:LastADIndex] integerValue]?[[GFStaticData getObjectForKey:LastADIndex] integerValue]:0;
                
                if (index + 1 >= self.adCount) {
                    index = 0;
                }else{
                    index += 1;
                }
//            NSLog(@"vvvv---%@,---%@",@(self.adCount).stringValue,@(index).stringValue);
                /* 存储，在点击时直接取就可以，不用进行其他操作 */
                [GFStaticData saveObject:@(index).stringValue forKey:LastADIndex];
                
                FWListADModel * adModel = self.settingModel.list_ad[index];
                
                DHWeakSelf;
                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:adModel.ad_img] options:0 progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                    weakSelf.isNeedRemovePage = NO;
                    if (image) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf dealWithImage:image];
                        });
                    }
                    else {
                        [weakSelf removeADVCWithModifyScale:NO];
                    }
                }];
            });
        }else{
            [self removeADVCWithModifyScale:NO];
        }
    }];
}

#pragma mark - > 显示广告
- (void)dealWithImage:(UIImage *)image{
    
    if (showTime > 1) {
        return;
    }
    
    [UIView transitionWithView:self.loadingPage duration:0.5f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        
        self.loadingPage.image = image;
//        self.loadingPage.image = [UIImage imageNamed:@"test1"];
    } completion:^(BOOL finished) {
        
        [GFStaticData saveObject:[Utility currentTimeStr] forKey:LastADShowTime];
       
        self.isCanSkip = YES;
        
        self.loadingPage.hidden = NO;
        
        NSInteger index = [[GFStaticData getObjectForKey:LastADIndex] integerValue]?[[GFStaticData getObjectForKey:LastADIndex] integerValue]:0;
        NSArray * adlistArray = self.settingModel.list_ad;

        if (adlistArray.count > 0) {
            if(adlistArray.count <= index){
                index = 0;
            }
            
            FWListADModel * adModel = adlistArray[index];
            
            if (adModel.ad_id&& [adModel.ad_id integerValue] > 0) {
                /* 广告统计（展示）*/
                [self requestADWithAction:@"show" withID:adModel.ad_id];
            }
        }
        
        [self setupFinishView];
    }];
}

- (void)timerCounting
{
    self.leftTime = self.leftTime - 1;
    if (self.leftTime < 0)
    {
        [self removeADVCWithModifyScale:YES];
    }
}

#pragma mark - > 跳过
- (void)skipOnClick{
    [self removeADVCWithModifyScale:YES];
}


/**
 *   是否第一次启动
 */
-(BOOL)allowShowLocationAlert{
    
    NSDate *now = [NSDate date];
    //当前时间的时间戳
    NSTimeInterval nowStamp = [now timeIntervalSince1970];
    //当天零点的时间戳
    NSTimeInterval zeroStamp = [[[NSUserDefaults standardUserDefaults] objectForKey:@"zeroStamp"] doubleValue];
    //一天的时间戳
    NSTimeInterval oneDay = 60* 60 * 24;
    
    /**
     "showedLocation"代表了是否当天是否提醒过开启定位，NO代表没有提醒过，YES代表已经提醒过
     */
    
    if(nowStamp - zeroStamp> oneDay){
        
        zeroStamp = [self getTodayZeroStampWithDate:now];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:zeroStamp] forKey:@"zeroStamp"];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"showedLocation"];
        return YES;
        
    }else{
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"showedLocation"]) {
            return NO;
            
        }else{
            return YES;
        }
        
    }
    
}

/**
 * 获取当天零点时间戳
 */
- (double)getTodayZeroStampWithDate:(NSDate *)date{
    
    NSDateFormatter *dateFomater = [[NSDateFormatter alloc]init];
    dateFomater.dateFormat = @"yyyy年MM月dd日";
    NSString *original = [dateFomater stringFromDate:date];
    NSDate *ZeroDate = [dateFomater dateFromString:original];
    // 今天零点的时间戳
    NSTimeInterval zeroStamp = [ZeroDate timeIntervalSince1970];
    return zeroStamp;
    
}
@end
