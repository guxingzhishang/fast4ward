//
//  FWNavigationController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWNavigationController.h"
#import "FWBaseViewController.h"

@interface FWNavigationController ()

@end

@implementation FWNavigationController

+ (void)initialize{
    //初始化默认的导航栏配置
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.viewControllers.count > 0) {
        // 隐藏底部tabbar
        viewController.hidesBottomBarWhenPushed = YES;
        UIColor *titleColor=DHTitleColor_FFFFFF;
        if ([viewController isKindOfClass:[FWBaseViewController class]]) {
            FWBaseViewController *vc = (FWBaseViewController *)viewController;
            titleColor=vc.viewModel.BackItemTitleColor;
        }
    }
    
    if ([self.visibleViewController isEqual:viewController]) {
        return;
    }
    
    [super pushViewController:viewController animated:animated];
}

-(UIViewController *)popViewControllerAnimated:(BOOL)animated{
    return [super popViewControllerAnimated:animated];
}

-(UIViewController *)childViewControllerForStatusBarStyle {
    return self.topViewController;
}

-(UIViewController *)childViewControllerForStatusBarHidden {
    return self.topViewController;
}
@end
