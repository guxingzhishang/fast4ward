//
//  FWWebViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/17.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWWebViewController.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWShareRankViewController.h"

@interface FWWebViewController ()<FWSubLoginDelegate>

@property (nonatomic, strong) UIActivityIndicatorView * loading;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) UIImage *sendImage;
@property (nonatomic, assign) BOOL  isCut;// 已经截屏过

@property (nonatomic, assign) BOOL  isLoading;// 正在加载

@end

@implementation FWWebViewController

@synthesize detailView;
@synthesize requestUrl;
@synthesize pageType;
@synthesize filePath;
@synthesize webTitle;
@synthesize newsId;
@synthesize htmlStr;
@synthesize loading;
@synthesize shareButton;
@synthesize sendImage;
@synthesize isCut;
@synthesize isLoading;

- (void)backBtnClick{
    
    if ([self.backToVC isEqualToString:@"mine"]) {
       
        for (UIViewController * vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[FWNewMineViewController class]]) {
                [self.navigationController popToViewController:vc animated:YES];
                return ;
            }
        }
    }else{
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if (self.needShare) {
        
        if ([self.title containsString:@"商家"]) {
            [self trackPageBegin:@"商家影响页"];
        }else if ([self.title containsString:@"车友"]){
            [self trackPageBegin:@"车友影响页"];
        }
        [self setUpNavigationBarTheme_Black];
    }else{
        [self setUpNavigationBarTheme_White];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = self.webTitle;
    isCut = NO;
    
    if (self.needShare) {
        
        shareButton = [[UIButton alloc] init];
        shareButton.userInteractionEnabled = NO;
        shareButton.frame = CGRectMake(0, 0, 30, 40);
        [shareButton setImage:[UIImage imageNamed:@"user_share"] forState:UIControlStateNormal];
        [shareButton addTarget:self action:@selector(shareButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
        
        if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
            self.navigationItem.rightBarButtonItems = @[rightBtnItem];
        }
    }
    
    detailView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop)];
    detailView.delegate = self;
    detailView.dataDetectorTypes = UIDataDetectorTypeNone;//关闭自动数字链接
    [self.view addSubview:detailView];
    
    if (pageType == WebViewTypeURL){
        htmlStr = [htmlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.htmlStr]];
        [detailView loadRequest:request];
    }else if (pageType == WebViewTypeLocalHtml){
        
        //encoding:NSUTF8StringEncoding error:nil 这一段一定要加，不然中文字会乱码
        NSString * htmlstring=[[NSString alloc] initWithContentsOfFile:self.filePath  encoding:NSUTF8StringEncoding error:nil];
        
        [detailView loadHTMLString:htmlstring baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    }else if (pageType == WebViewTypeHtml){
        [detailView loadHTMLString:self.htmlStr baseURL:nil];
    }
    
    if (@available(iOS 11.0, *)) {
        
        detailView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    //        loading.frame = CGRectMake((SCREEN_WIDTH-50)/2, 200, 50, 50);
    loading.center = self.detailView.center;
    loading.backgroundColor = [UIColor colorWithHexString:@"000000" alpha:0.8];
    loading.alpha = 0.5;
    [self.detailView addSubview:loading];
}

#pragma mark - > 分享排行榜
- (void)shareButtonOnClick{
    
    if (isCut) {
        
        FWShareRankViewController * vc = [[FWShareRankViewController alloc] init];
        vc.cutshortImage = sendImage;
        vc.titleString = self.title;
        vc.qrcode_url = self.qrcode_url;
        vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark - > 规则
- (void)ruleButtonOnClick{
    
    NSString * message = @"在肆放平台认证成为商家或者车友，即可进入影响力榜单的评选。发帖，参与活动，评论，点赞，转发都能够提升平台影响力。榜单每小时更新，每天中午12：00结算影响力。上榜用户将会获得更多曝光。";
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"影响力榜单说明" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIView *subView1 = alertController.view.subviews[0];
    UIView *subView2 = subView1.subviews[0];
    UIView *subView3 = subView2.subviews[0];
    UIView *subView4 = subView3.subviews[0];
    UIView *subView5 = subView4.subviews[0];
    
    for (UILabel * label in subView5.subviews) {
        if ([label isKindOfClass:[UILabel class]]&&
            [label.text isEqualToString:message]) {
            label.textAlignment = NSTextAlignmentLeft;
        }
    }
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

//保存成功调用的方法
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    
    [[FWHudManager sharedManager] showSuccessMessage:@"保存成功" toController:self];
}

- (UIImage *)imageRepresentation{
    
    CGSize boundsSize = self.view.bounds.size;
    CGFloat boundsWidth = self.view.bounds.size.width;
    CGFloat boundsHeight = self.view.bounds.size.height;
    
    CGPoint offset = self.detailView.scrollView.contentOffset;
    
    CGFloat contentHeight = self.detailView.scrollView.contentSize.height;
    
    // 可变数组
    NSMutableArray *images = [NSMutableArray array];
    
    while (contentHeight > 0) {
        
        // 截取整个 self.view
        UIGraphicsBeginImageContext(boundsSize);
        [self.detailView.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [images addObject:image];
        
        CGFloat offsetY = self.detailView.scrollView.contentOffset.y;
        [self.detailView.scrollView setContentOffset:CGPointMake(0, offsetY + boundsHeight)];
        contentHeight -= boundsHeight;
    }
    
    [self.detailView.scrollView setContentOffset:offset];
    
    // 给 webView 截图UIGraphicsBeginImageContext(self.webView.frame.size);
    [images enumerateObjectsUsingBlock:^(UIImage *image, NSUInteger idx, BOOL *stop) {
        [image drawInRect:CGRectMake(0, boundsHeight * idx, boundsWidth, boundsHeight)];
    }];
    UIImage *fullImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return fullImage;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *targetPage = [request.URL absoluteString];
    if ([targetPage componentsSeparatedByString:@"cmd=4b69a2ae60b5010b97a10950dc6a32e3"].count > 1)
    {
        NSArray * componentsArr = [targetPage componentsSeparatedByString:@"&"];
        NSArray * uidArr = [componentsArr[0] componentsSeparatedByString:@"uid="];
        
        NSString * uid = uidArr[1];
        
        FWNewUserInfoViewController * WDCVC = [[FWNewUserInfoViewController alloc] init];
        WDCVC.user_id = uid;
        [self.navigationController pushViewController:WDCVC animated:YES];
        return NO;
    }else if ([targetPage componentsSeparatedByString:@"cmd=20d485f9014481815a74e792aaa09f73"].count > 1){
        
        FWSubLoginViewController * LVC = [[FWSubLoginViewController alloc] init];
        LVC.delegate = self;
        [self.navigationController pushViewController:LVC animated:YES];
        return NO;
    }
    
    return YES;
}

#pragma mark - > 登录成功回调
- (void)logininSuccess{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_bc_login_url WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            NSString * url = [[back objectForKey:@"data"]objectForKey:@"url"];
            url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [detailView loadRequest:request];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // 开始加载
    
    if (!isLoading) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    isLoading = webView.isLoading ;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //   加载成功，取消加载
    
    isLoading = webView.isLoading ;
    shareButton.userInteractionEnabled = YES;

    if (self.webTitle.length <= 0) {
        NSString *htmlTitle = @"document.title";
        NSString *titleHtmlInfo = [webView stringByEvaluatingJavaScriptFromString:htmlTitle];
        
        self.title = titleHtmlInfo;
    }
    
    if (!isCut && self.needShare) {
        
        /* 需要分享的，并且还没截图过 */
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self shortCutImage];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    }else{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    /* 加载失败，取消加载 */
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (self.needShare) {
        if ([self.title containsString:@"商家"]) {
            [self trackPageEnd:@"商家影响页"];
        }else if ([self.title containsString:@"车友"]){
            [self trackPageEnd:@"车友影响页"];
        }
    }
}

#pragma mark - > 截屏
- (void)shortCutImage{
    
    isCut = YES;
    
    CGFloat px = 2;
    
    if(DH_isiPhone4){
        px = 1;
    }else if(DH_isiPhone5 || DH_isiPhone6 ||DH_isiPhoneXR){
        px = 2;
    }else if(DH_isiPhone6Plus ||DH_isiPhoneX||DH_isiPhoneXMax){
        px = 3;
    }
    
    CGFloat height = FWAdaptive(580);
    CGSize size = CGSizeMake(SCREEN_WIDTH, height);
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    [self.detailView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGImageRef imageRef = viewImage.CGImage;
    CGRect rect = CGRectMake(0, 50*px,size.width*px,size.height*px);
    //这里可以设置想要截图的区域
    CGImageRef imageRefRect =CGImageCreateWithImageInRect(imageRef, rect);
    sendImage = [[UIImage alloc] initWithCGImage:imageRefRect];
}

@end
