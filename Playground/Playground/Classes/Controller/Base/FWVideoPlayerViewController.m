//
//  FWVideoPlayerViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/13.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVideoPlayerViewController.h"
#import "ZFPlayer.h"
#import "ZFAVPlayerManager.h"
#import "ZFIJKPlayerManager.h"
#import "KSMediaPlayerManager.h"
#import "ZFPlayerControlView.h"
#import "FWVideoPlayerCell.h"
#import "FWVideoPlayerControlView.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import <MJRefresh/MJRefresh.h>

#import "FWHomeRequest.h"
#import "FWLikeRequest.h"

static NSString *kIdentifier = @"kIdentifier";

@interface FWVideoPlayerViewController ()  <UITableViewDelegate,UITableViewDataSource,FWVideoPlayerCellDelegate,FWVideoPlayerControlViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) ZFAVPlayerManager *playerManager;
@property (nonatomic, strong) FWVideoPlayerControlView *controlView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) NSMutableArray *urls;

@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) NSString * likeType;
@property (nonatomic, assign) BOOL  isTapHeart;

@property (nonatomic, assign) BOOL isLastLoadMore; // 最后一个已经加载过下一页了


@end

@implementation FWVideoPlayerViewController
@synthesize feedPageNum;
@synthesize feedModel;

#pragma mark - > 请求视频列表
- (void)requestListWithLoadMoredData{
    FWHomeRequest * request = [[FWHomeRequest alloc] init];
    
    NSString * action;
    NSDictionary * params;
    
    FWFeedListModel * model = [self.listModel firstObject];
    
    if (self.requestType == FWAttentionRequestType) {
        // 关注列表
        params  = @{
                    @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                    @"page":@(feedPageNum).stringValue,
                    @"feed_id":model.feed_id,
                    @"page_size":@"20",
                    };
        action = Get_feeds_by_follow_users;
    }else if (self.requestType == FWRecommendRequestType) {
        // 推荐列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"feed_id":self.feed_id,
                   @"flag_return":self.flag_return,
                   @"page3":self.page3,
                   @"page2_feed_id":self.page2_feed_id,
                   @"page":@(feedPageNum).stringValue,
                   @"page_size":@"40",
                   };
        action = Get_feeds_by_suggestion_v3;
    }else if (self.requestType == FWNoteRequestType) {
        // 笔记列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"page":@(feedPageNum).stringValue,
                   @"feed_id":self.feed_id,
                   @"page_size":@"20",
                   @"feed_type":@"1",
                   @"cat_id":self.cat_id,
                   };
        action = Get_feeds_by_note;
    }else if (self.requestType == FWInfoListRequestType) {
        // 个人中心所有作品、视频列表(包括好友的)
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"base_uid":self.base_uid,
                   @"feed_type":model.feed_type,
                   @"is_draft":@"2",
                   @"feed_id":model.feed_id,
                   @"page":@(feedPageNum).stringValue,
                   @"page_size":@"20",
                   };
        action = Get_feeds_by_uid_v2;
    }else if(self.requestType == FWGodListRequestType){
        // 大神专区列表
        params  = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"page":@(feedPageNum).stringValue,
                   @"feed_id":self.feed_id,
                   @"page_size":@"20",
                   @"feed_type":@"1",
                   };
        action = Get_feeds_by_dashen;
    }else if (self.requestType == FWHomeTagRequestType) {
        // 首页标签列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"page":@(feedPageNum).stringValue,
                   @"page_size":@"20",
                   @"feed_id":self.feed_id?self.feed_id:model.feed_id,
                   @"feed_type":@"1",
                   @"tag_id":self.tag_id,
                   @"order_type":@"new",
                   @"type":@"nav",
                   };
        action = Get_feeds_by_tags_v3;
    }else if (self.requestType == FWTagListRequestType) {
        // 标签所有作品、视频列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"tag_id":self.tag_id,
                   @"feed_type":model.feed_type,
                   @"pagesize":@"20",
                   @"page":@(feedPageNum).stringValue,
                   @"feed_id":model.feed_id,
                   @"order_type":self.order_type,// 按热度（时间）排序
                   @"type":@"fromtopic",
                   };
        action = Get_feeds_by_tags_v3;
    }else if (self.requestType == FWCategaryListRequestType) {
        // 改装案例页所有作品和视频的请求
        params = @{
                    @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                    @"category_id":self.category_id,
                    @"tag_id":self.tag_id,
                    @"car_type":self.car_type,
                    @"feed_id":self.feed_id,
                    @"order_type":self.order_type,
                    @"page":@(feedPageNum).stringValue,
                    @"page_size":@"20",
                    @"type":@"fromtopic",
                   };
        action = Get_feeds_by_category;
    }else if (self.requestType == FWActivityTagsRequestType) {
        // 活动页进入
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"pagesize":@"20",
                   @"page":@(feedPageNum).stringValue,
                   @"feed_type":@"1",
                   @"feed_id":self.feedListModel.feed_id?self.feedListModel.feed_id:self.feed_id,
                   };
        action = Get_feeds_by_activity_v2;
    }else if (self.requestType == FWSearchResultListRequestType) {
        // 搜索结果页随机列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"pagesize":@"20",
                   @"page":@(feedPageNum).stringValue,
                   @"feed_id":model.feed_id,
                   };
        action = Get_feeds_by_rand;
    }else if (self.requestType == FWFavouriteListRequestType) {
        // 收藏页
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"watermark":@"1",
                   @"feed_type":@"1",
                   @"feed_id":model.feed_id,
                   @"page":@(feedPageNum).stringValue,
                   @"pagesize":@"20",
                   };
        action = Get_favourite_feed_list;
    }else if (self.requestType == FWMatchListRequestType) {
        // 赛事列表
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"watermark":@"1",
                   @"feed_id":model.feed_id,
                   @"page":@(feedPageNum).stringValue,
                   @"pagesize":@"20",
                   };
        action = Get_sport_index;
    } else{
        // 推送点击进入视频详情页
        params = @{
                   @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                   @"feed_id":self.feed_id,
                   @"watermark":@"1",
                   };
        action = Get_feed_info_by_id;
    }
    
    DHWeakSelf;
    [request startWithParameters:params WithAction:action  WithDelegate:self  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        self.isLastLoadMore = NO;
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [weakSelf.tableView.mj_footer endRefreshing];
            
            feedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            
            if (self.requestType == FWPushMessageRequestType ||
                self.requestType == FWADRequestType) {
                
                // push 或 广告 进来的只显示一条
                weakSelf.tableView.scrollEnabled = NO;
                
//                if (weakSelf.feedModel.feed_info) {
                    [weakSelf.feedDataSource addObject:feedModel.feed_info];
                    
                    NSString *URLString = [weakSelf.feedModel.feed_info.video_info.playURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                    if (URLString && URLString.length > 0) {
                        NSURL *url = [NSURL URLWithString:URLString];
                        [self.urls addObject:url];
                    }
                    
                    [weakSelf.tableView reloadData];
                    
                    [self playTheIndex:0];
//                }
            }else{
                
                if (feedModel.feed_list.count > 0) {
                    FWFeedListModel * listModel = feedModel.feed_list[0];
                    
                    if ([listModel.flag_hidden isEqualToString:@"1"]){
                        
                        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:listModel.flag_hidden_msg preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
                        [alertController addAction:okAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        return ;
                    }
                }
                
                if([feedModel.feed_list count] == 0 &&
                   feedPageNum != 1){
                    weakSelf.tableView.mj_footer = nil;
                    [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:weakSelf];
                    return;
                }else{
                    [weakSelf.feedDataSource addObjectsFromArray: feedModel.feed_list];
                    
                    for (FWFeedListModel * model in weakSelf.feedModel.feed_list) {
                        if (model.video_info.playURL.length > 0) {
                            NSString *URLString = [model.video_info.playURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                            NSURL *url = [NSURL URLWithString:URLString];
                            [self.urls addObject:url];
                        }
                    }
                    
                    [weakSelf.tableView reloadData];
                }
                
                if (feedPageNum == 1) {
                    
                    if (self.urls.count > 0) {
                        [self playTheIndex:0];
                    }
                }
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:weakSelf];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
//    NSLog(@"清空了");
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"视频流页"];
    [self.player stopCurrentPlayingCell];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self trackPageBegin:@"视频流页"];
    
    // 播放视频
    if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
        if ( [[GFStaticData getObjectForKey:Current_Network] isEqualToString:@"WIFI"] ||
             [[GFStaticData getObjectForKey:Allow_4G_Play] isEqualToString:@"1"]) {
            [self videoPlay];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView * bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_video_loading"]];
    bgView.frame = self.view.bounds;
    [self.view addSubview:bgView];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.backBtn];
    
    self.fd_prefersNavigationBarHidden = YES;

    [self checkWWAN];

//    [self monitorNetworking];

    /* 推荐：要将传来的数据加入数据源中，因为推荐过的不会再推荐，
     如果不存入，就不会显示点击的那条数据。
     关注：不需要存入数组中，服务器会根据第一条的feed_id,重新返回包含这条数据在内的20条数据*/
    if (self.requestType == FWRecommendRequestType) {
        [self.feedDataSource addObjectsFromArray:self.listModel];
        
        for (FWFeedListModel * model in self.feedDataSource) {
            
            NSString *URLString = [model.video_info.playURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:URLString];
            if (!url) {
                continue;
            }
            [self.urls addObject:url];
        }
        
        [self.tableView reloadData];
    }
    
    feedPageNum = 1;
    
    if (self.requestType != FWRecommendRequestType) {
        [self requestListWithLoadMoredData];
    }
    
    /// playerManager
    self.playerManager = [[ZFAVPlayerManager alloc] init];
//    self.playerManager.scalingMode = ZFPlayerScalingModeAspectFit;
    
    /// player,tag值必须在cell里设置
    self.player = [ZFPlayerController playerWithScrollView:self.tableView playerManager:self.playerManager containerViewTag:100];
    self.player.assetURLs = self.urls;
    self.player.disableGestureTypes =   ZFPlayerDisableGestureTypesPan | ZFPlayerDisableGestureTypesPinch;
    self.player.controlView = self.controlView;
    self.player.allowOrentitaionRotation = NO;
    
    if ([[GFStaticData getObjectForKey:Allow_4G_Play] boolValue]) {
        self.player.WWANAutoPlay = YES;
    }else{
        self.player.WWANAutoPlay = NO;
    }
    
    /// 1.0是完全消失时候
    self.player.playerDisapperaPercent = 1;
    
    @weakify(self)
    self.player.playerDidToEnd = ^(id  _Nonnull asset) {
        @strongify(self)
        [self.player.currentPlayerManager replay];
    };
    
    self.player.currentPlayerManager.scalingMode = ZFPlayerScalingModeAspectFill;

//    self.player.presentationSizeChanged = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, CGSize size) {
//          @strongify(self)
//          if (size.width >= size.height) {
//              self.player.currentPlayerManager.scalingMode = ZFPlayerScalingModeAspectFit;
//          } else {
//              self.player.currentPlayerManager.scalingMode = ZFPlayerScalingModeAspectFill;
//          }
//      };
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self requestTrace];
        [self requestVisitor];
        
        if (self.requestType == FWRecommendRequestType) {
            [self requestHasReadFeed];
        }
    });
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.backBtn.frame = CGRectMake(15, CGRectGetMaxY([UIApplication sharedApplication].statusBarFrame), 36, 36);
}

- (void)playTheIndex:(NSInteger)index {
    /// 指定到某一行播放
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
    
    //播放
    [self videoPlay];

//    if (indexPath.row == self.feedDataSource.count-1) {
//        /// 加载下一页数据
//        feedPageNum +=1;
//        
//        [self requestListWithLoadMoredData];
//        self.player.assetURLs = self.urls;
//        [self.tableView reloadData];
//    }
}

#pragma mark - > 4G流量提醒
- (void)checkWWAN{
    
    if ([[GFStaticData getObjectForKey:Current_Network] isEqualToString:@"WIFI"]) {
        
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            //播放视频
            [self videoPlay];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }else{
        
        if ([[GFStaticData getObjectForKey:Network_Available] boolValue]) {
            
            if (![[GFStaticData getObjectForKey:Allow_4G_Play] boolValue]) {
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"当前正在使用流量，是否用流量观看" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                }];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [GFStaticData saveObject:@"1" forKey:Allow_4G_Play];
                    
                    self.player.WWANAutoPlay = YES;
                    
                    //播放视频
                    [self videoPlay];
                }];
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"当前网络不给力" toController:self];
        }
    }
}

#pragma mark - UIScrollViewDelegate  列表播放必须实现
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [scrollView zf_scrollViewDidEndDecelerating];

    [self checkWWAN];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [scrollView zf_scrollViewDidEndDraggingWillDecelerate:decelerate];
    
    self.isTapHeart = NO;
//    [self requestTrace];
    [self requestVisitor];
    
    if (self.requestType == FWRecommendRequestType) {
        [self requestHasReadFeed];
    }
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [scrollView zf_scrollViewDidScrollToTop];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [scrollView zf_scrollViewDidScroll];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [scrollView zf_scrollViewWillBeginDragging];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.feedDataSource.count?self.feedDataSource.count:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FWVideoPlayerCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifier];
    cell.vc = self;
    cell.delegate = self;
    
    self.currentIndex = indexPath.row;
    cell.data = self.feedDataSource[indexPath.row];
    
    @weakify(self);
    // 关注修改回调
    [[cell.payAttentionSignal takeUntil:cell.rac_prepareForReuseSignal] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        // 更新模型关注信息
        for (FWFeedListModel *info in self.feedDataSource) {
            info.is_followed = x;
        }
        // 不用reloadData单独修改buttonUI
        NSArray *visibleCell = self.tableView.visibleCells;
        for (FWVideoPlayerCell *cell in visibleCell) {
            [cell updateUI];
        }
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
}

#pragma mark - > 改变播放画面方向
- (void)changeDirection:(BOOL)isHen{
    
    if (!isHen) {
        // 横屏切换到竖屏
        CGFloat angle = 0.f;
        CGAffineTransform transform =CGAffineTransformMakeRotation(angle);
        [self.player.containerView setTransform:transform];

        self.playerManager.scalingMode = ZFPlayerScalingModeAspectFit;

        self.controlView.sliderView.hidden = NO;
        self.player.controlView.frame = self.view.bounds;
    }else{
        // 竖屏切换成横屏
        CGFloat angle = M_PI/2;
        CGAffineTransform transform =CGAffineTransformMakeRotation(angle);
        [self.player.containerView setTransform:transform];

        self.playerManager.scalingMode = ZFPlayerScalingModeNone;

        self.controlView.sliderView.hidden = YES;
        self.player.controlView.frame = self.view.bounds;
    }
}
//        self.player.controlView.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height) ;

#pragma mark - > cell点赞
- (void)cellRequestLike{
    [self requestLikes];
}

#pragma mark - > 双击cell小心心点赞
- (void)cellDoubleTapRequestLike{
    [self showLikeAnim];
}

#pragma mark - > 双击控制层小心心点赞
- (void)controlViewRequestLike{
    [self showLikeAnim];
}

- (void)showLikeAnim{
    
    FWFeedListModel * data = self.feedDataSource[self.currentIndex];
    
    if ([data.is_liked integerValue] != 1 && !self.isTapHeart) {
        self.isTapHeart = YES;
        [self requestLikes];
    }
}

#pragma mark - > 点赞
- (void)requestLikes{
    
    FWFeedListModel * data = self.feedDataSource[self.currentIndex];

    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:self.currentIndex inSection:0];
    
    [self.tableView registerClass:[FWVideoPlayerCell class] forCellReuseIdentifier:kIdentifier];
    FWVideoPlayerCell * cell = (FWVideoPlayerCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    [self cheackLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        
        if ([data.is_liked isEqualToString:@"2"]) {
            self.likeType = @"add";
        }else{
            self.likeType = @"cancel";
        }
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":data.feed_id,
                                  @"like_type":self.likeType,
                                  };
        
        [request startWithParameters:params WithAction:Submit_like_feed WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if ([self.likeType isEqualToString: @"add"]) {
                    
                    data.is_liked = @"1";
                    self.isTapHeart = YES;
                    cell.favorite.image = [UIImage imageNamed:@"car_detail_like"];
                    
                    if ([data.count_realtime.like_count_format integerValue]||
                        [data.count_realtime.like_count_format integerValue] == 0){
                        
                        data.count_realtime.like_count_format = @([data.count_realtime.like_count_format integerValue] +1).stringValue;
                        cell.favoriteNum.text = data.count_realtime.like_count_format;
                    }
                }else if ([self.likeType isEqualToString: @"cancel"]){
                    
                    data.is_liked = @"2";
                    self.isTapHeart = NO;
                    cell.favorite.image = [UIImage imageNamed:@"unlike"];
                    
                    if ([data.count_realtime.like_count_format integerValue]||
                        [data.count_realtime.like_count_format integerValue] == 0) {
                        
                        data.count_realtime.like_count_format = @([data.count_realtime.like_count_format integerValue] -1).stringValue;
                        cell.favoriteNum.text = data.count_realtime.like_count_format;
                    }
                }
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - ZFTableViewCellDelegate

- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath {
    [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
}

#pragma mark - private method

- (void)backClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/// 找到可以播放的视频并播放
- (void)videoPlay{

    @weakify(self)
    [self.tableView zf_filterShouldPlayCellWhileScrolled:^(NSIndexPath *indexPath) {
        @strongify(self)
        [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
    }];
}

/// play the video
- (void)playTheVideoAtIndexPath:(NSIndexPath *)indexPath scrollToTop:(BOOL)scrollToTop {
    [self.player playTheIndexPath:indexPath scrollToTop:scrollToTop];
    [self.controlView resetControlView];
    
    FWFeedListModel * data = self.feedDataSource[indexPath.row];
    UIViewContentMode imageMode = UIViewContentModeScaleAspectFit;
//    if (data.feed_cover_width >= data.feed_cover_height) {
//       imageMode = UIViewContentModeScaleAspectFit;
//    } else {
//       imageMode = UIViewContentModeScaleAspectFill;
//    }
    
    [self.controlView showCoverViewWithUrl:data.feed_cover withImageMode:imageMode];
    
    if (indexPath.row == self.feedDataSource.count-1 && !self.isLastLoadMore) {
        
        self.isLastLoadMore = YES;
        
        self.feedPageNum +=1;
        
        /// 加载下一页数据
        [self requestListWithLoadMoredData];
        
        self.player.assetURLs = self.urls;
        [self.tableView reloadData];
    }
}

#pragma mark - getter

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.pagingEnabled = YES;
        [_tableView registerClass:[FWVideoPlayerCell class] forCellReuseIdentifier:kIdentifier];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.frame = self.view.bounds;
        _tableView.rowHeight = _tableView.frame.size.height;
        
        /// 停止的时候找出最合适的播放
        @weakify(self)
        _tableView.zf_scrollViewDidStopScrollCallback = ^(NSIndexPath * _Nonnull indexPath) {
            @strongify(self)
            if (indexPath.row == self.feedDataSource.count-1 && !self.isLastLoadMore) {

                self.isLastLoadMore = YES;
                
                self.feedPageNum +=1;
                /// 加载下一页数据
                [self requestListWithLoadMoredData];
                
                self.player.assetURLs = self.urls;
                [self.tableView reloadData];
            }
            [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
        };
    }
    return _tableView;
}

- (FWVideoPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [FWVideoPlayerControlView new];
        _controlView.delegate = self;
    }
    return _controlView;
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = @[].mutableCopy;
    }
    return _dataSource;
}

- (NSMutableArray *)feedDataSource {
    if (!_feedDataSource) {
        _feedDataSource = @[].mutableCopy;
    }
    return _feedDataSource;
}

- (NSMutableArray *)urls {
    if (!_urls) {
        _urls = @[].mutableCopy;
    }
    return _urls;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(backClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}

#pragma mark - > 统计
- (void)requestTrace{
    
    if (self.feedDataSource.count > 0 && self.currentIndex >=0) {
        
        FWFeedListModel * model = self.feedDataSource[self.currentIndex];
        NSString * url = Trace_URL(model.feed_id, model.feed_type, model.uid);
        
        FWHomeRequest * request = [[FWHomeRequest alloc] init];
        [request requestTraceWithURL:url];
    }
}

#pragma mark - > 进行访问记录
- (void)requestVisitor{
    
    if (self.feedDataSource.count > self.currentIndex) {
        FWFeedListModel * model = self.feedDataSource[self.currentIndex];
        
        NSString * uid = [GFStaticData getObjectForKey:kTagUserKeyID];
        if ([model.uid isEqualToString:uid]) {
            // 自己进入自己页面，不请求
            return;
        }
        
        if (!uid || !model.uid || !model.feed_id) {
            return;
        }
        
        FWVisitorRequest * request = [[FWVisitorRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":uid,
                                  @"f_uid":model.uid,
                                  @"visitor_type":@"2",
                                  @"feed_id":model.feed_id,
                                  };
        [request startWithParameters:params WithAction:Submit_visitor_log WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {}];
    }
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return self.player.isStatusBarHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

#pragma mark - > 标记已读帖子
- (void)requestHasReadFeed{
    
    FWFeedListModel * model = self.feedDataSource[self.currentIndex];

    if (!model.feed_id || !model.feed_type) {
        return;
    }
    NSArray * feedsArray = @[@{@"feed_id":model.feed_id,@"feed_type":model.feed_type}];
    NSString * feed_ids = [feedsArray mj_JSONString];
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_ids":feed_ids,
                              };
    
    [request startWithParameters:params WithAction:Submit_mark_feed_id WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        //        NSLog(@"responseObject=== %@",responseObject);
    }];
}
@end
