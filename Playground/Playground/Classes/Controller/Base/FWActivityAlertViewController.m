//
//  FWActivityAlertViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWActivityAlertViewController.h"
#import "FWMineRequest.h"
#import "FWSettingModel.h"

@interface FWActivityAlertViewController ()

@property (nonatomic, strong) NSMutableArray * dataSource;

@end

@implementation FWActivityAlertViewController
@synthesize cancelButton;
@synthesize activityImageView;
@synthesize mainView;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view.backgroundColor = FWClearColor;
    [self trackPageBegin:@"首页弹屏活动页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"首页弹屏活动页"];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor clearColor];
    self.dataSource = @[].mutableCopy;
    
    [self setupSubviews];
}

- (void)setupSubviews{
    
    mainView = [[UIView alloc] init];
    mainView.backgroundColor = [UIColor colorWithHexString:@"000000" alpha:0.6];
    [self.view addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    activityImageView = [[UIImageView alloc] init];
    
    activityImageView.image = [UIImage imageNamed:@"placeholder"];
    activityImageView.contentMode = UIViewContentModeScaleAspectFill;
    activityImageView.userInteractionEnabled = YES;
    activityImageView.layer.cornerRadius = 5;
    activityImageView.layer.masksToBounds = YES;
    activityImageView.clipsToBounds = YES;
    [mainView addSubview:activityImageView];
    [activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).mas_offset(60);
        make.right.mas_equalTo(self.view).mas_offset(-60);
        make.centerY.mas_equalTo(self.view);
        make.height.mas_equalTo((SCREEN_WIDTH-120)*175/125);
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activityTap)];
    [activityImageView addGestureRecognizer:tap];

    cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setImage:[UIImage imageNamed:@"activity_close"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(activityImageView.mas_bottom).mas_offset(25);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.centerX.mas_equalTo(activityImageView);
    }];
    
    [activityImageView sd_setImageWithURL:[NSURL URLWithString:self.activityInfoModel.img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
        if (!image) {
            // 如果图片没下载下楼，直接销毁当前活动页，下次启动时在显示
            [DHWindow removeDHViewController:self];
        }
    }];

}

#pragma mark - > 跳转
- (void)activityTap{
    
    if ([self.delegate respondsToSelector:@selector(activityClick)]) {
        [self.delegate activityClick];
    }
    
    [self dealWithVersion];
}

#pragma mark - > 关闭
- (void)cancelButtonClick{
    
    [self dealWithVersion];
}

#pragma mark - > 当前设备点击过这个id的活动，就不在显示
- (void)dealWithVersion{
    
    NSMutableDictionary * dict = [[GFStaticData getObjectForKey:IsShowedActivity] mutableCopy];
    
    if (dict.allKeys) {
        [dict removeAllObjects];
    }else{
        dict = @{}.mutableCopy;
    }
    
    NSString * phoneID = [Utility getDeviceID];

    [dict setObject:self.activityInfoModel.act_id forKey:phoneID];
    [GFStaticData saveObject:dict forKey:IsShowedActivity];
    
    [DHWindow removeDHViewController:self];
}

@end
