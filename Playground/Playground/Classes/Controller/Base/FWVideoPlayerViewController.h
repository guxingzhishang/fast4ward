//
//  FWVideoPlayerViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/13.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWVideoPlayerViewController : FWBaseViewController

@property (nonatomic, assign) NSInteger      currentIndex;

/* 1:关注  2:推荐  3:标签 */
@property (nonatomic, assign) FWFeedDetailRequestType  requestType;
@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * listModel;

@property (nonatomic, strong) FWFeedModel    * feedModel;
@property (nonatomic, strong) FWFeedListModel    * feedListModel;
@property (nonatomic, strong) NSMutableArray * feedDataSource;
@property (nonatomic, assign) NSInteger        feedPageNum;

/* 以下两个参数，为推荐页传值，别的接口不用 */
@property (nonatomic, strong) NSString       *page3;
@property (nonatomic, strong) NSString       *page2_feed_id;

/* 首页标签需要传 */
@property (nonatomic, strong) NSString       * tag_id;
/* 查看人的uid */
@property (nonatomic, strong) NSString       * base_uid;

/* 按热度排序传hot，按时间排序传new  */
@property (nonatomic, strong) NSString       * order_type;

@property (nonatomic, strong) NSString       * feed_id;

@property (nonatomic, strong) NSString       * flag_return;
/* 笔记下的某个分类 */
@property (nonatomic, strong) NSString       * cat_id;

@property (nonatomic, strong) NSString       * category_id;

@property (nonatomic, strong) NSString       * car_type;


- (void)playTheIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
