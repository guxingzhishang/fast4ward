//
//  FWADViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/29.
//  Copyright © 2018 孤星之殇. All rights reserved.
//
/**
 * 广告页
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWADViewController : FWBaseViewController

@property (nonatomic, strong) UINavigationController *navi;

@end

NS_ASSUME_NONNULL_END
