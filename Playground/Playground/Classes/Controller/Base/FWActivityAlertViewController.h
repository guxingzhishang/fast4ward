//
//  FWActivityAlertViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FWActivityAlertDelegate <NSObject>

- (void)activityClick;

@end

@interface FWActivityAlertViewController : FWBaseViewController

@property (nonatomic, strong) UIView * mainView;
@property (nonatomic, strong) UIImageView * activityImageView;
@property (nonatomic, strong) UIButton * cancelButton;

@property (nonatomic, weak) id<FWActivityAlertDelegate>delegate;


@property (nonatomic, strong) FWActivityInfoModel * activityInfoModel;

@end

NS_ASSUME_NONNULL_END
