//
//  FWUpdateAlertViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUpdateAlertViewController.h"
#import "FWMineRequest.h"
#import "FWSettingModel.h"

@interface FWUpdateAlertViewController ()

@property (nonatomic, strong) NSMutableArray * dataSource;

@end

@implementation FWUpdateAlertViewController
@synthesize cancelButton;
@synthesize updateButton;
@synthesize mainView;
@synthesize containerView;
@synthesize settingModel;
@synthesize titleLabel;
@synthesize noticeView;

- (void)configData{
 
    titleLabel.text = settingModel.update_msg?settingModel.update_msg:@"更新提醒";
    if ([settingModel.update_status isEqualToString:@"2"]) {
        cancelButton.hidden = YES;
    }
  
    if (settingModel.update_notice.count >0 ) {
        if (self.dataSource.count >0) {
            [self.dataSource removeAllObjects];
        }

        [self.dataSource addObjectsFromArray:settingModel.update_notice];
        
        CGFloat height = 0;

        for (NSInteger i = 0; i< settingModel.update_notice.count; i++) {
            UILabel * contentLabel = [[UILabel alloc] init];
            contentLabel.numberOfLines = 0;
            contentLabel.text = settingModel.update_notice[i];
            contentLabel.font = DHSystemFontOfSize_14;
            contentLabel.textColor = FWTextColor_000000;
            contentLabel.textAlignment = NSTextAlignmentLeft;
            [noticeView addSubview:contentLabel];
            [contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(noticeView);
                make.height.mas_greaterThanOrEqualTo(10);
                make.top.mas_equalTo(height);
                make.width.mas_offset(280);
                make.right.mas_equalTo(noticeView);
                
                if (i == settingModel.update_notice.count-1) {
                    make.bottom.mas_equalTo(noticeView);
                }
            }];
            
            [self.view layoutIfNeeded];
            height += contentLabel.frame.size.height+10;
//            NSLog(@"----%.2f",height);
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view.backgroundColor = [UIColor clearColor];
    [self trackPageBegin:@"升级更新提示页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"升级更新提示页"];
}

#pragma mark - > 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor clearColor];
    self.dataSource = @[].mutableCopy;
    
    [self setupSubviews];
}

- (void)setupSubviews{
    
    mainView = [[UIView alloc] init];
    mainView.backgroundColor = [UIColor colorWithHexString:@"222222" alpha:0.2];
    [self.view addSubview:mainView];
    [mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    containerView = [[UIView alloc] init];
    containerView.layer.cornerRadius = 8;
    containerView.layer.masksToBounds = YES;
    containerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [mainView addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300);
        make.centerX.mas_equalTo(mainView);
        make.centerY.mas_equalTo(mainView);
        make.height.mas_greaterThanOrEqualTo(50);
    }];
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.font = DHSystemFontOfSize_16;
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [containerView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(containerView);
        make.top.mas_equalTo(containerView).mas_offset(35);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(containerView);
    }];
    
    noticeView = [[UIView alloc] init];
    [containerView addSubview:noticeView];
    [noticeView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(containerView).mas_offset(35);
        make.right.mas_equalTo(containerView).mas_offset(-35);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(20);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    updateButton = [[UIButton alloc] init];
    updateButton.layer.cornerRadius = 4;
    updateButton.layer.masksToBounds = YES;
    updateButton.backgroundColor = FWColor(@"42464a");
    updateButton.titleLabel.font = DHSystemFontOfSize_16;
    [updateButton setTitle:@"立即更新" forState:UIControlStateNormal];
    [updateButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [updateButton addTarget:self action:@selector(updateButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:updateButton];
    [updateButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(noticeView.mas_bottom).mas_offset(30);
        make.size.mas_equalTo(CGSizeMake(200, 44));
        make.centerX.mas_equalTo(containerView);
        make.bottom.mas_equalTo(containerView).mas_offset(-56);
    }];
    
    cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.titleLabel.font = DHSystemFontOfSize_16;
    [cancelButton setTitle:@"以后再说" forState:UIControlStateNormal];
    [cancelButton setTitleColor:FWColor(@"C9C9C9") forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(updateButton.mas_bottom).mas_offset(10);
        make.size.mas_equalTo(updateButton);
        make.centerX.mas_equalTo(containerView);
    }];
    
    [self configData];
}

#pragma mark - > 升级
- (void)updateButtonClick{
 
    [GFStaticData saveObject:self.settingModel.newest_version forKey:HomeUpdateVersion];

    NSURL *url = [NSURL URLWithString:AppStroeUpdateURL];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark - > 取消升级
- (void)cancelButtonClick{
    
    [GFStaticData saveObject:self.settingModel.newest_version forKey:HomeUpdateVersion];

    [DHWindow removeDHViewController:self];
}

@end
