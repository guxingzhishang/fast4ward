//
//  FWTabBarControllerConfig.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWTabBarControllerConfig.h"
#import "FWNavigationController.h"
#import "CYLPlusButtonSubclass.h"
#import "FWMatchViewController.h"
#import "FWMessageViewController.h"

@interface FWTabBarControllerConfig ()<UITabBarControllerDelegate>

@property (nonatomic, strong) FWTabBarViewModel *viewModel;

@end

@implementation FWTabBarControllerConfig
@dynamic viewModel;

- (void)viewDidLoad {

    [super viewDidLoad];
    self.tabBarController.delegate = self;
}

- (void)bindViewModel{
    [super bindViewModel];
    
}

- (void)addSubviews{
    
    [super addSubviews];
}

- (CYLTabBarController *)tabBarController {


    if (_tabBarController == nil) {

        CYLTabBarController *tabBarController = [CYLTabBarController tabBarControllerWithViewControllers:self.viewControllers tabBarItemsAttributes:self.tabBarItemsAttributesForController];
        tabBarController.delegate=self;
        tabBarController.view.backgroundColor = FWTextColor_0D0A1B;
        [self customizeTabBarAppearance:tabBarController];
        _tabBarController = tabBarController;
    }
    return _tabBarController;
}

- (NSArray *)viewControllers {

    FWHomeViewController *homeViewController = [[FWHomeViewController alloc] init];
    FWNavigationController *homeNavigationController = [[FWNavigationController alloc]
                                                        initWithRootViewController:homeViewController];
    

    FWMatchViewController *matchViewController = [[FWMatchViewController alloc] init];
    FWNavigationController *matchNavigationController = [[FWNavigationController alloc]
                                                        initWithRootViewController:matchViewController];
    
    FWMessageViewController *messageViewController = [[FWMessageViewController alloc] init];
    FWNavigationController *messageNavigationController = [[FWNavigationController alloc]
                                                        initWithRootViewController:messageViewController];
    
    
    FWNewMineViewController *mineViewController = [[FWNewMineViewController alloc] initWithViewModel:self.viewModel.mineViewModel];
    FWNavigationController *mineNavigationController = [[FWNavigationController alloc]
                                                        initWithRootViewController:mineViewController];

    NSArray *viewControllers = @[
                                 homeNavigationController,
                                 matchNavigationController,
                                 messageNavigationController,
                                 mineNavigationController,
                                 ];
    return viewControllers;
}

- (NSArray *)tabBarItemsAttributesForController
{
    NSDictionary *homeTabBarItemsAttributes = @{
                                                CYLTabBarItemTitle : @"首页",
                                                CYLTabBarItemImage : @"tabbar_home_icon",
                                                CYLTabBarItemSelectedImage :[DHImage(@"tabbar_home_click_icon") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                };

    NSDictionary *matchTabBarItemsAttributes = @{
                                                CYLTabBarItemTitle : @"赛事",
                                                CYLTabBarItemImage : @"tabbar_match_icon",
                                                CYLTabBarItemSelectedImage :[DHImage(@"tabbar_match_click_icon") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                };
    
    NSDictionary *messageTabBarItemsAttributes = @{
                                                CYLTabBarItemTitle : @"消息",
                                                CYLTabBarItemImage : @"tabbar_activity_icon",
                                                CYLTabBarItemSelectedImage :[DHImage(@"tabbar_activity_click_icon") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                };
    
    NSDictionary *mineTabBarItemsAttributes = @{
                                                CYLTabBarItemTitle : @"我的",
                                                CYLTabBarItemImage : @"tabbar_me_icon",
                                                CYLTabBarItemSelectedImage : [DHImage(@"tabbar_me_click_icon") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal],
                                                };

    NSArray *tabBarItemsAttributes = @[
                                       homeTabBarItemsAttributes,
                                       matchTabBarItemsAttributes,
                                       messageTabBarItemsAttributes,
                                       mineTabBarItemsAttributes
                                       ];
    return tabBarItemsAttributes;
}

/**
 *  更多TabBar自定义设置：比如：tabBarItem 的选中和不选中文字和背景图片属性、tabbar 背景图片属性等等
 */
- (void)customizeTabBarAppearance:(CYLTabBarController *)tabBarController {
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"9ea3ab"],NSFontAttributeName : DHSystemFontOfSize_11}
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"60646C" alpha:1],NSFontAttributeName :DHSystemFontOfSize_11}
                                             forState:UIControlStateSelected];
}


#pragma mark - > UITabBarControllerDelegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    [[self cyl_tabBarController] updateSelectionStatusIfNeededForTabBarController:tabBarController shouldSelectViewController:viewController];

   
    return YES;
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectControl:(UIControl *)control{
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{

    if (viewController == tabBarController.viewControllers[2]||
        viewController == tabBarController.viewControllers[3]) {
        // 消息、个人中心
        [self checkTabbarLogin];
    }
}

@end
