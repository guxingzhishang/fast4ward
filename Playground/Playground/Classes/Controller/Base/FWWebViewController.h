//
//  FWWebViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/17.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

typedef enum WebViewType
{
    WebViewTypeURL = 1,
    WebViewTypeLocalHtml,
    WebViewTypeHtml
}_WebViewType;


@interface FWWebViewController : FWBaseViewController<UIWebViewDelegate,UIAlertViewDelegate>
{
    UIActivityIndicatorView *progressInd;
}

@property (nonatomic, strong) UIWebView * detailView;
@property (nonatomic, strong) NSURL * requestUrl;
@property (nonatomic, assign) NSInteger pageType;
@property (nonatomic, copy)  NSString *filePath;
@property (nonatomic, copy)  NSString * webTitle;
@property (nonatomic, copy)  NSString * newsId;
@property (nonatomic, copy)  NSString * htmlStr;
@property (nonatomic, copy)  NSString * qrcode_url;
/* 1、车友排行  2、商家排行*/
@property (nonatomic, strong)  NSString * type;
@property (nonatomic, assign) BOOL needShare;//需要分享

/* mine:返回到个人中心  */
@property (nonatomic, strong) NSString * backToVC;


@end
