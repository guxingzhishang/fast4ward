//
//  FWBaseViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWNavigationController.h"
#import "FWPublishViewController.h"
#import "FWHomeRequest.h"
#import "FWMessageHomeModel.h"
#import "FWHomeAttentionViewController.h"

@interface FWBaseViewController ()
/* 判断是否有未读消息 */
@property (nonatomic, assign) BOOL   isUnread;
@end

@implementation FWBaseViewController

#pragma mark - > 请求是否有未读消息
- (void)requestUnreadMessage{
    
    CYLTabBarController * tabBarController = self.cyl_tabBarController;
    [tabBarController hideTabBadgeBackgroundSeparator];
    //添加小红点
    UIViewController *viewController = tabBarController.viewControllers[2];
    viewController.cyl_tabBadgePointViewOffset = UIOffsetMake(-3, 2);
    viewController.cyl_badgeBackgroundColor = FWColor(@"ff6f00");
    
    FWHomeRequest * request = [[FWHomeRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_new_msg_count  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWMessageHomeModel * model = [FWMessageHomeModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            if ([model.unread_like_count integerValue]>0 ||
                [model.unread_comment_count integerValue]>0 ||
                [model.unread_follow_count integerValue]>0 ||
                [model.unread_total_count integerValue]>0 ||
                [model.unread_sys_count integerValue]>0||
                [model.unread_at_count integerValue]>0) {
                
                // 私信未读数
//                || [[RCIMClient sharedRCIMClient] getTotalUnreadCount] > 0
                
                NSInteger unreand = [model.unread_total_count integerValue];
                
                self.unreandCount = unreand;
                
                if ([[RCIMClient sharedRCIMClient] getTotalUnreadCount]>0) {
                    self.unreandCount += [[RCIMClient sharedRCIMClient] getTotalUnreadCount];
                }

                
                if (self.unreandCount > 0) {
                    viewController.tabBarItem.badgeValue = @(self.unreandCount).stringValue;
                    viewController.tabBarItem.badgeColor = FWViewBackgroundColor_FF6F00;
//                    [viewController cyl_showTabBadgePoint];
                }
            }else{
                viewController.tabBarItem.badgeValue = nil;
                self.unreandCount = 0;
//                [viewController cyl_removeTabBadgePoint];
            }
            
            NSString * selectImage ;
            NSString * unselectImage ;

            if ([model.live_status isEqualToString:@"2"]) {
                /* 直播中 */
                selectImage = @"tabbar_activity_live_click";
                unselectImage = @"tabbar_activity_live";
            }else{
                selectImage = @"tabbar_match_click_icon";
                unselectImage = @"tabbar_match_icon";
            }
            [[self cyl_tabBarController].tabBar.items enumerateObjectsUsingBlock:^(UITabBarItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (idx == 1) {
                    obj.selectedImage = [[UIImage imageNamed:selectImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    obj.image = [[UIImage imageNamed:unselectImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                }
            }];
        }else{
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                return ;
            }
            [[FWHudManager sharedManager] showErrorMessage:errmsg toController:self];
        }
    }];
}

- (NSDictionary *)backDict{
    if (!_backDict) {
        _backDict = [[NSDictionary alloc] init];
    }
    
    return _backDict;
}

- (void)dealloc{
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:upLoadStatusSuccess object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:upLoadStatusFailure object:nil];

    NSLog(@"\n =========+++ %@  销毁了 +++======== \n",[self class]);
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    
    FWBaseViewController *viewController = [super allocWithZone:zone];
    @weakify(viewController)
    [[viewController rac_signalForSelector:@selector(viewDidLoad)] subscribeNext:^(id x) {
        
        @strongify(viewController)
        [viewController addSubviews];
        [viewController bindViewModel];
    }];

    return viewController;
}

- (instancetype)initWithViewModel:(FWBaseViewModel *)viewModel {
    self = [super init];
    if (self) {
        self.viewModel = viewModel;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    [self trackPageBegin:NSStringFromClass([self class])];
    
    /// 配置键盘
    IQKeyboardManager.sharedManager.enable = self.viewModel.keyboardEnable;
    IQKeyboardManager.sharedManager.shouldResignOnTouchOutside = self.viewModel.shouldResignOnTouchOutside;
//    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;

    self.viewModel.BackItemTitleColor=DHTitleColor_FFFFFF;
    
    [self setUpNavigationBarTheme_White];
    //统计埋点、友盟统计等
    [self monitorNetworking];
    
    if (@available(iOS 11.0, *)) {
        self.automaticallyAdjustsScrollViewInsets = YES;
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]) {
        [self requestUnreadMessage];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
//    [self trackPageEnd:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWTextColor_0D0A1B;

    [self monitorNetworking];
    
    /// 导航栏隐藏 只能在ViewDidLoad里面加载，无法动态
    self.fd_prefersNavigationBarHidden = self.viewModel.prefersNavigationBarHidden;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkUnAvailable:) name:NetworkUnAvailableNoti object:nil];
    });
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadSuccess:) name:upLoadStatusSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadFailure:) name:upLoadStatusFailure object:nil];

}

- (void)uploadSuccess:(NSNotification *)noti{
    
    NSDictionary * dict = (NSDictionary *)noti.userInfo;
    NSString * tipTitle = @"帖子上传成功";
    if ([dict[@"is_draft"] isEqualToString:@"1"]) {
        /* 草稿 */
        tipTitle = @"已保存到我的草稿";
    }
    
    if ([[GFStaticData getObjectForKey:@"backgroundToShowLoading"] boolValue]) {
        
        [[FWHudManager sharedManager] showErrorMessage:tipTitle toController:self];
    }else{
        [[FWHudManager sharedManager] showErrorMessage:tipTitle toController:self];
//        [GFStaticData saveObject:@"没显示过" forKey:@"missShow"];
    }
}

- (void)uploadFailure:(NSNotification *)noti{
    
    NSDictionary * dict = (NSDictionary *)noti.userInfo;
    NSString * tipTitle = @"上传遇到了问题，请重新上传";
    if ([dict[@"is_draft"] isEqualToString:@"1"]) {
        /* 草稿 */
        tipTitle = @"草稿保存失败，请稍后再试";
    }
    
    if ([[GFStaticData getObjectForKey:@"backgroundToShowLoading"] boolValue]) {
        
        [[FWHudManager sharedManager] showErrorMessage:tipTitle toController:self];
    }else{
        [[FWHudManager sharedManager] showErrorMessage:tipTitle toController:self];
//        [GFStaticData saveObject:@"没显示过" forKey:@"missShow"];
    }
}

/**
 *  设置UINavigationBarTheme的白色主题
 */
- (void)setUpNavigationBarTheme_White{
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;

    [self.navigationController.navigationBar setTranslucent:NO];
    // 设置导航栏的样式
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    // 设置导航栏的背景渲染色
    [self.navigationController.navigationBar setBarTintColor:DHTitleColor_FFFFFF];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageNamed:@""]];

    // 设置文字属性
    NSDictionary *dic = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:DHTextHexColor_Dark_222222, DHSystemFontOfSize_18, nil] forKeys:[NSArray arrayWithObjects:NSForegroundColorAttributeName, NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTitleTextAttributes:dic];
    //状态栏黑色字体
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
 
    UIButton* backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,40)];
    backButton.titleLabel.font = DHSystemFontOfSize_16;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}

/**
 *  设置UINavigationBarTheme的黑色主题
 */
- (void)setUpNavigationBarTheme_Black{
    self.view.backgroundColor = FWTextColor_0D0A1B;
    
    [self.navigationController.navigationBar setTranslucent:NO];
    // 设置导航栏的样式
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    // 设置导航栏的背景渲染色
    [self.navigationController.navigationBar setBarTintColor:FWTextColor_0D0A1B];
    
    [self.navigationController.navigationBar setShadowImage:[FWClearColor image]];

    // 设置文字属性
    NSDictionary *dic = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:FWViewBackgroundColor_FFFFFF, DHSystemFontOfSize_18, nil] forKeys:[NSArray arrayWithObjects:NSForegroundColorAttributeName, NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setTitleTextAttributes:dic];
    // 状态栏白色字体
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    UIButton* backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,40)];
    backButton.titleLabel.font = DHSystemFontOfSize_16;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}

-(void)backBtnClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark ------------
/**
 *  绑定数据模型
 */
- (void)bindViewModel{
    @weakify(self);
    /**
     *  KVO
     *  RACObserve:快速的监听某个对象的某个属性改变
     *  返回的是一个信号,对象的某个属性改变的信号
     */
    
    //设置导航栏标题
//    RAC(self.navigationItem , title) = RACObserve(self, viewModel.title);
    
    /// 动态改变
    [[[RACObserve(self.viewModel, interactivePopDisabled) distinctUntilChanged] deliverOnMainThread] subscribeNext:^(NSNumber * x) {
        @strongify(self);
        self.fd_interactivePopDisabled = x.boolValue;
    }];
}

/**
 *  添加控件
 */
- (void)addSubviews {}


/**
 *  设置navation
 
 - (void)layoutNavigation{}
 */


#pragma mark - UIViewController 跳转

- (UINavigationController *)currentSelectedNavigationController
{
    return [self currentViewController].navigationController;
}

- (UIViewController *)currentViewController{
    
    if ([DHAppWindow.rootViewController isKindOfClass:[CYLTabBarController class]]) {
        
        return  [DHAppTabbarController.selectedViewController cyl_getViewControllerInsteadOfNavigationController];
    }else{
        return DHAppWindow.rootViewController.navigationController.visibleViewController;
    }
    
}

- (void)didSelectedTabBarControllerForClass:(Class)selectedClass{
    if ([DHAppWindow.rootViewController isKindOfClass:[CYLTabBarController class]]) {
        
        [self cyl_popSelectTabBarChildViewControllerForClassType:selectedClass completion:^(__kindof UIViewController *selectedTabBarChildViewController) {
            
        }];
    }
}

- (void)pushViewControllerWtihAppModuleModel:(id)model{}

- (BOOL)isHiddenNavigationBar:(UIViewController *)vc
{
    SEL  sel = NSSelectorFromString(@"hiddenNavigationBar");
    BOOL use = NO;
    if ([vc respondsToSelector:sel]) {
        SuppressPerformSelectorLeakWarning(use = (BOOL)[vc performSelector:sel]);
    }
    return use;
}

- (void)cheackLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {

        [GFStaticData clearUserData];

        /* 登录 */
        [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        
        return;
    }
}

- (void)checkTabbarLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]||
        ![@"1" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyMobileState]]) {
        
        [GFStaticData clearUserData];
        
        /* 登录 */
        [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
        
        return;
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    ///这里设置白色
    return UIStatusBarStyleLightContent;
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

- (void)monitorNetworking
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case -1:
//                NSLog(@"未知网络");
                break;
            case 0:
//                NSLog(@"网络不可达");
                break;
            case 1:
            {
//                NSLog(@"GPRS网络");
                
                if ([[GFStaticData getObjectForKey:Allow_4G_Play] isEqualToString:@"1"]) {
                    // 如果允许4G播，默认置成wifi
                    [GFStaticData saveObject:@"WIFI" forKey:Current_Network];
                }else{
                    [GFStaticData saveObject:@"GPRS" forKey:Current_Network];
                }
            }
                break;
            case 2:
            {
//                NSLog(@"wifi网络");
                [GFStaticData saveObject:@"WIFI" forKey:Current_Network];
            }
                break;
            default:
                break;
        }
        if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
            // 有网
            [GFStaticData saveObject:@"YES" forKey:Network_Available];
        }else{
            // 没网
            [GFStaticData saveObject:@"NO" forKey:Network_Available];
        }
    }];
}

- (void)networkUnAvailable:(NSNotification *)noti{
    
}

- (void)requestVisitorWithUserID:(NSString*)f_uid WithType:(NSString*)visitor_type WithID:(NSString *)feed_id{
    
    NSString * uid = [GFStaticData getObjectForKey:kTagUserKeyID];
    if ([f_uid isEqualToString:uid]) {
        // 自己进入自己页面，不请求
        return;
    }
    
    if (!uid || !f_uid || !visitor_type || !feed_id) {
        return;
    }
    
    FWVisitorRequest * request = [[FWVisitorRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":uid,
                              @"f_uid":f_uid,
                              @"visitor_type":visitor_type,
                              @"feed_id":feed_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_visitor_log WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
//        NSLog(@"---%@",responseObject);
    }];
}

/* 统计进入页面 */
- (void)trackPageBegin:(NSString *)pageName{
    [TalkingData trackPageBegin:pageName];
}

/* 统计退出页面 */
- (void)trackPageEnd:(NSString *)pageName{
    [TalkingData trackPageEnd:pageName];
}


@end
