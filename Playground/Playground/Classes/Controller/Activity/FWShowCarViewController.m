//
//  FWShowCarViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWShowCarViewController.h"
#import "FWSelectBarView.h"
#import "FWShowCarCell.h"
#import "FWCarModel.h"
#import "FWCarDetailViewController.h"

static NSString * const newestCollectionID = @"newestCollectionID";
static NSString * const hotCollectionID = @"hotCollectionID";

static NSString * const newType = @"new";
static NSString * const hotType = @"hot";


@interface FWShowCarViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) FWSelectBarView * barView;
@property (nonatomic, strong) UIScrollView * mainScrollView;

@property (nonatomic, strong)  FWCollectionView * newestCollectionView;
@property (nonatomic, strong)  FWCollectionView * hotCollectionView;

@property (nonatomic, assign)  NSInteger newestPageNum;
@property (nonatomic, assign)  NSInteger hotPageNum;

// 以下代表是否第一次请求过，用来第一次切换标签时用
@property (nonatomic, assign) BOOL  isnewestRequest;
@property (nonatomic, assign) BOOL  ishotRequest;

@property (nonatomic, strong) FWCarModel * newestModel;
@property (nonatomic, strong) FWCarModel * hotModel;

@property (nonatomic, strong) NSMutableArray * newestDataSource;
@property (nonatomic, strong) NSMutableArray * hotDataSource;

/* 标记是哪一个列表  1:最新  2:最热*/
@property (nonatomic, strong) NSString * order_by;

@end

@implementation FWShowCarViewController
@synthesize newestDataSource;
@synthesize hotDataSource;
@synthesize barView;
@synthesize mainScrollView;
@synthesize newestCollectionView;
@synthesize hotCollectionView;
@synthesize newestPageNum;
@synthesize hotPageNum;
@synthesize isnewestRequest;
@synthesize ishotRequest;
@synthesize newestModel;
@synthesize hotModel;


#pragma mark - > 请求最新
- (void)requestnewestDataWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.newestPageNum = 1;
        
        if (self.newestDataSource.count > 0 ) {
            [self.newestDataSource removeAllObjects];
        }
    }else{
        self.newestPageNum +=1;
    }
    
    FWVisitorRequest * request = [[FWVisitorRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"order_by":self.order_by,
                              @"page":@(self.newestPageNum).stringValue,
                              @"pagesize":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_car_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [newestCollectionView.mj_footer endRefreshing];
            }else{
                [newestCollectionView.mj_header endRefreshing];
            }
            
            newestModel = [FWCarModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.newestDataSource addObjectsFromArray:newestModel.list];
            
            FWMineModel * mineModel = [[FWMineModel alloc] init];
            mineModel.feed_count_video = @"";
            mineModel.feed_count_image = @"";
            [barView setTabNmuber:mineModel];
            
            if([newestModel.list count] == 0 && self.newestPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.newestCollectionView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求图片
- (void)requesthotDataWithLoadMoredData:(BOOL)isLoadMoredData{
    
    NSInteger tempPageNum = 1;
    
    if (isLoadMoredData == NO) {
        self.hotPageNum = 1;
        
        if (self.hotDataSource.count > 0 ) {
            [self.hotDataSource removeAllObjects];
        }
    }else{
        self.hotPageNum +=1;
    }
    
    FWVisitorRequest *request = [[FWVisitorRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"order_by":self.order_by,
                              @"page":@(hotPageNum).stringValue,
                              @"page_size":@"20",
                              };
    request.isNeedShowHud = YES;
    
    [request startWithParameters:params WithAction:Get_car_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [hotCollectionView.mj_footer endRefreshing];
            }else{
                [hotCollectionView.mj_header endRefreshing];
            }
            
            hotModel = [FWCarModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.hotDataSource addObjectsFromArray:hotModel.list];
            
            FWMineModel * mineModel = [[FWMineModel alloc] init];
            [barView setTabNmuber:mineModel];
            
            if([hotModel.list count] == 0 &&
               tempPageNum != 1){
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
                return ;
            }
            
            [hotCollectionView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"爱车爆照页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"爱车爆照页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"新车爆照";
    
    self.hotPageNum = 0;
    self.newestPageNum = 0;
    
    self.ishotRequest = NO;
    self.isnewestRequest = NO;
    
    self.newestDataSource = @[].mutableCopy;
    self.hotDataSource = @[].mutableCopy;
    
    
    [self setupSelectBarView];
    [self setupSubViews];
    [self setupCollectionView];
    
    self.order_by = newType;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestnewestDataWithLoadMoredData:NO];
    });
}


#pragma mark - > 初始化视图
- (void)setupSelectBarView{
    barView = [[FWSelectBarView alloc] init];
    barView.titleArray = @[@"最新",@"最热"];
    barView.numType = 2;
    [self.view addSubview:barView];
    barView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
    
    [barView.firstButton addTarget:self action:@selector(didHeadButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [barView.secondButton addTarget:self action:@selector(didHeadButtonClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupSubViews{
    
    mainScrollView = [[UIScrollView alloc] init];
    mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(barView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(barView.frame)-49-FWSafeBottom-5);
    mainScrollView.pagingEnabled = YES;
    mainScrollView.delegate = self;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:mainScrollView];
    mainScrollView.contentSize = CGSizeMake(2*SCREEN_WIDTH, 0);
}

-(void)setupCollectionView{
    
    UICollectionViewFlowLayout *newestFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    newestFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    newestFlowLayout.sectionInset = UIEdgeInsetsMake(10,14,10,14);
    newestFlowLayout.minimumLineSpacing = 5;// cell的横向间距
    newestFlowLayout.minimumInteritemSpacing = 5; // cell的纵向间距
    
    self.newestCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.mainScrollView.frame)) collectionViewLayout:newestFlowLayout];
    self.newestCollectionView.dataSource = self;
    self.newestCollectionView.delegate = self;
    self.newestCollectionView.showsVerticalScrollIndicator = NO;
    self.newestCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.newestCollectionView registerClass:[FWShowCarCell class] forCellWithReuseIdentifier:newestCollectionID];
    self.newestCollectionView.isNeedEmptyPlaceHolder = YES;
    self.newestCollectionView.verticalOffset = -150;
    NSString * allTitle = @"还没有最新新车爆照哦~";
    NSDictionary * allAttributes = @{
                                     NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                     NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * allAttributeString = [[NSMutableAttributedString alloc] initWithString:allTitle attributes:allAttributes];
    self.newestCollectionView.emptyDescriptionString = allAttributeString;
    [self.mainScrollView addSubview:self.newestCollectionView];
    
    UICollectionViewFlowLayout *hotFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    hotFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    hotFlowLayout.sectionInset = UIEdgeInsetsMake(10,14,10,14);
    hotFlowLayout.minimumLineSpacing = 5;// cell的横向间距
    hotFlowLayout.minimumInteritemSpacing = 5; // cell的纵向间距
    
    self.hotCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH,0, SCREEN_WIDTH, CGRectGetHeight(self.mainScrollView.frame)) collectionViewLayout:hotFlowLayout];
    self.hotCollectionView.dataSource = self;
    self.hotCollectionView.delegate = self;
    self.hotCollectionView.showsVerticalScrollIndicator = NO;
    self.hotCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.hotCollectionView registerClass:[FWShowCarCell class] forCellWithReuseIdentifier:hotCollectionID];
    self.hotCollectionView.isNeedEmptyPlaceHolder = YES;
    self.hotCollectionView.verticalOffset = -150;
    NSString * newestTitle = @"还没有最热新车爆照哦~";
    NSDictionary * newestAttributes = @{
                                       NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                       NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * newestAttributeString = [[NSMutableAttributedString alloc] initWithString:newestTitle attributes:newestAttributes];
    self.hotCollectionView.emptyDescriptionString = newestAttributeString;
    [self.mainScrollView addSubview:self.hotCollectionView];
    
    DHWeakSelf;
    
    newestCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestnewestDataWithLoadMoredData:NO];
    }];
    
    newestCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestnewestDataWithLoadMoredData:YES];
    }];
    
    hotCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requesthotDataWithLoadMoredData:NO];
    }];
    
    hotCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requesthotDataWithLoadMoredData:YES];
    }];
}

#pragma mark - > collectionView delegate & datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger number;
    if (collectionView == newestCollectionView) {
        number = newestDataSource.count?newestDataSource.count:0;
    }else{
        number = hotDataSource.count?hotDataSource.count:0;
    }
    
    return number;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == newestCollectionView) {
        FWShowCarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:newestCollectionID forIndexPath:indexPath];
        cell.vc = self;
        
        if (indexPath.row < newestDataSource.count) {
            FWCarListModel * model = newestDataSource[indexPath.item];
            [cell configForCell:model];
        }
        return cell;
    }else {
        FWShowCarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:hotCollectionID forIndexPath:indexPath];
        cell.vc = self;
        
        if (indexPath.row < hotDataSource.count) {
            FWCarListModel * model = hotDataSource[indexPath.item];
            [cell configForCell:model];
        }
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FWCarDetailViewController * CDVC = [[FWCarDetailViewController alloc] init];
    if (collectionView == self.newestCollectionView) {
        if (indexPath.item >= self.newestDataSource.count) {
            return;
        }
        CDVC.user_car_id = ((FWCarListModel *)newestDataSource[indexPath.item]).user_car_id;
    }else{
        if (indexPath.item >= self.hotDataSource.count) {
            return;
        }
        CDVC.user_car_id = ((FWCarListModel *)hotDataSource[indexPath.item]).user_car_id;
    }
    [self.navigationController pushViewController:CDVC animated:YES];
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH-38)/2, ((SCREEN_WIDTH-38)/2)*127/169);
}

#pragma mark - > 切换标签
- (void)didHeadButtonClick:(UIButton *)sender{
    
    NSInteger index = 0;
    if (sender == barView.firstButton) {
        if ([self.order_by isEqualToString:newType]) {
            return;
        }
        self.order_by = newType;
        barView.firstButton.selected = YES;
        barView.secondButton.selected = NO;
        
        barView.shadowView.frame = CGRectMake(CGRectGetMinX(barView.firstButton.frame)+(CGRectGetWidth(barView.firstButton.frame)-32)/2, CGRectGetMaxY(barView.firstButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
        
        index = 0;
    }else if (sender == barView.secondButton){
        if ([self.order_by isEqualToString:hotType]) {
            return;
        }
        
        self.order_by = hotType;
        barView.firstButton.selected = NO;
        barView.secondButton.selected = YES;
        
        barView.shadowView.frame = CGRectMake(CGRectGetMinX(barView.secondButton.frame)+(CGRectGetWidth(barView.secondButton.frame)-32)/2, CGRectGetMaxY(barView.secondButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
        
        index = 1;
        
        if (!self.ishotRequest) {
            self.ishotRequest = YES;
            [self requesthotDataWithLoadMoredData:NO];
        }
    }
    
    //点击btn的时候scrollowView的contentSize发生变化
    [self.mainScrollView setContentOffset:CGPointMake(index *SCREEN_WIDTH, 0) animated:YES];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (self.mainScrollView == scrollView){
        
        float scrollViewX = scrollView.contentOffset.x;
        if (scrollViewX == 0) {
            [self didHeadButtonClick:barView.firstButton];
        }else if (scrollViewX == SCREEN_WIDTH){
            [self didHeadButtonClick:barView.secondButton];
        }
    }
}


@end
