//
//  FWBussinessRankViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 商家排行榜
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWBussinessRankViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
