//
//  FWActivityViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/10.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWActivityViewController : FWBaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FWBaseCollectionLayoutDalegate,UIGestureRecognizerDelegate>

/* 承载collectionView的背景Scrollview */
@property (nonatomic, strong) UIScrollView * collectionScr;

/* 全部列表 */
@property (nonatomic, strong) FWCollectionView * allCollectionView;
@property (nonatomic, strong) FWBaseCollectionLayout *allLayout;

@end

NS_ASSUME_NONNULL_END
