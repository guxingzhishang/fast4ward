//
//  FWActivityViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/12/10.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWActivityViewController.h"
#import "FWCustomNavigationView.h"
#import "FWActivityCell.h"
#import "FWSearchInfoViewController.h"
#import "FWActivityHeaderView.h"
#import "FWActivityRequest.h"
#import "FWActivityModel.h"
#import "FWBussinessRankViewController.h"

static NSString * const allCellId = @"activityAllCellId";

@interface FWActivityViewController ()
{
    FWMainTouchScrollView *rootScrollView; //rootScrollView要有滑动穿透
    FWScrollViewFollowCollectionView *thFollow; //必须写成属性
    NSInteger  ScrollHeight;//需三处保持一致
    
    UIView * naviBarView;
    UILabel * titleLable;
    
    CGFloat alphaValue;
}

@property (nonatomic, strong) UITableView * infoTableView;

@property (nonatomic, strong) FWActivityHeaderView * headerView;

@property (nonatomic, assign) NSInteger  pageNum;

@property (nonatomic, strong) FWActivityModel * activityModel;

@property (nonatomic, strong) NSMutableArray * dataSource;

@property (nonatomic, assign) NSInteger  currentIndex;

@end

@implementation FWActivityViewController

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

- (void)requestActivityWithLoadMore:(BOOL)isLoadMoredData {
    
    if (isLoadMoredData) {
        self.pageNum +=1;
    }else{
        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
        self.pageNum = 1;
    }
    
    FWActivityRequest * request = [[FWActivityRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"6",
                              };
    
    [request startWithParameters:params WithAction:Get_feeds_by_activity_v2 WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.activityModel = [FWActivityModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            if (isLoadMoredData) {
                [self.allCollectionView.mj_footer endRefreshing];
            }else{
                self.allCollectionView.mj_footer.hidden = NO;
                [rootScrollView.mj_header endRefreshing];
            }
            
            [self.dataSource addObjectsFromArray: self.activityModel.feed_list];
            
            if([self.activityModel.feed_list count] == 0 &&
               self.pageNum != 1){
                self.allCollectionView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.allCollectionView reloadData];
            }
            
            [self.headerView configViewForModel:self.activityModel];
            [self refreshFrame];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"社区页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"社区页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    self.fd_interactivePopDisabled = YES;
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.pageNum = 0;
    self.currentIndex = 0;
    
    alphaValue = 0;
    ScrollHeight = 360;

    [self setupSubViews];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PushToPublishViewController) name:kTagPushPublish object:nil];
    
    [self requestActivityWithLoadMore:NO];
}

#pragma mark - > 初始化视图
- (void)setupSubViews{
    
    naviBarView = [[UIView alloc] init];
    naviBarView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    naviBarView.userInteractionEnabled = YES;
    [self.view addSubview:naviBarView];
    naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    naviBarView.alpha = 0;
    
    titleLable = [[UILabel alloc] init];
    titleLable.text = @"社区";
    titleLable.font = DHSystemFontOfSize_18;
    titleLable.textColor = FWTextColor_000000;
    titleLable.textAlignment = NSTextAlignmentCenter;
    [naviBarView addSubview:titleLable];
    [titleLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(naviBarView);
        make.top.mas_equalTo(naviBarView).mas_offset(20+FWCustomeSafeTop);
        make.height.mas_equalTo(35);
    }];

    
    rootScrollView = [[FWMainTouchScrollView alloc] init];
    rootScrollView.frame  = CGRectMake(0, 30+FWCustomeSafeTop,SCREEN_WIDTH, SCREEN_HEIGHT-30-FWCustomeSafeTop-49-FWSafeBottom);
    rootScrollView.delegate = self; //要遵循代理
    rootScrollView.showsVerticalScrollIndicator = NO;
    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);  //滚动大小
    [self.view addSubview:rootScrollView];
    
    self.headerView = [[FWActivityHeaderView alloc] init];
    self.headerView.vc = self;
    [rootScrollView addSubview:self.headerView];
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 460);
    
    self.collectionScr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headerView.frame)+1, CGRectGetWidth(rootScrollView.frame), SCREEN_HEIGHT-25-FWCustomeSafeTop-49-FWSafeBottom-13)];
    self.collectionScr.delegate = self;
    self.collectionScr.pagingEnabled = YES;
    self.collectionScr.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [rootScrollView addSubview:self.collectionScr];
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH, 0);
    
    self.allLayout = [[FWBaseCollectionLayout alloc] init];
    self.allLayout.columns = 2;
    self.allLayout.rowMargin = 12;
    self.allLayout.colMargin = 12;
    self.allLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.allLayout.delegate = self;
    [self.allLayout autuContentSize];
    
    self.allCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.allLayout];
    self.allCollectionView.dataSource = self;
    self.allCollectionView.delegate = self;
    self.allCollectionView.showsVerticalScrollIndicator = NO;
    self.allCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.allCollectionView registerClass:[FWActivityCell class] forCellWithReuseIdentifier:allCellId];
    self.allCollectionView.isNeedEmptyPlaceHolder = YES;
    self.allCollectionView.verticalOffset = -150;
    NSString * allTitle = @"还没有发布的视频哦~";
    NSDictionary * allAttributes = @{
                                     NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                     NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * allAttributeString = [[NSMutableAttributedString alloc] initWithString:allTitle attributes:allAttributes];
    self.allCollectionView.emptyDescriptionString = allAttributeString;
    [self.collectionScr addSubview:self.allCollectionView];
    
    
    //利用initRoot创建 给予底部上下滑动的ScrollView 要滑动的ScrollRange 大小和左右滑动的Table数组
    thFollow = [[FWScrollViewFollowCollectionView alloc]init];
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[self.allCollectionView]];
    
    {
        DHWeakSelf;
        rootScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf requestActivityWithLoadMore:NO];
        }];
        
        @weakify(self);
        self.allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestActivityWithLoadMore:YES];
        }];
    }
    
    [self.view bringSubviewToFront:naviBarView];
}

#pragma mark - > 刷新视图坐标
- (void)refreshFrame{
    
    ScrollHeight = self.headerView.currentHeight;
    
    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);
    
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, ScrollHeight);
    
    self.collectionScr.frame = CGRectMake(0, CGRectGetMaxY(self.headerView.frame)+1, CGRectGetWidth(rootScrollView.frame), SCREEN_HEIGHT-64-FWCustomeSafeTop);
    
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[self.allCollectionView]];
}

#pragma mark - > collectionView`s  Delegate & DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.dataSource.count?self.dataSource.count:0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    FWActivityCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:allCellId forIndexPath:indexPath];
    cell.viewController = self;
    
    if (self.dataSource.count > 0) {
        
        FWFeedListModel * model = self.dataSource[indexPath.item];
        [cell configForCell:model];
        
        if (model.h5_url.length > 0 ) {
            
            cell.activityImageView.hidden = NO;
            
            NSString * imageName = @"activity";
            CGSize imageViewSize = CGSizeMake(30, 15);
            
            cell.activityImageView.image = [UIImage imageNamed:imageName];
            [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                make.size.mas_equalTo(imageViewSize);
            }];
        }else{
            cell.activityImageView.hidden = YES;
        }
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray * tempArr = self.dataSource;
    
    FWFeedListModel * model = tempArr[indexPath.row];
    
    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }
    
    if ([model.feed_type isEqualToString:@"1"]) {
        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {};
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
            controller.currentIndex = 0;
            controller.feed_id = model.feed_id;
            controller.requestType = FWActivityTagsRequestType;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        // 图文详情
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.requestType = FWActivityTagsRequestType;
        TVC.listModel = model;
        TVC.feed_id = model.feed_id;
        TVC.myBlock = ^(FWFeedListModel *awemeModel) {};
        [self.navigationController pushViewController:TVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {
        
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"4"]) {
        // 问答
        FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
//        DVC.myBlock = ^(FWFeedListModel *listModel) {
//            self.dataSource[index] = listModel;
//        };
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        // 改装
        FWRefitCaseDetailViewController * RCDVC = [[FWRefitCaseDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
    //        DVC.myBlock = ^(FWFeedListModel *listModel) {
    //            self.dataSource[index] = listModel;
    //        };
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"6"]) {
        /* 闲置 */
        FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }
}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = self.dataSource;
    
    if (indexPath.item >= tempArr.count) {
        return 0;
    }
    
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"2"]) {
        // 图文贴 固定比例3：4
        height = (cover_width *4)/3;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if([model.feed_type isEqualToString:@"3"]){
        // 文章帖 固定比例5：4
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if ([model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 20;
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 25;
        }
    }else  if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [thFollow followScrollViewScrollScrollViewScroll:scrollView];
    
    if (scrollView == rootScrollView) {
        
        if (scrollView.contentOffset.y > 30){
            alphaValue = 1;
        }else{
            alphaValue = (scrollView.contentOffset.y/30);
        }
        
        titleLable.alpha = alphaValue;
        naviBarView.alpha = alphaValue;
        
        if (scrollView.contentOffset.y >0) {
            titleLable.hidden = NO;
        }else{
            titleLable.hidden = YES;
        }
    }
}
#pragma mark - > 搜索页
- (void)searchButtonClick{
    
    [TalkingData trackEvent:@"搜索" label:@"发现" parameters:nil];

    FWSearchInfoViewController * SIVC = [[FWSearchInfoViewController alloc] init];
    [self.navigationController pushViewController:SIVC animated:YES];
}

#pragma mark - > 跳转到发布页
- (void)PushToPublishViewController{
    
    if (![((UINavigationController *)self.tabBarController.selectedViewController).topViewController isEqual:self]
        ) {
        return;
    }
    
    [self cheackLogin];

    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self];
        }
    }else {
        if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
            ![@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
            
            FWPublishViewController * vc = [[FWPublishViewController alloc] init];
            vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
            vc.fd_prefersNavigationBarHidden = YES;
            vc.navigationController.navigationBar.hidden = YES;
            
            CATransition *animation = [CATransition animation];
            [animation setDuration:0.4];
            [animation setType: kCATransitionMoveIn];
            [animation setSubtype: kCATransitionFromTop];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            [self.navigationController pushViewController:vc animated:NO];
            [self.navigationController.view.layer addAnimation:animation forKey:nil];
        }
    }
}

@end
