//
//  FWPersonRankViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPersonRankViewController.h"

@interface FWPersonRankViewController ()<UITableViewDelegate,UITableViewDataSource,FWRankHeaderViewDelegate>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) NSArray * rankImage;
@property (nonatomic, strong) NSArray * rankNumImage;
@property (nonatomic, strong) FWRankHeaderView * headerView;
@property (nonatomic, strong) UIImage * sendImage;
@property (nonatomic, strong) FWTopRankModel * rankModel;

@end

@implementation FWPersonRankViewController
@synthesize infoTableView;
@synthesize sendImage;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

#pragma mark - > 请求车友排行榜
- (void)requestTopList{
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"type":@"car",
                              };
    
    [request startWithParameters:params WithAction:Get_top_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.rankModel = [FWTopRankModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.dataSource addObjectsFromArray:self.rankModel.list_user];
            self.headerView.rankModel = self.rankModel;

            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车友排行榜页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车友排行榜页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.rankImage = @[@"rank_first",@"rank_second",@"rank_third"];
    self.rankNumImage = @[@"rank_new_first",@"rank_new_second",@"rank_new_third"];

    [self setupSubViews];
    [self requestTopList];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 76;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.headerView = [[FWRankHeaderView alloc] init];
    self.headerView.clipsToBounds = YES;
    self.headerView.vc = self;
    self.headerView.delegate = self;
    self.headerView.index = 2;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 179 *SCREEN_WIDTH/375);
    infoTableView.tableHeaderView = self.headerView;
}

#pragma mark - > UITableViewDelegate && DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"bussinessRankID";
    
    FWBaseRankCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWBaseRankCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    CGFloat rankImageHeight = 17;
    
    if (indexPath.row <= 2) {
        cell.rankImageView.hidden = NO;
        cell.rankNumImageView.hidden = NO;
        
        cell.rankImageView.image = [UIImage imageNamed:self.rankImage[indexPath.row]];
        cell.rankNumImageView.image = [UIImage imageNamed:self.rankNumImage[indexPath.row]];
        cell.numberLabel.text = @"";
        
        rankImageHeight = 24;
    }else{
        cell.rankImageView.hidden = YES;
        cell.rankNumImageView.hidden = YES;

        cell.numberLabel.text = @(indexPath.row+1).stringValue;

        rankImageHeight = 17;
    }
    [cell.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cell.numberLabel.mas_right).mas_offset(0);
        make.top.mas_equalTo(cell.contentView).mas_offset(rankImageHeight);
        make.size.mas_equalTo(CGSizeMake(41, 41));
    }];
    
    if (self.dataSource.count > indexPath.row) {
        cell.type = @"2";
        [cell configCellForModel:self.dataSource[indexPath.row]];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.dataSource.count > indexPath.row) {
        FWMineInfoModel * userInfo = self.dataSource[indexPath.row];
        
        FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
        UIVC.user_id = userInfo.uid;
        [self.navigationController pushViewController:UIVC animated:YES];
    }
}

#pragma mark - > 分享
- (void)shareClick{
    
    [self shortCutImage];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        FWShareRankViewController * vc = [[FWShareRankViewController alloc] init];
        vc.cutshortImage = sendImage;
        vc.qrcode_url = self.rankModel.qrcode_url;
        vc.shareType = @"2";
        vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:vc animated:YES completion:nil];
    });
}

#pragma mark - > 截屏
- (void)shortCutImage{
    
    CGFloat px = 2;
    
    if(DH_isiPhone4){
        px = 1;
    }else if(DH_isiPhone5 || DH_isiPhone6 ||DH_isiPhoneXR){
        px = 2;
    }else if(DH_isiPhone6Plus ||DH_isiPhoneX||DH_isiPhoneXMax){
        px = 3;
    }
    
    CGSize size = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGImageRef imageRef = viewImage.CGImage;
    CGRect rect = CGRectMake(0, 179 *SCREEN_WIDTH/375 *px+2*px,size.width*px,76*px*3);
    
    //这里可以设置想要截图的区域
    CGImageRef imageRefRect =CGImageCreateWithImageInRect(imageRef, rect);
    sendImage = [[UIImage alloc] initWithCGImage:imageRefRect];
}
@end
