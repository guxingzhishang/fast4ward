//
//  FWConversationSettingViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWConversationSettingViewController.h"

@implementation FWConversationSettingViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.isDelete = NO;
    
    UIButton* backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    backButton.titleLabel.font = DHSystemFontOfSize_16;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}

#pragma mark - > 返回
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setSwitchState {
    
    //设置新消息通知状态
    [[RCIMClient sharedRCIMClient] getConversationNotificationStatus:self.conversationType targetId:self.targetId success:^(RCConversationNotificationStatus nStatus) {
        BOOL enableNotification = NO;
        if (nStatus == NOTIFY) {
            enableNotification = YES;
        }
        self.switch_newMessageNotify = enableNotification;
    } error:^(RCErrorCode status) {
//        NSLog(@"status==%d",status);
    }];
    
    //设置置顶聊天状态
    RCConversation *conversation = [[RCIMClient sharedRCIMClient] getConversation:self.conversationType targetId:self.targetId];
    self.switch_isTop = conversation.isTop;
}

/* 置顶聊天 */
- (void)onClickIsTopSwitch:(id)sender {
    UISwitch *swch = sender;
    [[RCIMClient sharedRCIMClient] setConversationToTop:ConversationType_PRIVATE targetId:self.targetId isTop:swch.on];
}

/**
 *  override
 *
 *  @param sender sender description
 */
- (void)onClickNewMessageNotificationSwitch:(id)sender {
    UISwitch *swch = sender;
    __weak FWConversationSettingViewController *weakSelf = self;
    [[RCIMClient sharedRCIMClient] setConversationNotificationStatus:self.conversationType
                                                            targetId:self.targetId
                                                           isBlocked:!swch.on
                                                             success:^(RCConversationNotificationStatus nStatus) {
                                                                 BOOL enableNotification = NO;
                                                                 if (nStatus == NOTIFY) {
                                                                     enableNotification = YES;
                                                                 }
                                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                                     weakSelf.switch_newMessageNotify =
                                                                     enableNotification;
                                                                 });
                                                             }
                                                               error:^(RCErrorCode status){
                                                                   
                                                               }];
}


- (void)clearHistoryMessage {
//    BOOL __block isClear = false;
//    __weak typeof(self) weakSelf = self;
//    NSArray *latestMessages = [[RCIMClient sharedRCIMClient] getLatestMessages:self.conversationType targetId:self.targetId count:1];
//    if (latestMessages.count > 0) {
//        RCMessage *message = (RCMessage *)[latestMessages firstObject];
//        [[RCIMClient sharedRCIMClient] clearRemoteHistoryMessages:self.conversationType
//                                                        targetId:weakSelf.targetId
//                                                      recordTime:message.sentTime
//                                                         success:^{
//                                                             isClear = [[RCIMClient sharedRCIMClient] clearMessages:weakSelf.conversationType targetId:weakSelf.targetId];
//                                                         }
//                                                           error:^(RCErrorCode status) {
//                                                               NSLog(@"status===%d",status);
//                                                           }];
//    }
    
    __weak typeof(self) weakSelf = self;

    NSArray *latestMessages = [[RCIMClient sharedRCIMClient] getLatestMessages:ConversationType_PRIVATE targetId:self.targetId count:1];
    if (latestMessages.count > 0) {
        RCMessage *message = (RCMessage *)[latestMessages firstObject];
        [[RCIMClient sharedRCIMClient]clearRemoteHistoryMessages:self.conversationType
                                                        targetId:self.targetId
                                                      recordTime:message.sentTime
                                                         success:^{
                                                             [[RCIMClient sharedRCIMClient] deleteMessages:ConversationType_PRIVATE
                                                                                                  targetId:self.targetId
                                                                                                   success:^{
                                                                                                       [weakSelf performSelectorOnMainThread:@selector(clearCacheAlertMessage:)
                                                                                                                                  withObject:@"清除聊天记录成功！"
                                                                                                                               waitUntilDone:YES];
                                                                                                       [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearHistoryMsg" object:nil];
                                                                                                       dispatch_async(dispatch_get_main_queue(), ^{
//                                                                                                           [loadingView removeFromSuperview];
                                                                                                       });
                                                                                                   }
                                                                                                     error:^(RCErrorCode status) {

                                                                                                     }];
                                                         }
                                                           error:^(RCErrorCode status) {
                                                               [weakSelf performSelectorOnMainThread:@selector(clearCacheAlertMessage:)
                                                                                          withObject:@"清除聊天记录失败！"
                                                                                       waitUntilDone:YES];
                                                               dispatch_async(dispatch_get_main_queue(), ^{
//                                                                   [loadingView removeFromSuperview];
                                                               });
                                                           }];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:@"ClearHistoryMsg" object:nil];
}

- (void)clearCacheAlertMessage:(NSString *)msg {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                              
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

@end
