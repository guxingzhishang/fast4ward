//
//  FWMessageViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMessageViewController.h"
#import "FWMessageHomeRequest.h"
#import "FWMessageHomeModel.h"
#import "FWFunsViewController.h"
#import "FWOfficailMessageViewController.h"
#import "FWAskAndAnswerMessageViewController.h"
#import "FWCustomNavigationView.h"
#import "FWConversationListViewController.h"

@interface FWMessageViewController ()

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) FWMessageHomeModel * officialModel;
@property (nonatomic, strong) FWCustomNavigationView * navigationView;

@property (nonatomic, strong) NSArray * titleArray;
@property (nonatomic, strong) NSArray * imageArray;

@property (nonatomic, strong) FWMessageHomeModel * messageModel;

@end

@implementation FWMessageViewController

#pragma mark - > 请求融云iM
- (void)requestRongYunTokenIM{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_im_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:userId];
                                            [RCIM sharedRCIM].currentUserInfo = userInfo;
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"IM登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            NSLog(@"IMtoken错误");
                                        }];
        }
    }];
}


- (void)requestSegementUnreadMessage{
    
    FWMessageHomeRequest * request = [[FWMessageHomeRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_new_msg_count  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        [_infoTableView.mj_header endRefreshing];
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.messageModel = [FWMessageHomeModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [_infoTableView reloadData];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    [self trackPageBegin:@"消息页"];
    [self requestSegementUnreadMessage];
//    [self requestUnreadMessage];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"消息页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requestRongYunTokenIM];
    
    self.fd_prefersNavigationBarHidden = YES;
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.title = @"消息";
    
    self.titleArray = @[@"粉丝",@"点赞",@"评论",@"@我",@"回答"];
    self.imageArray = @[@"message_funs",@"message_thumup",@"message_comment",@"message_@_me",@"message_answer"];
//    @"message_official"   ,@"message_private"
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PushToPublishViewController) name:kTagPushPublish object:nil];

    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.navigationView = [[FWCustomNavigationView alloc] init];
    self.navigationView.navigationImageView.hidden = YES;
    self.navigationView.navigationLabel.hidden = NO;
    self.navigationView.navigationLabel.text = @"消息";
    self.navigationView.navigationLabel.textColor = FWTextColor_222222;
    self.navigationView.navigationLabel.font = DHBoldFont(22);
    self.navigationView.navigationLabel.textAlignment = NSTextAlignmentLeft;
    self.navigationView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    [self.view addSubview:self.navigationView];
    [self.navigationView.navigationLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 35));
        make.bottom.mas_equalTo(self.navigationView).mas_offset(-1);
        make.left.mas_equalTo(self.navigationView).mas_offset(14);
    }];

    self.infoTableView = [[UITableView alloc] init];
    self.infoTableView.delegate = self;
    self.infoTableView.dataSource = self;
//    self.infoTableView.scrollEnabled = NO;
    self.infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.infoTableView.rowHeight = 58;
    [self.view addSubview:self.infoTableView];
    [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.navigationView.mas_bottom);
    }];
    
    
    DHWeakSelf;
    self.infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestSegementUnreadMessage];
    }];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"messageID";
    
    FWMessageCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.titleLabel.text = self.titleArray[indexPath.row];
    cell.iconView.image = [UIImage imageNamed:self.imageArray[indexPath.row]];


    [cell.iconView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell.contentView);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.left.mas_equalTo(cell.contentView).mas_offset(16);
    }];
    
    if (indexPath.row == 0) {
        if ([self.messageModel.unread_follow_count integerValue] > 0){
            cell.badgeLabel.hidden = NO;
            cell.badgeLabel.text = self.messageModel.unread_follow_count;
        }else{
            cell.badgeLabel.hidden = YES;
        }
    }
    
    if (indexPath.row == 1) {
        if ([self.messageModel.unread_like_count integerValue] > 0){
            cell.badgeLabel.hidden = NO;
            cell.badgeLabel.text = self.messageModel.unread_like_count;
        }else{
            cell.badgeLabel.hidden = YES;
        }
    }
    
    if (indexPath.row == 2) {
        if ([self.messageModel.unread_comment_count integerValue] > 0){
            cell.badgeLabel.hidden = NO;
            cell.badgeLabel.text = self.messageModel.unread_comment_count;
        }else{
            cell.badgeLabel.hidden = YES;
        }
    }
    
    if (indexPath.row == 3) {
        // @我
        if ([self.messageModel.unread_at_count integerValue] > 0){
            cell.badgeLabel.hidden = NO;
            cell.badgeLabel.text = self.messageModel.unread_at_count;
        }else{
            cell.badgeLabel.hidden = YES;
        }
    }
    
    if (indexPath.row == 4) {
        if ([self.messageModel.unread_question_count integerValue] > 0){
            cell.badgeLabel.hidden = NO;
            cell.badgeLabel.text = self.messageModel.unread_question_count;
        }else{
            cell.badgeLabel.hidden = YES;
        }
    }
//    if (indexPath.row == 4) {
//        if ([[RCIMClient sharedRCIMClient] getTotalUnreadCount] > 0){
//            cell.badgeLabel.hidden = NO;
//            cell.badgeLabel.text = @([[RCIMClient sharedRCIMClient] getTotalUnreadCount]).stringValue;
//        }else{
//            cell.badgeLabel.hidden = YES;
//        }
//    }
//
//    if (indexPath.row == 5) {
//        if ([self.messageModel.unread_sys_count integerValue] > 0){
//            cell.badgeLabel.hidden = NO;
//            cell.badgeLabel.text = self.messageModel.unread_sys_count;
//        }else{
//            cell.badgeLabel.hidden = YES;
//        }
//    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        /* 粉丝 */
        FWFunsViewController * FVC = [[FWFunsViewController alloc] init];
        FVC.funsType = @"1";
        FVC.base_uid = [GFStaticData getObjectForKey:kTagUserKeyID];
        [self.navigationController pushViewController:FVC animated:YES];
    }else if (indexPath.row == 1){
        /* 点赞 */
        FWThumbUpViewController * TUVC = [[FWThumbUpViewController alloc] init];
        TUVC.listType = FWMessageThumbUpList;
        [self.navigationController pushViewController:TUVC animated:YES];
    }else if (indexPath.row == 2){
        /* 评论 */
        FWThumbUpViewController * TUVC = [[FWThumbUpViewController alloc] init];
        TUVC.listType = FWMessageCommentList;
        [self.navigationController pushViewController:TUVC animated:YES];
    }else if (indexPath.row == 3){
        /* @我 */
        FWAskAndAnswerMessageViewController * AAAVC = [[FWAskAndAnswerMessageViewController alloc] init];
        AAAVC.msg_type = @"5";
        AAAVC.titleString = @"@我";
        [self.navigationController pushViewController:AAAVC animated:YES];
    }else if (indexPath.row == 4){
        /* 问答 */
        FWAskAndAnswerMessageViewController * AAAVC = [[FWAskAndAnswerMessageViewController alloc] init];
        AAAVC.msg_type = @"4";
        AAAVC.titleString = @"问答消息";
        [self.navigationController pushViewController:AAAVC animated:YES];
    }
//    else if (indexPath.row == 4){
//        /* 私信 */
//        FWConversationListViewController * OMVC = [[FWConversationListViewController alloc] init];
//        [self.navigationController pushViewController:OMVC animated:YES];
//    }
//    else if (indexPath.row == 5){
//        /* 系统消息 */
//        FWOfficailMessageViewController * OMVC = [[FWOfficailMessageViewController alloc] init];
//        OMVC.messageType = 3;
//        [self.navigationController pushViewController:OMVC animated:YES];
//    }
}


#pragma mark - > 跳转到发布页
- (void)PushToPublishViewController{
    
    if (![((UINavigationController *)self.tabBarController.selectedViewController).topViewController isEqual:self]
        ) {
        return;
    }
    

    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self];
        }
    }else {
        [self cheackLogin];
        
        if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
            ![@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
            
            FWPublishViewController * vc = [[FWPublishViewController alloc] init];
            vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
            vc.fd_prefersNavigationBarHidden = YES;
            vc.navigationController.navigationBar.hidden = YES;
            
            CATransition *animation = [CATransition animation];
            [animation setDuration:0.4];
            [animation setType: kCATransitionMoveIn];
            [animation setSubtype: kCATransitionFromTop];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
            [self.navigationController pushViewController:vc animated:NO];
            [self.navigationController.view.layer addAnimation:animation forKey:nil];
        }
    }
}

#pragma mark - > 检测是否登录
- (void)cheackLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        /* 登录 */
        [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        
        return;
    }
}
@end
