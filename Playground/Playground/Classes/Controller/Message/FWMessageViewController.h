//
//  FWMessageViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/26.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 首页 - > 消息
 */

#import "FWBaseViewController.h"
#import "FWMessageHeaderView.h"
#import "FWMessageCell.h"
#import "FWTableView.h"

@interface FWMessageViewController : FWBaseViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@end
