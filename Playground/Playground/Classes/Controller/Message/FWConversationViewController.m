//
//  FWConversationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/2.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWConversationViewController.h"
#import "FWConversationSettingViewController.h"

@interface FWConversationViewController()<RCLocationPickerViewControllerDelegate,RCIMUserInfoDataSource>
//- (void)onCancelMultiSelectEvent:(UIBarButtonItem *)item;
//- (void)deleteMessages;

@end

@implementation FWConversationViewController
@synthesize picker;

#pragma mark - > 请求个人的信息并更新
- (void)requestWithSelfId:(NSString *)userId  {
    
    if (!userId) {
        return;
    }
    // 每次展示用户头像等信息都会调用,正常开发应该从app服务器获取,保存在本地
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{@"f_uid":userId};
    
    [request startWithParameters:params WithAction:Get_user_profile WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            NSDictionary * infoDict = [[back objectForKey:@"data"] objectForKey:@"user_info"];
            
            RCUserInfo * userinfo = [[RCUserInfo alloc] init];
            userinfo.portraitUri = infoDict[@"header_url"];
            userinfo.name = infoDict[@"nickname"];
            userinfo.userId = userId;
            
            [RCIM sharedRCIM].currentUserInfo = userinfo;
            [[RCIM sharedRCIM] refreshUserInfoCache:userinfo withUserId:userId];
            [self.conversationMessageCollectionView reloadData];
        }
    }];
}

#pragma mark - > 请求好友的信息并更新
- (void)requestWithId:(NSString *)userId {

    if (!userId) {
        return;
    }
    
    // 每次展示用户头像等信息都会调用,正常开发应该从app服务器获取,保存在本地
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{@"f_uid":userId};

    [request startWithParameters:params WithAction:Get_user_profile WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {

            NSDictionary * infoDict = [[back objectForKey:@"data"] objectForKey:@"user_info"];
            self.title = infoDict[@"nickname"];

            RCUserInfo * userinfo = [[RCUserInfo alloc] init];
            userinfo.portraitUri = infoDict[@"header_url"];
            userinfo.name = infoDict[@"nickname"];
            userinfo.userId = userId;

            [[RCIM sharedRCIM] refreshUserInfoCache:userinfo withUserId:userId];
            [self setupNavigationBar];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if (@available(iOS 11.0, *)) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentAutomatic];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupNavigationBar];
}

- (void)setupNavigationBar{
    
    UIButton * backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    backButton.titleLabel.font = DHSystemFontOfSize_16;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    UIButton * settingButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    settingButton.titleLabel.font = DHSystemFontOfSize_16;
    settingButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    [settingButton setImage:[UIImage imageNamed:@"mine_settingnew"] forState:UIControlStateNormal];
    [settingButton addTarget:self action:@selector(settingButtonClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:settingButton];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

-(void) viewDidLoad{
    
    [super viewDidLoad];
    
    [self setupNavigationBar];
    self.allowsMessageCellSelection = NO;

    // 设置发送的消息是否携带用户信息
    [RCIM sharedRCIM].enableMessageAttachUserInfo = YES;

    // 设置代理,代理方法要返回用户信息
    [[RCIM sharedRCIM] setUserInfoDataSource:self];

//    [RCIM sharedRCIM].globalMessageAvatarStyle = RC_USER_AVATAR_CYCLE;
    
    /* SDK中全局的导航按钮字体颜色 */
    [RCIM sharedRCIM].globalNavigationBarTintColor = FWTextColor_12101D;
    
    //清除历史消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(clearHistoryMSG:)
                                                 name:@"ClearHistoryMsg"
                                               object:nil];
    [self requestWithId:self.targetId];
    [self requestWithSelfId:[RCIM sharedRCIM].currentUserInfo.userId];
}


#pragma mark - > 返回
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 点击头像
- (void)didTapCellPortrait:(NSString *)userId{

    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = userId;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 设置
- (void)settingButtonClick{
    
    FWConversationSettingViewController * CSVC = [[FWConversationSettingViewController alloc] init];
    CSVC.targetId = self.targetId;
    CSVC.conversationType = ConversationType_PRIVATE;
    [self.navigationController pushViewController:CSVC animated:YES];
}

///* 非会员禁止发消息（用来屏蔽那些恶意注册用户发广告） */
//- (void)sendMessage:(RCMessageContent *)messageContent pushContent:(NSString *)pushContent{
//    
//    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
//    
//    if ([usvm.userModel.user_level isEqualToString:@"1"] ||
//        [usvm.userModel.user_level isEqualToString:@"2"]||
//        [usvm.userModel.user_level isEqualToString:@"3"]) {
//        [super sendMessage:messageContent pushContent:pushContent];
//    }else{
//        [self.view endEditing:YES];
//
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"非会员禁止发消息" preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            }];
//            [alertController addAction:okAction];
//            [self presentViewController:alertController animated:YES completion:nil];
//        });
//    }
//}

#pragma mark - > 截取pluginBoardView
- (void)pluginBoardView:(RCPluginBoardView *)pluginBoardView clickedItemWithTag:(NSInteger)tag{
    
    if (tag == 1002) {
        [self selectLibrary];
    }else{
        [super pluginBoardView:pluginBoardView clickedItemWithTag:tag];
    }
}

#pragma mark - > 发送位置
- (void)locationPicker:(RCLocationPickerViewController *)locationPicker didSelectLocation:(CLLocationCoordinate2D)location locationName:(NSString *)locationName mapScreenShot:(UIImage *)mapScreenShot{
    
    RCLocationMessage *locationMessage = [RCLocationMessage messageWithLocationImage:mapScreenShot location:location locationName:locationName];
    locationMessage.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;

    [[RCIM sharedRCIM] sendMessage:self.conversationType
                          targetId:self.targetId
                           content:locationMessage
                       pushContent:@"[位置]"
                          pushData:nil
                           success:^(long messageId) {
                              NSLog(@"位置发送成功了---%ld",messageId);
                           } error:^(RCErrorCode nErrorCode, long messageId) {
                               NSLog(@"位置发送失败了---%ld，----%ld",nErrorCode,messageId);
                           }];
}

#pragma mark - > 选择相册
- (void)selectLibrary{
    
    if (@available(iOS 11, *)) {
        UIScrollView.appearance.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
    }
    
    NSUInteger sourceType = 0;
    
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    picker = [[UIImagePickerController alloc] init];
    picker.delegate                 = self;
    picker.sourceType               = sourceType;
    picker.allowsEditing            = NO;
    picker.navigationController.navigationBar.hidden = NO;
    picker.fd_prefersNavigationBarHidden = NO;
    picker.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentViewController:picker animated:YES completion:NULL];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([UIDevice currentDevice].systemVersion.floatValue < 11) {
        return;
    }
    if ([viewController isKindOfClass:NSClassFromString(@"PUPhotoPickerHostViewController")]) {
        [viewController.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.frame.size.width < 42) {
                [viewController.view sendSubviewToBack:obj];
                *stop = YES;
            }
        }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    //处理完毕，回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:NULL];
    NSLog(@"----%@----%@",[RCIM sharedRCIM].currentUserInfo.name,[RCIM sharedRCIM].currentUserInfo.userId);
    if (image) {
        RCImageMessage *imageMessage = [RCImageMessage messageWithImage:image];
        imageMessage.senderUserInfo = [RCIM sharedRCIM].currentUserInfo;
        
        [[RCIM sharedRCIM] sendMediaMessage:self.conversationType
                                   targetId:self.targetId
                                    content:imageMessage
                                pushContent:@"[图片]"
                                   pushData:nil
                                   progress:^(int progress, long messageId) {
        } success:^(long messageId) {
            NSLog(@"成功了---%ld",messageId);
        } error:^(RCErrorCode errorCode, long messageId) {
            NSLog(@"失败了---%ld，----%ld",errorCode,messageId);
        } cancel:^(long messageId) {
            NSLog(@"取消了---%ld",messageId);
        }];
    }
}

- (void)clearHistoryMSG:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.conversationDataRepository removeAllObjects];
        [self.conversationMessageCollectionView reloadData];
    });
}

- (NSArray<UIMenuItem *> *)getLongTouchMessageCellMenuList:(RCMessageModel *)model {
    NSMutableArray<UIMenuItem *> *menuList = [[super getLongTouchMessageCellMenuList:model] mutableCopy];
    
     [menuList enumerateObjectsUsingBlock:^(UIMenuItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
         if ([obj.title isEqualToString:@"更多..."]) {
             [menuList removeObjectAtIndex:idx];
             *stop = YES;
         }
     }];
    
    return menuList;
}

//#pragma mark - > 重写多选后取消功能
//- (void)onCancelMultiSelectEvent:(UIBarButtonItem *)item{
//    self.allowsMessageCellSelection = NO;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self setupNavigationBar];
//    });
//}
//
//#pragma mark - > 重写多选后删除功能
//- (void)deleteMessages{
//
//    for (int i = 0; i < self.selectedMessages.count; i++) {
//        [self deleteMessage:self.selectedMessages[i]];
//    }
//    //置为 NO,将消息 cell 重置为初始状态
//    self.allowsMessageCellSelection = NO;
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self setupNavigationBar];
//    });
//}

@end
