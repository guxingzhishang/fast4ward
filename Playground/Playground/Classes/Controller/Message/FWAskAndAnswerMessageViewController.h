//
//  FWAskAndAnswerMessageViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/3.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWAskAndAnswerMessageViewController : FWBaseViewController
@property (nonatomic, strong) NSString * msg_type;
@property (nonatomic, strong) NSString * titleString;

@end

NS_ASSUME_NONNULL_END
