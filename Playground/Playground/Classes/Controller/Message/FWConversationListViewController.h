//
//  FWConversationListViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWConversationListViewController : RCConversationListViewController<RCIMUserInfoDataSource,UITableViewDelegate,UITableViewDataSource>

@end

NS_ASSUME_NONNULL_END
