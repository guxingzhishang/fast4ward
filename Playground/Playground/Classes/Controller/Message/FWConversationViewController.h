//
//  FWConversationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/2.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWConversationViewController : RCConversationViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>

/**
 * 照片选择器（照相 | 相册）
 */
@property (nonatomic, strong) UIImagePickerController * picker;
@end

NS_ASSUME_NONNULL_END
