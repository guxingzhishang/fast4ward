//
//  FWOfficailMessageViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWOfficailMessageViewController.h"
#import "FWMessageHomeModel.h"
#import "FWMessageHomeRequest.h"
#import "FWMessageOfficialCell.h"

@interface FWOfficailMessageViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) NSMutableArray * officialDataSource;
@property (nonatomic, assign) NSInteger officialPageNum;
@property (nonatomic, strong) FWMessageHomeModel * officialModel;

@end

@implementation FWOfficailMessageViewController
@synthesize infoTableView;
@synthesize officialDataSource;
@synthesize officialPageNum;

#pragma mark - > 请求官方消息
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.officialPageNum = 1;
        
        if (self.officialDataSource.count > 0 ) {
            [self.officialDataSource removeAllObjects];
        }
    }else{
        self.officialPageNum +=1;
    }
    
    FWMessageHomeRequest * request = [[FWMessageHomeRequest alloc] init];
    request.isNeedShowHud = YES;
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.officialPageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_msg_home  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [self.infoTableView.mj_footer endRefreshing];
            }else{
                [self.infoTableView.mj_header endRefreshing];
            }
            
            self.officialModel = [FWMessageHomeModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.officialDataSource addObjectsFromArray:self.officialModel.sys_msg_list];
            
            if([self.officialModel.sys_msg_list count] == 0 && self.officialPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:[NSString stringWithFormat:@"%@页",self.currentType]];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:[NSString stringWithFormat:@"%@页",self.currentType]];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.messageType == FWMessageAtMeType) {
        self.currentType = @"@我";
    }else if (self.messageType == FWMessageAnswerType) {
        self.currentType = @"回答";
    }else if (self.messageType == FWMessageOfficialType) {
        self.currentType = @"系统消息";
    }
    self.title = self.currentType;

    self.officialDataSource = @[].mutableCopy;

    [self setupSubViews];
    [self requestDataWithLoadMoredData:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 74;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:NO];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.officialDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"officailMessageID";

    FWMessageOfficialCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWMessageOfficialCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    [cell cellConfigureFor:self.officialDataSource[indexPath.row]];

    return cell;
}


#pragma mark - > 标记为已读
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    //移除红点提醒
    [self cyl_removeTabBadgePoint];

    FWMessageListModel * model = self.officialDataSource[indexPath.row];
    FWMessageOfficialCell * cell = [tableView cellForRowAtIndexPath:indexPath];

    NSString * sys_msg_id = model.sys_msg_id;

    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"sys_msg_id":sys_msg_id,
                              };

    FWMessageHomeRequest * request = [[FWMessageHomeRequest alloc] init];

    [request startWithParameters:params WithAction:Submit_read_sysmsg  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            /*如果不需要点击进入其他页面，需要请求一次数据，将小红点去掉。
             如果需要进入其他页面，则不需要在请求了*/
            //            [self requestData];
            model.read_status = @"1";
            [cell cellConfigureFor:self.officialDataSource[indexPath.row]];

            [self openURLWithURL:model.url];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)openURLWithURL:(NSString *)URL{

    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = URL;
    [self.navigationController pushViewController:WVC animated:YES];
}

@end
