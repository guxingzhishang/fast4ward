//
//  FWAskAndAnswerMessageViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/3.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWAskAndAnswerMessageViewController.h"
#import "FWThumbUpCell.h"
#import "FWUnionMessageModel.h"
#import "FWLikesMessageRequest.h"
#import "FWCommentMessageRequest.h"
#import "FWTableView.h"

#import "FWCommentMessageCell.h"
#import "FWCommentDetailViewController.h"
#import "FWCarDetailViewController.h"
#import "FWAskAndAnswerDetailViewController.h"

@interface FWAskAndAnswerMessageViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) NSMutableArray<FWUnionMessageModel *> * dataSource;

@property (nonatomic, assign) NSInteger pageNum;

@end

@implementation FWAskAndAnswerMessageViewController
@synthesize infoTableView;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self requestDataWithLoadMoredData:NO];

    [self trackPageBegin:[NSString stringWithFormat:@"%@页",self.titleString]];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
   
    [self trackPageEnd:[NSString stringWithFormat:@"%@页",self.titleString]];
}

- (void)viewDidLoad {
  
    [super viewDidLoad];

    self.pageNum = 0;
    self.dataSource = @[].mutableCopy;

    self.title = self.titleString;
   
    [self setupSubViews];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.rowHeight = 70;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDataWithLoadMoredData:YES];
    }];
}

#pragma mark - > 网络请求
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{

    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];;

    NSDictionary * params = @{
                              @"msg_type":self.msg_type,
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"pagesize":@"20",
                              };
    request.isNeedShowHud = YES;
    
    [request startWithParameters:params WithAction:Get_msg_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSMutableArray * tempArr = [FWUnionMessageModel mj_objectArrayWithKeyValuesArray:[[back objectForKey:@"data"] objectForKey:@"msg_list"]];
            
            [self.dataSource addObjectsFromArray:tempArr];
            
            if([tempArr count] == 0 && self.pageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger number = self.dataSource.count?self.dataSource.count:0;
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"CommentID";
    
    FWThumbUpCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWThumbUpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.vc = self;
    [cell cellConfigureFor:self.dataSource[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
//    RCDVC.listModel = self.dataSource[indexPath.row];
    RCDVC.feed_id = self.dataSource[indexPath.row].feed_id;
//        DVC.myBlock = ^(FWFeedListModel *listModel) {
//            self.dataSource[index] = listModel;
//        };
    [self.navigationController pushViewController:RCDVC animated:YES];
}


@end
