//
//  FWConversationSettingViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWConversationSettingViewController : RCSettingViewController

@property (nonatomic, assign) BOOL  isDelete;

@end

NS_ASSUME_NONNULL_END
