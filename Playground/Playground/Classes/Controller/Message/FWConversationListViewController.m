//
//  FWConversationListViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWConversationListViewController.h"
#import "FWConversationViewController.h"


@implementation FWConversationListViewController

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if (@available(iOS 11.0, *)) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentAutomatic];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.conversationListTableView reloadData];
    });
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.navigationItem.title = @"私信";
    
    // 设置发送的消息是否携带用户信息
    [RCIM sharedRCIM].enableMessageAttachUserInfo = YES;
    
    //开启输入状态监听
    [RCIM sharedRCIM].enableTypingStatus = YES;

    // 设置需要显示哪些类型的会话
    [self setDisplayConversationTypes:@[@(ConversationType_PRIVATE),
                                        @(ConversationType_GROUP)]];
    
    // 设置代理,代理方法要返回用户信息
    [[RCIM sharedRCIM] setUserInfoDataSource:self];
    
    // 设置会话列表头像和会话页面头像
    [RCIM sharedRCIM].globalConversationPortraitSize = CGSizeMake(46, 46);
    [RCIM sharedRCIM].globalConversationAvatarStyle = RC_USER_AVATAR_CYCLE;
    
    /* 去除没有消息时的线 */
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1)];
    self.conversationListTableView.tableFooterView = view;
    
    UIButton* backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    backButton.titleLabel.font = DHSystemFontOfSize_16;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    self.conversationListTableView.delegate = self;
    self.conversationListTableView.dataSource = self;
}

#pragma mark - >  重写RCConversationListViewController的onSelectedTableRow事件
- (void)onSelectedTableRow:(RCConversationModelType)conversationModelType
         conversationModel:(RCConversationModel *)model
               atIndexPath:(NSIndexPath *)indexPath {
    
    FWConversationViewController *chat = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:model.targetId];
    [self.navigationController pushViewController:chat animated:YES];
}

#pragma mark - > 返回
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - >  RCIMUserInfoDataSource的代理方法
- (void)getUserInfoWithUserId:(NSString *)userId completion:(void (^)(RCUserInfo *))completion {
    // 每次展示用户头像等信息都会调用,正常开发应该从app服务器获取,保存在本地
//    NSLog(@"userId===%@",userId);
    
    if (!userId) {
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"f_uid":userId};
    
    [request startWithParameters:params WithAction:Get_user_profile  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            NSDictionary * infoDict = [[back objectForKey:@"data"] objectForKey:@"user_info"];
            RCUserInfo *info = [[RCUserInfo alloc] initWithUserId:userId name:infoDict[@"nickname"] portrait:infoDict[@"header_url"]];
            completion(info);
        }
    }];
}

@end

