//
//  FWOfficailMessageViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSUInteger, FWMessageType) {
    FWMessageAtMeType = 1,  // 消息 - @我
    FWMessageAnswerType,    // 消息 - 回答
    FWMessageOfficialType,  // 消息 - 系统消息
};

@interface FWOfficailMessageViewController : FWBaseViewController

@property (nonatomic, assign) NSInteger messageType;
@property (nonatomic, strong) NSString * currentType;

@end

NS_ASSUME_NONNULL_END
