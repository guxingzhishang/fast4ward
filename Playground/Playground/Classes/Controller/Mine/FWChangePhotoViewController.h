//
//  FWChangePhotoViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWChangePhotoViewController : FWBaseViewController

@property (nonatomic, strong) UIImageView * browseImgView;

@property (nonatomic, strong) UIView *background;

@property (nonatomic, strong) UIButton * changePhotoButton;

@property (nonatomic, strong) NSString * photoURL;

@property (nonatomic, assign) BOOL  isMine;


@end

NS_ASSUME_NONNULL_END
