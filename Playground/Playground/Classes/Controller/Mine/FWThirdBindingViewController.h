//
//  FWThirdBindingViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/29.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWBaseCellView.h"

@interface FWThirdBindingViewController : FWBaseViewController

@property (nonatomic, strong) FWBaseCellView * cellView;


@end
