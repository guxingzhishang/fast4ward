//
//  FWInformationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWInformationViewController.h"
#import "FWUpdateInfomationRequest.h"
#import "ALiOssUploadTool.h"
#import "FWAttentionRefitTopicViewController.h"

static NSInteger photoWidth = 120;

@interface FWInformationViewController ()

//默认选择男性
@property (nonatomic, assign) NSInteger sexType;

@property(nonatomic,strong)NSDictionary * dataDict;

@property(nonatomic,strong) UIImagePickerController *picker;

@end

@implementation FWInformationViewController
@synthesize titleLabel;
@synthesize photoImageView;
@synthesize nameTextField;
@synthesize tipLabel;
@synthesize maleButton;
@synthesize femaleButton;
@synthesize completeButton;
@synthesize sexType;
@synthesize rightImageView;
@synthesize imagePicker;
@synthesize uploadImage;
@synthesize isNext;
@synthesize picker;

#pragma mark - > 请求阿里临时验证
- (void)requestStsData{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    [request requestStsTokenWithType:@"1"];
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadNameWithImage:(UIImage *)image{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"user_header",
                              @"number":@"1",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.dataDict = [back objectForKey:@"data"];
            
            [self uploadALIYunWithImage:image];
        }
    }];
}

- (void)uploadALIYunWithImage:(UIImage *)imagenew{
    
    photoImageView.image = imagenew;
    
    [ALiOssUploadTool asyncUploadImage:imagenew WithBucketName:[self.dataDict objectForKey:@"bucket"] WithObject_key:[self.dataDict objectForKey:@"object_key"] WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
        if (UploadImageSuccess == state) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                photoImageView.layer.cornerRadius = photoWidth/2;
                
                NSMutableDictionary * params = @{}.mutableCopy;
                [params setObject:[self.dataDict objectForKey:@"object_key"] forKey:@"user_header"];
                [self requestUpdateInfo:params];
                [[FWHudManager sharedManager] showSuccessMessage:@"上传成功" toController:self];
            });
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"完善个人信息页（注册流程）"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"完善个人信息页（注册流程）"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.view.userInteractionEnabled = YES;
    
    self.fd_interactivePopDisabled = YES;
    self.fd_prefersNavigationBarHidden = YES;
    
    sexType = 1;
    isNext = NO;
    
    [self requestStsData];
    
    [self setupSubViews];
}


#pragma mark - > 初始化视图
- (void)setupSubViews{
    
    photoImageView = [[UIImageView alloc] init];
    photoImageView.layer.cornerRadius = photoWidth/2;
    photoImageView.layer.masksToBounds = YES;
    photoImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    [self.view addSubview:photoImageView];
    [photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset(120);
        make.left.mas_equalTo(self.view).mas_offset((SCREEN_WIDTH-photoWidth)/2);
        make.size.mas_equalTo(CGSizeMake(photoWidth, photoWidth));
    }];
    photoImageView.userInteractionEnabled = YES;

    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhoto)];
    [photoImageView addGestureRecognizer:gesture];
    
    tipLabel = [[UILabel alloc] init];
    tipLabel.text = @"昵称";
    tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.1];
    tipLabel.textColor = FWTextColor_000000;
    tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(photoImageView.mas_bottom).mas_offset(40);
        make.size.mas_equalTo(CGSizeMake(150, 20));
        make.left.mas_equalTo(self.view).mas_offset(38);
    }];
    
    nameTextField = [[UITextField alloc] init];
    nameTextField.delegate = self;
    nameTextField.placeholder = @"请输入昵称";
    nameTextField.textColor = FWTextColor_12101D;
    nameTextField.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:18];
    nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.view addSubview:nameTextField];
    [nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(tipLabel);
        make.right.mas_equalTo(self.view).mas_offset(-40);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-100, 40));
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(tipLabel.mas_bottom).mas_offset(5);
    }];
    
    rightImageView = [[UIImageView alloc] init];
    rightImageView.hidden = YES;
    rightImageView.image = [UIImage imageNamed:@"select"];
    [self.view addSubview:rightImageView];
    [rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(nameTextField);
        make.size.mas_equalTo(CGSizeMake(12, 12));
        make.right.mas_equalTo(nameTextField).mas_offset(-10);
    }];
    
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWTextColor_12101D;
    [self.view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(nameTextField);
        make.height.mas_equalTo(0.5);
        make.top.mas_equalTo(nameTextField.mas_bottom).mas_offset(5);
    }];
    
    UILabel * sexLabel = [[UILabel alloc] init];
    sexLabel.text = @"性别";
    sexLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    sexLabel.textColor = FWTextColor_12101D;
    sexLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:sexLabel];
    [sexLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(25);
        make.size.mas_equalTo(CGSizeMake(150, 20));
        make.left.mas_equalTo(tipLabel);
    }];
    
    /* 男 */
    maleButton = [[UIButton alloc] init];
    [maleButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateHighlighted];
    [maleButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateSelected];
    [maleButton setBackgroundImage:[FWClearColor image] forState:UIControlStateNormal];
    
    [maleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateSelected];
    [maleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateHighlighted];
    [maleButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];

    maleButton.layer.cornerRadius = 2;
    maleButton.layer.masksToBounds = YES;
    [maleButton setTitle:@"男" forState:UIControlStateNormal];
    [maleButton setSelected:YES];
    maleButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:12];
    [maleButton addTarget:self action:@selector(selectMaleClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:maleButton];
    [maleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(sexLabel.mas_bottom).mas_offset(11);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.left.mas_equalTo(nameTextField);
    }];
    
    /* 女 */
    femaleButton = [[UIButton alloc] init];
    [femaleButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateHighlighted];
    [femaleButton setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateSelected];
    [femaleButton setBackgroundImage:[FWClearColor image] forState:UIControlStateNormal];
    
    [femaleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateSelected];
    [femaleButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateHighlighted];
    [femaleButton setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
    femaleButton.layer.borderColor = FWTextColor_222222.CGColor;
    femaleButton.layer.borderWidth = 1;
    femaleButton.layer.cornerRadius = 2;
    femaleButton.layer.masksToBounds = YES;
    [femaleButton setTitle:@"女" forState:UIControlStateNormal];
    femaleButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:12];
    [femaleButton addTarget:self action:@selector(selectFemaleClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:femaleButton];
    [femaleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(maleButton);
        make.size.mas_equalTo(maleButton);
        make.left.mas_equalTo(maleButton.mas_right).mas_offset(29);
    }];
    
    completeButton = [[UIButton alloc] init];
    completeButton.backgroundColor =FWTextColor_222222;
    [completeButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    completeButton.layer.cornerRadius = 2;
    completeButton.layer.masksToBounds = YES;
    [completeButton setTitle:@"下一步" forState:UIControlStateNormal];
    completeButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:16];
    [completeButton addTarget:self action:@selector(completeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:completeButton];
    [completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(femaleButton.mas_bottom).mas_offset(71);
        make.height.mas_equalTo(44);
        make.left.right.mas_equalTo(nameTextField);
    }];
}

#pragma mark - > 选择男性
- (void)selectMaleClick{
    
    sexType = 1;
    [maleButton setSelected:YES];
    [femaleButton setSelected:NO];

    maleButton.layer.borderColor = FWClearColor.CGColor;
    maleButton.layer.borderWidth = 0;
    
    femaleButton.layer.borderColor = FWTextColor_222222.CGColor;
    femaleButton.layer.borderWidth = 1;
}

#pragma mark - > 选择女性
- (void)selectFemaleClick{
    
    sexType = 2;
    [maleButton setSelected:NO];
    [femaleButton setSelected:YES];
    
    maleButton.layer.borderColor = FWTextColor_222222.CGColor;
    maleButton.layer.borderWidth = 1;
    
    femaleButton.layer.borderColor = FWClearColor.CGColor;
    femaleButton.layer.borderWidth = 0;
}

#pragma mark - > 更换头像
-(void)uploadPhoto{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"更换头像" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:1];
    }];
    UIAlertAction *selectAction = [UIAlertAction actionWithTitle:@"选择相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:2];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:takePhotoAction];
    [alertController addAction:selectAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 选择相册
- (void)selectLibraryWithType:(NSInteger)type{
    
    if (@available(iOS 11, *)) {
        
        UIScrollView.appearance.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
    }
    
    NSUInteger sourceType = 0;
    
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeCamera;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
    }else{
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
    }
    
    picker= [[UIImagePickerController alloc] init];
    picker.delegate                 = self;
    picker.sourceType               = sourceType;
    picker.allowsEditing            = YES;
    picker.navigationController.navigationBar.hidden = NO;
    picker.fd_prefersNavigationBarHidden = NO;
    picker.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *avatar = info[UIImagePickerControllerEditedImage];
    //处理完毕，回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    photoImageView.layer.cornerRadius = photoWidth/2;

    [self requestUploadNameWithImage:avatar];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([UIDevice currentDevice].systemVersion.floatValue < 11) {
        return;
    }
    if ([viewController isKindOfClass:NSClassFromString(@"PUPhotoPickerHostViewController")]) {
        [viewController.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.frame.size.width < 42) {
                [viewController.view sendSubviewToBack:obj];
                *stop = YES;
            }
        }];
    }
}

#pragma mark - > 完成
- (void)completeButtonClick{
 
    if (nameTextField.text.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入昵称" toController:self];
        return;
    }
    
    NSMutableDictionary * params = @{}.mutableCopy;
    
    NSString * sex         = [GFStaticData getObjectForKey:kTagUserKeySex];
    NSString * nickName    = [GFStaticData getObjectForKey:kTagUserKeyName];
    
    if ([sex integerValue] != self.sexType) {
        [params setObject:@(self.sexType).stringValue forKey:@"sex"];
    }
    
    if (![nickName isEqualToString:nameTextField.text] && nameTextField.text.length > 0) {
        [params setObject:nameTextField.text forKey:@"nickname"];
    }

    isNext = YES;
    
    [self requestUpdateInfo:params];
}

#pragma mark - > 更新个人信息
- (void)requestUpdateInfo:(NSMutableDictionary *)params{
    
    [params setObject:[GFStaticData getObjectForKey:kTagUserKeyID] forKey:@"uid"];
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];

    [request startWithParameters:params WithAction:Submit_update_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            NSDictionary * infoDict = HYGET_OBJECT_FROMDIC(back, @"data",@"user_info");
            [GFStaticData saveLoginData:infoDict];
            
            [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
            [[FWUserCenter sharedManager] saveUserInfoToDiskWithJson:back];
            [FWUserCenter sharedManager].user_IsLogin = YES;
            
            /* 如果是请求完上传头像后，不需要跳转；如果是保存信息后需要跳转到下一页面 */
            if (isNext) {
                FWAttentionRefitTopicViewController * IVC = [[FWAttentionRefitTopicViewController alloc] init];
                IVC.fromLogin = self.fromLogin;
                [self.navigationController pushViewController:IVC animated:YES];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([string isEqualToString:@"<"] ||
       [string isEqualToString:@">"] ||
       [string isEqualToString:@"/"] ||
       [string isEqualToString:@"“"] ||
       [string isEqualToString:@"•"] ||
       [string isEqualToString:@"’"] ||
       [string isEqualToString:@"…"] ||
       [string isEqualToString:@"……"] ){
        return NO;
    }
    
    UITextRange *selectedRange = textField.markedTextRange;
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    
    if (!position) {
        // 没有高亮选择的字
        // 1. 过滤非汉字、字母、数字字符
        nameTextField.text = [self filterCharactor:textField.text withRegex:@"[^a-zA-Z0-9\u4e00-\u9fa5]"];
        // 2. 截取
        if (nameTextField.text.length > 12) {
            nameTextField.text = [nameTextField.text substringToIndex:12];
        }
    } else {
        // 有高亮选择的字 不做任何操作
    }
    
    return YES;
}

// 过滤字符串中的非汉字、字母、数字
- (NSString *)filterCharactor:(NSString *)string withRegex:(NSString *)regexStr{
    NSString *filterText = string;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexStr options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *result = [regex stringByReplacingMatchesInString:filterText options:NSMatchingReportCompletion range:NSMakeRange(0, filterText.length) withTemplate:@""];
    return result;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
