//
//  FWBussinessHomePageViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 商家主页
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWBussinessHomePageViewController : FWBaseViewController

@property (nonatomic, strong) FWMineModel * mineModel;

@end

NS_ASSUME_NONNULL_END
