//
//  FWMyVIPViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWMyVIPViewController : FWBaseViewController

@property (nonatomic, strong) FWMineInfoModel * infoModel;


@end

NS_ASSUME_NONNULL_END
