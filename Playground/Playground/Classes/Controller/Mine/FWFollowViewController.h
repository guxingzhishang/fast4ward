//
//  FWFollowViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 我的 - > 关注
 */
#import "FWBaseViewController.h"
#import "FWFollowCell.h"
#import "FWFollowHeaderView.h"
#import "FWTagsFollowCell.h"

@interface FWFollowViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource,FWFollowCellCellDelegate,FWFollowHeaderViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) FWTableView * infoTableView;

@property (nonatomic, strong) FWTableView * tagsTableView;

@property (nonatomic, strong) FWFollowHeaderView * headerView;

@property (nonatomic, strong) UIScrollView * segementScr;

/* 1 关注的用户; 2 关注的标签 */
@property (nonatomic, assign) NSInteger segementType;

@property (nonatomic, assign) BOOL isRefresh;

@property (nonatomic, strong) NSString * base_uid;

@end
