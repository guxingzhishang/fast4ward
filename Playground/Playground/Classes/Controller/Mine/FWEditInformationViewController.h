//
//  FWEditInformationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 我的 - > 编辑个人信息
 */

#import "FWBaseViewController.h"
#import "FWEditInformationView.h"

@interface FWEditInformationViewController : FWBaseViewController<FWEditInformationViewDelegate>

@property (nonatomic, strong) FWEditInformationView * editView;

@property (nonatomic, strong) UIButton * saveButton;

@property (nonatomic, strong) UIBarButtonItem * rightBtnItem;

@property (nonatomic, strong) NSString * header_url;

@end
