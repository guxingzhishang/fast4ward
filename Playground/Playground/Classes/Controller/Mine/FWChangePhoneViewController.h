//
//  FWChangePhoneViewController.h
//  Playground
//
//  Created by hehe on 2018/9/24.
//  Copyright © 2018年 hehe. All rights reserved.
//

/**
 * 我的 - > 设置 - > 修改手机号
 * 登录 — > 输入手机号 - >验证码页
 */

#import "FWBaseViewController.h"
#import "FWChangePhoneView.h"

@interface FWChangePhoneViewController : FWBaseViewController<FWChangePhoneViewDelegate>

@property (nonatomic, strong) FWChangePhoneView * changeView;

/**
 * 手机号
 */
@property (nonatomic, copy) NSString * phoneNumber;

/**
 * 验证码
 */
@property (nonatomic, copy) NSString * codeNumber;


/**
 * 1 登录输入验证码 ；2 微信绑定手机号；3、更换新手机号
 */
@property (nonatomic, assign) NSInteger type;

/* 1、正常登录   2、游客模式下的登录（成功后要返回登录之前停留页面）*/
@property (nonatomic, assign) NSInteger  fromLogin;

@end
