//
//  FWVisitorViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWTableView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWVisitorViewController : FWBaseViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@end

NS_ASSUME_NONNULL_END
