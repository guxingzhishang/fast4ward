//
//  FWFeedBackViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/23.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWFeedBackViewController.h"
#import "FWFeedBackRequest.h"

#define MAX_LIMIT_NUMS 200

@interface FWFeedBackViewController ()

@end

@implementation FWFeedBackViewController
@synthesize feedbackView;
@synthesize numberLabel;
@synthesize tipLabel;
@synthesize cpButton;
@synthesize sendButton;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"意见反馈";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"意见反馈页"];

    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"意见反馈页"];

    //移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)addSubviews{
    
    NSInteger Margin = 15;
    
    feedbackView = [[IQTextView alloc] init];
    feedbackView.placeholder = @"有什么想吐槽的丢在这里吧!";
    feedbackView.delegate = self;
    feedbackView.textContainerInset = UIEdgeInsetsMake(Margin, Margin-5, Margin, Margin);
    [self.view addSubview:feedbackView];
    [feedbackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 175));
    }];
    
    numberLabel = [[UILabel alloc] init];
    numberLabel.text = @"0/200";
    numberLabel.font = DHSystemFontOfSize_12;
    numberLabel.textColor = FWTextColor_969696;
    numberLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:numberLabel];
    [self.view bringSubviewToFront:numberLabel];
    [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(feedbackView).mas_offset(-15);
        make.bottom.mas_equalTo(feedbackView).mas_offset(-5);
        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    
    tipLabel = [[UILabel alloc] init];
    tipLabel.numberOfLines = 0;
    tipLabel.font = DHSystemFontOfSize_13;
    tipLabel.textAlignment = NSTextAlignmentLeft;
    tipLabel.textColor = FWTextColor_000000;
    
    NSString * testString = self.tipString;
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:testString];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [testString length])];
    [tipLabel setAttributedText:attributedString];

    [self.view addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(60);
        make.left.mas_equalTo(feedbackView).mas_offset(15);
        make.right.mas_equalTo(feedbackView).mas_offset(-15);
        make.top.mas_equalTo(feedbackView.mas_bottom).mas_offset(14);
    }];
    
    cpButton = [[UIButton alloc] init];
    cpButton.layer.cornerRadius = 12;
    cpButton.layer.masksToBounds = YES;
    cpButton.titleLabel.font = DHSystemFontOfSize_12;
    cpButton.backgroundColor = FWTextColor_68769F;
    [cpButton setTitle:@"点击复制" forState:UIControlStateNormal];
    [cpButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [cpButton addTarget:self action:@selector(cpButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cpButton];
    [cpButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(67);
        make.height.mas_equalTo(24);
        make.right.mas_equalTo(self.view).mas_offset(-10);
        make.top.mas_equalTo(feedbackView.mas_bottom).mas_offset(45);
    }];
    
    sendButton = [[UIButton alloc] init];
    sendButton.layer.cornerRadius = 2;
    sendButton.layer.masksToBounds = YES;
    sendButton.titleLabel.font = DHSystemFontOfSize_16;
    sendButton.backgroundColor = FWTextColor_222222;
    [sendButton setTitle:@"提交反馈" forState:UIControlStateNormal];
    [sendButton setTitleColor:DHTitleColor_FFFFFF forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(sendButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sendButton];
    [sendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH-60);
        make.height.mas_equalTo(48);
        make.right.mas_equalTo(self.view).mas_offset(-30);
        make.left.mas_equalTo(self.view).mas_offset(30);
        make.bottom.mas_equalTo(self.view).mas_offset(-78);
    }];
}

#pragma mark - > 复制
- (void)cpButtonClick{
    
    [[FWHudManager sharedManager] showSuccessMessage:@"复制成功" toController:self];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.gongzhonghao;
}

#pragma mark - > 发送
- (void)sendButtonClick{
    
    FWFeedBackRequest * request = [[FWFeedBackRequest alloc] init];
    request.isNeedShowHud=YES;
    
    NSDictionary * dict = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                            @"content":feedbackView.text};
    
    request.isNeedShowHud = YES;
    [request startWithParameters:dict WithAction:Submit_feedback  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        if ([code isEqualToString:NetRespondCodeSuccess]){
            
            [[FWHudManager sharedManager] showSuccessMessage:@"反馈成功" toController:self];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [[FWHudManager sharedManager] showErrorMessage:[responseObject objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - UITextView的代理
//内容将要发生改变编辑
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text{
    
    UITextRange *selectedRange = [textView markedTextRange];
    //获取高亮部分
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
    
    //如果有高亮且当前字数开始位置小于最大限制时允许输入
    if (selectedRange && pos) {
        NSInteger startOffset = [textView offsetFromPosition:textView.beginningOfDocument toPosition:selectedRange.start];
        NSInteger endOffset = [textView offsetFromPosition:textView.beginningOfDocument toPosition:selectedRange.end];
        NSRange offsetRange = NSMakeRange(startOffset, endOffset - startOffset);
        
        if (offsetRange.location < MAX_LIMIT_NUMS) {
            return YES;
        }else{
            return NO;
        }
    }
    
    NSString *comcatstr = [textView.text stringByReplacingCharactersInRange:range withString:text];

    if (comcatstr.length >200) {
        sendButton.enabled = NO;
        sendButton.backgroundColor = FWTextColor_969696;
        numberLabel.textColor = DHPinkColorFF8B8C;
    }else{
        sendButton.enabled = YES;
        sendButton.backgroundColor = FWOrange;
        numberLabel.textColor = FWTextColor_969696;
    }
    
    numberLabel.text = [NSString stringWithFormat:@"%ld/%ld",(long)comcatstr.length,(long)MAX_LIMIT_NUMS];
    
    if (comcatstr.length > 0){
        
        NSInteger len = text.length;
        //防止当text.length + caninputlen < 0时，使得rg.length为一个非法最大正数出错
        NSRange rg = {0,MAX(len,0)};
        
        if (rg.length > 0){
            
            NSString *s = @"";
            //判断是否只普通的字符或asc码(对于中文和表情返回NO)
            BOOL asc = [text canBeConvertedToEncoding:NSASCIIStringEncoding];
            if (asc) {
                s = [text substringWithRange:rg];//因为是ascii码直接取就可以了不会错
            }else{
                __block NSInteger idx = 0;
                __block NSString  *trimString = @"";//截取出的字串
                //使用字符串遍历，这个方法能准确知道每个emoji是占一个unicode还是两个
                [text enumerateSubstringsInRange:NSMakeRange(0, [text length])
                                         options:NSStringEnumerationByComposedCharacterSequences
                                      usingBlock: ^(NSString* substring, NSRange substringRange, NSRange enclosingRange, BOOL* stop) {
                                          
                                          if (idx >= rg.length) {
                                              *stop = YES; //取出所需要就break，提高效率
                                              return ;
                                          }
                                          
                                          trimString = [trimString stringByAppendingString:substring];
                                          
                                          idx++;
                                      }];
                
                s = trimString;
            }
//            //rang是指从当前光标处进行替换处理(注意如果执行此句后面返回的是YES会触发didchange事件)
//            [textView setText:[textView.text stringByReplacingCharactersInRange:range withString:s]];
//            //既然是超出部分截取了，哪一定是最大限制了。
        }
//        return NO;
    }
    
    return YES;
}

@end
