//
//  FWFavouriteViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 收藏
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, FWFavouriteCollectionViewType) {
    FWFavouriteVideoCollectionType = 1,// 视频列表
    FWFavouritePicCollectionType,  // 图片列表
};


@interface FWFavouriteViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
