//
//  FWUserCarViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 个人中心 - 座驾
 */
#import "FWBaseViewController.h"
#import "YNPageTableView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWUserCarViewController : FWBaseViewController<UITableViewDataSource ,UITableViewDelegate>

@property (nonatomic, assign) BOOL isMine;
@property (nonatomic, strong) UITableView * carTableView;
@property (nonatomic, strong) NSString * user_id;

@end

NS_ASSUME_NONNULL_END
