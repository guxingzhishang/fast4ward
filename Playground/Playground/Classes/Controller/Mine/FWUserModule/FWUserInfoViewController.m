//
//  FWUserInfoViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/17.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWUserInfoViewController.h"
#import "FWMyworksCollectionCell.h"
#import "FWMyVideoCollectionCell.h"
#import "FWMineRequest.h"
#import "FWMineModel.h"
#import "FWReportViewController.h"
#import "FWFeedBackRequest.h"
#import "FWPublishPreviewViewController.h"
#import "FWPublishPreviewGraphViewController.h"
#import "FWArticalViewController.h"
#import "FWConversationViewController.h"
#import "FWMineCarListCell.h"
#import "FWCarDetailViewController.h"

static NSString * const allCellId = @"allCellId";
static NSString * const videoCellId = @"videoCellId";
static NSString * const carCellId = @"carCellId";

@interface FWUserInfoViewController()<UITableViewDelegate,UITableViewDataSource>
{
    FWMainTouchScrollView *rootScrollView; //rootScrollView要有滑动穿透
    FWScrollViewFollowCollectionView *thFollow; //必须写成属性
    NSInteger  ScrollHeight;//需三处保持一致
    
    UIImageView * naviBarView;
    UILabel * titleLable;
    UIImageView * headerView;
    
    FWUserTopView * userTopView;
    
    FWFeedModel * allFeedModel;
    FWFeedModel * videoFeedModel;
    FWCarModel * carModel;
    
    NSMutableArray * allDataSource;
    NSMutableArray * videoDataSource;
    NSMutableArray * carDataSource;

    NSInteger   allPageNum;
    NSInteger   videoPageNum;

    /* 左右划是否刷新（第一次划到视频或者草稿要刷新，其他情况只有上下拉刷新） */
    BOOL   isVideoRefresh;
    BOOL   isCarRefresh;

    /* 当前导航栏的透明度 */
    CGFloat alphaValue;
    
    FWUserDefaultsVariableModel * uvam;
}

@end

@implementation FWUserInfoViewController
@synthesize backButton;
@synthesize allCollectionView;
@synthesize videoCollectionView;
@synthesize carTableView;
@synthesize backGroundScr;
@synthesize collectionType;
@synthesize collectionScr;
@synthesize user_id;
@synthesize blackBackButton;
@synthesize blackShareButton;
@synthesize blackMoreButton;
@synthesize secondString;
@synthesize mineModel;
@synthesize chatButton;


- (void)requestRongYunTokenWithIM{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_im_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:userId];
                                            [RCIM sharedRCIM].currentUserInfo = userInfo;
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"IM登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            NSLog(@"IMtoken错误");
                                        }];
        }
    }];
}

#pragma mark - > 请求个人数据
- (void)requsetMineData{
    
    if (!self.user_id) {
        return;
    }
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":self.user_id,
                              };
    
    [request startWithParameters:params WithAction:Get_user_page  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            mineModel = [FWMineModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [userTopView configForView:mineModel];
            [self dealwithFrame];

            if (self.collectionType == FWCarTablevType) {
                [rootScrollView.mj_header endRefreshing];
            }
            
            titleLable.text = mineModel.user_info.nickname;
            [headerView sd_setImageWithURL:[NSURL URLWithString:mineModel.user_info.header_url]];
            
            if (collectionType == FWCarTablevType) {
                if (carDataSource.count > 0) {
                    [carDataSource removeAllObjects];
                }
                [carDataSource addObjectsFromArray:mineModel.list_car];
                [self.carTableView reloadData];
            }
            
            if ([mineModel.block_status isEqualToString:@"1"]) {
                secondString = @"取消屏蔽";
            }else{
                secondString = @"屏蔽";
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)dealwithFrame{
    
    CGFloat tempScrollHeight = 230;
    CGFloat tempUserTopHeight = 350+FWCustomeSafeTop;
    
    if ([self.mineModel.user_info.car_cert_status isEqualToString:@"3"] && [self.mineModel.user_info.driver_cert_status isEqualToString:@"2"]) {
        tempScrollHeight = 270;
        tempUserTopHeight = 350+FWCustomeSafeTop+40;
    }else if(([self.mineModel.user_info.car_cert_status isEqualToString:@"3"] && ![self.mineModel.user_info.driver_cert_status isEqualToString:@"2"])||
             (![self.mineModel.user_info.car_cert_status isEqualToString:@"3"] && [self.mineModel.user_info.driver_cert_status isEqualToString:@"2"])){
        tempScrollHeight = 250;
        tempUserTopHeight = 350+FWCustomeSafeTop+20;
    }
    
    ScrollHeight = tempScrollHeight;

    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);  //滚动大小
    
    userTopView.frame = CGRectMake(0, 0, SCREEN_WIDTH, tempUserTopHeight);
    
    self.collectionScr.frame = CGRectMake(0, CGRectGetMaxY(userTopView.frame)+1, CGRectGetWidth(rootScrollView.frame), SCREEN_HEIGHT-50-64-FWCustomeSafeTop-2);
    
    userTopView.mineViewType = FWUserType;
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH*3, 0);
    
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView,videoCollectionView,carTableView]];
}

#pragma mark - > 获取视频列表信息
- (void)requestAllListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (!self.user_id) {
        return;
    }
    if (isLoadMoredData == NO) {
        allPageNum = 1;
        
        if (allDataSource.count > 0 ) {
            [allDataSource removeAllObjects];
        }
    }else{
        allPageNum += 1;
    }
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"base_uid":self.user_id,
                             @"feed_type":@"1",
                             @"is_draft":@"0",
                             @"page":@(allPageNum).stringValue,
                             @"page_size":@"20",
                             };
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_uid_v2  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if (isLoadMoredData) {
                [allCollectionView.mj_footer endRefreshing];
            }else{
                [rootScrollView.mj_header endRefreshing];
            }
            
            allFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [allDataSource addObjectsFromArray: allFeedModel.feed_list];
            
            if([allFeedModel.feed_list count] == 0 &&
               allPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.allCollectionView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 获取图片列表信息
- (void)requestVideoListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        videoPageNum = 1;
        
        if (videoDataSource.count > 0 ) {
            [videoDataSource removeAllObjects];
        }
    }else{
        videoPageNum += 1;
    }
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"base_uid":self.user_id,
                             @"feed_type":@"2",
                             @"is_draft":@"0",
                             @"page":@(videoPageNum).stringValue,
                             @"page_size":@"20",
                             };
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_uid_v2  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if (isLoadMoredData) {
                [videoCollectionView.mj_footer endRefreshing];
            }else{
                [rootScrollView.mj_header endRefreshing];
            }
            
            videoFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [videoDataSource addObjectsFromArray: videoFeedModel.feed_list];
            
            if([videoFeedModel.feed_list count] == 0 &&
               videoPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.videoCollectionView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"用户个人页"];

    [self requestRongYunTokenWithIM];
    [self requsetMineData];
    [self requestVisitorWithUserID:self.user_id WithType:@"1" WithID:@"0"];
    
    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {
        
        if (collectionType == FWAllCollectionType) {
            int tempNum = -1;
            for (int i = 0; i < allDataSource.count; i++) {
                FWFeedListModel * tempModel = allDataSource[i];
                
                if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                    tempNum = i;
                }
            }
            
            if (tempNum >=0) {
                [allDataSource removeObjectAtIndex:tempNum];
                [allCollectionView reloadData];
            }
        }else if (collectionType == FWVideoCollectionType){
            
            int tempNum = -1;
            for (int i = 0; i < videoDataSource.count; i++) {
                FWFeedListModel * tempModel = videoDataSource[i];
                
                if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                    tempNum = i;
                }
            }
            
            if (tempNum >=0) {
                [videoDataSource removeObjectAtIndex:tempNum];
                [videoCollectionView reloadData];
            }
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"用户个人页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.fd_prefersNavigationBarHidden = YES;
    
    uvam = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];

    allDataSource = @[].mutableCopy;
    videoDataSource = @[].mutableCopy;
    carDataSource = @[].mutableCopy;
    
    isVideoRefresh = YES;
    isCarRefresh = YES;
    
    allPageNum = 0;
    videoPageNum = 0;
    ScrollHeight = 230;
    alphaValue = 0;

    [self setupCollectionView];
    
    //启动默认是所有列表
    collectionType = FWAllCollectionType;
    
    [self requestAllListWithLoadMoreData:NO];
}

#pragma mark - > 初始化视图
- (void)setupCollectionView{
    
    naviBarView = [[UIImageView alloc] init];
    naviBarView.userInteractionEnabled = YES;
    naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    naviBarView.image = [UIImage imageNamed:@"user_bg"];
    [self.view addSubview:naviBarView];
    naviBarView.alpha = 0;
    
    blackBackButton = [[UIButton alloc] init];
    [blackBackButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [blackBackButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [naviBarView addSubview:blackBackButton];
    [blackBackButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(naviBarView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(naviBarView).mas_offset(30+FWCustomeSafeTop);
    }];
    
    blackMoreButton = [[UIButton alloc] init];
    [blackMoreButton setImage:[UIImage imageNamed:@"white_more"] forState:UIControlStateNormal];
    [blackMoreButton addTarget:self action:@selector(moreButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [naviBarView addSubview:blackMoreButton];
    
    if([self.user_id isEqualToString:uvam.userModel.uid]){
        [blackMoreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(naviBarView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(0.1, 0.1));
            make.centerY.mas_equalTo(blackBackButton);
        }];
    }else{
        [blackMoreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(naviBarView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(blackBackButton);
        }];
    }
    
    blackShareButton = [[UIButton alloc] init];
    [blackShareButton setImage:[UIImage imageNamed:@"new_mine_share_white"] forState:UIControlStateNormal];
    [blackShareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [naviBarView addSubview:blackShareButton];
    [blackShareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(blackMoreButton.mas_left).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(36, 20));
        make.centerY.mas_equalTo(blackBackButton);
    }];
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        blackShareButton.hidden = NO;
    }else{
        blackShareButton.hidden = YES;
    }
    
    titleLable = [[UILabel alloc] init];
    titleLable.alpha = 0;
    titleLable.hidden = YES;
    titleLable.font = [UIFont fontWithName:@"PingFangSC-Regular" size:16];
    titleLable.textColor = FWViewBackgroundColor_FFFFFF;
    titleLable.textAlignment = NSTextAlignmentCenter;
    [naviBarView addSubview:titleLable];
    [titleLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(naviBarView);
        make.centerX.mas_equalTo(naviBarView).mas_offset(10);
        make.height.mas_equalTo(35);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-220);
    }];
    
    headerView = [[UIImageView alloc] init];
    headerView.layer.cornerRadius = 15;
    headerView.layer.masksToBounds = YES;
    headerView.alpha = 0;
    [naviBarView addSubview:headerView];
    [headerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(titleLable);
        make.right.mas_equalTo(titleLable.mas_left).mas_offset(-5);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    rootScrollView = [[FWMainTouchScrollView alloc] init];
    rootScrollView.frame  = CGRectMake(0,0,SCREEN_WIDTH, SCREEN_HEIGHT-0);
    rootScrollView.delegate=self; //要遵循代理
    rootScrollView.showsVerticalScrollIndicator = NO;
    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);  //滚动大小
    [self.view addSubview:rootScrollView];
    
    userTopView = [[FWUserTopView alloc] init];
    userTopView.backgroundColor = [UIColor clearColor];
    userTopView.hidden = NO;
    userTopView.delegate = self;
    [userTopView initClickMethodWithController:self];
    [rootScrollView addSubview:userTopView];
    userTopView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 350+FWCustomeSafeTop);

    
    if ([self.user_id isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        userTopView.attentionButton.hidden = YES;
    }else{
        userTopView.attentionButton.hidden = NO;
    }
    
    self.collectionScr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(userTopView.frame)+1, CGRectGetWidth(rootScrollView.frame), SCREEN_HEIGHT-50-64-FWCustomeSafeTop-2)];
    self.collectionScr.delegate = self;
    self.collectionScr.pagingEnabled = YES;
    self.collectionScr.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [rootScrollView addSubview:self.collectionScr];
    
    
    self.allLayout = [[FWBaseCollectionLayout alloc] init];
    self.allLayout.columns = 2;
    self.allLayout.rowMargin = 12;
    self.allLayout.colMargin = 12;
    self.allLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.allLayout.delegate = self;
    [self.allLayout autuContentSize];
    
    self.allCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.allLayout];
    self.allCollectionView.dataSource = self;
    self.allCollectionView.delegate = self;
    self.allCollectionView.showsVerticalScrollIndicator = NO;
    self.allCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.allCollectionView registerClass:[FWMyworksCollectionCell class] forCellWithReuseIdentifier:allCellId];
    self.allCollectionView.isNeedEmptyPlaceHolder = YES;
    self.allCollectionView.verticalOffset = -150;
    NSString * allTitle = @"还没有发布的视频哦~";
    NSDictionary * allAttributes = @{
                                     NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                     NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * allAttributeString = [[NSMutableAttributedString alloc] initWithString:allTitle attributes:allAttributes];
    self.allCollectionView.emptyDescriptionString = allAttributeString;
    [self.collectionScr addSubview:self.allCollectionView];

    
    self.videoLayout = [[FWBaseCollectionLayout alloc] init];
    self.videoLayout.columns = 2;
    self.videoLayout.rowMargin = 12;
    self.videoLayout.colMargin = 12;
    self.videoLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.videoLayout.delegate = self;
    [self.videoLayout autuContentSize];
    
    self.videoCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) collectionViewLayout:self.videoLayout];
    self.videoCollectionView.dataSource = self;
    self.videoCollectionView.delegate = self;
    self.videoCollectionView.showsVerticalScrollIndicator = NO;
    self.videoCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.videoCollectionView registerClass:[FWMyVideoCollectionCell class] forCellWithReuseIdentifier:videoCellId];
    self.videoCollectionView.isNeedEmptyPlaceHolder = YES;
    self.videoCollectionView.verticalOffset = -150;
    NSString * videoTitle = @"还没有发布的图片哦~";
    NSDictionary * videoAttributes = @{
                                       NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                       NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * videoAttributeString = [[NSMutableAttributedString alloc] initWithString:videoTitle attributes:videoAttributes];
    self.videoCollectionView.emptyDescriptionString = videoAttributeString;
    [self.collectionScr addSubview:self.videoCollectionView];
   
    carTableView = [[FWTableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*2,0, SCREEN_WIDTH, CGRectGetHeight(self.collectionScr.frame)) style:UITableViewStylePlain];
    carTableView.delegate = self;
    carTableView.dataSource = self;
    carTableView.rowHeight = 163;
    carTableView.isNeedEmptyPlaceHolder = YES;
    carTableView.verticalOffset = -150;
    NSString * title = @"暂无座驾";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    carTableView.emptyDescriptionString = attributeString;
    [self.collectionScr addSubview:carTableView];
    carTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];
    
    //利用initRoot创建 给予底部上下滑动的ScrollView 要滑动的ScrollRange 大小和左右滑动的Table数组
    thFollow = [[FWScrollViewFollowCollectionView alloc]init];
    
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:self.user_id]) {
        userTopView.mineViewType = FWMineType;
    }else{
        userTopView.mineViewType = FWUserType;
    }
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH*3, 0);
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[allCollectionView,videoCollectionView,carTableView]];

    
    {
        DHWeakSelf;
        rootScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{

            if (weakSelf.collectionType == FWAllCollectionType) {
                [weakSelf requestAllListWithLoadMoreData:NO];
            }else if (weakSelf.collectionType == FWVideoCollectionType){
                [weakSelf requestVideoListWithLoadMoreData:NO];
            }
            
            [weakSelf requsetMineData];
        }];
        
        @weakify(self);
        self.allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestAllListWithLoadMoreData:YES];
        }];
        
        self.videoCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestVideoListWithLoadMoreData:YES];
        }];
    }
    
    [self.view bringSubviewToFront:naviBarView];
    
    chatButton = [[UIButton alloc] init];
    [chatButton setImage:[UIImage imageNamed:@"private_chat"] forState:UIControlStateNormal];
    [chatButton addTarget:self action:@selector(chatButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:chatButton];
    [self.view bringSubviewToFront:chatButton];
    [chatButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).mas_offset(-4);
        make.size.mas_equalTo(CGSizeMake(52, 52));
        make.bottom.mas_equalTo(self.view).mas_offset(-89-FWSafeBottom);
    }];
    
    if ([self.user_id isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        chatButton.hidden = YES;
    }else{
        chatButton.hidden = NO;
    }
}

#pragma mark - > 私聊
- (void)chatButtonOnClick{
    
    [self cheackLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        
        FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
        if ([usvm.userModel.user_level intValue] > 0) {
            
            FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:self.user_id];
            [self.navigationController pushViewController:CVC animated:YES];
        }else{
            [self.view endEditing:YES];
    
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"会员可享受“私信”权益！" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"我要发私信" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
                    VVC.webTitle = @"会员权益";
                    [self.navigationController pushViewController:VVC animated:YES];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
    }
}

#pragma mark - > 代理三件套
- (void)shareButtonOnClick{
    [self shareButtonClick];
}

- (void)backButtonOnClick{
    [self backBtnClick];
}

- (void)moreButtonOnClick{
    [self moreButtonClick];
}

#pragma mark - > 分享
- (void)shareButtonClick{
    
    ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
    [shareView setupUserQRViewWithModel:mineModel];
    [shareView showView];
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0){
            
            [[ShareManager defaultShareManager] userShareToMinProgramObjectWithModel:mineModel];
        }else if (index == 1){
            
            UIImageView * imageView = [[ShareView defaultShareView] getCurrenImage];
            [[ShareManager defaultShareManager] shareToPengyouquan:imageView WithFeedID:mineModel.user_info.uid WithType:Share_uid];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}


#pragma mark - > 返回
- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 更多操作
- (void)moreButtonClick{
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]) {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"您可以进行以下操作" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            FWReportViewController * RVC = [[FWReportViewController alloc] init];
            RVC.f_uid = mineModel.user_info.uid;
            [self.navigationController pushViewController:RVC animated:YES];
        }];
        UIAlertAction *selectAction = [UIAlertAction actionWithTitle:secondString style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            if([mineModel.block_status isEqualToString:@"1"]){
                // 取消屏蔽
                [self requestFeedBackWithBlockStatus:@"2"];
            }else{
                // 屏蔽，二次确认
                [self secondMakeSure];
            }
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alertController addAction:takePhotoAction];
        [alertController addAction:selectAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self cheackLogin];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 屏蔽二次确认
- (void)secondMakeSure{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"屏蔽此人后，TA的视频不会再推荐给你。确定要屏蔽吗？" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self requestFeedBackWithBlockStatus:@"1"];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 屏蔽
- (void)requestFeedBackWithBlockStatus:(NSString *)block_status{
    
    FWFeedBackRequest * request = [[FWFeedBackRequest alloc] init];
    
    NSDictionary * dict = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                            @"feed_id":@"0",
                            @"content":secondString,
                            @"f_uid":mineModel.user_info.uid,
                            @"block_status":block_status,
                            };
    
    [request startWithParameters:dict WithAction:Submit_feedback  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        if ([code isEqualToString:NetRespondCodeSuccess]){
            
            if([block_status isEqualToString:@"1"]){
                [[FWHudManager sharedManager] showSuccessMessage:@"已加入黑名单" toController:self];
            }else {
                [[FWHudManager sharedManager] showSuccessMessage:@"已解除屏蔽" toController:self];
            }
            [self requsetMineData];
        }
    }];
}

#pragma mark - > collectionView`s  Delegate & DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger number;

    if (collectionView == allCollectionView) {
        number = allDataSource.count?allDataSource.count:0;
    }else {
        number = videoDataSource.count?videoDataSource.count:0;
    }
    return number;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == allCollectionView) {
        
        FWMyworksCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:allCellId forIndexPath:indexPath];
        cell.viewController = self;
        
        if (allDataSource.count > 0) {
            
            FWFeedListModel * model = allDataSource[indexPath.item];
            [cell configForCell:model];
            
            if (model.h5_url.length > 0 ) {
                
                cell.activityImageView.hidden = NO;
                
                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);
                
                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }else{
            cell.iv.hidden = YES;
            cell.userIcon.hidden = YES;
            cell.pvImageView.hidden = YES;
            cell.authenticationImageView.hidden = YES;
        }
        return cell;
    }else{
        
        FWMyVideoCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:videoCellId forIndexPath:indexPath];
        cell.viewController = self;

        if (videoDataSource.count > 0) {
            
            FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
            
            FWFeedListModel * model = videoDataSource[indexPath.item];
            [cell configForCell:model];
            
            if ([uvam.userModel.uid isEqualToString:model.user_info.uid] && [model.feed_type isEqualToString:@"3"] &&
                ([usvm.userModel.cert_status isEqualToString:@"2"] || [usvm.userModel.merchant_cert_status isEqualToString:@"3"])) {
                /* 如果是自己 并且是文章，显示关联商品*/
                cell.linkButton.hidden = NO;
            }else{
                cell.linkButton.hidden = YES;
            }
            
            if (model.h5_url.length > 0 ) {
                
                cell.activityImageView.hidden = NO;
                
                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);
                
                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }else{
            cell.iv.hidden = YES;
            cell.userIcon.hidden = YES;
            cell.pvImageView.hidden = YES;
            cell.authenticationImageView.hidden = YES;
        }
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray * tempArr;
    if (collectionView == allCollectionView) {
        tempArr = allDataSource;
    }else{
        tempArr = videoDataSource;
    }
    
    FWFeedListModel * model = tempArr[indexPath.row];
    NSMutableArray * tempDataSource = @[].mutableCopy;
    
    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }
    
    if ([model.feed_type isEqualToString:@"1"]) {
        
        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.listModel = model;
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {};
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            for (NSInteger i = indexPath.row; i<tempArr.count; i++) {
                FWFeedListModel * model = tempArr[i];
                
                if ([model.feed_type isEqualToString:@"1"]) {
                    [tempDataSource addObject:model];
                }
            }
            
            // 视频流 由于筛选过，所以每次点击的都是数据源第一个。
            FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
            controller.currentIndex = 0;
            controller.listModel = tempDataSource;
            controller.base_uid = self.user_id;
            controller.requestType = FWInfoListRequestType;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        for (FWFeedListModel * model in tempArr) {
            if ([model.feed_type isEqualToString:@"2"]) {
                [tempDataSource addObject:model];
            }
        }
        
        // 图文详情
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.listModel = model;
        TVC.feed_id = model.feed_id;
        TVC.myBlock = ^(FWFeedListModel *awemeModel) {};
        [self.navigationController pushViewController:TVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {
        
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"4"]) {
        // 问答
        FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
//        DVC.myBlock = ^(FWFeedListModel *listModel) {
//            self.dataSource[index] = listModel;
//        };
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        // 改装
        FWRefitCaseDetailViewController * RCDVC = [[FWRefitCaseDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
    //        DVC.myBlock = ^(FWFeedListModel *listModel) {
    //            self.dataSource[index] = listModel;
    //        };
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"6"]) {
        /* 闲置 */
        FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }
}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr;
    
    if (collectionType == FWAllCollectionType) {
        tempArr = allDataSource;
    }else{
        tempArr = videoDataSource;
    }
    
    if (indexPath.item >= tempArr.count) {
        return 0;
    }
    
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"2"]) {
        // 图文贴 固定比例3：4
        height = (cover_width *4)/3;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if([model.feed_type isEqualToString:@"3"]){
        // 文章帖 固定比例5：4
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if ([model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 20;
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 25;
        }
    }else  if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}

#pragma mark - > tableView‘s delegate & datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return carDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWMineCarListCell * cell = [tableView dequeueReusableCellWithIdentifier:carCellId];
    
    if (nil == cell) {
        cell = [[FWMineCarListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:carCellId];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row < carDataSource.count) {
        [cell configForCell:carDataSource[indexPath.row]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.mineModel.car_notice_status isEqualToString:@"1"]) {
        /* 座驾信息 */
        [[FWHudManager sharedManager] showErrorMessage:@"ta还没有完善座驾信息哦" toController:self];
        return;
    }else{
        
        FWCarDetailViewController * CDVC = [[FWCarDetailViewController alloc] init];
        if (indexPath.row >= carDataSource.count) {
            return;
        }
        CDVC.user_car_id = ((FWCarListModel *)carDataSource[indexPath.row]).user_car_id;
        [self.navigationController pushViewController:CDVC animated:YES];
    }
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (self.collectionScr == scrollView){
        
        float scrollViewX = scrollView.contentOffset.x;
        if (scrollViewX == 0) {
            [self headerBtnClick:userTopView.selectBar.firstButton];
        }else if (scrollViewX == SCREEN_WIDTH){
            [self headerBtnClick:userTopView.selectBar.secondButton];
        } else if (scrollViewX == 2*SCREEN_WIDTH){
            [self headerBtnClick:userTopView.selectBar.thirdButton];
        }
    }
    
    if (scrollView == rootScrollView) {
        
        if (scrollView.contentOffset.y > 60){
            alphaValue = 1;
        }else{
            alphaValue = (scrollView.contentOffset.y/60);
        }
        
        titleLable.alpha = alphaValue;
        headerView.alpha = alphaValue;
        naviBarView.alpha = alphaValue;
        
        if (scrollView.contentOffset.y >0) {
            titleLable.hidden = NO;
            
            userTopView.blackBackButton.hidden = YES;
            userTopView.blackMoreButton.hidden = YES;
            userTopView.blackShareButton.hidden = YES;
        }else{
            titleLable.hidden = YES;
            
            userTopView.blackBackButton.hidden = NO;
            userTopView.blackMoreButton.hidden = NO;
            userTopView.blackShareButton.hidden = NO;
        }
    }
    
    /**
     * 如果tableview上滑过程中，不松手。segement1会跟随一起动。这样体验不好，所以写一个segement2放在固定地方，当滚动到某一处是，让segement2显示出来。
     * 点击其中一个segement的某一个button的时，当他隐藏时，另一个segement在显示的同时，也要让光标和文字颜色显示成对应的 位置/颜色
     */
    if (scrollView == rootScrollView) {
        if (scrollView.contentOffset.y >= ScrollHeight-5) {
            userTopView.selectBar.honOneView.hidden = NO;
        }else if (scrollView.contentOffset.y < ScrollHeight-5){
            userTopView.selectBar.honOneView.hidden = YES;
        }
    }
    
    [thFollow followScrollViewScrollScrollViewScroll:scrollView];
}

#pragma mark -> 点击关注 & 推荐
- (void)headerBtnClick:(UIButton *)sender{
    
    if ([sender isEqual:userTopView.selectBar.firstButton]) {

        collectionType = FWAllCollectionType;

        userTopView.selectBar.firstButton.selected = YES;
        userTopView.selectBar.secondButton.selected =  NO;
        userTopView.selectBar.thirdButton.selected = NO;

        [allCollectionView reloadData];
        
        userTopView.selectBar.shadowView.frame = CGRectMake(CGRectGetMinX(userTopView.selectBar.firstButton.frame)+(CGRectGetWidth(userTopView.selectBar.firstButton.frame)-32)/2, CGRectGetMaxY(userTopView.selectBar.firstButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if([sender isEqual:userTopView.selectBar.secondButton]) {

        collectionType = FWVideoCollectionType;

        userTopView.selectBar.firstButton.selected = NO;
        userTopView.selectBar.secondButton.selected =  YES;
        userTopView.selectBar.thirdButton.selected = NO;

        if (isVideoRefresh) {
            isVideoRefresh = NO;
            [self requestVideoListWithLoadMoreData:NO];
        }else{
            [videoCollectionView reloadData];
        }
        
        userTopView.selectBar.shadowView.frame = CGRectMake(CGRectGetMinX(userTopView.selectBar.secondButton.frame)+(CGRectGetWidth(userTopView.selectBar.secondButton.frame)-32)/2, CGRectGetMaxY(userTopView.selectBar.secondButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if([sender isEqual:userTopView.selectBar.thirdButton]){

        userTopView.selectBar.firstButton.selected = NO;
        userTopView.selectBar.secondButton.selected =  NO;
        userTopView.selectBar.thirdButton.selected = YES;

        collectionType = FWCarTablevType;
        
        if (isCarRefresh) {
            isCarRefresh = NO;
            [self requsetMineData];
        }else{
            [carTableView reloadData];
        }
        
        userTopView.selectBar.shadowView.frame = CGRectMake(CGRectGetMinX(userTopView.selectBar.thirdButton.frame)+(CGRectGetWidth(userTopView.selectBar.thirdButton.frame)-32)/2, CGRectGetMaxY(userTopView.selectBar.thirdButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }
    
    NSInteger index = collectionType;

    //点击btn的时候scrollowView的contentSize发生变化
    [self.collectionScr setContentOffset:CGPointMake((index-1) *SCREEN_WIDTH, 0) animated:YES];
}


@end
