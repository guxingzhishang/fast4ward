//
//  FWUserCarViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUserCarViewController.h"
#import "FWMineRequest.h"
#import "FWMineCarListCell.h"
#import "FWCarDetailViewController.h"
#import "FWAddCarViewController.h"

static NSString * const carCellId = @"carCellId";

@interface FWUserCarViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray * carDataSource;
@property (nonatomic, strong) FWCarModel * carModel;
@property (nonatomic, strong) FWMineModel * mineModel;
@property (nonatomic, strong) UIView * footView;
@property (nonatomic, strong) UILabel * placeholderLabel;
@property (nonatomic, strong) UIButton * addButton;
@end

@implementation FWUserCarViewController
@synthesize carTableView;
@synthesize carDataSource;
@synthesize carModel;
@synthesize mineModel;

#pragma mark - > 请求个人数据
- (void)requsetMineData{
    
    if (!self.user_id) {
        return;
    }
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":self.user_id,
                              };
    
    [request startWithParameters:params WithAction:Get_user_page  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            mineModel = [FWMineModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.carTableView.mj_header endRefreshing];
            
            if (carDataSource.count > 0) {
                [carDataSource removeAllObjects];
            }
            [carDataSource addObjectsFromArray:mineModel.list_car];

            if (self.isMine) {
                if (mineModel.list_car.count >= 3) {
                    self.addButton.hidden = YES;

                    self.footView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 20);
                }else{
                    self.addButton.hidden = NO;

                    self.addButton.frame = CGRectMake(14,+10 , SCREEN_WIDTH-28, 49);
                    self.footView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 90);
                    self.carTableView.tableFooterView = self.footView;
                }
                self.carTableView.tableFooterView = self.footView;
            }else{
                if (mineModel.list_car.count > 0 ){
                    self.placeholderLabel.hidden = YES;
                }else{
                    self.placeholderLabel.hidden = NO;
                }
                self.footView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 150);
                self.carTableView.tableFooterView = self.footView;
            }
            

            [self.carTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self trackPageBegin:@"个人座驾页"];
    
    [self requsetMineData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"个人座驾页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    carDataSource = @[].mutableCopy;
    [self setupSubViews];
    [self requsetMineData];
}

- (void)setupSubViews{
    
    self.carTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    self.carTableView.delegate = self;
    self.carTableView.rowHeight = 163;
    self.carTableView.dataSource = self;
    self.carTableView.backgroundColor = FWTextColor_F8F8F8;
    self.carTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.carTableView];
    
    if (self.isMine) {
        [self.carTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(-49-FWSafeBottom);
        }];
        self.footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];
        
        self.addButton = [[UIButton alloc] init];
        self.addButton.layer.cornerRadius = 2;
        self.addButton.layer.masksToBounds = YES;
        self.addButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
        self.addButton.backgroundColor = FWViewBackgroundColor_EEEEEE;
        [self.addButton setTitle:@"+ 添加座驾" forState:UIControlStateNormal];
        [self.addButton setTitleColor:FWColor(@"515151") forState:UIControlStateNormal];
        [self.addButton addTarget:self action:@selector(addCarClick) forControlEvents:UIControlEventTouchUpInside];
        [self.footView addSubview:self.addButton];
        self.addButton.hidden = YES;
        
        self.addButton.frame = CGRectMake(14,10 , SCREEN_WIDTH-28, 49);
        self.carTableView.tableFooterView = self.footView;
    }else{
        
        self.carTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        self.footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];
        
        self.placeholderLabel = [[UILabel alloc] init];
        self.placeholderLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH, 150);
        self.placeholderLabel.text = @"暂无座驾~";
        self.placeholderLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
        self.placeholderLabel.textColor = FWTextColor_BCBCBC;
        self.placeholderLabel.textAlignment = NSTextAlignmentCenter;
        [self.footView addSubview:self.placeholderLabel];
        self.placeholderLabel.hidden = YES;
        
        self.carTableView.tableFooterView = self.footView;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.carDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWMineCarListCell * cell = [tableView dequeueReusableCellWithIdentifier:carCellId];
    
    if (nil == cell) {
        cell = [[FWMineCarListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:carCellId];
    }
    
    cell.contentView.backgroundColor =FWTextColor_F8F8F8;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row < carDataSource.count) {
        [cell configForCell:carDataSource[indexPath.row]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.mineModel.car_notice_status isEqualToString:@"1"]) {
        /* 座驾信息 */
        [[FWHudManager sharedManager] showErrorMessage:@"ta还没有完善座驾信息哦" toController:self];
        return;
    }else{
        
        FWCarDetailViewController * CDVC = [[FWCarDetailViewController alloc] init];
        if (indexPath.row >= carDataSource.count) {
            return;
        }
        CDVC.user_car_id = ((FWCarListModel *)carDataSource[indexPath.row]).user_car_id;
        [self.navigationController pushViewController:CDVC animated:YES];
    }
}

#pragma mark - > 添加座驾
- (void)addCarClick{
    
    FWAddCarViewController * ACVC = [[FWAddCarViewController alloc] init];
    ACVC.type = @"add";
    ACVC.isReFresh = ^(BOOL isRequest) {};
    [self.navigationController pushViewController:ACVC animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}
@end
