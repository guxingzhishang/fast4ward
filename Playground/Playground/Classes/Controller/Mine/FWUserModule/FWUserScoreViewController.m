//
//  FWUserScoreViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUserScoreViewController.h"
#import "FWPlayerArchivesHeaderView.h"
#import "FWPlayerArchivesCell.h"
#import "FWPlayerArchivesModel.h"
#import "FWPlayerInfoShareViewController.h"
#import "FWPlayerScoreDetailViewController.h"
#import "FWPlayerRankViewController.h"
#import "FWPlayerScoreDetailViewController.h"

@interface FWUserScoreViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWPlayerArchivesModel * archivesModel;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) FWUserSportScoreModel * rankModel;
@end

@implementation FWUserScoreViewController

- (void)requestData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if (!self.driver_id) {
        return;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"driver_id":self.driver_id,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_driver_rank_history WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.archivesModel = [FWPlayerArchivesModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self requestRank];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 获取04、02排名
- (void)requestRank{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"f_uid":self.f_uid,
    };
    
    [request startWithParameters:params WithAction:Get_user_sport_score WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.rankModel = [FWUserSportScoreModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self setupSubViews];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.driver_id.length > 0) {
        [self requestData];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.userInteractionEnabled = YES;
    [self setupSubViews];
}

- (void)setupSubViews{
    
    if (!self.mainView) {
        self.mainView = [[UIScrollView alloc] init];
    }
    self.mainView.clipsToBounds = YES;
    self.mainView.delegate = self;
    self.mainView.backgroundColor = FWTextColor_F8F8F8;
    [self.view addSubview:self.mainView];
    if (self.isMine) {
        [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(self.view).mas_offset(-49-FWSafeBottom);
        }];
    }else{
        [self.mainView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
        }];
    }

    for (UIView * view in self.bgView.subviews) {
        [view removeFromSuperview];
    }
    
    if (!self.bgView) {
         self.bgView = [[UIView alloc] init];
    }
    self.bgView.clipsToBounds = YES;
    self.bgView.userInteractionEnabled = YES;
    [self.mainView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.mainView);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(self.mainView);
    }];
    
    FWUserScoreRankView * scoreView = [[FWUserScoreRankView alloc] init];
    scoreView.vc = self;
    scoreView.userInteractionEnabled = YES;
    [self.bgView addSubview:scoreView];
    scoreView.frame = CGRectMake(0, 5, SCREEN_WIDTH, 15);
    
    
    if (self.rankModel.rank_list.type_04.index.length >0 ||
        self.rankModel.rank_list.type_02.index.length >0) {
        /* 04或02 有排名 */
        [scoreView configforView:self.rankModel.rank_list];
        scoreView.frame = CGRectMake(0, 5, SCREEN_WIDTH, 59);
    }else{
        scoreView.frame = CGRectMake(0, 5, SCREEN_WIDTH, 15);
    }
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWColorWihtAlpha(@"000000", 0.2);
    [self.bgView addSubview:lineView];
    lineView.frame = CGRectMake(20, CGRectGetMaxY(scoreView.frame)+15, 6, 5000);
    lineView.hidden = YES;
    
    CGFloat currentY = CGRectGetMaxY(scoreView.frame)-9;

    for (int i = 0; i < self.archivesModel.sport_list.count; i++) {
        
        FWPlayerSportModel * listModel = self.archivesModel.sport_list[i];
        
        UIImageView * iconImageView = [[UIImageView alloc] init];
        iconImageView.image = [UIImage imageNamed:@"Schedule_cycle"];
        iconImageView.frame = CGRectMake(12.5, currentY+18, 18, 18);
        [self.bgView addSubview:iconImageView];
        
        UILabel * yearLabel = [[UILabel alloc] init];
        yearLabel.text = listModel.year;
        yearLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 17];
        yearLabel.textColor = FWColorWihtAlpha(@"000000",0.4);
        yearLabel.textAlignment = NSTextAlignmentLeft;
        [self.bgView addSubview:yearLabel];
        [yearLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(iconImageView.mas_right).mas_offset(10);
            make.centerY.mas_equalTo(iconImageView);
            make.width.mas_greaterThanOrEqualTo(10);
            make.height.mas_equalTo(22);
        }];
        
        currentY = CGRectGetMaxY(iconImageView.frame) +12;
        
        for (int j = 0;j<listModel.list.count; j++){
            
            FWScoreDetailModel * subListModel = listModel.list[j];
            
            FWUserScoreView * scheduleView = [[FWUserScoreView alloc] init];
            scheduleView.vc = self;
            scheduleView.tag = 16666+i;
            scheduleView.userInteractionEnabled = YES;
            [scheduleView configForView:subListModel WithSection:i WithRow:j];
            scheduleView.frame = CGRectMake(30, currentY, SCREEN_WIDTH-44, 158);
            [self.bgView addSubview:scheduleView];
            [scheduleView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.bgView).mas_offset(30);
                make.width.mas_equalTo(SCREEN_WIDTH-44);
                make.top.mas_equalTo(self.bgView).mas_offset(currentY);
                make.height.mas_greaterThanOrEqualTo(158);
            }];

            
//            NSString * sport_id = [GFStaticData getObjectForKey:@"sport_id"];
//
//            if (i == 0 && j == 0) {
//                // 永远判断第一个
//                if (sport_id.length >0 && ![sport_id isEqualToString:subListModel.sport_id]) {
//
//                    scheduleView.nearestImageView.hidden = NO;
//                }else{
//                    if (sport_id.length <= 0) {
//                        scheduleView.nearestImageView.hidden = NO;
//                    }else{
//                        scheduleView.nearestImageView.hidden = YES;
//                    }
//                }
//            }
           
            if (i == self.archivesModel.sport_list.count-1 && j == listModel.list.count- 1) {
                [scheduleView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(self.bgView).mas_offset(30);
                    make.width.mas_equalTo(SCREEN_WIDTH-44);
                    make.top.mas_equalTo(self.bgView).mas_offset(currentY);
                    make.height.mas_greaterThanOrEqualTo(158);
                    make.bottom.mas_equalTo(self.bgView).mas_offset(-40);
                }];
                
                lineView.hidden = NO;
            }
            
            
            [self.bgView layoutIfNeeded];

            if(j == listModel.list.count - 1 ){
                currentY = CGRectGetMaxY(scheduleView.frame);
            }else{
                currentY = CGRectGetMaxY(scheduleView.frame)+10;
            }
        }
    }
    
    [self.bgView layoutIfNeeded];

    lineView.frame = CGRectMake(20, CGRectGetMaxY(scoreView.frame)+15, 3, CGRectGetHeight(self.bgView.frame)-40-36);
}

@end


@implementation FWUserScoreView
@synthesize subListModel;

- (id)init{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        [self setupSubviews];
    }
    
    return self;
}


- (void)scheduleViewClick{
    FWPlayerScoreDetailViewController * SDVC = [[FWPlayerScoreDetailViewController alloc] init];
    SDVC.ScoreModel = self.sportModel;
    [self.vc.navigationController pushViewController:SDVC animated:YES];
}


- (void)setupSubviews{
    
    self.shadowView = [[UIView alloc]init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 2;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 5);
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).mas_offset(10);
        make.left.mas_equalTo(self).mas_offset(0);
        make.right.mas_equalTo(self).mas_offset(0);
        make.height.mas_greaterThanOrEqualTo(158);
        make.width.mas_equalTo(SCREEN_WIDTH-44);
        make.bottom.mas_equalTo(self).mas_offset(-5);
    }];
    
    self.matchView = [[UIView alloc] init];
    self.matchView.userInteractionEnabled = YES;
    self.matchView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.matchView.layer.cornerRadius = 2;
    self.matchView.layer.masksToBounds = YES;
    [self.shadowView addSubview:self.matchView];
    [self.matchView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    [self.matchView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scheduleViewClick)]];

//    self.nearestImageView = [[UIImageView alloc]init];
//    self.nearestImageView.image = [UIImage imageNamed:@"match_new"];
//    [self.matchView addSubview:self.nearestImageView];
//    [self.nearestImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.top.right.mas_equalTo(self.matchView);
//        make.size.mas_equalTo(CGSizeMake(29, 29));
//    }];
//    self.nearestImageView.hidden = YES;
    
    self.typeImageView = [[UIImageView alloc] init];
    [self.matchView addSubview:self.typeImageView];
    [self.typeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(self.matchView).mas_offset(14);
       make.right.mas_equalTo(self.matchView).mas_offset(-24);
       make.size.mas_equalTo(CGSizeMake(28, 40));
    }];

    self.matchNameLabel = [[UILabel alloc] init];
    self.matchNameLabel.numberOfLines = 0;
    self.matchNameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.matchNameLabel.textColor = FWTextColor_222222;
    self.matchNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.matchNameLabel];
    [self.matchNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(20);
        make.left.mas_equalTo(self.matchView).mas_offset(14);
        make.top.mas_equalTo(self.matchView).mas_offset(13);
        make.right.mas_equalTo(self.typeImageView.mas_left).mas_offset(-10);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.groupNameLabel = [[UILabel alloc] init];
    self.groupNameLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 15];
    self.groupNameLabel.textColor = FWTextColor_222222;
    self.groupNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.groupNameLabel];
    [self.groupNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.matchNameLabel);
        make.top.mas_equalTo(self.matchNameLabel.mas_bottom).mas_offset(11);
        make.right.mas_equalTo(self.matchNameLabel);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.carLabel = [[UILabel alloc] init];
    self.carLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.carLabel.textColor = FWTextColor_BCBCBC;
    self.carLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.carLabel];
    [self.carLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.matchNameLabel);
        make.top.mas_equalTo(self.groupNameLabel.mas_bottom).mas_offset(12);
        make.right.mas_equalTo(self.matchView).mas_offset(-15);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
    [self.matchView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.matchNameLabel);
        make.right.mas_equalTo(self.matchView).mas_offset(-14);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.carLabel.mas_bottom).mas_offset(3);
    }];
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"入榜成绩:";
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    self.tipLabel.textColor = FWColor(@"515151");
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.matchView addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(self.matchNameLabel);
        make.top.mas_equalTo(self.lineView.mas_bottom).mas_offset(3);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    
    self.detailLabel = [[UILabel alloc] init];
    self.detailLabel.text = @"查看";
    self.detailLabel.layer.cornerRadius = 2;
    self.detailLabel.layer.masksToBounds = YES;
    self.detailLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.detailLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.detailLabel.backgroundColor = FWTextColor_222222;
    self.detailLabel.textAlignment = NSTextAlignmentCenter;
    [self.matchView addSubview:self.detailLabel];
    [self.detailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.matchView).mas_offset(-15);
        make.right.mas_equalTo(self.matchView).mas_offset(-14);
        make.size.mas_equalTo(CGSizeMake(51, 23));
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(5);
    }];
    
    self.scoreView = [[UIView alloc] init];
    self.scoreView.layer.cornerRadius = 2;
    self.scoreView.layer.masksToBounds = YES;
    self.scoreView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.matchView addSubview:self.scoreView];
    [self.scoreView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.detailLabel);
        make.right.mas_equalTo(self.detailLabel.mas_left).mas_offset(-5);
        make.height.mas_equalTo(23);
        make.left.mas_equalTo(self.matchNameLabel);
        make.width.mas_equalTo(SCREEN_WIDTH-130);
    }];
    
    self.ETLabel = [[UILabel alloc] init];
    self.ETLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.ETLabel.textColor = FWColor(@"515151");
    self.ETLabel.textAlignment = NSTextAlignmentLeft;
    [self.scoreView addSubview:self.ETLabel];
    [self.ETLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.scoreView);
        make.left.mas_equalTo(self.scoreView).mas_offset(0);
        make.height.mas_equalTo(self.scoreView);
        make.width.mas_equalTo((SCREEN_WIDTH-130)/3);
    }];
    
    
    self.RTLabel = [[UILabel alloc] init];
    self.RTLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.RTLabel.textColor = FWColor(@"515151");
    self.RTLabel.textAlignment = NSTextAlignmentRight;
    [self.scoreView addSubview:self.RTLabel];
    [self.RTLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.scoreView);
        make.right.mas_equalTo(self.scoreView).mas_offset(-4);
        make.height.mas_equalTo(self.scoreView);
        make.width.mas_equalTo(self.ETLabel);
    }];
    
    self.WeisuLabel = [[UILabel alloc] init];
    self.WeisuLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.WeisuLabel.textColor = FWColor(@"515151");
    self.WeisuLabel.textAlignment = NSTextAlignmentCenter;
    [self.scoreView addSubview:self.WeisuLabel];
    [self.WeisuLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.scoreView);
        make.left.mas_equalTo(self.ETLabel.mas_right).mas_offset(0);
        make.height.mas_equalTo(self.scoreView);
        make.right.mas_equalTo(self.RTLabel.mas_left).mas_offset(0);
        make.width.mas_greaterThanOrEqualTo(self.ETLabel);
    }];
}

- (void)configForView:(id)model WithSection:(NSInteger)section WithRow:(NSInteger)row{
    
    self.sectionIndex = section;
    self.rowIndex = row;
    
    self.sportModel = (FWScoreDetailModel *)model;
    
    self.matchNameLabel.text = self.sportModel.sport_name;
    self.groupNameLabel.text = self.sportModel.car_group;
    self.carLabel.text = [NSString stringWithFormat:@"车型：%@ %@/车队：%@",self.sportModel.car_brand,self.sportModel.car_type,self.sportModel.club_name];
    self.ETLabel.text = [NSString stringWithFormat:@" ET:%@",self.sportModel.et];
    self.WeisuLabel.text = [NSString stringWithFormat:@"尾速:%@",self.sportModel.vspeed];
    self.RTLabel.text = [NSString stringWithFormat:@"RT:%@",self.sportModel.rt];

    if ([self.sportModel.type isEqualToString:@"02"]) {
        self.typeImageView.image = [UIImage imageNamed:@"type_02"];
    }else{
        self.typeImageView.image = [UIImage imageNamed:@"type_04"];
    }
}
@end

@implementation FWUser04SportScoreModel
@end

@implementation FWUser02SportScoreModel
@end

@implementation FWUserSportScoreListModel
@end

@implementation FWUserSportScoreModel


@end

@implementation FWUserScoreRankView

- (id)init{
    self = [super init];
    self.backgroundColor = FWTextColor_F8F8F8;
    return self;
}

- (void)configforView:(FWUserSportScoreListModel *)model{
    
    self.listModel = (FWUserSportScoreListModel *)model;
    
    if (model.type_04.index.length >0) {
        /* 04有排名 */
        UIView * view_04 = [[UIView alloc] init];
        view_04.userInteractionEnabled = YES;
        view_04.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self addSubview:view_04];
        [view_04 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(14);
            make.top.mas_equalTo(self);
            make.height.mas_equalTo(59);
            make.width.mas_equalTo((SCREEN_WIDTH-38)/2);
        }];
        [view_04 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_04Click)]];

        
        UIImageView * imageView_04 = [[UIImageView alloc] init];
        imageView_04.image = [UIImage imageNamed:@"type_04"];
        [view_04 addSubview:imageView_04];
        [imageView_04 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(view_04);
            make.left.mas_equalTo(view_04).mas_offset(14);
            make.size.mas_equalTo(CGSizeMake(16, 23));
        }];
        
        UILabel * leftLabel_04 = [[UILabel alloc] init];
        leftLabel_04.text = @"04总榜";
        leftLabel_04.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        leftLabel_04.textColor = FWTextColor_252527;
        leftLabel_04.textAlignment = NSTextAlignmentLeft;
        [view_04 addSubview:leftLabel_04];
        [leftLabel_04 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(view_04);
            make.left.mas_equalTo(imageView_04.mas_right).mas_offset(10);
            make.height.mas_equalTo(23);
            make.width.mas_equalTo(55);
        }];
        
        UIImageView * right_04 = [[UIImageView alloc] init];
        right_04.image = [UIImage imageNamed:@"right_arrow"];
        [view_04 addSubview:right_04];
        [right_04 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(view_04);
            make.right.mas_equalTo(view_04).mas_offset(-14);
            make.size.mas_equalTo(CGSizeMake(5, 9));
        }];
        
        UILabel * rightLabel_04 = [[UILabel alloc] init];
        rightLabel_04.text = [NSString stringWithFormat:@"第%@名",model.type_04.index];
        rightLabel_04.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
        rightLabel_04.textColor = FWColor(@"#38A4F8");
        rightLabel_04.textAlignment = NSTextAlignmentRight;
        [view_04 addSubview:rightLabel_04];
        [rightLabel_04 mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(view_04);
            make.right.mas_equalTo(right_04.mas_left).mas_offset(-10);
            make.height.mas_equalTo(23);
            make.left.mas_equalTo(leftLabel_04.mas_right);
            make.width.mas_greaterThanOrEqualTo(10);
        }];
        
        if (model.type_02.index.length >0) {
            /* 且02有排名 */
            UIView * view_02 = [[UIView alloc] init];
            view_02.userInteractionEnabled = YES;
            view_02.backgroundColor = FWViewBackgroundColor_FFFFFF;
            [self addSubview:view_02];
            [view_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(view_04.mas_right).mas_offset(10);
                make.centerY.mas_equalTo(view_04);
                make.height.mas_equalTo(view_04);
                make.width.mas_equalTo(view_04);
            }];
            [view_02 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_02Click)]];

            UIImageView * imageView_02 = [[UIImageView alloc] init];
            imageView_02.image = [UIImage imageNamed:@"type_02"];
            [view_02 addSubview:imageView_02];
            [imageView_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(view_02);
                make.left.mas_equalTo(view_02).mas_offset(14);
                make.size.mas_equalTo(CGSizeMake(16, 23));
            }];
            
            UILabel * leftLabel_02 = [[UILabel alloc] init];
            leftLabel_02.text = @"02总榜";
            leftLabel_02.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
            leftLabel_02.textColor = FWTextColor_252527;
            leftLabel_02.textAlignment = NSTextAlignmentLeft;
            [view_02 addSubview:leftLabel_02];
            [leftLabel_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(view_02);
                make.left.mas_equalTo(imageView_02.mas_right).mas_offset(10);
                make.height.mas_equalTo(23);
                make.width.mas_equalTo(55);
            }];
            
            UIImageView * right_02 = [[UIImageView alloc] init];
            right_02.image = [UIImage imageNamed:@"right_arrow"];
            [view_02 addSubview:right_02];
            [right_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(view_02);
                make.right.mas_equalTo(view_02).mas_offset(-14);
                make.size.mas_equalTo(CGSizeMake(5, 9));
            }];
            
            UILabel * rightLabel_02 = [[UILabel alloc] init];
            rightLabel_02.text = [NSString stringWithFormat:@"第%@名",model.type_02.index];
            rightLabel_02.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
            rightLabel_02.textColor = FWColor(@"#38A4F8");
            rightLabel_02.textAlignment = NSTextAlignmentRight;
            [view_02 addSubview:rightLabel_02];
            [rightLabel_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(view_02);
                make.right.mas_equalTo(right_02.mas_left).mas_offset(-10);
                make.height.mas_equalTo(23);
                make.left.mas_equalTo(leftLabel_02.mas_right);
                make.width.mas_greaterThanOrEqualTo(10);
            }];
        }
    }else{
        /* 04没排名 */
        if (model.type_02.index.length >0) {
            /* 02有排名 */
            UIView * view_02 = [[UIView alloc] init];
            view_02.userInteractionEnabled = YES;
            view_02.backgroundColor = FWViewBackgroundColor_FFFFFF;
            [self addSubview:view_02];
            [view_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self).mas_offset(10);
                make.top.mas_equalTo(self);
                make.height.mas_equalTo(59);
                make.width.mas_equalTo((SCREEN_WIDTH-38)/2);
            }];
            [view_02 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_02Click)]];

            UIImageView * imageView_02 = [[UIImageView alloc] init];
            imageView_02.image = [UIImage imageNamed:@"type_02"];
            [view_02 addSubview:imageView_02];
            [imageView_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(view_02);
                make.left.mas_equalTo(view_02).mas_offset(14);
                make.size.mas_equalTo(CGSizeMake(16, 23));
            }];
            
            UILabel * leftLabel_02 = [[UILabel alloc] init];
            leftLabel_02.text = @"02总榜";
            leftLabel_02.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
            leftLabel_02.textColor = FWTextColor_252527;
            leftLabel_02.textAlignment = NSTextAlignmentLeft;
            [view_02 addSubview:leftLabel_02];
            [leftLabel_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(view_02);
                make.left.mas_equalTo(imageView_02.mas_right).mas_offset(10);
                make.height.mas_equalTo(23);
                make.width.mas_equalTo(55);
            }];
            
            UIImageView * right_02 = [[UIImageView alloc] init];
            right_02.image = [UIImage imageNamed:@"right_arrow"];
            [view_02 addSubview:right_02];
            [right_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(view_02);
                make.right.mas_equalTo(view_02).mas_offset(-14);
                make.size.mas_equalTo(CGSizeMake(5, 9));
            }];
            
            UILabel * rightLabel_02 = [[UILabel alloc] init];
            rightLabel_02.text = [NSString stringWithFormat:@"第%@名",model.type_02.index];
            rightLabel_02.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
            rightLabel_02.textColor = FWColor(@"#38A4F8");
            rightLabel_02.textAlignment = NSTextAlignmentRight;
            [view_02 addSubview:rightLabel_02];
            [rightLabel_02 mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(view_02);
                make.right.mas_equalTo(right_02.mas_left).mas_offset(-10);
                make.height.mas_equalTo(23);
                make.left.mas_equalTo(leftLabel_02.mas_right);
                make.width.mas_greaterThanOrEqualTo(10);
            }];
        }
    }
}

- (void)view_04Click{
    
    FWPlayerRankViewController * LVC = [[FWPlayerRankViewController alloc] init];
    LVC.selectIndex = 1;
    [self.vc.navigationController pushViewController:LVC animated:YES];
}

- (void)view_02Click{
    FWPlayerRankViewController * LVC = [[FWPlayerRankViewController alloc] init];
    LVC.selectIndex = 2;
    [self.vc.navigationController pushViewController:LVC animated:YES];
}
@end
