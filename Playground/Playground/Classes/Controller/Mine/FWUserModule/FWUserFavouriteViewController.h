//
//  FWUserFavouriteViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 用户页 - > 收藏
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWUserFavouriteViewController : FWBaseViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>

@property(nonatomic,strong)FWCollectionView* collectionViewLabel;
@property (nonatomic, assign) BOOL isMine;
@property (nonatomic, strong) NSString * user_id;

- (void)requestAllListWithLoadMoreData:(BOOL)isLoadMoredData;

@end

NS_ASSUME_NONNULL_END
