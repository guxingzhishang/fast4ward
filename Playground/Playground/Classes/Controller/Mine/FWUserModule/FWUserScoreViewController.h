//
//  FWUserScoreViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 个人中心 - 成绩
 */
#import "FWBaseViewController.h"
#import "FWAllScheduleModel.h"

#import "FWPlayerArchivesModel.h"
#import "FWPlayerScoreDetailViewController.h"
#import "FWPlayerRankModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface FWUserScoreViewController : FWBaseViewController<UITableViewDataSource ,UITableViewDelegate>

@property (nonatomic, assign) BOOL isMine;

@property (nonatomic, strong) UIScrollView * mainView;
@property (nonatomic, strong) NSString * driver_id;
@property (nonatomic, strong) NSString * f_uid;

- (void)requestData;

@end


@interface FWUserScoreView : UIView

@property (nonatomic, strong) UIView *shadowView;
@property (nonatomic, strong) UIView * matchView;
@property (nonatomic, strong) UIImageView * nearestImageView;
@property (nonatomic, strong) UIImageView * typeImageView;
@property (nonatomic, strong) UILabel * matchNameLabel;
@property (nonatomic, strong) UILabel * groupNameLabel;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UILabel * carLabel;
@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UILabel * ETLabel;
@property (nonatomic, strong) UILabel * WeisuLabel;
@property (nonatomic, strong) UILabel * RTLabel;

@property (nonatomic, strong) UIView * scoreView;
@property (nonatomic, strong) UILabel * detailLabel;

@property (nonatomic, strong) FWScheduleListModel * subListModel;
@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, assign) CGFloat  sectionIndex;
@property (nonatomic, assign) CGFloat  rowIndex;

@property (nonatomic, strong) FWScoreDetailModel * sportModel;

- (void)configForView:(id)model WithSection:(NSInteger)section WithRow:(NSInteger)row;
@end


@interface FWUser04SportScoreModel : NSObject
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * index;
@end

@interface FWUser02SportScoreModel :NSObject
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * index;
@end

@interface FWUserSportScoreListModel :NSObject
@property (nonatomic, strong) FWUser04SportScoreModel * type_04;
@property (nonatomic, strong) FWUser02SportScoreModel * type_02;
@end

@interface FWUserSportScoreModel :NSObject
@property (nonatomic, strong) FWUserSportScoreListModel * rank_list;
@end


@interface FWUserScoreRankView : UIView
@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic, strong) FWUserSportScoreListModel * listModel;
- (void)configforView:(FWUserSportScoreListModel *)model;
@end
NS_ASSUME_NONNULL_END
