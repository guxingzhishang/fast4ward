//
//  FWNewMineViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWNewMineViewController : FWBaseViewController
@property (nonatomic, strong) NSString * user_id;

@end

NS_ASSUME_NONNULL_END
