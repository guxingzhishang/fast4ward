//
//  FWUserFavouriteViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUserFavouriteViewController.h"
#import "FWMineRequest.h"
#import "FWNewCollectionCell.h"

#import "ZLCollectionViewVerticalLayout.h"
#import "FWUserFeedListHeaderView.h"
#import "FWNewCollectionCell.h"

static NSString * const allCellId = @"allCellId";
#define ARLayoutCellInvalidateValue [NSValue valueWithCGSize:CGSizeZero]

@interface FWUserFavouriteViewController ()
<UICollectionViewDelegate,UICollectionViewDataSource,ZLCollectionViewBaseFlowLayoutDelegate,FWUserFeedListHeaderViewDelegate>

@property (nonatomic, strong) FWFeedModel * allFeedModel;
@property (nonatomic, strong) NSMutableArray * allDataSource;
@property (nonatomic, assign) NSInteger   allPageNum;

@property (nonatomic, assign) NSInteger currentType;
@property (nonatomic, strong) NSMutableDictionary * sizeCacheDict;

@end

@implementation FWUserFavouriteViewController
@synthesize allFeedModel;
@synthesize allDataSource;
@synthesize allPageNum;

#pragma mark - > 获取视频列表信息
- (void)requestAllListWithLoadMoreData:(BOOL)isLoadMoredData{

    if (!self.user_id) {
        return;
    }
    if (isLoadMoredData == NO) {
        allPageNum = 1;

        if (allDataSource.count > 0 ) {
            [allDataSource removeAllObjects];
        }
    }else{
        allPageNum += 1;
    }

    FWMineRequest * request = [[FWMineRequest alloc] init];

    NSDictionary * param = @{
                            @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                            @"watermark":@"1",
                            @"feed_type":@(self.currentType).stringValue,
                            @"page":@(self.allPageNum).stringValue,
                            @"pagesize":@"20",
                            };
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_favourite_feed_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if (isLoadMoredData) {
                [self.collectionViewLabel.mj_footer endRefreshing];
            }else{
                [self.collectionViewLabel.mj_header endRefreshing];
            }

            allFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            [allDataSource addObjectsFromArray: allFeedModel.feed_list];

            if([allFeedModel.feed_list count] == 0 &&
               allPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.collectionViewLabel reloadData];
                
                if (!isLoadMoredData) {
                    // 下拉刷新，或者第一次请求，直接滚动到顶部
                    [self.collectionViewLabel scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                }
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    [self trackPageBegin:@"用户帖子列表页"];

    [self requestVisitorWithUserID:self.user_id WithType:@"1" WithID:@"0"];

    if ([GFStaticData getObjectForKey:Delete_Work_FeedID]) {

        int tempNum = -1;
        for (int i = 0; i < allDataSource.count; i++) {
            FWFeedListModel * tempModel = allDataSource[i];

            if ([tempModel.feed_id isEqualToString:[GFStaticData getObjectForKey:Delete_Work_FeedID]]) {
                tempNum = i;
            }
        }

        if (tempNum >=0) {
            [allDataSource removeObjectAtIndex:tempNum];
            [self.collectionViewLabel reloadData];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];

    [self trackPageEnd:@"用户帖子列表页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;

    allDataSource = @[].mutableCopy;
    allPageNum = 0;
    self.currentType = 0;

    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    [self.view addSubview:self.collectionViewLabel];
    
    if (self.isMine) {
        [self.collectionViewLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(self.view);
            make.bottom.mas_equalTo(self.view).mas_offset(-49-FWSafeBottom);
        }];
    }else{
        [self.collectionViewLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
        }];
    }
    
    [self requestAllListWithLoadMoreData:NO];

    DHWeakSelf;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (!allFeedModel) {
            return ;
        }
        
        if ([allFeedModel.has_more isEqualToString:@"1"]) {
            self.collectionViewLabel.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
                [weakSelf requestAllListWithLoadMoreData:NO];
            }];
            
            self.collectionViewLabel.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
                [weakSelf requestAllListWithLoadMoreData:YES];
            }];
        }
    });
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.allDataSource.count;
}

- (ZLLayoutType)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout *)collectionViewLayout typeOfLayout:(NSInteger)section {
    return ClosedLayout;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWNewCollectionCell* cell = [FWNewCollectionCell cellWithCollectionView:collectionView forIndexPath:indexPath];
    cell.viewController = self;
    
    if (allDataSource.count > 0) {

        FWFeedListModel * model = allDataSource[indexPath.item];
        [cell configForCell:model];

        if (model.h5_url.length > 0 ) {

            cell.activityImageView.hidden = NO;

            NSString * imageName = @"activity";
            CGSize imageViewSize = CGSizeMake(30, 15);

            cell.activityImageView.image = [UIImage imageNamed:imageName];
            [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                make.size.mas_equalTo(imageViewSize);
            }];
        }else{
            cell.activityImageView.hidden = YES;
        }
    }else{
        cell.iv.hidden = YES;
        cell.userIcon.hidden = YES;
        cell.pvImageView.hidden = YES;
        cell.authenticationImageView.hidden = YES;
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray * tempArr = allDataSource;

    FWFeedListModel * model = tempArr[indexPath.row];
    NSMutableArray * tempDataSource = @[].mutableCopy;

    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }

    if ([model.feed_type isEqualToString:@"1"]) {

        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.listModel = model;
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {};
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            for (NSInteger i = indexPath.row; i<tempArr.count; i++) {
                FWFeedListModel * model = tempArr[i];

                if ([model.feed_type isEqualToString:@"1"]) {
                    [tempDataSource addObject:model];
                }
            }

            // 视频流 由于筛选过，所以每次点击的都是数据源第一个。
            FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
            controller.currentIndex = 0;
            controller.listModel = tempDataSource;
            controller.base_uid = self.user_id;
            controller.requestType = FWFavouriteListRequestType;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {

        for (FWFeedListModel * model in tempArr) {
            if ([model.feed_type isEqualToString:@"2"]) {
                [tempDataSource addObject:model];
            }
        }

        // 图文详情
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.listModel = model;
        TVC.feed_id = model.feed_id;
        TVC.myBlock = ^(FWFeedListModel *awemeModel) {};
        [self.navigationController pushViewController:TVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {

        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"4"]) {
        // 问答
        FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        // 改装
        FWRefitCaseDetailViewController * RCDVC = [[FWRefitCaseDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"6"]) {
        // 闲置
        FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }
}

//如果是百分比布局必须实现该代理，设置每个item的百分比，如果没实现默认比例为1
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout*)collectionViewLayout percentOfRow:(NSIndexPath*)indexPath; {
    return 3.0/5;
}

//如果是ClosedLayout样式的section，必须实现该代理，指定列数
- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout*)collectionViewLayout columnCountOfSection:(NSInteger)section {
    return 2;
}

//如果是绝对定位布局必须是否该代理，设置每个item的frame
- (CGRect)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout*)collectionViewLayout rectOfItem:(NSIndexPath*)indexPath {
    return CGRectZero;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout *)collectionViewLayout zIndexOfItem:(NSIndexPath*)indexPath {
    return 0;
}

- (CATransform3D)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout *)collectionViewLayout transformOfItem:(NSIndexPath*)indexPath {
    return CATransform3DIdentity;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString : UICollectionElementKindSectionHeader]){
        
        if (allFeedModel.feed_list.count > 0) {
            FWUserFeedListHeaderView* headerView = [FWUserFeedListHeaderView headerViewWithCollectionView:collectionView forIndexPath:indexPath];
            headerView.delegate = self;
            [headerView setupSubViews];
            [headerView setButtonText:allFeedModel withType:@"2"];
            return headerView;
        }else{
            FWUserFeedListHeaderView* headerView = [FWUserFeedListHeaderView headerViewWithCollectionView:collectionView forIndexPath:indexPath];
            [headerView setupSubViews];
            [headerView setButtonText:allFeedModel withType:@"2"];

            
            UILabel * placeholderLabel = [[UILabel alloc] init];
            placeholderLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
            placeholderLabel.textColor = FWTextColor_BCBCBC;
            placeholderLabel.textAlignment = NSTextAlignmentCenter;
            placeholderLabel.text = @"暂无收藏的帖子~";
            placeholderLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH, 200);
            [headerView addSubview:placeholderLabel];
            return headerView;
        }
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (allFeedModel.feed_list.count <= 0 && self.allPageNum == 1) {
        return CGSizeMake(SCREEN_WIDTH, 100);
    }else{
        return CGSizeMake(SCREEN_WIDTH, 45);
    }
}

- (NSString*)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout *)collectionViewLayout registerBackView:(NSInteger)section {
    return @"";
}

- (void)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout *)collectionViewLayout loadView:(NSInteger)section {
    //NSLog(@"当前section=%zd，需要处理什么操作？",section);
}

- (UIColor*)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout *)collectionViewLayout backColorForSection:(NSInteger)section {
    return FWTextColor_F8F8F8;
}

- (BOOL)collectionView:(UICollectionView *)collectionView layout:(ZLCollectionViewBaseFlowLayout *)collectionViewLayout attachToTop:(NSInteger)section {
    return YES;
}

- (UICollectionView*)collectionViewLabel {
    if (!_collectionViewLabel) {
        ZLCollectionViewVerticalLayout *flowLayout = [[ZLCollectionViewVerticalLayout alloc] init];
        flowLayout.delegate = self;
        flowLayout.canDrag = NO;
        flowLayout.header_suspension = YES;

        _collectionViewLabel = [[FWCollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        _collectionViewLabel.dataSource = self;
        _collectionViewLabel.delegate = self;
        _collectionViewLabel.backgroundColor = FWTextColor_F8F8F8;
        [_collectionViewLabel registerClass:[FWNewCollectionCell class] forCellWithReuseIdentifier:[FWNewCollectionCell cellIdentifier]];
        [_collectionViewLabel registerClass:[FWUserFeedListHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[FWUserFeedListHeaderView headerViewIdentifier]];
    }
    return _collectionViewLabel;
}

- (void)reloadFeedList:(NSInteger)index{
    self.currentType = index;
    [self requestAllListWithLoadMoreData:NO];
}

#pragma mark - > 判断当前item高度是否被缓存
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    NSMutableArray * tempArr = allDataSource;

    if (indexPath.item >= tempArr.count) {
       return CGSizeZero;
    }

    FWFeedListModel * model = tempArr[indexPath.row];
    
//    BOOL hasCache = [self hasCacheWithFeedID:model.feed_id];
//    if (hasCache) {
//        if (![[self sizeCacheWithFeedID:model.feed_id]
//              isEqualToValue:ARLayoutCellInvalidateValue]) {
//            return [[self sizeCacheWithFeedID:model.feed_id] CGSizeValue];
//        }
//    }

    /* 没缓存，进行计算 */
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;

    if ([model.feed_type isEqualToString:@"2"]) {
        // 图文贴 固定比例3：4
        height = (cover_width *4)/3;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if([model.feed_type isEqualToString:@"3"]){
        // 文章帖 固定比例5：4
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if ([model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 20;
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 25;
        }
    }else if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];

            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }

    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }

    CGSize size = CGSizeMake((SCREEN_WIDTH-38)/2, height + textHeight + 40) ;
//    [self.sizeCacheDict setValue:[NSValue valueWithCGSize:size] forKey:model.feed_id];
    
    return size;
}

- (NSMutableDictionary *)sizeCacheDict{
    
    if (!_sizeCacheDict) {
        _sizeCacheDict = [[NSMutableDictionary alloc] init];
    }
    return _sizeCacheDict;
}

- (BOOL)hasCacheWithFeedID:(NSString *)feed_id {
    BOOL hasCache = NO;

    if ([[self.sizeCacheDict allKeys] indexOfObject:feed_id] != NSNotFound) {
        if (![self.sizeCacheDict[feed_id] isEqualToValue:ARLayoutCellInvalidateValue]) {
            hasCache = YES;
        }
    }
    return hasCache;
}

- (NSValue *)sizeCacheWithFeedID:(NSString *)feed_id {
    NSValue *sizeValue = self.sizeCacheDict[feed_id];
    return sizeValue;
}

@end
