//
//  FWUserInfoViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/17.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 用户个人信息页
 */

#import "FWBaseViewController.h"
#import "FWUserTopView.h"
#import "FWScrollViewFollowCollectionView.h"
#import "FWMainTouchScrollView.h"

typedef NS_ENUM(NSUInteger, FWMineCollectionViewType) {
    FWAllCollectionType = 1,// 所有列表
    FWVideoCollectionType ,  // 视频列表
    FWCarTablevType ,// 座驾列表
};

@interface FWUserInfoViewController : FWBaseViewController<FWUserTopViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FWBaseCollectionLayoutDalegate,UIGestureRecognizerDelegate>

/*
 * 承载含有ceollectionview的Scrollview 和header 的Scrollview
 */
@property (nonatomic, strong) UIScrollView * backGroundScr;

/*
 * 承载collectionView的背景Scrollview
 */
@property (nonatomic, strong) UIScrollView * collectionScr;

/**
 * 全部列表
 */
@property (nonatomic, strong) FWCollectionView * allCollectionView;

/**
 * 视频列表
 */
@property (nonatomic, strong) FWCollectionView * videoCollectionView;

/**
 * 座驾列表
 */
@property (nonatomic, strong) FWTableView * carTableView;

/**
 * 标记是哪一个列表
 */
@property (nonatomic, assign) FWMineCollectionViewType collectionType;

/**
 * 返回按钮
 */
@property (nonatomic, strong) UIButton * backButton;

/**
 * 黑色返回按钮
 */
@property (nonatomic, strong) UIButton * blackBackButton;

/**
 * 分享
 */
@property (nonatomic, strong) UIButton * blackShareButton;

/**
 * 更多
 */
@property (nonatomic, strong) UIButton * blackMoreButton;

/* 私聊按钮 */
@property (nonatomic, strong) UIButton * chatButton;

@property (nonatomic, strong) NSArray *modelArr;

@property (nonatomic, strong) NSString * user_id;

@property (nonatomic, strong) NSString * secondString;

@property (nonatomic, strong) FWMineModel * mineModel;

@property (nonatomic, strong) FWBaseCollectionLayout *allLayout;
@property (nonatomic, strong) FWBaseCollectionLayout *videoLayout;

@end
