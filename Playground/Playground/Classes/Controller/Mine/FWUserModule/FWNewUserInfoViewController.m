//
//  FWNewUserInfoViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWNewUserInfoViewController.h"
#import "UIView+YNPageExtend.h"
#import "FWUserFeedListViewController.h"
#import "FWUserCarViewController.h"
#import "FWUserScoreViewController.h"
#import "FWNewUserHeaderView.h"
#import "YNPageViewController.h"
#import "FWMineRequest.h"
#import "FWMineModel.h"
#import "FWMyGoldenViewController.h"
#import "FWMatchPicsViewController.h"
#import "FWUserFavouriteViewController.h"
#import "FWReportViewController.h"
#import "FWFeedBackRequest.h"
#import "FWConversationViewController.h"

#define kOpenRefreshHeaderViewHeight 0

@interface FWNewUserInfoViewController ()<FWNewUserHeaderViewDelegate,YNPageViewControllerDataSource, YNPageViewControllerDelegate, SDCycleScrollViewDelegate,SDCycleScrollViewDelegate>
{
    UIImageView * naviBarView;
    UILabel * titleLable;
    UIButton * messageButton;
    UILabel * unReadLabel;

    FWMineModel * mineModel;
    YNPageViewController *vc;
    UIButton * chatButton;
    CGFloat alphaValue;
}

@property (nonatomic, strong) NSString * secondString;

@property (nonatomic, assign) CGFloat alphaValue;
@property (nonatomic, strong) UIView * naviBarView;

@property (nonatomic, strong) UIButton * leftButton;
@property (nonatomic, strong) UIButton * signButton;
@property (nonatomic, strong) UIButton * shareButton;

@property (nonatomic, strong) FWSettingModel * settingModel;
@property (nonatomic, strong) FWNewUserHeaderView * userHeadView;
@end

@implementation FWNewUserInfoViewController
@synthesize secondString;

#pragma mark - > 请求融云iM
- (void)requestRongYunTokenIM{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_im_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:userId];
                                            [RCIM sharedRCIM].currentUserInfo = userInfo;
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"IM登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            NSLog(@"IMtoken错误");
                                        }];
        }
    }];
}

#pragma mark - > 请求个人数据
- (void)requsetMineData{
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":self.user_id,
    };
    
    [request startWithParameters:params WithAction:Get_user_page  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            mineModel = [FWMineModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            [self.userHeadView configForView:mineModel];
            self.userHeadView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.userHeadView.currentHeight);
            vc.headerView = self.userHeadView;
            
            [vc reloadSuspendHeaderViewFrame];

            if ([mineModel.block_status isEqualToString:@"1"]) {
                secondString = @"取消屏蔽";
            }else{
                secondString = @"屏蔽";
            }
            
            if ([mineModel.user_info.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
                self.signButton.hidden = YES;
            }else{
                self.signButton.hidden = NO;
            }
            if (mineModel.user_info.driver_id.length <= 0 ||
                [mineModel.user_info.driver_id integerValue] == 0) {
                [vc removePageControllerWithTitle:@"成绩"];
                [vc updateMenuItemTitles:@[@"帖子", @"座驾"]];
            }
            
            [vc reloadSuspendHeaderViewFrame];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = FWTextColor_F8F8F8;
    [self trackPageBegin:@"个人中心页"];
    
    [self requestRongYunTokenIM];
    [self requsetMineData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"个人中心页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_interactivePopDisabled = YES;
    self.fd_prefersNavigationBarHidden = YES;
    
    [self setupNavigation];
    [self setupPageVC];
}

- (void)setupNavigation{
    
    self.naviBarView = [[UIView alloc] init];
    self.naviBarView.backgroundColor = FWClearColor;
    self.naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    [self.view addSubview:self.naviBarView];
//    self.naviBarView.alpha = 0;
        
    self.leftButton = [[UIButton alloc] init];
    [self.leftButton setImage:[UIImage imageNamed:@"new_black_back"] forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.leftButton];
    [self.leftButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.naviBarView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(self.naviBarView).mas_offset(30+FWCustomeSafeTop);
    }];
    
    self.shareButton = [[UIButton alloc] init];
    [self.shareButton setImage:[UIImage imageNamed:@"new_black_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.shareButton];

    
    self.signButton = [[UIButton alloc] init];
    [self.signButton setImage:[UIImage imageNamed:@"new_black_more"] forState:UIControlStateNormal];
    [self.signButton addTarget:self action:@selector(signButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.signButton];
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        self.shareButton.hidden = NO;
        
        [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.naviBarView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.leftButton).mas_offset(1);
        }];

        [self.signButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.shareButton.mas_left).mas_offset(-10);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.leftButton);
        }];
    }else{
        self.shareButton.hidden = YES;
        
        [self.signButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.naviBarView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.leftButton);
        }];
    }
    
    
//    chatButton = [[UIButton alloc] init];
//    [chatButton setImage:[UIImage imageNamed:@"private_chat"] forState:UIControlStateNormal];
//    [chatButton addTarget:self action:@selector(chatButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:chatButton];
//    [self.view bringSubviewToFront:chatButton];
//    [chatButton mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self.view).mas_offset(-4);
//        make.size.mas_equalTo(CGSizeMake(52, 52));
//        make.bottom.mas_equalTo(self.view).mas_offset(-89-FWSafeBottom);
//    }];
//
//    if ([self.user_id isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
//        chatButton.hidden = YES;
//    }else{
//        chatButton.hidden = NO;
//    }
}

#pragma mark - > 私聊
- (void)chatButtonOnClick{
    
    [self cheackLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        
        FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
        if ([usvm.userModel.user_level intValue] > 0) {
            
            FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:self.user_id];
            [self.navigationController pushViewController:CVC animated:YES];
        }else{
            [self.view endEditing:YES];
    
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"会员可享受“私信”权益！" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"我要发私信" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    FWVIPWebViewController * VVC = [[FWVIPWebViewController alloc] init];
                    VVC.webTitle = @"会员权益";
                    [self.navigationController pushViewController:VVC animated:YES];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
    }
}
- (void)setupPageVC {
    
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleSuspensionCenter;
    configration.headerViewCouldScale = YES;
    configration.pageScrollEnabled = NO;
    /// 控制tabbar 和 nav
    configration.showTabbar = NO;
    configration.showNavigation = NO;
    configration.scrollMenu = NO;
    configration.aligmentModeCenter = NO;
    configration.lineWidthEqualFontWidth = YES;
    configration.showBottomLine = YES;
    /// 设置悬浮停顿偏移量
    configration.suspenOffsetY = kYNPAGE_NAVHEIGHT;
    
    vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs  titles:[self getArrayTitles] config:configration];
    vc.dataSource = self;
    vc.delegate = self;
    
    self.userHeadView = [[FWNewUserHeaderView alloc] init];
    self.userHeadView.vc = self;
    self.userHeadView.delegate = self;
    self.userHeadView.frame = CGRectMake(0, 0, SCREEN_WIDTH,451);
    
    vc.headerView = self.userHeadView;
    vc.pageIndex = 0;
    [vc addSelfToParentViewController:self];
    
    [self.view bringSubviewToFront:self.naviBarView];
//    [self.view bringSubviewToFront:chatButton];
}

- (NSArray *)getArrayVCs {
    FWUserFeedListViewController *firstVC = [[FWUserFeedListViewController alloc] init];
    firstVC.user_id = self.user_id;
    firstVC.isMine = NO;

    FWUserCarViewController *secondVC = [[FWUserCarViewController alloc] init];
    secondVC.user_id = self.user_id;
    secondVC.isMine = NO;

    FWUserScoreViewController * thirdVC= [[FWUserScoreViewController alloc] init];
    thirdVC.driver_id = mineModel.user_info.driver_id;
    thirdVC.f_uid = self.user_id;
    thirdVC.isMine = NO;

    return @[firstVC, secondVC, thirdVC];
}

- (NSArray *)getArrayTitles {
    return @[@"帖子", @"座驾", @"成绩"];
}

#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    UIViewController *vc = pageViewController.controllersM[index];
    
    if (index == 0) {
        return [(FWUserFeedListViewController *)vc collectionViewLabel];
    }else if (index == 1) {
        return [(FWUserCarViewController *)vc carTableView];
    }else if (index == 2){
        return [(FWUserScoreViewController *)vc mainView];
    }
    
    return [(FWUserScoreViewController *)vc mainView];
}

#pragma mark - YNPageViewControllerDelegate
- (void)pageViewController:(YNPageViewController *)pageViewController
            contentOffsetY:(CGFloat)contentOffset
                  progress:(CGFloat)progress {
//    NSLog(@"--- contentOffset = %f,    progress = %f", contentOffset, progress);
    
    self.naviBarView.backgroundColor = [UIColor colorWithRed:(246)/255.0 green:(246)/255.0 blue:(246)/255.0 alpha:progress];

    self.naviBarView.alpha = progress;
    
    if (progress >0.05) {
        
        self.leftButton.hidden = NO;
        self.shareButton.hidden = NO;
        self.signButton.hidden = NO;

        self.userHeadView.settingButton.hidden = YES;
        self.userHeadView.shareButton.hidden = YES;
        self.userHeadView.signButton.hidden = YES;
        
        if ([mineModel.user_info.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            self.signButton.hidden = YES;
            self.userHeadView.signButton.hidden = YES;
        }
    }else{
        self.leftButton.hidden = YES;
        self.shareButton.hidden = YES;
        self.signButton.hidden = YES;
        self.userHeadView.settingButton.hidden = NO;
        self.userHeadView.signButton.hidden = NO;
        
        if ([mineModel.user_info.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            self.signButton.hidden = YES;
            self.userHeadView.signButton.hidden = YES;
        }
        if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
            self.userHeadView.shareButton.hidden = NO;
        }else{
            self.userHeadView.shareButton.hidden = YES;
        }
    }
}

- (void)pageViewController:(YNPageViewController *)pageViewController didScroll:(UIScrollView *)scrollView progress:(CGFloat)progress
 formIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
    
    FWBaseViewController * tovc = (FWBaseViewController *)pageViewController.controllersM[toIndex];

    [vc.bgScrollView scrollToTop];
    
    if (toIndex == 2) {
        FWUserScoreViewController *svc = (FWUserScoreViewController *)tovc;

        if (svc.driver_id.length <= 0) {
            svc.driver_id = mineModel.user_info.driver_id;
            [svc requestData];
        }
    }
}

#pragma mark - > 分享
- (void)shareButtonClick{
    
    ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
    [shareView setupUserQRViewWithModel:mineModel];
    [shareView showView];
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0){
            
            [[ShareManager defaultShareManager] userShareToMinProgramObjectWithModel:mineModel];
        }else if (index == 1){
            
            UIImageView * imageView = [[ShareView defaultShareView] getCurrenImage];
            [[ShareManager defaultShareManager] shareToPengyouquan:imageView WithFeedID:mineModel.user_info.uid WithType:Share_uid];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}

#pragma mark - > 更多
- (void)signButtonClick{
        
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]) {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"您可以进行以下操作" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            FWReportViewController * RVC = [[FWReportViewController alloc] init];
            RVC.f_uid = mineModel.user_info.uid;
            [self.navigationController pushViewController:RVC animated:YES];
        }];
        UIAlertAction *selectAction = [UIAlertAction actionWithTitle:secondString style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            if([mineModel.block_status isEqualToString:@"1"]){
                // 取消屏蔽
                [self requestFeedBackWithBlockStatus:@"2"];
            }else{
                // 屏蔽，二次确认
                [self secondMakeSure];
            }
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alertController addAction:takePhotoAction];
        [alertController addAction:selectAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self cheackLogin];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 屏蔽二次确认
- (void)secondMakeSure{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"屏蔽此人后，TA的视频不会再推荐给你。确定要屏蔽吗？" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self requestFeedBackWithBlockStatus:@"1"];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 屏蔽
- (void)requestFeedBackWithBlockStatus:(NSString *)block_status{
    
    FWFeedBackRequest * request = [[FWFeedBackRequest alloc] init];
    
    NSDictionary * dict = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                            @"feed_id":@"0",
                            @"content":secondString,
                            @"f_uid":mineModel.user_info.uid,
                            @"block_status":block_status,
                            };
    
    [request startWithParameters:dict WithAction:Submit_feedback  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        if ([code isEqualToString:NetRespondCodeSuccess]){
            
            if([block_status isEqualToString:@"1"]){
                [[FWHudManager sharedManager] showSuccessMessage:@"已加入黑名单" toController:self];
            }else {
                [[FWHudManager sharedManager] showSuccessMessage:@"已解除屏蔽" toController:self];
            }
            [self requsetMineData];
        }
    }];
}

#pragma mark - > 返回
- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)shareOnClick{
    [self shareButtonClick];
}

- (void)moreOnClick{
    [self signButtonClick];
}
@end
