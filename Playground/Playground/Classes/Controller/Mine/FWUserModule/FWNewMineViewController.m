//
//  FWNewMineViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWNewMineViewController.h"
#import "UIView+YNPageExtend.h"
#import "FWUserFeedListViewController.h"
#import "FWUserCarViewController.h"
#import "FWUserScoreViewController.h"
#import "FWNewMineView.h"
#import "YNPageViewController.h"
#import "FWMineRequest.h"
#import "FWMineModel.h"
#import "FWMyGoldenViewController.h"
#import "FWMatchPicsViewController.h"
#import "FWUserFavouriteViewController.h"
#import "FWSettingViewController.h"

#define kOpenRefreshHeaderViewHeight 0
@interface FWNewMineViewController ()<YNPageViewControllerDataSource, YNPageViewControllerDelegate,FWNewMineViewDelegate>
{
    UIImageView * naviBarView;
    UILabel * titleLable;
    UIView * signView;
    UIButton * messageButton;
    UILabel * unReadLabel;

    FWMineModel * mineModel;
    YNPageViewController *vc;
    
    CGFloat alphaValue;
}

@property (nonatomic, strong) UIView * naviBarView;

@property (nonatomic, strong) UIButton * leftButton;
@property (nonatomic, strong) UIButton * signButton;
@property (nonatomic, strong) UIButton * shareButton;

@property (nonatomic, strong) FWSettingModel * settingModel;
@property (nonatomic, strong) FWNewMineView * userHeadView;

@end

@implementation FWNewMineViewController

#pragma mark - > 请求融云iM
- (void)requestRongYunTokenIM{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_im_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:userId];
                                            [RCIM sharedRCIM].currentUserInfo = userInfo;
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"IM登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            NSLog(@"IMtoken错误");
                                        }];
        }
    }];
}

#pragma mark - > 请求基本配置参数
- (void)requestSetting{
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_settings  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [GFStaticData saveObject:self.settingModel.kefu_uid forKey:kKefuUID];
            [GFStaticData saveObject:self.settingModel.kefu_nickname forKey:kKefuNickname];
            
            self.userHeadView.settingModel = self.settingModel;
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求个人数据
- (void)requsetMineData{
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [GFStaticData saveLoginData:[[back objectForKey:@"data"] objectForKey:@"user_info"]];
           
            [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
            [[FWUserCenter sharedManager] saveUserInfoToDiskWithJson:back];
            [FWUserCenter sharedManager].user_IsLogin = YES;

            mineModel = [FWMineModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            [self.userHeadView configForView:mineModel];
            self.userHeadView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.userHeadView.currentHeight);
            vc.headerView = self.userHeadView;
            
            [vc reloadSuspendHeaderViewFrame];
            
            if (mineModel.user_info.driver_id.length <= 0 ||
                [mineModel.user_info.driver_id integerValue] == 0) {
                [vc removePageControllerWithTitle:@"成绩"];
                [vc updateMenuItemTitles:@[@"帖子",@"收藏", @"座驾"]];
            }
            
            NSString * selectImage = @"";
            NSString * unselectImage = @"";
            if ([mineModel.signin_status intValue] == 2 ){
                /* 当日未签到 */
                selectImage = @"tabbar_me_sign_click";
                unselectImage = @"tabbar_me_sign";
                signView.hidden = NO;
                self.userHeadView.signView.hidden = NO;
            }else if( [mineModel.gold_status isEqualToString:@"1"] && [mineModel.signin_status intValue] == 1){
                selectImage = @"tabbar_me_badge_click";
                unselectImage = @"tabbar_me_badge";
                
                signView.hidden = NO;
                self.userHeadView.signView.hidden = NO;
            }else{
                /* 当日已签到 */
                selectImage = @"tabbar_me_click_icon";
                unselectImage = @"tabbar_me_icon";
                signView.hidden = YES;
                self.userHeadView.signView.hidden = YES;
            }
            
            [[self cyl_tabBarController].tabBar.items enumerateObjectsUsingBlock:^(UITabBarItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (idx == 3) {
                    obj.selectedImage = [[UIImage imageNamed:selectImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    obj.image = [[UIImage imageNamed:unselectImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                }
            }];
            
            [self requestSetting];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = FWTextColor_F8F8F8;
    [self trackPageBegin:@"个人中心页"];
    
    [self requestRongYunTokenIM];
    [self requsetMineData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"个人中心页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_interactivePopDisabled = YES;
    self.fd_prefersNavigationBarHidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PushToPublishViewController) name:kTagPushPublish object:nil];

    [self setupNavigation];
    [self setupPageVC];
}

- (void)setupNavigation{
    
    self.naviBarView = [[UIView alloc] init];
    self.naviBarView.backgroundColor = FWClearColor;
    self.naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    [self.view addSubview:self.naviBarView];
//    self.naviBarView.alpha = 0;
        
    self.leftButton = [[UIButton alloc] init];
    [self.leftButton setImage:[UIImage imageNamed:@"new_black_setting"] forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(settingButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.leftButton];
    [self.leftButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.naviBarView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(self.naviBarView).mas_offset(30+FWCustomeSafeTop);
    }];
    
    self.shareButton = [[UIButton alloc] init];
    [self.shareButton setImage:[UIImage imageNamed:@"new_black_share"] forState:UIControlStateNormal];
    [self.shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.shareButton];

    
    self.signButton = [[UIButton alloc] init];
    [self.signButton setImage:[UIImage imageNamed:@"new_black_sign"] forState:UIControlStateNormal];
    [self.signButton addTarget:self action:@selector(signButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.signButton];
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        self.shareButton.hidden = NO;
        
        [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.naviBarView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.leftButton).mas_offset(1);
        }];

        [self.signButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.shareButton.mas_left).mas_offset(-10);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.leftButton);
        }];
    }else{
        self.shareButton.hidden = YES;
        
        [self.signButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.naviBarView).mas_offset(-15);
            make.size.mas_equalTo(CGSizeMake(36, 20));
            make.centerY.mas_equalTo(self.leftButton);
        }];
    }
    
    signView = [[UIView alloc] init];
    signView.layer.cornerRadius = 6.5/2;
    signView.layer.masksToBounds = YES;
    signView.backgroundColor = FWColor(@"ff6f00");
    [self.naviBarView addSubview:signView];
    [signView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(6.5, 6.5));
        make.top.mas_equalTo(self.signButton).mas_offset(-1);
        make.right.mas_equalTo(self.signButton).mas_offset(-4);
    }];
    signView.hidden = YES;
}

- (void)setupPageVC {
    
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleSuspensionCenter;
    configration.headerViewCouldScale = YES;
    configration.pageScrollEnabled = NO;
    /// 控制tabbar 和 nav
    configration.showTabbar = NO;
    configration.showNavigation = NO;
    configration.scrollMenu = NO;
    configration.aligmentModeCenter = NO;
    configration.lineWidthEqualFontWidth = YES;
    configration.showBottomLine = YES;
    /// 设置悬浮停顿偏移量
    configration.suspenOffsetY = kYNPAGE_NAVHEIGHT;
    
    vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs  titles:[self getArrayTitles] config:configration];
    vc.dataSource = self;
    vc.delegate = self;
    
    self.userHeadView = [[FWNewMineView alloc] init];
    self.userHeadView.vc = self;
    self.userHeadView.delegate = self;
    self.userHeadView.frame = CGRectMake(0, 0, SCREEN_WIDTH,452);
    
    vc.headerView = self.userHeadView;
    vc.pageIndex = 0;
    [vc addSelfToParentViewController:self];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.view bringSubviewToFront:self.naviBarView];
        
        [vc scrollToTop:NO];
    });
}

- (NSArray *)getArrayVCs {

    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    self.user_id = usvm.userModel.uid;
    
    FWUserFeedListViewController *firstVC = [[FWUserFeedListViewController alloc] init];
    firstVC.user_id = self.user_id;
    firstVC.isMine = YES;
    
    FWUserFavouriteViewController *secondVC = [[FWUserFavouriteViewController alloc] init];
    secondVC.user_id = self.user_id;
    secondVC.isMine = YES;

    FWUserCarViewController *thirdVC = [[FWUserCarViewController alloc] init];
    thirdVC.user_id = self.user_id;
    thirdVC.isMine = YES;

    FWUserScoreViewController * fourthVC= [[FWUserScoreViewController alloc] init];
    fourthVC.driver_id = mineModel.user_info.driver_id;
    fourthVC.f_uid = self.user_id;
    
    fourthVC.isMine = YES;

    return @[firstVC, secondVC, thirdVC,fourthVC];
}

- (NSArray *)getArrayTitles {
    return @[@"帖子",@"收藏", @"座驾", @"成绩"];
}

#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    UIViewController *vc = pageViewController.controllersM[index];
    
    if (index == 0) {
        return [(FWUserFeedListViewController *)vc collectionViewLabel];
    }else if (index == 1) {
        
        return [(FWUserFavouriteViewController *)vc collectionViewLabel];
    }else if (index == 2) {
        
        return [(FWUserCarViewController *)vc carTableView];
    }else if (index == 3){
        
        return [(FWUserScoreViewController *)vc mainView];
    }
    
    return [(FWUserScoreViewController *)vc mainView];
}

#pragma mark - YNPageViewControllerDelegate
- (void)pageViewController:(YNPageViewController *)pageViewController
            contentOffsetY:(CGFloat)contentOffset
                  progress:(CGFloat)progress {
//    NSLog(@"--- contentOffset = %f,    progress = %f", contentOffset, progress);
    
    self.naviBarView.backgroundColor = [UIColor colorWithRed:(246)/255.0 green:(246)/255.0 blue:(246)/255.0 alpha:progress];

    self.naviBarView.alpha = progress;
    
    if (nil == mineModel) {
        return;
    }
    
    if (progress >0.01) {
        
        self.leftButton.hidden = NO;
        self.shareButton.hidden = NO;
        self.signButton.hidden = NO;

        self.userHeadView.settingButton.hidden = YES;
        self.userHeadView.shareButton.hidden = YES;
        self.userHeadView.signButton.hidden = YES;
        
        if ([mineModel.signin_status intValue] == 2||
            [mineModel.gold_status isEqualToString:@"1"]) {
            /* 当日未签到 */
            signView.hidden = NO;
            self.userHeadView.signView.hidden = NO;
        }else{
            /* 当日已签到 */
            signView.hidden = YES;
            self.userHeadView.signView.hidden = YES;
        }
    }else{
        self.leftButton.hidden = YES;
        self.shareButton.hidden = YES;
        self.signButton.hidden = YES;
        self.userHeadView.settingButton.hidden = NO;
        self.userHeadView.signButton.hidden = NO;

        /* 当日是否签到了 */
        if ([mineModel.signin_status isEqualToString:@"2"] ||
            [mineModel.gold_status isEqualToString:@"1"]) {
            self.userHeadView.signView.hidden = NO;
        }else{
            signView.hidden = YES;
            self.userHeadView.signView.hidden = YES;
        }
        
        if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
            self.userHeadView.shareButton.hidden = NO;
        }else{
            self.userHeadView.shareButton.hidden = YES;
        }
    }
}

- (void)pageViewController:(YNPageViewController *)pageViewController didScroll:(UIScrollView *)scrollView progress:(CGFloat)progress
 formIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{

//    [vc.bgScrollView scrollToBottom];
//
    FWBaseViewController * tovc = (FWBaseViewController *)pageViewController.controllersM[toIndex];
//    FWBaseViewController * fromvc = (FWBaseViewController *)pageViewController.controllersM[fromIndex];
//
//    /* 帖子 */
//    if (toIndex == 1 && fromIndex == 0) {
//        FWUserFeedListViewController *ffromvc = (FWUserFeedListViewController *)fromvc;
//        FWUserFavouriteViewController *ftovc = (FWUserFavouriteViewController *)tovc;
//        ftovc.collectionViewLabel.mj_header.hidden = YES;
//        ftovc.collectionViewLabel.mj_footer.hidden = YES;
//        ffromvc.collectionViewLabel.mj_header.hidden = YES;
//        ffromvc.collectionViewLabel.mj_footer.hidden = YES;
//    }
//
//    /* 收藏 */
//    if (toIndex == 2 && fromIndex == 1) {
//        FWUserFavouriteViewController *ffromvc = (FWUserFavouriteViewController *)fromvc;
//        FWUserCarViewController *ftovc = (FWUserCarViewController *)tovc;
//
////        ftovc.carTableView.mj_header.hidden = YES;
////        ftovc.carTableView.mj_footer.hidden = YES;
//        ffromvc.collectionViewLabel.mj_header.hidden = YES;
//        ffromvc.collectionViewLabel.mj_footer.hidden = YES;
//    }
//
////    /* 座驾 */
////    if (toIndex == 3 && fromIndex == 2) {
////        FWUserCarViewController *ffromvc = (FWUserCarViewController *)fromvc;
////
////        ffromvc.carTableView.mj_header.hidden = YES;
////        ffromvc.carTableView.mj_footer.hidden = YES;
////    }
//
//
//    if (toIndex == 0 && fromIndex == 0) {
//        FWUserFeedListViewController *ffromvc = (FWUserFeedListViewController *)fromvc;
//        [vc.bgScrollView scrollToTop];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            ffromvc.collectionViewLabel.mj_header.hidden = NO;
//            ffromvc.collectionViewLabel.mj_footer.hidden = NO;
//        });
//    }
//
//    if (toIndex == 1 && fromIndex == 1) {
//        FWUserFavouriteViewController *ffromvc = (FWUserFavouriteViewController *)fromvc;
//        [vc.bgScrollView scrollToTop];
//
////        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
////            ffromvc.collectionViewLabel.mj_header.hidden = NO;
////            ffromvc.collectionViewLabel.mj_footer.hidden = NO;
////        });
//    }
//
//    if (toIndex == 2 && fromIndex == 2) {
//        [vc.bgScrollView scrollToTop];
//    }
//
//    if (toIndex == 3 && fromIndex == 3) {
//        [vc.bgScrollView scrollToTop];
//    }
//
    if (toIndex == 3) {
        FWUserScoreViewController *svc = (FWUserScoreViewController *)tovc;

        if (svc.driver_id.length <= 0) {
            svc.driver_id = mineModel.user_info.driver_id;
            [svc requestData];
        }
    }
}

#pragma mark - > 分享
- (void)shareButtonOnClick{
    [self shareButtonClick];
}

- (void)shareButtonClick{
    
    ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
    [shareView setupUserQRViewWithModel:mineModel];
    [shareView showView];
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0){
            
            [[ShareManager defaultShareManager] userShareToMinProgramObjectWithModel:mineModel];
        }else if (index == 1){
            
            UIImageView * imageView = [[ShareView defaultShareView] getCurrenImage];
            [[ShareManager defaultShareManager] shareToPengyouquan:imageView WithFeedID:mineModel.user_info.uid WithType:Share_uid];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}

#pragma mark - > 签到
- (void)signButtonClick{
    [self.navigationController pushViewController:[FWMyGoldenViewController new] animated:YES];
}

#pragma mark - > 设置
- (void)settingButtonClick{
    
    FWSettingViewController * SVC = [[FWSettingViewController alloc] init];
    SVC.mineModel = mineModel;
    SVC.settingModel = self.settingModel;
    [self.navigationController pushViewController:SVC animated:YES];
}

#pragma mark - > 跳转到发布页
- (void)PushToPublishViewController{
    
    if (![((UINavigationController *)self.tabBarController.selectedViewController).topViewController isEqual:self]
        ) {
        return;
    }
    
    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self];
        }
    }else {
        [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
        
        FWPublishViewController * vc = [[FWPublishViewController alloc] init];
        vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
        vc.fd_prefersNavigationBarHidden = YES;
        vc.navigationController.navigationBar.hidden = YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.4];
        [animation setType: kCATransitionMoveIn];
        [animation setSubtype: kCATransitionFromTop];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        [self.navigationController pushViewController:vc animated:NO];
        [self.navigationController.view.layer addAnimation:animation forKey:nil];
    }
}



@end
