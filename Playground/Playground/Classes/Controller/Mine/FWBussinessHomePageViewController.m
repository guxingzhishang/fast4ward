//
//  FWBussinessHomePageViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessHomePageViewController.h"
#import "FWBussinessHomepageView.h"
#import "FWBussinessHomepageCell.h"
#import "FWBussinessHomePageModel.h"
#import "FWBussinessVideoServiceViewController.h"

#import "FWInviteActivityViewController.h"
#import "FWAuthenticationViewController.h"

@interface FWBussinessHomePageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) FWBussinessHomepageView * headerView;

@property (nonatomic, strong) FWBussinessHomePageModel * bussinessModel;

@property (nonatomic, strong) NSArray * titleArray;
@property (nonatomic, strong) NSArray * imageArray;
@end

@implementation FWBussinessHomePageViewController
@synthesize infoTableView;


#pragma mark - > 请求商家主页相关信息
- (void)requestData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_merchant_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            /* 已认证，进入商家主页 */
            self.bussinessModel = [FWBussinessHomePageModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.headerView configViewForModel:self.bussinessModel];
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"商家主页"];
    [self requestData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"商家主页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    self.title = @"商家主页";

    self.titleArray = @[@"我的店铺",@"视频套餐",@"专属运营"];
    self.imageArray = @[@"bussiness_shop",@"bussiness_video",@"bussiness_yunying",];
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.scrollEnabled = NO;
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 58;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.headerView = [[FWBussinessHomepageView alloc] init];
    self.headerView.vc = self;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 211+FWCustomeSafeTop+16);
    infoTableView.tableHeaderView = self.headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"bussinessHomepageCellID";
    
    FWBussinessHomepageCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWBussinessHomepageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.nameLabel.text = self.titleArray[indexPath.row];
    cell.photoImageView.image = [UIImage imageNamed:self.imageArray[indexPath.row]];
    
    cell.signLabel.text = @"";
    cell.selectImageView.hidden = NO;
    
    if (indexPath.row == 2) {
        cell.selectImageView.hidden = YES;

        if (self.bussinessModel.kefu_msg) {
            cell.signLabel.text = [NSString stringWithFormat:@"请加微信：%@",self.bussinessModel.kefu_msg];
        }
        [cell.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(cell.contentView).mas_offset(-15);
            make.centerY.mas_equalTo(cell.contentView);
            make.height.mas_equalTo(20);
            make.width.mas_greaterThanOrEqualTo(20);
        }];
    }else if(indexPath.row == 0) {
        
        cell.signLabel.text = self.bussinessModel.user_info.shop_status_desc;
        [cell.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(cell.selectImageView.mas_left).mas_offset(-5);
            make.centerY.mas_equalTo(cell.contentView);
            make.height.mas_equalTo(20);
            make.width.mas_greaterThanOrEqualTo(20);
        }];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {

        NSInteger shopStatus = [self.bussinessModel.user_info.shop_status integerValue];
        if (shopStatus == 0) {
            
            /* 未提交审核 */
            [self showAlertView:NO];
        }else if (shopStatus == 1){
            
            /* 提交审核 */
            [[FWHudManager sharedManager] showErrorMessage:@"小四正在加急审核中，请耐心等待" toController:self];
        }else if (shopStatus == 2){
            
            /* 审核通过 */
            FWBussinessShopHostViewController * BSHVC = [[FWBussinessShopHostViewController alloc] init];
            [self.navigationController pushViewController:BSHVC animated:YES];
        }else if (shopStatus == 3){
            
            /* 审核拒绝 */
            [self showAlertView:YES];
        }
    }else if (indexPath.row == 1){
        if ([self.bussinessModel.carshadow_status isEqualToString:@"2"]) {
            /* 已购买视频套餐 */
            FWBussinessVideoServiceViewController * VSVC = [[FWBussinessVideoServiceViewController alloc] init];
            VSVC.productList = self.bussinessModel.list_product;
            [self.navigationController pushViewController:VSVC animated:YES];
        }else{
            FWWebViewController * WVC = [[FWWebViewController alloc] init];
            WVC.pageType = WebViewTypeURL;
            WVC.htmlStr = self.bussinessModel.h5_carshadow;
            [self.navigationController pushViewController:WVC animated:YES];
        }
    }else if (indexPath.row == 2){
        
        [[FWHudManager sharedManager] showSuccessMessage:@"复制成功" toController:self];
        
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = self.bussinessModel.kefu_msg;
    }
}

#pragma mark - > 补充资质
- (void)showAlertView:(BOOL)isBeforeFailure{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"未认证资质" message:self.bussinessModel.shop_msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"补充信息" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        FWAuthenticationViewController * AVC = [[FWAuthenticationViewController alloc] init];
        AVC.isBeforeFailure = isBeforeFailure;
        [self.navigationController pushViewController:AVC animated:YES];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
