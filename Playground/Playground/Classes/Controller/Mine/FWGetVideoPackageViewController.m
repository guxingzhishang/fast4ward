//
//  FWGetVideoPackageViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGetVideoPackageViewController.h"
#import "FWGetMemberView.h"
#import "FWMineRequest.h"

@interface FWGetVideoPackageViewController ()<FWGetMemberViewDelegate>

@property (nonatomic, strong) FWGetMemberView * vipView;
@property (nonatomic, strong) FWMemberInfoModel * vipInfoModel;
@property (nonatomic, strong) FWWechatOrderModel * wechatModel;
@end

@implementation FWGetVideoPackageViewController

//- (void)viewDidLoad{
//    [super viewDidLoad];
//}
//
//#warning 当前版本先不显示,以后删掉下面的这个布尔值
#pragma mark - > 请求vip用户信息
- (void)requestVIPData{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_vip_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.vipInfoModel = [FWMemberInfoModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.vipView settingInfo:self.vipInfoModel];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 提交支付(微信/支付宝)
- (void)requestPay:(NSString *)type{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"product_id":self.memberListModel.product_id,
                              @"pay_service":type,
                              @"real_name":self.vipView.nameField.text,
                              @"mobile":self.vipView.phoneField.text,
                              };
    
    NSString * action ;
    if ([type isEqualToString:@"weixinapp"]) {
        action = Submit_weixin_pay;
    }else if ([type isEqualToString:@"alipayapp"]){
        action = Submit_alipay_pay;
    }else{
        return;
    }
    [request startWithParameters:params WithAction:action WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([type isEqualToString:@"weixinapp"]) {
                /* 微信支付 */
                NSDictionary * result = [[back objectForKey:@"data"] objectForKey:@"result"];
                NSDictionary * pay_order_id = [[back objectForKey:@"data"] objectForKey:@"pay_order_id"];
                
                self.wechatModel = [FWWechatOrderModel mj_objectWithKeyValues:result];
                [self payrequestWithModel:self.wechatModel];
                
                [[NSUserDefaults standardUserDefaults] setObject:pay_order_id forKey:@"订单号"];
            }else if ([type isEqualToString:@"alipayapp"]){
                /* 支付宝支付 */
                
                [[NSUserDefaults standardUserDefaults] setObject:[[back objectForKey:@"data"] objectForKey:@"pay_order_id"] forKey:@"订单号"];
                
                [self alipayRequestWithResult:[[back objectForKey:@"data"] objectForKey:@"result"]];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"购买视频套餐页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"购买视频套餐页"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"会员购买";
    
    self.vipView = [[FWGetMemberView alloc] init];
    self.vipView.memberListModel = self.memberListModel;
    self.vipView.vc = self;
    self.vipView.delegate = self;
    [self.view addSubview:self.vipView];
    [self.vipView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self requestVIPData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySucceed) name:FWWXReturnSussessPayNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payFailed) name:FWWXReturnFailedPayNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selfPayFailed) name:FWSelfControlFailedPayNotification object:nil];
}

#pragma mark - > 去支付
- (void)payButtonClick:(NSString *)payMethod{
    
    if ([payMethod isEqualToString:@"wechat"]) {
        [self requestPay:@"weixinapp"];
    }else if ([payMethod isEqualToString:@"alipay"]){
        [self requestPay:@"alipayapp"];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:payMethod forKey:@"支付返回"];
}

#pragma mark - > 调起微信支付
- (void)payrequestWithModel:(FWWechatOrderModel *)model {
    
    NSString * timeStampStr = model.timestamp;
    UInt32 timesta = (UInt32)[timeStampStr intValue];
    
    PayReq *request = [[PayReq alloc] init];
    request.sign      = model.sign;
    request.package   = @"Sign=WXPay";
    request.nonceStr  = model.noncestr;
    request.prepayId  = model.prepayid;
    request.partnerId = model.partnerid;
    request.timeStamp = timesta;
    
    /* 调起支付 */
    if ([WXApi sendReq:request]) {
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"支付失败" message:@"未安装微信客户端,请使用其他支付方式" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - > 微信pay成功
- (void)paySucceed{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"订单号"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"支付返回"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self requsetMineData];
}

#pragma mark - > 微信pay失败
- (void)payFailed{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付取消" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 自行操作返回pay失败
- (void)selfPayFailed{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付失败，请重试" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 阿里
- (void)alipayRequestWithResult:(NSString *)signedString{
    
    if (signedString != nil) {
        
        NSString *appScheme = @"alipayProduct";
        
        // NOTE: 调用支付结果开始支付
        //        [[AlipaySDK defaultService] payOrder:signedString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        //            if ([[resultDic objectForKey:@"resultStatus"] isEqualToString:@"9000"]) {
        //
        //                [[NSNotificationCenter defaultCenter] postNotificationName:FWWXReturnSussessPayNotification object:nil];
        //            }else{
        //
        //                [[NSNotificationCenter defaultCenter] postNotificationName:FWWXReturnFailedPayNotification object:nil];
        //            }
        //        }];
    }
}

#pragma mark - > 请求个人数据
- (void)requsetMineData{
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
//            FWMineModel * mineModel = [FWMineModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
//            FWMyVIPViewController * vipVC = [FWMyVIPViewController new];
//            vipVC.infoModel = mineModel.user_info;
//            [self.navigationController pushViewController:vipVC animated:YES];
        }
    }];
}

@end
