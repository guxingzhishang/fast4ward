//
//  FWManageCarViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 管理图片
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^requestData)(BOOL isRequest);

@interface FWManageCarViewController : FWBaseViewController

@property (nonatomic, strong) NSString * user_car_id;
@property (nonatomic, copy) requestData isRequest;

@end

NS_ASSUME_NONNULL_END
