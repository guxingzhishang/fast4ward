//
//  FWBussinessCertificationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 商家认证
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWBussinessCertificationViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
