//
//  FWVIPWebViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVIPWebViewController.h"
#import "FWMemberRequest.h"
#import "FWVIPModel.h"
#import "FWGetMemberViewController.h"

@interface FWVIPWebViewController ()

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIView * choiceView;
@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UIButton * attentionButton;

@property (nonatomic, strong) FWVIPModel * vipModel;
@property (nonatomic, assign) NSInteger  currentIndex;

@end

@implementation FWVIPWebViewController
@synthesize bottomView;
@synthesize choiceView;
@synthesize priceLabel;
@synthesize attentionButton;
@synthesize vipModel;

#pragma mark - > 获取产品列表
- (void)requestProductList{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_vip_product_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            vipModel = [FWVIPModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            /* 加载网页 */
            NSString * htmlString = [vipModel.product_intro_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:htmlString]];
            [self.detailView loadRequest:request];
            
            if(self.showBottomView){
                
                priceLabel.text = [NSString stringWithFormat:@"%@元/年",(FWMemberListModel*)(vipModel.product_list[0]).price_yuan];
                [self createChoiceWithVipList:vipModel.product_list];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求基本配置参数
- (void)requestSetting{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_settings  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            FWSettingModel * settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if ([settingModel.show_update isEqualToString:@"2"]) {
                /* 审核期间，隐藏 */
                self.showBottomView = NO;
            }else{
                /* 非审核期间 */
                if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
                    ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]) {
                    /* 登录 */
                    [self requestMineInfo];
                    return ;
                }else{
                    /* 未登录 */
                    self.showBottomView = YES;
                }
            }
            
            [self settingBottomView];
        }
    }];
}
     

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"huiyuan介绍页"];
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    [self requestSetting];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.showBottomView = NO;
    [self settingBottomView];
    
    [self trackPageEnd:@"huiyuan介绍页"];
}

- (void)settingBottomView{
    
#warning 当前版本先不显示,以后删掉下面的这个布尔值
//    self.showBottomView = NO;
   
    if (self.showBottomView) {
        NSInteger height = 130;
        
        if (IPHONE_X) {
            height = 110+FWSafeBottom;
        }
        self.detailView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop - height);
        
        bottomView.hidden = NO;
    }else{
        bottomView.hidden = YES;
        self.detailView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop);
    }
}

#pragma mark - > 请求个人信息
- (void)requestMineInfo{
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]){
        
        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
        NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
        
        [request startWithParameters:params WithAction:Get_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                NSDictionary * infoDict = [[back objectForKey:@"data"] objectForKey:@"user_info"];
                
                [GFStaticData saveLoginData:infoDict];
                
                [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
                [[FWUserCenter sharedManager] saveUserInfoToDiskWithJson:back];
                [FWUserCenter sharedManager].user_IsLogin = YES;
                
                if ([[GFStaticData getObjectForKey:kTagUserKeyUserLevel] integerValue] > 0) {
                    self.showBottomView = NO;
                }else{
                    self.showBottomView = YES;
                }
                
                [self settingBottomView];
            }
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.currentIndex = 0;
    self.showBottomView = YES;

    self.detailView.backgroundColor = FWTextColor_000000;
    [self setupBottomView];
    bottomView.hidden = YES;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestProductList];
    });
}

- (void)setupBottomView{
    
    bottomView = [UIView new];
    bottomView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:bottomView];
    [bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        if (IPHONE_X) {
            make.height.mas_equalTo(110+FWSafeBottom);
        }else{
            make.height.mas_equalTo(130);
        }
    }];
    
    choiceView = [UIView new];
    [bottomView addSubview:choiceView];
    [choiceView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(bottomView);
        make.bottom.mas_equalTo(bottomView);
        make.width.mas_equalTo(SCREEN_WIDTH-100);
    }];
    
    priceLabel = [UILabel new];
    priceLabel.text = @"--";
    priceLabel.font = DHSystemFontOfSize_16;
    priceLabel.textColor = FWColor(@"E84709");
    priceLabel.textAlignment = NSTextAlignmentCenter;
    [bottomView addSubview:priceLabel];
    [priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bottomView).mas_offset(30);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(120);
        make.right.mas_equalTo(bottomView);
    }];
    
    attentionButton = [[UIButton alloc] init];
    attentionButton.layer.borderColor = FWViewBackgroundColor_EEEEEE.CGColor;
    attentionButton.layer.borderWidth = 1;
    attentionButton.layer.cornerRadius = 4;
    attentionButton.layer.masksToBounds = YES;
    attentionButton.titleLabel.font = DHSystemFontOfSize_14;
    [attentionButton setTitle:@"立即加入" forState:UIControlStateNormal];
    [attentionButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [attentionButton addTarget:self action:@selector(attentionButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:attentionButton];
    [attentionButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(80, 35));
        make.top.mas_equalTo(priceLabel.mas_bottom).mas_offset(20);
        make.centerX.mas_equalTo(priceLabel);
    }];
}

- (void)createChoiceWithVipList:(NSMutableArray *)array{
    
    if (array.count > 0) {
        for (int i = 0; i < array.count; i++) {
            FWMemberListModel * model = array[i];
            
            NSString * title = [NSString stringWithFormat:@"   %@(%@元/年)",model.product_name,model.price_yuan];
            
            UIButton * memberButton = [[UIButton alloc] init];
            memberButton.tag = 4980+i;
            memberButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            memberButton.titleLabel.font = DHSystemFontOfSize_14;
            if (i == 0) {
                [memberButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateNormal];
            }else{
                [memberButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
            }
            [memberButton setTitle:title forState:UIControlStateNormal];
            [memberButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
            [memberButton addTarget:self action:@selector(memberButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [choiceView addSubview:memberButton];
            [memberButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(choiceView).mas_offset(12);
                make.top.mas_equalTo(choiceView).mas_offset(i*40+10);
                make.height.mas_equalTo(30);
                make.width.mas_equalTo(SCREEN_WIDTH-100-20);
            }];
        }
    }
}

#pragma mark - > 选择会员
- (void)memberButtonClick:(UIButton *)sender{
    
    self.currentIndex = sender.tag - 4980;
    
    FWMemberListModel * model = vipModel.product_list[self.currentIndex];

    priceLabel.text = [NSString stringWithFormat:@"%@元/年",model.price_yuan];
    
    for (UIButton * memberButton in choiceView.subviews) {
        if (memberButton == sender) {
            [memberButton setImage:[UIImage imageNamed:@"radio_save_sel"] forState:UIControlStateNormal];
        }else{
            [memberButton setImage:[UIImage imageNamed:@"radio_save_unsel"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - > 买会员
- (void)attentionButtonClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        if (self.currentIndex < 0 ) {
            // 没有选择无法购买
            [[FWHudManager sharedManager] showErrorMessage:@"请选择会员" toController:self];
            return;
        }
        
        FWGetMemberViewController * PayVC = [[FWGetMemberViewController alloc] init];
        PayVC.memberListModel = vipModel.product_list[self.currentIndex];
        PayVC.infoModel = self.infoModel;
        [self.navigationController pushViewController:PayVC animated:YES];
    }
}

- (void)checkLogin{
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}
@end
