//
//  FWGoldenCheckingViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGoldenCheckingViewController : FWBaseViewController
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * idLabel;
@end

NS_ASSUME_NONNULL_END
