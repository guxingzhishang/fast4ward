//
//  FWGoldenCheckingDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
* 金币核销详情页（扫码或输入辅助码进入该页面）
*/
#import "FWBaseViewController.h"
#import "FWGoldenCheckModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGoldenCheckingDetailViewController : FWBaseViewController

@property (nonatomic, strong) FWGoldenCheckModel * checkModel;
@property (nonatomic, strong) NSString * code;

@end

NS_ASSUME_NONNULL_END
