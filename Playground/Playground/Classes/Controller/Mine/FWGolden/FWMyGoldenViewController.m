//
//  FWMyGoldenViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyGoldenViewController.h"
#import "FWMyGoldenHeaderView.h"
#import "FWMyGoldenCell.h"
#import "FWGoldenModel.h"

@interface FWMyGoldenViewController ()<UITableViewDelegate,UITableViewDataSource,FWMyGoldenHeaderViewDelegate,FWMyGoldenCellDelegate>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWMyGoldenHeaderView * headerView;
@property (nonatomic, assign) CGFloat alphaValue;
@property (nonatomic, strong) UIView * naviBarView;
@property (nonatomic, strong) UIButton * blackBackButton;
@property (nonatomic, strong) UILabel * titleLabel;

@end

@implementation FWMyGoldenViewController
@synthesize infoTableView;

- (void)requestData{
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_gold_task_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            FWGoldenModel * goldenModel = [FWGoldenModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.headerView configForView:goldenModel];
            
            if (goldenModel.task_list.count >0) {
                if (self.dataSource.count >0) {
                    [self.dataSource removeAllObjects];
                }
                
                [self.dataSource addObjectsFromArray:goldenModel.task_list];
            }
            
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"我的金币页"];
    
    self.view.backgroundColor = FWTextColor_F8F8F8;
    [self requestData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"我的金币页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    self.alphaValue = 0;

    [self setupNavigation];
    [self setupSubViews];
}

- (void)setupNavigation{
    
    self.naviBarView = [[UIView alloc] init];
    self.naviBarView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    [self.view addSubview:self.naviBarView];
    self.naviBarView.alpha = 0;
    
    self.blackBackButton = [[UIButton alloc] init];
    [self.blackBackButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [self.blackBackButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:self.blackBackButton];
    [self.blackBackButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.naviBarView).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.top.mas_equalTo(self.naviBarView).mas_offset(20+FWCustomeSafeTop);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.alpha = 0;
    self.titleLabel.hidden = YES;
    self.titleLabel.text = @"我的金币";
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:18.9];
    self.titleLabel.textColor = FWTextColor_000000;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.naviBarView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.naviBarView);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(100);
        make.centerY.mas_equalTo(self.blackBackButton);
    }];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 59;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.headerView = [[FWMyGoldenHeaderView alloc] init];
    self.headerView.vc = self;
    self.headerView.delegate = self;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 340+FWCustomeSafeTop);
    infoTableView.tableHeaderView = self.headerView;
    
    
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    self.infoTableView.tableFooterView = footView;
    
    [self.view bringSubviewToFront:self.naviBarView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return ((FWTaskListModel *)self.dataSource[section]).list.count?((FWTaskListModel *)self.dataSource[section]).list.count:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWMyGoldenCellID";
    
    FWMyGoldenCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWMyGoldenCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.vc = self;
    cell.delegate = self;
    
    if (indexPath.section < self.dataSource.count) {
        
        if (((FWTaskListModel *)self.dataSource[indexPath.section]).list.count > indexPath.row) {
            FWTaskSubListModel * taskModel = ((FWTaskListModel *)self.dataSource[indexPath.section]).list[indexPath.row];
            cell.type = ((FWTaskListModel *)self.dataSource[indexPath.section]).type;
            [cell configForCell:taskModel];
        }
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 55)];
    view.backgroundColor = FWTextColor_F8F8F8;
    
    UIView * shuImageView = [[UIView alloc] init];
    shuImageView.backgroundColor = FWColor(@"ff6f00");
    shuImageView.frame = CGRectMake(16, 32, 4, 13);
    [view addSubview:shuImageView];
    
    UILabel * leftLabel = [[UILabel alloc] init];
    leftLabel.frame = CGRectMake(CGRectGetMaxX(shuImageView.frame)+10, CGRectGetMinY(shuImageView.frame)-2, 100, 18);
    leftLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    leftLabel.textColor = FWTextColor_222222;
    leftLabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:leftLabel];
    if (section == 0) {
        leftLabel.text = @"每日任务";
    }else{
        leftLabel.text = @"新手任务";
    }
        
    UILabel * rightLabel = [[UILabel alloc] init];
    rightLabel.text = @"做任务，赚金币~";
    rightLabel.frame = CGRectMake(SCREEN_WIDTH - 120-16, CGRectGetMinY(shuImageView.frame)-2, 120, 18);
    rightLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    rightLabel.textColor = FWColor(@"BCBCBC");
    rightLabel.textAlignment = NSTextAlignmentRight;
    [view addSubview:rightLabel];

    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55;
}

#pragma mark - > 领取后请求数据
- (void)statusButtonOnClick{
    [self requestData];
}

#pragma mark - > 签到
- (void)signInClick{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Submit_gold_user_signin WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [self requestData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == self.infoTableView) {
        
        if (scrollView.contentOffset.y > 60){
            self.alphaValue = 1;
        }else{
            self.alphaValue = (scrollView.contentOffset.y/60);
        }
        
        self.titleLabel.alpha = self.alphaValue;
        self.naviBarView.alpha = self.alphaValue;
        
        if (scrollView.contentOffset.y >0) {
            self.titleLabel.hidden = NO;
            
            self.headerView.backButton.hidden = YES;
            self.headerView.titleLabel.hidden = YES;
        }else{
            self.titleLabel.hidden = YES;
            
            self.headerView.backButton.hidden = NO;
            self.headerView.titleLabel.hidden = NO;
        }
    }
}

- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
