//
//  FWGoldenCheckingDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoldenCheckingDetailViewController.h"

@interface FWGoldenCheckingDetailViewController ()
@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;

@property (nonatomic, strong) UILabel * tipLabel;
@property (nonatomic, strong) UITextField * countTextField;


@property (nonatomic, strong) UIImageView * minButton;
@property (nonatomic, strong) UIImageView * maxButton;
@property (nonatomic, strong) UIButton * confirmButton;

@property (nonatomic, assign) NSInteger  count;

@end

@implementation FWGoldenCheckingDetailViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"核销订单详情页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"核销订单详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"金币核销";
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 5;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.view addSubview:self.shadowView];
    self.shadowView.frame = CGRectMake(32, 10, SCREEN_WIDTH-64, 126);
    
    self.bgView = [[UIView alloc] init];
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.shadowView);
    }];
    
    NSArray * leftArray = @[@"ID",@"用户昵称",@"金币余额"];
    NSArray * rightArray = @[self.checkModel.user_info.uid,self.checkModel.user_info.f4w_id,self.checkModel.balance_value];
    for (int i = 0; i < 3; i++) {
        UILabel * leftLabel = [[UILabel alloc] init];
        leftLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        leftLabel.textColor = FWColor(@"b6bcc4");
        leftLabel.text = leftArray[i];
        leftLabel.textAlignment = NSTextAlignmentLeft;
        [self.bgView addSubview:leftLabel];
        leftLabel.frame = CGRectMake(17, 20+ 35*i, 60, 20);
        
        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        rightLabel.textColor = FWColor(@"b6bcc4");
        rightLabel.text = rightArray[i];
        rightLabel.textAlignment = NSTextAlignmentLeft;
        [self.bgView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftLabel.mas_right).mas_offset(0);
            make.centerY.mas_equalTo(leftLabel);
            make.right.mas_equalTo(self.bgView).mas_offset(-5);
            make.height.mas_equalTo(leftLabel);
            make.width.mas_greaterThanOrEqualTo(5);
        }];
    }
    
    
    self.tipLabel = [[UILabel alloc] init];
    self.tipLabel.text = @"请选择核销金币数量";
    self.tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 20];
    self.tipLabel.textColor = FWColor(@"222222");
    self.tipLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.tipLabel];
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.shadowView.mas_bottom).mas_offset(37);
        make.centerX.mas_equalTo(self.view);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    
    self.countTextField = [[UITextField alloc] init];
    self.countTextField.text = @"1";
    self.countTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.countTextField.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 20];
    self.countTextField.textColor = FWColor(@"222222");
    self.countTextField.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.countTextField];
    [self.countTextField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tipLabel.mas_bottom).mas_offset(25);
        make.centerX.mas_equalTo(self.bgView);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(50);
    }];
    
    self.maxButton = [[UIImageView alloc] init];
    self.maxButton.userInteractionEnabled = YES;
    self.maxButton.image = [UIImage imageNamed:@"check_add"] ;
    [self.view addSubview:self.maxButton];
    [self.maxButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.countTextField.mas_right).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(45, 40));
        make.centerY.mas_equalTo(self.countTextField);
    }];
    [self.maxButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(maxButtonClick)]];

    
    self.minButton = [[UIImageView alloc] init];
    self.minButton.userInteractionEnabled = YES;
    self.minButton.image = [UIImage imageNamed:@"check_sub"] ;
    [self.view addSubview:self.minButton];
    [self.minButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.countTextField.mas_left).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(45, 40));
        make.centerY.mas_equalTo(self.countTextField);
    }];
    [self.minButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(minButtonClick)]];

    
    self.confirmButton = [[UIButton alloc] init];
    self.confirmButton.layer.cornerRadius = 20.5;
    self.confirmButton.layer.masksToBounds = YES;
    [self.confirmButton addTarget:self action:@selector(confirmButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.confirmButton];
    self.confirmButton.frame = CGRectMake(56, SCREEN_HEIGHT-41-84-FWSafeBottom-(64+FWCustomeSafeTop), SCREEN_WIDTH-112, 41);
    
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.confirmButton.bounds;
    [self.confirmButton.layer addSublayer:gradientLayer];
    gradientLayer.colors = @[(__bridge id)FWColor(@"6B95EF").CGColor,
                             (__bridge id)FWColor(@"3E68DC").CGColor];
    
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    
    self.confirmButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [self.confirmButton setTitle:@"确认核销" forState:UIControlStateNormal];
    [self.confirmButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
}

- (void)maxButtonClick{
    
    NSInteger tempCount = [self.countTextField.text integerValue] +1;
    
    if (tempCount > [self.checkModel.balance_value integerValue]) {
        [[FWHudManager sharedManager] showErrorMessage:@"超出可用余额" toController:self];
        return;
    }
    
//    self.maxButton.image = [UIImage imageNamed:@"buy_add"] ;
//    self.minButton.image = [UIImage imageNamed:@"buy_sub"] ;

    self.countTextField.text = @(tempCount).stringValue;
//
//    if (tempCount+1 > [self.checkModel.balance_value integerValue]) {
//        self.maxButton.image = [UIImage imageNamed:@"buy_max"] ;
//    }
}

- (void)minButtonClick{
    
    NSInteger tempCount = [self.countTextField.text integerValue] - 1;
    
    if (tempCount <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入有效数值" toController:self];
        return;
    }
//
//    self.maxButton.image = [UIImage imageNamed:@"buy_add"] ;
//    self.minButton.image = [UIImage imageNamed:@"buy_sub"] ;
    self.countTextField.text = @(tempCount).stringValue;
    
//    if (tempCount-1 < [self.checkModel.balance_value integerValue]) {
//        self.minButton.image = [UIImage imageNamed:@"buy_min"] ;
//    }
}

#pragma mark - > 确认核销
- (void)confirmButtonClick{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"请确认是否核销当前金币！" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        if (self.code.length <= 0) {
            [[FWHudManager sharedManager] showErrorMessage:@"缺少相关数据，请联系工作人员" toController:self];
            return;
        }
        
        if (self.countTextField.text.length <= 0) {
            [[FWHudManager sharedManager] showErrorMessage:@"请输入金币有效数量" toController:self];
            return;
        }
        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
        NSDictionary * params = @{
                                    @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                    @"code":self.code,
                                    @"gold_value":self.countTextField.text,

        };
        
        [request startWithParameters:params WithAction:Submit_gold_writeoff WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                [[FWHudManager sharedManager] showErrorMessage:@"核销成功" toController:self];

                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
