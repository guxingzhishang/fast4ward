//
//  FWGoldenDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoldenDetailViewController.h"
#import "FWGoldenDetailCell.h"
#import "FWGoldenModel.h"

@interface FWGoldenDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) UIView * headerView;
@property (nonatomic, strong) UILabel * getGoldenLabel;
@property (nonatomic, strong) UILabel * ableGoldenLabel;


@end

@implementation FWGoldenDetailViewController
@synthesize infoTableView;

- (void)requestData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_user_gold_log WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            FWGoldenModel * goldenModel = [FWGoldenModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.dataSource addObjectsFromArray:goldenModel.list];
            
            self.ableGoldenLabel.text = goldenModel.balance_value;
            self.getGoldenLabel.text = goldenModel.total_sum;

            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"金币明细页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"金币明细页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"金币明细";
    
    [self setupSubViews];
    [self requestData];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 54;
    infoTableView.backgroundColor = FWTextColor_F8F8F8;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.headerView = [[UIView alloc] init];
    self.headerView.backgroundColor = FWTextColor_F8F8F8;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 120);
    infoTableView.tableHeaderView = self.headerView;
    
    for (int i= 0; i < 2; i ++) {
        UIView * bgView = [[UIView alloc] init];
        bgView.layer.cornerRadius = 2;
        bgView.layer.masksToBounds = YES;
        bgView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [self.headerView addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.headerView).mas_offset(10);
            make.width.mas_equalTo((SCREEN_WIDTH-38)/2);
            make.left.mas_equalTo(self.headerView).mas_offset(14+ (i *((SCREEN_WIDTH-38)/2 +10)));
            make.height.mas_equalTo(91);
        }];
        
        UILabel * titleLabel = [[UILabel alloc] init];
        titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
        titleLabel.textColor = FWTextColor_222222;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [bgView addSubview:titleLabel];
        [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(bgView).mas_offset(15);
            make.top.mas_equalTo(bgView).mas_offset(19);
            make.size.mas_equalTo(CGSizeMake(100, 20));
        }];
        
        UILabel * numLabel = [[UILabel alloc] init];
        numLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 28];
        numLabel.textColor = FWColor(@"FF6F00");
        numLabel.textAlignment = NSTextAlignmentLeft;
        [bgView addSubview:numLabel];
        [numLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(titleLabel);
            make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(0);
            make.bottom.mas_equalTo(bgView);
            make.right.mas_equalTo(bgView);
            make.width.mas_greaterThanOrEqualTo(10);
            make.height.mas_greaterThanOrEqualTo(10);
        }];
        
        if (i == 0) {
            titleLabel.text = @"积累获得金币";
            self.getGoldenLabel = numLabel;
        }else{
            titleLabel.text = @"可用金币";
            self.ableGoldenLabel = numLabel;
        }
    }
    
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
    infoTableView.tableFooterView = footView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWGoldenDetailCellID";
    
    FWGoldenDetailCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWGoldenDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if (self.dataSource.count > indexPath.row) {
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

@end
