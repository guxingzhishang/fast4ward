//
//  FWGoldenCheckingViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoldenCheckingViewController.h"
#import "FWScanCheckViewController.h"
#import "FWGoldenLogViewController.h"

@interface FWGoldenCheckingViewController ()

@end

@implementation FWGoldenCheckingViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"金币核销页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"金币核销页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"金币核销";
        
    [self setupSubviews];
}

- (void)setupSubviews{
    
    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    UIView *lineview = [[UIView alloc] init];
    lineview.frame = CGRectMake(30,10,SCREEN_WIDTH-60,44);
    lineview.backgroundColor = [UIColor colorWithRed:246/255.0 green:248/255.0 blue:250/255.0 alpha:1.0];
    [self.view addSubview:lineview];
    
    UIImageView * iconImage = [[UIImageView alloc] init];
    iconImage.image = [UIImage imageNamed:@"ticket_check_icon"];
    [lineview addSubview:iconImage];
    [iconImage mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(41, 43));
        make.left.mas_equalTo(lineview).mas_offset(0);
        make.centerY.mas_equalTo(lineview);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.text = usvm.userModel.nickname;
    self.nameLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 19];
    self.nameLabel.textColor = FWTextColor_222222;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [lineview addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(lineview);
        make.left.mas_equalTo(iconImage.mas_right).mas_offset(0);
        make.height.mas_equalTo(25);
        make.width.mas_lessThanOrEqualTo(200);
    }];
    
    self.idLabel = [[UILabel alloc] init];
    self.idLabel.text = [NSString stringWithFormat:@"（%@）", usvm.userModel.f4w_id];
    self.idLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.idLabel.textColor = FWTextColor_222222;
    self.idLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.idLabel];
    [self.idLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.nameLabel);
        make.height.mas_equalTo(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(5);
    }];
    
        
    NSArray * iconArray = @[@"scan_check",@"check_log"];
    
    for (int i = 0; i < iconArray.count; i ++) {
        
        UIView * shadowView = [[UIView alloc] init];
        shadowView.userInteractionEnabled = YES;
        shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        shadowView.userInteractionEnabled = YES;
        shadowView.layer.cornerRadius = 5;
        shadowView.layer.shadowOffset = CGSizeMake(0, 0);
        shadowView.layer.shadowOpacity = 0.7;
        shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
        [self.view addSubview:shadowView];
        [shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (i<2) {
                make.top.mas_equalTo(lineview.mas_bottom).mas_offset(16);
                make.left.mas_equalTo(self.view).mas_offset(30+i*(10+(SCREEN_WIDTH-70)/2));
            }else{
                make.top.mas_equalTo(lineview.mas_bottom).mas_offset(16 + (143*(SCREEN_WIDTH-70)/2)/153+10);
                make.left.mas_equalTo(self.view).mas_offset(30);
            }
            make.height.mas_equalTo((143*(SCREEN_WIDTH-70)/2)/153);
            make.width.mas_equalTo((SCREEN_WIDTH-70)/2);
        }];
        
        UIImageView * bgView = [[UIImageView alloc] init];
        bgView.tag = 100000+i;
        bgView.userInteractionEnabled = YES;
        bgView.image = [UIImage imageNamed:iconArray[i]];
        [shadowView addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(shadowView);
        }];
        
        [bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewClick:)]];
    }
}

- (void)viewClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 100000;
    
    if (index == 0) {
        /* 扫码核销 */
        FWScanCheckViewController * vc = [[FWScanCheckViewController alloc] init];
        vc.fromType = @"golden_check";
        [self.navigationController pushViewController:vc animated:YES];
    }else if (index == 1) {
        /* 日志 */
        [self.navigationController pushViewController:[FWGoldenLogViewController new] animated:YES];
    }
}

@end
