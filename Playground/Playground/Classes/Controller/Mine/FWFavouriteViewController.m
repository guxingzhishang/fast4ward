//
//  FWFavouriteViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWFavouriteViewController.h"
#import "FWVisitorSelectBarView.h"
#import "FWFavouritePicCell.h"
#import "FWFavouriteVideoCell.h"

static NSString * const videoFavouriteID = @"videoFavouriteID";
static NSString * const picFavouriteID = @"picFavouriteID";

@interface FWFavouriteViewController ()<UIScrollViewDelegate,FWVisitorSelectBarViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FWBaseCollectionLayoutDalegate,FWCollectionViewEmptyButtonDelegate>

@property (nonatomic, strong) NSMutableArray * videoDataSource;
@property (nonatomic, strong) NSMutableArray * picDataSource;

@property (nonatomic, strong) FWSelectBarView * barView;

@property (nonatomic, strong) UIScrollView * mainScrollView;

@property (nonatomic, strong)  FWCollectionView * videoCollectionView;
@property (nonatomic, strong)  FWCollectionView * picCollectionView;

@property (nonatomic, assign)  NSInteger videoPageNum;
@property (nonatomic, assign)  NSInteger picPageNum;

// 以下代表是否第一次请求过，用来第一次切换标签时用
@property (nonatomic, assign) BOOL  isvideoRequest;
@property (nonatomic, assign) BOOL  ispicRequest;

@property (nonatomic, strong) FWFeedModel * videoFeedModel;
@property (nonatomic, strong) FWFeedModel * picFeedModel;

@property(nonatomic, strong) FWBaseCollectionLayout * videoLayout;
@property(nonatomic, strong) FWBaseCollectionLayout * picLayout;

/* 标记是哪一个列表 */
@property (nonatomic, assign) FWFavouriteCollectionViewType collectionType;

@end

@implementation FWFavouriteViewController
@synthesize videoDataSource;
@synthesize picDataSource;
@synthesize barView;
@synthesize mainScrollView;
@synthesize videoCollectionView;
@synthesize picCollectionView;
@synthesize videoPageNum;
@synthesize picPageNum;
@synthesize isvideoRequest;
@synthesize ispicRequest;
@synthesize videoFeedModel;
@synthesize picFeedModel;

#pragma mark - > 请求视频
- (void)requestvideoDataWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.videoPageNum = 1;
        
        if (self.videoDataSource.count > 0 ) {
            [self.videoDataSource removeAllObjects];
        }
    }else{
        self.videoPageNum +=1;
    }
    
    FWVisitorRequest * request = [[FWVisitorRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"watermark":@"1",
                              @"feed_type":@"1",
                              @"page":@(self.videoPageNum).stringValue,
                              @"pagesize":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_favourite_feed_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [videoCollectionView.mj_footer endRefreshing];
            }else{
                [videoCollectionView.mj_header endRefreshing];
            }
            
            videoFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.videoDataSource addObjectsFromArray:videoFeedModel.feed_list];
            
            FWMineModel * mineModel = [[FWMineModel alloc] init];
            mineModel.feed_count_video = videoFeedModel.total_count_video;
            mineModel.feed_count_image = videoFeedModel.total_count_image;
            [barView setTabNmuber:mineModel];
            
            if([videoFeedModel.feed_list count] == 0 && self.videoPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.videoCollectionView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求图片
- (void)requestPicDataWithLoadMoredData:(BOOL)isLoadMoredData{
    
    NSInteger tempPageNum = 1;
    
    if (isLoadMoredData == NO) {
        self.picPageNum = 1;
        
        if (self.picDataSource.count > 0 ) {
            [self.picDataSource removeAllObjects];
        }
    }else{
        self.picPageNum +=1;
    }
        
    FWVisitorRequest *request = [[FWVisitorRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"watermark":@"1",
                              @"feed_type":@"2",
                              @"page":@(picPageNum).stringValue,
                              @"page_size":@"20",
                              };
    request.isNeedShowHud = YES;
    
    [request startWithParameters:params WithAction:Get_favourite_feed_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [picCollectionView.mj_footer endRefreshing];
            }else{
                [picCollectionView.mj_header endRefreshing];
            }
            
            picFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.picDataSource addObjectsFromArray:picFeedModel.feed_list];
            
            FWMineModel * mineModel = [[FWMineModel alloc] init];
            mineModel.feed_count_video = picFeedModel.total_count_video;
            mineModel.feed_count_image = picFeedModel.total_count_image;
            [barView setTabNmuber:mineModel];
            
            if([picFeedModel.feed_list count] == 0 &&
               tempPageNum != 1){
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
                return ;
            }
            
            [picCollectionView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"我的收藏页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"我的收藏页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的收藏";
    self.picPageNum = 0;
    self.videoPageNum = 0;
    
    self.ispicRequest = NO;
    self.isvideoRequest = NO;

    self.videoDataSource = @[].mutableCopy;
    self.picDataSource = @[].mutableCopy;
    
    
    [self setupSelectBarView];
    [self setupSubViews];
    [self setupCollectionView];
    
    self.collectionType = FWFavouriteVideoCollectionType;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestvideoDataWithLoadMoredData:NO];
    });
}

#pragma mark - > 初始化视图
- (void)setupSelectBarView{
    barView = [[FWSelectBarView alloc] init];
    barView.titleArray = @[@"视频",@"图文"];
    barView.numType = 2;
    [self.view addSubview:barView];
    barView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);

    [barView.firstButton addTarget:self action:@selector(didHeadButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [barView.secondButton addTarget:self action:@selector(didHeadButtonClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupSubViews{
    
    mainScrollView = [[UIScrollView alloc] init];
    mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(barView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(barView.frame)-49-FWSafeBottom);
    mainScrollView.pagingEnabled = YES;
    mainScrollView.delegate = self;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:mainScrollView];
    mainScrollView.contentSize = CGSizeMake(2*SCREEN_WIDTH, 0);
}

-(void)setupCollectionView{
    
    self.videoLayout = [[FWBaseCollectionLayout alloc] init];
    self.videoLayout.columns = 2;
    self.videoLayout.rowMargin = 12;
    self.videoLayout.colMargin = 12;
    self.videoLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.videoLayout.delegate = self;
    [self.videoLayout autuContentSize];
    
    self.videoCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, CGRectGetHeight(self.mainScrollView.frame)) collectionViewLayout:self.videoLayout];
    self.videoCollectionView.dataSource = self;
    self.videoCollectionView.delegate = self;
    self.videoCollectionView.showsVerticalScrollIndicator = NO;
    self.videoCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.videoCollectionView registerClass:[FWFavouriteVideoCell class] forCellWithReuseIdentifier:videoFavouriteID];
    self.videoCollectionView.isNeedEmptyPlaceHolder = YES;
    self.videoCollectionView.verticalOffset = -150;
    NSString * allTitle = @"还没有发布的视频哦~";
    NSDictionary * allAttributes = @{
                                     NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                     NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * allAttributeString = [[NSMutableAttributedString alloc] initWithString:allTitle attributes:allAttributes];
    self.videoCollectionView.emptyDescriptionString = allAttributeString;
    [self.mainScrollView addSubview:self.videoCollectionView];
    
    self.picLayout = [[FWBaseCollectionLayout alloc] init];
    self.picLayout.columns = 2;
    self.picLayout.rowMargin = 12;
    self.picLayout.colMargin = 12;
    self.picLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.picLayout.delegate = self;
    [self.picLayout autuContentSize];
    
    self.picCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH,0, SCREEN_WIDTH, CGRectGetHeight(self.mainScrollView.frame)) collectionViewLayout:self.picLayout];
    self.picCollectionView.dataSource = self;
    self.picCollectionView.delegate = self;
    self.picCollectionView.showsVerticalScrollIndicator = NO;
    self.picCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.picCollectionView registerClass:[FWFavouritePicCell class] forCellWithReuseIdentifier:picFavouriteID];
    self.picCollectionView.isNeedEmptyPlaceHolder = YES;
    self.picCollectionView.verticalOffset = -150;
    NSString * videoTitle = @"还没有发布的图片哦~";
    NSDictionary * videoAttributes = @{
                                       NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                       NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * videoAttributeString = [[NSMutableAttributedString alloc] initWithString:videoTitle attributes:videoAttributes];
    self.picCollectionView.emptyDescriptionString = videoAttributeString;
    [self.mainScrollView addSubview:self.picCollectionView];
    
    DHWeakSelf;
    
    videoCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestvideoDataWithLoadMoredData:NO];
    }];
    
    videoCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestvideoDataWithLoadMoredData:YES];
    }];
    
    picCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestPicDataWithLoadMoredData:NO];
    }];
    
    picCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestPicDataWithLoadMoredData:YES];
    }];
}

#pragma mark - > collectionView delegate & datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger number;
    if (collectionView == videoCollectionView) {
        number = videoDataSource.count?videoDataSource.count:0;
    }else{
        number = picDataSource.count?picDataSource.count:0;
    }
    
    return number;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == videoCollectionView) {
        FWFavouriteVideoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:videoFavouriteID forIndexPath:indexPath];
        cell.viewController = self;
        
        if (videoDataSource.count > 0) {
            
            FWFeedListModel * model = videoDataSource[indexPath.item];
            [cell configForCell:model];
            
            if (model.h5_url.length > 0 ||
                [model.flag_create isEqualToString:@"1"]) {
                
                cell.activityImageView.hidden = NO;
                
                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);
                
                if ([model.flag_create isEqualToString:@"1"]){
                    // 发起者
                    imageName = @"faqizhe";
                    imageViewSize = CGSizeMake(50, 21);
                }
                
                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }
        return cell;
    }else {
        FWFavouritePicCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:picFavouriteID forIndexPath:indexPath];
        cell.viewController = self;
        
        if (picDataSource.count > 0) {
            
            FWFeedListModel * model = picDataSource[indexPath.item];
            [cell configForCell:model];
            
            if (model.h5_url.length > 0 ||
                [model.flag_create isEqualToString:@"1"]) {
                
                cell.activityImageView.hidden = NO;
                
                NSString * imageName = @"activity";
                CGSize imageViewSize = CGSizeMake(30, 15);
                
                if ([model.flag_create isEqualToString:@"1"]){
                    // 发起者
                    imageName = @"faqizhe";
                    imageViewSize = CGSizeMake(50, 21);
                }
                
                cell.activityImageView.image = [UIImage imageNamed:imageName];
                [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                    make.size.mas_equalTo(imageViewSize);
                }];
            }else{
                cell.activityImageView.hidden = YES;
            }
        }
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr;
    if (collectionView == videoCollectionView) {
        tempArr = videoDataSource;
    }else{
        tempArr = picDataSource;
    }
    
    FWFeedListModel * model = tempArr[indexPath.row];
    NSMutableArray * tempDataSource = @[].mutableCopy;
    
    if (model.h5_url.length > 0) {
        FWWebViewController * WVC = [[FWWebViewController alloc] init];
        WVC.pageType = WebViewTypeURL;
        WVC.htmlStr = model.h5_url;
        [self.navigationController pushViewController:WVC animated:YES];
        return;
    }
    
    if ([model.feed_type isEqualToString:@"1"]) {
        
        if ([model.is_long_video isEqualToString:@"1"]) {
            /* 长视频 */
            FWLongVideoPlayerViewController * LPVC = [[FWLongVideoPlayerViewController alloc] init];
            LPVC.feed_id = model.feed_id;
            LPVC.myBlock = ^(FWFeedListModel *listModel) {};
            [self.navigationController pushViewController:LPVC animated:YES];
        }else{
            /* 短视频 */
            for (NSInteger i = indexPath.row; i<tempArr.count; i++) {
                FWFeedListModel * model = tempArr[i];
                
                if ([model.feed_type isEqualToString:@"1"]) {
                    [tempDataSource addObject:model];
                }
            }
            
            FWVideoPlayerViewController *controller = [[FWVideoPlayerViewController alloc] init];
            controller.currentIndex = 0;
            controller.listModel = tempDataSource;
            controller.requestType = FWFavouriteListRequestType;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        for (FWFeedListModel * model in tempArr) {
            if ([model.feed_type isEqualToString:@"2"]) {
                [tempDataSource addObject:model];
            }
        }
        
        // 图文详情
        FWGraphTextDetailViewController * TVC = [[FWGraphTextDetailViewController alloc] init];
        TVC.listModel = model;
        TVC.feed_id = model.feed_id;
        TVC.myBlock = ^(FWFeedListModel *awemeModel) {};
        [self.navigationController pushViewController:TVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]){
        // 文章页
        FWArticalDetailViewController * DVC = [[FWArticalDetailViewController alloc] init];
        DVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:DVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"4"]) {
        // 问答
        FWAskAndAnswerDetailViewController * RCDVC = [[FWAskAndAnswerDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
//        DVC.myBlock = ^(FWFeedListModel *listModel) {
//            self.dataSource[index] = listModel;
//        };
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        // 改装
        FWRefitCaseDetailViewController * RCDVC = [[FWRefitCaseDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
    //        DVC.myBlock = ^(FWFeedListModel *listModel) {
    //            self.dataSource[index] = listModel;
    //        };
        [self.navigationController pushViewController:RCDVC animated:YES];
    }else if ([model.feed_type isEqualToString:@"6"]) {
        /* 闲置 */
        FWIdleDetailViewController * RCDVC = [[FWIdleDetailViewController alloc] init];
        RCDVC.listModel = model;
        RCDVC.feed_id = model.feed_id;
        [self.navigationController pushViewController:RCDVC animated:YES];
    }
}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr;
    if (self.collectionType == FWFavouriteVideoCollectionType) {
        tempArr = videoDataSource;
    }else{
        tempArr = picDataSource;
    }
    
    if (indexPath.item >= tempArr.count) {
        return 0;
    }
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"2"]) {
        // 图文贴 固定比例3：4
        height = (cover_width *4)/3;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if([model.feed_type isEqualToString:@"3"]){
        // 文章帖 固定比例5：4
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if ([model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 20;
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 25;
        }
    }else  if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {

            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}

#pragma mark - > 切换标签
- (void)didHeadButtonClick:(UIButton *)sender{
    
    NSInteger index = 0;
    if (sender == barView.firstButton) {
        if (self.collectionType == FWFavouriteVideoCollectionType) {
            return;
        }
        self.collectionType = FWFavouriteVideoCollectionType;
        barView.firstButton.selected = YES;
        barView.secondButton.selected = NO;
        
        barView.shadowView.frame = CGRectMake(CGRectGetMinX(barView.firstButton.frame)+(CGRectGetWidth(barView.firstButton.frame)-32)/2, CGRectGetMaxY(barView.firstButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);

        index = 0;
    }else if (sender == barView.secondButton){
        if (self.collectionType == FWFavouritePicCollectionType) {
            return;
        }
        
        self.collectionType = FWFavouritePicCollectionType;
        barView.firstButton.selected = NO;
        barView.secondButton.selected = YES;
        
        barView.shadowView.frame = CGRectMake(CGRectGetMinX(barView.secondButton.frame)+(CGRectGetWidth(barView.secondButton.frame)-32)/2, CGRectGetMaxY(barView.secondButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);

        index = 1;

        if (!self.ispicRequest) {
            self.ispicRequest = YES;
            [self requestPicDataWithLoadMoredData:NO];
        }
    }
    
    //点击btn的时候scrollowView的contentSize发生变化
    [self.mainScrollView setContentOffset:CGPointMake(index *SCREEN_WIDTH, 0) animated:YES];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (self.mainScrollView == scrollView){
        
        float scrollViewX = scrollView.contentOffset.x;
        if (scrollViewX == 0) {
            [self didHeadButtonClick:barView.firstButton];
        }else if (scrollViewX == SCREEN_WIDTH){
            [self didHeadButtonClick:barView.secondButton];
        }
    }
}

@end
