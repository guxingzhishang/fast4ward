//
//  FWThirdBindingViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/29.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWThirdBindingViewController.h"
#import "FWWXLoginRequest.h"
#import "FWMineRequest.h"

@interface FWThirdBindingViewController ()

@end

@implementation FWThirdBindingViewController
@synthesize cellView;

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"绑定第三方页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"绑定第三方页"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WXLoginFinished:) name:kTagWXShareFinished object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(WXLoginRefresh:) name:kTagWXLoginRefresh object:nil];
    
    self.title = @"设置";

    [self setupSubviews];
}

- (void)setupSubviews{
        
    cellView = [[FWBaseCellView alloc] initWithTitle:@"微信账号" WithImage:@""];
    cellView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [cellView addTarget:self action:@selector(cellViewClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cellView];
    [cellView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 45));
        make.top.mas_equalTo(self.view).mas_offset(16);
    }];
    
    [cellView.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cellView).mas_offset(15);
        make.centerY.mas_equalTo(cellView);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    cellView.iconImageView.hidden = YES;
    
    NSString * bindStatus = @"未绑定";
    
    if ([[GFStaticData getObjectForKey:kTagUserKeyWXStatus] isEqualToString:@"1"]) {
        
        NSDictionary * dict = [NSDictionary dictionaryWithJsonString:[GFStaticData getObjectForKey:kTagWeiXinUserInfo]];
        bindStatus = [dict objectForKey:@"nickname"];
    }
    cellView.rightLabel.text = bindStatus;
}

#pragma mark - > 去绑定
- (void)cellViewClick:(UIButton * )sender{
   
    if ([[GFStaticData getObjectForKey:kTagUserKeyWXStatus] isEqualToString:@"1"]) {
        //  解绑
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"确认解绑" message:@"解绑微信账号后将无法继续使用它登录该肆放账号" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self requestUnbind];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
            [GFStaticData saveObject:Submit_weixin_bind forKey:@"WXOperation"];
            
            //微信登录
            SendAuthReq * req = [[SendAuthReq alloc] init];
            req.openID = WXAppKey;
            req.scope = @"snsapi_userinfo";
            req.state = @"wechat_sdk_demo";
            [WXApi sendReq:req];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
        }
        
    }
}

#pragma mark - > 解绑微信
- (void)requestUnbind{
    
    FWWXLoginRequest * request = [[FWWXLoginRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"bind_type":@"weixin",
                              };
    
    [request startWithParameters:params WithAction:Submit_oauth_unbind  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showSuccessMessage:@"解绑成功" toController:self];
            
            [GFStaticData saveObject:@"2" forKey:kTagUserKeyWXStatus];
            cellView.rightLabel.text = @"未绑定";

        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

// 微信请求结束
- (void)WXLoginFinished:(NSNotification *)noti{
    
    BaseResp * resp = [noti object];
    if (resp.errCode == 0)
    {
        if ([resp isKindOfClass:[SendAuthResp class]])
        {
            SendAuthResp * sendAuthResp = (SendAuthResp *)resp;
            if (sendAuthResp.code)
            {
                [self getAccessTokenWithCode:[NSString stringWithFormat:@"%@",sendAuthResp.code]];
            }
        }
    }
}

- (void)getAccessTokenWithCode:(NSString *)code{
    
    FWWXLoginRequest * request = [[FWWXLoginRequest alloc] init];
    [request getAccessTokenWithCode:code WithAction:WXLogin];
}


- (void)WXLoginRefresh:(NSNotification *)noti{
    
    NSDictionary * dict = (NSDictionary *)[noti object];

    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        [GFStaticData saveObject:@"1" forKey:kTagUserKeyWXStatus];

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [GFStaticData saveLoginData:[[back objectForKey:@"data"]objectForKey:@"user_info"]];
            
            [[FWHudManager sharedManager] showSuccessMessage:@"绑定成功" toController:self];
            
            NSDictionary * dict = [NSDictionary dictionaryWithJsonString:[GFStaticData getObjectForKey:kTagWeiXinUserInfo]];
            
            cellView.rightLabel.text = [dict objectForKey:@"nickname"];

        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}
@end
