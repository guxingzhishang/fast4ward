//
//  FWFollowViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWFollowViewController.h"
#import "FWFollowRequest.h"
#import "FWFollowModel.h"

@interface FWFollowViewController ()

@property (nonatomic, strong) NSMutableArray * followList;
@property (nonatomic, strong) NSMutableArray * tagsFollowList;

@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, assign) NSInteger tagsNum;

@property (nonatomic, strong) FWFollowModel * followModel;
@property (nonatomic, strong) FWSearchTagsListModel * tagsFollowModel;

@end

@implementation FWFollowViewController
@synthesize infoTableView;
@synthesize pageNum;
@synthesize tagsNum;
@synthesize followModel;
@synthesize tagsFollowModel;
@synthesize tagsTableView;
@synthesize segementScr;
@synthesize headerView;

- (NSMutableArray *)followList{
    if (!_followList) {
        _followList = [[NSMutableArray alloc] init];
    }
    return _followList;
}

- (NSMutableArray *)tagsFollowList{
    if (!_tagsFollowList) {
        _tagsFollowList = [[NSMutableArray alloc] init];
    }
    return _tagsFollowList;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"关注页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"关注页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"关注";

    self.fd_prefersNavigationBarHidden = YES;
    
    pageNum = 0;
    tagsNum = 0;
    
    self.segementType = 1;
    self.isRefresh = YES;
    
    [self requestFollowsListWithLoadMoredData:NO];
}

- (void)bindViewModel{
    [super bindViewModel];
}

#pragma mark - > 请求好友关注列表
- (void)requestFollowsListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.followList.count > 0 ) {
            [self.followList removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"base_uid":self.base_uid,
                              @"get_type":@"follow",
                              @"page":@(pageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_follow_users_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            followModel = [FWFollowModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSMutableArray * tempArr = followModel.user_list;
            
            [self.followList addObjectsFromArray:tempArr];
            
            if([tempArr count] == 0 && pageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求关注标签列表
- (void)requestFollowsTagsListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.tagsNum = 1;
        
        if (self.tagsFollowList.count > 0 ) {
            [self.tagsFollowList removeAllObjects];
        }
    }else{
        self.tagsNum +=1;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"base_uid":self.base_uid,
                              @"page":@(tagsNum).stringValue,
                              @"page_size":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_follow_tags_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            tagsFollowModel = [FWSearchTagsListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (isLoadMoredData) {
                [tagsTableView.mj_footer endRefreshing];
            }else{
                [tagsTableView.mj_header endRefreshing];
            }
            
            NSMutableArray * tempArr = tagsFollowModel.tag_list;
            
            [self.tagsFollowList addObjectsFromArray:tempArr];
            
            if([tempArr count] == 0 && tagsNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [tagsTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


#pragma mark - > 视图初始化
- (void)addSubviews{
    
    headerView = [[FWFollowHeaderView alloc] init];
    headerView.delegate = self;
    headerView.vc = self;
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    [self.view addSubview:headerView];
    
    segementScr = [[UIScrollView alloc]init];
    segementScr.delegate = self;
    segementScr.pagingEnabled = YES;
    segementScr.showsHorizontalScrollIndicator = NO;
    segementScr.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.segementScr];
    self.segementScr.frame = CGRectMake(0, CGRectGetMaxY(headerView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(headerView.frame));
    self.segementScr.contentSize = CGSizeMake(2* SCREEN_WIDTH, 0);
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 64;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    infoTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, CGRectGetHeight(self.segementScr.frame));
    [segementScr addSubview:infoTableView];
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    tagsTableView = [[FWTableView alloc] init];
    tagsTableView.delegate = self;
    tagsTableView.dataSource = self;
    tagsTableView.rowHeight = 64;
    tagsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tagsTableView.isNeedEmptyPlaceHolder = YES;
    NSString * tagstitle = @"还没有相关的内容哦~";
    NSDictionary * tagsAttributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * tagsAttributeString = [[NSMutableAttributedString alloc] initWithString:tagstitle attributes:tagsAttributes];
    tagsTableView.emptyDescriptionString = tagsAttributeString;
    tagsTableView.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, CGRectGetHeight(self.segementScr.frame));
    [segementScr addSubview:tagsTableView];
    
    if (@available(iOS 11.0, *)) {
        tagsTableView.estimatedSectionFooterHeight = 0;
        tagsTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestFollowsListWithLoadMoredData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestFollowsListWithLoadMoredData:YES];
    }];
    
    tagsTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestFollowsTagsListWithLoadMoredData:NO];
    }];
    tagsTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestFollowsTagsListWithLoadMoredData:YES];
    }];
}

#pragma mark - > UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    NSInteger number;
    if (self.segementType == 1) {
        number = self.followList.count?self.followList.count:0;
    }else{
        number = self.tagsFollowList.count?self.tagsFollowList.count:0;
    }
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == infoTableView) {
        static NSString * cellID = @"followCellID";
        
        FWFollowCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[FWFollowCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        cell.delegate = self;
        cell.followButton.tag = 5000+indexPath.row;
        [cell cellConfigureFor:self.followList[indexPath.row]];
        
        return cell;
    }else{
        
        static NSString * cellID = @"tagsFollowCellID";
        
        FWTagsFollowCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[FWTagsFollowCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        cell.followButton.tag = 5000+indexPath.row;
        [cell cellConfigureFor:self.tagsFollowList[indexPath.row]];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (tableView == tagsTableView) {
        // 跳转至标签页
        FWSearchTagsListModel * model = self.tagsFollowList[indexPath.row];

        FWTagsViewController * TVC = [[FWTagsViewController alloc] init];
        TVC.tags_id = model.tag_id;
        [self.navigationController pushViewController:TVC animated:YES];
    }
}

#pragma mark - > 我的粉丝页面，点击关注按钮
- (void)followButtonForIndex:(NSInteger)index withCell:(FWFollowCell *)cell{
    
    NSString * action ;
    
    FWFollowUserListModel * listModel = self.followList[index];
    
    if ([listModel.follow_status isEqualToString:@"2"]) {
        action = Submit_follow_users;
    }else{
        action = Submit_cancel_follow_users;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"f_uid":listModel.f_uid,
                              };
    
    [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
           
            if ([action isEqualToString: Submit_follow_users]) {
 
                listModel.follow_status_str = [[back objectForKey:@"data"] objectForKey:@"follow_status_str"];
                listModel.follow_status = [[back objectForKey:@"data"] objectForKey:@"follow_status"];
              
                [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self];
            }else{
                listModel.follow_status = @"2";
            }
            [cell cellConfigureFor:listModel];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (self.segementScr == scrollView){
        
        float scrollViewX = scrollView.contentOffset.x;
        if (scrollViewX == 0) {
            [self headerBtnClick:headerView.userButton];
        }
        else if (scrollViewX == SCREEN_WIDTH){
            [self headerBtnClick:headerView.tagsButton];
        }
    }
}

#pragma mark -> 点击关注用户 & 关注的帖子
- (void)headerBtnClick:(UIButton *)sender{
    
    if ([sender isEqual:headerView.userButton]) {
        if (self.segementType == 1) {
            return;
        }
    
        self.segementType = 1;
        
        [headerView.gunView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(headerView.userButton);
            make.size.mas_equalTo(CGSizeMake(32, 1.5));
            make.top.mas_equalTo(headerView.userButton.mas_bottom);
        }];
    }else{
        if (self.segementType == 2) {
            return;
        }
        
        if (self.isRefresh) {
            self.isRefresh = NO;
            [self requestFollowsTagsListWithLoadMoredData:NO];
        }
        
        self.segementType = 2;
        
        [headerView.gunView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(headerView.tagsButton);
            make.size.mas_equalTo(CGSizeMake(32, 1.5));
            make.top.mas_equalTo(headerView.tagsButton.mas_bottom);
        }];
    }

    //点击btn的时候scrollowView的contentSize发生变化
    [self.segementScr setContentOffset:CGPointMake((self.segementType-1) *SCREEN_WIDTH, 0) animated:YES];
}

@end
