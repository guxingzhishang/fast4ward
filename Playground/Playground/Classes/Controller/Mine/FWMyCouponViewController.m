//
//  FWMyCouponViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyCouponViewController.h"
#import "FWMyCouponCell.h"

@interface FWMyCouponViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) UILabel * headerLabel;

@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWCouponModel * couponModel;

@end

@implementation FWMyCouponViewController

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 查询优惠券
- (void)requestCouponList{
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_vip_coupon_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.couponModel = [FWCouponModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (self.couponModel.coupon_list.count > 0) {
                
                [self.dataSource addObjectsFromArray:self.couponModel.coupon_list];
                [self.infoTableView reloadData];
                
                [self.headerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.view).mas_offset(5);
                    make.left.mas_equalTo(self.view).mas_offset(10);
                    make.width.mas_equalTo(SCREEN_WIDTH-20);
                    make.height.mas_equalTo(1);
                }];
            }else{
                if (self.couponModel.nocoupon_text && self.couponModel.nocoupon_text.length > 0) {
                    
                    NSString * nocoupon_text =  self.couponModel.nocoupon_text;
                    nocoupon_text = [nocoupon_text stringByReplacingOccurrencesOfString:@"（" withString:@"\n（"];
                    self.headerLabel.text = nocoupon_text;
                }else{
                    [self.headerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.top.mas_equalTo(self.view).mas_offset(5);
                        make.left.mas_equalTo(self.view).mas_offset(10);
                        make.width.mas_equalTo(SCREEN_WIDTH-20);
                        make.height.mas_equalTo(1);
                    }];
                }
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"我的优惠券"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"我的优惠券"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的优惠券";
    
    [self setupRightItem];
    [self setupSubViews];
    
    [self requestCouponList];
}

- (void)setupRightItem{
    
    UIButton * descriptionButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 60, 30)];
    descriptionButton.titleLabel.font = DHSystemFontOfSize_12;
    [descriptionButton setTitle:@"领取说明" forState:UIControlStateNormal];
    [descriptionButton setTitleColor:FWTextColor_66739A forState:UIControlStateNormal];
    [descriptionButton addTarget:self action:@selector(descriptionButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:descriptionButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
}

#pragma mark - > 初始化视图
- (void)setupSubViews{
    
    self.headerLabel = [[UILabel alloc] init];
    self.headerLabel.numberOfLines = 0;
    self.headerLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.headerLabel.font = DHSystemFontOfSize_15;
    self.headerLabel.textColor = FWTextColor_12101D;
    self.headerLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.headerLabel];
    [self.headerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset(15);
        make.left.mas_equalTo(self.view).mas_offset(20);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    self.infoTableView = [[FWTableView alloc] init];
    self.infoTableView.delegate = self;
    self.infoTableView.dataSource = self;
    self.infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.infoTableView.rowHeight = UITableViewAutomaticDimension;
    self.infoTableView.estimatedRowHeight = 125*(SCREEN_WIDTH-20)/355;
    self.infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.infoTableView.isNeedEmptyPlaceHolder = YES;
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:@"还没有相关的内容哦~" attributes:Attributes];
    self.infoTableView.emptyDescriptionString = attributeString;
    [self.view addSubview:self.infoTableView];
    [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headerLabel.mas_bottom).mas_offset(10);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"MyTicktsCell";
    
    FWMyCouponCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell) {
        cell = [[FWMyCouponCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.vc = self;
    cell.couponModel = self.couponModel;
    [cell configForCellWithModel:self.dataSource[indexPath.row]];

    return cell;
}

#pragma mark - > 兑换码领取说明
- (void)descriptionButtonClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.htmlStr = self.couponModel.desc_url;
    WVC.pageType = WebViewTypeURL;
    [self.navigationController pushViewController:WVC animated:YES];
}

@end
