//
//  FWCarCertificationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarCertificationViewController.h"
#import "FWCarCertificationView.h"
#import "FWCarCertificationCell.h"
#import "FWAddCarViewController.h"
#import "FWCarDetailViewController.h"

@interface FWCarCertificationViewController ()<UITableViewDelegate,UITableViewDataSource,FWCarCertificationCellDelegate>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) UIView * placeHolderView;

@property (nonatomic, strong) UIButton * addButton;
@property (nonatomic, strong) UIView * footerView;
@property (nonatomic, strong) UIButton * footerAddButton;

@property (nonatomic, assign) BOOL isHideDelete;
@property (nonatomic, strong) UIBarButtonItem * rightBtnItem;

@property (nonatomic, strong) FWCarModel * carModel;

@end

@implementation FWCarCertificationViewController
@synthesize infoTableView;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 请求我的座驾列表
-(void)requestPersonalData{

    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID]
                              };
    
    [request startWithParameters:params WithAction:Get_my_cars WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.carModel = [FWCarModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.infoTableView.mj_header endRefreshing];
            
            if (self.dataSource.count > 0) {
                [self.dataSource removeAllObjects];
            }
            
            [self.dataSource addObjectsFromArray:self.carModel.list];
            
            [self dealWithArray];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)dealWithArray{
    
    if (self.dataSource.count >0){
        
        self.infoTableView.hidden = NO;
        self.placeHolderView.hidden = YES;
        self.addButton.hidden = YES;
        self.navigationItem.rightBarButtonItems = @[self.rightBtnItem];
        
        if (self.dataSource.count >= [self.carModel.car_number_limit integerValue]) {
            /* 座驾到上限了 */
            infoTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
        }else{
            self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 100);
            infoTableView.tableFooterView = self.footerView;
        }
        
        [self.infoTableView reloadData];
    }else{
        self.infoTableView.hidden = YES;
        self.placeHolderView.hidden = NO;
        self.addButton.hidden = NO;
        self.navigationItem.rightBarButtonItems = @[];
    }
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"我的座驾页"];
    [self requestPersonalData];
    
    self.isHideDelete = YES;
    [self setupRightButtonItem];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"我的座驾页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的座驾";
    self.isHideDelete = YES;
    
    UIButton * managerButton = [[UIButton alloc] init];
    managerButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [managerButton setTitle:@"管理" forState:UIControlStateNormal];
    managerButton.frame = CGRectMake(0, 0, 45, 40);
    [managerButton setTitleColor:FWTextColor_272727 forState:UIControlStateNormal];
    [managerButton addTarget:self action:@selector(managerButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:managerButton];
    
    [self setupPlaceHolderView];
    [self setupSubViews];
}

- (void)managerButtonClick{
    
    self.isHideDelete = !self.isHideDelete;
    
    [self setupRightButtonItem];
    [self.infoTableView reloadData];
}

- (void)setupRightButtonItem{
    
    UIButton * managerButton = [[UIButton alloc] init];
    managerButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [managerButton setTitleColor:FWTextColor_272727 forState:UIControlStateNormal];
    [managerButton addTarget:self action:@selector(managerButtonClick) forControlEvents:UIControlEventTouchUpInside];
    managerButton.frame = CGRectMake(0, 0, 45, 40);

    if (self.isHideDelete) {
        [managerButton setTitle:@"管理" forState:UIControlStateNormal];
    }else{
        [managerButton setTitle:@"取消" forState:UIControlStateNormal];
    }
    
    self.rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:managerButton];
    
    if (self.dataSource.count > 0) {
        self.navigationItem.rightBarButtonItems = @[self.rightBtnItem];
    }else{
        self.navigationItem.rightBarButtonItems = @[];
    }
}

#pragma mark - > 初始化视图
- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 162;
//    infoTableView.scrollEnabled = NO;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    infoTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-(64+FWCustomeSafeTop));

    self.footerView = [[UIView alloc] init];
    self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 100);
    infoTableView.tableFooterView = self.footerView;

    self.footerAddButton = [[UIButton alloc] init];
    self.footerAddButton.layer.cornerRadius = 18;
    self.footerAddButton.layer.masksToBounds = YES;
    self.footerAddButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.footerAddButton.backgroundColor = FWColor(@"616C91");
    [self.footerAddButton setTitle:@"添加座驾" forState:UIControlStateNormal];
    [self.footerAddButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.footerAddButton addTarget:self action:@selector(addCarButton) forControlEvents:UIControlEventTouchUpInside];
    [self.footerView addSubview:self.footerAddButton];
    self.footerAddButton.frame = CGRectMake((SCREEN_WIDTH-137)/2, 32, 137, 36);
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestPersonalData];
    }];
    
    infoTableView.hidden = YES;
    
    self.addButton = [[UIButton alloc] init];
    self.addButton.layer.cornerRadius = 18;
    self.addButton.layer.masksToBounds = YES;
    self.addButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    self.addButton.backgroundColor = FWColor(@"616C91");
    [self.addButton setTitle:@"添加座驾" forState:UIControlStateNormal];
    [self.addButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.addButton addTarget:self action:@selector(addCarButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.addButton];
    [self.addButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(137, 36));
        make.top.mas_equalTo(self.placeHolderView.mas_bottom).mas_offset(0);
    }];
}

- (void)setupPlaceHolderView{
    
    self.placeHolderView = [[UIView alloc] init];
    self.placeHolderView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.placeHolderView];
    [self.placeHolderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.centerY.mas_equalTo(self.view).mas_offset(-100);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_greaterThanOrEqualTo(50);
    }];
    
    UIImageView * imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.image = [UIImage imageNamed:@"empty"];
    [self.placeHolderView addSubview:imageView];
    [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.placeHolderView);
        make.top.mas_equalTo(self.placeHolderView).mas_offset(20);
        make.width.mas_equalTo(107);
        make.height.mas_equalTo(115);
    }];
    
    UILabel * placeholderLabel = [[UILabel alloc] init];
    placeholderLabel.text = @"还没有添加座驾哦~";
    placeholderLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    placeholderLabel.textColor = FWColor(@"959595");
    placeholderLabel.textAlignment = NSTextAlignmentCenter;
    [self.placeHolderView addSubview:placeholderLabel];
    [placeholderLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.placeHolderView);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(imageView.mas_bottom).mas_offset(20);
        make.bottom.mas_equalTo(self.placeHolderView.mas_bottom).mas_offset(-20);
    }];
    
    self.placeHolderView.hidden = NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"carCertificationCellID";
    
    FWCarCertificationCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWCarCertificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.vc = self;
    cell.delegate = self;
    if (indexPath.row < self.dataSource.count) {
        cell.deleteButton.tag = indexPath.row +7777;
        cell.deleteButton.hidden = self.isHideDelete;
        cell.mengcengView.hidden = self.isHideDelete;
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (!self.isHideDelete) {
        /* 显示删除时，不允许点击 */
        return;
    }
    
    if (indexPath.row >= self.dataSource.count) {
        return;
    }
    
    if (((FWCarListModel *)self.dataSource[indexPath.row]).car_desc.length <= 0) {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"座驾信息尚未完善，是否编辑？" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"暂不" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"编辑" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            DHWeakSelf;
            FWAddCarViewController * ACVC = [[FWAddCarViewController alloc] init];
            ACVC.type = @"edit";
            ACVC.isReFresh = ^(BOOL isRequest) {
                if (isRequest) {
                    [weakSelf requestPersonalData];
                }
            };
            ACVC.listModel = ((FWCarListModel *)self.dataSource[indexPath.row]);
            [self.navigationController pushViewController:ACVC animated:YES];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        FWCarDetailViewController * CDVC = [[FWCarDetailViewController alloc] init];
        CDVC.user_car_id = ((FWCarListModel *)self.dataSource[indexPath.row]).user_car_id;
        [self.navigationController pushViewController:CDVC animated:YES];
    }
}

#pragma mark - > 删除某一行
- (void)deleteButtonOnClick:(NSInteger)index WithCell:(FWCarCertificationCell *)cell{
    
    cell.deleteButton.enabled = NO;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        cell.deleteButton.enabled = YES;
    });
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"座驾信息一旦删除不可复原，确认删除吗？" preferredStyle:UIAlertControllerStyleAlert];
   
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"删除座驾" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self deleteCar:index];
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"暂不" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];

    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)deleteCar:(NSInteger)index{
    
    if (index >= self.dataSource.count) {
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    FWCarListModel * listModel = self.dataSource[index];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"user_car_id":listModel.user_car_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_delete_car WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [self.dataSource removeObjectAtIndex:index];
            [self.infoTableView reloadData];
            
            [self dealWithArray];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 添加座驾
- (void)addCarButton{
    
    FWAddCarViewController * ACVC = [[FWAddCarViewController alloc] init];
    ACVC.type = @"add";
    ACVC.isReFresh = ^(BOOL isRequest) {};
    [self.navigationController pushViewController:ACVC animated:YES];
}
@end
