//
//  FWCheckAndSignDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCheckAndSignDetailViewController.h"
#import "FWHomeNavigationView.h"
#import "FWCheckAndSignDetailCell.h"
#import "FWSeachPlayerViewController.h"
#import "FWDriverInfoManager.h"
#import "FWDriverInfoViewController.h"
#import "FWBaomingModel.h"
#import "FWScanCheckViewController.h"

static NSString * detailCellID = @"detailCellID";

@interface FWCheckAndSignDetailViewController ()<FWHomeNavigationViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FWCheckAndSignDetailCellDelegate>

@property (nonatomic, strong) FWHomeNavigationView * homeNavigationView;
@property (nonatomic, strong) UICollectionView * checkCollectionView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) UILabel * prepareLabel;

@end

@implementation FWCheckAndSignDetailViewController

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"签到/检录页"];
    
    self.dataSource = [[FWDriverInfoManager getDriverInfoManagerInfoJSONDataFromLocal] objectForKey:@"driver_list"];
    
    if (self.dataSource.count > 0) {
        self.prepareLabel.hidden = NO;
    }else{
        self.prepareLabel.hidden = YES;
    }
    
    [self.checkCollectionView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"签到/检录页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"签到/检录";
    
    UIButton * scanButton = [[UIButton alloc] init];
    scanButton.frame = CGRectMake(0, 0, 45, 40);
    [scanButton setImage:[UIImage imageNamed:@"check_scan"] forState:UIControlStateNormal];
    [scanButton addTarget:self action:@selector(scanButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:scanButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    
    [self setupSubViews];
}

- (void)setupSubViews{
 
    self.homeNavigationView = [[FWHomeNavigationView alloc] init];
    self.homeNavigationView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 120);
    self.homeNavigationView.delegate = self;
    self.homeNavigationView.userInteractionEnabled = YES;
    [self.view addSubview:self.homeNavigationView];

    self.homeNavigationView.searchButton.layer.cornerRadius = 5;
    [self.homeNavigationView.searchButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.top.mas_equalTo(self.homeNavigationView).mas_offset(48);
           make.centerX.mas_equalTo(self.homeNavigationView);
           make.left.mas_equalTo(self.homeNavigationView).mas_offset(33);
           make.right.mas_equalTo(self.homeNavigationView).mas_offset(-33);
           make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-66, 46));
    }];


    [self.homeNavigationView.searchImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.homeNavigationView.searchButton);
        make.left.mas_equalTo(self.homeNavigationView.searchButton).mas_offset(19);
        make.size.mas_equalTo(CGSizeMake(18, 16));
    }];

    self.homeNavigationView.searchLabel.textAlignment = NSTextAlignmentLeft;
    self.homeNavigationView.searchLabel.text = @"请输入车号或姓名";
    [self.homeNavigationView.searchLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.homeNavigationView.searchButton);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(self.homeNavigationView.searchButton);
        make.left.mas_equalTo(self.homeNavigationView.searchImageView.mas_right).mas_offset(15);
    }];

    self.prepareLabel = [[UILabel alloc] init];
    self.prepareLabel.text = @"准备签到/检录";
    self.prepareLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 17];
    self.prepareLabel.textColor = FWTextColor_272727;
    self.prepareLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.prepareLabel];
    self.prepareLabel.frame = CGRectMake(14, CGRectGetMaxY(self.homeNavigationView.frame), 200, 22);
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(5,14,5,14);
    flowLayout.minimumLineSpacing = 10;
    flowLayout.minimumInteritemSpacing = 10; // cell的纵向间距

    self.checkCollectionView = [[FWCollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.prepareLabel.frame)+20, SCREEN_WIDTH, SCREEN_HEIGHT-(20+CGRectGetMaxY(self.prepareLabel.frame))-30) collectionViewLayout:flowLayout];
    self.checkCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.checkCollectionView.dataSource = self;
    self.checkCollectionView.delegate = self;
    self.checkCollectionView.showsHorizontalScrollIndicator = NO;
    [self.checkCollectionView registerClass:[FWCheckAndSignDetailCell class] forCellWithReuseIdentifier:detailCellID];
    [self.view addSubview:self.checkCollectionView];
}

#pragma mark - > 签到/检录`s Delegate & DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWCheckAndSignDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:detailCellID forIndexPath:indexPath];
    
    cell.delegate = self;
    
    if (indexPath.item < self.dataSource.count) {
        cell.removeLabel.tag = 1090+indexPath.item;
        [cell configForAnalysisCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.item < self.dataSource.count) {
        
        NSMutableDictionary * dict = self.dataSource[indexPath.item];
        
        FWDriverInfoViewController * VC = [[FWDriverInfoViewController alloc] init];
        VC.type = @"2";
        VC.jianlu_info = [FWJianluInfoModel mj_objectWithKeyValues:dict];
        [self.navigationController pushViewController:VC animated:YES];
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"数据有误，请卸载重新安装或换手机" toController:self];
    }
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    /* 一行两个 */
    return CGSizeMake((SCREEN_WIDTH-38)/2, (SCREEN_WIDTH-38)/2 *101/169);
}

#pragma mark - > 移除第N个
- (void)removeItem:(NSInteger)index{
 
    NSMutableArray * tempArr = [self.dataSource mutableCopy];
    
    if (tempArr.count > index) {
        [tempArr removeObjectAtIndex:index];
        self.dataSource = [tempArr mutableCopy];
        [self.checkCollectionView reloadData];
        
        [FWDriverInfoManager saveDriverInfoManagerInfoJSONDataToLocalWithArray:self.dataSource];
    }
}

#pragma mark - > 搜索
- (void)searchButtonClick{
    
    FWSeachPlayerViewController * SPVC = [[FWSeachPlayerViewController alloc] init];
    [self.navigationController pushViewController:SPVC animated:YES];
}

#pragma mark - > 扫一扫
- (void)scanButtonClick{
    FWScanCheckViewController * vc = [[FWScanCheckViewController alloc] init];
    vc.fromType = @"driver_info";
    [self.navigationController pushViewController:vc animated:YES];
}


@end
