//
//  FWSeachPlayerViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWSearchBar.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWSeachPlayerViewController : FWBaseViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>

/**
 * 做navigation的view
 */
@property (nonatomic, strong) UIView * headerView;

/**
 * 取消按钮
 */
@property (nonatomic, strong) UIButton * cancelBtn;

/**
 * 搜索
 */
@property (nonatomic, strong) FWSearchBar * searchBar;

/**
 * 列表的tableview
 */
@property (nonatomic, strong) UITableView * listTableView;

@end

NS_ASSUME_NONNULL_END
