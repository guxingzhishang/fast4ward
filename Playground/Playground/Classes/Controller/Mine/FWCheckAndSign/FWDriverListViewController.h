//
//  FWDriverListViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWBaomingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWDriverListViewController : FWBaseViewController

@property (nonatomic, strong) FWBaominCheckListModel * baomingModel;


@end

NS_ASSUME_NONNULL_END
