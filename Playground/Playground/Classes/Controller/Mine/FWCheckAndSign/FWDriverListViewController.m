//
//  FWDriverListViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWDriverListViewController.h"
#import "FWDriverListCell.h"
#import "FWDriverListSegementView.h"
#import "FWDriverInfoViewController.h"
#import "FWEmptyView.h"

@interface FWDriverListViewController ()<UITableViewDelegate,UITableViewDataSource,FWDriverListSegementViewDelegate>

@property (nonatomic, assign) NSInteger  lastIndex;// 最后一次点击的segement

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) FWDriverListSegementView * segementView;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, strong) FWEmptyView * emptyView;

@end

@implementation FWDriverListViewController
@synthesize infoTableView;

- (void)requestResult:(BOOL)isLoadMoreData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"query":@"",
                                @"type":self.type,
                             };
    
    [request startWithParameters:params WithAction:Get_jianlu_search WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
//        [self.infoTableView.mj_footer endRefreshing];
//        [self.infoTableView.mj_header endRefreshing];
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (self.dataSource.count > 0) {
                [self.dataSource removeAllObjects];
            }
            
            self. baomingModel = [FWBaominCheckListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (self.baomingModel.list.count > 0) {
                self.emptyView.hidden = YES;
                self.infoTableView.hidden = NO;
                
                [self.dataSource addObjectsFromArray:self.baomingModel.list];
                [self.infoTableView reloadData];
                
                [self.infoTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            }else{
                self.emptyView.hidden = NO;
                self.infoTableView.hidden = YES;
            }

        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


- (NSMutableArray *)dataSource{
    
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手名单页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手名单页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"车手名单";
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.segementView = [[FWDriverListSegementView alloc]init];
    self.segementView.itemDelegate = self;
    [self.view addSubview:self.segementView];
    [self.segementView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 45));
        make.left.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view).mas_offset(0);
    }];
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 203;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.segementView.mas_bottom);
        make.bottom.mas_equalTo(self.view);
    }];
    
    self.emptyView = [[FWEmptyView alloc] init];
    [self.emptyView settingWithImageName:@"empty" WithTitle:@"暂无数据"];
    [self.view addSubview:self.emptyView];
    [self.emptyView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view).mas_offset(100);
        make.height.mas_equalTo(150);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
    self.emptyView.hidden = YES;
    
//    DHWeakSelf;
//    self.infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestResult:NO];
//    }];
//    self.infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestResult:YES];
//    }];
    
    [self.segementView deliverTitles:@[@"全部",@"查看未签到",@"查看未检录",@"查看未签到+检录"]];
    self.segementView.lastIndex = self.lastIndex;

    if (self.baomingModel) {
        [self.dataSource addObjectsFromArray:self.baomingModel.list];
        [self.infoTableView reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"DriverListCellID";

    FWDriverListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWDriverListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row < self.dataSource.count) {
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWDriverInfoViewController * VC = [[FWDriverInfoViewController alloc] init];
    VC.baoming_user_id = ((FWJianluInfoModel *)self.dataSource[indexPath.row]).baoming_user_id;
    VC.type = @"1";
    [self.navigationController pushViewController:VC animated:YES];
}


#pragma mark - > segement选择
-(void)segementItemClickTap:(NSInteger)index{
    
    if (self.lastIndex == index) {
        return;
    }
    
    self.lastIndex = index;
    
    self.type = @(index).stringValue;
    [self requestResult:NO];
}
@end
