//
//  FWLookDriverListViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWLookDriverListViewController.h"
#import "FWHomeNavigationView.h"
#import "FWSeachPlayerViewController.h"
#import "FWDriverListViewController.h"
#import "FWBaomingModel.h"

@interface FWLookDriverListViewController ()<FWHomeNavigationViewDelegate>

@property (nonatomic, strong) FWHomeNavigationView * homeNavigationView;
@property (nonatomic, strong) UIView * firstShadowView;
@property (nonatomic, strong) UIView * firstBgView;
@property (nonatomic, strong) FWBaominCheckListModel * baomingModel;

@end

@implementation FWLookDriverListViewController

- (void)requestResult{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                                @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"query":@"",
                                @"type":@"0",
                             };
    
    [request startWithParameters:params WithAction:Get_jianlu_search WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self. baomingModel = [FWBaominCheckListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self setupTopViews];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手名单页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手名单页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"车手名单";
    
    [self setupTopViews];
    [self setupSearchViews];
    
    [self requestResult];
}

- (void)setupTopViews{
 
    for (UIView * view in self.firstBgView.subviews) {
        [view removeFromSuperview];
    }
    
    if (!self.firstShadowView) {
        self.firstShadowView = [[UIView alloc] init];
    }
    self.firstShadowView.userInteractionEnabled = YES;
    self.firstShadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.firstShadowView.userInteractionEnabled = YES;
    self.firstShadowView.layer.cornerRadius = 5;
    self.firstShadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.firstShadowView.layer.shadowOpacity = 0.7;
    self.firstShadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.view addSubview:self.firstShadowView];
    [self.firstShadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset(10);
        make.left.mas_equalTo(self.view).mas_offset(33);
        make.right.mas_equalTo(self.view).mas_offset(-32);
        make.height.mas_equalTo(124);
        make.width.mas_equalTo(SCREEN_WIDTH-65);
    }];
      
    if (!self.firstBgView) {
        self.firstBgView = [[UIView alloc] init];
    }
    [self.firstShadowView addSubview:self.firstBgView];
    [self.firstBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.firstShadowView);
    }];
    
    NSMutableArray * leftArray = @[@"车手总数",@"已签/未签",@"已检录/未检录"].mutableCopy;
    NSMutableArray * rightArray = @[].mutableCopy;
    
    if (self.baomingModel.driver_total_count.length > 0) {
        [rightArray addObject:self.baomingModel.driver_total_count];
    }else{
        [rightArray addObject:@""];
    }
    
    if (self.baomingModel.driver_total_count_qiandao_y.length > 0 &&
        self.baomingModel.driver_total_count_qiandao_n.length > 0) {
        NSString * qiandao  = [NSString stringWithFormat:@"%@/%@",self.baomingModel.driver_total_count_qiandao_y,self.baomingModel.driver_total_count_qiandao_n];
        [rightArray addObject:qiandao];
    }else{
        [rightArray addObject:@""];
    }
    
    if (self.baomingModel.driver_total_count_jianlu_y.length > 0 &&
        self.baomingModel.driver_total_count_jianlu_n.length > 0) {
        NSString * jianlu  = [NSString stringWithFormat:@"%@/%@",self.baomingModel.driver_total_count_jianlu_y,self.baomingModel.driver_total_count_jianlu_n];
        [rightArray addObject:jianlu];
    }else{
        [rightArray addObject:@""];
    }
    
    for (int i = 0; i<leftArray.count; i++) {
        
        UILabel * leftLabel = [[UILabel alloc] init];
        leftLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12];
        leftLabel.textColor = FWTextColor_B6BCC4;
        leftLabel.text = leftArray[i];
        leftLabel.textAlignment = NSTextAlignmentLeft;
        [self.firstBgView addSubview:leftLabel];
        leftLabel.frame = CGRectMake(17, 16+i*35, 80, 35);
        
        UILabel * rightLabel = [[UILabel alloc] init];
        rightLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
        rightLabel.textColor = FWTextColor_222222;
        rightLabel.text = rightArray[i];
        rightLabel.numberOfLines = 0;
        rightLabel.textAlignment = NSTextAlignmentLeft;
        [self.firstBgView addSubview:rightLabel];
        [rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.firstBgView).mas_offset(-14);
            make.centerY.mas_equalTo(leftLabel);
            make.left.mas_equalTo(leftLabel.mas_right).mas_offset(5);
            make.width.mas_greaterThanOrEqualTo(10);
            make.height.mas_equalTo(28);
        }];
    }

    UIImageView * allImageView = [[UIImageView alloc] init];
    allImageView.image = [UIImage imageNamed:@"lookall"];
    allImageView.userInteractionEnabled = YES;
    [self.firstBgView addSubview:allImageView];
    [allImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.right.mas_equalTo(self.firstBgView);
        make.size.mas_equalTo(CGSizeMake(107, 48));
    }];
    [allImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(allImageViewClick)]];
}

- (void)setupSearchViews{
    self.homeNavigationView = [[FWHomeNavigationView alloc] init];
    self.homeNavigationView.delegate = self;
    self.homeNavigationView.userInteractionEnabled = YES;
    [self.view addSubview:self.homeNavigationView];
    [self.homeNavigationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 48));
        make.left.mas_equalTo(self.view).mas_offset(0);
        make.top.mas_equalTo(self.firstShadowView.mas_bottom).mas_offset(10);
    }];

    self.homeNavigationView.searchButton.layer.cornerRadius = 5;
    [self.homeNavigationView.searchButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.top.mas_equalTo(self.homeNavigationView).mas_offset(0);
           make.centerX.mas_equalTo(self.homeNavigationView);
           make.left.mas_equalTo(self.homeNavigationView).mas_offset(33);
           make.right.mas_equalTo(self.homeNavigationView).mas_offset(-33);
           make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-66, 46));
    }];


    [self.homeNavigationView.searchImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.homeNavigationView.searchButton);
        make.left.mas_equalTo(self.homeNavigationView.searchButton).mas_offset(19);
        make.size.mas_equalTo(CGSizeMake(18, 16));
    }];

    self.homeNavigationView.searchLabel.textAlignment = NSTextAlignmentLeft;
    self.homeNavigationView.searchLabel.text = @"请输入车号或姓名";
    self.homeNavigationView.searchLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 15.7];
    [self.homeNavigationView.searchLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.homeNavigationView.searchButton);
        make.width.mas_greaterThanOrEqualTo(20);
        make.height.mas_equalTo(self.homeNavigationView.searchButton);
        make.left.mas_equalTo(self.homeNavigationView.searchImageView.mas_right).mas_offset(15);
    }];
}

#pragma mark - > 查看全部
- (void)allImageViewClick{
    
    FWDriverListViewController * vc = [[FWDriverListViewController alloc] init];
    vc.baomingModel = self.baomingModel;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - > 搜索
- (void)searchButtonClick{
    
    FWSeachPlayerViewController * SPVC = [[FWSeachPlayerViewController alloc] init];
    [self.navigationController pushViewController:SPVC animated:YES];
}

@end
