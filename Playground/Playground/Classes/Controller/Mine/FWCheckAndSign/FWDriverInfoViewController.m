//
//  FWDriverInfoViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWDriverInfoViewController.h"
#import "FWDriverInfoView.h"
#import "FWDriverInfoManager.h"
#import "FWCheckAndSignDetailViewController.h"
#import "FWLookDriverListViewController.h"

@interface FWDriverInfoViewController ()
@property (nonatomic, strong) FWDriverInfoView * infoView;

@end

@implementation FWDriverInfoViewController


- (void)requestDriverInfo{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"baoming_user_id":self.baoming_user_id,
                              };
    [request startWithParameters:params WithAction:Get_jianlu_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            FWJianluModel * model = [FWJianluModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            model.jianlu_info.baoming_user_id = self.baoming_user_id;
            
            [self.infoView configForView:model.jianlu_info withType:self.type];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手信息页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手信息页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"车手信息";
    
    self.infoView = [[FWDriverInfoView alloc] init];
    self.infoView.vc = self;
    [self.view addSubview:self.infoView];
    [self.infoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];

    if ([self.type isEqualToString:@"1"]) {
        [self requestDriverInfo];
    }else{
        [self.infoView configForView:self.jianlu_info withType:self.type];
    }
}

- (void)backBtnClick{
    
    for (UIViewController * item in self.navigationController.viewControllers) {
      if ([item isKindOfClass:[FWCheckAndSignDetailViewController class]]) {
          [self.navigationController popToViewController:item animated:YES];
          return ;
      }
    }
    
    
    for (UIViewController * item in self.navigationController.viewControllers) {
      if ([item isKindOfClass:[FWLookDriverListViewController class]]) {
          [self.navigationController popToViewController:item animated:YES];
          return ;
      }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
