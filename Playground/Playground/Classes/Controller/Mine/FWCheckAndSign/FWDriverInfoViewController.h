//
//  FWDriverInfoViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWBaomingModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWDriverInfoViewController : FWBaseViewController
@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, strong) FWJianluInfoModel * jianlu_info;

@property (nonatomic, strong) NSString * type; // 1：正常请求  2：不需要请求，读取本地数据

@end

NS_ASSUME_NONNULL_END
