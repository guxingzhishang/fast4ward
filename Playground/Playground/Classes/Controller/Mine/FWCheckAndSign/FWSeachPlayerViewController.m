//
//  FWSeachPlayerViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWSeachPlayerViewController.h"
#import "FWSearchPlayerCell.h"
#import "FWBaomingModel.h"
#import "FWDriverInfoViewController.h"

@interface FWSeachPlayerViewController ()
@property (nonatomic, strong) NSMutableArray * searchArray;

@end

@implementation FWSeachPlayerViewController
@synthesize headerView;
@synthesize cancelBtn;
@synthesize searchBar;
@synthesize listTableView;

- (NSMutableArray *)searchArray{
    if (!_searchArray) {
        _searchArray = [[NSMutableArray alloc] init];
    }
    return _searchArray;
}


#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"搜索车手页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"搜索车手页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    self.fd_prefersNavigationBarHidden = YES;

    self.fd_interactivePopDisabled = YES;
    
    [self setupSubViews];
    
    // 添加通知监听见键盘弹出/退出
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAction:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - >  键盘监听事件
- (void)keyboardAction:(NSNotification*)sender{
    // 通过通知对象获取键盘frame: [value CGRectValue]
    NSDictionary *useInfo = [sender userInfo];
    NSValue *value = [useInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // <注意>具有约束的控件通过改变约束值进行frame的改变处理
    if([sender.name isEqualToString:UIKeyboardWillShowNotification]){
        listTableView.frame = CGRectMake(0, CGRectGetMaxY(headerView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(headerView.frame)-[value CGRectValue].size.height);
    }else{
        listTableView.frame = CGRectMake(0, CGRectGetMaxY(headerView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(headerView.frame));
    }
}

#pragma mark - > 视图初始化
- (void)setupSubViews{
    
    [self setupNavigationView];
    [self setupTableView];
}

- (void)setupNavigationView{
    
    headerView = [[UIView alloc] init];
    [self.view addSubview:headerView];
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 84+FWCustomeSafeTop);
    
    searchBar = [[FWSearchBar alloc] initWithFrame:CGRectMake(10, 25+FWCustomeSafeTop, SCREEN_WIDTH-70, 40)];
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault;
    searchBar.barTintColor = [UIColor whiteColor];
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    searchBar.tintColor = FWTextColor_A6A6A6;
    searchBar.placeholder = @"请输入车号或姓名";
    [headerView addSubview:searchBar];
    
    [self removeBorder:searchBar];

    cancelBtn = [[UIButton alloc] init];
    [cancelBtn setImage:[UIImage imageNamed:@"search_cancel"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:cancelBtn];
    [cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(headerView).mas_offset(-10);
        make.centerY.mas_equalTo(searchBar);
        make.size.mas_equalTo(CGSizeMake(40, 30));
    }];
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = FWViewBackgroundColor_F1F1F1;
    [headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(headerView);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(headerView.mas_bottom);
    }];
    
    [self.searchBar becomeFirstResponder];
}

- (void)setupTableView{
    
    listTableView = [[UITableView alloc] init];
    listTableView.delegate = self;
    listTableView.dataSource = self;
    listTableView.rowHeight = 60;
    listTableView.backgroundColor = [UIColor clearColor];
    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view  addSubview:listTableView];
    listTableView.frame = CGRectMake(0, CGRectGetMaxY(headerView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(headerView.frame));
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, FWSafeBottom+10)];
    footerView.backgroundColor =  [UIColor clearColor];
    listTableView.tableFooterView = footerView;
    
    listTableView.hidden = YES;
}

#pragma mark - > tableView  Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.searchArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"tagsListID";

    FWSearchPlayerCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWSearchPlayerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.searchArray.count) {
        [cell configForCell:self.searchArray[indexPath.row]];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.searchBar endEditing:YES];
    
    tableView.userInteractionEnabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        tableView.userInteractionEnabled = YES;
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        FWBaomingListModel * model = self.searchArray[indexPath.row];

        FWDriverInfoViewController * DIVC = [[FWDriverInfoViewController alloc] init];
        DIVC.baoming_user_id = model.baoming_user_id;
        DIVC.type = @"1";
        [self.navigationController pushViewController:DIVC animated:YES];
    });
}


#pragma mark - > searchBar delegate 实时搜索标签
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"query":searchBar.text
                              };
    
    if (searchBar.text.length >0) {
        listTableView.hidden = NO;
        
        [request startWithParameters:params WithAction:Get_search_drivers  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if (self.searchArray.count >0) {
                    [self.searchArray removeAllObjects];
                }
                
                
                FWBaomingModel * model = [FWBaomingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                self.searchArray = model.list;
                
                if (model.list.count <= 0) {
                    [[FWHudManager sharedManager] showErrorMessage:@"无当前车手，请仔细核对！" toController:self];
                }
                
                [listTableView reloadData];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }else{
        listTableView.hidden = YES;
    }
}

#pragma mark - > 取消搜索
- (void)cancelBtnOnClick:(UIButton *)sender{
   
    [self.searchBar endEditing:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}


#pragma mark - > 去掉输入框边框
- (void)removeBorder:(UISearchBar * )searchBar{
    
    if (@available(iOS 13.0, *)) {
        UITextField *textField = (UITextField *)self.searchBar.searchTextField;
        
        textField.font = DHSystemFontOfSize_14;
        // 设置 Search 按钮可用
        textField.enablesReturnKeyAutomatically = NO;
        
        // 改变输入框背景色
        textField.subviews[0].backgroundColor = [UIColor clearColor];
        textField.layer.cornerRadius = 2;
        textField.layer.masksToBounds = YES;
    }else{
        //设置背景图是为了去掉上下黑线
        self.searchBar.backgroundImage = [[UIImage alloc] init];
        // 设置SearchBar的颜色主题为白色
        self.searchBar.barTintColor = FWViewBackgroundColor_F1F1F1;

        UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
        if (searchField) {
            [searchField setBackgroundColor:FWViewBackgroundColor_F1F1F1];
            searchField.layer.cornerRadius = 2;
            searchField.layer.masksToBounds = YES;
        }
    }
}


@end
