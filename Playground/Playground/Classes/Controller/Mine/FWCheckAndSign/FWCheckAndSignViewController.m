//
//  FWCheckAndSignViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCheckAndSignViewController.h"
#import "FWCheckAndSignDetailViewController.h"
#import "FWLookDriverListViewController.h"

@interface FWCheckAndSignViewController ()

@end

@implementation FWCheckAndSignViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"签到/检录";
    
    NSArray * iconArray = @[@"driver_sign_check",@"driver_list"];
    
    for (int i = 0; i < 2; i ++) {
        
        UIView * shadowView = [[UIView alloc] init];
        shadowView.userInteractionEnabled = YES;
        shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
        shadowView.userInteractionEnabled = YES;
        shadowView.layer.cornerRadius = 5;
        shadowView.layer.shadowOffset = CGSizeMake(0, 0);
        shadowView.layer.shadowOpacity = 0.7;
        shadowView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
        [self.view addSubview:shadowView];
        [shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view).mas_offset(16);
            make.left.mas_equalTo(self.view).mas_offset(30+i*(10+(SCREEN_WIDTH-70)/2));
            make.height.mas_equalTo((143*(SCREEN_WIDTH-70)/2)/153);
            make.width.mas_equalTo((SCREEN_WIDTH-70)/2);
        }];
        
        UIImageView * bgView = [[UIImageView alloc] init];
        bgView.tag = 10090+i;
        bgView.userInteractionEnabled = YES;
        bgView.image = [UIImage imageNamed:iconArray[i]];
        [shadowView addSubview:bgView];
        [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(shadowView);
        }];
        
        [bgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgClick:)]];
    }
}

- (void)bgClick:(UITapGestureRecognizer *)tap{
    
    NSInteger index = tap.view.tag - 10090;
    
    if (index == 0) {
        /* 车手签到/检录 */
        FWCheckAndSignDetailViewController * CASDVC = [[FWCheckAndSignDetailViewController alloc] init];
        [self.navigationController pushViewController:CASDVC animated:YES];
    }else{
        /* 查看车手名单 */
        [self.navigationController pushViewController:[FWLookDriverListViewController new] animated:YES];
    }
}

@end
