//
//  FWMyTicketsViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyTicketsViewController.h"
#import "FWMyTicketsCell.h"

@interface FWMyTicketsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) UILabel * headerLabel;
@property (nonatomic, strong) FWTicketsModel * ticketsModel;
@property (nonatomic, strong) NSMutableArray * dataSource;

@end

@implementation FWMyTicketsViewController

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 请求门票列表
- (void)requestTicketsList{
    
    FWMemberRequest * request = [[FWMemberRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_vip_ticket_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.ticketsModel = [FWTicketsModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            NSString * ticket_text = self.ticketsModel.ticket_text;
            ticket_text = [ticket_text stringByReplacingOccurrencesOfString:@"（" withString:@"\n（"];

            self.headerLabel.text = ticket_text;
            
            if (self.ticketsModel.ticket_list.count > 0) {
                [self.dataSource addObjectsFromArray:self.ticketsModel.ticket_list];
                [self.infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"我的门票页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"我的门票页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"我的门票";
    
    [self setupSubViews];
    
    [self requestTicketsList];
}

- (void)setupRightItem{
    
    UIButton * descriptionButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 95, 30)];
    descriptionButton.titleLabel.font = DHSystemFontOfSize_12;
    [descriptionButton setTitle:@"兑换码领取说明" forState:UIControlStateNormal];
    [descriptionButton setTitleColor:FWTextColor_66739A forState:UIControlStateNormal];
    [descriptionButton addTarget:self action:@selector(descriptionButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:descriptionButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
}

#pragma mark - > 初始化视图
- (void)setupSubViews{
    
    self.headerLabel = [[UILabel alloc] init];
    self.headerLabel.numberOfLines = 0;
    self.headerLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.headerLabel.font = DHSystemFontOfSize_15;
    self.headerLabel.textColor = FWTextColor_12101D;
    self.headerLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.headerLabel];
    [self.headerLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset(15);
        make.left.mas_equalTo(self.view).mas_offset(20);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    
    self.infoTableView = [[UITableView alloc] init];
    self.infoTableView.delegate = self;
    self.infoTableView.dataSource = self;
    self.infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.infoTableView.rowHeight = UITableViewAutomaticDimension;
    self.infoTableView.estimatedRowHeight = 123*(SCREEN_WIDTH-10)/365;
    self.infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.infoTableView];
    [self.infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headerLabel.mas_bottom).mas_offset(10);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-10);
        make.width.mas_equalTo(SCREEN_WIDTH);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"MyTicktsCell";
    
    FWMyTicketsCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell) {
        cell = [[FWMyTicketsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.vc = self;
    cell.ticketsModel = self.ticketsModel;
    [cell configForCellWithModel:self.dataSource[indexPath.row]];
    
    return cell;
}

#pragma mark - > 兑换码领取说明
- (void)descriptionButtonClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.htmlStr = self.ticketsModel.desc_url;
    WVC.pageType = WebViewTypeURL;
    [self.navigationController pushViewController:WVC animated:YES];
}
@end
