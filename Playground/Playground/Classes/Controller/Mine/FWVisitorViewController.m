//
//  FWVisitorViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVisitorViewController.h"
#import "FWVisitorSelectBarView.h"
#import "FWVisitorNameListCell.h"
#import "FWVisitorNameListHeaderView.h"
#import "FWVisitorAnalysisCell.h"
#import "FWVisitorDetailViewController.h"

@interface FWVisitorViewController ()<FWVisitorSelectBarViewDelegate>

@property (nonatomic, strong) NSMutableArray * nameListDataSource;
@property (nonatomic, strong) NSMutableArray * analysisCarDataSource;
@property (nonatomic, strong) NSMutableArray * analysisAreaDataSource;

@property (nonatomic, strong) FWVisitorSelectBarView * selectBarView;
@property (nonatomic, strong) FWVisitorNameListHeaderView * headerView;

@property (nonatomic, strong) UIScrollView * mainScrollView;

@property (nonatomic, strong)  FWTableView * nameListTableView;
@property (nonatomic, strong)  FWCollectionView * analysisCollectionView;

@property (nonatomic, assign)  NSInteger nameListPageNum;
@property (nonatomic, assign)  NSInteger analysisCarPageNum;
@property (nonatomic, assign)  NSInteger analysisAreaPageNum;

// 以下代表是否第一次请求过，用来第一次切换标签时用
@property (nonatomic, assign) BOOL  isnameListRequest;
@property (nonatomic, assign) BOOL  isanalysisRequest;

// 访客分析排序按钮
@property (nonatomic, strong) UIButton * carSortButton;
@property (nonatomic, strong) UIButton * areaSortButton;

@property (nonatomic, strong) FWVisitorModel * visitorModel;
@property (nonatomic, strong) FWVisitorAnalysisModel * visitorAnalysisModel;

// 1、车型 2、地区
@property (nonatomic, strong) NSString * type;

@property (nonatomic, assign) BOOL  isAreaRefresh;

@end

static NSString * AnalysisID = @"AnalysisID";

@implementation FWVisitorViewController
@synthesize nameListDataSource;
@synthesize analysisCarDataSource;
@synthesize analysisAreaDataSource;
@synthesize selectBarView;
@synthesize mainScrollView;
@synthesize nameListTableView;
@synthesize analysisCollectionView;
@synthesize nameListPageNum;
@synthesize analysisCarPageNum;
@synthesize analysisAreaPageNum;
@synthesize isnameListRequest;
@synthesize isanalysisRequest;
@synthesize headerView;
@synthesize carSortButton;
@synthesize areaSortButton;
@synthesize visitorModel;
@synthesize visitorAnalysisModel;

#pragma mark - > 请求访客名单
- (void)requestNameListDataWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.nameListPageNum = 1;
        
        if (self.nameListDataSource.count > 0 ) {
            [self.nameListDataSource removeAllObjects];
        }
    }else{
        self.nameListPageNum +=1;
    }
    
    FWVisitorRequest * request = [[FWVisitorRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.nameListPageNum).stringValue,
                              @"pagesize":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_visitor_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [nameListTableView.mj_footer endRefreshing];
            }else{
                [nameListTableView.mj_header endRefreshing];
            }
            
            visitorModel = [FWVisitorModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            headerView.todayVisivorLabel.text = [NSString stringWithFormat:@"今日访客：%@", visitorModel.visitor_count_today];
            headerView.totalVisivorLabel.text = [NSString stringWithFormat:@"累计访客：%@", visitorModel.visitor_count_all];

            [self.nameListDataSource addObjectsFromArray:visitorModel.list_visitor];
            
            if([visitorModel.list_visitor count] == 0 && self.nameListPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [nameListTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求访客分析
- (void)requestAnalysisDataWithLoadMoredData:(BOOL)isLoadMoredData{
    
    NSInteger tempPageNum = 1;
    
    if ([self.type isEqualToString:@"1"]) {
        if (isLoadMoredData == NO) {
            self.analysisCarPageNum = 1;
            
            if (self.analysisCarDataSource.count > 0 ) {
                [self.analysisCarDataSource removeAllObjects];
            }
        }else{
            self.analysisCarPageNum +=1;
        }
        
        tempPageNum = self.analysisCarPageNum;
    }else{
        if (isLoadMoredData == NO) {
            self.analysisAreaPageNum = 1;
            
            if (self.analysisAreaDataSource.count > 0 ) {
                [self.analysisAreaDataSource removeAllObjects];
            }
        }else{
            self.analysisAreaPageNum +=1;
        }
        
        tempPageNum = self.analysisAreaPageNum;
    }
    
    
    FWVisitorRequest *request = [[FWVisitorRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"type":self.type,
                              @"page":@(tempPageNum).stringValue,
                              @"page_size":@"20",
                              };
    request.isNeedShowHud = YES;
    
    [request startWithParameters:params WithAction:Get_visitor_count_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [analysisCollectionView.mj_footer endRefreshing];
            }else{
                [analysisCollectionView.mj_header endRefreshing];
            }
            
            if ([self.type isEqualToString:@"1"]) {
                visitorAnalysisModel = [FWVisitorAnalysisModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

                [self.analysisCarDataSource addObjectsFromArray:visitorAnalysisModel.list_visitor];
                
                if([visitorAnalysisModel.list_visitor count] == 0 &&
                   tempPageNum != 1){
                    
                    [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
                    return ;
                }
            }else{
                visitorAnalysisModel = [FWVisitorAnalysisModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

                [self.analysisAreaDataSource addObjectsFromArray:visitorAnalysisModel.list_visitor];
                
                if([visitorAnalysisModel.list_visitor count] == 0 &&
                   tempPageNum != 1){
                    
                    [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
                    return ;
                }
            }
            [analysisCollectionView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"访客页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"访客页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.type = @"1";
    self.isAreaRefresh = NO;
    
//    self.title = @"最近访客";
    
    self.nameListDataSource = @[].mutableCopy;
    self.analysisCarDataSource = @[].mutableCopy;
    self.analysisAreaDataSource = @[].mutableCopy;

    [self setupSelectBarView];
    [self setupSubViews];
    
    [self requestNameListDataWithLoadMoredData:NO];
}

#pragma mark - > 初始化视图
- (void)setupSelectBarView{
    selectBarView = [[FWVisitorSelectBarView alloc] init];
    selectBarView.delegate = self;
    selectBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
    [self.view addSubview:selectBarView];
}

- (void)setupSubViews{
    
    mainScrollView = [[UIScrollView alloc] init];
    mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(selectBarView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(selectBarView.frame)-49-FWSafeBottom);
    mainScrollView.pagingEnabled = YES;
    mainScrollView.delegate = self;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:mainScrollView];
    mainScrollView.contentSize = CGSizeMake(4*SCREEN_WIDTH, 0);
    
    [self createnameListTableView];
    [self createSortButton];
    [self createanalysisCollectionView];
}

#pragma mark - > 初始化粉丝列表
-(void)createnameListTableView{
    
    headerView = [[FWVisitorNameListHeaderView alloc] init];
    headerView.vc = self;
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
    [mainScrollView addSubview:headerView];
    
    nameListTableView = [[FWTableView alloc] init];
    nameListTableView.delegate = self;
    nameListTableView.dataSource = self;
    nameListTableView.rowHeight = 69;
    nameListTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    nameListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    nameListTableView.frame = CGRectMake(0, CGRectGetMaxY(headerView.frame), SCREEN_WIDTH, CGRectGetHeight(mainScrollView.frame)-CGRectGetHeight(headerView.frame));
    [mainScrollView addSubview:nameListTableView];
    
    nameListTableView.isNeedEmptyPlaceHolder = YES;
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:@"还没有相关的内容哦~" attributes:Attributes];
    nameListTableView.emptyDescriptionString = attributeString;
    nameListTableView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_FFFFFF;
    
    DHWeakSelf;
    
    nameListTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestNameListDataWithLoadMoredData:NO];
    }];
    nameListTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestNameListDataWithLoadMoredData:YES];
    }];
}

#pragma mark - > 初始化点赞列表
- (void)createanalysisCollectionView{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(0,20,20,20);
    flowLayout.minimumLineSpacing = 30;
    
    analysisCollectionView = [[FWCollectionView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, CGRectGetMaxY(areaSortButton.frame)+15, SCREEN_WIDTH, CGRectGetHeight(mainScrollView.frame)-(20+CGRectGetHeight(areaSortButton.frame))) collectionViewLayout:flowLayout];
    analysisCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    analysisCollectionView.dataSource = self;
    analysisCollectionView.delegate = self;
    analysisCollectionView.showsHorizontalScrollIndicator = NO;
    [analysisCollectionView registerClass:[FWVisitorAnalysisCell class] forCellWithReuseIdentifier:AnalysisID];
    analysisCollectionView.isNeedEmptyPlaceHolder = YES;
    NSString * attetionTitle = @"还没有关注的内容哦~";
    NSDictionary *attetionAttributes = @{
                                         NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                         NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attentionString = [[NSMutableAttributedString alloc] initWithString:attetionTitle attributes:attetionAttributes];
    analysisCollectionView.emptyDescriptionString = attentionString;
    analysisCollectionView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_FFFFFF;
    [mainScrollView addSubview:analysisCollectionView];

    
    DHWeakSelf;
    analysisCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestAnalysisDataWithLoadMoredData:NO];
    }];
    analysisCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestAnalysisDataWithLoadMoredData:YES];
    }];
}

#pragma mark - > 初始化访客分析排序按钮
- (void)createSortButton{
    
    areaSortButton = [self setupSortButtonWithName:@"地区排序" WithTag:7001 WithIsSelect:NO];
    areaSortButton.frame = CGRectMake(2*SCREEN_WIDTH-20-90, 14, 90, 24);
    [mainScrollView addSubview:areaSortButton];
    
    carSortButton = [self setupSortButtonWithName:@"车型排序" WithTag:7000 WithIsSelect:YES];
    carSortButton.frame = CGRectMake(CGRectGetMinX(areaSortButton.frame)-101, CGRectGetMinY(areaSortButton.frame), CGRectGetWidth(areaSortButton.frame), CGRectGetHeight(areaSortButton.frame));
    
    [mainScrollView addSubview:carSortButton];
}


#pragma mark - > 初始化按钮模型
- (UIButton *)setupSortButtonWithName:(NSString *)name WithTag:(NSInteger)index WithIsSelect:(BOOL)isSelect{
    
    UIButton * btn = [[UIButton alloc] init];
    btn.tag = index;
    btn.selected = isSelect;
    btn.layer.cornerRadius = 2;
    btn.layer.masksToBounds = YES;
    btn.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:13];
    [btn setTitle:name forState:UIControlStateNormal];
    [btn setBackgroundImage:[FWClearColor image] forState:UIControlStateNormal];
    [btn setBackgroundImage:[FWTextColor_222222 image] forState:UIControlStateSelected];
    [btn setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
    [btn setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateSelected];
    [btn addTarget:self action:@selector(sortButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    if (isSelect) {
        btn.layer.borderColor = FWClearColor.CGColor;
        btn.layer.borderWidth = 0;
    }else{
        btn.layer.borderColor = FWTextColor_222222.CGColor;
        btn.layer.borderWidth = 1;
    }
    
    return btn;
}

- (void)sortButtonClick:(UIButton *)sender{
    
    sender.selected = !sender.selected;
    
    if (sender == carSortButton) {
        if (sender.selected) {
            /* 从未选中到选中，将另一个按钮变成未选中
             并进行数据请求 */
            areaSortButton.selected = NO;
            
            carSortButton.layer.borderColor = FWClearColor.CGColor;
            carSortButton.layer.borderWidth = 0;
            
            areaSortButton.layer.borderColor = FWTextColor_222222.CGColor;
            areaSortButton.layer.borderWidth = 1;
            
            self.type = @"1";
            [analysisCollectionView reloadData];
        }else{
            // 从选中变成未选中,做不任何操作，在变成之前状态
            sender.selected = !sender.selected;
            return;
        }
    }else if (sender == areaSortButton){
        if (sender.selected) {
            /* 从未选中到选中，将另一个按钮变成未选中
             并进行数据请求 */
            carSortButton.selected = NO;
            
            areaSortButton.layer.borderColor = FWClearColor.CGColor;
            areaSortButton.layer.borderWidth = 0;
            
            carSortButton.layer.borderColor = FWTextColor_222222.CGColor;
            carSortButton.layer.borderWidth = 1;
            
            self.type = @"2";

            if (!self.isAreaRefresh) {
                self.isAreaRefresh = YES;
                [self requestAnalysisDataWithLoadMoredData:NO];
            }
            [analysisCollectionView reloadData];
        }else{
            // 从选中变成未选中,做不任何操作，在变成之前状态
            sender.selected = !sender.selected;
            return;
        }
    }
}

#pragma mark - > 访客名单列表`s Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger number = 0;
    number = self.nameListDataSource.count?self.nameListDataSource.count:0;
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"VistiorNameListCellID";
    
    FWVisitorNameListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWVisitorNameListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.vc = self;
    [cell cellConfigureFor:self.nameListDataSource[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWVisitorNameListModel * visitorModel = self.nameListDataSource[indexPath.row];
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = visitorModel.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}

#pragma mark - > 访客分析`s Delegate & DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([self.type isEqualToString:@"1"]) {
        return self.analysisCarDataSource.count;
    }else{
        return self.analysisAreaDataSource.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWVisitorAnalysisCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:AnalysisID forIndexPath:indexPath];
    
    if ([self.type isEqualToString:@"1"]) {
        [cell configForAnalysisCell:self.analysisCarDataSource[indexPath.row]];
    }else{
        [cell configForAnalysisCell:self.analysisAreaDataSource[indexPath.row]];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FWVisitorDetailViewController * VDVC = [[FWVisitorDetailViewController alloc] init];
    FWVisitorAnalysisListModel * model  ;
    
    if ([self.type isEqualToString:@"1"]) {
        model = self.analysisCarDataSource[indexPath.row];
    }else{
        model = self.analysisAreaDataSource[indexPath.row];
    }

    if (carSortButton.selected) {
        VDVC.type = @"1";
        VDVC.carModel = model;
        VDVC.currentTitle = model.car_style;
    }else if (areaSortButton.selected){
        VDVC.type = @"2";
        VDVC.proviceModel = model;
        VDVC.currentTitle = model.province;
    }
    [self.navigationController pushViewController:VDVC animated:YES];
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    /* 一行两个 */
    return CGSizeMake((SCREEN_WIDTH-60)/2, SCREEN_WIDTH *46/375);
    
    /* 一行一个 */
//    return CGSizeMake((SCREEN_WIDTH-40), SCREEN_WIDTH *46/375);
}


#pragma mark - > 切换标签
- (void)didClickHeadButton:(NSInteger)index{
    
    if (index == 0) {
        
        selectBarView.nameListButton.selected = YES;
        selectBarView.analysisButton.selected = NO;
        
        selectBarView.shadowView.frame = CGRectMake(CGRectGetMinX(selectBarView.nameListButton.frame)+(CGRectGetWidth(selectBarView.nameListButton.frame)-32)/2, CGRectGetMaxY(selectBarView.nameListButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if (index == 1){
        
        selectBarView.nameListButton.selected = NO;
        selectBarView.analysisButton.selected = YES;
        
        selectBarView.shadowView.frame = CGRectMake(CGRectGetMinX(selectBarView.analysisButton.frame)+(CGRectGetWidth(selectBarView.analysisButton.frame)-32)/2, CGRectGetMaxY(selectBarView.analysisButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);

        if (!self.isnameListRequest) {
            self.isnameListRequest = YES;
            [self requestAnalysisDataWithLoadMoredData:NO];
        }
    }
    
    //点击btn的时候scrollowView的contentSize发生变化
    [self.mainScrollView setContentOffset:CGPointMake(index *SCREEN_WIDTH, 0) animated:YES];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (self.mainScrollView == scrollView){
        
        float scrollViewX = scrollView.contentOffset.x;
        if (scrollViewX == 0) {
            [self didClickHeadButton:0];
        }else if (scrollViewX == SCREEN_WIDTH){
            [self didClickHeadButton:1];
        }
    }
}

@end
