//
//  FWSettingViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/23.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWSettingViewController.h"
#import "FWPhoneViewController.h"
#import "FWThirdBindingViewController.h"
#import "FWFeedBackViewController.h"
#import "FWLogoutRequest.h"
#import "WdCleanCaches.h"
#import "FWMineRequest.h"
#import "FWEditInformationViewController.h"
#import "FWSettingCell.h"

#import "FWConversationViewController.h"
#import "FWCheckAndSignViewController.h"
#import "FWTicketCheckingViewController.h"
#import "FWGoldenCheckingViewController.h"


#define CELLHEIGHT 40
static NSString * signCheck = @"签到/检录";
static NSString * ticketCheck = @"门票核销";
static NSString * goldenCheck = @"金币核销";

@interface FWSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) NSString * mobileState;
@property (nonatomic, strong) NSString * wxState;

@property (nonatomic, assign) BOOL isTicket;
@property (nonatomic, assign) BOOL isSign;
@property (nonatomic, assign) BOOL isGolden;

@end

@implementation FWSettingViewController
@synthesize settingDict;
@synthesize settingModel;
@synthesize infoTableView;
@synthesize mobileState;
@synthesize wxState;

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    mobileState = @"未绑定";
    wxState = @"未绑定";
    
    [self trackPageBegin:@"设置页"];

    if ([[GFStaticData getObjectForKey:kTagUserKeyMobileState] integerValue] == 1) {
        mobileState = [GFStaticData getObjectForKey:kTagUserKeyPhone];
    }
    
    if ([[GFStaticData getObjectForKey:kTagUserKeyWXStatus] integerValue] == 1) {
        wxState = @"已绑定";
    }
    
    if (infoTableView) {
        [infoTableView reloadData];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"设置页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"设置";
    self.view.backgroundColor = FWViewBackgroundColor_EEEEEE;
    settingDict = @{};
    
    self.dataSource = @[].mutableCopy;
    
    NSMutableArray * firstArray = @[@"金币核销",
                                    @"签到/检录",
                                    @"门票核销",
                                    @"联系管理员",
                                    @"修改个人资料",
                                    @"修改绑定手机号",
                                    @"绑定第三方登录",
                                    @"使用帮助",
                                    @"给我们提意见",
                                    @"社区规范",
                                    @"隐私政策",
                                    @"关于我们",
                                    @"版本更新"].mutableCopy;
    NSMutableArray * secondArray = @[@"清理缓存"].mutableCopy;
    
    if(![self.mineModel.gold_write_off_status isEqualToString:@"1"]){
        /* 金币核销 */
        self.isGolden = NO;
        NSInteger index = [firstArray indexOfObject:goldenCheck];
        
        [firstArray removeObjectAtIndex:index];
    }else{
        self.isGolden = YES;
    }
    
    if(![self.mineModel.ticket_check isEqualToString:@"1"]){
        /* 门票核销 */
        self.isTicket = NO;
        NSInteger index = [firstArray indexOfObject:ticketCheck];
        
        [firstArray removeObjectAtIndex:index];
    }else{
        self.isTicket = YES;
    }

    if([self.mineModel.jianlu_check isEqualToString:@"1"]||
       [self.mineModel.qiandao_check isEqualToString:@"1"]){
        self.isSign = YES;
    }else{
        /* 签到/检录 */
        self.isSign = NO;
        NSInteger index = [firstArray indexOfObject:signCheck];
        
        [firstArray removeObjectAtIndex:index];
    }

    [self.dataSource addObject:firstArray];
    [self.dataSource addObject:secondArray];

    [self setupSubViews];
    
    [self dealWithData];
}

- (void)dealWithData{
    
    [GFStaticData saveObject:settingModel.newest_version forKey:MineUpdateVersion];
    
    if ([settingModel.show_update isEqualToString:@"2"]) {
        /* 审核阶段，*/
        NSMutableArray * firstArr = [self.dataSource firstObject];
        [firstArr removeLastObject];
        [self.dataSource replaceObjectAtIndex:0 withObject:firstArr];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [infoTableView reloadData];
    });
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
//    infoTableView.scrollEnabled = NO;
    infoTableView.backgroundColor = FWBgColor_Lightgray_F7F8FA;
    infoTableView.rowHeight = 40;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-74-FWSafeBottom);
    }];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];
    infoTableView.tableFooterView = footerView;
    
    UIButton * signoutButton = [[UIButton alloc] init];
    signoutButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
    signoutButton.titleLabel.font = DHSystemFontOfSize_14;
    [signoutButton setTitle:@"退  出" forState:UIControlStateNormal];
    [signoutButton setTitleColor:DHRedColorff6f00 forState:UIControlStateNormal];
    [signoutButton addTarget:self action:@selector(signoutButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signoutButton];
    [signoutButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 44+FWSafeBottom));
        make.bottom.mas_equalTo(self.view);
    }];
    
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    
    UILabel * versionLabel = [[UILabel alloc] init];
    versionLabel.text = [NSString stringWithFormat:@"版本号 V%@",[infoDictionary objectForKey:@"CFBundleShortVersionString"]];
    versionLabel.font = DHSystemFontOfSize_12;
    versionLabel.textColor = FWTextColor_646464;
    versionLabel.backgroundColor = FWBgColor_Lightgray_F7F8FA;
    versionLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:versionLabel];
    [versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 30));
        make.bottom.mas_equalTo(signoutButton.mas_top);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [(NSMutableArray *)self.dataSource[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"";
    
    FWSettingCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWSettingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if (indexPath.section == 0) {
        
        NSArray * firstArr = self.dataSource[indexPath.section];
        cell.titleLabel.text = firstArr[indexPath.row];
        cell.arrowImageView.hidden = NO;

        [cell.rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(cell.arrowImageView.mas_left).mas_offset(-15);
            make.centerY.mas_equalTo(cell.arrowImageView);
            make.height.mas_equalTo(20);
        }];
        
        NSInteger selectIndex = 0;
        
        if (self.isTicket && self.isSign && self.isGolden) {
            /* 有门票核销 、 签到/检录 、金币核销*/
        }else if (self.isGolden && self.isTicket && !self.isSign){
            /* 只有金币和门票核销 */
            selectIndex = -1;
        }else if (self.isGolden && !self.isTicket && self.isSign){
            /* 只有金币核销 和 签到检录 */
            selectIndex = -1;
        }else if (!self.isGolden && self.isTicket && self.isSign){
            /* 只有门票核销 和 签到/检录 */
            selectIndex = -2;
        }else if (self.isGolden &&!self.isTicket && !self.isSign){
            /* 只有金币核销 */
            selectIndex = -2;
        }else if (!self.isGolden && self.isTicket && !self.isSign){
            /* 只有门票核销 */
            selectIndex = -2;
        }else if (!self.isGolden && !self.isTicket && self.isSign){
            /* 只有签到/检录 */
            if (indexPath.row == selectIndex) {
                [self.navigationController pushViewController:[FWCheckAndSignViewController new] animated:YES];
            }
            
            selectIndex = -2;
        }else if (!self.isGolden && !self.isTicket && !self.isSign){
            /* 都没有 */
            selectIndex = -3;
        }
        
        
        if (indexPath.row == selectIndex + 5) {
            cell.rightLabel.text = mobileState;
        }else if (indexPath.row == selectIndex +6){
            cell.rightLabel.text = wxState;
        }else if (indexPath.row == selectIndex+12){
            if ([self isHaveNewVersion]) {
                cell.rightLabel.textColor = DHRedColorff6f00;
                cell.rightLabel.text = settingModel.update_msg;
            }else{
                cell.rightLabel.textColor = [UIColor lightGrayColor];
                cell.rightLabel.text = @"无新版";
            }
        }
    }else if (indexPath.section == 1){
        
        NSArray * secondArr = self.dataSource[indexPath.section];
        cell.titleLabel.text = secondArr[indexPath.row];
        
        CGFloat cacheSize = [self folderSize];
        cell.rightLabel.text = [NSString stringWithFormat:@"%.2fMB",cacheSize];
    }
    
    return cell;
}

- (BOOL)isHaveNewVersion{
    BOOL isHave = NO;
    
    NSArray * oldArray = [kAppVersion componentsSeparatedByString:@"."];
    NSArray * newArray = [settingModel.newest_version componentsSeparatedByString:@"."];

    if ([newArray[0] integerValue]> [oldArray[0] integerValue]) {
        return YES;
    }else if ([newArray[0] integerValue] == [oldArray[0] integerValue]){
        
        if ([newArray[1] integerValue]> [oldArray[1] integerValue]) {
            return YES;
        }else if ([newArray[1] integerValue] == [oldArray[1] integerValue]){
            
            if ([newArray[2] integerValue]> [oldArray[2] integerValue]) {
                return YES;
            }else{
                return NO;
            }
        }
    }
    return isHave;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        NSInteger selectIndex = 0;
        
        if (self.isTicket && self.isSign && self.isGolden) {
            /* 有门票核销 、 签到/检录 、金币核销*/
            if (indexPath.row == selectIndex) {
                [self.navigationController pushViewController:[FWGoldenCheckingViewController new] animated:YES];
                return;
            }else if (indexPath.row == selectIndex+1) {
                [self.navigationController pushViewController:[FWCheckAndSignViewController new] animated:YES];
                return;
            }else if (indexPath.row == selectIndex+2){
                [self.navigationController pushViewController:[FWTicketCheckingViewController new] animated:YES];
                return;
            }
        }else if (self.isGolden && self.isTicket && !self.isSign){
            /* 只有金币和门票核销 */
            if (indexPath.row == selectIndex) {
                [self.navigationController pushViewController:[FWGoldenCheckingViewController new] animated:YES];
                return;
            }else if (indexPath.row == selectIndex+1) {
               [self.navigationController pushViewController:[FWTicketCheckingViewController new] animated:YES];
                return;
            }
            
            selectIndex = -1;
        }else if (self.isGolden && !self.isTicket && self.isSign){
            /* 只有金币核销 和 签到检录 */
            if (indexPath.row == selectIndex) {
               [self.navigationController pushViewController:[FWGoldenCheckingViewController new] animated:YES];
                return;
            }else if (indexPath.row == selectIndex+1){
                [self.navigationController pushViewController:[FWCheckAndSignViewController new] animated:YES];
                return;
            }
            
            selectIndex = -1;
        }else if (!self.isGolden && self.isTicket && self.isSign){
            /* 只有门票核销 和 签到/检录 */
            if (indexPath.row == selectIndex) {
                [self.navigationController pushViewController:[FWCheckAndSignViewController new] animated:YES];
                return;
            }else if (indexPath.row == selectIndex+1) {
               [self.navigationController pushViewController:[FWTicketCheckingViewController new] animated:YES];
                return;
            }
            
            selectIndex = -2;
        }else if (self.isGolden &&!self.isTicket && !self.isSign){
            /* 只有金币核销 */
            if (indexPath.row == selectIndex) {
               [self.navigationController pushViewController:[FWGoldenCheckingViewController new] animated:YES];
                return;
            }
            
            selectIndex = -2;
        }else if (!self.isGolden && self.isTicket && !self.isSign){
            /* 只有门票核销 */
            if (indexPath.row == selectIndex) {
               [self.navigationController pushViewController:[FWTicketCheckingViewController new] animated:YES];
                return;
            }
            
            selectIndex = -2;
        }else if (!self.isGolden && !self.isTicket && self.isSign){
            /* 只有签到/检录 */
            if (indexPath.row == selectIndex) {
                [self.navigationController pushViewController:[FWCheckAndSignViewController new] animated:YES];
                return;
            }
            
            selectIndex = -2;
        }else if (!self.isGolden && !self.isTicket && !self.isSign){
            /* 都没有 */
            selectIndex = -3;
        }

        
        if (indexPath.row == selectIndex + 3){
            if ([GFStaticData getObjectForKey:kKefuUID]) {
                FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:[GFStaticData getObjectForKey:kKefuUID]];
                [self.navigationController pushViewController:CVC animated:YES];
                return;
            }
        }else if (indexPath.row == selectIndex+4) {
            [self.navigationController pushViewController:[FWEditInformationViewController new] animated:YES];
            return;
        }else if (indexPath.row == selectIndex+5){
            FWPhoneViewController * PVC = [[FWPhoneViewController alloc] init];
            PVC.type = 3;
            [self.navigationController pushViewController:PVC animated:YES];
            return;
        }else if (indexPath.row == selectIndex+6){
            
            [self.navigationController pushViewController:[FWThirdBindingViewController new] animated:YES];
            return;
        }else if (indexPath.row == selectIndex+7){
         
            [self pushToWebViewController:HTMLAPI_URL(HelpHTML) title:@"使用帮助"];
            return;
        }else if (indexPath.row == selectIndex+8){
            
            FWFeedBackViewController * FBVC = [[FWFeedBackViewController alloc] init];
            FBVC.gongzhonghao = settingModel.service_weixin;
            FBVC.tipString = settingModel.feedback_notice;
            [self.navigationController pushViewController:FBVC animated:YES];
            return;
        }else if (indexPath.row == selectIndex+9){
           
            [self pushToWebViewController:HTMLAPI_URL(StandardHTML) title:@"社区规范"];
            return;
        }else if (indexPath.row == selectIndex+10){
        
            [self pushToWebViewController:HTMLAPI_URL(AgreementHTML) title:@"隐私政策"];
            return;
        }else if (indexPath.row == selectIndex+11){

            [self pushToWebViewController:HTMLAPI_URL(AboutHTML) title:@"关于我们"];
            return;
        }else if (indexPath.row == selectIndex+12){
            if ([self isHaveNewVersion]) {
                NSURL *url = [NSURL URLWithString:AppStroeUpdateURL];
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                return;
            }
        }
    }else{
        //清除缓存
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"是否清空缓存" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self removeCache];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)pushToWebViewController:(NSString *)urlString title:(NSString *)title{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = urlString;
    WVC.webTitle = title;
    [self.navigationController pushViewController:WVC animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = FWBgColor_Lightgray_F7F8FA;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

#pragma mark - > 退出
- (void)signoutButtonClick{
    
    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self];
        }
    }else {
        FWLogoutRequest * request = [[FWLogoutRequest alloc] init];
        
        NSDictionary * param = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
        
        [request startWithParameters:param WithAction:Submit_user_logout  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
           
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
                [GFStaticData clearUserData];
                [[RCIMClient sharedRCIMClient] logout];
                [FWUserCenter sharedManager].user_IsLogin = NO;

                // 清空后，重新设置成游客模式
                [GFStaticData saveObject:@"0" forKey:kTagUserKeyID];
                [GFStaticData saveObject:@"0" forKey:kTagUserKeyName];

                [[FWHudManager sharedManager] showSuccessMessage:@"退出成功" toController:self];

                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"login"}];
                });
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 拨打电话
- (void)phoneClick{

    NSString *tel = [NSString stringWithFormat:@"tel:%@", settingModel.service_phone];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel] options:@{} completionHandler:nil];
    });
}

#pragma mark - > 获取当前应用缓存大小
- (CGFloat)folderSize{
    
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES)firstObject];
    
    return [WdCleanCaches sizeWithFilePaht:cachePath];
}

#pragma mark - > 清除缓存
- (void)removeCache{
    
    NSString*cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES)objectAtIndex:0];

    NSArray*files = [[NSFileManager defaultManager]subpathsAtPath:cachePath];

    for(NSString *p in files){
        NSError*error;

        NSString*path = [cachePath stringByAppendingString:[NSString stringWithFormat:@"/%@",p]];

        if([[NSFileManager defaultManager]fileExistsAtPath:path]){

            BOOL isRemove = [[NSFileManager defaultManager]removeItemAtPath:path error:&error];
            if(isRemove) {
                [infoTableView reloadData];
                [[FWHudManager sharedManager] showSuccessMessage:@"清除成功" toController:self];
            }else{
                NSLog(@"清除失败");
            }
        }
    }
}
@end
