//
//  FWDraftViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWDraftViewController.h"
#import "FWMineRequest.h"
#import "FWMyDraftCollectionCell.h"
#import "FWPublishPreviewViewController.h"
#import "FWPublishPreviewGraphViewController.h"
#import "FWPublishPreviewAskViewController.h"
#import "FWPublishPreviewRefitViewController.h"
#import "FWArticalViewController.h"
#import "FWPublishPreviewIdleViewController.h"

@interface FWDraftViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,FWBaseCollectionLayoutDalegate>
{
    FWFeedModel * draftFeedModel;
    NSMutableArray * draftDataSource;
    NSInteger   draftPageNum;
    BOOL   isDraftRefresh;
    
}
@end

@implementation FWDraftViewController
@synthesize draftCollectionView;

#pragma mark - > 获取草稿箱列表信息
- (void)requestDraftListWithLoadMoreData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        draftPageNum = 1;
        
        if (draftDataSource.count > 0 ) {
            [draftDataSource removeAllObjects];
        }
    }else{
        draftPageNum += 1;
    }
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSMutableArray * array = [GFStaticData getObjectForKey:Draft_Feed_id forUser:[GFStaticData getObjectForKey:kTagUserKeyID]];
    
    /* 去重 */
    NSArray * tempArr = [NSArray arrayWithArray:array];
    NSSet *set = [NSSet setWithArray:tempArr];
    [array removeAllObjects];
    for (NSString * str in set){
        [array addObject:str];
    }
    
    NSString * arr_draft_id = [array mj_JSONString]?[array mj_JSONString]:@"";
    
    NSDictionary * param = @{
                             @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"base_uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                             @"feed_type":@"0",
                             @"is_draft":@"1",
                             @"page":@(draftPageNum).stringValue,
                             @"page_size":@"20",
                             @"arr_draft_id":arr_draft_id,
                             };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:param WithAction:Get_feeds_by_uid_v2  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            if (isLoadMoredData) {
                [draftCollectionView.mj_footer endRefreshing];
            }else{
                [draftCollectionView.mj_header endRefreshing];
            }
            
            draftFeedModel = [FWFeedModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [draftDataSource addObjectsFromArray: draftFeedModel.feed_list];
            
            if([draftFeedModel.feed_list count] == 0 &&
               draftPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.draftCollectionView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"草稿箱页"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestDraftListWithLoadMoreData:NO];
    });
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"草稿箱页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"我的草稿";
    self.fd_interactivePopDisabled = YES;
    
    draftDataSource = @[].mutableCopy;
    isDraftRefresh = YES;
    draftPageNum = 0;

    [self setupSubViews];
}


- (void)setupSubViews{
    
    self.draftLayout = [[FWBaseCollectionLayout alloc] init];
    self.draftLayout.columns = 2;
    self.draftLayout.rowMargin = 12;
    self.draftLayout.colMargin = 12;
    self.draftLayout.sectionInset = UIEdgeInsetsMake(0, 10, 80, 10);
    self.draftLayout.delegate = self;
    [self.draftLayout autuContentSize];
    
    self.draftCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop) collectionViewLayout:self.draftLayout];
    self.draftCollectionView.dataSource = self;
    self.draftCollectionView.delegate = self;
    self.draftCollectionView.showsVerticalScrollIndicator = NO;
    self.draftCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.draftCollectionView registerClass:[FWMyDraftCollectionCell class] forCellWithReuseIdentifier:draftCellId];
    self.draftCollectionView.isNeedEmptyPlaceHolder = YES;
    self.draftCollectionView.verticalOffset = -150;
    NSString * draftTitle = @"还没有发布的草稿哦~";
    NSDictionary * draftAttributes = @{
                                       NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                       NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * draftAttributeString = [[NSMutableAttributedString alloc] initWithString:draftTitle attributes:draftAttributes];
    self.draftCollectionView.emptyDescriptionString = draftAttributeString;
    [self.view addSubview:self.draftCollectionView];
    
    
    @weakify(self);
    self.draftCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDraftListWithLoadMoreData:NO];
    }];
 
    self.draftCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self requestDraftListWithLoadMoreData:YES];
    }];
}


#pragma mark - > collectionView`s  Delegate & DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return draftDataSource.count?draftDataSource.count:0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWMyDraftCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:draftCellId forIndexPath:indexPath];
    cell.viewController = self;
    
    if (draftDataSource.count > 0) {
        
        FWFeedListModel * model = draftDataSource[indexPath.item];
        [cell configForCell:model];
        
        if (model.h5_url.length > 0) {
            
            cell.activityImageView.hidden = NO;
            
            NSString * imageName = @"activity";
            CGSize imageViewSize = CGSizeMake(30, 15);

            cell.activityImageView.image = [UIImage imageNamed:imageName];
            [cell.activityImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.top.mas_equalTo(cell.iv).mas_offset(5);
                make.size.mas_equalTo(imageViewSize);
            }];
        }else{
            cell.activityImageView.hidden = YES;
        }
    }else{
        cell.iv.hidden = YES;
        cell.userIcon.hidden = YES;
        cell.pvImageView.hidden = YES;
        cell.authenticationImageView.hidden = YES;
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = draftDataSource;
    
    FWFeedListModel * model = tempArr[indexPath.row];
    
    if ([model.feed_type isEqualToString:@"1"]) {
        
        [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
        
        FWPublishPreviewViewController * PPGV = [FWPublishPreviewViewController new];
        PPGV.editType = 2;
        PPGV.listModel = model;
        PPGV.draft_id = model.feed_id;
        PPGV.feed_id = model.feed_id;
        [self.navigationController pushViewController:PPGV animated:YES];
    }else if ([model.feed_type isEqualToString:@"2"]) {
        
        [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
        
        FWPublishPreviewGraphViewController * PPGV = [FWPublishPreviewGraphViewController new];
        PPGV.editType = 2;
        PPGV.listModel = model;
        PPGV.draft_id = model.feed_id;
        PPGV.feed_id = model.feed_id;
        [self.navigationController pushViewController:PPGV animated:YES];
    }else if ([model.feed_type isEqualToString:@"3"]) {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"文章暂不支持客户端编辑，请前往pc端编辑" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"去编辑" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController pushViewController:[FWArticalViewController new] animated:YES];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }else if ([model.feed_type isEqualToString:@"4"]) {
        // 问答
        
        [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
        
        FWPublishPreviewAskViewController * PPGV = [FWPublishPreviewAskViewController new];
        PPGV.editType = 2;
        PPGV.listModel = model;
        PPGV.draft_id = model.feed_id;
        PPGV.feed_id = model.feed_id;
        [self.navigationController pushViewController:PPGV animated:YES];
    }else if ([model.feed_type isEqualToString:@"5"]) {
        // 改装
        
        [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
        
        FWPublishPreviewRefitViewController * PPGV = [FWPublishPreviewRefitViewController new];
        PPGV.editType = 2;
        PPGV.listModel = model;
        PPGV.draft_id = model.feed_id;
        PPGV.feed_id = model.feed_id;
        [self.navigationController pushViewController:PPGV animated:YES];
    }else if ([model.feed_type isEqualToString:@"6"]) {
        // 闲置
        
        [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
        
        FWPublishPreviewIdleViewController * PPGV = [FWPublishPreviewIdleViewController new];
        PPGV.editType = 2;
        PPGV.listModel = model;
        PPGV.draft_id = model.feed_id;
        PPGV.feed_id = model.feed_id;
        [self.navigationController pushViewController:PPGV animated:YES];
    }
}

-(CGFloat)FWCollectionLayout:(FWBaseCollectionLayout *)collectionLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray * tempArr = draftDataSource;
    
    if (indexPath.item >= tempArr.count) {
        return 0;
    }
    
    FWFeedListModel * model = tempArr[indexPath.row];
    
    CGFloat height = 0;
    CGFloat cover_width = (SCREEN_WIDTH-50)/2;
    
    if ([model.feed_type isEqualToString:@"2"]) {
        // 图文贴 固定比例3：4
        height = (cover_width *4)/3;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if([model.feed_type isEqualToString:@"3"]){
        // 文章帖 固定比例5：4
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
    }else if ([model.feed_type isEqualToString:@"4"] ){
        /* 问答 */
        if (model.feed_cover_nowatermark.length <= 0) {
            /* 没有图片 */
            height = 0.01;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 20;
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
            height += 25;
        }
    }else if ([model.feed_type isEqualToString:@"6"] ){
        /* 闲置 */
        height = (cover_width *4)/5;
        model.feed_cover_width = @(cover_width).stringValue;
        model.feed_cover_height = @(height).stringValue;
        height += 30;
    }else{
        if (model.feed_cover_width.length >0 && model.feed_cover_height.length >0) {
            
            CGFloat feed_cover_width = [model.feed_cover_width floatValue];
            CGFloat feed_cover_height = [model.feed_cover_height floatValue];
            
            if (feed_cover_width/feed_cover_height <3/4) {
                /* 视频贴宽高比最大不超过4：3*/
                height = (cover_width *4)/3;
            }else{
                height = (cover_width *feed_cover_height)/feed_cover_width;
            }
        }else{
            height = (cover_width *4)/3;
            model.feed_cover_width = @(cover_width).stringValue;
            model.feed_cover_height = @(height).stringValue;
        }
    }
    
    CGFloat textHeight;
    if (model.feed_title.length > 0) {
        textHeight = [[Utility stringToAttributeString:model.feed_title] multiLineSize:(cover_width-24)].height;
        if (textHeight == 14) {
            textHeight = textHeight+10;
        }else if (textHeight >= 28){
            textHeight = 28+10;
        }
    }else{
        textHeight = 1;
    }
    
    return  height + textHeight + 40;
}

@end
