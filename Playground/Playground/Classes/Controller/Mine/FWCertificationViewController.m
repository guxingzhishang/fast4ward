//
//  FWCertificationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCertificationViewController.h"
#import "FWPersonalCertificationViewController.h"
#import "FWBussinessCertificationViewController.h"
#import "FWShopInfoViewController.h"

@interface FWCertificationViewController ()

@end

@implementation FWCertificationViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"账号认证页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"账号认证页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"账号认证";
    
    [self setupSubviews];
}

- (void)setupSubviews{
   
    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"升级为肆放认证账号，获取更多特权。";
    titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:17];
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).mas_offset(20);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-42, 22));
        make.left.mas_equalTo(self.view).mas_offset(21);
    }];

    NSArray * imageArray = @[@"bussiness",@"personal"];
    NSArray * firstArray = @[@"商家认证",@"车手认证"];

    CGFloat width = (SCREEN_WIDTH - 67)/2;
    CGFloat height = width;
    
    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    for (int i = 0; i < 2 ; i++) {
        UIButton * button = [[UIButton alloc] init];
        button.tag = 88+i;
        button.backgroundColor = FWViewBackgroundColor_FFFFFF;
        [button setBackgroundImage:[FWViewBackgroundColor_FFFFFF image] forState:UIControlStateHighlighted];
        [button setBackgroundImage:[UIImage imageNamed:@"certificate_bg"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];

        if (i == 0) {
            [button mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(14);
                make.size.mas_equalTo(CGSizeMake(width, height));
                make.left.mas_equalTo(self.view).mas_offset(21);
            }];
        }else{
            [button mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(14);
                make.size.mas_equalTo(CGSizeMake(width, height));
                make.right.mas_equalTo(self.view).mas_offset(-21);
            }];
        }
        
        UIImageView * imageView = [[UIImageView alloc] init];
        imageView.image = [UIImage imageNamed:imageArray[i]];
        [button addSubview:imageView];
        [imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(button);
            make.top.mas_equalTo(button).mas_offset(23);
            make.size.mas_equalTo(CGSizeMake(54, 54));
        }];
        
        UILabel * firstLabel = [[UILabel alloc] init];
        firstLabel.text = firstArray[i];
        firstLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:21];
        firstLabel.textColor = FWTextColor_000000;
        firstLabel.textAlignment = NSTextAlignmentCenter;
        [button addSubview:firstLabel];
        [firstLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(imageView.mas_bottom).mas_offset(10);
            make.height.mas_equalTo(28);
            make.width.mas_equalTo(button);
            make.centerX.mas_equalTo(button);
        }];
        
        
        if (i == 0 && ([usvm.userModel.cert_status isEqualToString:@"2"] || [usvm.userModel.merchant_cert_status isEqualToString:@"3"])) {
            UILabel * editLabel = [[UILabel alloc] init];
            editLabel.text = @"（编辑商家信息）";
            editLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
            editLabel.textColor = FWTextColor_66739A;
            editLabel.textAlignment = NSTextAlignmentCenter;
            [button addSubview:editLabel];
            [editLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(firstLabel.mas_bottom).mas_offset(2);
                make.height.mas_equalTo(15);
                make.width.mas_equalTo(button);
                make.centerX.mas_equalTo(button);
            }];
        }
        
        if (i == 1){
            if([usvm.userModel.driver_cert_status isEqualToString:@"2"]) {
                UILabel * editLabel = [[UILabel alloc] init];
                editLabel.text = @"（已认证）";
                editLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
                editLabel.textColor = FWTextColor_12101D;
                editLabel.textAlignment = NSTextAlignmentCenter;
                [button addSubview:editLabel];
                [editLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(firstLabel.mas_bottom).mas_offset(2);
                    make.height.mas_equalTo(15);
                    make.width.mas_equalTo(button);
                    make.centerX.mas_equalTo(button);
                }];
            }
        }
    }
}

- (void)buttonClick:(UIButton *)sender{
    
    FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];

    if (sender.tag == 88) {
        if ([usvm.userModel.cert_status isEqualToString:@"2"] ||
             [usvm.userModel.merchant_cert_status isEqualToString:@"3"]) {
            FWShopInfoViewController * SIVC = [[FWShopInfoViewController alloc] init];
            SIVC.oprationType = AddGoods;
            [self.navigationController pushViewController:SIVC animated:YES];
        }else{
            [self.navigationController pushViewController:[FWBussinessCertificationViewController new] animated:YES];
        }
    }else{
        if([usvm.userModel.driver_cert_status isEqualToString:@"2"]) {
            [[FWHudManager sharedManager] showErrorMessage:@"您已认证" toController:self];
        }else{
            [self.navigationController pushViewController:[FWPersonalCertificationViewController new] animated:YES];
        }
    }
}
@end
