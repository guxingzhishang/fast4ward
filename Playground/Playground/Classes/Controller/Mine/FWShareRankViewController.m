//
//  FWShareRankViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/6.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWShareRankViewController.h"

@interface FWShareRankViewController ()

@property (nonatomic, strong) UIButton * pengyouquanButton;

@property (nonatomic, strong) UIButton * friendButton;
/* 承载所有要分享的视图 */
@property (nonatomic, strong) UIImageView * containerView;
/* 分享用的视图，没有圆角 */
@property (nonatomic, strong) UIImageView * shareImageView;
/* 顶部视图 */
@property (nonatomic, strong) UIImageView * topView;
/* icon */
@property (nonatomic, strong) UIImageView * iconView;
/* 小程序二维码 */
@property (nonatomic, strong) UIImageView * codeView;
/* 提示文案 */
@property (nonatomic, strong) UILabel * rankLabel;
/* 底部视图 */
@property (nonatomic, strong) UIView * bottomView;
/* 排行截图 */
@property (nonatomic, strong) UIImageView * rankImageView;
/* 提示 */
@property (nonatomic, strong) UILabel * tipLabel;


@end

@implementation FWShareRankViewController
@synthesize cutshortImage;
@synthesize pengyouquanButton;
@synthesize friendButton;
@synthesize containerView;
@synthesize shareImageView;
@synthesize topView;
@synthesize iconView;
@synthesize codeView;
@synthesize rankLabel;
@synthesize bottomView;
@synthesize tipLabel;
@synthesize rankImageView;

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    [self trackPageBegin:@"车友/商家分享页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车友/商家分享页"];
}


- (void)viewDidLoad {
    [super viewDidLoad];

//    //方法一：系统方法，iOS8及以上可用
//    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
//        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
//        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
//        blurEffectView.frame = self.view.bounds;
//        [self.view addSubview:blurEffectView];
//    }
//    
    [self setupSubviews];
    [self setupShareButton];
}

#pragma mark - > 初始化视图
- (void)setupSubviews{
    
    UIView * bgView = [[UIView alloc] init];
    bgView.userInteractionEnabled= YES;
    bgView.backgroundColor = FWClearColor;
    [self.view addSubview:bgView];
    [bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapNothing)];
    [bgView addGestureRecognizer:tap];
    
    containerView = [[UIImageView alloc] init];
    containerView.layer.cornerRadius = 2;
    containerView.layer.masksToBounds = YES;
    containerView.clipsToBounds = YES;
    [self.view addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgView).mas_offset(24);
        make.right.mas_equalTo(bgView).mas_offset(-24);
        make.centerY.mas_equalTo(bgView).mas_offset(-30);
        make.width.mas_equalTo(SCREEN_WIDTH-48);
        make.height.mas_equalTo(423*(SCREEN_WIDTH-48)/327);
    }];
    
    shareImageView = [[UIImageView alloc] init];
    [containerView addSubview:shareImageView];
    [shareImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(containerView);
    }];
    
    topView = [[UIImageView alloc] init];
    if ([self.shareType integerValue]== 1) {
        topView.image = [UIImage imageNamed:@"share_bussiness_bg"];
    }else{
        topView.image = [UIImage imageNamed:@"share_person_bg"];
    }
    topView.contentMode = UIViewContentModeScaleAspectFill;
    topView.clipsToBounds = YES;
    [shareImageView addSubview:topView];
    [topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(containerView);
        make.width.mas_equalTo(containerView);
        make.height.mas_equalTo(139*(SCREEN_WIDTH-48)/327);
    }];
    
    bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [shareImageView addSubview:bottomView];
    [bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(containerView);
        make.top.mas_equalTo(topView.mas_bottom);
        make.width.mas_equalTo(topView);
        make.bottom.mas_equalTo(containerView);
        make.height.mas_equalTo(315*(SCREEN_WIDTH-48)/327);
    }];
    
    rankImageView = [[UIImageView alloc] init];
    rankImageView.contentMode = UIViewContentModeScaleAspectFill;
    [bottomView addSubview:rankImageView];
    [rankImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(bottomView);
        make.width.mas_equalTo(bottomView);
        make.height.mas_equalTo(174*(SCREEN_WIDTH-48)/327);
    }];
    
    CGFloat bottomHight = -31;
    if (SCREEN_WIDTH > 375) {
        bottomHight = -41;
    }
    tipLabel = [[UILabel alloc] init];
    tipLabel.text = @"长按识别二维码\n超多汽车资讯等你发现";
    tipLabel.font = DHSystemFontOfSize_12;
    tipLabel.textColor = FWTextColor_12101D;
    tipLabel.numberOfLines = 0;
    tipLabel.textAlignment = NSTextAlignmentLeft;
    [bottomView addSubview:tipLabel];
    [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(bottomView).mas_offset(bottomHight);
        make.left.mas_equalTo(rankImageView).mas_offset(21);
        make.size.mas_equalTo(CGSizeMake(150, 32));
    }];

    codeView = [UIImageView new];
    [codeView sd_setImageWithURL:[NSURL URLWithString:self.qrcode_url] placeholderImage:[UIImage imageNamed:@""]];
    [bottomView addSubview:codeView];
    [codeView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(tipLabel);
        make.right.mas_equalTo(bottomView).mas_offset(-21);
        make.size.mas_equalTo(CGSizeMake(83,83));
    }];
    
    rankImageView.image = self.cutshortImage;
}

#pragma mark - > 初始化分享按钮
- (void)setupShareButton{
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        
        CGFloat buttonHeight = 37;
        
        friendButton = [[UIButton alloc] init];
        [friendButton setImage:[UIImage imageNamed:@"share_weixinhaoyou"] forState:UIControlStateNormal];
        friendButton.layer.cornerRadius = buttonHeight/2;
        friendButton.layer.masksToBounds = YES;
        [friendButton addTarget:self action:@selector(friendButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:friendButton];
        [friendButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(47);
            make.left.mas_equalTo(52);
            make.height.mas_equalTo(48);
            make.top.mas_equalTo(containerView.mas_bottom).mas_offset(38);
        }];
        
        UILabel * friendLabel = [[UILabel alloc] init];
        friendLabel.text = @"微信好友";
        friendLabel.userInteractionEnabled = YES;
        friendLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        friendLabel.textColor = FWViewBackgroundColor_FFFFFF;
        friendLabel.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:friendLabel];
        [friendLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(friendButton.mas_right).mas_offset(10);
            make.centerY.mas_equalTo(friendButton);
            make.size.mas_equalTo(CGSizeMake(70, 48));
        }];
        UITapGestureRecognizer * friendTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(friendButtonOnClick)];
        [friendLabel addGestureRecognizer:friendTap];
        
        
        pengyouquanButton = [[UIButton alloc] init];
        pengyouquanButton.layer.cornerRadius = buttonHeight/2;
        pengyouquanButton.layer.masksToBounds = YES;
        [pengyouquanButton setImage:[UIImage imageNamed:@"share_pengyouquan"] forState:UIControlStateNormal];
        [pengyouquanButton addTarget:self action:@selector(pengyouquanButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pengyouquanButton];
        [pengyouquanButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(friendButton);
            make.right.mas_equalTo(-117);
            make.height.mas_equalTo(friendButton);
            make.top.mas_equalTo(friendButton);
        }];
        
        UILabel * pengyouquanLabel = [[UILabel alloc] init];
        pengyouquanLabel.text = @"朋友圈";
        pengyouquanLabel.userInteractionEnabled = YES;
        pengyouquanLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
        pengyouquanLabel.textColor = FWViewBackgroundColor_FFFFFF;
        pengyouquanLabel.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:pengyouquanLabel];
        [pengyouquanLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(pengyouquanButton.mas_right).mas_offset(10);
            make.centerY.mas_equalTo(pengyouquanButton);
            make.size.mas_equalTo(CGSizeMake(70, 48));
        }];
        UITapGestureRecognizer * pengyouquanTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pengyouquanButtonOnClick)];
        [pengyouquanLabel addGestureRecognizer:pengyouquanTap];
    }
}

#pragma mark - > 分享
- (void)friendButtonOnClick{
    [self actionForScreenShotWith:shareImageView savePhoto:NO withSence:WXSceneSession];
}

- (void)pengyouquanButtonOnClick{
    [self actionForScreenShotWith:shareImageView savePhoto:NO withSence:WXSceneTimeline];
}

- (void)WXSendImage:(UIImage *)image withShareScene:(enum WXScene)scene {
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [documentPath stringByAppendingPathComponent:@"ocr.jpg"];
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        [imageData writeToFile:filePath atomically:NO];
        
        UIImage *thumbImage = [self compressImage:image toByte:32768];
        
        WXImageObject *ext = [WXImageObject object];
        // 小于10MB
        ext.imageData = imageData;
        
        WXMediaMessage *message = [WXMediaMessage message];
        message.mediaObject = ext;
        // 缩略图 小于32KB
        [message setThumbImage:thumbImage];
        
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.scene = scene;
        req.message = message;
        [WXApi sendReq:req];
    }else {
        // 提示用户安装微信
        [[FWHudManager sharedManager] showErrorMessage:@"您还没装微信哦~" toController:self];
    }
}

#pragma mark - > 截屏
- (void)actionForScreenShotWith:(UIView *)aimView savePhoto:(BOOL)savePhoto withSence:(enum WXScene)scene {
    
    if (!aimView) return;
    
    UIGraphicsBeginImageContextWithOptions(aimView.bounds.size, NO, 0.0f);
    [aimView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (savePhoto) {
        UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }else{
        [self WXSendImage:viewImage withShareScene:scene];
    }
}

#pragma mark - >  保存到本地相册
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    if (error) {
        NSLog(@"保存失败，请重试");
    } else {
        NSLog(@"保存成功");
    }
}

#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {
    
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}

- (void)tapNothing{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
