//
//  FWFeedBackViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/23.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 我的 - > 意见反馈
 */

#import "FWBaseViewController.h"
#import "IQTextView.h"

@interface FWFeedBackViewController : FWBaseViewController<UITextViewDelegate>

/**
 * 输入框
 */
@property (nonatomic, strong) IQTextView * feedbackView;

/**
 * 提示文案
 */
@property (nonatomic, strong) UILabel * tipLabel;

/**
 * 限制字数提示
 */
@property (nonatomic, strong) UILabel * numberLabel;

/**
 * 复制按钮
 */
@property (nonatomic, strong) UIButton * cpButton;

/**
 * 发送按钮
 */
@property (nonatomic, strong) UIButton * sendButton;

/**
 * 提示文案
 */
@property (nonatomic, strong) NSString * tipString;

/**
 * 公主号
 */
@property (nonatomic, strong) NSString * gongzhonghao;


@end
