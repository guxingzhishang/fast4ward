//
//  FWVIPWebViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWVIPWebViewController : FWWebViewController

@property (nonatomic, assign) BOOL showBottomView;

@property (nonatomic, strong) FWMineInfoModel * infoModel;

@end

NS_ASSUME_NONNULL_END
