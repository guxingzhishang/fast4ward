//
//  FWInviteActivityViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 邀请活动
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWInviteActivityViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
