//
//  FWSelectCertificationCarViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWSelectCertificationCarViewController.h"
#import "FWSelectCertificationCarCell.h"

@interface FWSelectCertificationCarViewController ()

@property (nonatomic, strong) UIImageView * mainView;

@property (nonatomic, strong) NSMutableArray * searchArray;

@property (nonatomic, assign) NSInteger height;

@end

@implementation FWSelectCertificationCarViewController
@synthesize headerView;
@synthesize cancelBtn;
@synthesize searchBar;
@synthesize listTableView;
@synthesize mainView;
@synthesize height;

- (NSMutableArray *)searchArray{
    if (!_searchArray) {
        _searchArray = [[NSMutableArray alloc] init];
    }
    return _searchArray;
}

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"选择认证车型页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"选择认证车型页"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWTextColor_0D0A1B;
    
    self.fd_prefersNavigationBarHidden = YES;
    
    self.fd_interactivePopDisabled = YES;
    
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                selector:@selector(keyboardWillShow:)
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
}

- (void)keyboardWillShow:(NSNotification *)aNotification{
    
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    height = keyboardRect.size.height;
    
    [listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(headerView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-height));
    }];
}

#pragma mark - > 视图初始化
- (void)addSubviews{
    
    mainView = [[UIImageView alloc] init];
    mainView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [self.view addSubview:mainView];
    
    
    [self setupNavigationView];
    [self setupTableView];
}

- (void)setupNavigationView{
    
    headerView = [[UIView alloc] init];
    [self.view addSubview:headerView];
    [headerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 74+FWCustomeSafeTop));
    }];
    
    searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    searchBar.backgroundColor = FWViewBackgroundColor_E5E5E5;
    searchBar.barStyle = UIBarStyleDefault;
    searchBar.placeholder = @"请输入您的车型";
    [headerView addSubview:searchBar];
    
    UITextField*searchField = [searchBar valueForKey:@"searchField"];
    //更改searchBar输入文字颜色
    searchField.textColor= FWTextColor_12101D;
    searchField.font = [UIFont systemFontOfSize:14];

    [searchBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(headerView).mas_offset(20);
        make.top.mas_equalTo(headerView).mas_offset(25+FWCustomeSafeTop);
        make.height.mas_equalTo(36);
        make.right.mas_equalTo(headerView.mas_right).mas_offset(-70);
    }];
    [searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];

    if (@available(iOS 11.0, *)) {
        [[self.searchBar.heightAnchor constraintEqualToConstant:36] setActive:YES];
    }
    
    [self removeBorder:searchBar];
    [searchBar becomeFirstResponder];
    
    
    cancelBtn = [[UIButton alloc] init];
    cancelBtn.titleLabel.font = DHSystemFontOfSize_14;
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:cancelBtn];
    [cancelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(headerView).mas_offset(-10);
        make.centerY.mas_equalTo(searchBar);
        make.size.mas_equalTo(CGSizeMake(40, 30));
    }];
}

- (void)setupTableView{
    
    listTableView = [[UITableView alloc] init];
    listTableView.delegate = self;
    listTableView.dataSource = self;
    listTableView.rowHeight = 60;
    listTableView.backgroundColor = [UIColor clearColor];
    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view  addSubview:listTableView];
    [listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(headerView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop));
    }];
}

#pragma mark - > tableView  Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.searchArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"selectCertificationCarListID";
    
    FWSelectCertificationCarCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWSelectCertificationCarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.backgroundColor = [UIColor clearColor];
    
    [cell cellConfigureFor:self.searchArray[indexPath.row]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWPersonalCertificationListModel * model = self.searchArray[indexPath.row];
    self.myBlock(model);
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > searchBar delegate 实时搜索标签
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([self.searchBar.text length] > 12)
    {
        self.searchBar.text = [self.searchBar.text substringToIndex:12];
    }
    
    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"keyword":searchBar.text
                              };
    
    if (searchBar.text.length >0) {
        listTableView.hidden = NO;
        
        [request startWithParameters:params WithAction:Get_car_list_by_suggestion  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                if (self.searchArray.count >0) {
                    [self.searchArray removeAllObjects];
                }
                
                FWPersonalCertificationModel * model = [FWPersonalCertificationModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                self.searchArray = model.list;
                [listTableView reloadData];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }else{
        listTableView.hidden = YES;
    }
}

#pragma mark - > 取消搜索
- (void)cancelBtnOnClick:(UIButton *)sender{
    
    self.myBlock(nil);
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 去掉输入框边框
- (void)removeBorder:(UISearchBar * )searchBar{
    
    if (@available(iOS 13.0, *)) {

    }else{
        for(int i =  0 ;i < searchBar.subviews.count;i++){
            UIView * backView = searchBar.subviews[i];
            if ([backView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                [backView removeFromSuperview];
                [searchBar setBackgroundColor:[UIColor clearColor]];
                
                break;
            }else{
                NSArray * arr = searchBar.subviews[i].subviews;
                for(int j = 0;j<arr.count;j++   ){
                    UIView * barView = arr[j];
                    if ([barView isKindOfClass:NSClassFromString(@"UISearchBarBackground")] == YES) {
                        
                        [barView removeFromSuperview];
                        [searchBar setBackgroundColor:[UIColor clearColor]];
                    }
                    if ([barView isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                        barView.backgroundColor = FWViewBackgroundColor_E5E5E5;
                    }
                }
            }
        }
    }
}

@end
