//
//  FWInviteActivityViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWInviteActivityViewController.h"

@interface FWInviteActivityViewController ()<UIScrollViewDelegate,ShareViewDelegate>

@property (nonatomic, strong) UIImageView * bgImageView;

@property (nonatomic, strong) UIScrollView * mainScrollView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIImageView * topImageView;
@property (nonatomic, strong) UIButton * detailButton;

@property (nonatomic, strong) UIImageView * firstView;
@property (nonatomic, strong) UILabel * pointLabel;

@property (nonatomic, strong) UIButton * inviteButton;

@property (nonatomic, strong) UITextView * secondView;
@property (nonatomic, strong) UITextView * thirdView;

@property (nonatomic, strong) NSDictionary * infoDict;


@end

@implementation FWInviteActivityViewController

- (NSDictionary *)infoDict{
    if (!_infoDict) {
        _infoDict = @{};
    }
    
    return _infoDict;
}

- (void)requestData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_merchant_invite_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.infoDict = [[back objectForKey:@"data"] copy];
            [self refreshView:self.infoDict];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"邀请活动页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"邀请活动页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    
    [self setupSubViews];
    [self requestData];
}

- (void)setupSubViews{
    
    self.mainScrollView = [[UIScrollView alloc] init];
    self.mainScrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT);
    self.mainScrollView.delegate = self;
    self.mainScrollView.scrollEnabled = YES;
    self.mainScrollView.backgroundColor = FWBlue_4598F4;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.mainScrollView];
    
    if (@available(iOS 11.0, *)) {
        self.automaticallyAdjustsScrollViewInsets = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.bgImageView = [[UIImageView alloc] init];
    self.bgImageView.userInteractionEnabled = YES;
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgImageView.image = [UIImage imageNamed:@"invite_bg"];
    [self.mainScrollView addSubview:self.bgImageView];
    [self.bgImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.mainScrollView);
    }];
    
    self.topImageView = [[UIImageView alloc] init];
    self.topImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topImageView.userInteractionEnabled = YES;
    self.topImageView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.topImageView.image = [UIImage imageNamed:@"invite_top"];
    [self.bgImageView addSubview:self.topImageView];
    [self.topImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.bgImageView);
        make.top.mas_equalTo(self.mainScrollView).mas_offset(0);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(280*SCREEN_WIDTH/375);
    }];
    
    self.backButton = [[UIButton alloc] init];
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgImageView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.top.mas_equalTo(20+FWCustomeSafeTop);
        make.left.mas_equalTo(15);
    }];
    
    self.detailButton = [[UIButton alloc] init];
    [self.detailButton setImage:[UIImage imageNamed:@"invite_detail"] forState:UIControlStateNormal];
    [self.detailButton addTarget:self action:@selector(detailButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView addSubview:self.detailButton];
    [self.detailButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.topImageView);
        make.size.mas_equalTo(CGSizeMake(93, 26));
        make.bottom.mas_equalTo(self.topImageView).mas_offset(-75);
    }];
    
    self.firstView = [[UIImageView alloc] init];
    self.firstView.userInteractionEnabled = YES;
    self.firstView.image = [UIImage imageNamed:@"invite_point_bg"];
    [self.bgImageView addSubview:self.firstView];
    [self.firstView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgImageView).mas_offset(0);
        make.top.mas_equalTo(self.topImageView.mas_bottom).mas_offset(-15);
        make.right.mas_equalTo(self.bgImageView).mas_offset(-0);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 283));
    }];
    
    self.pointLabel = [[UILabel alloc] init];
    self.pointLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 42];
    self.pointLabel.textColor = FWBlue_4598F4;
    self.pointLabel.textAlignment = NSTextAlignmentCenter;
    [self.firstView addSubview:self.pointLabel];
    [self.pointLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.firstView);
        make.top.mas_equalTo(self.firstView).mas_offset(95);
        make.height.mas_equalTo(50);
    }];
    
    self.inviteButton = [[UIButton alloc] init];
    [self.inviteButton setImage:[UIImage imageNamed:@"invite_button"] forState:UIControlStateNormal];
    [self.inviteButton addTarget:self action:@selector(inviteButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.firstView addSubview:self.inviteButton];
    [self.inviteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.firstView);
        make.bottom.mas_equalTo(self.firstView.mas_bottom).mas_offset(-50);
        make.size.mas_equalTo(CGSizeMake(203, 45));
    }];
    
    self.secondView = [[UITextView alloc] init];
    self.secondView.editable = NO;
    self.secondView.scrollEnabled = NO;
    self.secondView.layer.cornerRadius = 5;
    self.secondView.layer.masksToBounds = YES;
    self.secondView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.secondView.textContainerInset = UIEdgeInsetsMake(15,15,20,18);
    [self.bgImageView addSubview:self.secondView];
    [self.secondView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgImageView).mas_offset(20);
        make.top.mas_equalTo(self.firstView.mas_bottom).mas_offset(20);
        make.right.mas_equalTo(self.bgImageView).mas_offset(-20);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
        make.height.mas_greaterThanOrEqualTo(50);
    }];
    
    self.thirdView = [[UITextView alloc] init];
    self.thirdView.editable = NO;
    self.thirdView.scrollEnabled = NO;
    self.thirdView.layer.cornerRadius = 5;
    self.thirdView.layer.masksToBounds = YES;
    self.thirdView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.thirdView.textContainerInset = UIEdgeInsetsMake(15,15,14,18);
    [self.bgImageView addSubview:self.thirdView];
    [self.thirdView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgImageView).mas_offset(20);
        make.top.mas_equalTo(self.secondView.mas_bottom).mas_offset(15);
        make.right.mas_equalTo(self.bgImageView).mas_offset(-20);
        make.width.mas_equalTo(SCREEN_WIDTH-40);
        make.height.mas_greaterThanOrEqualTo(50);
        make.bottom.mas_equalTo(self.bgImageView).mas_offset(-33);
    }];
    
}

- (void)refreshView:(NSDictionary *)dict{
    
    self.pointLabel.text = dict[@"effect_value"];
    
    NSString * desc1 = dict[@"desc1"];
    NSString * desc2 = dict[@"desc2"];

    if (desc1.length > 0) {
        desc1 = [desc1 stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    }
    if (desc2.length > 0) {
        desc2 = [desc2 stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    NSMutableParagraphStyle *paragraphStyle2 = [[NSMutableParagraphStyle alloc] init];

    paragraphStyle.lineSpacing = 5;// 字体的行间距
    paragraphStyle2.lineSpacing = 5;// 字体的行间距

    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Regular" size: 12],
                                 NSForegroundColorAttributeName : FWBlue_4598F4,
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
    NSDictionary *attributes2 = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Regular" size: 12],
                                  NSForegroundColorAttributeName : FWBlue_4598F4,
                                  NSParagraphStyleAttributeName:paragraphStyle2
                                  };
    
    self.secondView.attributedText = [[NSAttributedString alloc] initWithString:desc1 attributes:attributes];
    self.thirdView.attributedText = [[NSAttributedString alloc] initWithString:desc2 attributes:attributes2];
    
    self.secondView.text = desc1;
    self.thirdView.text = desc2;
}

#pragma mark - > 邀请好友
- (void)inviteButtonOnClick{

    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
            ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
            [shareView showView];
        }
    }
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    FWFeedListModel * model = [[FWFeedListModel alloc] init];
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0){
            NSDictionary * shareDict =self.infoDict[@"share_app_message"];
            model.feed_id = @"other";
            model.share_url = shareDict[@"link"];
            model.share_desc = shareDict[@"desc"];
            model.share_title = shareDict[@"title"];
            model.feed_cover = shareDict[@"img_url"];
            
            [[ShareManager defaultShareManager] sendWXWithModel:model WithType:0];
        }else if (index == 1){
            NSDictionary * shareDict =self.infoDict[@"share_timeline"];
            
            model.feed_id = @"other";
            model.share_url = shareDict[@"link"];
            model.share_desc = shareDict[@"desc"];
            model.share_title = shareDict[@"title"];
            model.feed_cover = shareDict[@"img_url"];

            [[ShareManager defaultShareManager] sendWXWithModel:model WithType:1];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}


#pragma mark - > 查看详情
- (void)detailButtonOnClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.infoDict[@"h5_cert"];
    [self.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 返回
- (void)backButtonOnClick{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

@end
