//
//  FWCarDetailRefitListViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWCarDetailRefitListViewController.h"

@interface FWCarDetailRefitListViewController ()

@end

@implementation FWCarDetailRefitListViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"改装清单页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"改装清单页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.listView = [[FWRefitListView alloc] init];
    self.listView.vc = self;
    self.listView.user_car_id = self.listModel.user_car_id;
    [self.view addSubview:self.listView];
    [self.listView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view).mas_offset(10);
    }];
    
    if (self.listModel.gaizhuang_list.length > 0) {
        NSData *jsonData = [self.listModel.gaizhuang_list dataUsingEncoding:NSUTF8StringEncoding];
        NSError *err;
        NSArray *arr = [NSJSONSerialization JSONObjectWithData: jsonData options:NSJSONReadingMutableContainers error:&err];
        
        NSDictionary * dict = @{@"list_gaizhuang_category":arr};
        FWRefitCategaryModel * cateModel = [FWRefitCategaryModel mj_objectWithKeyValues:dict];
        self.listView.type = @"2";
        
        /* 改装清单 */
        for(int i = 0; i< cateModel.list_gaizhuang_category.count;i++){
            
            /* 第一层数据 */
            FWRefitCategaryListModel * firstModel = cateModel.list_gaizhuang_category[i];
            for (int j = 0; j <firstModel.sub.count; j++) {
            
                /* 第二层数据 */
                FWRefitCategarySubListModel * secondModel = firstModel.sub[j];
                
                if (secondModel.val.length > 0) {
                    firstModel.firstLevelType = 1;
                    
                    if ([secondModel.val containsString:@"原厂"]) {
                        secondModel.secondLevelType = 1;
                        secondModel.tempName = [NSString stringWithFormat:@"%@-原厂",secondModel.name];
                    }
                }else{
                    for (int k = 0; k < secondModel.list.count; k++) {
                        /* 有编辑 */
                        FWRefitCategarySubDetailModel * detailModel = secondModel.list[k];
                        
                        if (detailModel.val.length > 0) {
                            
                            firstModel.firstLevelType = 1;
                            secondModel.secondLevelType = 2;
                            
                            NSString * pinpai = @"";
                            NSString * xinghao = @"";
                            NSString * guige = @"";
                            NSString * feiyong = @"";
                            
                            for (int k = 0;k < secondModel.list.count; k++) {
                                if ([secondModel.list[k].name isEqualToString:@"品牌"]) {
                                    pinpai = secondModel.list[k].val;
                                }else  if ([secondModel.list[k].name isEqualToString:@"型号"]) {
                                     xinghao = secondModel.list[k].val;
                                 }else  if ([secondModel.list[k].name isEqualToString:@"规格"]) {
                                     guige = secondModel.list[k].val;
                                 }else  if ([secondModel.list[k].name isEqualToString:@"费用"]) {
                                     feiyong = secondModel.list[k].val;
                                 }
                            }
                            
                            if (pinpai.length > 0) {
                                pinpai = [NSString stringWithFormat:@"-%@",pinpai];
                            }
                            
                            if (xinghao.length > 0) {
                                xinghao = [NSString stringWithFormat:@"-%@",xinghao];
                            }
                            
                            if (guige.length > 0) {
                                guige = [NSString stringWithFormat:@"-%@",guige];
                            }
                            
                            if (feiyong.length > 0) {
                                feiyong = [NSString stringWithFormat:@"-%@",feiyong];
                            }
                            NSString * moduleString = [NSString stringWithFormat:@"%@%@%@%@",pinpai,xinghao,guige,feiyong];
                            if ([[moduleString substringToIndex:1] isEqualToString:@"-"]) {
                                /* 如果第一个是- ，先给去掉 */
                                moduleString = [moduleString substringFromIndex:1];
                            }

                            secondModel.tempName = [NSString stringWithFormat:@"%@\n%@",secondModel.name,moduleString];
                        }
                    }
                }
            }
        }
        
        self.listView.categaryModel = cateModel;
    }else{
        self.listView.type = @"1";

        if (!self.listModel) {
            [self requestGaizhuangList];
        }
    }
}

- (void)requestGaizhuangList{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_gaizhuang_category WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.categaryModel = [FWRefitCategaryModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            self.listView.categaryModel = self.categaryModel;
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

@end
