//
//  FWBussinessVideoServiceViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 个人中心 -> 商家联盟 -> 视频套餐
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWBussinessVideoServiceViewController : FWBaseViewController

@property (nonatomic, strong) NSMutableArray * productList;

@end

NS_ASSUME_NONNULL_END
