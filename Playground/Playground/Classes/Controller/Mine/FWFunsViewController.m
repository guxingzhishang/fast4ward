//
//  FWFunsViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWFunsViewController.h"
#import "FWFunsMessageRequest.h"
#import "FWUnionMessageModel.h"
#import "FWFunsRequest.h"
#import "FWFollowModel.h"

@interface FWFunsViewController ()

@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, assign) FWFollowModel * followModel;
@end

@implementation FWFunsViewController
@synthesize infoTableView;
@synthesize pageNum;
@synthesize followModel;
@synthesize funsType;
@synthesize newsPicURL;
@synthesize oldPicURL;
@synthesize refreshPicURL;

#pragma mark - > 请求消息通知（谁关注了我）
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{
   
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    FWFunsMessageRequest * request = [[FWFunsMessageRequest alloc] init];
    NSDictionary * params = @{
                              @"msg_type":@"2",
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"pagesize":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_msg_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSMutableArray * tempArr = [FWUnionMessageModel mj_objectArrayWithKeyValuesArray:[[back objectForKey:@"data"] objectForKey:@"msg_list"]];
            
            [self.dataSource addObjectsFromArray:tempArr];
            
            if([tempArr count] == 0 && self.pageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求关注列表
- (void)requestFollowsListWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum +=1;
    }
    
    FWFunsRequest * request = [[FWFunsRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"base_uid":self.base_uid,
                              @"get_type":@"fans",
                              @"page":@(pageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_follow_users_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }

            NSMutableArray * tempArr = [FWFollowUserListModel mj_objectArrayWithKeyValuesArray:[[back objectForKey:@"data"] objectForKey:@"user_list"]];

            [self.dataSource addObjectsFromArray:tempArr];
            
            if([tempArr count] == 0 && self.pageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"粉丝页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"粉丝页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"粉丝";

    newsPicURL = @[].mutableCopy;
    oldPicURL = @[].mutableCopy;
    refreshPicURL = @[].mutableCopy;
    
    self.pageNum = 0;
    self.dataSource = @[].mutableCopy;
    
    if ([self.funsType isEqualToString:@"1"]) {
        
        [self requestDataWithLoadMoredData:NO];
    }else if ([self.funsType isEqualToString:@"2"]||
              [self.funsType isEqualToString:@"3"]){
        
        [self requestFollowsListWithLoadMoredData:NO];
    }
}

- (void)bindViewModel{
    [super bindViewModel];
}

- (void)addSubviews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 80;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    @weakify(self);
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);

        if ([self.funsType isEqualToString:@"1"]) {
            [self requestDataWithLoadMoredData:NO];
        }else if ([self.funsType isEqualToString:@"2"]||
                  [self.funsType isEqualToString:@"3"]){
            [self requestFollowsListWithLoadMoredData:NO];
        }
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);

        if ([self.funsType isEqualToString:@"1"]) {
            [self requestDataWithLoadMoredData:YES];
        }else if ([self.funsType isEqualToString:@"2"]||
                  [self.funsType isEqualToString:@"3"]){
            [self requestFollowsListWithLoadMoredData:YES];
        }
    }];
}

#pragma mark - > UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    NSInteger number ;
    
    if ([self.funsType isEqualToString:@"1"]){
        number = self.dataSource.count?self.dataSource.count:0;
    }else{
        number = self.dataSource.count?self.dataSource.count:0;
    }
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.funsType isEqualToString:@"1"]) {
        static NSString * cellID = @"messageFunsCellID";
        
        FWMessageFunsCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[FWMessageFunsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.delegate = self;
        cell.followButton.tag = 5000+indexPath.row;
        [cell cellConfigureFor:self.dataSource[indexPath.row]];
        
        return cell;
    }else{
        static NSString * cellID = @"mineFunsCellID";
        
        FWMineFunsCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[FWMineFunsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.delegate = self;
        cell.followButton.tag = 5000+indexPath.row;
        
        [cell cellConfigureFor:self.dataSource[indexPath.row]];

        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.funsType isEqualToString:@"1"]) {
        
        FWUnionMessageModel * messageModel = self.dataSource[indexPath.row];

        FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
        UIVC.user_id = messageModel.r_uid;
        [self.navigationController pushViewController:UIVC animated:YES];
    }
}

#pragma mark - > 我的粉丝页面，点击关注按钮
- (void)followButtonForIndex:(NSInteger)index withCell:(FWMineFunsCell *)cell{
    
    if (![self.funsType isEqualToString:@"1"]) {
        
        NSString * action ;
        FWFollowUserListModel * model = self.dataSource[index];
     
        if ([model.follow_status isEqualToString:@"2"]) {
            action = Submit_follow_users;
        }else{
            action = Submit_cancel_follow_users;
        }
        
        FWFunsMessageRequest * request = [[FWFunsMessageRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"f_uid":model.uid,
                                  };
        
        [request startWithParameters:params WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                if ([action isEqualToString: Submit_follow_users]) {
                    model.follow_status_str = [[back objectForKey:@"data"] objectForKey:@"follow_status_str"];
                    model.follow_status = [[back objectForKey:@"data"] objectForKey:@"follow_status"];
                    
                    [[FWHudManager sharedManager] showSuccessMessage:@"关注成功" toController:self];
                }else{
                    model.follow_status = @"2";
                }
                
                [cell cellConfigureFor:model];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

@end
