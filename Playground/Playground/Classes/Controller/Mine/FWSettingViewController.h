//
//  FWSettingViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/23.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 我的 - > 设置 
 */

#import "FWBaseViewController.h"
#import "FWBaseCellView.h"
#import "FWSettingModel.h"

@interface FWSettingViewController : FWBaseViewController

@property (nonatomic, strong) NSDictionary * settingDict;

@property (nonatomic, strong) FWSettingModel * settingModel;

@property (nonatomic, strong) FWMineModel * mineModel;

@end
