//
//  FWChangePhoneViewController.m
//  Playground
//
//  Created by hehe on 2018/9/24.
//  Copyright © 2018年 hehe. All rights reserved.
//

#import "FWChangePhoneViewController.h"
#import "FBKVOController.h"
#import "DHTimerManager.h"
#import "FWInformationViewController.h"
#import "FWLoginPhoneRequest.h"
#import "FWLoginGetCodeRequest.h"
#import "FWSettingViewController.h"
#import "FWMineRequest.h"
#import "NSTimer+Addition.h"
#import "FWAttentionRefitTopicViewController.h"
#import "FWAttentionRefitTopicViewController.h"

@interface FWChangePhoneViewController ()

@property (nonatomic, strong) NSTimer * checkTimer;
@property (nonatomic, assign) NSInteger time;

@end

@implementation FWChangePhoneViewController
@synthesize changeView;
@synthesize codeNumber;
@synthesize phoneNumber;
@synthesize checkTimer;
@synthesize time;

- (void)dealloc{
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self.view endEditing:YES];
    [checkTimer invalidate];

    if (self.type != 3) {
        [self trackPageEnd:@"验证码页"];
    }else{
        [self trackPageEnd:@"修改手机号页"];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if (self.type != 3) {
        [self trackPageBegin:@"验证码页"];
    }else{
        [self trackPageBegin:@"修改手机号页"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fd_prefersNavigationBarHidden = YES;
    
    [self setupSubviews];
}

#pragma mark - > 视图初始化
- (void)setupSubviews{
    
    changeView =  [[FWChangePhoneView alloc] init];
    changeView.delegate = self;
    changeView.phoneNumber = self.phoneNumber;
    [self.view addSubview:changeView];
    
    [changeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [changeView.codeTextView becomeFirstResponder];
    
    [self Countdown];
}

// 倒计时
- (void)Countdown
{
    [changeView.nextButton setTitle:@"60秒后重新发送" forState:UIControlStateNormal];
    
    time = 60;
    checkTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self
                                                selector:@selector(handleTimer)
                                                userInfo:nil
                                                 repeats:YES];
    changeView.nextButton.userInteractionEnabled = NO;
}

- (void)handleTimer
{
    if (time == 0) {
        [checkTimer pauseTimer];
        changeView.nextButton.userInteractionEnabled = YES;
        [changeView.nextButton setTitleColor:FWTextColor_222222 forState:UIControlStateNormal];
        [changeView.nextButton setTitle:@"重新发送" forState:UIControlStateNormal];
    }
    else
    {
//        NSLog(@"time===%ld",time);
        [changeView.nextButton setTitleColor:FWTextColor_B6BCC4 forState:UIControlStateNormal];
        [changeView.nextButton setTitle:[NSString stringWithFormat:@"%d秒后重新发送",(int)time] forState:UIControlStateNormal];
        changeView.nextButton.userInteractionEnabled = NO;
        
        time -= 1;
    }
}

- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 再次发送验证码
-(void)nextButtonClick{
    
    [changeView endEditing:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSDictionary * dict = @{@"mobile":self.phoneNumber};
        
        FWLoginGetCodeRequest * request = [[FWLoginGetCodeRequest alloc] init];
        request.isNeedShowHud = YES;
        [request startWithParameters:dict WithAction:Get_reglogin_sms  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject,nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                [self Countdown];
//                [[FWHudManager sharedManager] showSuccessMessage:@"发送成功" toController:self];
            }else{
                
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    });
    
}

#pragma mark - > 填满后自动提交
- (void)codeInputView:(FWChangePhoneView *)codeInputView finished:(BOOL)finished{
    
    if (finished) {
        [self.view endEditing:YES];
        
        NSString * codeString = [changeView getCode];
        
        //手机登录页，输入验证码，后来可能要有别的地方需要，需添加一个type字段
        NSDictionary * dict = @{@"mobile":self.phoneNumber,
                                @"verify_code":codeString};
        
        NSString * action;
        if (self.type == 1) {
            action = Submit_mobile_login;
        }else if (self.type == 2||
                  self.type == 3){
            action = Submit_mobile_bind;
        }
        
        //先收键盘，在请求网络
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            FWLoginPhoneRequest * request = [[FWLoginPhoneRequest alloc] init];
            request.isNeedShowHud = YES;
            [request startWithParameters:dict WithAction:action  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
                NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject,nil);
                
                if ([code isEqualToString:NetRespondCodeSuccess]) {
                    
                    [GFStaticData saveLoginData:[back objectForKey:@"data"]];
                    
                    [self pushViewControllerWithType:[[back objectForKey:@"data"] objectForKey:@"next_page"]];
                }else{
                    
                    [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
                }
            }];
        });
    }
}


#pragma mark - > 根据状态，跳转至不同页面
- (void)pushViewControllerWithType:(NSString *)nextPage{
    
    if(self.type == 1){
        [self requsetMineData];
    }
    
    if ([nextPage isEqualToString:@"follow_users"]){
        FWAttentionRefitTopicViewController * IVC = [[FWAttentionRefitTopicViewController alloc] init];
        IVC.fromLogin = self.fromLogin;
        [self.navigationController pushViewController:IVC animated:YES];
    }else if ([nextPage isEqualToString:@"follow_tags"]){
        FWAttentionRefitTopicViewController * IVC = [[FWAttentionRefitTopicViewController alloc] init];
        IVC.fromLogin = self.fromLogin;
        [self.navigationController pushViewController:IVC animated:YES];
    }else if ([nextPage isEqualToString:@"fill_profile"]){
        
        /* 展示设置头像和昵称的页面 */
        FWInformationViewController * IVC = [[FWInformationViewController alloc] init];
        IVC.fromLogin = self.fromLogin;
        [self.navigationController pushViewController:IVC animated:YES];
    }else if ([nextPage isEqualToString:@"home"]){
        
        if (self.fromLogin == 1) {
            //正常登录，跳转至首页
            [GFStaticData saveObject:@"1" forKey:kTagUserKeyName];
            /* 首页 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
        }else{
            // 游客模式登录，返回
            __block FWSubLoginViewController * loginVC;
            [self.navigationController.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isMemberOfClass:[FWSubLoginViewController class]])
                {
                    loginVC = obj;
                }
            }];
            
            if (loginVC != nil)
            {
                //返回到登录页面
                loginVC.isBack = YES;
                [self.navigationController popToViewController:loginVC animated:NO];
            } else {
                //正常登录，跳转至首页
                [GFStaticData saveObject:@"1" forKey:kTagUserKeyName];
                /* 首页 */
                [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":@"newFeature"}];
            }
        }
    }else{
        if (self.type == 3) {
            
            [self requsetMineData];
        }else if (self.type == 2){
            
            [[FWHudManager sharedManager] showSuccessMessage:@"绑定成功" toController:self];
            /* 展示推荐关注的标签 */
            FWAttentionRefitTopicViewController * FCV = [[FWAttentionRefitTopicViewController alloc] init];
            FCV.fromLogin = self.fromLogin;
            [self.navigationController pushViewController:FCV animated:YES];
        }
    }
}

#pragma mark - > 请求个人数据
- (void)requsetMineData{
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [GFStaticData saveLoginData:[[back objectForKey:@"data"] objectForKey:@"user_info"]];
            
            [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
            [[FWUserCenter sharedManager] saveUserInfoToDiskWithJson:back];
            [FWUserCenter sharedManager].user_IsLogin = YES;
            
            // 修改新手机号，成功后退到设置页面
            [[FWHudManager sharedManager] showSuccessMessage:@"修改成功" toController:self];
            for (FWBaseViewController *viewC in self.navigationController.viewControllers) {
                if ([viewC isKindOfClass:[FWSettingViewController class]]) {
                    [self.navigationController popToViewController:viewC animated:YES];
                }
            }
        }
    }];
}

@end
