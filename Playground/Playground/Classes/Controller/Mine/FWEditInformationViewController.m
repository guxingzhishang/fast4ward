//
//  FWEditInformationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWEditInformationViewController.h"
#import "FWUpdateInfomationRequest.h"
#import "ALiOssUploadTool.h"
#import "IQKeyboardManager.h"

@interface FWEditInformationViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong)NSDictionary * dataDict;
@property(nonatomic,strong)UIImagePickerController *picker;

@end

@implementation FWEditInformationViewController
@synthesize editView;
@synthesize saveButton;
@synthesize rightBtnItem;
@synthesize picker;
@synthesize header_url;

#pragma mark - > 请求阿里临时验证
- (void)requestStsData{

    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    [request requestStsTokenWithType:@"1"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestUploadName];
    });
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"user_header",
                              @"number":@"1",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.dataDict = [back objectForKey:@"data"];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"编辑个人信息页"];
    
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
     [self trackPageEnd:@"编辑个人信息页"];
    [IQKeyboardManager sharedManager].enable = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataDict = @{};
    header_url = @"";
    
    [self setupSubviews];
    
    [self textValueChanged];

    [self requestStsData];
}

#pragma mark - > 视图初始化
- (void)setupSubviews{
    
    saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    saveButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15.2];
    saveButton.titleLabel.textAlignment = NSTextAlignmentRight;
    [saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [saveButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    
    saveButton.enabled = NO;

    editView = [[FWEditInformationView alloc] init];
    editView.delegate = self;
    editView.vc = self;
    [self.view addSubview:editView];
    [editView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
        
    [self addViewObserve];
}

- (void)addViewObserve{
    
    [editView.nameTextField addTarget:self action:@selector(textValueChanged) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - > 更换头像
-(void)uploadPhotoClick{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"更换头像" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:1];
    }];
    UIAlertAction *selectAction = [UIAlertAction actionWithTitle:@"选择相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectLibraryWithType:2];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];

    [alertController addAction:takePhotoAction];
    [alertController addAction:selectAction];
    [alertController addAction:cancelAction];

    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 选择相册
- (void)selectLibraryWithType:(NSInteger)type{
    
    if (@available(iOS 11, *)) {
        
        UIScrollView.appearance.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
    }
    
    NSUInteger sourceType = 0;

    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeCamera;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
    }else{
        if (type == 1){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }else if (type == 2){
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
    }
    
    picker= [[UIImagePickerController alloc] init];
    picker.delegate                 = self;
    picker.sourceType               = sourceType;
    picker.allowsEditing            = YES;
    picker.navigationController.navigationBar.hidden = NO;
    picker.fd_prefersNavigationBarHidden = NO;
    picker.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *avatar = info[UIImagePickerControllerEditedImage];
    //处理完毕，回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [ALiOssUploadTool asyncUploadImage:avatar WithBucketName:[self.dataDict objectForKey:@"bucket"] WithObject_key:[self.dataDict objectForKey:@"object_key"] WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
        if (UploadImageSuccess == state) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                editView.photoImageView.layer.cornerRadius = 100/2;
                editView.photoImageView.image = avatar;
                
                header_url = [self.dataDict objectForKey:@"object_key"];
                [self textValueChanged];
            });
        }
    }];
}

#pragma mark - > 保存
- (void)saveButtonClick{

    NSMutableDictionary * params = @{}.mutableCopy;
    
    NSString * sex         = [GFStaticData getObjectForKey:kTagUserKeySex];
    NSString * nickName    = [GFStaticData getObjectForKey:kTagUserKeyName];
    NSString * sign        = [GFStaticData getObjectForKey:kTagUserKeySign];

    if ([sex integerValue] != [editView.sexType integerValue]) {
        [params setObject:editView.sexType forKey:@"sex"];
    }
    
    if (![nickName isEqualToString:editView.nameTextField.text] && editView.nameTextField.text.length > 0) {
        [params setObject:editView.nameTextField.text forKey:@"nickname"];
    }
    
//    if (![sign isEqualToString:editView.signTextField.text] && editView.signTextField.text.length > 0) {
    [params setObject:editView.signTextField.text forKey:@"autograph"];
//    }
    
    if (header_url.length > 0) {
        [params setObject:header_url forKey:@"user_header"];
    }
    
    [self requestUpdateInfo:params withType:2];
}

#pragma mark - > 更新个人信息(1、上传图片；2保存修改)
- (void)requestUpdateInfo:(NSMutableDictionary *)params withType:(NSInteger)type{
    
    [self.view endEditing:YES];
    
    [params setObject:[GFStaticData getObjectForKey:kTagUserKeyID] forKey:@"uid"];
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    
    [request startWithParameters:params WithAction:Submit_update_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
    
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showSuccessMessage:@"修改成功" toController:self];
            
            if (type == 2) {
                
                [GFStaticData saveLoginData:[[back objectForKey:@"data"] objectForKey:@"user_info"]];
                
                [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
                [[FWUserCenter sharedManager] saveUserInfoToDiskWithJson:back];
                [FWUserCenter sharedManager].user_IsLogin = YES;
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 根据昵称和性别，判断保存按钮状态
- (void)textValueChanged{
    
    if (editView.nameTextField.text.length != 0) {
        
        saveButton.enabled = YES;
        [saveButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    }else{
        saveButton.enabled = NO;
        [saveButton setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([UIDevice currentDevice].systemVersion.floatValue < 11) {
        return;
    }
    if ([viewController isKindOfClass:NSClassFromString(@"PUPhotoPickerHostViewController")]) {
        [viewController.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.frame.size.width < 42) {
                [viewController.view sendSubviewToBack:obj];
                *stop = YES;
            }
        }];
    }
}

@end
