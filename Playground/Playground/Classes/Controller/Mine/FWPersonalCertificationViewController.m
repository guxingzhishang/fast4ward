//
//  FWPersonalCertificationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPersonalCertificationViewController.h"
#import "FWPersonalCertificationView.h"

@interface FWPersonalCertificationViewController ()

@property (nonatomic, strong) FWPersonalCertificationView * certificationView;

@end

@implementation FWPersonalCertificationViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手认证页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手认证页"];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"车手认证";
    
    self.certificationView = [[FWPersonalCertificationView alloc] init];
    self.certificationView.vc = self;
    [self.view addSubview:self.certificationView];
    [self.certificationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

@end
