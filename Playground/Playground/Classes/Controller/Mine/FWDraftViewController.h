//
//  FWDraftViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

static NSString * const draftCellId = @"draftCellId";

@interface FWDraftViewController : FWBaseViewController


/**
 * 草稿箱列表
 */
@property (nonatomic, strong) FWCollectionView * draftCollectionView;

@property(nonatomic, strong) FWBaseCollectionLayout *draftLayout;

@end

NS_ASSUME_NONNULL_END
