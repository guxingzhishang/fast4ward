//
//  FWVisitorDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVisitorDetailViewController.h"
#import "FWVisitorNameListCell.h"

@interface FWVisitorDetailViewController ()
@property (nonatomic, strong) NSMutableArray * nameListDataSource;

@property (nonatomic, strong)  FWTableView * nameListTableView;

@property (nonatomic, assign)  NSInteger nameListPageNum;

@property (nonatomic, strong) FWVisitorModel * visitorModel;

@end

@implementation FWVisitorDetailViewController
@synthesize nameListDataSource;
@synthesize nameListTableView;
@synthesize nameListPageNum;
@synthesize type;
@synthesize proviceModel;
@synthesize carModel;
@synthesize visitorModel;

#pragma mark - > 请求访客名单
- (void)requestNameListDataWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        self.nameListPageNum = 1;
        
        if (self.nameListDataSource.count > 0 ) {
            [self.nameListDataSource removeAllObjects];
        }
    }else{
        self.nameListPageNum +=1;
    }
    
    NSString * car_style_id = @"0";
    NSString * province_code = @"0";
    
    if ([self.type isEqualToString:@"1"]) {
        car_style_id = carModel.car_style_id?carModel.car_style_id:@"0";
    }else {
        province_code = proviceModel.province_code?proviceModel.province_code:@"0";
    }
    
    FWVisitorRequest * request = [[FWVisitorRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"type":self.type,
                              @"car_style_id":car_style_id,
                              @"province_code":province_code,
                              @"page":@(self.nameListPageNum).stringValue,
                              @"pagesize":@"20",
                              };
    
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_visitor_list_by_type  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [nameListTableView.mj_footer endRefreshing];
            }else{
                [nameListTableView.mj_header endRefreshing];
            }
            
            visitorModel = [FWVisitorModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            [self.nameListDataSource addObjectsFromArray:visitorModel.list_visitor];
            
            if([visitorModel.list_visitor count] == 0 && self.nameListPageNum != 1){
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [nameListTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"访客分析列表页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self trackPageEnd:@"访客分析列表页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.nameListDataSource = @[].mutableCopy;
    
    [self createnameListTableView];
    [self requestNameListDataWithLoadMoredData:NO];
    
    self.title = self.currentTitle;
}

- (void)createnameListTableView{
    
    nameListTableView = [[FWTableView alloc] init];
    nameListTableView.delegate = self;
    nameListTableView.dataSource = self;
    nameListTableView.rowHeight = 69;
    nameListTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    nameListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    nameListTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop);
    [self.view addSubview:nameListTableView];
    
    nameListTableView.isNeedEmptyPlaceHolder = YES;
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:@"还没有相关的内容哦~" attributes:Attributes];
    nameListTableView.emptyDescriptionString = attributeString;
    
    DHWeakSelf;
    
    nameListTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestNameListDataWithLoadMoredData:NO];
    }];
    nameListTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestNameListDataWithLoadMoredData:YES];
    }];
}

#pragma mark - > 访客名单列表`s Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger number = 0;
    number = self.nameListDataSource.count?self.nameListDataSource.count:0;
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"VistiorNameListCellID";
    
    FWVisitorNameListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWVisitorNameListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.vc = self;
    [cell.signLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(43/2);
        make.left.mas_equalTo(cell.nameLabel);
        make.top.mas_equalTo(cell.nameLabel.mas_bottom);
        make.right.mas_equalTo(cell.contentView).mas_offset(-5);
    }];

    [cell cellConfigureFor:self.nameListDataSource[indexPath.row]];
    cell.selectImageView.hidden = YES;

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FWVisitorNameListModel * visitorModel = self.nameListDataSource[indexPath.row];
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = visitorModel.uid;
    [self.navigationController pushViewController:UIVC animated:YES];
}
@end
