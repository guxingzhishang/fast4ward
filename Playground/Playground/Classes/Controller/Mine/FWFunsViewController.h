//
//  FWFunsViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 我的 - > 关注
 */
#import "FWBaseViewController.h"
#import "FWMineFunsCell.h"
#import "FWMessageFunsCell.h"

@interface FWFunsViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource,FWMineFunsCellDelegate,FWMessageFunsCellDelegate>

@property (nonatomic, strong) FWTableView * infoTableView;

/**
 * 1 消息列表的粉丝； 2 个人中心的粉丝; 3 其他用户的粉丝
 */
@property (nonatomic, strong) NSString * funsType;

@property (nonatomic, strong) NSString * base_uid;
@property (nonatomic, strong) NSMutableArray * newsPicURL;
@property (nonatomic, strong) NSMutableArray * oldPicURL;
@property (nonatomic, strong) NSMutableArray * refreshPicURL;

@end
