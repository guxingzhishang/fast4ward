//
//  FWShareRankViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/6.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 商家、车友排行榜
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWShareRankViewController : FWBaseViewController

@property (nonatomic, strong) UIImage * cutshortImage;
@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) NSString * shareType; // 1、商家   2、车友

@property (nonatomic, strong) NSString * titleString;

@end

NS_ASSUME_NONNULL_END
