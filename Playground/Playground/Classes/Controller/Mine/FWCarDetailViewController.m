//
//  FWCarDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarDetailViewController.h"
#import "FWCarDetailView.h"
#import "TZImagePickerController.h"
#import "ALiOssUploadTool.h"
#import "SYLineProgressView.h"
#import "FWManageCarViewController.h"
#import "FWAddCarViewController.h"
#import "FWScrollViewFollowCollectionView.h"
#import "FWMainTouchScrollView.h"
#import "FWCarDetailTongxiCell.h"
#import "FWCarModel.h"
#import "FWCarDetailPicsCell.h"
#import "FWCarDetailPicHeaderView.h"
#import "FWCarDetailRefitCell.h"
#import "FWCustomNavigationView.h"

@interface FWCarDetailViewController ()<TZImagePickerControllerDelegate,FWCarDetailViewDelegate,UITableViewDelegate,UITableViewDataSource,FWCarDetailPicHeaderViewDelegate>
{
    FWMainTouchScrollView *rootScrollView; //rootScrollView要有滑动穿透
    FWScrollViewFollowCollectionView *thFollow; //必须写成属性
    NSInteger  ScrollHeight;//需三处保持一致
    
    /* 左右划是否刷新（第一次划到视频或者草稿要刷新，其他情况只有上下拉刷新） */
    BOOL   isRefresh;
    BOOL   isGaizhuangRefresh;
    BOOL   isTongxiRefresh;
    
    CGFloat alphaValue;
}

@property (nonatomic, strong) FWCarDetailView * carDetailView;
@property (nonatomic, strong) NSDictionary * dataDict;
@property (nonatomic, strong) NSMutableArray * photoArray; // 每次新上传的图片
@property (nonatomic, strong) NSMutableArray * allPathArray; // 所有图片的路径
@property (nonatomic, strong) NSMutableArray * picArray;

@property (nonatomic, strong) UIView * naviBarView;

/* 承载collectionView的背景Scrollview */
@property (nonatomic, strong) UIScrollView * collectionScr;

@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UIView * blackView;
@property (nonatomic, strong) UILabel * upLabel;
@property (nonatomic, strong) UILabel * downLabel;
@property (nonatomic, strong) UIActivityIndicatorView *loading;
@property (nonatomic, strong) UIButton * uploadButton;

@property (nonatomic, assign) NSInteger  tongxiPageNum;
@property (nonatomic, strong) NSMutableArray * tongxiDataSource;

@property (nonatomic, strong) FWCarModel * carModel;
@property (nonatomic, strong) FWCarDetailPicHeaderView * picHeaderView;

@end

@implementation FWCarDetailViewController

#pragma mark - > 请求阿里临时验证
- (void)requestStsData{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    [request requestStsTokenWithType:@"1"];
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"car_imgs",
                              @"number":@(self.photoArray.count).stringValue,
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.dataDict = [back objectForKey:@"data"];
            
            self.bgView.hidden = NO;
            [self.loading startAnimating]; // 开始旋转

            NSArray * pathArr = [self.dataDict objectForKey:@"object_keys"];
            
            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, pathArr.count)];
            [self.allPathArray insertObjects:pathArr atIndexes:indexes];
            
            [ALiOssUploadTool asyncUploadImages:self.photoArray WithBucketName:[self.dataDict objectForKey:@"bucket"] WithObject_key:nil WithObject_keys:[self.dataDict objectForKey:@"object_keys"] complete:^(NSArray<NSString *> *names, UploadImageState state) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (UploadImageSuccess == state) {
                        [self uploadCarImages];
                    }else{
                        [[FWHudManager sharedManager] showErrorMessage:@"上传失败，请重试" toController:self];
                    }
                });
            }];
        }
    }];
}

#pragma mark - > 上传座驾图片
- (void)uploadCarImages{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSString * car_imgs = [self.allPathArray mj_JSONString];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"user_car_id":self.listModel.user_car_id,
                              @"car_imgs":car_imgs,
                              };
    
    [request startWithParameters:params WithAction:Upload_car_imgs WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        [self.loading stopAnimating]; // 结束旋转
        self.bgView.hidden = YES;

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [self requestCarInfo];
            [[FWHudManager sharedManager] showErrorMessage:@"上传成功" toController:self];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"上传失败，请重试" toController:self];
        }
    }];
}

#pragma mark - > 根据ID获取座驾信息
- (void)requestCarInfo{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"user_car_id":self.user_car_id,
                              };
    
    [request startWithParameters:params WithAction:Get_car_info_by_id WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.listModel = [FWCarListModel mj_objectWithKeyValues:[[back objectForKey:@"data"] objectForKey:@"car_info"]];
            
            if ([self.listModel.flag_hidden isEqualToString:@"1"]){
                
                [[FWHudManager sharedManager] showErrorMessage:self.listModel.flag_hidden_msg toController:self];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
                return ;
            }
            
            [self.carDetailView configForView:self.listModel];
        
            if (self.listModel.car_imgs.count > 0) {
                if (self.picArray.count > 0) {
                    [self.picArray removeAllObjects];
                }
                
                if ([self.listModel.user_info.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
                    self.picHeaderView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 20);
                    self.picTableView.tableHeaderView = self.picHeaderView;
                }
                
                [self.picArray addObjectsFromArray:self.listModel.car_imgs];
                [self.picTableView reloadData];
            }else{
                self.picTableView.tableHeaderView = nil;
            }
            
            [rootScrollView.mj_header endRefreshing];

            [self.gaizhuangTableView reloadData];
            
            [self refreshFrame];

            if (self.allPathArray.count > 0) {
                [self.allPathArray removeAllObjects];
            }
            for (FWCarImgsModel * imageModel in self.listModel.car_imgs) {
                [self.allPathArray addObject:imageModel.car_img];
            }
            
            FWUserDefaultsVariableModel * usvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
            
            if ([usvm.userModel.uid isEqualToString:self.listModel.user_info.uid]) {
                self.uploadButton.hidden = NO;
                self.carDetailView.editButton.hidden = NO;
            }else{
                self.uploadButton.hidden = YES;
                self.carDetailView.editButton.hidden = YES;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)requestTongxiReloadMore:(BOOL)isReloadMore{
    
    if (isReloadMore == NO) {
        self.tongxiPageNum = 1;
        
        if (self.tongxiDataSource.count > 0 ) {
            [self.tongxiDataSource removeAllObjects];
        }
    }else{
        self.tongxiPageNum +=1;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
    NSDictionary * params = @{
                            @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                            @"page":@(self.tongxiPageNum).stringValue,
                            @"page_size":@"20",
                            @"user_car_id":self.listModel.user_car_id,
    };
    
    [request startWithParameters:params WithAction:Get_car_list_by_type WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isReloadMore) {
                [self.tongxiTableView.mj_footer endRefreshing];
            }else{
                [self.tongxiTableView scrollToTopAnimated:YES];
                [rootScrollView.mj_header endRefreshing];
            }
            
            self.carModel = [FWCarModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.tongxiDataSource addObjectsFromArray:self.carModel.list];
            
            if([self.carModel.list count] == 0 &&
               self.tongxiPageNum != 1){
                self.tongxiTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.tongxiTableView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 刷新视图坐标
- (void)refreshFrame{
    
    CGFloat height = [self.carDetailView getCurrentViewHeight];
    
    ScrollHeight = [self.carDetailView getBarViewTopHeight]-(64+FWCustomeSafeTop);

    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+height);

    self.carDetailView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
    
    self.collectionScr.frame = CGRectMake(0, CGRectGetMaxY(self.carDetailView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop);
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH*3, 0);

    self.picTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH,  CGRectGetHeight(self.collectionScr.frame));

    self.gaizhuangTableView.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH,  CGRectGetHeight(self.collectionScr.frame));

    self.tongxiTableView.frame = CGRectMake(2*SCREEN_WIDTH, 0, SCREEN_WIDTH,  CGRectGetHeight(self.collectionScr.frame));

    
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[self.picTableView,self.gaizhuangTableView,self.tongxiTableView]];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"座驾详情页"];
    
    [self requestCarInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"座驾详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    
    
    ScrollHeight = 180;
    alphaValue = 0;

    self.currentType = FWCarPicType;
    
    isTongxiRefresh = YES;
    
    self.dataDict = @{};
    self.picArray = @[].mutableCopy;
    self.photoArray = @[].mutableCopy;
    self.allPathArray = @[].mutableCopy;
    self.tongxiDataSource = @[].mutableCopy;
    
    [self requestStsData];
    [self requestCarInfo];
    
    [self setupSubViews];
}

- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupSubViews{
    
    self.naviBarView = [[UIView alloc] init];
    self.naviBarView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.naviBarView.userInteractionEnabled = YES;
    [self.view addSubview:self.naviBarView];
    self.naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    self.naviBarView.alpha = 0;
    
    UIButton* backButton= [[UIButton alloc] initWithFrame: CGRectMake(14,FWCustomeSafeTop+20,45,40)];
    backButton.titleLabel.font = DHSystemFontOfSize_16;
    backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [backButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.naviBarView addSubview:backButton];
    
    rootScrollView = [[FWMainTouchScrollView alloc] init];
    rootScrollView.frame  = CGRectMake(0, 0,SCREEN_WIDTH, SCREEN_HEIGHT);
    rootScrollView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    rootScrollView.delegate=self; //要遵循代理
    rootScrollView.showsVerticalScrollIndicator = NO;
    rootScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetHeight(rootScrollView.frame)+ScrollHeight);  //滚动大小
    [self.view addSubview:rootScrollView];
    
    
    self.carDetailView = [[FWCarDetailView alloc] init];
    self.carDetailView.vc = self;
    self.carDetailView.delegate = self;
    [rootScrollView addSubview:self.carDetailView];
    self.carDetailView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 383+10+50);

    self.collectionScr = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.carDetailView.frame)+1,SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop-90)];
    self.collectionScr.delegate = self;
    self.collectionScr.pagingEnabled = YES;
    self.collectionScr.showsVerticalScrollIndicator = NO;
    self.collectionScr.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [rootScrollView addSubview:self.collectionScr];
    self.collectionScr.contentSize = CGSizeMake(SCREEN_WIDTH*3, 0);
    
    self.picTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,  CGRectGetHeight(self.collectionScr.frame)) style:UITableViewStylePlain];
    self.picTableView.delegate = self;
    self.picTableView.dataSource = self;
    self.picTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.picTableView.rowHeight = 130;
    [self.collectionScr addSubview:self.picTableView];
    
    self.gaizhuangTableView = [[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH,  CGRectGetHeight(self.collectionScr.frame)) style:UITableViewStylePlain];
    self.gaizhuangTableView.delegate = self;
    self.gaizhuangTableView.dataSource = self;
    self.gaizhuangTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.gaizhuangTableView.rowHeight = 215;
    [self.collectionScr addSubview:self.gaizhuangTableView];

    self.tongxiTableView = [[UITableView alloc] initWithFrame:CGRectMake(2*SCREEN_WIDTH, 0, SCREEN_WIDTH,  CGRectGetHeight(self.collectionScr.frame)) style:UITableViewStylePlain];
    self.tongxiTableView.delegate = self;
    self.tongxiTableView.dataSource = self;
    self.tongxiTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tongxiTableView.rowHeight = 215;
    [self.collectionScr addSubview:self.tongxiTableView];
    
    
    self.picHeaderView = [[FWCarDetailPicHeaderView alloc] init];
    self.picHeaderView.delegate = self;
    
    //利用initRoot创建 给予底部上下滑动的ScrollView 要滑动的ScrollRange 大小和左右滑动的Table数组
    thFollow = [[FWScrollViewFollowCollectionView alloc]init];
    [thFollow RootScrollView:rootScrollView AndRootScrollViewScrollRange:ScrollHeight AndTableArray:@[self.picTableView,self.gaizhuangTableView,self.tongxiTableView]];

    {
       @weakify(self);
       rootScrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
           @strongify(self);
           if (self.currentType == FWCarTongxiType){
               [self requestTongxiReloadMore:NO];
           }else{
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  [rootScrollView.mj_header endRefreshing];
               });
           }
       }];
       
       self.tongxiTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
           @strongify(self);
           [self requestTongxiReloadMore:YES];
       }];
    }
    
    /* 如果是自己，显示上传按钮 */
    self.uploadButton = [[UIButton alloc] init];
    [self.uploadButton setImage:[UIImage imageNamed:@"upload_car"] forState:UIControlStateNormal];
    [self.uploadButton addTarget:self action:@selector(uploadButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.uploadButton];
    [self.uploadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).mas_offset(-5);
        make.size.mas_equalTo(CGSizeMake(52, 52));
        make.bottom.mas_equalTo(self.view).mas_offset(-88);
    }];
    self.uploadButton.hidden = YES;
    
    [self setupUploadView];
    [self.view bringSubviewToFront:self.naviBarView];
}

- (void)uploadButtonClick{
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowTakePicture = YES;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
        self.photoArray = [photos mutableCopy];
        [self requestUploadName];
    }];
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}


- (void)setupUploadView{
    
    self.bgView = [[UIView alloc] init];
    self.bgView.backgroundColor = FWClearColor;
    self.bgView.frame = CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
    [self.view addSubview:self.bgView];
    
    self.blackView = [[UIView alloc] init];
    self.blackView.layer.cornerRadius = 5;
    self.blackView.layer.masksToBounds = YES;
    self.blackView.backgroundColor = FWColorWihtAlpha(@"000000", 0.7);
    self.blackView.frame = CGRectMake((SCREEN_WIDTH-235)/2, (SCREEN_HEIGHT-140)/2, 235, 140);
    [self.bgView addSubview:self.blackView];
    
    self.upLabel = [[UILabel alloc] init];
    self.upLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.upLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.upLabel.text = @"正在上传图片";
    self.upLabel.textAlignment = NSTextAlignmentCenter;
    [self.blackView addSubview:self.upLabel];
    self.upLabel.frame = CGRectMake(0, 25, CGRectGetWidth(self.blackView.frame), 18);
    
    self.downLabel = [[UILabel alloc] init];
    self.downLabel.text = @"请不要离开哦";
    self.downLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 14];
    self.downLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.downLabel.textAlignment = NSTextAlignmentCenter;
    [self.blackView addSubview:self.downLabel];
    self.downLabel.frame = CGRectMake(0, CGRectGetMaxY(self.upLabel.frame), CGRectGetWidth(self.blackView.frame), 18);
   
    self.loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.loading.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [self.blackView addSubview:self.loading];
    self.loading.center = CGPointMake(235/2, 140/2+30);
    
    self.bgView.hidden = YES;
}

- (void)carPicViewClick:(NSInteger)index{

    NSMutableArray * imageArray = @[].mutableCopy;
    for (FWCarImgsModel * imageModel in self.listModel.car_imgs) {
        [imageArray addObject:imageModel.car_img_original];
    }

    FWPhotoBrowser *browser = [[FWPhotoBrowser alloc] init];
    browser.isFullWidthForLandScape = YES;
    browser.isNeedLandscape = YES;
    browser.currentImageIndex = (int)index;
    browser.imageArray = [imageArray mutableCopy];
    browser.originalImageArray = [imageArray copy];
    browser.vc = self;
    browser.fromType = 2;
    [browser show];
}

#pragma mark - > 图片管理
- (void)manageButtonOnClick{
    
    DHWeakSelf;
    FWManageCarViewController * MCVC = [[FWManageCarViewController alloc] init];
    MCVC.user_car_id = self.listModel.user_car_id;
    MCVC.isRequest = ^(BOOL isRequest) {
        if (isRequest) {
            [weakSelf requestCarInfo];
        }
    };
    [self.navigationController pushViewController:MCVC animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.picTableView) {
        if (self.picArray.count %2 == 0) {
            /* 偶数 */
            return self.picArray.count/2;
        }else{
            /* 奇数 */
            return self.picArray.count/2+1;
        }
    }else if (tableView == self.gaizhuangTableView){
        return 2;
    }else{
        return self.tongxiDataSource.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.picTableView) {
        return 130;
    }else if (tableView == self.tongxiTableView){
        return 215;
    }else{
        if (indexPath.row == 0) {
            return 53;
        }else{
            return 280;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.picTableView) {
        static NSString * cellID = @"FWCarDetailPicsCellID";
        
        FWCarDetailPicsCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[FWCarDetailPicsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        NSInteger leftindex = indexPath.row *2;
        NSInteger rightindex = indexPath.row *2 +1;
        
        if (self.picArray.count > leftindex) {
            [cell configForLeftView:self.picArray[leftindex]];
            
            //左侧列的按钮Block回调
            cell.leftButtonBlock = ^(void){
                [self carPicViewClick:indexPath.row];
            };
        }
        
        if (self.picArray.count > rightindex) {
            
            cell.rightCarPicView.hidden = NO;
            [cell configForRightView:self.picArray[rightindex]];
            
            //右侧列的按钮Block回调
            cell.rightButtonBlock = ^(void){
                [self carPicViewClick:indexPath.row];
            };
        }else{
            cell.rightCarPicView.hidden = YES;
        }
        
        return cell;
    }else if(tableView == self.gaizhuangTableView){
        
        if (indexPath.row == 0) {
            static NSString * cellID = @"bianjiDetailCellID";
            
            FWCarDetailRefitCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[FWCarDetailRefitCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
            cell.vc = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:self.listModel.user_info.uid]){
                cell.editButton.hidden = NO;
            }else{
                cell.editButton.hidden = YES;
            }
            cell.listModel = self.listModel;
            
            return cell;
        }else{
            static NSString * cellID = @"bianjiCellID";
            
            FWCarDetailRefitDetailCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (!cell) {
                cell = [[FWCarDetailRefitDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            if (self.listModel.gaizhuang_list_show.count > 0) {
                
                NSString * list_show = [self.listModel.gaizhuang_list_show componentsJoinedByString:@"\n"];
                
                cell.detailLabel.textColor = FWTextColor_222222;
                cell.detailLabel.text = list_show;
            }else{
                cell.detailLabel.textColor = FWTextColor_BCBCBC;
                cell.detailLabel.text = @"暂无改装项目";
            }
            
            return cell;
        }
    }else{
        
        static NSString * cellID = @"tongxicheyouID";
        
        FWCarDetailTongxiCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[FWCarDetailTongxiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.vc = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (self.tongxiDataSource.count > indexPath.row) {
            [cell configForCell:self.tongxiDataSource[indexPath.row]];
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.picTableView) {
        //
    }else if (tableView == self.gaizhuangTableView){
        
    }else{
        if (self.tongxiDataSource.count > indexPath.row) {
            FWCarListModel * listModel = self.tongxiDataSource[indexPath.row];
            
            FWCarDetailViewController * DVC = [[FWCarDetailViewController alloc] init];
            DVC.user_car_id = listModel.user_car_id;
            [self.navigationController pushViewController:DVC animated:YES];
        }
    }
}

#pragma mark - > 编辑座驾
- (void)editButtonOnClick{
    
    DHWeakSelf;
    
    FWAddCarViewController * ACVC = [[FWAddCarViewController alloc] init];
    ACVC.type = @"edit";
    ACVC.listModel = self.listModel;
    ACVC.isReFresh = ^(BOOL isRequest) {
        if (isRequest) {
            [weakSelf requestCarInfo];
        }
    };
    [self.navigationController pushViewController:ACVC animated:YES];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (self.collectionScr == scrollView){
        
        float scrollViewX = scrollView.contentOffset.x;
        if (scrollViewX == 0) {
            [self headerBtnClick:self.carDetailView.barView.firstButton];
        }else if (scrollViewX == SCREEN_WIDTH){
            [self headerBtnClick:self.carDetailView.barView.secondButton];
        }else if (scrollViewX == SCREEN_WIDTH *2){
            [self headerBtnClick:self.carDetailView.barView.thirdButton];
        }
    }
    
    if (scrollView == rootScrollView) {
        
        if (scrollView.contentOffset.y > 30){
            alphaValue = 1;
        }else{
            alphaValue = (scrollView.contentOffset.y/30);
        }
        
        self.naviBarView.alpha = alphaValue;
    }
    
    [thFollow followScrollViewScrollScrollViewScroll:scrollView];
}

#pragma mark -> 点击关注 & 推荐
- (void)headerBtnClick:(UIButton *)sender{
    
    if ([sender isEqual:self.carDetailView.barView.firstButton]) {
        
        self.currentType = FWCarPicType;

//        if (isRefresh) {
//            isRefresh = NO;
//            [self requestTongxiReloadMore:NO];
//        }else{
//            [self.tongxiTableView reloadData];
//        }
        
        self.carDetailView.barView.firstButton.selected = YES;
        self.carDetailView.barView.secondButton.selected =  NO;
        self.carDetailView.barView.thirdButton.selected =  NO;

        self.carDetailView.barView.shadowView.frame = CGRectMake(CGRectGetMinX(self.carDetailView.barView.firstButton.frame)+(CGRectGetWidth(self.carDetailView.barView.firstButton.frame)-32)/2, CGRectGetMaxY(self.carDetailView.barView.firstButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if([sender isEqual:self.carDetailView.barView.secondButton]) {
        
        self.currentType = FWCarGaizhuangType;
//
//        if (isVideoRefresh) {
//            isVideoRefresh = NO;
//            [self requestVideoListWithLoadMoreData:NO];
//        }else{
//            [videoCollectionView reloadData];
//        }
        
        self.carDetailView.barView.firstButton.selected = NO;
        self.carDetailView.barView.secondButton.selected =  YES;
        self.carDetailView.barView.thirdButton.selected =  NO;

        self.carDetailView.barView.shadowView.frame = CGRectMake(CGRectGetMinX(self.carDetailView.barView.secondButton.frame)+(CGRectGetWidth(self.carDetailView.barView.secondButton.frame)-32)/2, CGRectGetMaxY(self.carDetailView.barView.secondButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }else if([sender isEqual:self.carDetailView.barView.thirdButton]) {
        
        self.currentType = FWCarTongxiType;

        if (isTongxiRefresh) {
            isTongxiRefresh = NO;
            [self requestTongxiReloadMore:NO];
        }else{
            [self.tongxiTableView reloadData];
        }

        self.carDetailView.barView.firstButton.selected = NO;
        self.carDetailView.barView.secondButton.selected =  NO;
        self.carDetailView.barView.thirdButton.selected = YES;

        self.carDetailView.barView.shadowView.frame = CGRectMake(CGRectGetMinX(self.carDetailView.barView.thirdButton.frame)+(CGRectGetWidth(self.carDetailView.barView.thirdButton.frame)-32)/2, CGRectGetMaxY(self.carDetailView.barView.thirdButton.frame)-SELECTBAR_BOTTOM_PADDING,32, 1.5);
    }

    // 点击btn的时候scrollowView的contentSize发生变化
    [self.collectionScr setContentOffset:CGPointMake((self.currentType-1) *SCREEN_WIDTH, 0) animated:YES];
}

@end
