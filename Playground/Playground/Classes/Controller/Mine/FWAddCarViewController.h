//
//  FWAddCarViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWChooseCarStyleModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^requestNewData)(BOOL isRequest);

@interface FWAddCarViewController : FWBaseViewController

@property (nonatomic, strong) FWCarListModel * listModel;
@property (nonatomic, strong) NSString * type; // add: 添加  edit:编辑
@property (nonatomic, copy) requestNewData isReFresh;

@property (nonatomic, strong) FWChooseCarStyleSubListModel * subListModel;

@end

NS_ASSUME_NONNULL_END
