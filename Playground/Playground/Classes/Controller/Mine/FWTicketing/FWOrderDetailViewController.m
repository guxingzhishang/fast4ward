//
//  FWOrderDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWOrderDetailViewController.h"
#import "FWOrderDetailView.h"
#import "FWConversationViewController.h"
#import "FWTicketDetailModel.h"

@interface FWOrderDetailViewController ()

@property (nonatomic, strong) FWOrderDetailView * orderView;
@property (nonatomic, strong) FWTicketDetailModel * detailModel;

@end

@implementation FWOrderDetailViewController

- (void)requestTicketOrder{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"order_id":self.order_id,
                              };
    
    [request startWithParameters:params WithAction:Get_ticket_order_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.detailModel = [FWTicketDetailModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.orderView configForView:self.detailModel];
            
            if ([self.detailModel.pay_status isEqualToString:@"2"]) {
                
                UIButton * contectButton = [[UIButton alloc] init];
                contectButton.frame = CGRectMake(0, 0, 45, 40);
                [contectButton setImage:[UIImage imageNamed:@"order_contect"] forState:UIControlStateNormal];
                [contectButton addTarget:self action:@selector(contectButtonClick) forControlEvents:UIControlEventTouchUpInside];
                
                UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:contectButton];
                self.navigationItem.rightBarButtonItems = @[rightBtnItem];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"订单详情页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"订单详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"订单详情";

    [self setupSubviews];
    
    [self requestTicketOrder];
}

- (void)setupSubviews{

    self.orderView = [[FWOrderDetailView alloc] init];
    self.orderView.order_id = self.order_id;
    self.orderView.vc = self;
    [self.view addSubview:self.orderView];
    [self.orderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - > 联系客服
- (void)contectButtonClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        if ([GFStaticData getObjectForKey:kKefuUID]) {
            FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:[GFStaticData getObjectForKey:kKefuUID]];
            [self.navigationController pushViewController:CVC animated:YES];
        }
    }
}

#pragma mark - > 检测是否登录
- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}
@end
