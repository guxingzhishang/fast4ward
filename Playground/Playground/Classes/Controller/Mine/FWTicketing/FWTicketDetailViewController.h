//
//  FWTicketDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWTicketDetailViewController : FWBaseViewController<UIScrollViewDelegate,UIWebViewDelegate>


@property (nonatomic, strong) UIScrollView * mainView;

@property (nonatomic, strong) UIView * topView;
@property (nonatomic, strong) UIImageView * topImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * addressLabel;

@property (nonatomic, strong) UIView * middleView;
@property (nonatomic, strong) UILabel * priceLabel;

@property (nonatomic, strong) UIButton * minButton;
@property (nonatomic, strong) UIButton * maxButton;
@property (nonatomic, strong) UILabel * countLabel;

@property (nonatomic, strong) UIView * descView;
@property (nonatomic, strong) UILabel * descLabel;

@property (nonatomic, strong) UIWebView * detailWebView;

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIButton * contectButton;
@property (nonatomic, strong) UIButton * signupButton;

@property (nonatomic, assign) NSInteger ticketCount;

@property (nonatomic, strong) NSString * ticket_id;


@end

NS_ASSUME_NONNULL_END
