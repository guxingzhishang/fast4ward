//
//  FWConfirmOrderViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 确认订单
 */
#import "FWBaseViewController.h"
#import "FWTicketDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWConfirmOrderViewController : FWBaseViewController

@property (nonatomic, strong) NSString * pay_fee;
@property (nonatomic, strong) NSString * ticketCount;
@property (nonatomic, strong) NSString * ticket_id;
@property (nonatomic, strong) FWTicketDetailModel * detailModel;

@property (nonatomic, strong) NSString * order_id;


@end

NS_ASSUME_NONNULL_END
