//
//  FWScanCheckViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWScanCheckViewController.h"
#import "FWCheckingDetailViewController.h"
#import "FWCheckModel.h"
#import "FWDriverInfoViewController.h"
#import "FWGoldenCheckingDetailViewController.h"
#import "FWGoldenCheckModel.h"

@interface FWScanCheckViewController ()

@end

@implementation FWScanCheckViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"扫码核销页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"扫码核销页"];
}


- (void)viewDidLoad {
    
    self.type = self.fromType;
    [super viewDidLoad];
}

- (void)sw_handleWithValue:(NSString *)value{

    if (value.length < 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"二维码有问题，请重新尝试" toController: self];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        return;
    }
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if ([self.fromType isEqualToString:@"ticket_check"]) {
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"order_code":value,
                                  };
        
        [request startWithParameters:params WithAction:Get_ticket_check_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                FWCheckModel * checkModel = [FWCheckModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                
                FWCheckingDetailViewController * CDVC = [[FWCheckingDetailViewController alloc] init];
                CDVC.checkModel = checkModel;
                [self.navigationController pushViewController:CDVC animated:YES];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }];
    }else if ([self.fromType isEqualToString:@"driver_info"]){
        FWDriverInfoViewController * VC = [[FWDriverInfoViewController alloc] init];
        VC.baoming_user_id = value;
        VC.type= @"1";
        [self.navigationController pushViewController:VC animated:YES];
    }else if ([self.fromType isEqualToString:@"golden_check"]){
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"code":value,
                                  };
        
        [request startWithParameters:params WithAction:Get_gold_balance_by_code WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                FWGoldenCheckModel * checkModel = [FWGoldenCheckModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
                
                FWGoldenCheckingDetailViewController * VC = [[FWGoldenCheckingDetailViewController alloc] init];
                VC.checkModel = checkModel;
                VC.code = value;
                [self.navigationController pushViewController:VC animated:YES];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }];
    }
}

@end
