//
//  FWTicketCheckingViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWTicketCheckingViewController : FWBaseViewController

@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * idLabel;


@end

NS_ASSUME_NONNULL_END
