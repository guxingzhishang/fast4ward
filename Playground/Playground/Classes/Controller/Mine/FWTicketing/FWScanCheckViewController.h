//
//  FWScanCheckViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "SWQRCodeViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWScanCheckViewController : SWQRCodeViewController
@property (nonatomic, strong) NSString * fromType;

@end

NS_ASSUME_NONNULL_END
