//
//  FWCheckingDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 核销详情页（扫码或输入辅助码进入该页面）
 */
#import "FWBaseViewController.h"
#import "FWCheckModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCheckingDetailViewController : FWBaseViewController

@property (nonatomic, strong) FWCheckModel * checkModel;


@end

NS_ASSUME_NONNULL_END
