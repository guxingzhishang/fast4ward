//
//  FWTicketDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWTicketDetailViewController.h"
#import "FWTicketDetailShareViewController.h"
#import "FWConversationViewController.h"
#import "FWTicketDetailModel.h"
#import "FWConfirmOrderViewController.h"

@interface FWTicketDetailViewController ()<ShareViewDelegate>

@property (nonatomic, strong) UIView * naviBarView;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIButton * shareButton;
@property (nonatomic, strong) UILabel * navititleLabel;
@property (nonatomic, assign) CGFloat alphaValue;
@property (nonatomic, strong) FWTicketDetailModel * detailModel;

@end

@implementation FWTicketDetailViewController
@synthesize mainView;
@synthesize topImageView;
@synthesize titleLabel;
@synthesize timeLabel;
@synthesize addressLabel;
@synthesize middleView;
@synthesize priceLabel;
@synthesize minButton;
@synthesize maxButton;
@synthesize countLabel;
@synthesize descView;
@synthesize descLabel;
@synthesize detailWebView;
@synthesize topView;
@synthesize ticketCount;
@synthesize naviBarView;
@synthesize navititleLabel;
@synthesize alphaValue;
@synthesize shareButton;
@synthesize backButton;

- (void)requestRongYunTokenIM{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_im_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:userId];
                                            [RCIM sharedRCIM].currentUserInfo = userInfo;
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"IM登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            NSLog(@"IMtoken错误");
                                        }];
        }
    }];
}

#pragma mark - > 请求门票详情
- (void)requestTicketInfo{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"ticket_id":self.ticket_id,
                              };
    
    [request startWithParameters:params WithAction:Get_ticket_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.detailModel = [FWTicketDetailModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            NSString *  htmlStr = [self.detailModel.h5_ticket_detail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:htmlStr]];
            [self.detailWebView loadRequest:request];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            self.titleLabel.text = self.detailModel.title;
            self.timeLabel.text = [NSString stringWithFormat:@"日期：%@",self.detailModel.sport_date];
            self.addressLabel.text = [NSString stringWithFormat:@"地点：%@",self.detailModel.sport_area];
            self.priceLabel.text = self.detailModel.price;
            
            [self.topImageView sd_setImageWithURL:[NSURL URLWithString:self.detailModel.detail_page_img] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            self.topImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.topImageView.clipsToBounds = YES;

            if ([self.detailModel.status_user_buy isEqualToString:@"1"]) {
                if ([self.detailModel.can_buy_number integerValue] > 0) {
                    self.signupButton.enabled = YES;
                    self.signupButton.backgroundColor = FWColor(@"#222222");
                    [self.signupButton setTitle:@"立即购买" forState:UIControlStateNormal];
                }else{
                    self.signupButton.enabled = NO;
                    self.signupButton.backgroundColor = FWColor(@"#E5E5E5");
                    [self.signupButton setTitle:@"已售罄" forState:UIControlStateNormal];
                }
            }else if ([self.detailModel.status_user_buy isEqualToString:@"2"]) {
                self.signupButton.enabled = NO;
                self.signupButton.backgroundColor = FWColor(@"#E5E5E5");
                [self.signupButton setTitle:@"已达到购买次数限制" forState:UIControlStateNormal];
            }
            
            /* 库存为0，隐藏布进器 */
            if ([self.detailModel.can_buy_number integerValue] <= 0) {
                self.maxButton.hidden = YES;
                self.countLabel.hidden = YES;
                self.minButton.hidden = YES;
            }else{
                self.maxButton.hidden = NO;
                self.countLabel.hidden = NO;
                self.minButton.hidden = NO;
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    if (webView.isLoading) {
        return;
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    [self.descView layoutIfNeeded];
    
    CGFloat descViewHeight = CGRectGetMaxY(self.descView.frame);
    NSString * htmlHeight = [self.detailWebView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    
    self.detailWebView.frame = CGRectMake(10,descViewHeight+10 , SCREEN_WIDTH-20, [htmlHeight floatValue]);
    NSLog(@"self.detailWebView.frame=%@",NSStringFromCGRect(self.detailWebView.frame));
    
    UIScrollView * tempView=(UIScrollView *)[self.detailWebView.subviews objectAtIndex:0];
    tempView.scrollEnabled=NO;
    
    self.mainView.contentSize = CGSizeMake(0, CGRectGetMaxY(self.detailWebView.frame));
    NSLog(@"self.mainView.contentSize===%.2f",self.mainView.contentSize.height);
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    /* 加载失败，取消加载 */
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"门票详情页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"门票详情页"];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"门票详情";
    self.ticketCount = 1;
    
    [self setupTopView];
    [self setupBottomView];

    [self requestTicketInfo];
}


#pragma mark - > 分享
- (void)shareButtonClick{
    
    if ([self.detailModel.share_type isEqualToString:@"h5"]) {
        ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
        [shareView showView];
    }else{
        FWTicketDetailShareViewController * vc = [[FWTicketDetailShareViewController alloc] init];
        vc.share_url = self.detailModel.share_img;
        vc.qrcode_url = self.detailModel.qrcode_url;
        vc.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    FWFeedListModel * model = [[FWFeedListModel alloc] init];
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        
        model.feed_id = @"other";
        model.share_url = self.detailModel.h5_share_url;
        model.share_desc = self.detailModel.h5_share_desc;
        model.share_title = self.detailModel.h5_share_title;
        model.feed_cover = self.detailModel.share_img;
       
        if (index == 0){
            [[ShareManager defaultShareManager] sendWXWithModel:model WithType:0];
        }else if (index == 1){
            [[ShareManager defaultShareManager] sendWXWithModel:model WithType:1];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}


#pragma mark - > 返回
- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - > 报名咨询
- (void)contectButtonClick{
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:self.detailModel.kefu_uid];
        [self.navigationController pushViewController:CVC animated:YES];
    }
}

#pragma mark - > 立即购买
- (void)signupButtonClick{
    
    self.signupButton.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.signupButton.enabled = YES;
    });
    
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        
        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"ticket_id":self.ticket_id,
                                  @"buy_number":@(self.ticketCount).stringValue,
                                  };
        
        [request startWithParameters:params WithAction:Get_ticket_pay_fee WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                NSString * pay_fee = [[back objectForKey:@"data"] objectForKey:@"pay_fee"];
                
                FWConfirmOrderViewController * COVC = [[FWConfirmOrderViewController alloc] init];
                COVC.pay_fee = pay_fee;
                COVC.detailModel = self.detailModel;
                COVC.ticket_id = self.ticket_id;
                COVC.ticketCount = @(self.ticketCount).stringValue;
                [self.navigationController pushViewController:COVC animated:YES];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}

#pragma mark - > 增加票数
- (void)maxButtonClick{
    
    if (ticketCount < [self.detailModel.can_buy_number integerValue]) {
        ticketCount += 1;
        self.countLabel.text =@(ticketCount).stringValue;
        [maxButton setImage:[UIImage imageNamed:@"buy_add"] forState:UIControlStateNormal];
        [minButton setImage:[UIImage imageNamed:@"buy_sub"] forState:UIControlStateNormal];

        if (ticketCount >= [self.detailModel.can_buy_number integerValue]) {
            [maxButton setImage:[UIImage imageNamed:@"buy_max"] forState:UIControlStateNormal];
        }
    }else{
        if ([self.detailModel.number_remain integerValue] > [self.detailModel.can_buy_number integerValue]) {
            /* 余票数 > 本次可购买数 说明余票充足，就按照单次购买限制来提示*/
            [[FWHudManager sharedManager] showErrorMessage:[NSString stringWithFormat:@"每个订单限购%@张",self.detailModel.can_buy_number] toController:self];
        }else{
            /* 余票数 <= 本次可购买数 说明余票不足 */
            [[FWHudManager sharedManager] showErrorMessage:@"门票剩余数量不足" toController:self];
        }
    }
}

#pragma mark - > 减少票数
- (void)minButtonClick{
    
    if (ticketCount > 1) {
        ticketCount -= 1;
        self.countLabel.text =@(ticketCount).stringValue;
        [minButton setImage:[UIImage imageNamed:@"buy_sub"] forState:UIControlStateNormal];
        [maxButton setImage:[UIImage imageNamed:@"buy_add"] forState:UIControlStateNormal];

        if (ticketCount <= 1) {
            [minButton setImage:[UIImage imageNamed:@"buy_min"] forState:UIControlStateNormal];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"最少购买1张门票" toController:self];
        [minButton setImage:[UIImage imageNamed:@"buy_min"] forState:UIControlStateNormal];
    }
}

#pragma mark - > 初始化视图
- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)setupTopView{
    
    if ([WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled]) {
        self.shareButton.hidden = NO;
        
        UIButton * shareButton = [[UIButton alloc] init];
        shareButton.frame = CGRectMake(0, 0, 45, 40);
        [shareButton setImage:[UIImage imageNamed:@"new_black_share"] forState:UIControlStateNormal];
        [shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
        self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    }
    
    mainView = [[UIScrollView alloc] init];
    mainView.delegate = self;
    mainView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-(64+FWCustomeSafeTop)-(60+FWSafeBottom));
    [self.view addSubview:mainView];
    
    topView = [[UIView alloc] init];
    topView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [mainView addSubview:topView];
    [topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.mainView);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    topImageView = [[UIImageView alloc] init];
    topImageView.contentMode = UIViewContentModeScaleAspectFill;
    topImageView.clipsToBounds = YES;
    topImageView.image = [UIImage imageNamed:@"placeholder"];
    [topView addSubview:topImageView];
    [topImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(topView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 198*SCREEN_WIDTH/375));
    }];
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.numberOfLines = 2;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [topView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topImageView.mas_bottom).mas_offset(10);
        make.left.mas_equalTo(topImageView).mas_offset(14);
        make.right.mas_equalTo(topImageView).mas_offset(-14);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    timeLabel.textColor = FWColor(@"A8ACB3");
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [topView addSubview:timeLabel];
    [timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(3);
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_equalTo(16);
    }];
    
    addressLabel = [[UILabel alloc] init];
    addressLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 13];
    addressLabel.textColor = FWColor(@"A8ACB3");
    addressLabel.textAlignment = NSTextAlignmentLeft;
    [topView addSubview:addressLabel];
    [addressLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(timeLabel.mas_bottom).mas_offset(3);
        make.left.mas_equalTo(titleLabel);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
        make.height.mas_equalTo(16);
        make.bottom.mas_equalTo(topView).mas_offset(-15);
    }];
    
    middleView = [[UIView alloc] init];
    middleView.backgroundColor = FWColorWihtAlpha(@"969696", 0.08);
    [self.mainView addSubview:middleView];
    [middleView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topView.mas_bottom);
        make.left.right.mas_equalTo(self.mainView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 52));
    }];
    
    priceLabel = [[UILabel alloc] init];
    priceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 20];
    priceLabel.textColor = FWColor(@"ff6f00");
    priceLabel.textAlignment = NSTextAlignmentLeft;
    [middleView addSubview:priceLabel];
    [priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel);
        make.width.mas_greaterThanOrEqualTo(10);
        make.centerY.mas_equalTo(middleView);
        make.height.mas_equalTo(middleView);
    }];
    
    
    maxButton = [[UIButton alloc] init];
    [maxButton setImage:[UIImage imageNamed:@"buy_add"] forState:UIControlStateNormal];
    [maxButton addTarget:self action:@selector(maxButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [middleView addSubview:maxButton];
    [maxButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(middleView).mas_offset(-14);
        make.size.mas_equalTo(CGSizeMake(28, 25));
        make.centerY.mas_equalTo(middleView);
    }];
    
    countLabel = [[UILabel alloc] init];
    countLabel.text = @(self.ticketCount).stringValue;
    countLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    countLabel.textColor = FWColor(@"222222");
    countLabel.textAlignment = NSTextAlignmentCenter;
    [middleView addSubview:countLabel];
    [countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(maxButton.mas_left);
        make.width.mas_equalTo(32);
        make.centerY.mas_equalTo(middleView);
        make.height.mas_equalTo(middleView);
    }];
    
    minButton = [[UIButton alloc] init];
    [minButton setImage:[UIImage imageNamed:@"buy_min"] forState:UIControlStateNormal];
    [minButton addTarget:self action:@selector(minButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [middleView addSubview:minButton];
    [minButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(countLabel.mas_left).mas_offset(0);
        make.size.mas_equalTo(CGSizeMake(28, 25));
        make.centerY.mas_equalTo(middleView);
    }];
    
    descView = [[UIView alloc] init];
    [self.mainView addSubview:descView];
    [descView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(middleView.mas_bottom);
        make.left.right.mas_equalTo(self.mainView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 43));
    }];
    
    descLabel = [[UILabel alloc] init];
    descLabel.text = @"门票介绍";
    descLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size: 13];
    descLabel.textColor = FWColor(@"222222");
    descLabel.textAlignment = NSTextAlignmentLeft;
    [descView addSubview:descLabel];
    [descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(descView).mas_offset(15);
        make.width.mas_equalTo(100);
        make.centerY.mas_equalTo(descView);
        make.height.mas_equalTo(43);
    }];
}

- (void)setupBottomView{
    
    self.detailWebView = [[UIWebView alloc]  initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 1)];
    self.detailWebView.delegate = self;
    self.detailWebView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.mainView addSubview:self.detailWebView];
    
    self.bottomView = [[UIView alloc] init];
    self.bottomView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view).mas_offset(0);
        make.bottom.mas_equalTo(self.view);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(60+FWSafeBottom);
    }];
    
    self.contectButton = [[UIButton alloc] init];
    [self.contectButton addTarget:self action:@selector(contectButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:self.contectButton];
    [self.contectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(self.bottomView);
        make.size.mas_equalTo(CGSizeMake(80, 60));
    }];
    
    UIImageView * contectIV = [[UIImageView alloc] init];
    contectIV.image = [UIImage imageNamed:@"signup_connect"];
    [self.contectButton addSubview:contectIV];
    [contectIV mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(19, 22));
        make.top.mas_equalTo(self.bottomView).mas_offset(14);
        make.left.mas_equalTo(self.bottomView).mas_offset(31);
    }];
    
    UILabel * contectLabel = [[UILabel alloc] init];
    contectLabel.text = @"购票咨询";
    contectLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 10];
    contectLabel.textColor = FWColor(@"9FA3AA");
    contectLabel.textAlignment = NSTextAlignmentCenter;
    [self.contectButton addSubview:contectLabel];
    [contectLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(contectIV);
        make.top.mas_equalTo(contectIV.mas_bottom).mas_offset(3);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(15);
    }];
    
    self.signupButton = [[UIButton alloc] init];
    self.signupButton.layer.cornerRadius = 2;
    self.signupButton.layer.masksToBounds = YES;
    self.signupButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [self.signupButton addTarget:self action:@selector(signupButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:self.signupButton];
    [self.signupButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contectButton.mas_right);
        make.top.mas_equalTo(self.bottomView).mas_offset(8);
        make.right.mas_equalTo(self.bottomView).mas_offset(-14);
        make.height.mas_equalTo(44);
        make.width.mas_greaterThanOrEqualTo(100);
    }];
    
    [self.view bringSubviewToFront:naviBarView];
}

#pragma mark - > 判断是否登录
- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

@end
