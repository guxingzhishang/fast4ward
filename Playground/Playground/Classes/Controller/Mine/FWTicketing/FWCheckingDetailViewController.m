//
//  FWCheckingDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCheckingDetailViewController.h"
#import "FWCheckingDetailView.h"

@interface FWCheckingDetailViewController ()
@property (nonatomic, strong) FWCheckingDetailView * detailView;

@end

@implementation FWCheckingDetailViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"核销订单详情页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"核销订单详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"订单详情";
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.detailView = [[FWCheckingDetailView alloc] init];
    self.detailView.vc = self;
    [self.view addSubview:self.detailView];
    [self.detailView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.detailView configForView:self.checkModel];
}

@end
