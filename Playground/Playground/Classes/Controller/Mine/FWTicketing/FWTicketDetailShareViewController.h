//
//  FWTicketDetailShareViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/10.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWTicketDetailShareViewController : FWBaseViewController

@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) NSString * share_url;

@end

NS_ASSUME_NONNULL_END
