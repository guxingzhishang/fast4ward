//
//  FWCodeCheckingViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCodeCheckingViewController.h"
#import "FWCheckingDetailViewController.h"
#import "FWCheckModel.h"

@interface FWCodeCheckingViewController ()

@end

@implementation FWCodeCheckingViewController
@synthesize codeField;

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"辅助码核销页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"辅助码核销页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"辅助码核销";
    
    [self setupSubviews];
}

- (void)setupSubviews{
    
    codeField = [[IQTextView alloc] init];
    codeField.delegate = self;
    codeField.contentInset = UIEdgeInsetsMake(5, 5, 0, 0);
    codeField.font =  [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    codeField.placeholder = @"请输入入场辅助码";
    codeField.layer.cornerRadius = 5;
    codeField.layer.masksToBounds = YES;
    codeField.backgroundColor = FWColor(@"F5F5F5");
    [self.view addSubview:codeField];
    codeField.frame = CGRectMake(38, 150, SCREEN_WIDTH-76, 46);
    
    UIButton * findButton = [[UIButton alloc] init];
    findButton.layer.cornerRadius = 20.5;
    findButton.layer.masksToBounds = YES;
    [findButton addTarget:self action:@selector(findButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:findButton];
    findButton.frame = CGRectMake(56, CGRectGetMaxY(codeField.frame)+30, SCREEN_WIDTH-112, 41);

    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = findButton.bounds;
    [findButton.layer addSublayer:gradientLayer];
        gradientLayer.colors = @[(__bridge id)FWColor(@"6B95EF").CGColor,
                                 (__bridge id)FWColor(@"3E68DC").CGColor];
    
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    
    findButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
    [findButton setTitle:@"查询" forState:UIControlStateNormal];
    [findButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
}

#pragma mark - > 查询
- (void)findButtonClick{
    
    if (codeField.text.length < 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入辅助码" toController: self];
        return;
    }
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"order_code":codeField.text,
                              };
    
    [request startWithParameters:params WithAction:Get_ticket_check_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWCheckModel * checkModel = [FWCheckModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            FWCheckingDetailViewController * CDVC = [[FWCheckingDetailViewController alloc] init];
            CDVC.checkModel = checkModel;
            [self.navigationController pushViewController:CDVC animated:YES];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}
@end
