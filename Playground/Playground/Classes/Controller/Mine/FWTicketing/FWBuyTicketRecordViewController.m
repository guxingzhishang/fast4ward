//
//  FWBuyTicketRecordViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBuyTicketRecordViewController.h"
#import "FWBuyTicketRecordCell.h"
#import "FWBuyTicketRecordModel.h"
#import "FWOrderDetailViewController.h"

@interface FWBuyTicketRecordViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) FWBuyTicketRecordModel * recordModel;

@end

@implementation FWBuyTicketRecordViewController
@synthesize infoTableView;
@synthesize pageNum;

#pragma mark - > 获取订单列表
- (void)requestOrderList:(BOOL)isLoadMoredData{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if (isLoadMoredData == NO) {
        pageNum = 1;
        
        if (self.dataSource.count > 0 ) {
            [self.dataSource removeAllObjects];
        }
    }else{
        pageNum +=1;
    }
    
    NSDictionary * params  = @{
                               @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                               @"page":@(pageNum).stringValue,
                               @"page_size":@"20",
                               };
    
    [request startWithParameters:params WithAction:Get_ticket_order_list  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            self.recordModel = [FWBuyTicketRecordModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.dataSource addObjectsFromArray: self.recordModel.list];
            
            if([self.recordModel.list count] == 0 &&
               pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            if (isLoadMoredData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            NSString * errmsg = [back objectForKey:@"errmsg"];
            if ([errmsg isEqualToString:@"访问受限,请先登录"]) {
                [[FWHudManager sharedManager] showErrorMessage:@"您还未登录，请先登录才能查看关注哦~" toController:self];
                return ;
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}


- (NSMutableArray *)dataSource{
    
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"购票记录页"];
    
    [self requestOrderList:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"购票记录页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"购票记录";
    pageNum = 1;
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.estimatedRowHeight = 190;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"还没有相关的内容哦~";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    
    if (@available(iOS 11.0, *)) {
        infoTableView.estimatedSectionFooterHeight = 0;
        infoTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestOrderList:NO];
    }];
    infoTableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        [weakSelf requestOrderList:YES];
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWBuyTicketRecordCellID";
    
    FWBuyTicketRecordCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWBuyTicketRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.dataSource.count) {
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.dataSource.count) {
        
        FWBuyTicketRecordListModel * listModel = self.dataSource[indexPath.row];
        
        FWOrderDetailViewController * ODVC = [[FWOrderDetailViewController alloc] init];
        ODVC.order_id = listModel.order_id;
        [self.navigationController pushViewController:ODVC animated:YES];
    }
}
@end
