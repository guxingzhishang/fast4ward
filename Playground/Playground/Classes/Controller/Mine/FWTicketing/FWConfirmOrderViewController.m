//
//  FWConfirmOrderViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWConfirmOrderViewController.h"
#import "FWConfirmOrderView.h"

@interface FWConfirmOrderViewController ()
@property (nonatomic, strong) FWConfirmOrderView * orderView;

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UILabel * priceDetailLabel;
@property (nonatomic, strong) UIButton * payButton;
@property (nonatomic, strong) FWWechatOrderModel * wechatModel;
@end

@implementation FWConfirmOrderViewController

#pragma mark - > 生命周期
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    
    [self trackPageBegin:@"确认订单页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
    [self trackPageEnd:@"确认订单页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"确认订单";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySucceed) name:FWWXReturnSussessPayNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payFailed) name:FWWXReturnFailedPayNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selfPayFailed) name:FWSelfControlFailedPayNotification object:nil];

    [self setupSubviews];
}

- (void)setupSubviews{
    
    self.orderView = [[FWConfirmOrderView alloc] init];
    [self.view addSubview:self.orderView];
    [self.orderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    self.orderView.countLabel.text = [NSString stringWithFormat:@"数量：%@",self.ticketCount];

    [self.orderView configForView:self.detailModel];
    
    self.bottomView = [[UIView alloc] init];
    self.bottomView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.bottomView.layer.shadowOffset = CGSizeMake(0, -3);
    self.bottomView.layer.shadowOpacity = 0.4;
    self.bottomView.layer.shadowColor = FWTextColor_D9E2E9.CGColor;
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 60+FWSafeBottom));
        make.left.right.bottom.mas_equalTo(self.view);
    }];
    
    self.payButton = [[UIButton alloc] init];
    self.payButton.layer.cornerRadius = 2;
    self.payButton.layer.masksToBounds = YES;
    self.payButton.backgroundColor = FWTextColor_222222;
    self.payButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
    [self.payButton setTitle:@"立即支付" forState:UIControlStateNormal];
    [self.payButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.payButton addTarget:self action:@selector(payButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:self.payButton];
    [self.payButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bottomView).mas_offset(8);
        make.right.mas_equalTo(self.bottomView).mas_offset(-14);
        make.height.mas_equalTo(44);
        make.width.mas_equalTo(183);
    }];
    
    
    self.priceLabel =  [[UILabel alloc] init];
    self.priceLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 20];
    self.priceLabel.textColor = FWColor(@"ff6f00");
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    [self.bottomView addSubview:self.priceLabel];
    [self.priceLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.payButton);
        make.left.mas_equalTo(self.bottomView).mas_offset(25);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_equalTo(24);
    }];
    
    self.priceDetailLabel =  [[UILabel alloc] init];
    self.priceDetailLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 13];
    self.priceDetailLabel.textColor = FWColor(@"ff6f00");
    self.priceDetailLabel.textAlignment = NSTextAlignmentLeft;
    [self.bottomView addSubview:self.priceDetailLabel];
    [self.priceDetailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.priceLabel).mas_offset(-2);
        make.left.mas_equalTo(self.priceLabel.mas_right).mas_offset(8);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(17);
    }];

    self.priceLabel.text = [NSString stringWithFormat:@"￥%@",self.pay_fee];
    self.priceDetailLabel.text = @"总票价";
}

#pragma mark - > 立即购买
- (void)payButtonOnClick{
    
    if ([self.detailModel.if_real_name isEqualToString:@"1"] && self.orderView.nameTextView.text.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写真实姓名" toController:self];
        return;
    }
    
    if ([self.detailModel.if_mobile isEqualToString:@"1"]){
        if(self.orderView.phoneTextView.text.length <= 0) {
            [[FWHudManager sharedManager] showErrorMessage:@"请输入手机号码" toController:self];
            return;
        }else if(self.orderView.phoneTextView.text.length !=11){
            [[FWHudManager sharedManager] showErrorMessage:@"请输入有效手机号码" toController:self];
            return;
        }
    }
    
    if ([self.detailModel.if_address isEqualToString:@"1"] && self.orderView.threeTextView.text.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请填写收货地址" toController:self];
        return;
    }
    
    if ([self.detailModel.if_sex isEqualToString:@"1"] && [self.orderView.sexType isEqualToString:@"0"]) {
          [[FWHudManager sharedManager] showErrorMessage:@"请选择性别" toController:self];
          return;
    }
    
    if (self.order_id.length >0) {
        /* 如果有订单号，直接调起支付，不需要在请求了 */
        [self submitOrderPay:self.order_id];
        return;
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if (!self.orderView.threeTextView.text || self.orderView.threeLabel.text.length <= 0) {
        self.orderView.threeLabel.text = @"";
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"ticket_id":self.ticket_id,
                              @"buy_number":self.ticketCount,
                              @"mobile":self.orderView.phoneTextView.text,
                              @"real_name":self.orderView.nameTextView.text,
                              @"sex":self.orderView.sexType,
                              @"address":self.orderView.threeTextView.text,
                              @"car":self.orderView.haveCarType,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Create_ticket_order WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"wechat" forKey:@"支付返回"];

            self.order_id = [[back objectForKey:@"data"] objectForKey:@"order_id"];
            [self submitOrderPay:self.order_id];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 调起微信支付
- (void)payrequestWithModel:(FWWechatOrderModel *)model {

    NSString * timeStampStr = model.timestamp;
    UInt32 timesta = (UInt32)[timeStampStr intValue];

    PayReq *request = [[PayReq alloc] init];
    request.sign      = model.sign;
    request.package   = @"Sign=WXPay";
    request.nonceStr  = model.noncestr;
    request.prepayId  = model.prepayid;
    request.partnerId = model.partnerid;
    request.timeStamp = timesta;

    /* 调起支付 */
    if ([WXApi sendReq:request]) {

    } else {

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"支付失败" message:@"未安装微信客户端,请使用其他支付方式" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - > 微信pay成功
- (void)paySucceed{

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"订单号"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"支付返回"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付成功" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 微信pay失败
- (void)payFailed{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付取消" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 自行操作返回pay失败
- (void)selfPayFailed{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"支付失败，请重试" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - > 提交支付(微信/支付宝)
- (void)submitOrderPay:(NSString *)order_id{

    FWMemberRequest * request = [[FWMemberRequest alloc] init];

    if (order_id.length < 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"订单号有误，请重新购买" toController:self];
        return;
    }
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"order_id":order_id,
                              @"pay_service":@"weixin",
                              };

    [request startWithParameters:params WithAction:Submit_ticket_pay WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);

        if ([code isEqualToString:NetRespondCodeSuccess]) {

            /* 微信支付 */
            NSDictionary * result = [[back objectForKey:@"data"] objectForKey:@"result_weixin"];
            NSDictionary * pay_order_id = [[back objectForKey:@"data"] objectForKey:@"pay_order_id"];

            self.wechatModel = [FWWechatOrderModel mj_objectWithKeyValues:result];
            [self payrequestWithModel:self.wechatModel];
            
            [[NSUserDefaults standardUserDefaults] setObject:pay_order_id forKey:@"订单号"];
            
//            if ([type isEqualToString:@"weixin"]) {
//            }else if ([type isEqualToString:@"alipay"]){
//                /* 支付宝支付 */
//                //                [[NSUserDefaults standardUserDefaults] setObject:[[back objectForKey:@"data"] objectForKey:@"pay_order_id"] forKey:@"订单号"];
//                //                [self alipayRequestWithResult:[[back objectForKey:@"data"] objectForKey:@"result"]];
//            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

@end
