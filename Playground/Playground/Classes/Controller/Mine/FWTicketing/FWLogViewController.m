//
//  FWLogViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWLogViewController.h"
#import "FWLogCell.h"
#import "FWLogModel.h"

@interface FWLogViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, strong) FWLogModel * logModel;

@end

@implementation FWLogViewController
@synthesize infoTableView;
@synthesize logModel;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = @[].mutableCopy;
    }
    
    return _dataSource;
}

- (void)requestLogList:(BOOL)isRequestMoreData{
    
    if(isRequestMoreData){
        self.pageNum += 1;
    }else{
        self.pageNum = 1;
        if (self.dataSource.count) {
            [self.dataSource removeAllObjects];
        }
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_ticket_check_log_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            
            if (isRequestMoreData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            self.logModel = [FWLogModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.dataSource addObjectsFromArray: self.logModel.list];
            
            if([self.logModel.list count] == 0 &&
               self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            if (isRequestMoreData) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"日志页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"日志页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"日志";
    
    [self setupSubViews];
    [self requestLogList:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = UITableViewAutomaticDimension;
    infoTableView.estimatedRowHeight = 100;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestLogList:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestLogList:YES];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWLogCellID";
    
    FWLogCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWLogCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row < self.dataSource.count) {
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

@end
