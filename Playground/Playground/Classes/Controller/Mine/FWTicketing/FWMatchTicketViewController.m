//
//  FWMatchTicketViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchTicketViewController.h"
#import "FWMatchTicketCell.h"
#import "FWMatchTicketHeaderView.h"
#import "FWBuyTicketRecordViewController.h"
#import "FWTicketDetailViewController.h"
#import "FWMatchTicketsModel.h"

@interface FWMatchTicketViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) FWTableView * infoTableView;
@property (nonatomic, strong) FWMatchTicketHeaderView * headerView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger  pageNum;
@property (nonatomic, strong) FWMatchTicketsModel * ticketsModel;

@end

@implementation FWMatchTicketViewController
@synthesize infoTableView;

- (NSMutableArray *)dataSource{
 
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (void)requesetData:(BOOL)isLoadMore{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    if (isLoadMore) {
        
        self.pageNum +=1;
    }else{
        self.pageNum = 1;
        
        if (self.dataSource.count) {
            [self.dataSource removeAllObjects];
        }
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"10",
                              };
    
    [request startWithParameters:params WithAction:Get_ticket_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.ticketsModel = [FWMatchTicketsModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (isLoadMore) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                self.infoTableView.mj_footer.hidden = NO;
                [infoTableView.mj_header endRefreshing];
            }
            
            if (self.ticketsModel.list_ticket.count > 0) {
                [self.headerView configForView:self.ticketsModel.banner];
            }
            [self.dataSource addObjectsFromArray: self.ticketsModel.list_ticket];
            
            if([self.ticketsModel.list_ticket count] == 0 &&
               self.pageNum != 1){
                self.infoTableView.mj_footer.hidden = YES;
                
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [infoTableView reloadData];
            }
        }else{
            
            if (isLoadMore) {
                [infoTableView.mj_footer endRefreshing];
            }else{
                [infoTableView.mj_header endRefreshing];
            }
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"赛事门票页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"赛事门票页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"赛事门票";
    
    
    self.pageNum = 1;
    
    UIButton * rightButton = [[UIButton alloc] init];
    
    rightButton.frame = CGRectMake(0, 0, 76, 30);
    [rightButton setImage:[UIImage imageNamed:@"buy_record"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    
    [self setupSubViews];
    
    [self requesetData:NO];
}

- (void)setupSubViews{
    
    infoTableView = [[FWTableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 158;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    infoTableView.isNeedEmptyPlaceHolder = YES;
    NSString * title = @"近期无赛事活动";
    NSDictionary * Attributes = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                  NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
    infoTableView.emptyDescriptionString = attributeString;
    infoTableView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_FFFFFF;
    
    DHWeakSelf;
    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requesetData:NO];
    }];
    infoTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requesetData:YES];
    }];
    
    self.headerView = [[FWMatchTicketHeaderView alloc] init];
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 146*SCREEN_WIDTH/375+20);
    infoTableView.tableHeaderView = self.headerView;
    
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
    infoTableView.tableFooterView = footView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWMatchTicketCellID";
    
    FWMatchTicketCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWMatchTicketCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if (indexPath.row < self.dataSource.count) {
        [cell configForCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row < self.dataSource.count) {
        FWTicketDetailViewController * TDVC = [[FWTicketDetailViewController alloc] init];
        TDVC.ticket_id = ((FWMatchTicketsListModel *)self.dataSource[indexPath.row]).ticket_id;
        [self.navigationController pushViewController:TDVC animated:YES];
    }
}

#pragma mark - > 购票记录
- (void)rightButtonClick{
 
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        FWBuyTicketRecordViewController * TRVC = [[FWBuyTicketRecordViewController alloc] init];
        [self.navigationController pushViewController:TRVC animated:YES];
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

@end
