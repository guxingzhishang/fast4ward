//
//  FWGetVideoPackageViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 获取套餐
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGetVideoPackageViewController : FWBaseViewController

@property (nonatomic, strong) FWMemberListModel * memberListModel;
@property (nonatomic, strong) FWMineInfoModel * infoModel;

@end

NS_ASSUME_NONNULL_END
