//
//  FWInformationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 我的 - > 设置 - > 修改手机号 - > 完善个人信息
 * 登录 - > 手机号 - > 验证码 - > 个人信息
 */
#import "FWBaseViewController.h"

@interface FWInformationViewController : FWBaseViewController<UIGestureRecognizerDelegate,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

/**
 * 标题文案
 */
@property (nonatomic, strong) UILabel * titleLabel;

/**
 * 头像
 */
@property (nonatomic, strong) UIImageView * photoImageView;

/**
 * 满足条件图标
 */
@property (nonatomic, strong) UIImageView * rightImageView;

/**
 * 姓名输入框
 */
@property (nonatomic, strong) UITextField * nameTextField;

/**
 * 提醒文案
 */
@property (nonatomic, strong) UILabel * tipLabel;

/**
 * 男性按钮
 */
@property (nonatomic, strong) UIButton * maleButton;

/**
 * 女性按钮
 */
@property (nonatomic, strong) UIButton * femaleButton;

/**
 * 完成按钮
 */
@property (nonatomic, strong) UIButton * completeButton;

/**
 * 照片选择器（照相 | 相册）
 */
@property (nonatomic, strong) UIImagePickerController * imagePicker;

/**
 * 上传的图片
 */
@property (nonatomic, strong) UIImage *uploadImage;

/**
 * 是否跳转到下一页
 */
@property (nonatomic, assign) BOOL  isNext;

/* 1、正常登录   2、游客模式下的登录（成功后要返回登录之前停留页面）*/
@property (nonatomic, assign) NSInteger  fromLogin;
@end
