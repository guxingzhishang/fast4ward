//
//  FWCarDetailShareViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/19.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCarDetailShareViewController : FWBaseViewController

@property (nonatomic, strong) FWCarListModel * listModel;


@end

NS_ASSUME_NONNULL_END
