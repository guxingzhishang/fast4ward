//
//  FWMyVIPViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMyVIPViewController.h"
#import "FWMyVIPView.h"
#import "FWRegistrationHomeViewController.h"

@interface FWMyVIPViewController ()

@property (nonatomic, strong) FWMyVIPView * vipView;

@end

@implementation FWMyVIPViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"huiyuanzhongxin页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"huiyuanzhongxin页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"会员中心";
    self.fd_interactivePopDisabled = YES;
    self.fd_prefersNavigationBarHidden = YES;

    self.vipView = [[FWMyVIPView alloc] init];
    self.vipView.vc = self;
    [self.view addSubview:self.vipView];
    [self.vipView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.vipView configViewWithModel:self.infoModel];
}

- (void)backBtnClick{
    
    for (UIViewController * item in self.navigationController.viewControllers) {
        if ([item isKindOfClass:[FWRegistrationHomeViewController class]]) {
            [self.navigationController popToViewController:item animated:YES];
            return ;
        }
        
        if ([item isKindOfClass:[FWNewMineViewController class]]) {
            [self.navigationController popToViewController:item animated:YES];
            return ;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
