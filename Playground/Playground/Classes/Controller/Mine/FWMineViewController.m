//
//  FWMineViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMineViewController.h"
#import "FWFeedBackViewController.h"
#import "FWSettingViewController.h"
#import "FWMineRequest.h"
#import "FWMineModel.h"
#import "ShareView.h"
#import "ShareManager.h"
#import "FWPublishPreviewViewController.h"
#import "FWPublishPreviewGraphViewController.h"
#import "FWGraphTextDetailViewController.h"
#import "FWMineCell.h"
#import "FWSettingModel.h"
#import "FWFavouriteViewController.h"
#import "FWCertificationViewController.h"
#import "FWVisitorViewController.h"
#import "FWMyVIPViewController.h"
#import "FWMineView.h"
#import "FWBussinessHomePageViewController.h"
#import "FWCarCertificationViewController.h"
#import "FWConversationViewController.h"
#import "FWDraftViewController.h"
#import "FWCarPlayerCenterViewController.h"
#import "FWTicketCheckingViewController.h"
#import "FWCheckAndSignViewController.h"

static NSString * ticketCheck = @"门票核销";
static NSString * signCheck = @"签到/检录";

@interface FWMineViewController ()<ShareViewDelegate,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImageView * naviBarView;
    UILabel * titleLable;
    UIButton * messageButton;
    UILabel * unReadLabel;

    FWMineView * mineView;
    UITableView * infoTableView;
    
    FWMineModel * mineModel;
    
    CGFloat alphaValue;
}

@property (nonatomic, strong) FWSettingModel * settingModel;

@property (nonatomic, strong) NSMutableArray * titleArray;
@property (nonatomic, strong) NSMutableArray * imageArray;
@property (nonatomic, assign) BOOL isTicket;
@property (nonatomic, assign) BOOL isSign;
      
@end

@implementation FWMineViewController
@synthesize shareButton;
@synthesize settingButton;
@synthesize backButton;
@synthesize settingModel;

#pragma mark - > 请求融云iM
- (void)requestRongYunTokenIM{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params;
    if ([[GFStaticData getObjectForKey:kTagUserKeyID] length] > 0) {
        params = @{@"uid":[[GFStaticData getObjectForKey:kTagUserKeyID] description]};
    }else{
        params = @{@"uid":@"0"};
    }
    
    [request startWithParameters:params WithAction:Get_im_token WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[RCIM sharedRCIM] connectWithToken:[back objectForKey:@"token"]
                                        success:^(NSString *userId) {
                                            RCUserInfo *userInfo = [[RCUserInfo alloc] initWithUserId:[GFStaticData getObjectForKey:kTagUserKeyID] name:[GFStaticData getObjectForKey:kTagUserKeyName] portrait:[GFStaticData getObjectForKey:kTagUserKeyImageUrl]];
                                            [[RCIM sharedRCIM] refreshUserInfoCache:userInfo withUserId:userId];
                                            [RCIM sharedRCIM].currentUserInfo = userInfo;
                                        } error:^(RCConnectErrorCode status) {
                                            NSLog(@"IM登陆的错误码为:%zd", status);
                                        } tokenIncorrect:^{
                                            NSLog(@"IMtoken错误");
                                        }];
        }
    }];
}

#pragma mark - > 请求基本配置参数
- (void)requestSetting{
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_settings  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            settingModel = [FWSettingModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [GFStaticData saveObject:self.settingModel.kefu_uid forKey:kKefuUID];
            [GFStaticData saveObject:self.settingModel.kefu_nickname forKey:kKefuNickname];
            
            [infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求个人数据
- (void)requsetMineData{
    
    FWMineRequest * request = [[FWMineRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_user_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        [infoTableView.mj_header endRefreshing];

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [GFStaticData saveLoginData:[[back objectForKey:@"data"] objectForKey:@"user_info"]];
           
            [[FWUserCenter sharedManager] clearAllPreviousUserInfo];
            [[FWUserCenter sharedManager] saveUserInfoToDiskWithJson:back];
            [FWUserCenter sharedManager].user_IsLogin = YES;
            
            mineModel = [FWMineModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [mineView configForView:mineModel];
            
            self.titleArray = @[ticketCheck,signCheck,@"我的座驾",@"我的草稿",@"我的收藏",@"车手中心",@"最近访客",@"账号认证",@"会员中心",@"商家联盟",@"联系管理员",@"设置"].mutableCopy;
            self.imageArray = @[@"mine_ticket_check",@"mine_sign_check",@"mine_car",@"mine_draft",@"mine_favourite_new",@"mine_carplayercenter",@"mine_visitor",@"ming_certificate",@"mine_member",@"mine_bussiness_homepage",@"mine_kefu",@"mine_settingnew"].mutableCopy;
            
            if(![mineModel.ticket_check isEqualToString:@"1"]){
                /* 门票核销 */
                self.isTicket = NO;
                NSInteger index = [self.titleArray indexOfObject:ticketCheck];
                
                [self.titleArray removeObjectAtIndex:index];
                [self.imageArray removeObjectAtIndex:index];
            }else{
                self.isTicket = YES;
            }

            if([mineModel.jianlu_check isEqualToString:@"1"]||
               [mineModel.qiandao_check isEqualToString:@"1"]){
                self.isSign = YES;
            }else{
                /* 签到/检录 */
                self.isSign = NO;
                NSInteger index = [self.titleArray indexOfObject:signCheck];
                
                [self.titleArray removeObjectAtIndex:index];
                [self.imageArray removeObjectAtIndex:index];
            }
            
            if (self.unreandCount > 0) {
                mineView.unReadLabel.hidden = NO;
                mineView.unReadLabel.text = @(self.unreandCount).stringValue;
            }else{
                mineView.unReadLabel.hidden = YES;
                mineView.unReadLabel.text = @"";
            }
            
            mineView.frame = CGRectMake(0, 0, SCREEN_WIDTH, mineView.currentHeight);
            infoTableView.tableHeaderView = mineView;
            
            [self requestSetting];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"个人中心页"];
    
    [self requestRongYunTokenIM];
    [self requsetMineData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"个人中心页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_FFFFFF;
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PushToPublishViewController) name:kTagPushPublish object:nil];

    alphaValue = 0;
    
    self.fd_prefersNavigationBarHidden = YES;

    [self setupSubViews];
}

#pragma mark - > 初始化视图
- (void)setupSubViews{

    naviBarView = [[UIImageView alloc] init];
    naviBarView.userInteractionEnabled = YES;
    naviBarView.image = [UIImage imageNamed:@"user_bg"];
    [self.view addSubview:naviBarView];
    naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    naviBarView.alpha = 0;

    messageButton = [[UIButton alloc] init];
    [messageButton setImage:[UIImage imageNamed:@"mine_message"] forState:UIControlStateNormal];
    [messageButton addTarget:self action:@selector(messageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [naviBarView addSubview:messageButton];
    [messageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.right.mas_equalTo(naviBarView).mas_offset(-10);
        make.top.mas_equalTo(naviBarView).mas_offset(20+FWCustomeSafeTop);
    }];
    
    titleLable = [[UILabel alloc] init];
    titleLable.text = @"个人中心";
    titleLable.font = DHSystemFontOfSize_18;
    titleLable.textColor = FWViewBackgroundColor_FFFFFF;
    titleLable.textAlignment = NSTextAlignmentCenter;
    [naviBarView addSubview:titleLable];
    [titleLable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(naviBarView);
        make.centerY.mas_equalTo(messageButton);
        make.height.mas_equalTo(35);
    }];

    
    unReadLabel = [[UILabel alloc] init];
    unReadLabel.layer.cornerRadius = 8;
    unReadLabel.layer.masksToBounds = YES;
    unReadLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
    unReadLabel.textColor = FWViewBackgroundColor_FFFFFF;
    unReadLabel.textAlignment = NSTextAlignmentCenter;
    unReadLabel.backgroundColor = DHRedColorff6f00;
    [messageButton addSubview:unReadLabel];
    [unReadLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(messageButton).mas_offset(-8);
        make.height.mas_equalTo(16);
        make.width.mas_greaterThanOrEqualTo(16);
        make.right.mas_equalTo(messageButton).mas_offset(-3);
    }];
    unReadLabel.hidden = YES;
    
    mineView = [[FWMineView alloc] init];
    mineView.viewcontroller = self;
    mineView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 320);
    mineView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [mineView initClickMethodWithController:self];
 
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.showsVerticalScrollIndicator = NO;
    infoTableView.rowHeight = 58;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-49-FWSafeBottom);
    }];
    
    infoTableView.tableHeaderView = mineView;
    
    UIView * footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
    infoTableView.tableFooterView = footView;

    infoTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requsetMineData];
    }];
    
    [self.view bringSubviewToFront:naviBarView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"mineCellID";
    
    FWMineCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWMineCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }

    cell.vc = self;
    cell.nameLabel.text = self.titleArray[indexPath.row];
    cell.photoImageView.image = [UIImage imageNamed:self.imageArray[indexPath.row]];
    cell.signLabel.text = @"";
    cell.signLabel.hidden = YES;

    if ( [mineModel.car_notice_status isEqualToString:@"1"]) {
        
        NSInteger index = [self.titleArray indexOfObject:@"我的座驾"];
        
        if (indexPath.row == index) {
            NSString * text = @"添加座驾上首页 • ";
            NSMutableAttributedString * attentionString = [[NSMutableAttributedString alloc] initWithString:text];
            
            NSDictionary *attetionAttributes = @{
                                                 NSForegroundColorAttributeName:FWTextColor_9EA3AB,
                                                 };
            [attentionString setAttributes:attetionAttributes range:NSMakeRange(0, [text length])];
            
            attetionAttributes = @{
                                   NSForegroundColorAttributeName:FWColor(@"CC3333"),
                                   };
            [attentionString setAttributes:attetionAttributes range:NSMakeRange(8, 1)];
            
            cell.signLabel.attributedText = attentionString;
            cell.signLabel.hidden = NO;
        }
    }
    
    [cell.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell.contentView);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.left.mas_equalTo(cell.contentView).mas_offset(24);
    }];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger selectIndex = 0;
    
    if (self.isTicket && self.isSign) {
        /* 有门票核销 、 签到/检录 */
        if (indexPath.row == selectIndex) {
            [self.navigationController pushViewController:[FWTicketCheckingViewController new] animated:YES];
        }else if (indexPath.row == selectIndex+1) {
            [self.navigationController pushViewController:[FWCheckAndSignViewController new] animated:YES];
        }
    }else if (self.isTicket && !self.isSign){
        /* 只有门票核销 */
        if (indexPath.row == selectIndex) {
           [self.navigationController pushViewController:[FWTicketCheckingViewController new] animated:YES];
        }
        
        selectIndex = -1;
    }else if (!self.isTicket && self.isSign){
        /* 只有签到/检录 */
        if (indexPath.row == selectIndex) {
            [self.navigationController pushViewController:[FWCheckAndSignViewController new] animated:YES];
        }
        
        selectIndex = -1;
    }else if (!self.isTicket && !self.isSign){
        /* 没有门票核销 、 没有签到/检录 */
        selectIndex = -2;
    }
    
    if (indexPath.row == selectIndex+2) {
        [self.navigationController pushViewController:[FWCarCertificationViewController new] animated:YES];
    }else if (indexPath.row == selectIndex+3) {
        /* 草稿 */
        [self.navigationController pushViewController:[FWDraftViewController new] animated:YES];
    }else if (indexPath.row == selectIndex+4) {
        [self.navigationController pushViewController:[FWFavouriteViewController new] animated:YES];
    }else if (indexPath.row == selectIndex+5) {
        [self.navigationController pushViewController:[FWCarPlayerCenterViewController new] animated:YES];
    }else if (indexPath.row == selectIndex+6) {
        [self.navigationController pushViewController:[FWVisitorViewController new] animated:YES];
    }else if (indexPath.row == selectIndex+7) {
        [self.navigationController pushViewController:[FWCertificationViewController new] animated:YES];
    }else if (indexPath.row == selectIndex+8) {
        if ([mineModel.user_info.user_level isEqualToString:@"0"]) {
            /* 成为会员 */
            FWVIPWebViewController * vipVC = [FWVIPWebViewController new];
            vipVC.webTitle = @"肆放会员";
            vipVC.infoModel = mineModel.user_info;
            [self.navigationController pushViewController:vipVC animated:YES];
        }else{
            /* 会员中心 */
            FWMyVIPViewController * myVipVC = [FWMyVIPViewController new];
            myVipVC.infoModel = mineModel.user_info;
            [self.navigationController pushViewController:myVipVC animated:YES];
        }
    }else if (indexPath.row == selectIndex+9) {
        if ([mineModel.user_info.cert_status isEqualToString:@"2"]) {
            /* 已认证，跳转至商家联盟*/
            FWBussinessHomePageViewController * BHPVC = [[FWBussinessHomePageViewController alloc] init];
            BHPVC.mineModel = mineModel;
            [self.navigationController pushViewController:BHPVC animated:YES];
        }else{
            /* 未认证  H5页  */
            FWWebViewController * WVC = [[FWWebViewController alloc] init];
            WVC.pageType = WebViewTypeURL;
            WVC.htmlStr = mineModel.h5_cert;
            [self.navigationController pushViewController:WVC animated:YES];
        }
    }else if (indexPath.row == selectIndex+10) {
        if ([GFStaticData getObjectForKey:kKefuUID]) {
            FWConversationViewController * CVC = [[FWConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:[GFStaticData getObjectForKey:kKefuUID]];
            [self.navigationController pushViewController:CVC animated:YES];
        }
    }else if (indexPath.row == selectIndex+11) {
        FWSettingViewController * settingVC = [[FWSettingViewController alloc] init];
        settingVC.settingModel = settingModel;
        [self.navigationController pushViewController:settingVC animated:YES];
    }
}

#pragma mark - > 消息
- (void)messageButtonClick{
    
    FWMessageViewController *  MVC = [[FWMessageViewController alloc] init];
    [self.navigationController pushViewController:MVC animated:YES];
}

#pragma mark ------- scrollowView滑动结束后调用的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView == infoTableView) {
        
        if (scrollView.contentOffset.y > 30){
            alphaValue = 1;
        }else{
            alphaValue = (scrollView.contentOffset.y/30);
        }
        
        titleLable.alpha = alphaValue;
        naviBarView.alpha = alphaValue;
        messageButton.alpha = alphaValue;
        
        if (scrollView.contentOffset.y >0) {
            titleLable.hidden = NO;
            
            if (self.unreandCount > 0) {
                unReadLabel.hidden = NO;
                unReadLabel.text = @(self.unreandCount).stringValue;
            }else{
                unReadLabel.hidden = YES;
                unReadLabel.text = @"";
            }
            messageButton.hidden = NO;
            mineView.messageButton.hidden = YES;
        }else{
            titleLable.hidden = YES;
            
            messageButton.hidden = YES;
            mineView.messageButton.hidden = NO;
        }
    }
}

#pragma mark - > 跳转到发布页
- (void)PushToPublishViewController{
    
    if (![((UINavigationController *)self.tabBarController.selectedViewController).topViewController isEqual:self]
        ) {
        return;
    }
    
    if ([[GFStaticData getObjectForKey:upLoadStatus] isEqualToString:upLoadStatusUploading]) {
        NSDictionary * params = [GFStaticData getObjectForKey:upLoadStatusUploading];
        
        if ([params[@"is_draft"] isEqualToString:@"1"]) {
            // 草稿
            [[FWHudManager sharedManager] showErrorMessage:@"你有草稿正在保存，请保存完成后再操作" toController:self];
        }else{
            // 发布
            [[FWHudManager sharedManager] showErrorMessage:@"你有正在上传的内容，请发布完成后再操作" toController:self];
        }
    }else {
        [GFStaticData saveObject:@"home" forKey:Frome_ViewController];
        
        FWPublishViewController * vc = [[FWPublishViewController alloc] init];
        vc.screenshotImage = [UIImage imageNamed:@"publish_bg"];
        vc.fd_prefersNavigationBarHidden = YES;
        vc.navigationController.navigationBar.hidden = YES;
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.4];
        [animation setType: kCATransitionMoveIn];
        [animation setSubtype: kCATransitionFromTop];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        [self.navigationController pushViewController:vc animated:NO];
        [self.navigationController.view.layer addAnimation:animation forKey:nil];
    }
}


@end
