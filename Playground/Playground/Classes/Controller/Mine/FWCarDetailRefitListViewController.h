//
//  FWCarDetailRefitListViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/7.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

/**
 * 座驾详情 改装清单
 */
#import "FWBaseViewController.h"
#import "FWRefitListView.h"
#import "FWCarListModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^refitListShowBlock)(NSDictionary * showDict);

@interface FWCarDetailRefitListViewController : FWBaseViewController

@property (nonatomic, strong) FWRefitListView * listView;
@property (nonatomic, strong) FWCarListModel * listModel;
@property (nonatomic, strong) FWRefitCategaryModel * categaryModel;

@property (nonatomic, copy) refitListShowBlock showBlock;

@end

NS_ASSUME_NONNULL_END
