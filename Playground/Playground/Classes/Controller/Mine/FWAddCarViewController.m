//
//  FWAddCarViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/15.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWAddCarViewController.h"
#import "FWAddCarView.h"

@interface FWAddCarViewController ()<UIScrollViewDelegate,FWAddCarViewDelegate>
@property (nonatomic, strong) FWAddCarView * addCarView;
@property (nonatomic, strong) NSDictionary * infoDict;
@end

@implementation FWAddCarViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([self.type isEqualToString:@"add"]) {
        [self trackPageBegin:@"添加座驾"];
    }else{
        [self trackPageBegin:@"编辑座驾"];
    }
    
    if (self.addCarView.isSelectCar) {
        self.addCarView.isSelectCar = NO;
        /* 解决，从子页面返回后，向上弹一下的bug */
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [IQKeyboardManager sharedManager].enable = YES;
        });
    }else{
        [IQKeyboardManager sharedManager].enable = YES;
    }
    
    if (self.subListModel.style.length > 0) {
        self.addCarView.selectLabel.text = self.subListModel.style;
        self.addCarView.car_style_id = self.subListModel.car_style_id;
        
        self.addCarView.selectLabel.textColor = FWTextColor_222222;
    }else{
        self.addCarView.selectLabel.textColor = FWTextColor_BCBCBC;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if ([self.type isEqualToString:@"add"]) {
        [self trackPageEnd:@"添加座驾"];
    }else{
        [self trackPageEnd:@"编辑座驾"];
    }
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.infoDict = @{};
    
    if ([self.type isEqualToString:@"add"]) {
        self.title = @"添加座驾";
    }else{
        self.title = @"编辑座驾";
    }
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    self.addCarView = [[FWAddCarView alloc] init];
    self.addCarView.vc = self;
    self.addCarView.delegate = self;
    self.addCarView.viewDelegate = self;
    [self.view addSubview:self.addCarView];
    [self.addCarView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    if ([self.type isEqualToString:@"edit"]) {
        [self.addCarView configForView:self.listModel];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.infoDict = [self.addCarView getParams];
        });
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = self.addCarView.contentOffset;
    if (offset.y <= 0) {
        offset.y = 0;
    }
    self.addCarView.contentOffset = offset;
}

- (void)backBtnClick{
    
    if ([self.type isEqualToString:@"edit"]) {
        
        if ([[self.addCarView getParams] isEqual:self.infoDict]) {
            if (self.isReFresh != nil) {
                self.isReFresh(NO);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:@"是否保存本次编辑结果？" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"暂不" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                if (self.isReFresh != nil) {
                    self.isReFresh(NO);
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.addCarView saveButtonClick];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }else{
        if (self.isReFresh != nil) {
            self.isReFresh(NO);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - > 保存成功
- (void)saveFinished{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (self.isReFresh != nil) {
            self.isReFresh(YES);
        }

        [self.navigationController popViewControllerAnimated:YES];
    });
}
@end
