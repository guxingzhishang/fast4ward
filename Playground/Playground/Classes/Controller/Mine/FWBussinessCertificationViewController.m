//
//  FWBussinessCertificationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessCertificationViewController.h"
#import "FWBussinessCertificationView.h"

@interface FWBussinessCertificationViewController ()

@property (nonatomic, strong) FWBussinessCertificationView * cerView;

@end

@implementation FWBussinessCertificationViewController

#pragma mark - > 请求主营业务列表
-(void)requestBussinessData{
    
    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID]
                              };
    
    [request startWithParameters:params WithAction:Get_main_business_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            FWBussinessCertificationModel * model = [FWBussinessCertificationModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.cerView configViewForModel:model];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"商家认证"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"商家认证"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"商家认证";
    
    [self setupSubviews];
    
    [self requestBussinessData];
}

#pragma mark - > 初始化视图
- (void)setupSubviews{
    
    self.cerView = [[FWBussinessCertificationView alloc] init];
    self.cerView.viewcontroller = self;
    [self.view addSubview:self.cerView];
    [self.cerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}



@end
