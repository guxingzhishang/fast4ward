//
//  FWCertificationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 认证选择页面
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCertificationViewController : FWBaseViewController



@end

NS_ASSUME_NONNULL_END
