//
//  FWManageCarViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

static NSString * const manageCollectionID = @"manageCollectionID";

#import "FWManageCarViewController.h"
#import "FWManageCarCell.h"

@interface FWManageCarViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong)  FWCollectionView * manageCollectionView;
@property (nonatomic, assign)  NSInteger managePageNum;
@property (nonatomic, strong)  NSMutableArray * manageDataSource;
@property (nonatomic, strong)  NSMutableArray * selectArray;
@property (nonatomic, strong)  FWCarListModel * manageModel;
@property (nonatomic, strong)  UIBarButtonItem * rightBtnItem;

@end

@implementation FWManageCarViewController

#pragma mark - > 请求图片
- (void)requestCarImgs{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"user_car_id":self.user_car_id,
                              };
    
    [request startWithParameters:params WithAction:Get_car_info_by_id WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            self.manageModel = [FWCarListModel mj_objectWithKeyValues:[[back objectForKey:@"data"] objectForKey:@"car_info"]];
            
            if (self.manageDataSource.count) {
                [self.manageDataSource removeAllObjects];
            }
            
            [self.manageDataSource addObjectsFromArray:self.manageModel.car_imgs];
            [self.manageCollectionView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"管理图片"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"管理图片"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"管理图片";
    
    self.managePageNum = 0;
    
    self.manageDataSource = @[].mutableCopy;
    self.selectArray = @[].mutableCopy;
    
    UIButton * deleteButton = [[UIButton alloc] init];
    deleteButton.frame = CGRectMake(0, 0, 45, 40);
    deleteButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    deleteButton.enabled = NO;
    [deleteButton setTitleColor:FWColor(@"9ea3ab") forState:UIControlStateNormal];
    [deleteButton addTarget:self action:@selector(deleteButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.rightBtnItem= [[UIBarButtonItem alloc] initWithCustomView:deleteButton];
    self.navigationItem.rightBarButtonItems = @[self.rightBtnItem];
    
    [self setupCollectionView];
    [self requestCarImgs];
}

-(void)setupCollectionView{
    
    UICollectionViewFlowLayout *manageFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    manageFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    manageFlowLayout.sectionInset = UIEdgeInsetsMake(10,14,10,14);
    manageFlowLayout.minimumLineSpacing = 5;// cell的横向间距
    manageFlowLayout.minimumInteritemSpacing = 5; // cell的纵向间距
    
    self.manageCollectionView = [[FWCollectionView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop) collectionViewLayout:manageFlowLayout];
    self.manageCollectionView.dataSource = self;
    self.manageCollectionView.delegate = self;
    self.manageCollectionView.showsVerticalScrollIndicator = NO;
    self.manageCollectionView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.manageCollectionView registerClass:[FWManageCarCell class] forCellWithReuseIdentifier:manageCollectionID];
    [self.view addSubview:self.manageCollectionView];
    
//    DHWeakSelf;
//    self.manageCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestCarImgs];
//    }];
//
//    self.manageCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestCarImgs];
//    }];
}

#pragma mark - > collectionView delegate & datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.manageDataSource.count?self.manageDataSource.count:0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWManageCarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:manageCollectionID forIndexPath:indexPath];
    cell.vc = self;
    
    if (indexPath.row < self.manageDataSource.count) {
        FWCarImgsModel * model = self.manageDataSource[indexPath.item];
        [cell configForCell:model];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < self.manageDataSource.count) {
        FWCarImgsModel * model = self.manageDataSource[indexPath.item];
        model.isSelect = !model.isSelect;

        if (self.selectArray.count >= 9 && model.isSelect) {
            [[FWHudManager sharedManager] showErrorMessage:@"最多只能选择9张图片" toController:self];
            return;
        }
        
        if (model.isSelect && ![self.selectArray containsObject:@(indexPath.item).stringValue]) {
            [self.selectArray addObject:@(indexPath.item).stringValue];
        }else if ([self.selectArray containsObject:@(indexPath.item).stringValue]) {
            [self.selectArray removeObject:@(indexPath.item).stringValue];
        }
        
        FWManageCarCell *cell = (FWManageCarCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [cell configForCell:model];
    }
    
    
    
    UIButton * deleteButton = (UIButton *)self.rightBtnItem.customView;

    if (self.selectArray.count > 0) {
        deleteButton.enabled = YES;
        [deleteButton setTitleColor:FWTextColor_272727 forState:UIControlStateNormal];
    }else{
        deleteButton.enabled = NO;
        [deleteButton setTitleColor:FWColor(@"9ea3ab") forState:UIControlStateNormal];
    }
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH-38)/2, ((SCREEN_WIDTH-38)/2)*127/169);
}

- (void)deleteButtonClick{
    
    self.rightBtnItem.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.rightBtnItem.enabled = YES;
    });
    
    NSString * car_imgs = @"";
    
    NSMutableArray * tempArr = self.manageDataSource.mutableCopy;
    
    [self.manageDataSource enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        FWCarImgsModel * model = self.manageDataSource[idx];
        
        if (model.isSelect) {
            [self.manageDataSource removeObject:model];
        }
    }];
 
    if (tempArr.count == self.manageDataSource.count) {
        /* 没有删除 */
        return;
    }
    
    if (self.manageDataSource.count >0) {
        if (tempArr.count > 0) {
            [tempArr removeAllObjects];
        }
        
        for (FWCarImgsModel * model in self.manageDataSource) {
            [tempArr addObject:model.car_img];
        }
        
        car_imgs = [tempArr mj_JSONString];
    }
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"user_car_id":self.user_car_id,
                              @"car_imgs":car_imgs,
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Upload_car_imgs WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"删除成功" toController:self];
            
            self.isRequest(YES);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

- (void)backBtnClick{
    self.isRequest(NO);
    [self.navigationController popViewControllerAnimated:YES];
}
@end
