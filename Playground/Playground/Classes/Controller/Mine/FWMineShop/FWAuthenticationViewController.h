//
//  FWAuthenticationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//
/**
 * 实名认证
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWAuthenticationViewController : FWBaseViewController

@property (nonatomic, assign) BOOL isBeforeFailure;//上次被拒绝过


@end

NS_ASSUME_NONNULL_END
