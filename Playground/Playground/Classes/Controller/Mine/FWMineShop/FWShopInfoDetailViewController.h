//
//  FWShopInfoDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 店铺详细信息
 */
#import "FWBaseViewController.h"
#import "FWHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWShopInfoDetailViewController : FWBaseViewController

@property (nonatomic, strong) FWBussinessShopModel * shopModel;
@property (nonatomic, assign) BOOL  isUserPage;
@property (nonatomic, strong) NSString * user_id;

@end

NS_ASSUME_NONNULL_END
