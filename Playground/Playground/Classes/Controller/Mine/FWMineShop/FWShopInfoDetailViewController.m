//
//  FWShopInfoDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWShopInfoDetailViewController.h"
#import "FWShopInfoDetailView.h"
#import "FWShopInfoViewController.h"

@interface FWShopInfoDetailViewController ()

@property (nonatomic, strong) FWShopInfoDetailView * detailView;

@end

@implementation FWShopInfoDetailViewController

- (void)requestShopInfo{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{
                              @"f_uid":self.user_id,
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@"1",
                              @"page_size":@"20",
                              };
    
    [request startWithParameters:params WithAction:Get_shop_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
           
            self.shopModel = [FWBussinessShopModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            self.shopModel.shop_info.user_info = self.shopModel.user_info;
            [self.detailView configForView:self.shopModel.shop_info];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"店铺信息详情页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"店铺信息详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"店铺信息";
    
    if (self.isUserPage) {
        UIButton * editButton = [[UIButton alloc] init];
        editButton.frame = CGRectMake(0, 0, 70, 40);
        editButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 14];
        [editButton setTitle:@"编辑信息" forState:UIControlStateNormal];
        [editButton setTitleColor:FWTextColor_272727 forState:UIControlStateNormal];
        [editButton addTarget:self action:@selector(editButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:editButton];
        self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    }
    
    self.detailView = [[FWShopInfoDetailView alloc] init];
    self.detailView.vc = self;
    self.detailView.isUserPage = self.isUserPage;
    [self.view addSubview:self.detailView];
    [self.detailView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self requestShopInfo];
}

- (void)editButtonClick{
     
    FWShopInfoViewController * SIVC = [[FWShopInfoViewController alloc] init];
    SIVC.shopInfoModel = self.shopModel.shop_info;
    SIVC.oprationType = EditGoods;
    [self.navigationController pushViewController:SIVC animated:YES];
}

@end
