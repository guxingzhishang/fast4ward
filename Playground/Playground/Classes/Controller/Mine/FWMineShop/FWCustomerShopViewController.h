//
//  FWCustomerShopViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 店铺主页 - 用户端
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCustomerShopViewController : FWBaseViewController

@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, assign) BOOL isUserPage;

@end

NS_ASSUME_NONNULL_END
