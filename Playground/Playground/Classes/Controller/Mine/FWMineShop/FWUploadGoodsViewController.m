//
//  FWUploadGoodsViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 上传商品
 */
#import "FWUploadGoodsViewController.h"
#import "FWUploadGoodsView.h"
#import "FWUploadManager.h"
#import "ALiOssUploadTool.h"

@interface FWUploadGoodsViewController ()<FWUploadGoodsViewDelegate>

@property (nonatomic, strong) FWUploadGoodsView * goodsView;
@property (nonatomic, assign) NSInteger showImageCount;
@property (nonatomic, assign) NSInteger detailImageCount;

@property (nonatomic, strong) NSMutableArray * imagesArray;

@property (nonatomic, strong) NSMutableDictionary * showDict;
@property (nonatomic, strong) NSMutableDictionary * detailDict;

@property (nonatomic, strong) NSMutableDictionary * combineDict;


@end

@implementation FWUploadGoodsViewController

- (NSMutableArray *)imageArray{
    if (!_imagesArray) {
        _imagesArray = [[NSMutableArray alloc] init];
    }
    
    return _imagesArray;
}

- (NSMutableDictionary *)showDict{
    if (!_showDict) {
        _showDict = [[NSMutableDictionary alloc] init];
    }
    return _showDict;
}

- (NSMutableDictionary *)detailDict{
    if (!_detailDict) {
        _detailDict = [[NSMutableDictionary alloc] init];
    }
    return _detailDict;
}

- (NSMutableDictionary *)combineDict{
    if (!_combineDict) {
        _combineDict = [[NSMutableDictionary alloc] init];
    }
    return  _combineDict;
}

#pragma mark - > 请求各种商店渠道
- (void)requestChannel{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_goods_channel WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {

        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWGoodsChannelModel * model = [FWGoodsChannelModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.goodsView setChannelMoudle:model];
            self.goodsView.addressTF.text = model.shop_address;

            if (self.oprationType == EditGoods) {
                [self.goodsView configForModel:self.listModel];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求阿里云临时凭证
- (void)requsetStsData{
    FWNetworkRequest * requst = [[FWNetworkRequest alloc] init];
    [requst requestStsTokenWithType:@"1"];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [IQKeyboardManager sharedManager].enable = YES;
    
    [self trackPageBegin:@"编辑/上传商品页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [IQKeyboardManager sharedManager].enable = NO;
    [self trackPageEnd:@"编辑/上传商品页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.oprationType == 1) {
        self.title = @"编辑商品";
    }else{
        self.title = @"上传商品";
    }
    
    [self setupSubViews];
    
    [self requsetStsData];
    [self requestChannel];
}

- (void)setupSubViews{
    
    self.goodsView = [[FWUploadGoodsView alloc] init];
    self.goodsView.vc = self;
    self.goodsView.uplLoadDelegate = self;
    [self.view addSubview:self.goodsView];
    [self.goodsView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - > 上传步骤
- (void)uploadButtonClick{
    
    [self requestImagesUploadName];
}


#pragma mark - > 获取展示图上传资源的文件名
- (void)requestImagesUploadName{
    
    NSArray * showImageArray = [self.goodsView.addShowImageView getImagesArray];
    self.imagesArray = [showImageArray mutableCopy];
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"goods_image",
                              @"number":@(showImageArray.count).stringValue,
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            [self requestDetailUploadName];
            self.showDict = [back objectForKey:@"data"];
        }
    }];
}

#pragma mark - > 获取详情图上传资源的文件名
- (void)requestDetailUploadName{
    
    NSArray * detailImageArray = [self.goodsView.addDetailImageView getImagesArray];
    [self.imagesArray addObjectsFromArray:[detailImageArray mutableCopy]];

    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"goods_image",
                              @"number":@(detailImageArray.count).stringValue,
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.detailDict = [back objectForKey:@"data"];
            [self combineDictionary];
        }
    }];
}

#pragma mark - > 合并字典
- (void)combineDictionary{
    
    NSMutableArray * tempArr = @[].mutableCopy;
    
    tempArr = [[self.showDict objectForKey:@"object_keys"] mutableCopy];
    [tempArr addObjectsFromArray:[[self.detailDict objectForKey:@"object_keys"] mutableCopy]];
    
    [self.combineDict setValue:[self.showDict objectForKey:@"bucket"] forKey:@"bucket"];
    [self.combineDict setValue:tempArr forKey:@"object_keys"];
    
    /* 合并结束后，预校验 */
    [self requestCheckWithType:@"1"];
}

#pragma mark - > 预校验 / 添加商品
- (void)requestCheckWithType:(NSString *)type{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    
    NSMutableDictionary * params = [[self getParams] mutableCopy];
    [params setObject:type forKey:@"flag_check"];
    
    NSString * goods_id = @"0";
    
    if (self.oprationType == EditGoods && self.listModel) {
        goods_id = self.listModel.goods_id;
        [params setObject:goods_id forKey:@"goods_id"];
    }
    
//    if ([type isEqualToString: @"2"]) {
//        request.isNeedShowHud = YES;
//    }
    [request startWithParameters:params.copy WithAction:Submit_add_goods  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        [[FWHudManager sharedManager] hideAllHudToController:self];

        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([type isEqualToString:@"1"]) {
                [self uploadFile];
            }else{
                
                [[FWHudManager sharedManager] showErrorMessage:@"商品已上架" toController:self];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

// 上传文件
- (void)uploadFile{
    
    // 商店上传图片
    DHWeakSelf;
    
//    [[FWHudManager sharedManager] showLoadingHudToController:self];
    
//    [ALiOssUploadTool asyncUploadImages:weakSelf.imagesArray WithBucketName:[weakSelf.combineDict objectForKey:@"bucket"] WithObject_key:nil WithObject_keys:[weakSelf.combineDict objectForKey:@"object_keys"] complete:^(NSArray<NSString *> *names, UploadImageState state) {
//        if (UploadImageSuccess == state) {
//
//            [weakSelf requestCheckWithType:@"2"];
//        }
//    }];
    
    NSMutableDictionary * params = [[self getParams] mutableCopy];
    NSString * goods_id = @"0";
    
    if (self.oprationType == EditGoods && self.listModel) {
        goods_id = self.listModel.goods_id;
        [params setObject:goods_id forKey:@"goods_id"];
    }
    
    FWUploadManager * defaultManager  = [FWUploadManager sharedRunLoopWorkDistribution];
    defaultManager.objectDict = self.combineDict;
    defaultManager.params = params;
    defaultManager.imagesArray = self.imagesArray;
    [defaultManager requestUploadFengmianImageWithType:@"3"];

    NSString * title =  @"上传中";
    NSString * desc = @"小四正在后台默默地帮您上传商品信息，请耐心等待";

    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:desc preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        [self.navigationController popViewControllerAnimated:YES];
    }];

    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (NSDictionary *)getParams{
    
    NSMutableArray * showTempImageArr = @[].mutableCopy;
    NSMutableArray * detailTempImageArr = @[].mutableCopy;

    NSArray * showImageArray = [self.goodsView.addShowImageView getImagesArray];
    NSArray * detailImageArray = [self.goodsView.addDetailImageView getImagesArray];

    for (int i =0; i < showImageArray.count ; i++) {
        UIImage * image = showImageArray[i];
        
        CGFloat fixelW = CGImageGetWidth(image.CGImage);
        CGFloat fixelH = CGImageGetHeight(image.CGImage);
        
        NSDictionary * dict = @{
                                @"wh":@"",
                                @"img_width":@(fixelW).stringValue,
                                @"img_height":@(fixelH).stringValue,
                                @"path":[self.showDict objectForKey:@"object_keys"][i],
                                };
        [showTempImageArr addObject:dict];
    }
    
    for (int i =0; i < detailImageArray.count ; i++) {
        UIImage * image = detailImageArray[i];
        
        CGFloat fixelW = CGImageGetWidth(image.CGImage);
        CGFloat fixelH = CGImageGetHeight(image.CGImage);
        
        NSDictionary * dict = @{
                                @"wh":@"",
                                @"img_width":@(fixelW).stringValue,
                                @"img_height":@(fixelH).stringValue,
                                @"path":[self.detailDict objectForKey:@"object_keys"][i],
                                };
        [detailTempImageArr addObject:dict];
    }
    
    NSString * imgs = [showTempImageArr mj_JSONString];
    NSString * detail = [detailTempImageArr mj_JSONString];

    NSString * channel_val;
    if ([self.goodsView.channelListModel.channel_desc isEqualToString:@"实体店"]) {
        channel_val = self.goodsView.addressTF.text;
    }else if ([self.goodsView.channelListModel.channel_desc isEqualToString:@"官网"]) {
        channel_val = self.goodsView.webTF.text;
    }else{
        channel_val = self.goodsView.paramTF.text;
    }
    
   
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"goods_id":@"0",
                              @"title":self.goodsView.titleTF.text,
                              @"title_short":self.goodsView.shortTitleTF.text,
                              @"price":self.goodsView.priceTF.text,
                              @"channel":self.goodsView.channelListModel.channel,
                              @"channel_val":channel_val,
                              @"imgs":imgs,
                              @"detail":detail,
                              @"flag_check":@"2",
                              };
    
    return params;
}
@end
