//
//  FWShopInfoViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 店铺信息
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWShopInfoViewController : FWBaseViewController
@property (nonatomic, strong) FWHomeShopListModel * shopInfoModel;
@property (nonatomic, assign) NSInteger oprationType;

@end

NS_ASSUME_NONNULL_END
