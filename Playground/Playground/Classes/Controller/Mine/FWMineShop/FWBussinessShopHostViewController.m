//
//  FWBussinessShopHostViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessShopHostViewController.h"
#import "FWBussinessShopHeaderView.h"
#import "FWBussinessShopHostCell.h"

static NSString * BussinessShopHostID = @"BussinessShopHostID";

@interface FWBussinessShopHostViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FWBussinessShopHeaderViewDelegate,FWBussinessShopHostCellDelegate>

@property (nonatomic, strong) FWCollectionView * shopCollectionView;
@property (nonatomic, strong) FWBussinessShopHeaderView * headerView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) NSString * status_show;// 上下架状态。0不显示，1上架状态，2下架状态
@property (nonatomic, strong) FWBussinessShopModel * shopModel;

@end

@implementation FWBussinessShopHostViewController
@synthesize shopCollectionView;
@synthesize headerView;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

#pragma mark - > 请求店铺首页
- (void)requestShopRequestWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        
        self.pageNum = 1;
        
        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum += 1;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"status_show":self.status_show,
                              @"page_size":@"20",
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_my_shop_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if (isLoadMoredData) {
            [self.shopCollectionView.mj_footer endRefreshing];
        }else{
            [self.shopCollectionView.mj_header endRefreshing];
        }
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.shopModel = [FWBussinessShopModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.dataSource addObjectsFromArray: self.shopModel.good_list];
            
            [headerView configForHeader:self.shopModel];

            if([self.shopModel.good_list count] == 0 &&
               self.pageNum != 1){
                [self.shopCollectionView.mj_footer endRefreshing];
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.shopCollectionView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self requestShopRequestWithLoadMoredData:NO];
    self.fd_prefersNavigationBarHidden = YES;
    
    [self trackPageBegin:@"店铺主页—商家管理端"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self trackPageEnd:@"店铺主页—商家管理端"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = FWViewBackgroundColor_EEEEEE;
    self.fd_prefersNavigationBarHidden = YES;

    self.pageNum = 1;
    self.status_show = @"1";
    
    [self setupSubviews];
}

- (void)setupSubviews{
    
    headerView = [[FWBussinessShopHeaderView alloc] init];
    headerView.isBussiness = YES;
    headerView.delegate = self;
    headerView.vc = self;
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 207);
    [self.view addSubview:headerView];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(0,17,10,17);
    flowLayout.minimumLineSpacing = 10;
    
    shopCollectionView = [[FWCollectionView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(headerView.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(headerView.frame)) collectionViewLayout:flowLayout];
    shopCollectionView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    shopCollectionView.dataSource = self;
    shopCollectionView.delegate = self;
    shopCollectionView.showsHorizontalScrollIndicator = NO;
    [shopCollectionView registerClass:[FWBussinessShopHostCell class] forCellWithReuseIdentifier:BussinessShopHostID];

    shopCollectionView.isNeedEmptyPlaceHolder = YES;
    NSString * attetionTitle = @"商家很懒，还没上传商品哦";
    NSDictionary *attetionAttributes = @{
                                         NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                         NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attentionString = [[NSMutableAttributedString alloc] initWithString:attetionTitle attributes:attetionAttributes];
    shopCollectionView.emptyDescriptionString = attentionString;
    shopCollectionView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.view addSubview:shopCollectionView];
    
    
    DHWeakSelf;
    shopCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestShopRequestWithLoadMoredData:NO];
    }];
    shopCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestShopRequestWithLoadMoredData:YES];
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWBussinessShopHostCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:BussinessShopHostID forIndexPath:indexPath];
    if (self.dataSource.count > indexPath.row) {
        cell.vc = self;
        cell.delegate = self;
        cell.deleteButton.tag = 4000+indexPath.row;
        [cell configForShopCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FWBussinessShopGoodsListModel * model = self.dataSource[indexPath.row];
    
    FWGoodsDetailViewController * DVC = [[FWGoodsDetailViewController alloc] init];
    DVC.goods_id = model.goods_id;
    [self.navigationController pushViewController:DVC animated:YES];
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(SCREEN_WIDTH-34, 91);
}

#pragma mark - > 查看已上、下架商品
- (void)bussinessShopGoodsDeal:(NSString *)index{
    
    self.status_show = index;
    [self requestShopRequestWithLoadMoredData:NO];
    [self.shopCollectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

#pragma mark - > 商品上下架操作
-(void)downButtonClick:(NSInteger)index{
    
    [self requestShopRequestWithLoadMoredData:NO];
}

- (void)deleteButtonClick:(NSInteger)index{
    
    FWBussinessShopGoodsListModel * model = self.dataSource[index];

    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"goods_id":model.goods_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_delete_goods WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"删除成功" toController:self];

            [self.dataSource removeObjectAtIndex:index];
            [UIView performWithoutAnimation:^{
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
                [self.shopCollectionView reloadSections:indexSet];
            }];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

@end
