//
//  FWCustomerShopViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCustomerShopViewController.h"
#import "FWBussinessShopHeaderView.h"
#import "FWCustomerShopHostCell.h"

static NSString * CustomerShopHostID = @"CustomerShopHostID";

@interface FWCustomerShopViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FWBussinessShopHeaderViewDelegate>

@property (nonatomic, strong) FWCollectionView * shopCollectionView;
@property (nonatomic, strong) FWBussinessShopHeaderView * headerView;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) FWBussinessShopModel * shopModel;

@end

@implementation FWCustomerShopViewController
@synthesize shopCollectionView;
@synthesize headerView;

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc] init];
    }
    
    return _dataSource;
}

#pragma mark - > 请求店铺首页
- (void)requestShopRequestWithLoadMoredData:(BOOL)isLoadMoredData{
    
    if (isLoadMoredData == NO) {
        
        self.pageNum = 1;
        
        if (self.dataSource.count > 0) {
            [self.dataSource removeAllObjects];
        }
    }else{
        self.pageNum += 1;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"f_uid":self.user_id,
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@(self.pageNum).stringValue,
                              @"page_size":@"20",
                              };
    request.isNeedShowHud = YES;
    [request startWithParameters:params WithAction:Get_shop_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if (isLoadMoredData) {
            [self.shopCollectionView.mj_footer endRefreshing];
        }else{
            [self.shopCollectionView.mj_header endRefreshing];
        }
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.shopModel = [FWBussinessShopModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.dataSource addObjectsFromArray: self.shopModel.good_list];
            
            [headerView configForHeader:self.shopModel];
            
            if([self.shopModel.good_list count] == 0 &&
               self.pageNum != 1){
                [self.shopCollectionView.mj_footer endRefreshing];
                [[FWHudManager sharedManager] showErrorMessage:loadRefreshTips toController:self];
            }else{
                [self.shopCollectionView reloadData];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view.backgroundColor = FWViewBackgroundColor_EEEEEE;
    
    [self trackPageBegin:@"店铺主页—用户端"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"店铺主页—用户端"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWViewBackgroundColor_EEEEEE;
    self.fd_prefersNavigationBarHidden = YES;
    
    self.pageNum = 1;
    
    [self setupSubviews];
    [self requestShopRequestWithLoadMoredData:NO];
}

- (void)setupSubviews{
    
    headerView = [[FWBussinessShopHeaderView alloc] init];
    headerView.isBussiness = NO;
    headerView.vc = self;
    headerView.delegate = self;
    headerView.isUserPage = self.isUserPage;
    headerView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 177);
    [self.view addSubview:headerView];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(0,10,10,10);
    flowLayout.minimumLineSpacing = 10;
    
    shopCollectionView = [[FWCollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(headerView.frame)+10, SCREEN_WIDTH, SCREEN_HEIGHT-10-CGRectGetMaxY(headerView.frame)) collectionViewLayout:flowLayout];
    shopCollectionView.backgroundColor = FWViewBackgroundColor_EEEEEE;
    shopCollectionView.dataSource = self;
    shopCollectionView.delegate = self;
    shopCollectionView.showsHorizontalScrollIndicator = NO;
    [shopCollectionView registerClass:[FWCustomerShopHostCell class] forCellWithReuseIdentifier:CustomerShopHostID];
    
    shopCollectionView.isNeedEmptyPlaceHolder = YES;
    NSString * attetionTitle = @"商家很懒，还没有上传商品哦~";
    NSDictionary *attetionAttributes = @{
                                         NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Light" size:14],
                                         NSForegroundColorAttributeName:FWTextColor_969696};
    NSMutableAttributedString * attentionString = [[NSMutableAttributedString alloc] initWithString:attetionTitle attributes:attetionAttributes];
    shopCollectionView.emptyDescriptionString = attentionString;
    shopCollectionView.emptyPlaceHolderBackgroundColor = FWViewBackgroundColor_EEEEEE;
    [self.view addSubview:shopCollectionView];
    
    
    DHWeakSelf;
    shopCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestShopRequestWithLoadMoredData:NO];
    }];
    shopCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestShopRequestWithLoadMoredData:YES];
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FWCustomerShopHostCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CustomerShopHostID forIndexPath:indexPath];
    
    if (self.dataSource.count > indexPath.row) {
        [cell configForShopCell:self.dataSource[indexPath.row]];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FWBussinessShopGoodsListModel * model = self.dataSource[indexPath.row];
    
    FWGoodsDetailViewController * DVC = [[FWGoodsDetailViewController alloc] init];
    DVC.goods_id = model.goods_id;
    [self.navigationController pushViewController:DVC animated:YES];
}


- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake((SCREEN_WIDTH-30)/2, 240);
}

@end
