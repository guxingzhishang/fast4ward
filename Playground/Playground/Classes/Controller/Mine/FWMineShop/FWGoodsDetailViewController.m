//
//  FWGoodsDetailViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoodsDetailViewController.h"
#import "FWGoodsDetailView.h"

@interface FWGoodsDetailViewController ()
@property (nonatomic, strong) FWGoodsDetailView * detailView;

@property (nonatomic, strong) UIView * naviBarView;
@property (nonatomic, strong) UIButton * blackBackButton;
@property (nonatomic, assign) CGFloat alphaValue;

@property (nonatomic, strong) UIView * linkView;
@property (nonatomic, strong) UIButton * linkButton;
@property (nonatomic, strong) FWBussinessShopGoodsListModel * goodsModel;
@end

@implementation FWGoodsDetailViewController
@synthesize naviBarView;
@synthesize blackBackButton;
@synthesize alphaValue;

#pragma mark - > 获取商品详情
- (void)requestGoodsInfo{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"goods_id":self.goods_id,
                              };
    
    [request startWithParameters:params WithAction:Get_goods_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.goodsModel = [FWBussinessShopGoodsListModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.detailView configForView:self.goodsModel];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self trackPageBegin:@"商品详情页"];
    
    self.navigationController.navigationBar.alpha = self.alphaValue;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"商品详情页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.fd_prefersNavigationBarHidden = YES;
    alphaValue = 0;

    [self setupSubviews];
    [self setupLinkView];
    
    [self requestGoodsInfo];
}

- (void)setupSubviews{
    
    naviBarView = [[UIView alloc] init];
    naviBarView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    naviBarView.alpha = 0;
    naviBarView.hidden = YES;
    [self.view addSubview:naviBarView];
    naviBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64+FWCustomeSafeTop);
    
    blackBackButton = [[UIButton alloc] init];
    [blackBackButton setImage:[UIImage imageNamed:@"返回-press"] forState:UIControlStateNormal];
    [blackBackButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [naviBarView addSubview:blackBackButton];
    [blackBackButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(naviBarView).mas_offset(15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.mas_equalTo(naviBarView).mas_offset(30+FWCustomeSafeTop);
    }];
    blackBackButton.hidden = YES;
    
    self.detailView = [[FWGoodsDetailView alloc] init];
    self.detailView.delegate = self;
    self.detailView.vc = self;
    [self.view addSubview:self.detailView];
    [self.detailView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view).mas_offset(-60-FWSafeBottom);
    }];
    
    [self.view bringSubviewToFront:naviBarView];
}

#pragma mark - > 初始化链接视图
- (void)setupLinkView{
    
    self.linkView = [UIView new];
    self.linkView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [self.view addSubview:self.linkView];
    [self.linkView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(self.view);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(60+FWSafeBottom);
    }];
    
    self.linkButton = [[UIButton alloc] init];
    self.linkButton.layer.cornerRadius = 2;
    self.linkButton.layer.masksToBounds = YES;
    self.linkButton.backgroundColor = FWColor(@"ff6f00");
    [self.linkButton setTitle:@"直达链接" forState:UIControlStateNormal];
    [self.linkButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.linkButton addTarget:self action:@selector(linkButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.linkView addSubview:self.linkButton];
    [self.linkButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.linkView);
        make.centerX.mas_equalTo(self.linkView);
        make.size.mas_equalTo(CGSizeMake(210, 40));
    }];
}


#pragma mark - > 直达链接
- (void)linkButtonOnClick{
    
    self.linkButton.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.linkButton.enabled = YES;
    });

    if (!self.goodsModel.channel ||!self.goodsModel.channel_val) {
        return;
    }
    
    if ([self.goodsModel.channel isEqualToString:@"taobao"]) {
        /* 淘宝 */
        NSURL * taobaoURL = [NSURL URLWithString:taobaoDetail(self.goodsModel.channel_val)];
        if ([[UIApplication sharedApplication] canOpenURL:taobaoURL]) {
            
            [[UIApplication sharedApplication] openURL:taobaoURL options:@{} completionHandler:nil];
        }else{
            [self openWebView:self.goodsModel.h5_url];
        }
    }else if ([self.goodsModel.channel isEqualToString:@"jingdong"]) {
        /* 京东 */
        NSURL * jdURL = [NSURL URLWithString:[JDDetail(self.goodsModel.channel_val) stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        if ([[UIApplication sharedApplication] canOpenURL:jdURL]){
            
            [[UIApplication sharedApplication] openURL:jdURL options:@{} completionHandler:nil];
        }else{
            [self openWebView:self.goodsModel.h5_url];
        }
    }else if ([self.goodsModel.channel isEqualToString:@"tianmao"]) {
        /* 天猫 */
        NSURL * tamllURL = [NSURL URLWithString:[tmallDetail(self.goodsModel.channel_val) stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        if ([[UIApplication sharedApplication] canOpenURL:tamllURL]){
            
            [[UIApplication sharedApplication] openURL:tamllURL options:@{} completionHandler:nil];
        }else{
            [self openWebView:self.goodsModel.h5_url];
        }
    }else if ([self.goodsModel.channel isEqualToString:@"weidian"]) {
        /* 微店 */
        NSURL * weidianURL = [NSURL URLWithString:weidianDetail(self.goodsModel.channel_val)];

        if ([[UIApplication sharedApplication] canOpenURL:weidianURL]) {
            
            [[UIApplication sharedApplication] openURL:weidianURL options:@{} completionHandler:nil];
        }else{
            [self openWebView:self.goodsModel.h5_url];
        }
    }else if ([self.goodsModel.channel isEqualToString:@"guanwang"]) {
        /* 官网 */
        [self openWebView:self.goodsModel.channel_val];
    }else{

        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"该商品暂不支持线上购买" message:[NSString stringWithFormat:@"店铺地址:%@",self.goodsModel.user_info.shop_address] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 如果打不开走H5
- (void)openWebView:(NSString *)urlString{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.htmlStr = urlString;
    WVC.pageType = WebViewTypeURL;
    [self.navigationController pushViewController:WVC animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([scrollView isEqual:self.detailView]){
        
        if (scrollView.contentOffset.y > 150){
            alphaValue = 1;
            naviBarView.alpha = alphaValue;
        }else{
            self.alphaValue = (self.detailView.contentOffset.y/150);
            naviBarView.alpha = alphaValue;
        }
        
        if (scrollView.contentOffset.y >0) {
            naviBarView.hidden = NO;
            self.detailView.backButton.hidden = YES;
            blackBackButton.hidden = NO;
        }else{
            naviBarView.hidden = YES;
            self.detailView.backButton.hidden = NO;
            blackBackButton.hidden = YES;
        }
    }
}

- (void)backButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
