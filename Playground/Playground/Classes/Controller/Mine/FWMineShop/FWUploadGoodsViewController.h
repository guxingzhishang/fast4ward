//
//  FWUploadGoodsViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

typedef NS_ENUM(NSUInteger, oprationType) {
    AddGoods = 1,// 添加
    EditGoods,   // 编辑
};

NS_ASSUME_NONNULL_BEGIN

@interface FWUploadGoodsViewController : FWBaseViewController

@property (nonatomic, strong) FWBussinessShopGoodsListModel * listModel;

@property (nonatomic, assign) NSInteger oprationType;

@end

NS_ASSUME_NONNULL_END
