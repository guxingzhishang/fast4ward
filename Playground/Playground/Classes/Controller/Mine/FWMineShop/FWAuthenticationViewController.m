//
//  FWAuthenticationViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/24.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWAuthenticationViewController.h"
#import "FWNameAuthenticationView.h"
#import "ALiOssUploadTool.h"
#import "FWBussinessHomePageModel.h"

@interface FWAuthenticationViewController ()<FWNameAuthenticationViewDelegate>
@property (nonatomic, strong) FWNameAuthenticationView * authenticationView;
@property (nonatomic, strong) NSDictionary * dataDict;
@property (nonatomic, strong) NSString * card_url;
@property (nonatomic, strong) FWBussinessHomePageModel * bussinessModel;

@end

@implementation FWAuthenticationViewController

#pragma mark - > 获取商家主页
- (void)requestMerchantInfo{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_merchant_info WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            /* 已认证，进入商家主页 */
            self.bussinessModel = [FWBussinessHomePageModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            [self.authenticationView configForView:self.bussinessModel];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求阿里临时验证
- (void)requestStsData{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    [request requestStsTokenWithType:@"1"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self requestUploadName];
    });
}

#pragma mark - > 获取上传资源的文件名
- (void)requestUploadName{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"idcard_hold",
                              @"number":@"1",
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.dataDict = [back objectForKey:@"data"];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"实名认证页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"实名认证页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"实名认证";
    
    
    self.authenticationView = [[FWNameAuthenticationView alloc] init];
    self.authenticationView.vc = self;
    self.authenticationView.delegate = self;
    [self.view addSubview:self.authenticationView];
    [self.authenticationView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self requestStsData];
    
    if (self.isBeforeFailure) {
        [self requestMerchantInfo];
    }
}

#pragma mark - > 上传身份证
- (void)uploadCardPhotoClick:(UIImage *)image{
    
    [ALiOssUploadTool asyncUploadImage:image WithBucketName:[self.dataDict objectForKey:@"bucket"] WithObject_key:[self.dataDict objectForKey:@"object_key"] WithObject_keys:nil complete:^(NSArray<NSString *> *names, UploadImageState state) {
        if (UploadImageSuccess == state) {
            self.card_url = [self.dataDict objectForKey:@"object_key"];
        }
    }];
}

#pragma mark - > 提交审核
- (void)commitButtonClick{
   
    if (self.card_url.length <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请上传身份证照片" toController:self];
    }else{
        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"real_name":self.authenticationView.realNameTextField.text,
                                  @"mobile":self.authenticationView.phoneTextField.text,
                                  @"idcard":self.authenticationView.cardIDTextField.text,
                                  @"idcard_hold_img":self.card_url,
                                  };
        
        request.isNeedShowHud = YES;
        [request startWithParameters:params WithAction:Submit_shop_verify WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"提交成功！小四会尽快审核您的信息，通过后立即短信通知您。" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
            }
        }];
    }
}
@end
