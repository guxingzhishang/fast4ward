//
//  FWShopInfoViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWShopInfoViewController.h"
#import "FWShopInfoView.h"
#import "FWUploadManager.h"

@interface FWShopInfoViewController ()<FWShopInfoViewDelegate>

@property (nonatomic, strong) FWShopInfoView * shopInfoView;
@property (nonatomic, strong) NSMutableArray * imagesArray;
@property (nonatomic, strong) NSMutableDictionary * showDict;
@property (nonatomic, strong) NSDictionary * originalDict; //进入页面后初始数组（要上传的参数）用于之后返回做判断
@property (nonatomic, strong) FWBussinessShopModel * shopModel;

@end

@implementation FWShopInfoViewController
- (NSMutableArray *)imageArray{
    if (!_imagesArray) {
        _imagesArray = [[NSMutableArray alloc] init];
    }
    
    return _imagesArray;
}

- (NSMutableDictionary *)showDict{
    if (!_showDict) {
        _showDict = [[NSMutableDictionary alloc] init];
    }
    return _showDict;
}

- (void)backBtnClick{
    
    if ([self.originalDict isEqual:[self getParams]]) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"您有信息修改未保存，直接返回吗?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"返回" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self saveButtonClick];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - > 请求阿里云临时凭证
- (void)requsetStsData{
    FWNetworkRequest * requst = [[FWNetworkRequest alloc] init];
    [requst requestStsTokenWithType:@"1"];
}

#pragma mark - > 获取主营业务
-(void)requestPersonalData{
    
    FWCertificationRequest * request = [[FWCertificationRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID]
                              };
    
    [request startWithParameters:params WithAction:Get_main_business_list WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWBussinessCertificationModel * model = [FWBussinessCertificationModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            [self.shopInfoView setupBussinessInfo:model];
            
            [self.shopInfoView configViewForModel:self.shopModel.shop_info];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.originalDict = [[self getParams] copy];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 请求店铺首页
- (void)requestShopDataWithAction:(NSString *)action{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"page":@"1",
                              @"status_show":@"0",
                              @"page_size":@"20",
                              };
    [request startWithParameters:params WithAction:action WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.shopModel = [FWBussinessShopModel mj_objectWithKeyValues:[back objectForKey:@"data"]];

            [self requestPersonalData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.view.backgroundColor = FWTextColor_F6F8FA;
    [IQKeyboardManager sharedManager].enable = YES;

    [self trackPageBegin:@"店铺信息页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [IQKeyboardManager sharedManager].enable = NO;
    
    [self trackPageEnd:@"店铺信息页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"店铺信息";
    self.originalDict = @{};
    
    self.shopInfoView = [[FWShopInfoView alloc] init];
    self.shopInfoView.vc = self;
    self.shopInfoView.shopInfoDelegate = self;
    [self.view addSubview:self.shopInfoView];
    [self.shopInfoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self requsetStsData];
    
    [self requestShopDataWithAction:Get_my_shop_info];
}

- (void)saveButtonClick{
    
    //去掉空格
    NSString *formatString = [self.shopInfoView.descTextView.text
                              stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (formatString.length > 100 && formatString.length < 8) {
        [[FWHudManager sharedManager] showErrorMessage:@"请输入8到100字店铺描述" toController:self];
        return;
    }
    

    if (self.shopInfoView.idArray.count <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请至少选择一项主营业务" toController:self];
        return;
    }
    
    NSArray * showImageArray = [self.shopInfoView.addPicsImageView getImagesArray];
    if (showImageArray.count <= 0) {
        [[FWHudManager sharedManager] showErrorMessage:@"请至少上传一张店铺图片" toController:self];
        return;
    }

    if (self.shopInfoView.provice_id.length <= 0 ||
        self.shopInfoView.city_id.length<= 0 ||
        self.shopInfoView.arear_id.length <=0) {
        [[FWHudManager sharedManager] showErrorMessage:@"地址选择有误，请重新选择" toController:self];
        return ;
    }
    
    [self requestImagesUploadName];
}


#pragma mark - > 获取展示图上传资源的文件名
- (void)requestImagesUploadName{
    
    NSArray * showImageArray = [self.shopInfoView.addPicsImageView getImagesArray];
    self.imagesArray = [showImageArray mutableCopy];
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"resource_type":@"goods_image",
                              @"number":@(showImageArray.count).stringValue,
                              };
    [request startWithParameters:params WithAction:Get_upload_object_name  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.showDict = [back objectForKey:@"data"];
            
            /* 预校验 */
            [self requestCheckWithType:@"1"];
        }
    }];
}

#pragma mark - > 预校验 / 添加商品
- (void)requestCheckWithType:(NSString *)type{
    
    FWUpdateInfomationRequest * request = [[FWUpdateInfomationRequest alloc] init];
    
    NSMutableDictionary * params = [[self getParams] mutableCopy];
    [params setObject:type forKey:@"flag_check"];
    
    [request startWithParameters:params.copy WithAction:Update_shop_info  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        [[FWHudManager sharedManager] hideAllHudToController:self];
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            if ([type isEqualToString:@"1"]) {
                [self uploadFile];
            }else{
                
                [[FWHudManager sharedManager] showErrorMessage:@"信息已提交" toController:self];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

// 上传文件
- (void)uploadFile{
    
    // 商店上传图片
    NSMutableDictionary * params = [[self getParams] mutableCopy];
    
    FWUploadManager * defaultManager  = [FWUploadManager sharedRunLoopWorkDistribution];
    defaultManager.objectDict = self.showDict;
    defaultManager.params = params;
    defaultManager.imagesArray = self.imagesArray;
    [defaultManager requestUploadFengmianImageWithType:@"4"];
    
    NSString * title =  @"上传中";
    NSString * desc = @"小四正在后台默默地帮您上传店铺信息，请耐心等待";
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:desc preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (NSDictionary *)getParams{
    
    NSMutableArray * showTempImageArr = @[].mutableCopy;
    
    NSArray * showImageArray = [self.shopInfoView.addPicsImageView getImagesArray];
    
    for (int i =0; i < showImageArray.count ; i++) {
        UIImage * image = showImageArray[i];
        
        CGFloat fixelW = CGImageGetWidth(image.CGImage);
        CGFloat fixelH = CGImageGetHeight(image.CGImage);
        
        NSDictionary * dict = @{
                                @"wh":@"",
                                @"img_width":@(fixelW).stringValue,
                                @"img_height":@(fixelH).stringValue,
                                @"path":[self.showDict objectForKey:@"object_keys"][i],
                                };
        [showTempImageArr addObject:dict];
    }
    
    NSString * imgs = [showTempImageArr mj_JSONString];
    
    NSString * idString = [self.shopInfoView.idArray mj_JSONString];

    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"shop_desc":self.shopInfoView.descTextView.text,
                              @"province_id":self.shopInfoView.provice_id,
                              @"city_id":self.shopInfoView.city_id,
                              @"area_id":self.shopInfoView.arear_id,
                              @"address":self.shopInfoView.addressField.text?self.shopInfoView.addressField.text:@"",
                              @"link_man":@"",
                              @"mobile":self.shopInfoView.phoneField.text?self.shopInfoView.phoneField.text:@"",
                              @"main_business_ids":idString,
                              @"main_cars":self.shopInfoView.carStyleField.text?self.shopInfoView.carStyleField.text:@"",
                              @"imgs":imgs,
                              @"flag_check":@"2",
                              };
    
    return params;
}

@end
