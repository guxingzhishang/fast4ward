//
//  FWGoodsDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/23.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 商品详情
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGoodsDetailViewController : FWBaseViewController<UIScrollViewDelegate>

@property (nonatomic, strong) NSString * goods_id;


@end

NS_ASSUME_NONNULL_END
