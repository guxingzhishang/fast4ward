//
//  FWVisitorDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWTableView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWVisitorDetailViewController : FWBaseViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * currentTitle;
@property (nonatomic, strong) FWVisitorAnalysisListModel * proviceModel;
@property (nonatomic, strong) FWVisitorAnalysisListModel * carModel;


@end

NS_ASSUME_NONNULL_END
