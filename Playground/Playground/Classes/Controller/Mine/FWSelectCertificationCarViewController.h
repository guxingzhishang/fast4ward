//
//  FWSelectCertificationCarViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 选择认证车型
 */
#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^valueBlock)(FWPersonalCertificationListModel *carModel);

@interface FWSelectCertificationCarViewController : FWBaseViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>

/**
 * 做navigation的view
 */
@property (nonatomic, strong) UIView * headerView;

/**
 * 取消按钮
 */
@property (nonatomic, strong) UIButton * cancelBtn;

/**
 * 搜索
 */
@property (nonatomic, strong) UISearchBar * searchBar;

/**
 * 列表的tableview
 */
@property (nonatomic, strong) UITableView * listTableView;

@property (nonatomic, copy) valueBlock myBlock;

@end

NS_ASSUME_NONNULL_END
