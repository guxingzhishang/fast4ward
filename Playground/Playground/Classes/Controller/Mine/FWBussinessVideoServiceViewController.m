//
//  FWBussinessVideoServiceViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessVideoServiceViewController.h"
#import "FWBussinessHomePageModel.h"

@interface FWBussinessVideoServiceViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView * mainScrollView;
@property (nonatomic, strong) UIView * containerView;

@end

@implementation FWBussinessVideoServiceViewController
@synthesize mainScrollView;
@synthesize containerView;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"视频套餐页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"视频套餐页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = FWTextColor_F8F8F8;
    self.title = @"视频套餐";
    
    mainScrollView = [[UIScrollView alloc] init];
    mainScrollView.delegate = self;
    mainScrollView.backgroundColor = FWTextColor_F8F8F8;
    mainScrollView.showsVerticalScrollIndicator = NO;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:mainScrollView];
    [mainScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    containerView = [[UIView alloc] init];
    [mainScrollView addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(mainScrollView);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    
    [self setupSubviews];
}

- (void)setupSubviews{

    UILabel * titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 12.1];
    titleLabel.textColor = FWTextColor_000000;
    titleLabel.text = @"我的套餐";
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [containerView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(containerView).mas_offset(30);
        make.top.mas_equalTo(containerView).mas_offset(30);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(15);
    }];
    
    CGFloat currentY = 63;
    
    for (int i = 0; i < self.productList.count; i ++) {
        
        FWBussinessProductListModel * model = self.productList[i];
        
        if (model.width.length <= 0) {
            model.width = @"216";
        }
        if (model.height.length <= 0) {
            model.height = @"88";
        }
        
        UILabel * numberLabel =  [[UILabel alloc] init];
        numberLabel.text = model.number;
        numberLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 16];
        numberLabel.textColor = FWTextColor_5F6680;
        numberLabel.textAlignment = NSTextAlignmentRight;
        [containerView addSubview:numberLabel];
        [numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_greaterThanOrEqualTo(10);
            make.top.mas_equalTo(containerView).mas_offset(currentY);
            make.right.mas_equalTo(containerView).mas_offset(-40);
            make.height.mas_equalTo(20);
        }];
        
        UILabel * descLabel =  [[UILabel alloc] init];
        descLabel.text = model.title;
        descLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 11];
        descLabel.textColor = FWTextColor_5F6680;
        descLabel.textAlignment = NSTextAlignmentLeft;
        [containerView addSubview:descLabel];
        [descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(containerView).mas_offset(40);
            make.top.mas_equalTo(containerView).mas_offset(currentY);
            make.right.mas_equalTo(numberLabel.mas_left).mas_offset(-20);
            make.height.mas_equalTo(20);
        }];
        
        UIImageView * serviceImageView = [[UIImageView alloc] init];
        [containerView addSubview:serviceImageView];
        [serviceImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(containerView).mas_offset(40);
            make.right.mas_equalTo(containerView).mas_offset(-40);
            make.top.mas_equalTo(containerView).mas_offset(currentY+20);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-80, [model.height floatValue]*(SCREEN_WIDTH-80)/[model.width floatValue]));
            if(i == self.productList.count-1){
                make.bottom.mas_equalTo(containerView).mas_offset(-50);
            }
        }];
        [serviceImageView sd_setImageWithURL:[NSURL URLWithString:model.img_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        currentY = currentY + 20 + [model.height floatValue]+27;
    }
}

@end
