//
//  FWPersonalCertificationViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPersonalCertificationViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
