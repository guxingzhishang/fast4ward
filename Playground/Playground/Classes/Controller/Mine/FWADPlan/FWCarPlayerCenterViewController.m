//
//  FWCarPlayerCenterViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/6.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarPlayerCenterViewController.h"
#import "FWCarPlayerCenterCell.h"
#import "FWCarPlayerCenterView.h"
#import "FWMySignupViewController.h"
#import "FWCarPlayerADPlanViewController.h"
#import "FWJoinADPlanWebViewController.h"

@interface FWCarPlayerCenterViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView * navigtionView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) FWCarPlayerCenterView * headerView;

@property (nonatomic, strong) NSArray * titleArray;
@property (nonatomic, strong) NSArray * iconArray;
@property (nonatomic, strong) NSDictionary * infoDict;


@end

@implementation FWCarPlayerCenterViewController
@synthesize infoTableView;

- (void)requestDriverCenter{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_driver_center WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.infoDict = [back objectForKey:@"data"];
            [self.infoTableView reloadData];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"车手中心页"];
    
    [self requestDriverCenter];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"车手中心页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.infoDict = @{};
    self.fd_prefersNavigationBarHidden = YES;

    self.titleArray = @[@"我的报名",@"广告车招募计划"];
    self.iconArray = @[@"carplayercenter_signup",@"carplayercenter_money"];
    
    self.navigtionView = [[UIView alloc] init];
    self.navigtionView.backgroundColor = FWTextColor_252527;
    [self.view addSubview:self.navigtionView];
    [self.navigtionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, FWCustomeSafeTop +64));
        make.top.left.right.mas_equalTo(self.view);
    }];
    
    self.backButton= [[UIButton alloc] initWithFrame: CGRectMake(0,0,45,30)];
    self.backButton.titleLabel.font = DHSystemFontOfSize_16;
    self.backButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [self.backButton setImage:[UIImage imageNamed:@"white_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.navigtionView addSubview:self.backButton];
    [self.backButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(55, 30));
        make.left.mas_equalTo(self.navigtionView).mas_offset(10);
        make.bottom.mas_equalTo(self.navigtionView).mas_offset(-10);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"车手中心";
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 19];
    self.titleLabel.textColor = FWViewBackgroundColor_FFFFFF;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navigtionView addSubview:self.titleLabel];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 30));
        make.centerY.mas_equalTo(self.backButton);
        make.centerX.mas_equalTo(self.navigtionView);
    }];
    
    [self setupSubViews];
}

- (void)setupSubViews{
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 59;
    infoTableView.scrollEnabled = NO;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.navigtionView.mas_bottom);
    }];
    
    self.headerView = [[FWCarPlayerCenterView alloc] init];
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.headerView.currentHeight);
    infoTableView.tableHeaderView = self.headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"CarPlayerCenterCellID";
    
    FWCarPlayerCenterCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWCarPlayerCenterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.vc = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.nameLabel.text = self.titleArray[indexPath.row];
    cell.photoImageView.image = [UIImage imageNamed:self.iconArray[indexPath.row]];
    cell.signLabel.text = @"";
    cell.signLabel.hidden = YES;
    
    if (indexPath.row == 1) {
        cell.signLabel.text = self.infoDict[@"msg"];
        cell.signLabel.textColor = FWTextColor_9EA3AB;
        cell.signLabel.hidden = NO;
    }
    
    [cell.photoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(cell.contentView);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.left.mas_equalTo(cell.contentView).mas_offset(24);
    }];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        FWMySignupViewController * msvc = [[FWMySignupViewController alloc] init];
        [self.navigationController pushViewController:msvc animated:YES];
    }else{
        if([self.infoDict[@"join_status"] isEqualToString:@"1"]){
            /* 已加入 */
            FWCarPlayerADPlanViewController * CAPVC = [[FWCarPlayerADPlanViewController alloc] init];
            [self.navigationController pushViewController:CAPVC animated:YES];
        }else{
            /* 未加入 */
            FWJoinADPlanWebViewController * WVC = [[FWJoinADPlanWebViewController alloc] init];
            WVC.showBottom = YES;
//            WVC.pageType = WebViewTypeURL;
            WVC.htmlStr = self.infoDict[@"h5_plan"];
            WVC.infodict = self.infoDict;
            [self.navigationController pushViewController:WVC animated:YES];
        }
    }
}

@end
