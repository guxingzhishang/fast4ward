//
//  FWCarPlayerADPlanViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/6.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCarPlayerADPlanCell : UITableViewCell

@property (nonatomic, strong) UIView * shadowView;
@property (nonatomic, strong) UIView * bgView;
@property (nonatomic, strong) UILabel * planLabel;
@property (nonatomic, strong) UIButton * detailButton;
@property (nonatomic, strong) UILabel * statusLabel;
@property (nonatomic, strong) UILabel * nameLabel;
@property (nonatomic, strong) UILabel * idLabel;
@property (nonatomic, strong) UIButton * signoutButton;
@property (nonatomic, strong) UIButton * protocolButton;
@property (nonatomic, weak) UIViewController * vc;

@property (nonatomic, strong) NSDictionary * infoDict;

- (void)configForCell:(id)model;

@end

@interface FWCarPlayerADPlanViewController : FWBaseViewController

@end

NS_ASSUME_NONNULL_END
