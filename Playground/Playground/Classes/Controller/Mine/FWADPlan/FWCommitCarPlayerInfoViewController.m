//
//  FWCommitCarPlayerInfoViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/7.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCommitCarPlayerInfoViewController.h"
#import "FWCommitCarPlayerInfoView.h"
#import "FWCarPlayerCenterViewController.h"

@interface FWCommitCarPlayerInfoViewController ()
@property (nonatomic, strong) FWCommitCarPlayerInfoView * infoView;

@end

@implementation FWCommitCarPlayerInfoViewController


- (void)requestDriverCenter{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_driver_center WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.infoView.h5_url = [[back objectForKey:@"data"] objectForKey:@"h5_agreement"];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"提交车手资料页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"提交车手资料页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"提交车手资料";
    
    self.infoView = [[FWCommitCarPlayerInfoView alloc] init];
    self.infoView.vc = self;
    self.infoView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [self.view addSubview:self.infoView];
    [self.infoView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self requestDriverCenter];
}

- (void)backBtnClick{

    for (UIViewController * vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[FWCarPlayerCenterViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            return ;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
