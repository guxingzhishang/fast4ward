//
//  FWJoinADPlanWebViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWJoinADPlanWebViewController.h"
#import "FWCommitCarPlayerInfoViewController.h"
#import <WebKit/WKWebView.h>
#import <WebKit/WebKit.h>

@interface FWJoinADPlanWebViewController ()<WKUIDelegate,WKNavigationDelegate>

@property (nonatomic, strong) WKWebView * webView;

@end

@implementation FWJoinADPlanWebViewController

#pragma mark - > 生命周期
- (void)dealloc{
    [self.webView removeObserver:self forKeyPath:@"title" context:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"广告车招募计划H5"];
    
    if (@available(iOS 11.0, *)) {
        _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    [self trackPageEnd:@"广告车招募计划H5"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.webTitle;

    UIButton * shareButton = [[UIButton alloc] init];
    shareButton.frame = CGRectMake(0, 0, 45, 40);
    [shareButton setImage:[UIImage imageNamed:@"new_black_share"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    // 是使用h5的视频播放器在线播放, 还是使用原生播放器全屏播放
    config.allowsInlineMediaPlayback = YES;
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop) configuration:config];
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    _webView.allowsBackForwardNavigationGestures = YES;
    [self.view addSubview:_webView];
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.htmlStr]]];
    
    //监听UIWindow隐藏
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(endFullScreen) name:UIWindowDidBecomeHiddenNotification object:nil];
    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];

    [self setupBottonview];
}

// 根据监听 实时修改title
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"title"]) {
        if (object == self.webView)
        {
            self.title = self.webView.title;
        }
        else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
        
    }
    
}

//通知回调
-(void)endFullScreen{
    
    if (@available(iOS 11.0, *)) {
        NSLog(@"退出全屏");
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        self.navigationController.navigationBar.frame = CGRectMake(0, -44-FWCustomeSafeTop, SCREEN_WIDTH, 44+FWCustomeSafeTop);//矫正导航栏移位
    #pragma clang diagnostic pop
    }
}

- (void)shareButtonClick{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        ShareView *shareView = [[ShareView defaultShareView] initViewWtihNumber:2 Delegate:self WithViewController:self WithType:2];
        [shareView showView];
    }
}

#pragma mark - > 分享代理
- (void)shareAtIndex:(NSInteger)index{
    
    FWFeedListModel * model = [[FWFeedListModel alloc] init];

    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        if (index == 0){
            model.feed_id = @"other";
            model.share_url = self.infodict[@"share_url"];
            model.share_desc = self.infodict[@"share_desc"];
            model.share_title = self.infodict[@"share_title"];
            model.feed_cover = self.infodict[@"share_img"];
            
            [[ShareManager defaultShareManager] sendWXWithModel:model WithType:0];
        }else if (index == 1){
            
            model.feed_id = @"other";
            model.share_url = self.infodict[@"share_url"];
            model.share_desc = self.infodict[@"share_desc"];
            model.share_title = self.infodict[@"share_title"];
            model.feed_cover = self.infodict[@"share_img"];
            
            [[ShareManager defaultShareManager] sendWXWithModel:model WithType:1];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self];
    }
}

- (void)setupBottonview{
    
    UIView * bottonview = [[UIView alloc] init];
    bottonview.backgroundColor = FWColorWihtAlpha(@"000000", 0.8);
    [self.view addSubview:bottonview];
    [bottonview mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 65+FWSafeBottom));
    }];
    
    UIButton * joinButton = [[UIButton alloc] init];
    joinButton.layer.borderColor = FWViewBackgroundColor_FFFFFF.CGColor;
    joinButton.layer.borderWidth = 0.5;
    joinButton.layer.cornerRadius = 2;
    joinButton.layer.masksToBounds = YES;
    joinButton.titleLabel.font = DHSystemFontOfSize_17;
    [joinButton setTitle:@"立即加入" forState:UIControlStateNormal];
    [joinButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [joinButton addTarget:self action:@selector(joinButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [bottonview addSubview:joinButton];
    [joinButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bottonview).mas_offset(13);
        make.centerX.mas_equalTo(bottonview);
        make.size.mas_equalTo(CGSizeMake(119, 40));
    }];
}

- (void)joinButtonClick{
    
    if (self.showBottom) {
        FWCommitCarPlayerInfoViewController * CCPIVC = [[FWCommitCarPlayerInfoViewController alloc] init];
        [self.navigationController pushViewController:CCPIVC animated:YES];
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"您已加入该计划，可在个人中心查看详情" toController:self];
    }
}

// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

//提交发生错误时调用
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

// 接收到服务器跳转请求即服务重定向时之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
}

//进程被终止时调用
- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{
}
@end
