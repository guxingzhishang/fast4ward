//
//  FWADArearViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/8.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"
#import "FWPerfectInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWADArearView : UIScrollView

@property (nonatomic, strong) UIView * mainView;
@property (nonatomic, strong) NSMutableArray * selectedArray;
@property (nonatomic, strong) NSMutableArray * list_ad_position;

- (void)configForView:(id)model  withSelctArray:(NSMutableArray *)array;

@end


@interface FWADArearViewController : FWWebViewController

typedef void(^arearBlock)(NSMutableArray * selectIDArray,NSString * selectTitle);

@property (nonatomic, copy) arearBlock myBlock;

@property (nonatomic, strong) NSMutableArray<FWPerfectInfoAdPositionModel *> * list_ad_position;
@property (nonatomic, strong) NSMutableArray * selectIDArray;
@property (nonatomic, strong) NSString * selectTitle;

@end

NS_ASSUME_NONNULL_END

