//
//  FWJoinADPlanWebViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/9.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWWebViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWJoinADPlanWebViewController : FWBaseViewController

@property (nonatomic, assign) BOOL showBottom;
@property (nonatomic, strong) NSDictionary * infodict;
@property (nonatomic, strong) NSString *htmlStr;
@property (nonatomic, copy)  NSString * webTitle;

@end

NS_ASSUME_NONNULL_END
