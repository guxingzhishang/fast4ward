//
//  FWCarPlayerADPlanViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/6.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarPlayerADPlanViewController.h"
#import "FWCarPlayerCenterViewController.h"
#import "FWJoinADPlanWebViewController.h"
#import "FWMySignupViewController.h"

@implementation FWCarPlayerADPlanCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupSubViews];
    }
    
    return self;
}

- (void)setupSubViews{
    
    self.shadowView = [[UIView alloc] init];
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.shadowView.userInteractionEnabled = YES;
    self.shadowView.layer.cornerRadius = 10;
    self.shadowView.layer.shadowOffset = CGSizeMake(0, 0);
    self.shadowView.layer.shadowOpacity = 0.7;
    self.shadowView.layer.shadowColor = FWColor(@"D9D9D9").CGColor;
    [self.contentView addSubview:self.shadowView];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).mas_offset(10);
        make.left.mas_equalTo(self.contentView).mas_offset(14);
        make.right.mas_equalTo(self.contentView).mas_offset(-14);
        make.bottom.mas_equalTo(self.contentView).mas_offset(0);
        make.height.mas_equalTo(160);
        make.width.mas_equalTo(SCREEN_WIDTH-28);
    }];
    
    self.bgView = [[UIView alloc] init];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    [self.shadowView addSubview:self.bgView];
    [self.bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.shadowView).mas_offset(2);
        make.left.right.bottom.mas_equalTo(self.shadowView);
    }];
    
    self.planLabel = [[UILabel alloc] init];
    self.planLabel.font = DHSystemFontOfSize_17;
    self.planLabel.textColor = FWTextColor_272727;
    self.planLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.planLabel];
    [self.planLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView).mas_offset(13);
        make.top.mas_equalTo(self.bgView).mas_offset(10);
        make.height.mas_equalTo(22);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH-28-100);
    }];
    
    self.detailButton = [[UIButton alloc] init];
    [self.detailButton setImage:[UIImage imageNamed:@"adplan_detail"] forState:UIControlStateNormal];
    [self.detailButton addTarget:self action:@selector(detailImageViewClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.detailButton];
    [self.detailButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.planLabel);
        make.height.mas_equalTo(36);
        make.width.mas_equalTo(36);
        make.left.mas_equalTo(self.planLabel.mas_right).mas_offset(0);
    }];

    self.statusLabel = [[UILabel alloc] init];
    self.statusLabel.font = DHSystemFontOfSize_14;
    self.statusLabel.textColor = FWColor(@"1FB62B");
    self.statusLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.statusLabel];
    [self.statusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgView).mas_offset(-10);
        make.size.mas_equalTo(CGSizeMake(45, 13));
        make.centerY.mas_equalTo(self.planLabel);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = DHSystemFontOfSize_15;
    self.nameLabel.textColor = FWTextColor_9EA3AB;
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.planLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-34, 20));
        make.top.mas_equalTo(self.planLabel.mas_bottom).mas_offset(14);
    }];
    
    self.idLabel = [[UILabel alloc] init];
    self.idLabel.font = DHSystemFontOfSize_15;
    self.idLabel.textColor = FWTextColor_9EA3AB;
    self.idLabel.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:self.idLabel];
    [self.idLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.planLabel);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-34, 20));
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(5);
    }];
    
    self.signoutButton = [[UIButton alloc] init];
    self.signoutButton.titleLabel.font = DHSystemFontOfSize_15;
    self.signoutButton.backgroundColor = FWViewBackgroundColor_FFFFFF;
    self.signoutButton.layer.borderColor = FWColor(@"222222").CGColor;
    self.signoutButton.layer.borderWidth = 1;
    self.signoutButton.layer.cornerRadius = 2;
    self.signoutButton.layer.masksToBounds = YES;
    [self.signoutButton setTitle:@"退出计划" forState:UIControlStateNormal];
    [self.signoutButton setTitleColor:FWColor(@"222222") forState:UIControlStateNormal];
    [self.signoutButton addTarget:self action:@selector(signoutButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.signoutButton];
    [self.signoutButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.bgView).mas_offset(-(47/2+60));
        make.size.mas_equalTo(CGSizeMake(120, 31));
        make.top.mas_equalTo(self.idLabel.mas_bottom).mas_offset(16);
    }];
    
    self.protocolButton = [[UIButton alloc] init];
    self.protocolButton.titleLabel.font = DHSystemFontOfSize_15;
    self.protocolButton.backgroundColor = FWColor(@"222222");
    self.protocolButton.layer.cornerRadius = 2;
    self.protocolButton.layer.masksToBounds = YES;
    [self.protocolButton setTitle:@"查看协议" forState:UIControlStateNormal];
    [self.protocolButton setTitleColor:FWViewBackgroundColor_FFFFFF forState:UIControlStateNormal];
    [self.protocolButton addTarget:self action:@selector(protocolButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.protocolButton];
    [self.protocolButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.signoutButton.mas_right).mas_offset(47);
        make.size.mas_equalTo(self.signoutButton);
        make.centerY.mas_equalTo(self.signoutButton);
    }];
}

- (void)configForCell:(id)model{
    
    self.infoDict = (NSDictionary *)model;
    
    if ([self.infoDict[@"join_status"] isEqualToString:@"1"]) {
            if (self.infoDict[@"top_tip"]) {
            self.planLabel.text = self.infoDict[@"top_tip"];
        }
        
        if (self.infoDict[@"msg"]) {
            self.statusLabel.text = self.infoDict[@"msg"];
        }
        
        if (self.infoDict[@"real_name"]) {
            self.nameLabel.text = [NSString stringWithFormat:@"姓名：%@",self.infoDict[@"real_name"]];
        }else{
            self.nameLabel.text = @"姓名：";
        }
        
        if (self.infoDict[@"idcard"]) {
            self.idLabel.text = [NSString stringWithFormat:@"证件号：%@",self.infoDict[@"idcard"]];
        } else {
            self.idLabel.text = @"证件号：";
        }
    }
}

#pragma mark - > 广告详情H5
- (void)detailImageViewClick{
    
    FWJoinADPlanWebViewController * WVC = [[FWJoinADPlanWebViewController alloc] init];
    WVC.showBottom = NO;
    WVC.htmlStr = self.infoDict[@"h5_plan"];
    WVC.infodict = self.infoDict;
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

#pragma mark - > 退出计划
- (void)signoutButtonOnClick{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"" message:self.infoDict[@"quit_tip"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确认退出" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
        
        NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
        
        [request startWithParameters:params WithAction:Submit_quit_driver_plan WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                [[FWHudManager sharedManager] showErrorMessage:@"已退出广告车招募计划" toController:self.vc];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    for (UIViewController * item in self.vc.navigationController.viewControllers) {
                        if ([item isKindOfClass:[FWCarPlayerCenterViewController class]]) {
                            [self.vc.navigationController popToViewController:item animated:YES];
                            return ;
                        }
                    }
                    
                    for (UIViewController * item in self.vc.navigationController.viewControllers) {
                        if ([item isKindOfClass:[FWMySignupViewController class]]) {
                            [self.vc.navigationController popToViewController:item animated:YES];
                            return ;
                        }
                    }
                    [self.vc.navigationController popViewControllerAnimated:YES];
                });
            }else{
                [[FWHudManager sharedManager] showErrorMessage:@"操作失败，请稍后再试" toController:self.vc];
            }
        }];
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"我再想想" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self.vc presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - > 查看协议
- (void)protocolButtonOnClick{
    
    FWWebViewController * WVC = [[FWWebViewController alloc] init];
    WVC.pageType = WebViewTypeURL;
    WVC.htmlStr = self.infoDict[@"h5_agreement"];
    [self.vc.navigationController pushViewController:WVC animated:YES];
}

@end



@interface FWCarPlayerADPlanViewController ()<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>

@property (nonatomic, strong) UITableView * infoTableView;
@property (nonatomic, strong) NSDictionary * infoDict;
@property (nonatomic, strong) UIWebView * detailView;

@end

@implementation FWCarPlayerADPlanViewController
@synthesize infoTableView;


- (void)requestDriverCenter{
    
    FWNetworkRequest * request = [[FWNetworkRequest alloc] init];
    
    NSDictionary * params = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID]};
    
    [request startWithParameters:params WithAction:Get_driver_center WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            self.infoDict = [back objectForKey:@"data"];
            
            if ([self.infoDict[@"join_status"] isEqualToString:@"1"]) {
                self.detailView.hidden = YES;
                self.infoTableView.hidden = NO;
                [self.infoTableView reloadData];
            }else{
                self.infoTableView.hidden = YES;
                self.detailView.hidden = NO;

                NSString *htmlStr = self.infoDict[@"h5_plan"];
                htmlStr = [htmlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:htmlStr]];
                [self.detailView loadRequest:request];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self];
        }
    }];
}

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"广告车招募计划页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"广告车招募计划页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.infoDict = @{};
    self.title = @"广告车招募计划";
    
    [self setupSubViews];
    [self requestDriverCenter];
}

- (void)setupSubViews{
    
    self.detailView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64-FWCustomeSafeTop)];
    self.detailView.delegate = self;
    self.detailView.dataDetectorTypes = UIDataDetectorTypeNone;//关闭自动数字链接
    [self.view addSubview:self.detailView];
    self.detailView.hidden = YES;
    
    infoTableView = [[UITableView alloc] init];
    infoTableView.delegate = self;
    infoTableView.dataSource = self;
    infoTableView.rowHeight = 170;
    infoTableView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:infoTableView];
    [infoTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    self.infoTableView.hidden = YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * cellID = @"FWCarPlayerADPlanCellID";
    
    FWCarPlayerADPlanCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[FWCarPlayerADPlanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.vc = self;
    [cell configForCell:self.infoDict];
    
    return cell;
}

- (void)backBtnClick{
    
    for (UIViewController * item in self.navigationController.viewControllers) {
        if ([item isKindOfClass:[FWCarPlayerCenterViewController class]]) {
            [self.navigationController popToViewController:item animated:YES];
            return ;
        }
    }
    
    
    for (UIViewController * item in self.navigationController.viewControllers) {
        if ([item isKindOfClass:[FWMySignupViewController class]]) {
            [self.navigationController popToViewController:item animated:YES];
            return ;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}
@end
