//
//  FWADArearViewController.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/8.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWADArearViewController.h"

@interface FWADArearView ()
@end

@implementation FWADArearView

- (id)init{
    self = [super init];
    if (self) {

        self.list_ad_position = @[].mutableCopy;
    }

    return self;
}


- (void)configForView:(id)model  withSelctArray:(nonnull NSMutableArray *)array{

    self.list_ad_position = (NSMutableArray *)model;
    self.selectedArray = array;
    
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }

    NSInteger count = self.list_ad_position.count;
    NSInteger lastRow = count/4; // 最后一行

    for (int i = 0; i < count; i++) {
        
        FWPerfectInfoAdPositionModel * positionModel = self.list_ad_position[i];
        
        CGFloat itemWidth = (SCREEN_WIDTH-20)/4;

        NSInteger row = i / 4;
        NSInteger lie = i % 4;
        
        UIButton * item = [[UIButton alloc] init];
        item.userInteractionEnabled = YES;
        item.tag = 10010+i;
        item.frame = CGRectMake(10+ itemWidth*lie, 30 * row, itemWidth, 30);
        [item addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:item];
        [item mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).mas_offset(10+ itemWidth*lie);
            make.size.mas_equalTo(CGSizeMake(itemWidth, 30));
            make.top.mas_equalTo(self).mas_offset(30 * row);

            if (lastRow == row) {
                make.bottom.mas_equalTo(self).mas_offset(-10);
            }
        }];
        
        UIImageView * selectImageView = [[UIImageView alloc] init];
        selectImageView.tag = 10110+i;
        selectImageView.image = [UIImage imageNamed:@"radio_save_unsel"];
        [item addSubview:selectImageView];
        [selectImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.centerY.mas_equalTo(item);
            make.left.mas_equalTo(item).mas_offset(3);
        }];

        UILabel * nameLabel = [[UILabel alloc] init];
        nameLabel.font = DHSystemFontOfSize_14;
        nameLabel.textColor = FWTextColor_222222;
        nameLabel.text = positionModel.ad_position_val;
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [item addSubview:nameLabel];
        [nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(selectImageView.mas_right).mas_offset(5);
            make.height.mas_equalTo(30);
            make.right.mas_equalTo(item).mas_offset(0);
            make.width.mas_greaterThanOrEqualTo(20);
            make.bottom.top.mas_equalTo(item);
        }];
        
        if ([array containsObject:positionModel.ad_position_id]) {
            item.selected = YES;
            selectImageView.image = [UIImage imageNamed:@"radio_save_sel"];
        }
    }
}

- (void)itemClick:(UIButton *)sender{
    
    NSInteger index = sender.tag - 10010;
    
    FWPerfectInfoAdPositionModel * positionModel = self.list_ad_position[index];

    sender.selected = !sender.selected;
    
    if (sender.isSelected && ![self.selectedArray containsObject:positionModel.ad_position_id]) {
        [self.selectedArray addObject:positionModel.ad_position_id];
    }
    
    if (!sender.isSelected && [self.selectedArray containsObject:positionModel.ad_position_id]) {
        [self.selectedArray removeObject:positionModel.ad_position_id];
    }
    
    UIImageView * selectImageView = (UIImageView *)[sender viewWithTag:10110 + index];
    if (sender.isSelected) {
        selectImageView.image = [UIImage imageNamed:@"radio_save_sel"];
    }else{
        selectImageView.image = [UIImage imageNamed:@"radio_save_unsel"];
    }
}

@end

@interface FWADArearViewController ()

@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) FWADArearView * arearView;
@property (nonatomic, strong) NSString * pre_selectTitle;
@property (nonatomic, strong) NSMutableArray * pre_adposition;//(广告位置页面，如果直接返回，会清空数组，所以预留一份已选，在下次进入下一个页面时赋值)

@end

@implementation FWADArearViewController

#pragma mark - > 生命周期
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackPageBegin:@"广告位区域页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"广告位区域页"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pre_adposition = @[].mutableCopy;
    self.fd_interactivePopDisabled = YES;
    
    UIButton * finishButton = [[UIButton alloc] init];
    finishButton.frame = CGRectMake(0, 0, 45, 40);
    finishButton.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15];
    [finishButton setTitle:@"完成" forState:UIControlStateNormal];
    [finishButton setTitleColor:FWTextColor_272727 forState:UIControlStateNormal];
    [finishButton addTarget:self action:@selector(finishButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:finishButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
    
    [self setupBottomView];
}

- (void)finishButtonClick{
    
    [self.arearView.selectedArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }];

    NSMutableArray * tempArr = @[].mutableCopy;
    
    for (int i = 0; i < self.arearView.selectedArray.count; i++) {
        NSString * selectID = self.arearView.selectedArray[i];

        for(int j = 0;j <self.list_ad_position.count;j ++){
            FWPerfectInfoAdPositionModel * model = self.list_ad_position[j];
            
            if ([selectID isEqualToString:model.ad_position_id]) {
                [tempArr addObject:model.ad_position_val];
                continue;
            }
        }
    }
    
    self.selectTitle = [tempArr componentsJoinedByString:@","];
    
    self.myBlock(self.arearView.selectedArray, self.selectTitle);
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backBtnClick{
    
    self.myBlock(self.pre_adposition, self.pre_selectTitle);
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupBottomView{

    self.bottomView = [[UIView alloc] init];
    self.bottomView.userInteractionEnabled = YES;
    self.bottomView.backgroundColor = FWColorWihtAlpha(@"ffffff", 0.95);
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 180+FWSafeBottom));
    }];

    UILabel * topLabel = [[UILabel alloc] init];
    topLabel.text = @"请选择可使用广告位置";
    topLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH, 30);
    topLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size: 15.2];
    topLabel.textColor = FWTextColor_222222;
    topLabel.textAlignment = NSTextAlignmentCenter;
    [self.bottomView addSubview:topLabel];
    [topLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.bottomView);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 30));
    }];

    self.arearView = [[FWADArearView alloc] init];
    [self.bottomView addSubview:self.arearView];
    [self.arearView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.bottomView);
        make.bottom.mas_equalTo(self.bottomView);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(150+FWSafeBottom);
    }];

    self.pre_selectTitle = self.selectTitle;
    self.pre_adposition = [self.selectIDArray mutableCopy];
    [self.arearView configForView:self.list_ad_position withSelctArray:self.selectIDArray];
}

@end
