//
//  FWMineViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/22.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 我的 
 */
#import "FWBaseViewController.h"
#import "FWScrollViewFollowCollectionView.h"
#import "FWMainTouchScrollView.h"

@interface FWMineViewController : FWBaseViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>
/**
 * 返回按钮
 */
@property (nonatomic, strong) UIButton * backButton;

/**
 * 转发按钮
 */
@property (nonatomic, strong) UIButton * shareButton;

/**
 * 设置按钮
 */
@property (nonatomic, strong) UIButton * settingButton;

@property(nonatomic, strong) NSArray *modelArr;

@end
