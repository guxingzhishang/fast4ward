//
//  FWCarDetailViewController.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseViewController.h"

typedef NS_ENUM(NSUInteger, FWCarDetailType) {
    FWCarPicType = 1,// 图片
    FWCarGaizhuangType,  // 改装清单
    FWCarTongxiType,   // 同系车友
};

NS_ASSUME_NONNULL_BEGIN

@interface FWCarDetailViewController : FWBaseViewController<UIScrollViewDelegate>

@property (nonatomic, strong) FWCarListModel * listModel;
@property (nonatomic, strong) NSString * user_car_id;

@property (nonatomic, strong) UITableView * picTableView;
@property (nonatomic, strong) UITableView * gaizhuangTableView;
@property (nonatomic, strong) UITableView * tongxiTableView;

@property (nonatomic, assign) FWCarDetailType currentType;


@end

NS_ASSUME_NONNULL_END
