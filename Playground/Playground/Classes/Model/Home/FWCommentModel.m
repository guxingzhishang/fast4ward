//
//  FWCommentModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWCommentModel.h"

@implementation FWCommentListModel

- (NSAttributedString *)attributedText
{
    if (self.content == nil) return nil;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.content];
    attributedString.yy_font = [UIFont systemFontOfSize:FWPxConvertPt(15.0f)];
//    if ([self.feed_type isEqualToString:@"1"]) {
        if ([self.fromMessage isEqualToString:@"1"]) {
            // 如果是从视频播放页来的，文字颜色为白色
            attributedString.yy_color = FWViewBackgroundColor_FFFFFF;
        }else{
            // 如果是从其他页过来，文字颜色为黑色
            attributedString.yy_color = FWTextColor_222222;
        }
//    }else{
//        attributedString.yy_color = FWTextColor_222222;
//    }
    
    attributedString.yy_lineSpacing = 10.f;
    return attributedString;
}

+ (NSDictionary *)mj_objectClassInArray{
    return @{@"reply_list":NSStringFromClass([FWReplyComment class])};
}

@end

@implementation FWCommentModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{@"comment_list":NSStringFromClass([FWCommentListModel class])};
}

@end
