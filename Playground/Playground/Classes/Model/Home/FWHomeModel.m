//
//  FWHomeModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/11.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWHomeModel.h"

@implementation FWHomeCoverListModel
@end

@implementation FWHomeShopListModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"imgs":NSStringFromClass([FWHomeCoverListModel class]),
             };
}
@end

@implementation FWHomeShopCityModel
@end

@implementation FWHomeModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"shop_city":NSStringFromClass([FWHomeShopCityModel class]),
             @"shop_list":NSStringFromClass([FWHomeShopListModel class]),
             };
}
@end
