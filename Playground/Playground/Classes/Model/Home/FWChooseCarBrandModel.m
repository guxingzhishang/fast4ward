//
//  FWChooseCarBrandModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWChooseCarBrandModel.h"

@implementation FWChooseCarBrandSubListModel
@end

@implementation FWChooseCarBrandListModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWChooseCarBrandSubListModel class]),
    };
}
@end

@implementation FWChooseCarBrandModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWChooseCarBrandListModel class]),
    };
}
@end
