//
//  FWRefitCaseModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRefitCaseModel.h"

@implementation FWRefitCaseCategoryInfoModel
@end


@implementation FWRefitCaseTagListModel
@end

@implementation FWRefitCaseCarTypeInfoModel
@end

@implementation FWRefitCaseTagInfoInfoModel
@end

@implementation FWRefitCaseModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list_tag" : NSStringFromClass([FWRefitCaseTagListModel class]),
        @"feed_list" : NSStringFromClass([FWFeedListModel class]),
    };
}
@end
