//
//  FWReplyComment.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWReplyComment.h"

@implementation FWReplyComment

//FWAllCommentsId
- (NSAttributedString *)attributedText
{
    if ([self.comment_id isEqualToString:FWAllCommentsId]) {
        
        NSString *textString = [NSString stringWithFormat:@"%@",self.content];
        
        NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
        mutableAttributedString.yy_font = DHSystemFontOfSize_12;
        mutableAttributedString.yy_color = FWTextColor_969696;
        mutableAttributedString.yy_lineSpacing = MHCommentContentLineSpacing;
        return mutableAttributedString;
    }
    
    if (!FWObjectIsNil(self.reply_user_info.nickname) &&
        [self.is_reply isEqualToString:@"1"]) {
        // 有回复
        NSString *textString = [NSString stringWithFormat:@"回复: %@ %@",  self.reply_user_info.nickname, self.content];;
        NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
        mutableAttributedString.yy_font = [UIFont systemFontOfSize:FWPxConvertPt(13.0f)];
        if ([self.fromMessage isEqualToString:@"1"]) {
            // 如果是从视频播放页来的，文字颜色为白色
            mutableAttributedString.yy_color = FWViewBackgroundColor_FFFFFF;
        }else{
            // 如果是从其他页过来，文字颜色为黑色
            mutableAttributedString.yy_color = FWTextColor_222222;
        }
        mutableAttributedString.yy_lineSpacing = MHCommentContentLineSpacing;
    
        NSRange toUserRange = [textString rangeOfString:[NSString stringWithFormat:@" %@ ",self.reply_user_info.nickname]];
        // 文本高亮模型
        YYTextHighlight *toUserHighlight = [YYTextHighlight highlightWithBackgroundColor:[UIColor colorWithWhite:0.000 alpha:0.220]];
        // 这里痛过属性的userInfo保存User模型，后期通过获取模型然后获取User模型
        toUserHighlight.userInfo = @{FWCommentUserKey:self.reply_user_info};
        
        [mutableAttributedString yy_setTextHighlight:toUserHighlight range:toUserRange];
        [mutableAttributedString yy_setColor:FWOrange range:toUserRange];
        
        return mutableAttributedString;
    }else{
        
        // 没有回复
        NSString *textString = [NSString stringWithFormat:@"%@",self.content];
        NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
        mutableAttributedString.yy_font = [UIFont systemFontOfSize:FWPxConvertPt(13.0f)];
        if ([self.fromMessage isEqualToString:@"1"]) {
            // 如果是从视频播放页来的，文字颜色为白色
            mutableAttributedString.yy_color = FWViewBackgroundColor_FFFFFF;
        }else{
            // 如果是从其他页过来，文字颜色为白色
            mutableAttributedString.yy_color = FWTextColor_222222;
        }
        mutableAttributedString.yy_lineSpacing = MHCommentContentLineSpacing;
        
        return mutableAttributedString;
    }
    
    return nil;
}
@end
