//
//  FWChooseCarTypeModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface FWChooseCarTypeListModel : NSObject
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSMutableArray<NSString *> * list;
@end

@interface FWChooseCarTypeModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWChooseCarTypeListModel *> * list;
@property (nonatomic, strong) NSString * brand;
@end



NS_ASSUME_NONNULL_END
