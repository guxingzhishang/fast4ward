//
//  FWIdleModel.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/17.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWIdleListModel : NSObject
@property (nonatomic, strong) NSString * category_id;
@property (nonatomic, strong) NSString * category_name;
@property (nonatomic, strong) NSString * img;
@end

@interface FWIdleModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWIdleListModel *> * list;

@end

NS_ASSUME_NONNULL_END
