//
//  FWRefrofitFeelingModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRefrofitFeelingModel.h"


@implementation FWRefrofitFeelingCatListModel

@end

@implementation FWRefrofitFeelingTagListModel

@end

@implementation FWRefrofitFeelingModel

+ (NSDictionary *)mj_objectClassInArray{

    return @{
             @"list_category":NSStringFromClass([FWRefrofitFeelingCatListModel class]),
             @"list_tag":NSStringFromClass([FWRefrofitFeelingTagListModel class]),
             @"list_hot_tag":NSStringFromClass([FWRefrofitFeelingTagListModel class]),
             };
}

@end
