//
//  FWTopicCommentFrame.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWTopicCommentFrame.h"

@interface FWTopicCommentFrame ()
/** 头像frame */
@property (nonatomic , assign) CGRect avatarFrame;

/** 昵称frame */
@property (nonatomic , assign) CGRect nicknameFrame;

/** 点赞frame */
@property (nonatomic , assign) CGRect thumbFrame;

/** 点赞图标frame */
@property (nonatomic , assign) CGRect thumbImageFrame;

/** 点赞数frame */
@property (nonatomic , assign) CGRect thumbNumberFrame;

/** 时间frame */
@property (nonatomic , assign) CGRect createTimeFrame;

/** 话题内容frame */
@property (nonatomic , assign) CGRect textFrame;

/** height*/
@property (nonatomic , assign) CGFloat height;

/** tableViewFrame cell嵌套tableView用到 本人有点懒 ，公用了一套模型 */
@property (nonatomic , assign ) CGRect tableViewFrame;

@end

@implementation FWTopicCommentFrame

- (instancetype)initWithType:(NSString *)commentType
{
    self = [super init];
    if (self) {
        // 初始化
        self.commentType = commentType;
        _commentFrames = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Setter
- (void)setTopic:(FWCommentListModel *)topic
{
    _topic = topic;
    
    // 整个宽度
    CGFloat width = SCREEN_WIDTH;
    
    // 头像
    CGFloat avatarX = MHTopicHorizontalSpace;
    CGFloat avatarY = MHTopicVerticalSpace;
    CGFloat avatarW = MHTopicAvatarWH;
    CGFloat avatarH = MHTopicAvatarWH;
    self.avatarFrame = (CGRect){{avatarX , avatarY},{avatarW , avatarH}};
    
    // 布局点赞按钮
    CGFloat thumbW = 40;
    CGFloat thumbX = width - thumbW;
    CGFloat thumbY = 0;
    CGFloat thumbH = 60;
    self.thumbFrame = CGRectMake(thumbX, thumbY, thumbW, thumbH);
    
    // 布局点赞图标
    CGFloat thumbImageX = (thumbW - 16)/2;
    CGFloat thumbImageY = avatarY;
    CGFloat thumbImageW = 16;
    CGFloat thumbImageH = 15;
    self.thumbImageFrame  = CGRectMake(thumbImageX, thumbImageY, thumbImageW, thumbImageH);
    
    // 布局点赞数
    CGFloat thumbNumberW = thumbW;
    CGFloat thumbNumberX = 0;
    CGFloat thumbNumberY = thumbImageY + thumbImageH+5;
    CGFloat thumbNumberH = 20;
    self.thumbNumberFrame = CGRectMake(thumbNumberX, thumbNumberY, thumbNumberW, thumbNumberH);
    
    // 昵称
    CGFloat nicknameX = CGRectGetMaxX(self.avatarFrame)+MHTopicHorizontalSpace;
    CGFloat nicknameY = avatarY;
    CGFloat nicknameW = CGRectGetMinX(self.thumbFrame) - nicknameX;
    CGFloat nicknameH = 20;
    self.nicknameFrame = CGRectMake(nicknameX, nicknameY, nicknameW, nicknameH);
    
    // 时间
    CGFloat createX = nicknameX;
    CGFloat createY = CGRectGetMaxY(self.nicknameFrame)+5;
    CGFloat createW = width - createX-thumbW-15;
    CGFloat createH = 15;
    self.createTimeFrame = CGRectMake(createX, createY, createW, createH);
    
    // 内容
    CGFloat textX = nicknameX;
    CGSize textLimitSize = CGSizeMake(width - textX - MHTopicHorizontalSpace, MAXFLOAT);
    CGFloat textY = CGRectGetMaxY(self.createTimeFrame)+10;
    CGFloat textH = [YYTextLayout layoutWithContainerSize:textLimitSize text:topic.attributedText].textBoundingSize.height+MHTopicVerticalSpace+MHTopicVerticalSpace;
    
    self.textFrame = (CGRect){{textX , textY} , {textLimitSize.width, textH}};
    
    
    CGFloat tableViewX = textX;
    CGFloat tableViewY = CGRectGetMaxY(self.textFrame);
    CGFloat tableViewW = textLimitSize.width;
    CGFloat tableViewH = 0;
    // 评论数据
    if (topic.reply_list>0)
    {
        for (FWReplyComment *comment in topic.reply_list)
        {
            FWCommentFrame *commentFrame = [[FWCommentFrame alloc] init];
            if ([self.commentType isEqualToString:@"1"]) {
                
                commentFrame.maxW = textLimitSize.width;
            }else if ([self.commentType isEqualToString:@"2"]){
                
                commentFrame.maxW = width;
            }
            commentFrame.comment = comment;
            [self.commentFrames addObject:commentFrame];
            
            tableViewH += commentFrame.cellHeight;
        }
    }
    
    self.tableViewFrame = CGRectMake(tableViewX, tableViewY, tableViewW, tableViewH);
    // 自身高度
    self.height = CGRectGetMaxY(self.textFrame);
}


@end
