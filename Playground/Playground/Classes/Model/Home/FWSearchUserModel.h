//
//  FWSearchUserModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/20.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWSearchUserModel : NSObject

@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * query;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSMutableArray<FWMineInfoModel *> * user_list;

@end

NS_ASSUME_NONNULL_END
