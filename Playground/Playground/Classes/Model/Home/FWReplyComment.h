//
//  FWReplyComment.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  strongright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWReplyComment : NSObject

@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * uniq_hash;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * is_liked;
@property (nonatomic, strong) NSString * format_create_time;
@property (nonatomic, strong) NSString * msg_status;
@property (nonatomic, strong) NSString * feed_id;
@property (nonatomic, strong) NSString * feed_type;
@property (nonatomic, strong) NSString * check_status;
@property (nonatomic, strong) NSString * check_time;
@property (nonatomic, strong) NSString * like_count;
@property (nonatomic, strong) NSString * like_count_format;
@property (nonatomic, strong) NSString * comment_id;
@property (nonatomic, strong) NSString * top_comment_id;
@property (nonatomic, strong) NSString * check_misuid;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * p_comment_id;
@property (nonatomic, strong) NSString * reply_comment_id;
@property (nonatomic, strong) NSString * reply_comment_uid;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSString * is_reply;
@property (nonatomic, strong) NSString * fromMessage;//自定义属性，从哪个页面来

@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) FWMineInfoModel * reply_user_info;

/** 富文本 */
- (NSAttributedString *)attributedText;

@end

NS_ASSUME_NONNULL_END
