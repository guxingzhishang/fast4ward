//
//  FWCommentFrame.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWCommentFrame.h"


@interface FWCommentFrame ()
/** 头像frame */
@property (nonatomic , assign) CGRect avatarFrame;

/** 昵称frame */
@property (nonatomic , assign) CGRect nicknameFrame;

/** 点赞frame */
@property (nonatomic , assign) CGRect thumbFrame;

/** 点赞图标frame */
@property (nonatomic , assign) CGRect thumbImageFrame;

/** 点赞数frame */
@property (nonatomic , assign) CGRect thumbNumberFrame;

/** 时间frame */
@property (nonatomic , assign) CGRect createTimeFrame;

/** 内容尺寸 */
@property (nonatomic , assign) CGRect textFrame;

/** cell高度 */
@property (nonatomic , assign) CGFloat cellHeight;

@end


@implementation FWCommentFrame

#pragma mark - Setter

- (void)setComment:(FWReplyComment *)comment
{
    _comment = comment;
    
    // 整个宽度
    CGFloat width = self.maxW;
    
    // 头像
    CGFloat avatarX = MHTopicHorizontalSpace;
    CGFloat avatarY = MHTopicVerticalSpace;
    CGFloat avatarW = MHTopicAvatarWH-6;
    CGFloat avatarH = MHTopicAvatarWH-6;
    self.avatarFrame = (CGRect){{avatarX , avatarY},{avatarW , avatarH}};
    
    
    // 布局点赞按钮
    CGFloat thumbW = 40;
    CGFloat thumbX = width+10 - thumbW;
    CGFloat thumbY = 0;
    CGFloat thumbH = 60;
    self.thumbFrame = CGRectMake(thumbX, thumbY, thumbW, thumbH);
    
    // 布局点赞图标
    CGFloat thumbImageX = (thumbW - 16)/2;
    CGFloat thumbImageY = avatarY;
    CGFloat thumbImageW = 16;
    CGFloat thumbImageH = 15;
    self.thumbImageFrame  = CGRectMake(thumbImageX, thumbImageY, thumbImageW, thumbImageH);
    
    // 布局点赞数
    CGFloat thumbNumberW = thumbW;
    CGFloat thumbNumberX = 0;
    CGFloat thumbNumberY = thumbImageY + thumbImageH;
    CGFloat thumbNumberH = 20;
    self.thumbNumberFrame = CGRectMake(thumbNumberX, thumbNumberY, thumbNumberW, thumbNumberH);
    
    
    // 昵称
    CGFloat nicknameX = CGRectGetMaxX(self.avatarFrame)+MHTopicHorizontalSpace;
    CGFloat nicknameY = avatarY;
    CGFloat nicknameW = CGRectGetMinX(self.thumbFrame) - nicknameX;
    CGFloat nicknameH = 20;
    self.nicknameFrame = CGRectMake(nicknameX, nicknameY, nicknameW, nicknameH);
    
    // 时间
    CGFloat createX = nicknameX;
    CGFloat createY = CGRectGetMaxY(self.nicknameFrame);
    CGFloat createW = width - createX-thumbW-5;
    CGFloat createH = 15;
    self.createTimeFrame = CGRectMake(createX, createY, createW, createH);
    
    
    // 文本内容
    CGFloat textX = nicknameX;
    CGFloat textY = CGRectGetMaxY(self.createTimeFrame)+10;
    CGSize  textLimitSize = CGSizeMake(self.maxW - 20-textX, MAXFLOAT);
    CGFloat textH = [YYTextLayout layoutWithContainerSize:textLimitSize text:comment.attributedText].textBoundingSize.height;
    
//    self.textFrame = (CGRect){{textX , textY} , {textLimitSize.width , textH}};
    self.textFrame = CGRectMake(textX, textY, textLimitSize.width, textH);
    
    
    if ([comment.comment_id isEqualToString:FWAllCommentsId]) {
        // 如果是最后一个查看更多，返回固定高度
        self.avatarFrame = CGRectZero;
        self.thumbFrame = CGRectZero;
        self.thumbNumberFrame = CGRectZero;
        self.nicknameFrame = CGRectZero;
        self.createTimeFrame = CGRectZero;
        self.textFrame = CGRectMake(nicknameX, MHCommentHorizontalSpace, textLimitSize.width , textH);
        
        self.cellHeight = 30;
        return;
    }
    
    if (self.maxW == SCREEN_WIDTH) {
        // 评论详情页
        self.thumbFrame = CGRectMake(thumbX-10, thumbY, thumbW, thumbH);

        CGSize  textLimitSize = CGSizeMake(self.maxW - nicknameX-5, MAXFLOAT);
        CGFloat textH = [YYTextLayout layoutWithContainerSize:textLimitSize text:comment.attributedText].textBoundingSize.height;
//        self.textFrame = (CGRect){{nicknameX , textY} , {textLimitSize.width , textH}};
        self.textFrame = CGRectMake(nicknameX, textY, textLimitSize.width, textH);

    }
    
    
    self.cellHeight = CGRectGetMaxY(self.textFrame) + MHCommentVerticalSpace;
}

@end
