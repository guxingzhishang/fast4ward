//
//  FWFeedModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/11.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWSearchTagsListModel.h"
#import "FWMineModel.h"
#import "FWSearchTagsListModel.h"
#import "FWTagsFeedModel.h"
#import "FWShopModel.h"
#import "FWLiveInfoModel.h"


@interface FWInviteModel : NSObject
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * header_url;
@end

@interface FWHomeNoteCategrayModel : NSObject

@property (nonatomic, strong) NSString * cat_id;
@property (nonatomic, strong) NSString * cat_name;

@end

@interface FWHomeBannerModel : NSObject

@property (nonatomic, strong) NSString * banner_id;
@property (nonatomic, strong) NSString * banner_type;
@property (nonatomic, strong) NSString * banner_val;
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * img_url;
@property (nonatomic, strong) NSString * h5_url;

@end

@interface FWFormatContentModel : NSObject

@property (nonatomic, strong) NSString * size;
@property (nonatomic, strong) NSString * value;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * textnum;
@property (nonatomic, strong) NSString * disabled;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSString * src;
@property (nonatomic, strong) NSString * width;
@property (nonatomic, strong) NSString * height;

@end

@interface FWForumAlbumsModel : NSObject

@property (nonatomic, strong) NSString * pic;
@property (nonatomic, strong) NSString * height;
@property (nonatomic, strong) NSString * width;

@end


@interface FWTagModel : NSObject

@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * tag_name;

@end

@interface FWFeedCountRealtimeModel : NSObject

@property (nonatomic, strong) NSString * like_count;
@property (nonatomic, strong) NSString * like_count_format;
@property (nonatomic, strong) NSString * comment_count;
@property (nonatomic, strong) NSString * pv_count;
@property (nonatomic, strong) NSString * pv_count_format;
@property (nonatomic, strong) NSString * share_count;
@property (nonatomic, strong) NSString * comment_count_format;
@property (nonatomic, strong) NSString * share_count_format;

@end

@interface FWFeedImgsModel : NSObject

@property (nonatomic, strong) NSString * img_url;
@property (nonatomic, strong) NSString * img_wh;
@property (nonatomic, strong) NSString * img_width;
@property (nonatomic, strong) NSString * img_height;
@property (nonatomic, strong) NSString * img_url_nowatermark;

@end

@interface FWRefitJianyaoModel : NSObject
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * val;
@end

@interface FWRefitCarStyleModel : NSObject
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * style;
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSString * jinqi;
@property (nonatomic, strong) NSString * pailiang;
@property (nonatomic, strong) NSString * mali;
@property (nonatomic, strong) NSString * type;

@end

@interface FWFeedVideoModel: NSObject

@property (nonatomic, strong) NSString * playURL;
@property (nonatomic, strong) NSString * downloadURL;
@property (nonatomic, strong) NSString * definition;
@property (nonatomic, strong) NSString * format;
@property (nonatomic, strong) NSString * duration;

@end

@interface FWFeedListModel : NSObject

@property (nonatomic, assign) BOOL isShow;

@property (nonatomic, strong) NSString * is_favourited;
@property (nonatomic, assign) BOOL  is_horizontal;
@property (nonatomic, strong) NSString * is_good;//精品贴
@property (nonatomic, strong) NSString * draft_id;
@property (nonatomic, strong) NSString * video_id;
@property (nonatomic, strong) NSString * feed_id;
@property (nonatomic, strong) NSString * feed_id_encode;
@property (nonatomic, strong) NSString * flag_hidden;
@property (nonatomic, strong) NSString * flag_hidden_msg;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * h5_url;
@property (nonatomic, strong) NSString * flag_create;
@property (nonatomic, strong) NSString * feed_type;// 1:视频  2图文  3文章  4问答   5改装
@property (nonatomic, strong) NSString * is_long_video;// 1代表长视频，2为短视频
@property (nonatomic, strong) NSString * feed_title;
@property (nonatomic, strong) NSString * wenda_content;
@property (nonatomic, strong) NSString * comment_count;
@property (nonatomic, strong) NSString * share_count;
@property (nonatomic, strong) NSString * repost_count;
@property (nonatomic, strong) NSString * like_count;
@property (nonatomic, strong) NSString * editor_score;
@property (nonatomic, strong) NSString * pv_count;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * show_start_time;
@property (nonatomic, strong) NSString * show_end_time;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * check_status;
@property (nonatomic, strong) NSString * show_status;
@property (nonatomic, strong) NSString * detail_update_time;
@property (nonatomic, strong) NSString * pub_ip;
@property (nonatomic, strong) NSString * client_version;
@property (nonatomic, strong) NSString * pub_from;
@property (nonatomic, strong) NSString * device_type;
@property (nonatomic, strong) NSString * check_time;
@property (nonatomic, strong) NSString * allow_comment;
@property (nonatomic, strong) NSString * is_draft;
@property (nonatomic, strong) NSString * check_misuid;
@property (nonatomic, strong) NSString * alicheck_status;
@property (nonatomic, strong) NSString * list_order;
@property (nonatomic, strong) NSString * alicheck_info;
@property (nonatomic, strong) NSString * is_recommand;
@property (nonatomic, strong) NSString * data_from;
@property (nonatomic, strong) NSString * share_url;
@property (nonatomic, strong) NSString * share_desc;
@property (nonatomic, strong) NSString * share_title;
@property (nonatomic, strong) NSString * goods_id;
@property (nonatomic, strong) NSString * format_create_time;
@property (nonatomic, strong) NSString * is_liked;
@property (nonatomic, strong) NSString * xcx_qrcode;
@property (nonatomic, strong) NSString * is_followed;
@property (nonatomic, strong) NSString * feed_cover;
@property (nonatomic, strong) NSString * feed_cover_nowatermark;
@property (nonatomic, strong) NSString * feed_cover_original;
@property (nonatomic, strong) NSString * feed_cover_wh;
@property (nonatomic, strong) NSString * feed_cover_width;
@property (nonatomic, strong) NSString * feed_cover_height;
@property (nonatomic, strong) NSString * gaizhuang_list; // 改装案例的Json字符串
@property (nonatomic, strong) NSString * car_type;
@property (nonatomic, strong) NSString * status_niming;// 1 不匿名  2匿名

@property (nonatomic, strong) NSString * province_code;// 省份code
@property (nonatomic, strong) NSString * province_name;
@property (nonatomic, strong) NSString * city_code;// 城市code
@property (nonatomic, strong) NSString * city_name;
@property (nonatomic, strong) NSString * county_code;// 县code
@property (nonatomic, strong) NSString * county_name;
@property (nonatomic, strong) NSString * price_buy_show;// 原价（购买价）
@property (nonatomic, strong) NSString * price_sell_show;// 出售价
@property (nonatomic, strong) NSString * freight;// 运费，1按距离计算  2包邮
@property (nonatomic, strong) NSString * xianzhi_category_id;// 闲置分类id
@property (nonatomic, strong) NSString * xianzhi_category_name;// 闲置分类
@property (nonatomic, strong) NSString * if_quanxin;// 是否全新1是2否
@property (nonatomic, strong) NSString * if_ziti;// 是是否自提，1是2否

@property (nonatomic, strong) NSMutableArray<FWRefitJianyaoModel *> * gaizhuang_jianyao; // 改装简要
@property (nonatomic, strong) FWRefitCarStyleModel * car_style; // 原厂车型
@property (nonatomic, strong) NSString * gaizhuang_content; // 改装故事
@property (nonatomic, strong) NSMutableArray<NSString *> * gaizhuang_jianyao_show;// 改装简要组合好的
@property (nonatomic, strong) NSMutableArray<NSString *> * gaizhuang_list_show;// 改装简要组合好的
@property (nonatomic, strong) NSMutableArray<FWSearchTagsSubListModel *> * tags;
@property (nonatomic, strong) NSMutableArray<FWFeedImgsModel *> * imgs;
@property (nonatomic, strong) NSMutableArray<FWFeedImgsModel *> * imgs_original;
@property (nonatomic, strong) NSMutableArray<FWForumAlbumsModel *> * forum_albums;
@property (nonatomic, strong) NSMutableArray<FWFormatContentModel *> * format_content;
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) FWFeedVideoModel * video_info;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) FWBussinessShopGoodsListModel * goods_info;
@property (nonatomic, strong) FWFeedCountRealtimeModel * count_realtime;
@property (nonatomic, assign) BOOL didNotNeedFuckingFadeAnimateEffect;
@property (nonatomic, strong) NSMutableArray<FWInviteModel*> * list_invite_user;

@end

@interface FWHomeGaiZhuangListModel : NSObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * category_id;
@property (nonatomic, strong) NSString * img;

@end

@interface FWHomeQuestionTagListModel : NSObject

@property (nonatomic, strong) NSString * feed_count;
@property (nonatomic, strong) NSString * tag_name;
@property (nonatomic, strong) NSString * tag_id;

@end

@interface FWFeedModel : NSObject

@property (nonatomic, assign) BOOL isShow;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * total_count_video;
@property (nonatomic, strong) NSString * total_count_image;
@property (nonatomic, strong) NSString * total_count_wenda;
@property (nonatomic, strong) NSString * total_count_gaizhuang;
@property (nonatomic, strong) NSString * total_count_xianzhi;
@property (nonatomic, strong) NSString * feed_count_video;
@property (nonatomic, strong) NSString * feed_count_image;
@property (nonatomic, strong) NSString * feed_count_wenda;// 问答数
@property (nonatomic, strong) NSString * feed_count_gaizhuang; // 改装数
@property (nonatomic, strong) NSString * feed_count_xianzhi;

@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * follow_status;
@property (nonatomic, strong) NSString * page2_feed_id;
@property (nonatomic, strong) NSString * page3;

@property (nonatomic, strong) NSString * h5_show_status; // 1为展示 2为隐藏
@property (nonatomic, strong) NSString * h5_img_url;
@property (nonatomic, strong) NSString * h5_href_url;

@property (nonatomic, strong) NSString * xianzhi_category_img;
@property (nonatomic, strong) NSString * xianzhi_category_id;
@property (nonatomic, strong) NSString * xianzhi_category_name;

@property (nonatomic, strong) NSString * live_status; // 1未开始 2直播中 3已结束
@property (nonatomic, strong) NSString * zhibo_status; // 1未开始 2直播中 3已结束
@property (nonatomic, strong) NSString * tuji_id;
@property (nonatomic, strong) NSString * tuji_show_name;
@property (nonatomic, strong) NSString * tuji_show_status;// 1:显示图集  2:不显示
@property (nonatomic, strong) NSString * live_show_status;// 1直播中 2未开始或已结束 3其他
@property (nonatomic, strong) NSString * sport_index_img;
@property (nonatomic, strong) FWTagsInfoModel * tag_info;
@property (nonatomic, strong) FWBussinessShopGoodsListModel * goods_info;
@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * feed_list;
@property (nonatomic, strong) NSMutableArray<FWMineInfoModel *> * list_dashen;
@property (nonatomic, strong) NSMutableArray<FWTagModel *> * tag_list;
@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * feed_list_top;
@property (nonatomic, strong) NSMutableArray<FWHomeBannerModel *> * list_banner;
@property (nonatomic, strong) NSMutableArray<FWMineInfoModel *> *list_suggestion_user;
@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> *list_suggestion;
@property (nonatomic, strong) NSMutableArray<FWHomeNoteCategrayModel *> *list_category;
@property (nonatomic, strong) NSString * banner_img_url;
@property (nonatomic, strong) FWFeedListModel * feed_info;
@property (nonatomic, strong) NSMutableArray * list_original;//图集原图
@property (nonatomic, strong) NSMutableArray * list_small;//缩略图
@property (nonatomic, strong) FWLiveInfoModel * live_info;
@property (nonatomic, strong) NSString * sport_id;

@property (nonatomic, strong) NSMutableArray<FWHomeGaiZhuangListModel *> * list_gaizhuang_category1;
@property (nonatomic, strong) NSMutableArray<FWHomeGaiZhuangListModel *> * list_gaizhuang_category2;
@property (nonatomic, strong) NSMutableArray<FWHomeQuestionTagListModel *>  * list_tag;

@end
