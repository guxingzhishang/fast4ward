//
//  FWHomeModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/11.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWHomeCoverListModel : NSObject
@property (nonatomic, strong) NSString * img_url;
@property (nonatomic, strong) NSString * img_height;
@property (nonatomic, strong) NSString * img_width;
@end

@interface FWHomeShopListModel : NSObject
@property (nonatomic, strong) NSArray * main_business;
@property (nonatomic, strong) NSString * main_business_string;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * show_address;
@property (nonatomic, strong) NSString * city_id;
@property (nonatomic, strong) NSString * province_id;
@property (nonatomic, strong) NSString * area_id;
@property (nonatomic, strong) NSString * main_cars;
@property (nonatomic, strong) NSString * area_name;
@property (nonatomic, strong) NSArray * main_business_ids;
@property (nonatomic, strong) NSString * shop_desc;
@property (nonatomic, strong) NSString * province_name;
@property (nonatomic, strong) NSString * city_name;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) NSString * cover;
@property (nonatomic, strong) NSMutableArray<FWHomeCoverListModel *> * imgs;

@end

@interface FWHomeShopCityModel : NSObject
@property (nonatomic, strong) NSString * city_id;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * total_count;
@end

@interface FWHomeModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWHomeShopCityModel *> * shop_city;
@property (nonatomic, strong) NSMutableArray<FWHomeShopListModel *> * shop_list;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * has_more;

@end

NS_ASSUME_NONNULL_END
