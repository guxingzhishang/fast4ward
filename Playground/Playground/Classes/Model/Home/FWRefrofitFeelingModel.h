//
//  FWRefrofitFeelingModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWRefrofitFeelingCatListModel : NSObject
@property (nonatomic, strong) NSString * cat_id;
@property (nonatomic, strong) NSString * cat_name;
@end

@interface FWRefrofitFeelingTagListModel : NSObject
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * tag_name;

@end

@interface FWRefrofitFeelingModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWRefrofitFeelingCatListModel *> * list_category;
@property (nonatomic, strong) NSMutableArray<FWRefrofitFeelingTagListModel *> * list_tag;
@property (nonatomic, strong) NSMutableArray<FWRefrofitFeelingTagListModel *> * list_hot_tag;

@end

NS_ASSUME_NONNULL_END
