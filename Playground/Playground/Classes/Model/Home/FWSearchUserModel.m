//
//  FWSearchUserModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/20.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWSearchUserModel.h"

@implementation FWSearchUserModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"user_list":NSStringFromClass([FWMineInfoModel class]),
             };
}

@end

