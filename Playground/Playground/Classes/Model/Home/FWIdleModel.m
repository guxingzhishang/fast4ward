//
//  FWIdleModel.m
//  Playground
//
//  Created by 孤星之殇 on 2020/2/17.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWIdleModel.h"

@implementation FWIdleListModel

@end


@implementation FWIdleModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWIdleListModel class]),
    };
}

@end
