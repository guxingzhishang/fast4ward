//
//  FWRefitCategaryModel.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/10.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

/**
 * 改装清单
 */
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWRefitCategarySubDetailModel : NSObject
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * type;

@end

@interface FWRefitCategarySubListModel : NSObject
 // 第二级状态， 0 初始值  1原厂  2 非原厂
@property (nonatomic, assign) NSInteger  secondLevelType;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * tempName;
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSDictionary * secondDict;
@property (nonatomic, strong) NSMutableArray<FWRefitCategarySubDetailModel *> * list;
@end

@interface FWRefitCategaryListModel : NSObject
 // 第一级状态， 0 初始状态（有原厂和编辑） 1 展开状态（显示一键原厂）  2 都设置完成（一级不显示按钮，二级显示修改）
@property (nonatomic, assign) NSInteger  firstLevelType;
// 是否一键原厂  0 初始值  1原厂  2 非原厂
@property (nonatomic, assign) NSInteger  isFirstOriginalType;

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSMutableArray<FWRefitCategarySubListModel *> * sub;
@end

@interface FWRefitCategaryModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWRefitCategaryListModel *> * list_gaizhuang_category;
@end

NS_ASSUME_NONNULL_END
