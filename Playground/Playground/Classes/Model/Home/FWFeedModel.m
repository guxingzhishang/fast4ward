//
//  FWFeedModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/11.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWFeedModel.h"

@implementation FWInviteModel
@end

@implementation FWHomeNoteCategrayModel
@end

@implementation FWHomeBannerModel
@end

@implementation FWFormatContentModel
@end

@implementation FWForumAlbumsModel
@end

@implementation FWTagModel

@end

@implementation FWFeedCountRealtimeModel

@end

@implementation FWFeedImgsModel

@end


@implementation FWRefitJianyaoModel
@end

@implementation FWRefitCarStyleModel
@end

@implementation FWFeedVideoModel

@end

@implementation FWFeedListModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"tags":NSStringFromClass([FWSearchTagsSubListModel class]),
             @"imgs":NSStringFromClass([FWFeedImgsModel class]),
             @"imgs_original":NSStringFromClass([FWFeedImgsModel class]),
             @"forum_albums":NSStringFromClass([FWForumAlbumsModel class]),
             @"format_content":NSStringFromClass([FWFormatContentModel class]),
             @"goods_info" :NSStringFromClass([FWBussinessShopGoodsListModel class]),
             @"gaizhuang_jianyao":NSStringFromClass([FWRefitJianyaoModel class]),
             @"list_invite_user":NSStringFromClass([FWInviteModel class]),
             };
}
@end

@implementation FWHomeGaiZhuangListModel
@end


@implementation FWHomeQuestionTagListModel
@end

@implementation FWFeedModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"feed_list":NSStringFromClass([FWFeedListModel class]),
             @"tag_list":NSStringFromClass([FWTagModel class]),
             @"feed_list_top":NSStringFromClass([FWFeedListModel class]),
             @"list_banner":NSStringFromClass([FWHomeBannerModel class]),
             @"list_suggestion_user":NSStringFromClass([FWMineInfoModel class]),
             @"list_suggestion":NSStringFromClass([FWFeedListModel class]),
             @"list_category":NSStringFromClass([FWHomeNoteCategrayModel class]),
             @"list_dashen":NSStringFromClass([FWMineInfoModel class]),
             @"list_gaizhuang_category1":NSStringFromClass([FWHomeGaiZhuangListModel class]),
             @"list_gaizhuang_category2":NSStringFromClass([FWHomeGaiZhuangListModel class]),
             @"list_tag":NSStringFromClass([FWHomeQuestionTagListModel class]),
             };
}

@end

