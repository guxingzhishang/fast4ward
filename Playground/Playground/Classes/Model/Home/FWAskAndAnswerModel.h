//
//  FWAskAndAnswerModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWAskAndAnswerListModel : NSObject
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * header_url;
@property (nonatomic, strong) NSString * wenda_title;
@end

@interface FWAskAndAnswerModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWAskAndAnswerListModel *> * list_wenda_suggestion;
@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * feed_list;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * has_more;

@end

NS_ASSUME_NONNULL_END
