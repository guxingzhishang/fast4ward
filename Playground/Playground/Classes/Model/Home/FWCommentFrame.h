//
//  FWCommentFrame.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWCommentModel.h"
#import "FWReplyComment.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCommentFrame : NSObject

/** 头像frame */
@property (nonatomic , assign , readonly) CGRect avatarFrame;

/** 昵称frame */
@property (nonatomic , assign , readonly) CGRect nicknameFrame;

/** 点赞frame */
@property (nonatomic , assign , readonly) CGRect thumbFrame;

/** 点赞图标frame */
@property (nonatomic , assign , readonly) CGRect thumbImageFrame;

/** 点赞数frame */
@property (nonatomic , assign , readonly) CGRect thumbNumberFrame;

/** 时间frame */
@property (nonatomic , assign , readonly) CGRect createTimeFrame;

/** 内容尺寸 */
@property (nonatomic , assign , readonly) CGRect textFrame;

/** cell高度 */
@property (nonatomic , assign , readonly) CGFloat cellHeight;

/** 最大宽度 外界传递 */
@property (nonatomic , assign) CGFloat maxW;

/** 评论模型 外界传递 */
@property (nonatomic , strong) FWReplyComment *comment;

@end

NS_ASSUME_NONNULL_END
