//
//  FWCommentModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/9.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWMineModel.h"
#import "FWReplyComment.h"


@interface FWCommentListModel: NSObject

@property (nonatomic, copy) NSString * update_time;
@property (nonatomic, copy) NSString * uniq_hash;
@property (nonatomic, copy) NSString * uid;
@property (nonatomic, copy) NSString * is_liked;
@property (nonatomic, copy) NSString * format_create_time;
@property (nonatomic, copy) NSString * msg_status;
@property (nonatomic, copy) NSString * feed_id;
@property (nonatomic, copy) NSString * feed_type;
@property (nonatomic, copy) NSString * fromMessage;//自定义属性，从哪个页面来
@property (nonatomic, copy) NSString * check_status;
@property (nonatomic, copy) NSString * check_time;
@property (nonatomic, copy) NSString * like_count;
@property (nonatomic, copy) NSString * like_count_format;
@property (nonatomic, copy) NSString * comment_id;
@property (nonatomic, copy) NSString * check_misuid;
@property (nonatomic, copy) NSString * create_time;
@property (nonatomic, copy) NSString * p_comment_id;
@property (nonatomic, copy) NSString * reply_comment_id;
@property (nonatomic, copy) NSString * reply_comment_uid;
@property (nonatomic, copy) NSString * has_more;
@property (nonatomic, copy) NSString * status;
@property (nonatomic, copy) NSString * content;
@property (nonatomic, copy) NSString *reply_total_count;
@property (nonatomic, strong) NSMutableArray<FWReplyComment *> * reply_list;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) FWMineInfoModel * reply_user_info;

/** 富文本 */
- (NSAttributedString *)attributedText;

@end

@interface FWCommentModel : NSObject

@property (nonatomic, strong) NSString * feed_id;
@property (nonatomic, strong) NSString * p_comment_id;
@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * all_comment_count;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSMutableArray<FWCommentListModel *> * comment_list;

@end
