//
//  FWRefitCategaryModel.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/10.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWRefitCategaryModel.h"
@implementation FWRefitCategarySubDetailModel

@end

@implementation FWRefitCategarySubListModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWRefitCategarySubDetailModel class]),
    };
}
@end

@implementation FWRefitCategaryListModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"sub" : NSStringFromClass([FWRefitCategarySubListModel class]),
    };
}
@end

@implementation FWRefitCategaryModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list_gaizhuang_category" : NSStringFromClass([FWRefitCategaryListModel class]),
    };
}
@end
