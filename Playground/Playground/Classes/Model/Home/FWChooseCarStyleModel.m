//
//  FWChooseCarStyleModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWChooseCarStyleModel.h"

@implementation FWChooseCarStyleSubListModel

@end

@implementation FWChooseCarStyleListModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWChooseCarStyleSubListModel class]),
    };
}
@end


@implementation FWChooseCarStyleModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWChooseCarStyleListModel class]),
    };
}
@end
