//
//  FWChooseCarTypeModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWChooseCarTypeModel.h"


@implementation FWChooseCarTypeListModel
@end

@implementation FWChooseCarTypeModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWChooseCarTypeListModel class]),
    };
}
@end
