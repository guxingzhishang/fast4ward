//
//  FWChooseCarStyleModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWChooseCarStyleSubListModel : NSObject
@property (nonatomic, strong) NSString * style;
@property (nonatomic, strong) NSString * car_style_id;
@end

@interface FWChooseCarStyleListModel : NSObject
@property (nonatomic, strong) NSString * parameter;
@property (nonatomic, strong) NSMutableArray<FWChooseCarStyleSubListModel *> * list;
@end

@interface FWChooseCarStyleModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWChooseCarStyleListModel *> * list;
@end


NS_ASSUME_NONNULL_END
