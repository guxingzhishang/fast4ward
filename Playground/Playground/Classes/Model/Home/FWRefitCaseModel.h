//
//  FWRefitCaseModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWRefitCaseCategoryInfoModel : NSObject
@property (nonatomic, strong) NSString * img;
@property (nonatomic, strong) NSString * category_id;
@property (nonatomic, strong) NSString * category_id_enc;
@property (nonatomic, strong) NSString * name;
@end

@interface FWRefitCaseTagListModel : NSObject
@property (nonatomic, strong) NSString * tag_name;
@property (nonatomic, strong) NSString * tag_id;
@end

@interface FWRefitCaseCarTypeInfoModel : NSObject
@property (nonatomic, strong) NSString * car_type;
@property (nonatomic, strong) NSString * img;
@end

@interface FWRefitCaseTagInfoInfoModel : NSObject
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * tag_name;
@property (nonatomic, strong) NSString * tag_img;
@end

@interface FWRefitCaseModel : NSObject
@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * category_id;
@property (nonatomic, strong) NSString * show_name;
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * car_type;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * share_title;
@property (nonatomic, strong) NSString * share_img;
@property (nonatomic, strong) NSString * share_url;
@property (nonatomic, strong) NSString * share_desc;
@property (nonatomic, strong) NSMutableArray<NSString *> * list_car_style_suggestion;
@property (nonatomic, strong) NSMutableArray<FWRefitCaseTagListModel *> * list_tag;
@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * feed_list;
@property (nonatomic, strong) FWRefitCaseCategoryInfoModel * category_info;
@property (nonatomic, strong) FWRefitCaseTagInfoInfoModel * tag_info;
@property (nonatomic, strong) FWRefitCaseCarTypeInfoModel * car_type_info;

@end

NS_ASSUME_NONNULL_END
