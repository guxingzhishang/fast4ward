//
//  FWChooseCarBrandModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/12/26.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface FWChooseCarBrandSubListModel : NSObject
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * logo;

@end

@interface FWChooseCarBrandListModel : NSObject
@property (nonatomic, strong) NSString * F_C;
@property (nonatomic, strong) NSMutableArray<FWChooseCarBrandSubListModel *> * list;
@end

@interface FWChooseCarBrandModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWChooseCarBrandListModel *> * list;
@property (nonatomic, strong) NSMutableArray<NSString *> * list_FC;

@end

NS_ASSUME_NONNULL_END
