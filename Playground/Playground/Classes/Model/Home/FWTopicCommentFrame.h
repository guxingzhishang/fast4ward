//
//  FWTopicCommentFrame.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/14.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWCommentModel.h"
#import "FWCommentFrame.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWTopicCommentFrame : NSObject

/** 头像frame */
@property (nonatomic , assign , readonly) CGRect avatarFrame;

/** 昵称frame */
@property (nonatomic , assign , readonly) CGRect nicknameFrame;

/** 点赞frame */
@property (nonatomic , assign , readonly) CGRect thumbFrame;

/** 点赞图标frame */
@property (nonatomic , assign , readonly) CGRect thumbImageFrame;

/** 点赞数frame */
@property (nonatomic , assign , readonly) CGRect thumbNumberFrame;

/** 时间frame */
@property (nonatomic , assign , readonly) CGRect createTimeFrame;

/** 话题内容frame */
@property (nonatomic , assign , readonly) CGRect textFrame;

/** height 这里只是 整个话题占据的高度 */
@property (nonatomic , assign , readonly) CGFloat height;


/** 评论尺寸模型 由于后期需要用到，所以不涉及为只读 */
@property (nonatomic , strong ) NSMutableArray *commentFrames;


/** tableViewFrame cell嵌套tableView用到 本人有点懒 ，公用了一套模型 */
@property (nonatomic , assign , readonly) CGRect tableViewFrame;


/** 话题模型 */
@property (nonatomic , strong) FWCommentListModel *topic;

/** 类型 1：评论页；2:评论详情 **/
@property (nonatomic , strong) NSString * commentType;

- (instancetype)initWithType:(NSString *)commentType;


@end

NS_ASSUME_NONNULL_END
