//
//  FWAskAndAnswerModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/12/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWAskAndAnswerModel.h"


@implementation FWAskAndAnswerListModel
@end

@implementation FWAskAndAnswerModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list_wenda_suggestion" : NSStringFromClass([FWAskAndAnswerListModel class]),
        @"feed_list" : NSStringFromClass([FWFeedListModel class]),
    };
}


@end
