//
//  FWSearchTagsListModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWSearchTagsSubListModel : NSObject

@property (nonatomic, strong) NSString * tag_id;

@property (nonatomic, strong) NSString * tag_name;

@property (nonatomic, strong) NSString * live_state;

@property (nonatomic, strong) NSString * flag_live;

@property (nonatomic, strong) NSString * live_chatroom;

@property (nonatomic, strong) NSString * update_time;

@property (nonatomic, strong) NSString * pv;

@property (nonatomic, strong) NSString * pv_count_format;

@property (nonatomic, strong) NSString * feed_count;

@property (nonatomic, strong) NSString * p_id;

@property (nonatomic, strong) NSString * create_uid;

@property (nonatomic, strong) NSString * uv;

@property (nonatomic, strong) NSString * is_pub_reco;

@property (nonatomic, strong) NSString * is_sys;

@property (nonatomic, strong) NSString * is_recommand;

@property (nonatomic, strong) NSString * is_selected;

@property (nonatomic, strong) NSString * share_url;

@property (nonatomic, strong) NSString * sort_order;

@property (nonatomic, strong) NSString * status;

@property (nonatomic, strong) NSString * create_time;

@property (nonatomic, strong) NSString * follow_status_str;

@property (nonatomic, strong) NSString * follow_status;

@property (nonatomic, strong) NSString * ft_id;

@end

@interface FWSearchTagsListModel : NSObject

@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * tag_name;

@property (nonatomic, strong) NSMutableArray<FWSearchTagsSubListModel*> * tag_list;


@end
