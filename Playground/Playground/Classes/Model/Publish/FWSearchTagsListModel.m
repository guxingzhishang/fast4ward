//
//  FWSearchTagsListModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/3.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWSearchTagsListModel.h"

@implementation FWSearchTagsSubListModel

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.tag_name forKey:@"tag_name"];
    [aCoder encodeObject:self.tag_id forKey:@"tag_id"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.tag_name = [aDecoder decodeObjectForKey:@"tag_name"];
        self.tag_id = [aDecoder decodeObjectForKey:@"tag_id"];
    }
    return self;
}

@end

@implementation FWSearchTagsListModel

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.tag_name forKey:@"tag_name"];
    [aCoder encodeObject:self.tag_id forKey:@"tag_id"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.tag_name = [aDecoder decodeObjectForKey:@"tag_name"];
        self.tag_id = [aDecoder decodeObjectForKey:@"tag_id"];
    }
    return self;
}


+ (NSDictionary *)mj_objectClassInArray {
    
    return @{@"tag_list" : NSStringFromClass([FWSearchTagsSubListModel class])};
}

@end
