//
//  FWTagsFeedModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/8.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWTagsFeedModel.h"

@implementation FWTagsFeedSubModel

@end

@implementation FWTagsInfoModel

@end

@implementation FWTagsFeedModel

+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"feed_list" : NSStringFromClass([FWTagsFeedSubModel class]),
             @"tag_info" : NSStringFromClass([FWTagsInfoModel class])
             };
}
@end
