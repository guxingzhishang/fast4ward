//
//  FWTagsFeedModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/8.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWTagsFeedSubModel : NSObject


@end

@interface FWTagsInfoModel : NSObject

@property (nonatomic, copy) NSString * create_user_nickname;
@property (nonatomic, copy) NSString * create_user_header;
@property (nonatomic, copy) NSString * is_nav;
@property (nonatomic, copy) NSString * pv;
@property (nonatomic, copy) NSString * pv_count_format;
@property (nonatomic, copy) NSString * flag_live;
@property (nonatomic, copy) NSString * live_chatroom;
@property (nonatomic, copy) NSString * live_image;
@property (nonatomic, copy) NSString * refresh_time;
@property (nonatomic, copy) NSString * is_pub_reco;
@property (nonatomic, copy) NSString * tag_image;
@property (nonatomic, copy) NSString * live_stream;
@property (nonatomic, copy) NSString * live_status;
@property (nonatomic, copy) NSString * feed_count;
@property (nonatomic, copy) NSString * live_hf2_url;
@property (nonatomic, copy) NSString * live_hf_url;
@property (nonatomic, copy) NSString * listorder_activity;
@property (nonatomic, copy) NSString * tag_desc;
@property (nonatomic, copy) NSString * uv;
@property (nonatomic, copy) NSString * update_time;
@property (nonatomic, copy) NSString * background_color;
@property (nonatomic, copy) NSString * p_id;
@property (nonatomic, copy) NSString * tag_id;
@property (nonatomic, copy) NSString * tag_id_enc;
@property (nonatomic, copy) NSString * border_color;
@property (nonatomic, copy) NSString * create_uid;
@property (nonatomic, copy) NSString * id_pub_reco;
@property (nonatomic, copy) NSString * is_sys;
@property (nonatomic, copy) NSString * is_recommand;
@property (nonatomic, copy) NSString * share_url;
@property (nonatomic, copy) NSString * is_selected;
@property (nonatomic, copy) NSString * follow_status;
@property (nonatomic, copy) NSString * sort_order;
@property (nonatomic, copy) NSString * tag_name;
@property (nonatomic, copy) NSString * status;
@property (nonatomic, copy) NSString * create_time;
@property (nonatomic, copy) NSString * share_desc;
@property (nonatomic, copy) NSString * share_title;

@end

@interface FWTagsFeedModel : NSObject

@property (nonatomic, copy) NSString * has_more;
@property (nonatomic, copy) NSString * follow_status;
@property (nonatomic, copy) NSString * page;
@property (nonatomic, copy) NSString * total_count;
@property (nonatomic, copy) NSString * page_size;
@property (nonatomic, strong) FWTagsInfoModel * tag_info;
@property (nonatomic, strong) NSMutableArray<FWTagsFeedSubModel *> *feed_list;

@end
