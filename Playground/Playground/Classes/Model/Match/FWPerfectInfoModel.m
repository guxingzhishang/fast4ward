//
//  FWPerfectInfoModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPerfectInfoModel.h"

@implementation FWPerfectInfoSizeListModel
@end

@implementation FWPerfectInfoBrandListModel
@end

@implementation FWPerfectInfoJinqiListModel
@end

@implementation FWPerfectInfoQudongListModel
@end

@implementation FWPerfectInfoZubieListModel
@end

@implementation FWPerfectInfoJiyouListModel
@end

@implementation FWPerfectInfoLuntaiListModel
@end

@implementation FWPerfectInfoDriverInfoModel
@end

@implementation FWPerfectInfoAdPositionModel
@end

@implementation FWPerfectInfoModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_ad_position":NSStringFromClass([FWPerfectInfoAdPositionModel class]),
             @"list_zubie":NSStringFromClass([FWPerfectInfoZubieListModel class]),
             @"list_qudong":NSStringFromClass([FWPerfectInfoQudongListModel class]),
             @"list_jinqi":NSStringFromClass([FWPerfectInfoJinqiListModel class]),
             @"list_brand":NSStringFromClass([FWPerfectInfoBrandListModel class]),
             @"list_size":NSStringFromClass([FWPerfectInfoSizeListModel class]),
             @"list_luntai":NSStringFromClass([FWPerfectInfoLuntaiListModel class]),
             @"list_jiyou":NSStringFromClass([FWPerfectInfoLuntaiListModel class]),
             };
}
@end
