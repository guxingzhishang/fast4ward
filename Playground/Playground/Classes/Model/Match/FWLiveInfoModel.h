//
//  FWLiveInfoModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/11.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWLiveURLModel : NSObject

@property (nonatomic, strong) NSString * hdl_url;
@property (nonatomic, strong) NSString * rtmp_url;
@property (nonatomic, strong) NSString * hls_url;
@property (nonatomic, strong) NSString * snapshot_url;

@end

@interface FWLiveInfoModel : NSObject

@property (nonatomic, strong) NSString * live_title;
@property (nonatomic, strong) NSString * chatroom_id;
@property (nonatomic, strong) NSString * live_status;
@property (nonatomic, strong) NSString * live_image;
@property (nonatomic, strong) NSString * live_status_desc;
@property (nonatomic, strong) NSString * live_ad_type;// 广告类型，h5_inner内部打开的h5,h5_outer外部打开的h5，tianmao天猫，jingdong京东，taobao淘宝，weidian微店
@property (nonatomic, strong) NSString * live_ad_val;// 跳转到的页面地址
@property (nonatomic, strong) NSString * live_ad_status_show;// 值为1代表展示，2隐藏
@property (nonatomic, strong) NSString * live_ad_img;// 广告图片url
@property (nonatomic, strong) NSString * live_ad_goods_id;//如果是商品，商品的id
@property (nonatomic, strong) FWLiveURLModel * list_url;
@property (nonatomic, strong) NSMutableArray<NSString *> *converts;

@end

NS_ASSUME_NONNULL_END
