//
//  FWGuessingModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWGuessingRecordModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGuessingListModel : NSObject

@property (nonatomic, strong) NSString * bid_status; // 当前用户是否下注了,1:已下注  3:没有,
@property (nonatomic, strong) NSString * min_gold;
@property (nonatomic, strong) NSString * match_id;
@property (nonatomic, strong) NSString * bid_status_str;
@property (nonatomic, strong) NSString * bid_value;
@property (nonatomic, strong) NSString * bid_car_no;
@property (nonatomic, strong) NSString * phase_name;
@property (nonatomic, strong) NSString * group_name;
@property (nonatomic, strong) NSString * guess_id;
@property (nonatomic, strong) NSString * max_gold;
@property (nonatomic, strong) NSString * ststus_str;
@property (nonatomic, strong) NSString * status; // 1为正在进行,2为封盘,3为未开始,4为已经结束

@property (nonatomic, assign) BOOL showMore; 

@property (nonatomic, strong) FWGuessingRecordUserInfoModel * car_1_info;
@property (nonatomic, strong) FWGuessingRecordUserInfoModel * car_2_info;
@end

@interface FWGuessingModel : NSObject

@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * gold_balance_value;
@property (nonatomic, strong) NSString * live_user_count;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * help_url;
@property (nonatomic, strong) NSMutableArray<FWGuessingListModel *> * guess_list;

@end

NS_ASSUME_NONNULL_END
