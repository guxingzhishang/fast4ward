//
//  FWFinalRankModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWFinalRankModel.h"

@implementation FWFinalScoreInfoModel
@end

@implementation FWFinalSubListModel
@end

@implementation FWFinalPhaseInfoModel
@end

@implementation FWFinalListModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list":NSStringFromClass([FWFinalSubListModel class]),
             };
}
@end

@implementation FWFinalRankModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list":NSStringFromClass([FWFinalListModel class]),
             };
}

@end
