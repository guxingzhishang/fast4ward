//
//  FWRegistrationHomeModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWPlayerSignupModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWRegistrationHomeListModel : NSObject
@property (nonatomic, strong) NSString * sport_date;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * sport_img;
@property (nonatomic, strong) NSString * sport_type_name;
@property (nonatomic, strong) NSString * baoming_id;
@property (nonatomic, strong) NSString * baoming_status;
@property (nonatomic, strong) NSString * sport_area;
@end

@interface FWRegistrationActInfoModel : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * match_time;
@property (nonatomic, strong) NSString * match_address;
@property (nonatomic, strong) NSString * h5_url;
@property (nonatomic, strong) NSString * img_url;

@end

@interface FWPlayerSignupDriverCardModel : NSObject
@property (nonatomic, strong) NSString * bg_url;
@property (nonatomic, strong) NSString * qrcode;
@property (nonatomic, strong) NSString * header_url;
@property (nonatomic, strong) NSString * nickname;
@end

@interface FWPlayerSignupPerfectInfoModel: NSObject

@property (nonatomic, strong) NSString * qudong;
@property (nonatomic, strong) NSString * club;
@property (nonatomic, strong) NSString * jinqi;
@property (nonatomic, strong) NSString * driver_img;
@property (nonatomic, strong) NSString * mianguan_img;
@property (nonatomic, strong) NSString * pailiang;
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * idcard_img;
@property (nonatomic, strong) NSString * weixin_id;
@property (nonatomic, strong) NSString * gaizhuang_order;
@property (nonatomic, strong) NSString * clothes_size;
@property (nonatomic, strong) NSString * gaizhuang_fee;
@property (nonatomic, strong) NSString * carframe_number;
@property (nonatomic, strong) NSString * mali;
@property (nonatomic, strong) NSString * car_style;
@property (nonatomic, strong) NSString * zubie;
@property (nonatomic, strong) NSString * luntai;
@property (nonatomic, strong) NSString * jiyou;
@property (nonatomic, strong) NSMutableArray * ad_position_ids;
@property (nonatomic, strong) NSMutableArray * ad_position_imgs;

@end



@interface FWExtraListModel : NSObject
@property (nonatomic, strong) NSString * extra_id;
@property (nonatomic, strong) NSString * val;
@end

@interface FWBaomingInfoModel : NSObject
@property (nonatomic, strong) NSString * sport;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * sport_img;
@property (nonatomic, strong) NSString * sport_type_name;
@property (nonatomic, strong) NSString * sport_date;
@property (nonatomic, strong) NSString * sport_area;
@property (nonatomic, strong) NSString * pay_tip;
@property (nonatomic, strong) NSString * baoming_status;// 1：未开始，2：进行中，3：已结束，4：名额已满
@property (nonatomic, strong) NSString * perfect_info_status;//1代表可以去完善个人信息，2代表不可以
@property (nonatomic, strong) NSString * my_baoming_status;// 1:未报名  2：已报名且成功支付  3：已报名但未支付
@property (nonatomic, strong) NSString * begin_time_show;
@property (nonatomic, strong) NSString * h5_sport_desc;
@property (nonatomic, strong) NSString * h5_agreement;
@property (nonatomic, strong) NSString * h5_group_desc;
@property (nonatomic, strong) NSString * fenixang_img;
@property (nonatomic, strong) NSString * toutu_beijing_img;
@property (nonatomic, strong) NSString * toutu_img;
@property (nonatomic, strong) NSString * kefu_uid;
@property (nonatomic, strong) NSString * kefu_nickname;
@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) NSString * mobile_notice;
@property (nonatomic, strong) NSString * fee_type;
@property (nonatomic, strong) NSString * show_money;
@property (nonatomic, strong) NSString * show_fee_type;
@property (nonatomic, strong) NSString * extra_status;// 额外附加字段是否显示，1显示，2不显示
@property (nonatomic, strong) NSString * extra_desc;
@property (nonatomic, strong) NSString * extra_tip;
@property (nonatomic, strong) NSString * baoming_id;
@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, strong) NSMutableArray<FWExtraListModel *> * extra_list;
@property (nonatomic, strong) NSString * baoming_num_remain;
@property (nonatomic, strong) FWPlayerSignupInfoModel * pay_info;

@end

@interface FWRegistrationHomeModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWRegistrationHomeListModel *> * list_baoming;

@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, strong) NSString * baoming_id;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * sport_img;
@property (nonatomic, strong) NSString * sport_type_name;
@property (nonatomic, strong) NSString * sport_date;
@property (nonatomic, strong) NSString * sport_area;
@property (nonatomic, strong) NSString * baoming_status;
@property (nonatomic, strong) NSString * perfect_info_status;
@property (nonatomic, strong) NSString * pay_status;
@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) FWPlayerSignupDriverCardModel * driver_card;
@property (nonatomic, strong) FWPlayerSignupPerfectInfoModel * perfect_info;
@property (nonatomic, strong) FWPlayerSignupInfoModel * pay_info;
@property (nonatomic, strong) NSString * has_more;

@end

NS_ASSUME_NONNULL_END
