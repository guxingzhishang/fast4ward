//
//  FWPlayerRankModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWScoreDetailVideoInfoModel : NSObject
@property (nonatomic, strong) NSString * video_time;
@property (nonatomic, strong) NSString * video_cover;
@property (nonatomic, strong) NSString * video_url;
@property (nonatomic, strong) NSString * video_title;
@end

@interface FWScoreDetailImgsModel : NSObject
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * original_url;
@end

@interface FWScoreMatchListModel : NSObject
@property (nonatomic, strong) NSString * et;
@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * club_name;
@property (nonatomic, strong) NSString * driver_id;
@property (nonatomic, strong) NSString * match_log_id;
@property (nonatomic, strong) NSString * rt;
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * nick;
@property (nonatomic, strong) NSString * car_group;
@property (nonatomic, strong) NSString * header;
@property (nonatomic, strong) NSString * vspeed;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * model;
@property (nonatomic, strong) NSString * phase_type_str;

@end


@interface FWScoreDetailModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWScoreMatchListModel *> * match_list;
@property (nonatomic, strong) NSString * userid;
@property (nonatomic, strong) NSString * last_push_time;
@property (nonatomic, strong) NSString * last_push_md5;
@property (nonatomic, strong) NSString * car_group;
@property (nonatomic, strong) NSString * index;
@property (nonatomic, strong) NSString * club_name;
@property (nonatomic, strong) NSString * et;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * imgs_count;
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * driver_id;
@property (nonatomic, strong) NSString * match_log_id;
@property (nonatomic, strong) NSString * player_name;
@property (nonatomic, strong) NSString * player_id;
@property (nonatomic, strong) NSString * vspeed;
@property (nonatomic, strong) NSString * rt;
@property (nonatomic, strong) NSString * phase_type_str;
@property (nonatomic, strong) NSString * nick;
@property (nonatomic, strong) NSString * header;
@property (nonatomic, strong) NSString * header_url;
@property (nonatomic, strong) NSString * model;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * car_type;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * refit_list;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * car_brand;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * rtandet;
@property (nonatomic, strong) NSString * bestet_id;
@property (nonatomic, strong) NSString * realname;
@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) NSString * group_name;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * sport_type;
@property (nonatomic, strong) NSMutableArray<FWScoreDetailVideoInfoModel *> * video_list;
@property (nonatomic, strong) NSMutableArray<FWScoreDetailImgsModel *> * imgs;

@end

@interface FWScoreSearchModel : NSObject
@property (nonatomic, strong) NSMutableArray * driver_ids;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSMutableArray<FWScoreDetailModel *> * score_list;
@end




@interface FWGroupInfoModel : NSObject
@property (nonatomic, strong) NSString * group_name;
@property (nonatomic, strong) NSString * group_id;
@end

@interface FWGroupListModel : NSObject
@property (nonatomic, strong) FWGroupInfoModel * group_info;
@property (nonatomic, strong) NSMutableArray<FWScoreDetailModel *> * score_list;
@end

@interface FWPlayerRankListModel : NSObject
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * year;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * sport_id;
@end

@interface FWPlayerRankModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWPlayerRankListModel *> * category_list;
@property (nonatomic, strong) NSMutableArray<FWGroupListModel *> * group_list;
@property (nonatomic, strong) FWScoreDetailModel * driver_rank_item;
@property (nonatomic, strong) NSString * top_img_url;
@end

NS_ASSUME_NONNULL_END
