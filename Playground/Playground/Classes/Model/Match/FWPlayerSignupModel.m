//
//  FWPlayerSignupModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerSignupModel.h"

@implementation FWPlayerSignupPayListModel
@end

@implementation FWPlayerSignupInfoModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_pay_type":NSStringFromClass([FWPlayerSignupPayListModel class]),
             };
}

@end

@implementation FWPlayerSignupGoodsModel
@end

@implementation FWPlayerSignupModel
@end
