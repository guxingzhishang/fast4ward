//
//  FWMatchModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/23.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWMatchModel.h"

@implementation FWF4WMatchReplayListModel
@end

@implementation FWF4WMatchLiveListModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
            @"list_replay":NSStringFromClass([FWF4WMatchReplayListModel class]),
             };
}
@end

@implementation FWF4WMatchLiveModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list":NSStringFromClass([FWF4WMatchLiveListModel class]),
             };
}
@end


@implementation FWF4WMatchNewsModel
@end

@implementation FWTagsModel
@end

@implementation FWAboutModel
//模型中的属性名和字典中的key不相同(或者需要多级映射)
+(NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"about_id" : @"id"
             };
}
@end

@implementation FWVodListModel
@end


@implementation FWF4WMatchModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"feed_list":NSStringFromClass([FWFeedListModel class]),
             @"list_news":NSStringFromClass([FWF4WMatchNewsModel class]),
             @"list_tag":NSStringFromClass([FWTagsModel class]),
             @"about":NSStringFromClass([FWAboutModel class]),
             @"vod_list":NSStringFromClass([FWVodListModel class]),
             @"refit_list":NSStringFromClass([FWRefitReportListModel class]),
    };
}
@end



@implementation FWMatchListModel

@end

@implementation FWMatchInfoModel

@end

@implementation FWMatchCategoryModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list":NSStringFromClass([FWMatchListModel class]),
             };
}

@end

@implementation FWMatchModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"category":NSStringFromClass([FWMatchCategoryModel class]),
             };
}

@end
