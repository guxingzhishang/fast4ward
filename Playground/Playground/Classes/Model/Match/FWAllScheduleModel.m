//
//  FWAllScheduleModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWAllScheduleModel.h"

@implementation FWScheduleBestETModel
@end

@implementation FWScheduleListModel
@end

@implementation FWAllScheduleListModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list" : NSStringFromClass([FWScheduleListModel class]),
             };
}
@end


@implementation FWAllScheduleModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"schedule_list" : NSStringFromClass([FWAllScheduleListModel class]),
             };
}

@end
