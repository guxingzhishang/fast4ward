//
//  FWRefitReportModel.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/8.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWRefitReportModel.h"

@implementation FWRefitReportListModel
@end

@implementation FWRefitReportModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWRefitReportListModel class]),
    };
}
@end
