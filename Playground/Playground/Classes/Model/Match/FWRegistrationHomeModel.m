//
//  FWRegistrationHomeModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRegistrationHomeModel.h"

@implementation FWRegistrationHomeListModel 
@end

@implementation FWRegistrationActInfoModel
@end

@implementation FWPlayerSignupDriverCardModel
@end

@implementation FWPlayerSignupPerfectInfoModel
@end

@implementation FWExtraListModel
@end

@implementation FWBaomingInfoModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"extra_list":NSStringFromClass([FWExtraListModel class]),
             };
}
@end

@implementation FWRegistrationHomeModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_baoming":NSStringFromClass([FWRegistrationHomeListModel class]),
            };
}
@end
