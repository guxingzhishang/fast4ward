//
//  FWFinalRankModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

/**
 * 决赛排名模型
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN



@interface FWFinalPhaseInfoModel : NSObject

@property (nonatomic, strong) NSString * FatherPhaseID;
@property (nonatomic, strong) NSString * ItemOrder;
@property (nonatomic, strong) NSString * FatherNodeKey;
@property (nonatomic, strong) NSString * PhaseCode;
@property (nonatomic, strong) NSString * SportID;
@property (nonatomic, strong) NSString * MatchID;
@property (nonatomic, strong) NSString * NodeLevel;
@property (nonatomic, strong) NSString * ScheduleItemName;
@property (nonatomic, strong) NSString * NodeKey;
@property (nonatomic, strong) NSString * PhaseID;
@property (nonatomic, strong) NSString * NodeTypeDes;

@end

@interface FWFinalScoreInfoModel : NSObject

@property (nonatomic, strong) NSString * phase_code;
@property (nonatomic, strong) NSString * run_status;
@property (nonatomic, strong) NSString * phase_type;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * phase_type_str;
@property (nonatomic, strong) NSString * winner_car_no;
@property (nonatomic, strong) FWCarInfoModel * car_1_info;
@property (nonatomic, strong) FWCarInfoModel * car_2_info;

@end

@interface FWFinalSubListModel : NSObject

@property (nonatomic, strong) NSString * FatherPhaseID;
@property (nonatomic, strong) NSString * ItemOrder;
@property (nonatomic, strong) NSString * FatherNodeKey;
@property (nonatomic, strong) NSString * PhaseCode;
@property (nonatomic, strong) NSString * SportID;
@property (nonatomic, strong) NSString * MatchID;
@property (nonatomic, strong) NSString * NodeLevel;
@property (nonatomic, strong) NSString * ScheduleItemName;
@property (nonatomic, strong) NSString * NodeKey;
@property (nonatomic, strong) NSString * PhaseID;
@property (nonatomic, strong) NSString * NodeTypeDes;
@property (nonatomic, strong) FWFinalScoreInfoModel * score_list;
@end


@interface FWFinalListModel : NSObject

@property (nonatomic, strong) FWFinalPhaseInfoModel * phase_info;
@property (nonatomic, strong) NSMutableArray<FWFinalSubListModel *> * list;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * name;

@end

@interface FWFinalRankModel : NSObject
@property (nonatomic, strong) NSString * live_user_count;
@property (nonatomic, strong) FWFinalPhaseInfoModel * phase_info;
@property (nonatomic, strong) NSMutableArray<FWFinalListModel *> * list;


@end

NS_ASSUME_NONNULL_END
