//
//  FWPlayerArchivesModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWPlayerRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerDriverInfoModel : NSObject
@property (nonatomic, strong) NSString * driver_id;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * f2;
@property (nonatomic, strong) NSString * f3;
@property (nonatomic, strong) NSString * f4;
@property (nonatomic, strong) NSString * f5;
@property (nonatomic, strong) NSString * f6;
@property (nonatomic, strong) NSString * f7;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * update_time;
@end


@interface FWPlayerSportModel : NSObject
@property (nonatomic, strong) NSString * year;
@property (nonatomic, strong) NSMutableArray<FWScoreDetailModel *> * list;

@end

@interface FWPlayerArchivesModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWPlayerSportModel *> * sport_list;
@property (nonatomic, strong) NSString * sport_count;
@property (nonatomic, strong) NSString * sport_count_text;
@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) FWPlayerDriverInfoModel * driver_info;

@end

NS_ASSUME_NONNULL_END
