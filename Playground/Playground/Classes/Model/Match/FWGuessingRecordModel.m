//
//  FWGuessingRecordModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGuessingRecordModel.h"

@implementation FWGuessingRecordUserInfoModel
@end

@implementation FWGuessingRecordListModel
@end

@implementation FWGuessingRecordModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWGuessingRecordListModel class]),
    };
}
@end
