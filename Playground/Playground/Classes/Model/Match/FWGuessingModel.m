//
//  FWGuessingModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGuessingModel.h"

@implementation FWGuessingListModel
@end

@implementation FWGuessingModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"guess_list" : NSStringFromClass([FWGuessingListModel class]),
    };
}
@end
