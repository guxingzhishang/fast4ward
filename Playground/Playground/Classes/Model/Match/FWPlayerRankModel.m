//
//  FWPlayerRankModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPlayerRankModel.h"

@implementation FWScoreDetailVideoInfoModel
@end

@implementation FWScoreDetailImgsModel
@end

@implementation FWScoreMatchListModel : NSObject
@end

@implementation FWScoreDetailModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"imgs":NSStringFromClass([FWScoreDetailImgsModel class]),
             @"match_list":NSStringFromClass([FWScoreMatchListModel class]),
             @"video_list":NSStringFromClass([FWScoreDetailVideoInfoModel class]),
             };
}
@end



@implementation FWScoreSearchModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"score_list":NSStringFromClass([FWScoreDetailModel class]),
             };
}
@end

@implementation FWGroupInfoModel
@end

@implementation FWGroupListModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"score_list":NSStringFromClass([FWScoreDetailModel class]),
             };
}

@end


@implementation FWPlayerRankListModel
@end

@implementation FWPlayerRankModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"category_list":NSStringFromClass([FWPlayerRankListModel class]),
             @"group_list":NSStringFromClass([FWGroupListModel class]),
             @"driver_rank_item":NSStringFromClass([FWGroupListModel class]),
             };
}
@end
