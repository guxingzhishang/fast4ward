//
//  FWOfficialVideoModel.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/9.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWMatchModel.h"
#import "FWPlayerRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWOfficialVideoModel : NSObject

@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSMutableArray<FWGroupInfoModel *>  * group_list;
@property (nonatomic, strong) NSMutableArray<FWVodListModel *> * vod_list;


@end

NS_ASSUME_NONNULL_END
