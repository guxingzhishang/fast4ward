//
//  FWGuessingRecordModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWScoreModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWGuessingRecordUserInfoModel : NSObject
@property (nonatomic, strong) NSString * index;
@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * count;
@property (nonatomic, strong) NSString * odds;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * car_type;
@property (nonatomic, strong) NSString * car_brand;
@property (nonatomic, strong) NSString * header;
@property (nonatomic, strong) NSString * win_status;
@property (nonatomic, strong) NSString * best_rt;
@property (nonatomic, strong) NSString * best_rtandet;
@property (nonatomic, strong) NSString * rate;
@property (nonatomic, strong) NSString * best_vspeed;

@end

@interface FWGuessingRecordListModel : NSObject

@property (nonatomic, strong) NSString * guess_log_id;
@property (nonatomic, strong) NSString * guess_id;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * match_id;
@property (nonatomic, strong) NSString * win_car_no;
@property (nonatomic, strong) NSString * guess_status;
@property (nonatomic, strong) NSString * guess_gold;
@property (nonatomic, strong) NSString * real_get_gold;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * index;
@property (nonatomic, strong) NSString * exchange_status;
@property (nonatomic, strong) NSString * group_name;
@property (nonatomic, strong) NSString * phase_name;
@property (nonatomic, strong) NSString * status; // 1为正在进行,2为封盘,3为未开始,4为已经结束
@property (nonatomic, strong) NSString * status_str;
@property (nonatomic, strong) NSString * fp_odds;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) FWGuessingRecordUserInfoModel * car_1_info;
@property (nonatomic, strong) FWGuessingRecordUserInfoModel * car_2_info;

@end

@interface FWGuessingRecordModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWGuessingRecordListModel*> * list;
@end

NS_ASSUME_NONNULL_END
