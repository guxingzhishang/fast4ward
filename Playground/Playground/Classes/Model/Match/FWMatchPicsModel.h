//
//  FWMatchPicsModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMatchTujiIDModel : NSObject
@property (nonatomic, strong) NSString * tuji_id;
@property (nonatomic, strong) NSString * tuji_name;

@end

@interface FWMatchTujiNavModel : NSObject
@property (nonatomic, strong) NSString * nav_title;
@property (nonatomic, strong) NSString * nav_id;
@end

@interface FWMatchPicsModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWMatchTujiIDModel *> * list_tuji_id;
@property (nonatomic, strong) NSMutableArray<FWMatchTujiNavModel *> * list_nav;
@property (nonatomic, strong) NSMutableArray * list_small;
@property (nonatomic, strong) NSMutableArray * list_original;
@property (nonatomic, strong) NSString * has_more;

@end

NS_ASSUME_NONNULL_END
