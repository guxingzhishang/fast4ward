//
//  FWPreRankModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//
/**
 * 预赛排名模型
 */
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerInfoModel : NSObject

@property (nonatomic, strong) NSString * player_name;
@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * car_brand;
@property (nonatomic, strong) NSString * userid;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * header;
@property (nonatomic, strong) NSString * player_id;
@property (nonatomic, strong) NSString * sport_id;

@property (nonatomic, strong) NSString * last_push_time;
@property (nonatomic, strong) NSString * car_type;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * group_name;
@end

@interface FWPreRankListModel : NSObject

@property (nonatomic, strong) FWPlayerInfoModel * player_info;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * rt;
@property (nonatomic, strong) NSString * et;
@property (nonatomic, strong) NSString * vspeed;
@property (nonatomic, strong) NSString * score_status_str;
@property (nonatomic, strong) NSString * score_status;
@property (nonatomic, strong) NSString * rtandet;

@end

@interface FWMyScoretModel : NSObject

@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * rt;
@property (nonatomic, strong) NSString * et;
@property (nonatomic, strong) NSString * vspeed;
@property (nonatomic, strong) NSString * data_from;
@property (nonatomic, strong) NSString * match_log_id;
@property (nonatomic, strong) NSString * rtandet;
@property (nonatomic, strong) NSString * group_name;
@property (nonatomic, strong) NSString * sort_index;
@property (nonatomic, strong) FWPlayerInfoModel * player_info;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * score_status_str;

@end

@interface FWPreRankModel : NSObject

/**
 * show_my_score 如果为2 则不展示我的成绩，1则展示我的成绩
 *
 * 如果 show_my_score = 1 并且 my_car_no 不为空字符串
 *     如果 my_score 不为空，则显示成绩
 *     如果 my_score 为空，则显示”当前暂无我的比赛成绩“
 */

@property (nonatomic, strong) FWMatchListModel * phase_info;
@property (nonatomic, strong) NSMutableArray<FWPreRankListModel *> * list;
@property (nonatomic, strong) FWMyScoretModel * my_score;
@property (nonatomic, strong) FWPlayerInfoModel * my_player_info;
@property (nonatomic, strong) NSString * show_my_score;
@property (nonatomic, strong) NSString * my_car_no;
@property (nonatomic, strong) NSString * live_user_count;

@end

NS_ASSUME_NONNULL_END
