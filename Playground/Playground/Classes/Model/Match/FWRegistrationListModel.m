//
//  FWRegistrationListModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/8/8.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWRegistrationListModel.h"

@implementation FWRegistrationListModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list":NSStringFromClass([FWRegistrationHomeModel class]),
             };
}

@end
