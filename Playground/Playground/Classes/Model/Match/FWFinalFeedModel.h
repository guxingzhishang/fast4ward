//
//  FWFinalFeedModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/2.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWFinalTagInfoModel : NSObject

@property (nonatomic, strong) NSString * create_user_header;
@property (nonatomic, strong) NSString * create_user_nickname;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * is_recommand;
@property (nonatomic, strong) NSString * pv;
@property (nonatomic, strong) NSString * pv_count_format;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * p_id;
@property (nonatomic, strong) NSString * flag_live;
@property (nonatomic, strong) NSString * live_chatroom;
@property (nonatomic, strong) NSString * data_from;
@property (nonatomic, strong) NSString * is_selected;
@property (nonatomic, strong) NSString * refresh_time;
@property (nonatomic, strong) NSString * share_desc;
@property (nonatomic, strong) NSString * sort_order;
@property (nonatomic, strong) NSString * is_pub_reco;
@property (nonatomic, strong) NSString * share_url;
@property (nonatomic, strong) NSString * live_stream;
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * live_status;
@property (nonatomic, strong) NSString * feed_count;
@property (nonatomic, strong) NSString * share_title;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * is_sys;
@property (nonatomic, strong) NSString * create_uid;
@property (nonatomic, strong) NSString * tag_name;
@property (nonatomic, strong) NSString * uv;
@property (nonatomic, strong) NSString * is_nav;
//sub_list

@end


@interface FWFinalFeedModel : NSObject

@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * follow_status;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * page_size;

@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * feed_list;
@property (nonatomic, strong) FWFinalTagInfoModel * tag_info;
@property (nonatomic, strong) FWLiveInfoModel * live_info;

@end

NS_ASSUME_NONNULL_END
