//
//  FWLiveModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/12/6.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWLiveModel.h"


@implementation FWLiveListModel

@end

@implementation FWLiveHomeListModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list":NSStringFromClass([FWLiveListModel class]),
             };
}
@end

@implementation FWLiveModel

@end
