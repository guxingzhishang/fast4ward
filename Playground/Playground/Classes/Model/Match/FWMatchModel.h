//
//  FWMatchModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/23.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWFinalFeedModel.h"
#import "FWRefitReportModel.h"
#import "FWRefitListKeyModel.h"

NS_ASSUME_NONNULL_BEGIN

#pragma mark - > 以下是赛事列表模型
@interface FWF4WMatchReplayListModel : NSObject
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * replay_url;
@property (nonatomic, strong) NSString * chatroom_id;
@property (nonatomic, strong) NSString * img_url;
@end

@interface FWF4WMatchLiveListModel : NSObject
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * area;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * sport_time;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * sport_type;
@property (nonatomic, strong) NSString * logo;
@property (nonatomic, strong) NSMutableArray<FWF4WMatchReplayListModel *> * list_replay;
@end

@interface FWF4WMatchLiveModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWF4WMatchLiveListModel *> * list;
@end

#pragma mark - > 以下是赛事公告模型
@interface FWF4WMatchNewsModel : NSObject
@property (nonatomic, strong) NSString * pub_date;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * val;
/* 1:h5  2:视频帖 3:图文帖 4:文章帖 */
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) FWFeedListModel * feed_info;
@property (nonatomic, strong) NSString * img_url;

@end

@interface FWTagsModel : NSObject
@property (nonatomic, strong) NSString * feed_count;
@property (nonatomic, strong) NSString * tag_name;
@property (nonatomic, strong) NSString * tag_id;

@end

@interface FWAboutModel : NSObject
@property (nonatomic, strong) NSString * about_id;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * logo;
@property (nonatomic, strong) NSString * href;
@end

@interface FWVodListModel : NSObject
@property (nonatomic, strong) NSString * feed_id;
@property (nonatomic, strong) NSString * feed_type;
@property (nonatomic, strong) NSString * feed_title;
@property (nonatomic, strong) NSString * feed_cover;
@property (nonatomic, strong) NSString * feed_id_encode;
@property (nonatomic, strong) NSString * feed_cover_original;
@property (nonatomic, strong) NSString * is_long_video;
@end


@interface FWF4WMatchModel : NSObject
@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * img_url_play;
@property (nonatomic, strong) NSString * img_url_replay;
@property (nonatomic, strong) NSString * live_chatroom;
@property (nonatomic, strong) NSString * live_show_status; // 1、3 展示，2 隐藏
@property (nonatomic, strong) NSString * live_show_desc;
@property (nonatomic, strong) NSString * live_status_desc;
@property (nonatomic, strong) NSString * baoming_status;// 是否开启报名1开启2未开启
@property (nonatomic, strong) NSString * sport_status;// 是否有比赛，1有2没有;
@property (nonatomic, strong) NSString * ticket_status; // 是否开启售卖门票1开启2未开启
@property (nonatomic, strong) NSString * sport_index_img;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * tuji_show_name;
@property (nonatomic, strong) FWLiveInfoModel * live_info;
@property (nonatomic, strong) FWRefitListKeyModel * refit_list_keys;
@property (nonatomic, strong) NSMutableArray<FWF4WMatchNewsModel *> * list_news;
@property (nonatomic, strong) NSMutableArray * list_original;//图集原图
@property (nonatomic, strong) NSMutableArray * list_small;//缩略图
@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * feed_list;
@property (nonatomic, strong) NSMutableArray<FWVodListModel *> * vod_list;
@property (nonatomic, strong) NSMutableArray<FWAboutModel *> * about;
@property (nonatomic, strong) NSMutableArray<FWTagsModel *> *list_tag;
@property (nonatomic, strong) NSMutableArray<FWRefitReportListModel *> *refit_list;

@end

#pragma mark - > 以下是比赛成绩相关模型
@interface FWMatchListModel : NSObject

@property (nonatomic, strong) NSString * FatherPhaseID;
@property (nonatomic, strong) NSString * ItemOrder;
@property (nonatomic, strong) NSString * FatherNodeKey;
@property (nonatomic, strong) NSString * PhaseCode;
@property (nonatomic, strong) NSString * SportID;
@property (nonatomic, strong) NSString * MatchID;
@property (nonatomic, strong) NSString * group_id;
@property (nonatomic, strong) NSString * NodeLevel;
@property (nonatomic, strong) NSString * ScheduleItemName;
@property (nonatomic, strong) NSString * NodeKey;
@property (nonatomic, strong) NSString * PhaseID;
@property (nonatomic, strong) NSString * NodeTypeDes;

@end

@interface FWMatchInfoModel : NSObject

@property (nonatomic, strong) NSString * FatherPhaseID;
@property (nonatomic, strong) NSString * ItemOrder;
@property (nonatomic, strong) NSString * FatherNodeKey;
@property (nonatomic, strong) NSString * PhaseCode;
@property (nonatomic, strong) NSString * SportID;
@property (nonatomic, strong) NSString * MatchID;
@property (nonatomic, strong) NSString * NodeLevel;
@property (nonatomic, strong) NSString * group_id;
@property (nonatomic, strong) NSString * ScheduleItemName;
@property (nonatomic, strong) NSString * NodeKey;
@property (nonatomic, strong) NSString * PhaseID;
@property (nonatomic, strong) NSString * NodeTypeDes;

@end

@interface FWMatchCategoryModel : NSObject

@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) FWMatchInfoModel * info;
@property (nonatomic, strong) NSMutableArray<FWMatchListModel *> * list;

@end

@interface FWMatchModel : NSObject

@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * area;
@property (nonatomic, strong) NSString * current_type;
@property (nonatomic, strong) NSString * gold_img_url;
@property (nonatomic, strong) NSString * guess_status;// 1: 已开竞猜  其他：未开
@property (nonatomic, strong) NSString * live_user_count;
@property (nonatomic, strong) NSMutableArray<FWMatchCategoryModel *> * category;

@end


NS_ASSUME_NONNULL_END
