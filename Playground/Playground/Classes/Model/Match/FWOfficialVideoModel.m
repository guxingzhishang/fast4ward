//
//  FWOfficialVideoModel.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/9.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWOfficialVideoModel.h"

@implementation FWOfficialVideoModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"group_list" : NSStringFromClass([FWGroupInfoModel class]),
        @"vod_list" : NSStringFromClass([FWVodListModel class]),
    };
}
@end
