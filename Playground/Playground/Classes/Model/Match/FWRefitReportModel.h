//
//  FWRefitReportModel.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/8.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWAllScheduleModel.h"
#import "FWRefitListKeyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWRefitReportListModel : NSObject
@property (nonatomic, strong) FWFeedListModel * feed_info;
@property (nonatomic, strong) FWScheduleBestETModel * driver_info;
@end

@interface FWRefitReportModel : NSObject

@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) FWRefitListKeyModel * refit_list_keys;
@property (nonatomic, strong) NSMutableArray<FWRefitReportListModel *> * list;

@end

NS_ASSUME_NONNULL_END
