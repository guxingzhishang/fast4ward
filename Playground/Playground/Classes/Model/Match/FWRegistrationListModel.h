//
//  FWRegistrationListModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/8/8.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWRegistrationHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWRegistrationListModel : NSObject

@property (nonatomic, strong) NSString * join_status;// 1已加入， 2未加入
@property (nonatomic, strong) NSString * share_desc;
@property (nonatomic, strong) NSString * share_url;
@property (nonatomic, strong) NSString * share_title;
@property (nonatomic, strong) NSString * share_img;

@property (nonatomic, strong) NSString * ad_plan_img;
@property (nonatomic, strong) NSString * h5_plan;

@property (nonatomic, strong) NSMutableArray<FWRegistrationHomeModel *> * list;
@end

NS_ASSUME_NONNULL_END
