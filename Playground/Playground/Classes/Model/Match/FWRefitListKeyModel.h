//
//  FWRefitListKeyModel.h
//  Playground
//
//  Created by 孤星之殇 on 2020/2/12.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWRefitListKeyModel : NSObject
@property (nonatomic, strong) NSString * cheshou;
@property (nonatomic, strong) NSString * car;
@property (nonatomic, strong) NSString * bestscore;

@end

NS_ASSUME_NONNULL_END
