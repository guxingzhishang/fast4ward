//
//  FWLiveModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/6.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWFinalFeedModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWLiveListModel : NSObject

@property (nonatomic, strong) NSString * live_id;
@property (nonatomic, strong) NSString * live_cover;
@property (nonatomic, strong) NSString * live_title;
@property (nonatomic, strong) NSString * live_time;
@property (nonatomic, strong) NSString * live_uid;
@property (nonatomic, strong) NSString * room_id;
@property (nonatomic, strong) NSString * live_hup;
@property (nonatomic, strong) NSString * user_total;
@property (nonatomic, strong) NSString * zhibo_status;
@property (nonatomic, strong) NSString * live_share_image;
@property (nonatomic, strong) NSString * live_share_qrcode;
@property (nonatomic, strong) NSString * replay_url;
@property (nonatomic, strong) NSString * feed_id;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) FWLiveURLModel * list_url;

@end

@interface FWLiveHomeListModel : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSMutableArray<FWLiveListModel *> * list;

@end


@interface FWLiveModel : NSObject

@property (nonatomic, strong) NSString * live_status;
@property (nonatomic, strong) NSString * live_hf_url;
@property (nonatomic, strong) NSString * live_hf2_url;
@property (nonatomic, strong) NSString * live_hf_title;
@property (nonatomic, strong) NSString * live_hf2_title;
@property (nonatomic, strong) NSString * live_status_desc;
@property (nonatomic, strong) NSString * msg;
@property (nonatomic, strong) NSString * weixin;

@property (nonatomic, strong) FWLiveHomeListModel * list_yugao;
@property (nonatomic, strong) FWLiveHomeListModel * list_zhibo;
@property (nonatomic, strong) FWLiveHomeListModel * list_huifang;

@end

NS_ASSUME_NONNULL_END
