//
//  FWPerfectInfoModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/4.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWRegistrationHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWPerfectInfoSizeListModel : NSObject
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * select_id;
@end

@interface FWPerfectInfoJiyouListModel : NSObject
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * select_id;
@end

@interface FWPerfectInfoLuntaiListModel : NSObject
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * select_id;
@end

@interface FWPerfectInfoBrandListModel : NSObject
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * select_id;
@end

@interface FWPerfectInfoJinqiListModel : NSObject
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * select_id;
@end

@interface FWPerfectInfoQudongListModel : NSObject
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * select_id;
@end

@interface FWPerfectInfoZubieListModel : NSObject
@property (nonatomic, strong) NSString * val;
@property (nonatomic, strong) NSString * select_id;
@end

@interface FWPerfectInfoDriverInfoModel : NSObject
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * real_name;
@property (nonatomic, strong) NSString * idcard;
@property (nonatomic, strong) NSString * header_url;
@property (nonatomic, strong) NSString * bg_url;
@end

@interface FWPerfectInfoAdPositionModel : NSObject
@property (nonatomic, strong) NSString * ad_position_id;
@property (nonatomic, strong) NSString * ad_position_val;
@end


@interface FWPerfectInfoModel : NSObject


@property (nonatomic, strong) NSString * size_img;
@property (nonatomic, strong) NSString * plan_status; //1:广告计划选中  2:广告计划未选中
@property (nonatomic, strong) NSString * car_ad; //1:显示广告计划  2:不显示广告计划
@property (nonatomic, strong) NSString * tip;
@property (nonatomic, strong) NSString * h5_group_desc;
@property (nonatomic, strong) NSString * mianguan_img_desc;
@property (nonatomic, strong) NSMutableArray<FWPerfectInfoJinqiListModel *> * list_jinqi;
@property (nonatomic, strong) NSMutableArray<FWPerfectInfoQudongListModel *> * list_qudong;
@property (nonatomic, strong) NSMutableArray<FWPerfectInfoZubieListModel *> * list_zubie;
@property (nonatomic, strong) NSMutableArray<FWPerfectInfoSizeListModel *> * list_size;
@property (nonatomic, strong) NSMutableArray<FWPerfectInfoBrandListModel *> * list_brand;
@property (nonatomic, strong) NSMutableArray<FWPerfectInfoLuntaiListModel *> * list_luntai;
@property (nonatomic, strong) NSMutableArray<FWPerfectInfoJiyouListModel *> * list_jiyou;

@property (nonatomic, strong) FWPerfectInfoDriverInfoModel * driver_info;
@property (nonatomic, strong) FWPlayerSignupPerfectInfoModel * perfect_info;

@property (nonatomic, strong) NSString * plan_title;
@property (nonatomic, strong) NSString * plan_title2;
@property (nonatomic, strong) NSString * plan_desc;
@property (nonatomic, strong) NSString * h5_ad_demo;
@property (nonatomic, strong) NSString * h5_ad_position;
@property (nonatomic, strong) NSMutableArray<FWPerfectInfoAdPositionModel *> * list_ad_position;
@end

NS_ASSUME_NONNULL_END
