//
//  FWScoreModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWCarInfoModel : NSObject

@property (nonatomic, strong) NSString * player_name;
@property (nonatomic, strong) NSString * match_status;
@property (nonatomic, strong) NSString * match_text;
@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * header;
@property (nonatomic, strong) NSString * vehicle_speed;
@property (nonatomic, strong) NSString * reaction_time;
@property (nonatomic, strong) NSString * elapsed_time;
@property (nonatomic, strong) NSString * car_type;
@property (nonatomic, strong) NSString * rtandet_time;
@property (nonatomic, strong) NSString * uid;

@end

@interface FWScoreListModel : NSObject

@property (nonatomic, strong) NSString * currentStatus; // 自定义字段

@property (nonatomic, strong) NSString * winner_car_no;
@property (nonatomic, strong) NSString * phase_code ;
@property (nonatomic, strong) NSString * day;
@property (nonatomic, strong) NSString * run_status ;
@property (nonatomic, strong) NSString * phase_type ;
@property (nonatomic, strong) NSString * match_log_id ;
@property (nonatomic, strong) NSString * create_time ;
@property (nonatomic, strong) NSString * phase_type_str ;

@property (nonatomic, strong) FWCarInfoModel * car_1_info;
@property (nonatomic, strong) FWCarInfoModel * car_2_info;

/* 以下是我的成绩 */
@property (nonatomic, strong) NSString * phase_id ;
@property (nonatomic, strong) NSString * ctrl_winner_car_no;
@property (nonatomic, strong) NSString * ScheduleItemName ;
@property (nonatomic, strong) NSString * is_supp ;
@property (nonatomic, strong) NSString * phase_code_ori ;
@property (nonatomic, strong) NSString * check_status;
@property (nonatomic, strong) NSString * data_from;
@property (nonatomic, strong) NSString * balance_pk_id;

@end

@interface FWScoreModel : NSObject

@property (nonatomic, strong) NSMutableArray <FWScoreListModel *> * list;
@property (nonatomic, strong) NSMutableArray <FWScoreListModel *> * my_score_list;
@property (nonatomic, strong) NSString * my_car_no; // 如果有值，显示我的成绩
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * img_title_1;
@property (nonatomic, strong) NSString * img_title_2;
@property (nonatomic, strong) NSString * share_bg_url;
@property (nonatomic, strong) NSString * redisCacheTime;
@property (nonatomic, strong) NSString * strCompTime;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * compTime;
@property (nonatomic, strong) NSMutableArray<NSString *> * day_list;
@property (nonatomic, strong) NSString * live_user_count;

@end

NS_ASSUME_NONNULL_END
