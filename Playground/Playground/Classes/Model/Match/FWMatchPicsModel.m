//
//  FWMatchPicsModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/6/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchPicsModel.h"

@implementation FWMatchTujiIDModel
@end

@implementation FWMatchTujiNavModel
@end

@implementation FWMatchPicsModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_tuji_id":NSStringFromClass([FWMatchTujiIDModel class]),
             @"list_nav":NSStringFromClass([FWMatchTujiNavModel class]),
             };
}
@end
