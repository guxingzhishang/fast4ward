//
//  FWAllScheduleModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/30.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWScheduleBestETModel: NSObject
@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * et;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) NSString * match_log_id;
@property (nonatomic, strong) NSString * driver_id;
@property (nonatomic, strong) NSString * realname;
@property (nonatomic, strong) NSString * rt;
@property (nonatomic, strong) NSString * header_url;
@property (nonatomic, strong) NSString * car_group;
@property (nonatomic, strong) NSString * car_brand;
@property (nonatomic, strong) NSString * bestet_id;
@property (nonatomic, strong) NSString * rtandet;
@property (nonatomic, strong) NSString * vspeed;
@property (nonatomic, strong) NSString * car_type;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * f3; // 车手
@property (nonatomic, strong) NSString * f8; // 车队
@property (nonatomic, strong) NSString * f9; // 品牌
@property (nonatomic, strong) NSString * f10; // 车型
@property (nonatomic, strong) NSString * f16; // 组别
@property (nonatomic, strong) NSString * best_score_str;

@end

@interface FWScheduleListModel : NSObject
@property (nonatomic, strong) NSString * logo;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * sport_id;
@property (nonatomic, strong) NSString * year;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) FWScheduleBestETModel * best_et;
@end

@interface FWAllScheduleListModel : NSObject
@property (nonatomic, strong) NSString * year;
@property (nonatomic, strong) NSMutableArray<FWScheduleListModel *> * list;
@end

@interface FWAllScheduleModel : NSObject

@property (nonatomic, strong) NSString * redisCacheTime;
@property (nonatomic, strong) NSString * redis_key;
@property (nonatomic, strong) NSString * top_img_url;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSMutableArray<FWAllScheduleListModel *> * schedule_list;

@end

NS_ASSUME_NONNULL_END
