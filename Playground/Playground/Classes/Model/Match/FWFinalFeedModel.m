//
//  FWFinalFeedModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/12/2.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWFinalFeedModel.h"

@implementation FWFinalTagInfoModel
@end

@implementation FWFinalFeedModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"feed_list":NSStringFromClass([FWFeedListModel class]),
             };
}
@end
