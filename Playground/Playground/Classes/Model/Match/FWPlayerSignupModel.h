//
//  FWPlayerSignupModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWPlayerSignupPayListModel : NSObject
@property (nonatomic, strong) NSString * pay_type;
@property (nonatomic, strong) NSString * pay_type_name;
@end

@interface FWPlayerSignupInfoModel : NSObject
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSString * pay_title;
@property (nonatomic, strong) NSString * pay_money;
@property (nonatomic, strong) NSString * show_money_total;
@property (nonatomic, strong) NSMutableArray<FWPlayerSignupPayListModel *> * list_pay_type;
@end

@interface FWPlayerSignupGoodsModel : NSObject

@property (nonatomic, strong) NSString * goods_id;
@property (nonatomic, strong) NSString * goods_status_sale; // 1为开启，2位关闭
@property (nonatomic, strong) NSString * goods_title; // 标题
@property (nonatomic, strong) NSString * goods_price;// 售卖价格，单位元
@property (nonatomic, strong) NSString * goods_h5_url; //商品介绍页面

@end

@interface FWPlayerSignupModel : NSObject

@property (nonatomic, strong) FWPlayerSignupGoodsModel * goods;
@property (nonatomic, strong) NSString * baoming_type;
@property (nonatomic, strong) NSString * if_pay;
@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * sport_date;
@property (nonatomic, strong) NSString * fee_type;
@property (nonatomic, strong) NSString * show_money;
@property (nonatomic, strong) NSString * show_fee_type;
@property (nonatomic, strong) FWPlayerSignupInfoModel * pay_info;
@end

NS_ASSUME_NONNULL_END
