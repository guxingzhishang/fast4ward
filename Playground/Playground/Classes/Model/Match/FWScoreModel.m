//
//  FWScoreModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/11/27.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWScoreModel.h"

@implementation FWCarInfoModel

@end

@implementation FWScoreListModel

@end

@implementation FWScoreModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list":NSStringFromClass([FWScoreListModel class]),
             @"my_score_list":NSStringFromClass([FWScoreListModel class]),
             };
}

@end
