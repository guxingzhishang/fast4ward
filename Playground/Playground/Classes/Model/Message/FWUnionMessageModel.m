//
//  FWUnionMessageModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/2.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWUnionMessageModel.h"

@implementation FWMessageUserInfoModel

@end

@implementation FWUnionMessageModel

+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"r_user_info" : NSStringFromClass([FWMessageUserInfoModel class]),
             };
}
@end
