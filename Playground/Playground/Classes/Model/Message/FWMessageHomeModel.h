//
//  FWMessageHomeModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/2.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWMessageListModel : NSObject

/**
 * 消息id
 */
@property (nonatomic, copy) NSString * sys_msg_id;

/**
 * 内容
 */
@property (nonatomic, copy) NSString * content;

/**
 * 链接
 */
@property (nonatomic, copy) NSString * url;

/**
 * 读取状态
 */
@property (nonatomic, copy) NSString * read_status;

/**
 * 读取状态文案
 */
@property (nonatomic, copy) NSString * read_status_str;

/**
 * 头像
 */
@property (nonatomic, copy) NSString * header_url;

/**
 * 昵称
 */
@property (nonatomic, copy) NSString * nickname;

/**
 * 发布时间文案
 */
@property (nonatomic, copy) NSString * format_create_time;

/**
 * 创建时间
 */
@property (nonatomic, copy) NSString * create_time;


@end

@interface FWMessageHomeModel : NSObject

/**
 * 未读被赞数
 */
@property (nonatomic, copy) NSString * unread_like_count;

/**
 * 未读系统消息数
 */
@property (nonatomic, copy) NSString * unread_sys_count;

/**
 * 未读回答消息数
 */
@property (nonatomic, copy) NSString * unread_question_count ;
/**
 * 未读被评论数
 */
@property (nonatomic, copy) NSString * unread_comment_count;

/**
 * 未读被关注数
 */
@property (nonatomic, copy) NSString * unread_follow_count;

/**
 * 未读at数
 */
@property (nonatomic, copy) NSString * unread_at_count;

/**
 * 总未读数
 */
@property (nonatomic, copy) NSString * unread_total_count;

/**
 * 1未开始2直播中3已结束
 */
@property (nonatomic, copy) NSString * live_status;

/**
 * 消息列表
 */
@property (nonatomic, strong) NSMutableArray<FWMessageListModel *> * sys_msg_list;


@end
