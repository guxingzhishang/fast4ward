//
//  FWUnionMessageModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/2.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

/**
 * 此为消息列表页，评论、粉丝、点赞公用model
 */

#import <Foundation/Foundation.h>


@interface FWMessageUserInfoModel : NSObject

@property (nonatomic, copy) NSString * car_cert_status;

@property (nonatomic, copy) NSString * merchant_cert_status;

@property (nonatomic, copy) NSString * driver_cert_status;

@property (nonatomic, copy) NSString * cert_status;

/**
 * 昵称
 */
@property (nonatomic, copy) NSString * nickname;

/**
 * 头像地址
 */
@property (nonatomic, copy) NSString * header_url;

/**
 * 签名
 */
@property (nonatomic, copy) NSString * autograph;

/**
 * 用户ID（评论、点赞等操作的用户）
 */
@property (nonatomic, copy) NSString * uid;

/**
 * 性别
 */
@property (nonatomic, copy) NSString * sex;

@end


@interface FWUnionMessageModel : NSObject

/**
 * 值有可能是 detail 或者chat ,只对评论生效
 */
@property (nonatomic, copy) NSString * jump_to;

/**
 * 消息id
 */
@property (nonatomic, copy) NSString * msg_id;

/**
 * 消息详情
 */
@property (nonatomic, copy) NSString * msg_content;

/**
 * 提示（回复/评论）
 */
@property (nonatomic, copy) NSString * msg_text;

/**
 * 消息阅读状态
 * 1为已读,2为未读
 */
@property (nonatomic, copy) NSString * read_status;

@property (nonatomic, copy) NSString * user_car_id;

/**
 * 显示的时间
 */
@property (nonatomic, copy) NSString * format_create_time;

/**
 * 如果是点赞或者评论可以跳转到指定的详情
 */
@property (nonatomic, copy) NSString * feed_id;

/**
 * 视频贴 or 图文贴
 */
@property (nonatomic, copy) NSString * feed_type;

/**
 * 相关评论ID,
 */
@property (nonatomic, copy) NSString * comment_id;

/**
 * 相关回复评论ID,
 */
@property (nonatomic, copy) NSString * p_comment_id;

/**
 * 相关回复评论ID,
 */
@property (nonatomic, copy) NSString * r_comment_id;

/**
 * 相关用户ID,点击头像可以跳到Ta的个人页
 */
@property (nonatomic, copy) NSString * r_uid;

/**
 * 是否是长视频
 */
@property (nonatomic, copy) NSString * is_long_video;

/**
 * 帖子封面
 */
@property (nonatomic, copy) NSString *feed_cover;

/**
 * 用户相关信息
 */
@property (nonatomic, strong) FWMessageUserInfoModel * r_user_info;

@end



