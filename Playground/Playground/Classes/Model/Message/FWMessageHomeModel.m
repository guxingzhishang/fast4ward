//
//  FWMessageHomeModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/2.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWMessageHomeModel.h"

@implementation FWMessageListModel

@end


@implementation FWMessageHomeModel


+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"sys_msg_list" : NSStringFromClass([FWMessageListModel class]),
             };
}

@end
