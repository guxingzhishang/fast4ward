//
//  FWFavouriteContentTagModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWFavouriteContentTagSubModel : NSObject

@property (nonatomic, copy) NSString *tag_name;
@property (nonatomic, copy) NSString *tag_id;
@property (nonatomic, copy) NSString *border_color;
@property (nonatomic, copy) NSString *background_color;
@property (nonatomic, copy) NSString *font_color;
@property (nonatomic, copy) NSString *is_selected;

@end


@interface FWFavouriteContentTagModel: NSObject

@property (nonatomic, copy) NSString *tag_name;
@property (nonatomic, copy) NSString *tag_id;
@property (nonatomic, copy) NSString *border_color;
@property (nonatomic, copy) NSString *background_color;
@property (nonatomic, copy) NSString *font_color;
@property (nonatomic, copy) NSString *is_selected;

@property (nonatomic, strong) NSMutableArray<FWFavouriteContentTagSubModel *> *sub_list;
@end
