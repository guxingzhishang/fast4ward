//
//  FWMatchTicketsModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWMatchTicketsModel.h"

@implementation FWMatchTicketsListModel

@end


@implementation FWMatchTicketsModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_ticket" : NSStringFromClass([FWMatchTicketsListModel class]),
             };
}

@end
