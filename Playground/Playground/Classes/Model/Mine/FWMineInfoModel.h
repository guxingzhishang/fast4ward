//
//  FWMineInfoModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWMineInfoModel : NSObject

@property (nonatomic, strong) NSString *show_effect;
@property (nonatomic, strong) NSString *uid;
@property (nonatomic, strong) NSString *uid_enc;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *cert_name;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *f4w_id;
@property (nonatomic, strong) NSString *mobile_status;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *car_cert_status;
@property (nonatomic, strong) NSString *car_style;
@property (nonatomic, strong) NSString *dashen_title;
@property (nonatomic, strong) NSString *driver_cert_status;
@property (nonatomic, strong) NSString *effect_value;
@property (nonatomic, strong) NSString *user_type;
@property (nonatomic, strong) NSString *header_url_original;
@property (nonatomic, strong) NSString *autograph;
@property (nonatomic, strong) NSString *header_url;
@property (nonatomic, strong) NSString *share_url;
@property (nonatomic, strong) NSString *share_title;
@property (nonatomic, strong) NSString *share_desc;
@property (nonatomic, strong) NSString *weixin_bind_status;
@property (nonatomic, strong) NSString *mobile_encode;
@property (nonatomic, strong) NSString *follow_status;
@property (nonatomic, strong) NSString *follow_status_str;
@property (nonatomic, strong) NSString *pre_follow_status;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *user_level;
@property (nonatomic, strong) NSString *level_name;
@property (nonatomic, strong) NSString *merchant_cert_status; //1未认证2认证中3认证通过4认证未通过
@property (nonatomic, strong) NSString *cert_status; // 1、未开通商家联盟，  2、已开通商家联盟
@property (nonatomic, strong) NSString *shop_status; //店铺状态，0未提交审核，1提交审核，2审核通过，3审核拒绝
@property (nonatomic, strong) NSString * shop_status_desc;
@property (nonatomic, strong) NSString * shop_bg;
@property (nonatomic, strong) NSString * shop_address;
@property (nonatomic, strong) NSString * driver_id;

@end

NS_ASSUME_NONNULL_END
