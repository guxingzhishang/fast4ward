//
//  FWCertificationModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCertificationModel.h"

@implementation FWPersonalCertificationListModel

@end

@implementation FWPersonalCertificationModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list" : NSStringFromClass([FWPersonalCertificationListModel class])
             };
}
@end

@implementation FWBussinessCertificationListModel

@end

@implementation FWBussinessCertificationModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list" : NSStringFromClass([FWBussinessCertificationListModel class])
             };
}
@end

@implementation FWCertificationModel

@end
