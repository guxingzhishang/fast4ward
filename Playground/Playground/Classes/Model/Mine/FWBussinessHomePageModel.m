//
//  FWBussinessHomePageModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/3/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBussinessHomePageModel.h"

@implementation FWBussinessProductListModel

@end

@implementation FWBussinessHomePageModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_product" : NSStringFromClass([FWBussinessProductListModel class])
             };
}
@end
