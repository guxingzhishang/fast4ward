//
//  FWFollowModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/8.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWMineModel.h"

@interface FWFollowUserListModel: NSObject

@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *follow_status;
@property (nonatomic, copy) NSString *pre_follow_status;
@property (nonatomic, copy) NSString *f_uid;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *follow_status_str;
@property (nonatomic, copy) NSString *fu_id;
@property (nonatomic, copy) NSString *msg_status;
@property (nonatomic, strong) FWMineInfoModel *user_info;

@end

@interface FWFollowModel : NSObject

@property (nonatomic, copy) NSString *page_size;
@property (nonatomic, copy) NSString *has_more;
@property (nonatomic, copy) NSString *total_count;
@property (nonatomic, copy) NSString *page;
@property (nonatomic, copy) NSMutableArray<FWFollowUserListModel*> *user_list;

@end
