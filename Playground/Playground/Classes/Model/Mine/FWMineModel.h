//
//  FWMineModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/8.
//  strongright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWCarModel.h"

@interface FWMineModel : NSObject
@property (nonatomic, strong) NSString *block_status;
@property (nonatomic, strong) NSString *follow_count;
@property (nonatomic, strong) NSString *follow_status;
@property (nonatomic, strong) NSString *fans_count;
@property (nonatomic, strong) NSString *beliked_count;
@property (nonatomic, strong) NSString *feed_count;
@property (nonatomic, strong) NSString *feed_count_image;
@property (nonatomic, strong) NSString *longfeed_draft_count;
@property (nonatomic, strong) NSString *feed_count_video;
@property (nonatomic, strong) NSString *draft_count;
@property (nonatomic, strong) NSString *car_count;
@property (nonatomic, strong) NSString *is_selected;
@property (nonatomic, strong) NSString *visitor_count;
@property (nonatomic, strong) NSString *h5_cert;
@property (nonatomic, strong) NSString *h5_jifen;
@property (nonatomic, strong) NSString *visitor_count_today;
@property (nonatomic, strong) NSString *signin_status; // 1:当天已签到  2:当天未签到
@property (nonatomic, strong) NSString *qiandao_check; // 值为1代表有签到权限
@property (nonatomic, strong) NSString *jianlu_check; // 值为1代表有检录权限
@property (nonatomic, strong) NSString *ticket_check; // 值为1代表可以核销
@property (nonatomic, strong) NSString *gold_status; // 1 显示红点（领取任务金币的提醒） 2不显示
@property (nonatomic, strong) NSString *gold_write_off_status; // 值为1代表可以核销
@property (nonatomic, strong) NSString * car_notice_status;//1为显示提示，2不显示
@property (nonatomic, strong) NSString *banner_url_show_status;// 个人中心活动 1为展示2为不展示
@property (nonatomic, strong) NSString *banner_url;
@property (nonatomic, strong) NSMutableArray<NSString *> *list_visitor_header;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) NSMutableArray <FWCarListModel *> * list_car;

@end
