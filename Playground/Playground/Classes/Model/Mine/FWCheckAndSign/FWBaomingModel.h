//
//  FWBaomingModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWJianluInfoModel : NSObject

@property (nonatomic, strong) NSString * car_no;
@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, strong) NSString * real_name;
@property (nonatomic, strong) NSString * idcard;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * zubie;
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * car_style;
@property (nonatomic, strong) NSString * carframe_number;
@property (nonatomic, strong) NSString * status_qiandao; // 1 未签到，2已签到
@property (nonatomic, strong) NSString * status_jianlu;// 1 未检录，2已检录
@property (nonatomic, strong) NSString * mianze_img;

@property (nonatomic, assign) BOOL  isSendCer; // 当前用户是否发放车手初入证件
@property (nonatomic, assign) BOOL  isSendGift; // 当前用户是否发放车手礼包
@property (nonatomic, assign) BOOL  isUploadPic; // 当前用户是否上传了免责照片

@end

@interface FWJianluModel : NSObject

@property (nonatomic, strong) FWJianluInfoModel * jianlu_info;

@end

@interface FWBaomingListModel : NSObject

@property (nonatomic, strong) NSString * baoming_user_id;
@property (nonatomic, strong) NSString * name;

@end


@interface FWBaominCheckListModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWJianluInfoModel *> * list;

@property (nonatomic, strong) NSString * driver_total_count; // 车手总数
@property (nonatomic, strong) NSString * driver_total_count_qiandao_y ; //已签到车手数
@property (nonatomic, strong) NSString * driver_total_count_qiandao_n; // 未签到车手数
@property (nonatomic, strong) NSString * driver_total_count_jianlu_y; // 已检录车手数
@property (nonatomic, strong) NSString * driver_total_count_jianlu_n;// 未检录车手数

@end

@interface FWBaomingModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWBaomingListModel *> * list;

@end

NS_ASSUME_NONNULL_END
