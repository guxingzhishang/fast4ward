//
//  FWFavouriteContentTagModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/24.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWFavouriteContentTagModel.h"

@implementation FWFavouriteContentTagSubModel

@end


@implementation FWFavouriteContentTagModel

+ (NSDictionary *)mj_objectClassInArray {
    
    return @{@"sub_list" : NSStringFromClass([FWFavouriteContentTagSubModel class])};
}

@end

