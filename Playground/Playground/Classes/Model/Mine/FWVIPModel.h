//
//  FWVIPModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - >  微信订单模型
@interface FWWechatOrderModel : NSObject
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSString * pay_money;
@property (nonatomic, strong) NSString * trade_type;
@property (nonatomic, strong) NSString * result_code;
@property (nonatomic, strong) NSString * device_info;
@property (nonatomic, strong) NSString * mch_id;
@property (nonatomic, strong) NSString * return_msg;
@property (nonatomic, strong) NSString * return_code;

@property (nonatomic, strong) NSString * appid;
@property (nonatomic, strong) NSString * noncestr;
@property (nonatomic, strong) NSString * sign;
@property (nonatomic, strong) NSString * prepayid;
@property (nonatomic, strong) NSString * timestamp;
@property (nonatomic, strong) NSString * partnerid;

@end


#pragma mark - >  vip信息模型
@interface FWMemberInfoModel : NSObject
@property (nonatomic, strong) NSString * user_level;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * real_name;
@property (nonatomic, strong) NSString * level_name;
@end


#pragma mark - > 会员产品模型
@interface FWMemberListModel : NSObject
@property (nonatomic, strong) NSString * product_id;
@property (nonatomic, strong) NSString * user_level;
@property (nonatomic, strong) NSString * price_yuan;
@property (nonatomic, strong) NSString * price_cent;
@property (nonatomic, strong) NSString * product_name;
@property (nonatomic, strong) NSString * service;
@end

@interface FWVIPModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWMemberListModel *> *product_list;
@property (nonatomic, strong) NSString * product_intro_url;

@end

NS_ASSUME_NONNULL_END
