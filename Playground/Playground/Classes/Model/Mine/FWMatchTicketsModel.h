//
//  FWMatchTicketsModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface FWMatchTicketsListModel : NSObject

@property (nonatomic, strong) NSString * ticket_id;
@property (nonatomic, strong) NSString * list_page_img;
@property (nonatomic, strong) NSString * sport_date;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * sport_area;

@end

@interface FWMatchTicketsModel : NSObject

@property (nonatomic, strong) NSString * banner;
@property (nonatomic, strong) NSString * has_more;

@property (nonatomic, strong) NSMutableArray<FWMatchTicketsListModel*> * list_ticket;

@end

NS_ASSUME_NONNULL_END
