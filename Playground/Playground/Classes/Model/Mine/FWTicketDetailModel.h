//
//  FWTicketDetailModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWTicketDetailModel : NSObject

@property (nonatomic, strong) NSString * if_car;
@property (nonatomic, strong) NSString * kefu_uid;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * price_original;
@property (nonatomic, strong) NSString * if_address;
@property (nonatomic, strong) NSString * sport;
@property (nonatomic, strong) NSString * sport_name;
@property (nonatomic, strong) NSString * sport_date;
@property (nonatomic, strong) NSString * sport_area;
@property (nonatomic, strong) NSString * h5_ticket_detail;
@property (nonatomic, strong) NSString * number_remain;
@property (nonatomic, strong) NSString * status_user_buy;//当前用户购买状态 1可以购买  2已经达到购买次数限制  3达到个人购买票数限制
@property (nonatomic, strong) NSString * user_buy_number;
@property (nonatomic, strong) NSString * can_buy_number;
@property (nonatomic, strong) NSString * kefu_nickname;
@property (nonatomic, strong) NSString * if_mobile;
@property (nonatomic, strong) NSString * number;
@property (nonatomic, strong) NSString * if_sex;
@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) NSString * detail_page_img;
@property (nonatomic, strong) NSString * user_buy_frequency;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * if_real_name;
@property (nonatomic, strong) NSString * share_img;
@property (nonatomic, strong) NSString * pay_time_limit;
@property (nonatomic, strong) NSString * entrance_time;
@property (nonatomic, strong) NSString * buy_number;
@property (nonatomic, strong) NSString * order_number;
@property (nonatomic, strong) NSString * order_code;
@property (nonatomic, strong) NSString * pay_fee;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * real_name;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * sex;
@property (nonatomic, strong) NSString * car;
@property (nonatomic, strong) NSString * pay_status;
@property (nonatomic, strong) NSString * order_status; // 当前订单状态1未支付2未使用3已使用4订单已失效5门票已过期
@property (nonatomic, strong) NSString * pay_time;
@property (nonatomic, strong) NSString * pay_type;
@property (nonatomic, strong) NSString * check_number;
@property (nonatomic, strong) NSString * share_type; // 值为1代表分享h5链接，值为2代表分享一张图片
@property (nonatomic, strong) NSString * h5_share_title;
@property (nonatomic, strong) NSString * h5_share_desc;
@property (nonatomic, strong) NSString * h5_share_url;

@end

NS_ASSUME_NONNULL_END
