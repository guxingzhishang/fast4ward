//
//  FWGoodsChannelModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWGoodsChannelListModel : NSObject

@property (nonatomic, strong) NSString * channel;
@property (nonatomic, strong) NSString * channel_desc;

@end

@interface FWGoodsChannelModel : NSObject

@property (nonatomic, strong) NSString * shop_address;
@property (nonatomic, strong) NSString * h5_get_id;
@property (nonatomic, strong) NSMutableArray<FWGoodsChannelListModel *> * list_channel;


@end

NS_ASSUME_NONNULL_END
