//
//  FWGoodsChannelModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoodsChannelModel.h"

@implementation FWGoodsChannelListModel

@end

@implementation FWGoodsChannelModel
+ (NSDictionary *)mj_objectClassInArray {
    
    return @{@"list_channel" : NSStringFromClass([FWGoodsChannelListModel class])};
}
@end
