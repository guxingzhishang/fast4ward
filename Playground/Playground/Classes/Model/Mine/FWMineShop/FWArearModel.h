//
//  FWArearModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWRegionListModel : NSObject

@property (nonatomic, strong) NSString * region_id;
@property (nonatomic, strong) NSString * region_name;

@end

@interface FWRegionModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWRegionListModel *> * list;
@property (nonatomic, strong) NSMutableArray<FWRegionListModel *> * list_city;

@end

@interface FWArearListModel : NSObject

@property (nonatomic, strong) NSString * area_id;
@property (nonatomic, strong) NSString * name;

@end

@interface FWArearModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWArearListModel *> * list;
@property (nonatomic, strong) NSMutableArray<FWArearListModel *> * list_beijing;

@end

NS_ASSUME_NONNULL_END
