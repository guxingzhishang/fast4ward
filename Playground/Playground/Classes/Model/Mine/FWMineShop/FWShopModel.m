//
//  FWShopModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWShopModel.h"


@implementation FWBussinessImageModel
@end

@implementation FWBussinessShopInfoModel
+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"imgs" : NSStringFromClass([FWBussinessImageModel class]),
             };
}
@end

@implementation FWBussinessShopGoodsListModel
+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"imgs" : NSStringFromClass([FWBussinessImageModel class]),
             @"detail" : NSStringFromClass([FWBussinessImageModel class]),
             };
}
@end

@implementation FWBussinessShopModel
+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"good_list" : NSStringFromClass([FWBussinessShopGoodsListModel class]),
             @"goods_info_selected" : NSStringFromClass([FWBussinessShopGoodsListModel class]),
             };
    
}
@end

@implementation FWShopModel

@end
