//
//  FWArearModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/5/20.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWArearModel.h"


@implementation FWRegionListModel

@end

@implementation FWRegionModel
+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"list" : NSStringFromClass([FWRegionListModel class]),
             @"list_city" : NSStringFromClass([FWRegionListModel class]),
             };
}
@end

@implementation FWArearListModel

@end

@implementation FWArearModel
+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"list" : NSStringFromClass([FWArearListModel class]),
             @"list_beijing" : NSStringFromClass([FWArearListModel class]),
             };
}
@end
