//
//  FWShopModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/25.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWHomeModel.h"

NS_ASSUME_NONNULL_BEGIN

#pragma mark - > 商家店铺模型
@interface FWBussinessImageModel : NSObject
@property (nonatomic, strong) NSString * img_url;
@property (nonatomic, strong) NSString * img_wh;
@property (nonatomic, strong) NSString * img_width;
@property (nonatomic, strong) NSString * img_height;
@end


@interface FWBussinessShopInfoModel : NSObject
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * city_id;
@property (nonatomic, strong) NSString * province_id;
@property (nonatomic, strong) NSString * area_id;
@property (nonatomic, strong) NSString * main_cars;
@property (nonatomic, strong) NSString * area_name;
@property (nonatomic, strong) NSString * main_business;
@property (nonatomic, strong) NSString * main_business_ids;
@property (nonatomic, strong) NSString * shop_desc;
@property (nonatomic, strong) NSString * province_name;
@property (nonatomic, strong) NSString * city_name;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSMutableArray<FWBussinessImageModel *> *imgs;
@end

@interface FWBussinessShopGoodsListModel : NSObject
@property (nonatomic, strong) NSString * goods_id;
@property (nonatomic, strong) NSString * shop_goods_count;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * title_short;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * channel;
@property (nonatomic, strong) NSString * channel_val;
@property (nonatomic, strong) NSString * status_show;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * h5_url;
@property (nonatomic, strong) NSString * data_from;
@property (nonatomic, strong) NSString * goods_id_encode;
@property (nonatomic, strong) NSString * click_count;
@property (nonatomic, strong) NSString * flag_hidden;
@property (nonatomic, strong) NSString * flag_hidden_msg;
@property (nonatomic, strong) FWBussinessImageModel * cover;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) NSMutableArray<FWBussinessImageModel *> * imgs;
@property (nonatomic, strong) NSMutableArray<FWBussinessImageModel *> * detail;
@end

@interface FWBussinessShopModel : NSObject

@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * total_count_all;
@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * total_count_on;
@property (nonatomic, strong) NSString * total_count_off;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) FWHomeShopListModel * shop_info;
@property (nonatomic, strong) NSMutableArray<FWBussinessShopGoodsListModel *> * good_list;
@property (nonatomic, strong) NSMutableArray<FWBussinessShopGoodsListModel *> * goods_info_selected;


@end

@interface FWShopModel : NSObject

@end

NS_ASSUME_NONNULL_END
