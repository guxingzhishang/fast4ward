//
//  FWCouponModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCouponModel.h"

@implementation FWCouponListModel

@end


@implementation FWCouponModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"coupon_list" : NSStringFromClass([FWCouponListModel class]),
             };
}
@end
