//
//  FWCarModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarModel.h"


@implementation FWCarModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list" : NSStringFromClass([FWCarListModel class])
             };
}

@end
