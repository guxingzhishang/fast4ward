//
//  FWCouponModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/18.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWCouponListModel : NSObject
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * get_status;
@property (nonatomic, strong) NSString * get_time;
@property (nonatomic, strong) NSString * gift_type;
@property (nonatomic, strong) NSString * remark;
@property (nonatomic, strong) NSString * code;
@end

@interface FWCouponModel : NSObject

@property (nonatomic, strong) NSString * confirm_text;
@property (nonatomic, strong) NSString * nocoupon_text;
@property (nonatomic, strong) NSString * desc_url;
@property (nonatomic, strong) NSMutableArray<FWCouponListModel *>  * coupon_list;


@end

NS_ASSUME_NONNULL_END
