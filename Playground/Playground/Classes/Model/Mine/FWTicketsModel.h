//
//  FWTicketsModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/2/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - > 点击领取后票详情
@interface FWTicketsInfoModel : NSObject
@property (nonatomic, strong) NSString * code_id;
@property (nonatomic, strong) NSString * order_id;
@property (nonatomic, strong) NSString * remark;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * code;
@end

#pragma mark - > 门票列表
@interface FWTicketsListModel : NSObject
@property (nonatomic, strong) NSString * gift_type;
@property (nonatomic, strong) NSString * code;
@property (nonatomic, strong) NSString * get_status;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * get_time;
@property (nonatomic, strong) NSString * remark;
@end

@interface FWTicketsModel : NSObject
@property (nonatomic, strong) NSString * desc_url;
@property (nonatomic, strong) NSString * ticket_text;
@property (nonatomic, strong) NSString * confirm_text;
@property (nonatomic, strong) NSMutableArray<FWTicketsListModel *> *ticket_list;
@end

NS_ASSUME_NONNULL_END
