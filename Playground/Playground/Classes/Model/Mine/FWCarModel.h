//
//  FWCarModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

/**
 * 座驾相关模型
 */
#import <Foundation/Foundation.h>
#import "FWCarListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCarModel : NSObject
@property (nonatomic, strong) NSMutableArray <FWCarListModel *> * list;
@property (nonatomic, strong) NSString * car_number_limit;
@property (nonatomic, strong) NSString * has_more;

@end

NS_ASSUME_NONNULL_END
