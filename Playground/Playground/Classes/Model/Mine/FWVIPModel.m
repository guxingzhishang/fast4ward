//
//  FWVIPModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/14.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVIPModel.h"

@implementation FWWechatOrderModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"desc" : @"description"};
}

@end

@implementation FWMemberInfoModel
@end

@implementation FWMemberListModel
@end

@implementation FWVIPModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"product_list" : NSStringFromClass([FWMemberListModel class]),
             };
}
@end

