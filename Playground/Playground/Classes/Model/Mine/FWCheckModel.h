//
//  FWCheckModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWCheckModel : NSObject

@property (nonatomic, strong) NSString * order_id;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * entrance_time;
@property (nonatomic, strong) NSString * buy_number;
@property (nonatomic, strong) NSString * check_number;
@property (nonatomic, strong) NSString * order_number;
@property (nonatomic, strong) NSString * sport_date;
@property (nonatomic, strong) NSString * check_status;


@end

NS_ASSUME_NONNULL_END
