//
//  FWVisitorModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWVisitorModel.h"

//@implementation FWVisitorAnalysisAreaListModel
//@end
//
//@implementation FWVisitorAnalysisAreaModel
//
//+(NSDictionary *)mj_objectClassInArray{
//    return @{
//             @"list_visitor_area" : NSStringFromClass([FWVisitorAnalysisAreaListModel class])
//             };
//}
//@end

@implementation FWVisitorAnalysisListModel
@end

@implementation FWVisitorAnalysisModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_visitor" : NSStringFromClass([FWVisitorAnalysisListModel class]),
             };
}
@end


@implementation FWVisitorNameListModel
@end

@implementation FWVisitorModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_visitor" : NSStringFromClass([FWVisitorNameListModel class])
             };
}
@end
