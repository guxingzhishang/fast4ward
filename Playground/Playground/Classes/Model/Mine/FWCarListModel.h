//
//  FWCarListModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWCarImgsModel : NSObject
@property (nonatomic, strong) NSString * car_img;
@property (nonatomic, strong) NSString * car_img_original;
@property (nonatomic, assign) BOOL isSelect;
@end

@interface FWCarRealTimeModel : NSObject
@property (nonatomic, strong) NSString * like_count;
@property (nonatomic, strong) NSString * like_count_format;
@property (nonatomic, strong) NSString * comment_count_format;
@property (nonatomic, strong) NSString * comment_count;
@end

@interface FWCarStyleInfoModel : NSObject
@property (nonatomic, strong) NSString * category;
@property (nonatomic, strong) NSString * style;
@property (nonatomic, strong) NSString * car_style_id;
@property (nonatomic, strong) NSString * jinqi;
@property (nonatomic, strong) NSString * pailiang;//
@property (nonatomic, strong) NSString * mali;
@property (nonatomic, strong) NSString * img;
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * type;
@end

@interface FWCarListModel : NSObject
@property (nonatomic, strong) NSString * feed_id;
@property (nonatomic, strong) NSString * flag_hidden;
@property (nonatomic, strong) NSString * flag_hidden_msg;
@property (nonatomic, strong) NSString * car_cover;
@property (nonatomic, strong) NSString * old_type;// 
@property (nonatomic, strong) NSString * car_cover_original;
@property (nonatomic, strong) NSString * car_nickname;
@property (nonatomic, strong) NSString * car_buy_time;
@property (nonatomic, strong) NSString * car_age;
@property (nonatomic, strong) NSString * car_style_id;
@property (nonatomic, strong) NSString * car_style_name;
@property (nonatomic, strong) NSString * car_mali;
@property (nonatomic, strong) NSString * mali_show;
@property (nonatomic, strong) NSString * car_desc;
@property (nonatomic, strong) NSString * user_car_id;
@property (nonatomic, strong) NSString * is_liked;
@property (nonatomic, strong) NSString * is_followed;
@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) NSString * gaizhuang_count;

@property (nonatomic, strong) NSString * share_img;
@property (nonatomic, strong) NSString * share_url;
@property (nonatomic, strong) NSString * share_title;
@property (nonatomic, strong) NSString * share_desc;

@property (nonatomic, strong) NSString * gaizhuang_list;
@property (nonatomic, strong) NSMutableArray<NSString *> * gaizhuang_list_show;

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, strong) FWCarRealTimeModel * count_realtime;
@property (nonatomic, strong) NSMutableArray<FWCarImgsModel *> * car_imgs;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) FWCarStyleInfoModel * car_style_info;
@end

NS_ASSUME_NONNULL_END
