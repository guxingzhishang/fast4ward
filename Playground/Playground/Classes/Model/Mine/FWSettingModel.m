//
//  FWSettingModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/26.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWSettingModel.h"

@implementation FWActivityInfoModel

@end

@implementation FWListADModel

@end

@implementation FWSettingModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list_ad" : NSStringFromClass([FWListADModel class]),
             };
}

@end
