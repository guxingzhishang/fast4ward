//
//  FWBussinessHomePageModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/3/28.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWBussinessProductListModel : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * number;
@property (nonatomic, strong) NSString * img_url;
@property (nonatomic, strong) NSString * width;
@property (nonatomic, strong) NSString * height;

@end


@interface FWBussinessHomePageModel : NSObject
@property (nonatomic, strong) NSString * real_name;
@property (nonatomic, strong) NSString * idcard;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * shop_msg;
@property (nonatomic, strong) NSString * kefu_msg;
@property (nonatomic, strong) NSString * video_buy_1_count;
@property (nonatomic, strong) NSString * video_buy_2_count;
@property (nonatomic, strong) NSString * video_buy_3_count;
@property (nonatomic, strong) NSString * carshadow_status;
@property (nonatomic, strong) NSString * banner_url_show_status;
@property (nonatomic, strong) NSString * banner_url;
@property (nonatomic, strong) NSString * h5_cert;
@property (nonatomic, strong) NSString * h5_carshadow;
@property (nonatomic, strong) NSString * product_msg;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@property (nonatomic, strong) NSMutableArray<FWBussinessProductListModel *> * list_product;

@end

NS_ASSUME_NONNULL_END
