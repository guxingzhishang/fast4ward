//
//  FWLogModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWLogListModel : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * order_number;
@property (nonatomic, strong) NSString * check_number;
@property (nonatomic, strong) NSString * check_time;

@end


@interface FWLogModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWLogListModel *> * list;
@property (nonatomic, strong) NSString * has_more;

@end


NS_ASSUME_NONNULL_END
