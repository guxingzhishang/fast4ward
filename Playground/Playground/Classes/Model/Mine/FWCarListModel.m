//
//  FWCarListModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/7/17.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCarListModel.h"
@implementation FWCarImgsModel
@end

@implementation FWCarRealTimeModel
@end

@implementation FWCarStyleInfoModel
@end

@implementation FWCarListModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"car_imgs" : NSStringFromClass([FWCarImgsModel class]),
             };
}
@end

