//
//  FWSettingModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/26.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWActivityInfoModel : NSObject

@property (nonatomic, strong) NSString * act_val;
@property (nonatomic, strong) NSString * act_type;
@property (nonatomic, strong) NSString * act_id;
@property (nonatomic, strong) NSString * begin_time;
@property (nonatomic, strong) NSString * end_time;
@property (nonatomic, strong) NSString * img_url;

@end


@interface FWListADModel : NSObject

@property (nonatomic, strong) NSString * ad_img;
@property (nonatomic, strong) NSString * ad_id;
@property (nonatomic, strong) NSString * ad_type;
@property (nonatomic, strong) NSString * ad_val; // 值
@end

@interface FWSettingModel : NSObject

@property (nonatomic, strong) NSString * pub_banner;
@property (nonatomic, strong) NSString * service_weixin;
@property (nonatomic, strong) NSString * service_weixin_kefu;
@property (nonatomic, strong) NSString * feedback_notice;
@property (nonatomic, strong) NSString * service_phone;
/* 开屏广告展示时间 */
@property (nonatomic, strong) NSString * startup_time;
@property (nonatomic, strong) NSMutableArray<FWListADModel *> * list_ad;

@property (nonatomic, strong) NSMutableArray * update_notice;
@property (nonatomic, strong) NSString * newest_version;
@property (nonatomic, strong) NSString * update_msg;
@property (nonatomic, strong) NSString * client_version;
// 1 不升级  2 强制升级   3 不强制升级
@property (nonatomic, strong) NSString * update_status;
// 1 展示（非审核阶段）   2 不展示（审核阶段）
@property (nonatomic, strong) NSString * show_update;
@property (nonatomic, strong) FWActivityInfoModel * alert_activity_info;
@property (nonatomic, strong) NSString * h5_cert;
@property (nonatomic, strong) NSString * kefu_uid;
@property (nonatomic, strong) NSString * kefu_nickname;

@end

NS_ASSUME_NONNULL_END
