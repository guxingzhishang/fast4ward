//
//  FWBuyTicketRecordModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/9/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWBuyTicketRecordListModel : NSObject

@property (nonatomic, strong) NSString * order_id;
@property (nonatomic, strong) NSString * ticket_id;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * buy_number;
@property (nonatomic, strong) NSString * pay_fee;
@property (nonatomic, strong) NSString * list_page_img;
@property (nonatomic, strong) NSString * sport_date;
@property (nonatomic, strong) NSString * sport_area;
@property (nonatomic, strong) NSString * pay_status; // 1为未支付，2为已支付
@property (nonatomic, strong) NSString * order_status;// 当前订单状态1未支付2未使用3已使用4订单已失效5门票已过期
@end

@interface FWBuyTicketRecordModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWBuyTicketRecordListModel *> * list;


@end

NS_ASSUME_NONNULL_END
