
//
//  FWLogModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWLogModel.h"


@implementation FWLogListModel
@end


@implementation FWLogModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list" : NSStringFromClass([FWLogListModel class]),
             };
}

@end

