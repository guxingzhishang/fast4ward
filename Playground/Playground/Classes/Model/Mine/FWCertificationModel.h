//
//  FWCertificationModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/3.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWPersonalCertificationListModel : NSObject

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * car_style_id;

@end

@interface FWPersonalCertificationModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWPersonalCertificationListModel*> * list;

@end

@interface FWBussinessCertificationListModel : NSObject

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * main_business_id;

@end

@interface FWBussinessCertificationModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWBussinessCertificationListModel*> * list;

@end

@interface FWCertificationModel : NSObject

@end

NS_ASSUME_NONNULL_END
