//
//  FWTicketsModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/2/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWTicketsModel.h"

@implementation FWTicketsInfoModel

@end

@implementation FWTicketsListModel

@end

@implementation FWTicketsModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"ticket_list" : NSStringFromClass([FWTicketsListModel class]),
             };
}
@end
