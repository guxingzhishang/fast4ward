//
//  FWGoldenLogModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface FWGoldenLogListModel : NSObject
@property (nonatomic, strong) NSString * log_id;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * gold_value;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * op_type;
@property (nonatomic, strong) NSString * op_item_id;
@property (nonatomic, strong) NSString * op_type_str;
@property (nonatomic, strong) FWMineInfoModel * user_info;
@end

@interface FWGoldenLogModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWGoldenLogListModel *> * list;
@end

NS_ASSUME_NONNULL_END
