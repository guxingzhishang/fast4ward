//
//  FWGoldenCheckModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWGoldenCheckModel : NSObject

@property (nonatomic, strong) NSString * f_uid;
@property (nonatomic, strong) NSString * balance_value;
@property (nonatomic, strong) FWMineInfoModel * user_info;

@end

NS_ASSUME_NONNULL_END
