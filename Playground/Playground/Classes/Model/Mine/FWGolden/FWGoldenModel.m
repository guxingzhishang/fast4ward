//
//  FWGoldenModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoldenModel.h"


@implementation FWSignListModel

@end


@implementation FWTaskSubListModel

@end

@implementation FWTaskListModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWTaskSubListModel class]),
    };
}


@end

@implementation FWGoldenListModel

@end


@implementation FWGoldenModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWGoldenListModel class]),
        @"signin_list" : NSStringFromClass([FWSignListModel class]),
        @"task_list" : NSStringFromClass([FWTaskListModel class]),
    };
}

@end
