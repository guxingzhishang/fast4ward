//
//  FWGoldenLogModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/11/16.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWGoldenLogModel.h"

@implementation FWGoldenLogListModel
@end

@implementation FWGoldenLogModel

+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : NSStringFromClass([FWGoldenLogListModel class]),
    };
}

@end
