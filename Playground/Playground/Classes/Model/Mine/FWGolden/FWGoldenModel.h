//
//  FWGoldenModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/31.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface FWSignListModel : NSObject

@property (nonatomic, strong) NSString * available_status_str;
@property (nonatomic, strong) NSString * pre_day ;
@property (nonatomic, strong) NSString * value;
@property (nonatomic, strong) NSString * available_status; // 1 可签， 2 不可签
@property (nonatomic, strong) NSString * sign_status;// 1 已签  2未签
@property (nonatomic, strong) NSString * sign_status_str ;
@property (nonatomic, strong) NSString * index ;

@end


@interface FWTaskSubListModel : NSObject

@property (nonatomic, strong) NSString * icon_id;
@property (nonatomic, strong) NSString * complete_status_str ;
@property (nonatomic, strong) NSString * complete_count;
@property (nonatomic, strong) NSString * value;
@property (nonatomic, strong) NSString * reached_count;
@property (nonatomic, strong) NSString * name ;
@property (nonatomic, strong) NSString * complete_status ; // 1 完成  2 未完成
@property (nonatomic, strong) NSString * get_status ;// 1领取  2未领取


@end

@interface FWTaskListModel : NSObject

@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSMutableArray<FWTaskSubListModel *> * list;

@end

@interface FWGoldenListModel : NSObject

@property (nonatomic, strong) NSString * log_id;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * gold_value;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * update_time;
@property (nonatomic, strong) NSString * op_type;
@property (nonatomic, strong) NSString * op_item_id;
@property (nonatomic, strong) NSString * op_type_str;

@end

@interface FWGoldenModel : NSObject

@property (nonatomic, strong) NSString * total_sum;
@property (nonatomic, strong) NSString * balance_value;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * help_url;
@property (nonatomic, strong) NSMutableArray<FWGoldenListModel *> * list;


@property (nonatomic, strong) NSString * signin_continue_days;
@property (nonatomic, strong) NSMutableArray<FWSignListModel *> * signin_list;
@property (nonatomic, strong) NSMutableArray<FWTaskListModel *> * task_list;

@end

NS_ASSUME_NONNULL_END
