//
//  FWFollowModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/8.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWFollowModel.h"

@implementation FWFollowUserListModel

@end

@implementation FWFollowModel

+(NSDictionary *)mj_objectClassInArray{
    return @{@"user_list" : NSStringFromClass([FWFollowUserListModel class]),
             };
}

@end
