//
//  FWVisitorModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/1/5.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - > ################### 访客分析 ###################

@interface FWVisitorAnalysisListModel : NSObject
@property (nonatomic, strong) NSString * province_code;
@property (nonatomic, strong) NSString * province;
@property (nonatomic, strong) NSString * count;
@property (nonatomic, strong) NSString * car_style_id;
@property (nonatomic, strong) NSString * car_style;
@property (nonatomic, strong) NSString * car_cert_status;
@property (nonatomic, strong) NSString * merchant_cert_status;
@property (nonatomic, strong) NSString * driver_cert_status;
@property (nonatomic, strong) NSString * cert_status;

@end

@interface FWVisitorAnalysisModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWVisitorAnalysisListModel *> * list_visitor;

@end
#pragma mark - > ################### 访客分析 ###################


#pragma mark - > ################### 访客名单 ###################
@interface FWVisitorNameListModel : NSObject

@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * header_url;
@property (nonatomic, strong) NSString * feed_id;
@property (nonatomic, strong) NSString * feed_cover;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSString * last_visit_time;
@property (nonatomic, strong) NSString * msg;
@property (nonatomic, strong) NSString * car_cert_status;
@property (nonatomic, strong) NSString * merchant_cert_status;
@property (nonatomic, strong) NSString * driver_cert_status;
@property (nonatomic, strong) NSString * cert_status;
@end

@interface FWVisitorModel : NSObject

@property (nonatomic, strong) NSString * visitor_count_today;
@property (nonatomic, strong) NSString * visitor_count_all;
@property (nonatomic, strong) NSMutableArray<FWVisitorNameListModel *> * list_visitor;

@property (nonatomic, strong) NSString * count;
@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) NSString * page;

@end
#pragma mark - > ################### 访客名单 ###################

NS_ASSUME_NONNULL_END
