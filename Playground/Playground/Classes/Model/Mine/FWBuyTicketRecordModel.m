//
//  FWBuyTicketRecordModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/9/27.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBuyTicketRecordModel.h"

@implementation FWBuyTicketRecordListModel

@end


@implementation FWBuyTicketRecordModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list" : NSStringFromClass([FWBuyTicketRecordListModel class]),
             };
}
@end
