//
//  FWAttentionRefitTopicModel.h
//  Playground
//
//  Created by 孤星之殇 on 2020/1/12.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface FWAttentionRefitTopicUserModel: NSObject
@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * header_url;
@property (nonatomic, assign) BOOL  isSelectUser;
@end

@interface FWAttentionRefitTopicListModel : NSObject
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * tag_name;
@property (nonatomic, strong) NSString * count_text;
@property (nonatomic, strong) NSString * img_url;
@property (nonatomic, assign) BOOL  isShowAll;
@property (nonatomic, strong) NSMutableArray<FWAttentionRefitTopicUserModel *> * user_list;

@end

@interface FWAttentionRefitTopicModel : NSObject
@property (nonatomic, strong) NSMutableArray<FWAttentionRefitTopicListModel *> * tag_list;

@end

NS_ASSUME_NONNULL_END
