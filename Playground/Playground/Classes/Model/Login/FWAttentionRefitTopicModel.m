//
//  FWAttentionRefitTopicModel.m
//  Playground
//
//  Created by 孤星之殇 on 2020/1/12.
//  Copyright © 2020 孤星之殇. All rights reserved.
//

#import "FWAttentionRefitTopicModel.h"

@implementation FWAttentionRefitTopicUserModel
@end

@implementation FWAttentionRefitTopicListModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"user_list" : NSStringFromClass([FWAttentionRefitTopicUserModel class]),
    };
}
@end

@implementation FWAttentionRefitTopicModel
+(NSDictionary *)mj_objectClassInArray{
    return @{
        @"tag_list" : NSStringFromClass([FWAttentionRefitTopicListModel class]),
    };
}
@end
