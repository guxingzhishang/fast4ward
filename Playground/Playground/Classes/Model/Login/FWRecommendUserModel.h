//
//  FWRecommendUserModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/10/1.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWRecommendUserModel : NSObject

@property (nonatomic, copy) NSString * uid;
@property (nonatomic, copy) NSString * nickname;
@property (nonatomic, copy) NSString * mobile_status;
@property (nonatomic, copy) NSString * sex;
@property (nonatomic, copy) NSString * user_type;
@property (nonatomic, copy) NSString * autograph;
@property (nonatomic, copy) NSString * header_url;
@property (nonatomic, assign) BOOL isSelect;

@end
