//
//  FWLoginModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/30.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWLoginModel : NSObject

@property (nonatomic, copy) NSString * login_type;
@property (nonatomic, copy) NSString * next_page;
@property (nonatomic, copy) NSString * sync_weixin_header; // 1需要同步微信头像 2不需要

@end
