//
//  FWActivityModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/12/12.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWActivityRankListModel : NSObject

@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * img;
@property (nonatomic, strong) NSString * qrcode_url;

@end

@interface FWActivityBannerModel : NSObject

@property (nonatomic, strong) NSString * banner_id;
@property (nonatomic, strong) NSString * banner_type;
@property (nonatomic, strong) NSString * banner_val;
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * img_url;
@property (nonatomic, strong) NSString * h5_url;

@end

@interface FWActivityModel : NSObject

@property (nonatomic, strong) NSString * page;
@property (nonatomic, strong) NSString * page_size;
@property (nonatomic, strong) NSString * total_count;
@property (nonatomic, strong) NSString * has_more;
@property (nonatomic, strong) FWMineInfoModel * car_top1;
@property (nonatomic, strong) FWMineInfoModel * merchant_top1;
@property (nonatomic, strong) NSMutableArray<FWCarListModel *> * list_car;
@property (nonatomic, strong) NSMutableArray<FWFeedListModel *> * feed_list;
@property (nonatomic, strong) NSMutableArray<FWTagsInfoModel *> * list_tag;
@property (nonatomic, strong) NSMutableArray<FWActivityRankListModel *> * rank_list;
@property (nonatomic, strong) NSMutableArray<FWActivityBannerModel*> * list_banner;


@end

NS_ASSUME_NONNULL_END
