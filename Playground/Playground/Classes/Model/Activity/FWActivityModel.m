//
//  FWActivityModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/12/12.
//  Copyright © 2018 孤星之殇. All rights reserved.
//

#import "FWActivityModel.h"

@implementation FWActivityRankListModel
@end

@implementation FWActivityBannerModel

@end

@implementation FWActivityModel

+ (NSDictionary *)mj_objectClassInArray {
    
    return @{
             @"feed_list" : NSStringFromClass([FWFeedListModel class]),
             @"list_banner" : NSStringFromClass([FWActivityBannerModel class]),
             @"rank_list" : NSStringFromClass([FWActivityRankListModel class]),
             @"list_tag" : NSStringFromClass([FWTagsInfoModel class]),
             @"list_car" : NSStringFromClass([FWCarListModel class]),
             };
}

@end
