//
//  FWTopRankModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/5/21.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FWTopRankModel : NSObject

@property (nonatomic, strong) NSMutableArray<FWMineInfoModel *> * list_user;
@property (nonatomic, strong) NSString * qrcode_url;
@property (nonatomic, strong) NSString * desc;

@end

NS_ASSUME_NONNULL_END
