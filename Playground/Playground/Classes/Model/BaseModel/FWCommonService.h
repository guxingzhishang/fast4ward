//
//  FWCommonService.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWUserDefaultsVariableModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWCommonService : NSObject

#pragma mark - 更新UserDefaultsVariableModel中关于NSUserDefaults的值
/**
 * @brief 更新UserDefaultsVariableModel中关于NSUserDefaults的值
 *
 * @param  udvModel UserDefaultsVariableModel
 *
 */
+(void)updateUserDefaultsVariableModelToNSUserDefaults:(FWUserDefaultsVariableModel *)udvModel;
#pragma mark - 清除UserDefaultsVariableModel中关于NSUserDefaults的值
/**
 * @brief 清除UserDefaultsVariableModel中关于NSUserDefaults的值
 *
 */
+(void)clearUserDefaultsVariableModelFromNSUserDefaults;
#pragma mark - 获取UserDefaultsVariableModel中关于NSUserDefaults的值
/**
 * @brief 获取UserDefaultsVariableModel中关于NSUserDefaults的值
 *
 * @return UserDefaultsVariableModel
 */
+(FWUserDefaultsVariableModel *)getUserDefaultsVariableModelFromNSUserDefaults;

@end

NS_ASSUME_NONNULL_END
