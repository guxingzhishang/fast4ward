//
//  FWUserDefaultsVariableModel.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWBaseModel.h"
#import "FWUserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FWUserDefaultsVariableModel : FWBaseModel

@property(nonatomic,strong) FWUserModel *userModel;//用户信息

@property(nonatomic,strong) NSString *ticket_check;
@property(nonatomic,strong) NSString *qiandao_check; // 值为1代表有签到权限
@property(nonatomic,strong) NSString *jianlu_check; // 值为1代表有检录权限
@property(nonatomic,strong) NSString *gold_status; // 值1 显示红点（领取任务金币的提醒） 2不显示。
@property(nonatomic,strong) NSString *gold_write_off_status;// 值为1代表有检录权限

@end

NS_ASSUME_NONNULL_END
