//
//  FWUserCenter.h
//  Playground
//
//  Created by 孤星之殇 on 2019/4/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FWUserModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FWUserCenter : NSObject

@property (nonatomic, strong) NSString * uid;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSString * mobile_status;
@property (nonatomic, strong) NSString * sex;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * user_type;
@property (nonatomic, strong) NSString * car_cert_status;
@property (nonatomic, strong) NSString * merchant_cert_status;
@property (nonatomic, strong) NSString * driver_cert_status;
@property (nonatomic, strong) NSString * cert_status;
@property (nonatomic, strong) NSString * autograph;
@property (nonatomic, strong) NSString * user_level;
@property (nonatomic, strong) NSString * cert_name;
@property (nonatomic, strong) NSString * shop_status;
@property (nonatomic, strong) NSString * level_name;
@property (nonatomic, strong) NSString * f4w_id;
@property (nonatomic, strong) NSString * effect_value;
@property (nonatomic, strong) NSString * header_url;
@property (nonatomic, strong) NSString * header_url_original;
@property (nonatomic, strong) NSString * share_url;
@property (nonatomic, strong) NSString * share_title;
@property (nonatomic, strong) NSString * share_desc;
@property (nonatomic, strong) NSString * max_video_duration;
@property (nonatomic, strong) NSString * weixin_nickname;
@property (nonatomic, strong) NSString * car_style;
@property (nonatomic, strong) NSString * main_business;
@property (nonatomic, strong) NSString * top10_1;
@property (nonatomic, strong) NSString * top10_2;
@property (nonatomic, strong) NSString * show_effect;
@property (nonatomic, strong) NSString * weixin_bind_status;
@property (nonatomic, strong) NSString * dashen_title;
@property (nonatomic, strong) NSString * mobile_encode;
@property (nonatomic, strong) NSString * uid_enc;
@property (nonatomic, strong) NSString * shop_address;

@property (nonatomic, assign) BOOL user_IsLogin;

@property (nonatomic, strong) NSString * ticket_check;
@property (nonatomic, strong) NSString * qiandao_check;
@property (nonatomic, strong) NSString * jianlu_check;
@property (nonatomic, strong) NSString * gold_status;
@property (nonatomic, strong) NSString * gold_write_off_status;
#pragma mark - > 保存用户数据到本地

/**
 保存用户信息到本地
 */
- (void)saveUserInfoToDiskWithJson:(NSDictionary *)json;
- (void)clearAllPreviousUserInfo;// 清空本地用户原先数据
+ (instancetype)sharedManager;

@end

NS_ASSUME_NONNULL_END
