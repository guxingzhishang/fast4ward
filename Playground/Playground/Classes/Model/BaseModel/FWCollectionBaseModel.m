//
//  FWCollectionBaseModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWCollectionBaseModel.h"

@implementation FWCollectionBaseModel

+ (NSDictionary *)replacedKeyFromPropertyName {
    return @{
             
             @"url" : @"images_list[0].url",
             @"height" : @"images_list[0].height",
             @"width" : @"images_list[0].width",
             @"nickname" : @"user.nickname",
             @"images" : @"user.images"
             };
};

@end
