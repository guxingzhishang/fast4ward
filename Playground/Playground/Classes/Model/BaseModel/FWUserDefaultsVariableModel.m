//
//  FWUserDefaultsVariableModel.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUserDefaultsVariableModel.h"

@implementation FWUserDefaultsVariableModel

- (FWUserModel *)userModel {
    if (_userModel == nil) {
        _userModel = [[FWUserModel alloc] init];
    }
    return _userModel;
}

@end
