//
//  FWALiYunCenter.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/10.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWALiYunCenter.h"

@implementation FWALiYunCenter

+ (instancetype)sharedManager{
    static FWALiYunCenter *center = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        center = [[FWALiYunCenter alloc] init];
    });
    return center;
}

@end
