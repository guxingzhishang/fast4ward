//
//  FWCommonService.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWCommonService.h"
#define NSUserDefaultKey  @"FWUserDefaultsVariableModelV1.2.0"

static NSLock *_lock;

@implementation FWCommonService

#pragma mark - 更新UserDefaultsVariableModel中关于NSUserDefaults的值
/**
 * @brief 更新UserDefaultsVariableModel中关于NSUserDefaults的值
 *
 * @param  udvModel UserDefaultsVariableModel
 *
 */
+(void)updateUserDefaultsVariableModelToNSUserDefaults:(FWUserDefaultsVariableModel *)udvModel{
    if (!_lock) {
        _lock =  [[NSLock alloc] init];
    }
    [_lock lock];
    
    FWUserDefaultsVariableModel *tempModel = [self getUserDefaultsVariableModelFromNSUserDefaults];
    /*
     By running all member variables when traversing category
     
     */
    //  Get the current class type
    Class cls = [udvModel class];
    unsigned int ivarsCnt = 0;
    //　Get the class member variables list, ivarsCnt the number of class members
    Ivar *ivars = class_copyIvarList(cls, &ivarsCnt);
    //　Traverse list of member variables, each of which variables are the type of structure Ivar
    for (const Ivar *p = ivars; p < ivars + ivarsCnt; ++p){
        Ivar const ivar = *p;
        // Get the variable name
        NSString *key = [NSString stringWithUTF8String:ivar_getName(ivar)];
        // If this variable is not declared but only declared Property, the variable name prefix in the class structure of the '_' underscore
        // Such asproperty (retain) NSString * abc; then key == _abc;
        // Get the variable value
        id value = [udvModel valueForKey:key];
        // Get the variable type
        // By type [0] can determine their specific built-in types
        const char *type = ivar_getTypeEncoding(ivar);
        if (value){
            [tempModel setValue:value forKey:key];
        }
    }
    //
    NSData *globalVariableModelEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:tempModel];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUserDefaultKey];
    [[NSUserDefaults standardUserDefaults] setValue:globalVariableModelEncodedObject forKey:NSUserDefaultKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_lock unlock];
}
#pragma mark - 清除UserDefaultsVariableModel中关于NSUserDefaults的值
/**
 * @brief 清除UserDefaultsVariableModel中关于NSUserDefaults的值
 *
 */
+(void)clearUserDefaultsVariableModelFromNSUserDefaults{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUserDefaultKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
#pragma mark - 获取UserDefaultsVariableModel中关于NSUserDefaults的值
/**
 * @brief 获取UserDefaultsVariableModel中关于NSUserDefaults的值
 *
 * @return UserDefaultsVariableModel
 */
+(FWUserDefaultsVariableModel *)getUserDefaultsVariableModelFromNSUserDefaults{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *globalVariableModelEncodedObject = [defaults objectForKey:NSUserDefaultKey];
    FWUserDefaultsVariableModel *obj = [[FWUserDefaultsVariableModel alloc] init];
    if (globalVariableModelEncodedObject) {
        obj = (FWUserDefaultsVariableModel *)[NSKeyedUnarchiver unarchiveObjectWithData: globalVariableModelEncodedObject];
    }
    return obj;
}
@end
