//
//  FWUserCenter.m
//  Playground
//
//  Created by 孤星之殇 on 2019/4/29.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWUserCenter.h"
#import "FWUserDefaultsVariableModel.h"
#import "FWCommonService.h"
@implementation FWUserCenter

+ (instancetype)sharedManager{
    static FWUserCenter *center = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        center = [[FWUserCenter alloc] init];
    });
    return center;
}

- (void)setUid:(NSString *)uid{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.uid = uid;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setNickname:(NSString *)nickname{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.nickname = nickname;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setTitle:(NSString *)title{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.title = title;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setDesc:(NSString *)desc{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.desc = desc;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setMobile_status:(NSString *)mobile_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.mobile_status = mobile_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setSex:(NSString *)sex{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.sex = sex;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setUser_type:(NSString *)user_type{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.user_type = user_type;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setCar_cert_status:(NSString *)car_cert_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.car_cert_status = car_cert_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setMerchant_cert_status:(NSString *)merchant_cert_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.merchant_cert_status = merchant_cert_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setDriver_cert_status:(NSString *)driver_cert_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.driver_cert_status = driver_cert_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setAutograph:(NSString *)autograph{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.autograph = autograph;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setCity:(NSString *)city{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.city = city;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setUser_level:(NSString *)user_level{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.user_level = user_level;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setCert_status:(NSString *)cert_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.cert_status = cert_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setCert_name:(NSString *)cert_name{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.cert_name = cert_name;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setShop_status:(NSString *)shop_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.shop_status = shop_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setLevel_name:(NSString *)level_name{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.level_name = level_name;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setF4w_id:(NSString *)f4w_id{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.f4w_id = f4w_id;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setEffect_value:(NSString *)effect_value{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.effect_value = effect_value;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setHeader_url:(NSString *)header_url{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.header_url = header_url;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setHeader_url_original:(NSString *)header_url_original{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.header_url_original = header_url_original;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setShare_url:(NSString *)share_url{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.share_url = share_url;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setShare_title:(NSString *)share_title{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.share_title = share_title;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setShare_desc:(NSString *)share_desc{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.share_desc = share_desc;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setMax_video_duration:(NSString *)max_video_duration{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.max_video_duration = max_video_duration;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setWeixin_nickname:(NSString *)weixin_nickname{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.weixin_nickname = weixin_nickname;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setCar_style:(NSString *)car_style{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.car_style = car_style;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setMain_business:(NSString *)main_business{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.main_business = main_business;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setTop10_1:(NSString *)top10_1{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.top10_1 = top10_1;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setGold_status:(NSString *)gold_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.gold_status = gold_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setTop10_2:(NSString *)top10_2{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.top10_2 = top10_2;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setShow_effect:(NSString *)show_effect{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.show_effect = show_effect;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setWeixin_bind_status:(NSString *)weixin_bind_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.weixin_bind_status = weixin_bind_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setDashen_title:(NSString *)dashen_title{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.dashen_title = dashen_title;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setMobile_encode:(NSString *)mobile_encode{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.mobile_encode = mobile_encode;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setUid_enc:(NSString *)uid_enc{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.uid_enc = uid_enc;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setShop_address:(NSString *)shop_address{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.userModel.shop_address = shop_address;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}


- (void)setQiandao_check:(NSString *)qiandao_check{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.qiandao_check = qiandao_check;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setJianlu_check:(NSString *)jianlu_check{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.jianlu_check = jianlu_check;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setTicket_check:(NSString *)ticket_check{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.ticket_check = ticket_check;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)setGold_write_off_status:(NSString *)gold_write_off_status{
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    udvm.gold_write_off_status = gold_write_off_status;
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}

- (void)clearAllPreviousUserInfo {
    
//    [[SDImageCache sharedImageCache] removeImageForKey:[FWUserCenter sharedManager].FWUser_UserHeaderIcon withCompletion:nil];
    
    FWUserDefaultsVariableModel *udvm = [FWCommonService getUserDefaultsVariableModelFromNSUserDefaults];
    
    udvm.ticket_check = @"";
    udvm.qiandao_check = @"";
    udvm.jianlu_check = @"";
    udvm.gold_write_off_status = @"";
    udvm.gold_status = @"";

    udvm.userModel.user_IsLogin = NO;

    udvm.userModel.uid = @"";
    udvm.userModel.nickname = @"";
    udvm.userModel.title = @"";
    udvm.userModel.desc = @"";
    udvm.userModel.mobile_status = @"";
    udvm.userModel.sex = @"";
    udvm.userModel.user_type = @"";
    udvm.userModel.car_cert_status = @"";
    udvm.userModel.merchant_cert_status = @"";
    udvm.userModel.driver_cert_status = @"";
    udvm.userModel.autograph = @"";
    udvm.userModel.user_level = @"";
    udvm.userModel.cert_status = @"";
    udvm.userModel.cert_name = @"";
    udvm.userModel.shop_status = @"";
    udvm.userModel.level_name = @"";
    udvm.userModel.f4w_id = @"";
    udvm.userModel.effect_value = @"";
    udvm.userModel.header_url = @"";
    udvm.userModel.header_url_original = @"";
    udvm.userModel.share_url = @"";
    udvm.userModel.share_desc = @"";
    udvm.userModel.share_title = @"";
    udvm.userModel.max_video_duration = @"";
    udvm.userModel.weixin_nickname = @"";
    udvm.userModel.car_style = @"";
    udvm.userModel.main_business = @"";
    udvm.userModel.top10_1 = @"";
    udvm.userModel.top10_2 = @"";
    udvm.userModel.show_effect = @"";
    udvm.userModel.weixin_bind_status = @"";
    udvm.userModel.dashen_title = @"";
    udvm.userModel.mobile_encode = @"";
    udvm.userModel.uid_enc = @"";
    
    [FWCommonService updateUserDefaultsVariableModelToNSUserDefaults:udvm];
}


- (void)saveUserInfoToDiskWithJson:(NSDictionary *)json {
    
    NSDictionary *member = HYGET_OBJECT_FROMDIC(json, @"data", @"user_info");
    
    if(!member) return;
    
    [FWUserCenter sharedManager].uid = HYGET_OBJECT_FROMDIC(member, @"uid");// 用户ID
    [FWUserCenter sharedManager].nickname= HYGET_OBJECT_FROMDIC(member, @"nickname");// 昵称
    [FWUserCenter sharedManager].title = HYGET_OBJECT_FROMDIC(member, @"title");//
    [FWUserCenter sharedManager].city = HYGET_OBJECT_FROMDIC(member, @"city");//
    [FWUserCenter sharedManager].desc = HYGET_OBJECT_FROMDIC(member, @"desc");//
    [FWUserCenter sharedManager].mobile_status = HYGET_OBJECT_FROMDIC(member, @"mobile_status");//
    [FWUserCenter sharedManager].sex = HYGET_OBJECT_FROMDIC(member, @"sex");
    [FWUserCenter sharedManager].user_type = HYGET_OBJECT_FROMDIC(member, @"user_type");//
    [FWUserCenter sharedManager].car_cert_status = HYGET_OBJECT_FROMDIC(member, @"car_cert_status");//
    [FWUserCenter sharedManager].merchant_cert_status = HYGET_OBJECT_FROMDIC(member, @"merchant_cert_status");//
    [FWUserCenter sharedManager].driver_cert_status = HYGET_OBJECT_FROMDIC(member, @"driver_cert_status");//
    [FWUserCenter sharedManager].autograph = HYGET_OBJECT_FROMDIC(member, @"autograph");//
    [FWUserCenter sharedManager].user_level = HYGET_OBJECT_FROMDIC(member, @"user_level");//
    [FWUserCenter sharedManager].cert_status = HYGET_OBJECT_FROMDIC(member, @"cert_status");//
    [FWUserCenter sharedManager].cert_name = HYGET_OBJECT_FROMDIC(member, @"cert_name");//
    [FWUserCenter sharedManager].shop_status = HYGET_OBJECT_FROMDIC(member, @"shop_status");//
    [FWUserCenter sharedManager].level_name = HYGET_OBJECT_FROMDIC(member, @"level_name");//
    [FWUserCenter sharedManager].f4w_id = HYGET_OBJECT_FROMDIC(member, @"f4w_id");//
    [FWUserCenter sharedManager].effect_value = HYGET_OBJECT_FROMDIC(member, @"effect_value");
    [FWUserCenter sharedManager].header_url = HYGET_OBJECT_FROMDIC(member, @"header_url");//
    [FWUserCenter sharedManager].header_url_original= HYGET_OBJECT_FROMDIC(member, @"header_url_original");//
    [FWUserCenter sharedManager].share_url = HYGET_OBJECT_FROMDIC(member, @"share_url");//
    [FWUserCenter sharedManager].share_title = HYGET_OBJECT_FROMDIC(member, @"share_title");//
    [FWUserCenter sharedManager].share_desc = HYGET_OBJECT_FROMDIC(member, @"share_desc");//
    [FWUserCenter sharedManager].max_video_duration = HYGET_OBJECT_FROMDIC(member, @"max_video_duration");
    [FWUserCenter sharedManager].weixin_nickname = HYGET_OBJECT_FROMDIC(member, @"weixin_nickname");//
    [FWUserCenter sharedManager].car_style = HYGET_OBJECT_FROMDIC(member, @"car_style");//
    [FWUserCenter sharedManager].main_business = HYGET_OBJECT_FROMDIC(member, @"main_business");//
    [FWUserCenter sharedManager].top10_1 = HYGET_OBJECT_FROMDIC(member, @"top10_1");//
    [FWUserCenter sharedManager].top10_2 = HYGET_OBJECT_FROMDIC(member, @"top10_2");//
    [FWUserCenter sharedManager].show_effect = HYGET_OBJECT_FROMDIC(member, @"show_effect");//
    [FWUserCenter sharedManager].weixin_bind_status = HYGET_OBJECT_FROMDIC(member, @"weixin_bind_status");//
    [FWUserCenter sharedManager].dashen_title = HYGET_OBJECT_FROMDIC(member, @"dashen_title");//
    [FWUserCenter sharedManager].mobile_encode = HYGET_OBJECT_FROMDIC(member, @"mobile_encode");//
    [FWUserCenter sharedManager].uid_enc = HYGET_OBJECT_FROMDIC(member, @"uid_enc");//
    [FWUserCenter sharedManager].shop_address = HYGET_OBJECT_FROMDIC(member, @"shop_address");//
    
    
    NSDictionary *dataInfo = HYGET_OBJECT_FROMDIC(json, @"data");
       
    if(!dataInfo) return;

    [FWUserCenter sharedManager].ticket_check = HYGET_OBJECT_FROMDIC(dataInfo, @"ticket_check");// 用户ID
    [FWUserCenter sharedManager].qiandao_check= HYGET_OBJECT_FROMDIC(dataInfo, @"qiandao_check");// 昵称
    [FWUserCenter sharedManager].jianlu_check = HYGET_OBJECT_FROMDIC(dataInfo, @"jianlu_check");//
    [FWUserCenter sharedManager].gold_write_off_status = HYGET_OBJECT_FROMDIC(dataInfo, @"gold_write_off_status");//
    [FWUserCenter sharedManager].gold_status = HYGET_OBJECT_FROMDIC(dataInfo, @"gold_status");//
}

@end
