
//
//  FWUserModel.m
//  Playground
//
//  Created by 孤星之殇 on 2018/10/10.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import "FWUserModel.h"

@implementation FWUserModel

#pragma mark - > 模型转换映射表

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{@"uid":@"data.user_info.uid",
             @"nickname":@"data.user_info.nickName",
             @"city":@"data.user_info.city",
             @"title":@"data.user_info.title",
             @"desc":@"data.user_info.desc",
             @"mobile_status":@"data.user_info.mobile_status",
             @"sex":@"data.user_info.sex",
             @"user_type":@"data.user_info.user_type",
             @"car_cert_status":@"data.user_info.car_cert_status",
             @"merchant_cert_status":@"data.user_info.merchant_cert_status",
             @"driver_cert_status":@"data.user_info.driver_cert_status",
             @"autograph":@"data.user_info.autograph",
             @"user_level":@"data.user_info.user_level",// intVlaue
             @"cert_status":@"data.user_info.cert_status",
             @"cert_name":@"data.user_info.cert_name",
             @"shop_status":@"data.user_info.shop_status",// 未找到
             @"level_name":@"data.user_info.level_name",
             @"f4w_id":@"data.user_info.f4w_id",
             @"effect_value":@"data.user_info.effect_value",
             @"header_url":@"data.user_info.header_url",
             @"header_url_original":@"data.user_info.header_url_original",
             @"share_url":@"data.user_info.share_url",
             @"share_title":@"data.user_info.share_title",// 未找到
             @"share_desc":@"data.user_info.share_desc",// 未找到
             @"max_video_duration":@"data.user_info.max_video_duration",
             @"weixin_nickname":@"data.user_info.weixin_nickname",
             @"car_style":@"data.user_info.car_style",
             @"main_business":@"data.user_info.main_business",
             @"top10_1":@"data.user_info.top10_1",
             @"top10_2":@"data.user_info.top10_2",
             @"show_effect":@"data.user_info.show_effect",
             @"weixin_bind_status":@"data.user_info.weixin_bind_status",// 未找到
             @"dashen_title":@"data.user_info.dashen_title",
             @"mobile_encode":@"data.user_info.mobile_encode",
             @"uid_enc":@"data.user_info.uid_enc",
             @"shop_address":@"data.user_info.shop_address",
             };
}

-(instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        
        if ([dict allKeys] != nil) {
            self.user_IsLogin=YES;
        }else{
            self.user_IsLogin=NO;
            self.uid=@"";
            self.nickname=@"";
            self.title=@"";
            self.desc=@"";
            self.mobile_status=@"";
            self.sex=@"";
            self.user_type = @"";
            self.car_cert_status = @"";
            self.merchant_cert_status = @"";
            self.driver_cert_status = @"";
            self.autograph=@"";
            self.user_level=@"";
            self.cert_status=@"";
            self.shop_status=@"";
            self.level_name=@"";
            self.f4w_id=@"";
            self.effect_value=@"";
            self.header_url=@"";
            self.header_url_original=@"";
            self.share_url=@"";
            self.share_title=@"";
            self.share_desc=@"";
            self.max_video_duration=@"";
            self.weixin_nickname=@"";
            self.car_style=@"";
            self.main_business=@"";
            self.top10_1=@"";
            self.top10_2=@"";
            self.show_effect=@"";
            self.weixin_bind_status=@"";
            self.dashen_title = @"";
            self.mobile_encode=@"";
            self.uid_enc=@"";
            self.shop_address=@"";
        }
        
        self.uid =[NSString stringWithFormat:@"%@",dict[@"uid"]!=nil?dict[@"uid"]:@""];
        self.dashen_title =[NSString stringWithFormat:@"%@",dict[@"dashen_title"]!=nil?dict[@"dashen_title"]:@""];
        self.nickname =[NSString stringWithFormat:@"%@",dict[@"nickname"]!=nil?dict[@"nickname"]:@""];
        self.title= [NSString stringWithFormat:@"%@",dict[@"title"]!=nil?dict[@"title"]:@""];
        self.desc = [NSString stringWithFormat:@"%@",dict[@"desc"]!=nil?dict[@"desc"]:@""];
        self.mobile_status = [NSString stringWithFormat:@"%@",dict[@"mobile_status"]!=nil?dict[@"mobile_status"]:@""];
        self.sex = [NSString stringWithFormat:@"%@",dict[@"sex"]!=nil?dict[@"sex"]:@""];
        self.user_type = [NSString stringWithFormat:@"%@",dict[@"user_type"]!=nil?dict[@"user_type"]:@""];
        self.car_cert_status = [NSString stringWithFormat:@"%@",dict[@"car_cert_status"]!=nil?dict[@"car_cert_status"]:@""];
        self.merchant_cert_status = [NSString stringWithFormat:@"%@",dict[@"merchant_cert_status"]!=nil?dict[@"merchant_cert_status"]:@""];
        self.driver_cert_status = [NSString stringWithFormat:@"%@",dict[@"driver_cert_status"]!=nil?dict[@"driver_cert_status"]:@""];
        self.autograph = [NSString stringWithFormat:@"%@",dict[@"autograph"]!=nil?dict[@"autograph"]:@""];
        self.user_level =  [NSString stringWithFormat:@"%@",dict[@"user_level"]!=nil?dict[@"user_level"]:@""];
        self.cert_status =  [NSString stringWithFormat:@"%@",dict[@"cert_status"]!=nil?dict[@"cert_status"]:@""];
        self.cert_name =  [NSString stringWithFormat:@"%@",dict[@"cert_name"]!=nil?dict[@"cert_name"]:@""];
        self.shop_status =  [NSString stringWithFormat:@"%@",dict[@"shop_status"]!=nil?dict[@"shop_status"]:@""];
        self.level_name =  [NSString stringWithFormat:@"%@",dict[@"level_name"]!=nil?dict[@"level_name"]:@""];
        self.f4w_id =  [NSString stringWithFormat:@"%@",dict[@"f4w_id"]!=nil?dict[@"f4w_id"]:@""];
        self.effect_value =  [NSString stringWithFormat:@"%@",dict[@"effect_value"]!=nil?dict[@"effect_value"]:@""];
        self.header_url =  [NSString stringWithFormat:@"%@",dict[@"header_url"]!=nil?dict[@"header_url"]:@""];
        self.header_url_original =  [NSString stringWithFormat:@"%@",dict[@"header_url_original"]!=nil?dict[@"header_url_original"]:@""];
        self.share_url =  [NSString stringWithFormat:@"%@",dict[@"share_url"]!=nil?dict[@"share_url"]:@""];
        self.share_title =  [NSString stringWithFormat:@"%@",dict[@"share_title"]!=nil?dict[@"share_title"]:@""];
        self.share_desc =  [NSString stringWithFormat:@"%@",dict[@"share_desc"]!=nil?dict[@"share_desc"]:@""];
        self.max_video_duration =  [NSString stringWithFormat:@"%@",dict[@"max_video_duration"]!=nil?dict[@"max_video_duration"]:@""];
        self.weixin_nickname =  [NSString stringWithFormat:@"%@",dict[@"weixin_nickname"]!=nil?dict[@"weixin_nickname"]:@""];
        self.car_style =  [NSString stringWithFormat:@"%@",dict[@"car_style"]!=nil?dict[@"car_style"]:@""];
        self.main_business =  [NSString stringWithFormat:@"%@",dict[@"main_business"]!=nil?dict[@"main_business"]:@""];
        self.top10_1 =  [NSString stringWithFormat:@"%@",dict[@"top10_1"]!=nil?dict[@"top10_1"]:@""];
        self.top10_2 =  [NSString stringWithFormat:@"%@",dict[@"top10_2"]!=nil?dict[@"top10_2"]:@""];
        self.show_effect =  [NSString stringWithFormat:@"%@",dict[@"show_effect"]!=nil?dict[@"show_effect"]:@""];
        self.weixin_bind_status =  [NSString stringWithFormat:@"%@",dict[@"weixin_bind_status"]!=nil?dict[@"weixin_bind_status"]:@""];
        self.mobile_encode =  [NSString stringWithFormat:@"%@",dict[@"mobile_encode"]!=nil?dict[@"mobile_encode"]:@""];
        self.uid_enc =  [NSString stringWithFormat:@"%@",dict[@"uid_enc"]!=nil?dict[@"uid_enc"]:@""];
        self.shop_address =  [NSString stringWithFormat:@"%@",dict[@"shop_address"]!=nil?dict[@"shop_address"]:@""];
    }
    return self;
}


@end
