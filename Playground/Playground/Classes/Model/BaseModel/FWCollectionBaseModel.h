//
//  FWCollectionBaseModel.h
//  Playground
//
//  Created by 孤星之殇 on 2018/9/25.
//  Copyright © 2018年 孤星之殇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWCollectionBaseModel : NSObject

@property(nonatomic, copy) NSString *url;

@property(nonatomic, copy) NSString *nickname;

@property(nonatomic, copy) NSString *title;

@property(nonatomic, copy) NSString *desc;

@property(nonatomic, copy) NSString *images;

@property(nonatomic, copy) NSString *likes;

@property(nonatomic, copy) NSString *height;

@property(nonatomic, copy) NSString *width;

@end
