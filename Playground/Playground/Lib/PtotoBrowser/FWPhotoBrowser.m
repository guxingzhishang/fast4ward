//
//  FWPhotoBrowser.m
//  Playground
//
//  Created by 孤星之殇 on 2019/10/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import "FWPhotoBrowser.h"
#import "UIImageView+WebCache.h"
#import "FWPhotoBrowserView.h"
#import "FWPhotoBrowserConfig.h"
#import "SharePicPopView.h"
#import "UIImage+Extension.h"
#import <SDWebImageDownloader.h>
#import <ImageIO/ImageIO.h>

@interface FWPhotoBrowser()
@property (nonatomic,strong) UITapGestureRecognizer *singleTap;
@property (nonatomic,strong) UITapGestureRecognizer *doubleTap;
@property (nonatomic,strong) UIPanGestureRecognizer *pan;
@property (nonatomic,strong) UILongPressGestureRecognizer *longPressTap;
@property (nonatomic,strong) UIImageView *tempView;
@property (nonatomic,strong) UIView *coverView;
@property (nonatomic,strong) FWPhotoBrowserView *photoBrowserView;
@property (nonatomic,assign) UIDeviceOrientation orientation;
@property (nonatomic,assign) FWPhotoBrowserStyle photoBrowserStyle;
@property (nonatomic,strong) UIActivityIndicatorView * loading;
@property (nonatomic,strong) UIButton * shareButton;

@property (nonatomic, strong) NSMutableArray * sizeArray;// 用来记录当前图片大小

// 是否展示，展示前禁止再次执行分享操作。
@property (nonatomic, assign) BOOL  isShareViewShowing;
// 当前长按是否结束
@property (nonatomic, assign) BOOL  isLongPress;

@property (nonatomic,strong) NSDate * startDare;

@property (nonatomic, assign) NSInteger  currentIndex;

@property (nonatomic, strong) UIImage * currentImage;

@end

@implementation FWPhotoBrowser
{
    UIScrollView *_scrollView;
    BOOL _hasShowedFistView;
    UIView *_contentView;
    
    UILabel * _numLabel;
    UIButton * _saveButton;
    UILabel * _lookOriginalLabel;
}

- (NSMutableArray *)sizeArray{
    
    if (!_sizeArray) {
        _sizeArray = [[NSMutableArray alloc] init];
    }
    
    return _sizeArray;
}

#pragma mark recyle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.currentIndex = -1;
        
        //异步并列执行
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSMutableArray * tempArr = @[].mutableCopy;
            for (int i = 0; i<self.imageArray.count; i++) {
                [tempArr addObject:@""];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.sizeArray addObjectsFromArray:tempArr];
                NSLog(@"------%lu",(unsigned long)self.sizeArray.count);
            });
        });
        
        self.backgroundColor = FWPhotoBrowserBackgrounColor;
        self.isFullWidthForLandScape = YES;
        self.isNeedLandscape = YES;
    }
    return self;
}

//当视图移动完成后调用
- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    
    //处理下标可能越界的bug
    _currentImageIndex = _currentImageIndex < 0 ? 0 : _currentImageIndex;
    NSInteger count = _imageCount - 1;
    if (count > 0) {
        if (_currentImageIndex > count) {
            _currentImageIndex = 0;
        }
    }
    [self setupScrollView];
    [self setupToolbars];
    [self addGestureRecognizer:self.singleTap];
    [self addGestureRecognizer:self.doubleTap];
    [self addGestureRecognizer:self.pan];
    if (self.fromType == 3) {
        [self addGestureRecognizer:self.longPressTap];
    }

    self.photoBrowserView = _scrollView.subviews[self.currentImageIndex];
}

- (void)setListModel:(FWFeedListModel *)listModel{
    
    _listModel = listModel;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect rect = self.bounds;
    rect.size.width += FWPhotoBrowserImageViewMargin * 2;
    _scrollView.bounds = rect;
    _scrollView.center = CGPointMake(self.bounds.size.width *0.5, self.bounds.size.height *0.5);
    
    CGFloat y = 0;
    __block CGFloat w = _scrollView.frame.size.width - FWPhotoBrowserImageViewMargin * 2;
    CGFloat h = _scrollView.frame.size.height;
    [_scrollView.subviews enumerateObjectsUsingBlock:^(FWPhotoBrowserView *obj, NSUInteger idx, BOOL *stop) {
        CGFloat x = FWPhotoBrowserImageViewMargin + idx * (FWPhotoBrowserImageViewMargin * 2 + w);
        obj.frame = CGRectMake(x, y, w, h);
    }];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.subviews.count * _scrollView.frame.size.width, _scrollView.frame.size.height);
    _scrollView.contentOffset = CGPointMake(self.currentImageIndex * _scrollView.frame.size.width, 0);
    
    [self scrollViewDidScroll:_scrollView];
    
    if (!_hasShowedFistView) {
        [self showFirstImage];
    }
}

- (void)dealloc
{
//    NSLog(@"图片浏览器销毁");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setSourceImagesContainerView:(UIView *)sourceImagesContainerView{
    _sourceImagesContainerView = sourceImagesContainerView;
    _imageArray = nil;
    _photoBrowserStyle = FWPhotoBrowserStyleDefault;
}

- (void)setImageArray:(NSMutableArray *)imageArray{
    _imageArray = imageArray;
    _imageCount = imageArray.count;
    _sourceImagesContainerView = nil;
    _photoBrowserStyle = FWPhotoBrowserStyleSimple;
}

#pragma mark getter settter
- (UITapGestureRecognizer *)singleTap{
    if (!_singleTap) {
        _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoClick:)];
        _singleTap.numberOfTapsRequired = 1;
//        _singleTap.numberOfTouchesRequired = 1;
        _singleTap.delaysTouchesBegan = YES;
        //只能有一个手势存在
        [_singleTap requireGestureRecognizerToFail:self.doubleTap];
    }
    return _singleTap;
}

- (UITapGestureRecognizer *)doubleTap
{
    if (!_doubleTap) {
        _doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        _doubleTap.numberOfTapsRequired = 2;
//        _doubleTap.numberOfTouchesRequired = 1;
    }
    return _doubleTap;
}

- (UILongPressGestureRecognizer *)longPressTap{
    if (!_longPressTap) {
        _longPressTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressClick:)];
    }
    return _longPressTap;
}

- (UIPanGestureRecognizer *)pan{
    if (!_pan) {
        _pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPan:)];
    }
    return _pan;
}

- (UIImageView *)tempView{
    if (!_tempView) {
        FWPhotoBrowserView *photoBrowserView = _scrollView.subviews[self.currentImageIndex];
        UIImageView *currentImageView = photoBrowserView.imageview;
        CGFloat tempImageX = currentImageView.frame.origin.x - photoBrowserView.scrollOffset.x;
        CGFloat tempImageY = currentImageView.frame.origin.y - photoBrowserView.scrollOffset.y;
        
        CGFloat tempImageW = photoBrowserView.zoomImageSize.width;
        CGFloat tempImageH = photoBrowserView.zoomImageSize.height;
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        if (UIDeviceOrientationIsLandscape(orientation)) {//横屏
            
            //处理长图,图片太长会导致旋转动画飞掉
            if (tempImageH > KAppHeight) {
                tempImageH = tempImageH > (tempImageW * 1.5)? (tempImageW * 1.5):tempImageH;
                if (fabs(tempImageY) > tempImageH) {
                    tempImageY = 0;
                }
            }

        }
        
        _tempView = [[UIImageView alloc] init];
        //这边的contentmode要跟 FWPhotoGrop里面的按钮的 contentmode保持一致（防止最后出现闪动的动画）
        _tempView.contentMode = UIViewContentModeScaleAspectFill;
        _tempView.clipsToBounds = YES;
        _tempView.frame = CGRectMake(tempImageX, tempImageY, tempImageW, tempImageH);
        _tempView.image = currentImageView.image;
    }
    return _tempView;
}

//做颜色渐变动画的view，让退出动画更加柔和
- (UIView *)coverView{
    if (!_coverView) {
        _coverView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _coverView.backgroundColor = FWPhotoBrowserBackgrounColor;
    }
    return _coverView;
}

- (void)setPhotoBrowserView:(FWPhotoBrowserView *)photoBrowserView{
    _photoBrowserView = photoBrowserView;
    __weak typeof(self) weakSelf = self;
    _photoBrowserView.scrollViewWillEndDragging = ^(CGPoint velocity,CGPoint offset) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
//        NSLog(@"%f  %f",velocity.y,offset.y);
        if (((velocity.y < -2 && offset.y < 0) || offset.y < -100)) {
            [strongSelf hidePhotoBrowser];
            NSLog(@"将要结束拖拽 -- 退出");
        }
    };
}

- (void)setCurrentImageIndex:(int)currentImageIndex{
//    _currentImageIndex = currentImageIndex;
    _currentImageIndex = currentImageIndex < 0 ? 0 : currentImageIndex;
    NSInteger count0 = _imageCount;
    NSInteger count1 = _imageArray.count;
    if (count0 > 0) {
        if (_currentImageIndex > count0) {
            _currentImageIndex = 0;
        }
    }
    if (count1 > 0) {
        if (_currentImageIndex > count1) {
            _currentImageIndex = 0;
        }
    }
}

#pragma mark private methods
- (void)setupToolbars
{
    {
        UILabel * numLabel = [[UILabel alloc] init];
        numLabel.layer.cornerRadius = 2;
        numLabel.layer.masksToBounds = YES;
        numLabel.font = DHSystemFontOfSize_18;
        numLabel.textColor = FWViewBackgroundColor_FFFFFF;
        numLabel.backgroundColor = [UIColor colorWithHexString:@"FFFFFF" alpha:0.11];
        numLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:numLabel];
        numLabel.frame = CGRectMake((SCREEN_WIDTH-80)/2, 23+FWCustomeSafeTop, 80, 25);


        if (self.imageCount >= 1) {
            numLabel.text = [NSString stringWithFormat:@"1/%ld", (long)self.imageCount];
            [self addSubview:numLabel];
            
            numLabel.hidden = NO;
        }else{
            numLabel.hidden = YES;
        }
        _numLabel = numLabel;

    }
    
    {
        UIButton * saveButton = [[UIButton alloc] init];
        [saveButton setImage:[UIImage imageNamed:@"download"] forState:UIControlStateNormal];
        [saveButton addTarget:self action:@selector(saveImage) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:saveButton];
        [saveButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).mas_offset(-2);
            make.bottom.mas_equalTo(self).mas_equalTo(-8-FWSafeBottom);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        [self bringSubviewToFront:saveButton];
        _saveButton = saveButton;
    }
    
    {
        UILabel * lookOriginalLabel = [[UILabel alloc] init];
        lookOriginalLabel.layer.cornerRadius = 2;
        lookOriginalLabel.layer.masksToBounds = YES;
        lookOriginalLabel.font = DHSystemFontOfSize_15;
        lookOriginalLabel.textColor = FWViewBackgroundColor_FFFFFF;
        lookOriginalLabel.backgroundColor = [UIColor colorWithHexString:@"FFFFFF" alpha:0.11];
        lookOriginalLabel.textAlignment = NSTextAlignmentCenter;
        lookOriginalLabel.userInteractionEnabled = YES;
        [self addSubview:lookOriginalLabel];
        [lookOriginalLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.bottom.mas_equalTo(self).mas_equalTo(-10-FWSafeBottom);
            make.height.mas_equalTo(36);
            make.width.mas_greaterThanOrEqualTo(130);
        }];

        [self bringSubviewToFront:lookOriginalLabel];
        [lookOriginalLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(downloadOriginalPic)]];

        _lookOriginalLabel = lookOriginalLabel;
        _lookOriginalLabel.hidden = YES;
    }
    
    {
        if (!self.shareButton) {
            self.shareButton = [[UIButton alloc] init];
        }
//        shareButton.image = [UIImage imageNamed:@"pic_share_image"];
//        shareButton.userInteractionEnabled = YES;
        [self.shareButton setImage:[UIImage imageNamed:@"white_more"] forState:UIControlStateNormal];
        [self.shareButton addTarget:self action:@selector(shareButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.shareButton];
        self.shareButton.frame = CGRectMake(SCREEN_WIDTH-60, 5+FWCustomeSafeTop, 60, 60);
        
        if (self.fromType == 1) {
            self.shareButton.hidden = YES;
        }else{
            if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
                self.shareButton.hidden = NO;
            }else{
                self.shareButton.hidden = YES;
            }
        }
    }
    
    {
        if (!_loading) {
            _loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        _loading.center = self.center;
        _loading.backgroundColor = [UIColor colorWithHexString:@"000000" alpha:1];
        _loading.alpha = 0.9;
        [self addSubview:_loading];
        [self bringSubviewToFront:_loading];
        _loading.hidden = YES;
    }
}

#pragma mark - > 分享
- (void)shareButtonOnClick{

    if (!self.isShareViewShowing) {

        self.isShareViewShowing = YES;
        
        int index = _scrollView.contentOffset.x / _scrollView.bounds.size.width;

        self.isShareViewShowing = NO;
        if (self.smallArray.count <= 0 || index >= self.smallArray.count) {
            return;
        }
        NSDictionary * params = @{
                                  @"imageArray":self.smallArray,
                                  @"index":@(index).stringValue,
                                  };
        
        @autoreleasepool {
            SharePicPopView * _popView = [[SharePicPopView alloc] initWithParams:params withImage:self.currentImage];
            _popView.viewcontroller = self.vc;
            [_popView show];
        }
    }
}

#pragma mark - > 保存图像
- (void)saveImage
{
    FWPhotoBrowserView *currentView = _scrollView.subviews[self.currentIndex];
    
    [MBProgressHUD showHUDAddedTo:self animated:YES];

    if ([((NSString *)self.imageArray[self.currentIndex]) containsString:@"resize"]) {
        /* 当前是缩略图，需要下载 */
        NSString * urlString = self.originalImageArray[self.currentIndex];
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:urlString] options:0 progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
            if (image) {
                UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
            }
        }];
    }else{
        /* 当前是原图，直接下载当前页面图片即可 */
        UIImageWriteToSavedPhotosAlbum(currentView.imageview.image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.90f];
    label.layer.cornerRadius = 2;
    label.clipsToBounds = YES;
    label.bounds = CGRectMake(0, 0, 150, 40);
    label.center = self.center;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = DHSystemFontOfSize_16;
    [[UIApplication sharedApplication].keyWindow addSubview:label];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:label];
    if (error) {
        label.text = FWPhotoBrowserSaveImageFailText;
    }   else {
        label.text = FWPhotoBrowserSaveImageSuccessText;
    }
    [MBProgressHUD hideHUDForView:self animated:YES];

    [label performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:1.0];
}

- (void)setupScrollView
{
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;
    [self addSubview:_scrollView];
    
    for (int i = 0; i < self.imageCount; i++) {
        FWPhotoBrowserView *view = [[FWPhotoBrowserView alloc] init];
        view.isFullWidthForLandScape = self.isFullWidthForLandScape;
        view.imageview.tag = i;
        [_scrollView addSubview:view];
    }
    [self setupImageOfImageViewForIndex:self.currentImageIndex];
    
}

// 加载图片
- (void)setupImageOfImageViewForIndex:(NSInteger)index
{
    FWPhotoBrowserView *view = _scrollView.subviews[index];
    if (view.beginLoadingImage) return;
    
    if ([self.imageArray[index] isKindOfClass:[UIImage class]]) {
        view.imageview.image = self.imageArray[index];
    }else{
        if ([self highQualityImageURLForIndex:index]) {
            [view setImageWithURL:[self highQualityImageURLForIndex:index] placeholderImage:[self placeholderImageForIndex:index]];
        } else {
            view.imageview.image = [self placeholderImageForIndex:index];
        }
    }
    
    view.beginLoadingImage = YES;
}

- (void)onDeviceOrientationChangeWithObserver
{
    [self onDeviceOrientationChange];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDeviceOrientationChange) name:UIDeviceOrientationDidChangeNotification object:nil];
}

-(void)onDeviceOrientationChange
{
    if (!self.isNeedLandscape) {
        return;
    }
    
    FWPhotoBrowserView *currentView = _scrollView.subviews[self.currentImageIndex];
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    self.orientation = orientation;
    if (UIDeviceOrientationIsLandscape(orientation)) {
        if (self.bounds.size.width < self.bounds.size.height) {
            NSLog(@"横屏 还原");
            [currentView.scrollview setZoomScale:1.0 animated:YES];//还原
            [self cancelPrepareForHide];
            [UIView animateWithDuration:kRotateAnimationDuration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                self.transform = (orientation==UIDeviceOrientationLandscapeRight)?CGAffineTransformMakeRotation(M_PI*1.5):CGAffineTransformMakeRotation(M_PI/2);
                if (iPhoneX) {
                    self.center = [UIApplication sharedApplication].keyWindow.center;
                    self.bounds = CGRectMake(0, 0,  KAppHeight - kStatusBar_Height - kBottomSafeHeight, kAPPWidth);
                } else {
                    self.bounds = CGRectMake(0, 0, KAppHeight, kAPPWidth);
                }
                [self setNeedsLayout];
                [self layoutIfNeeded];
            } completion:^(BOOL finished) {
            
            }];
        }
    }else if (orientation==UIDeviceOrientationPortrait){
        if (self.bounds.size.width > self.bounds.size.height) {
            NSLog(@"竖屏 还原");
            [currentView.scrollview setZoomScale:1.0 animated:YES];//还原
            [UIView animateWithDuration:kRotateAnimationDuration delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                self.transform = (orientation==UIDeviceOrientationPortrait)?CGAffineTransformIdentity:CGAffineTransformMakeRotation(M_PI);
                if (iPhoneX) {
                    self.bounds = CGRectMake(0, 0, kAPPWidth, KAppHeight - kStatusBar_Height - kBottomSafeHeight);
                } else {
                    self.bounds = CGRectMake(0, 0, kAPPWidth, KAppHeight);
                }
                
                [self setNeedsLayout];
                [self layoutIfNeeded];
            } completion:^(BOOL finished) {
                
            }];
        }
    }
}

- (void)showFirstImage
{
    if (_photoBrowserStyle == FWPhotoBrowserStyleDefault) {
        UIView *sourceView = self.sourceImagesContainerView.subviews[self.currentImageIndex];
        CGRect rect = [self.sourceImagesContainerView convertRect:sourceView.frame toView:self];
        UIImageView *tempView = [[UIImageView alloc] init];
        tempView.frame = rect;
        tempView.image = [self placeholderImageForIndex:self.currentImageIndex];
        [self addSubview:tempView];
        tempView.contentMode = UIViewContentModeScaleAspectFit;
        
        CGFloat placeImageSizeW = tempView.image.size.width;
        CGFloat placeImageSizeH = tempView.image.size.height;
        CGRect targetTemp;
        CGFloat selfW = self.frame.size.width;
        CGFloat selfH = self.frame.size.height;
        
        CGFloat placeHolderH = (placeImageSizeH * selfW)/placeImageSizeW;
        if (placeHolderH <= selfH) {
            targetTemp = CGRectMake(0, (selfH - placeHolderH) * 0.5 , selfW, placeHolderH);
        } else {//图片高度>屏幕高度
            targetTemp = CGRectMake(0, 0, selfW, placeHolderH);
        }
        //先隐藏scrollview
        _scrollView.hidden = YES;
        _shareButton.hidden = YES;
        _numLabel.hidden = YES;
        _lookOriginalLabel.hidden = YES;
        _saveButton.hidden = YES;
        [UIView animateWithDuration:FWPhotoBrowserShowImageAnimationDuration animations:^{
            //将点击的临时imageview动画放大到和目标imageview一样大
            tempView.frame = targetTemp;
        } completion:^(BOOL finished) {
            //动画完成后，删除临时imageview，让目标imageview显示
            _hasShowedFistView = YES;
            [tempView removeFromSuperview];
            _scrollView.hidden = NO;
            _numLabel.hidden = NO;
            _saveButton.hidden = NO;

            if (self.fromType == 1) {
                _lookOriginalLabel.hidden = YES;
                _shareButton.hidden = YES;
            }else if(self.fromType == 2) {
                _lookOriginalLabel.hidden = YES;
                _shareButton.hidden = NO;
            }else{
                [self dealWithOriginal];
                _shareButton.hidden = NO;
            }
        }];
    } else {
//        FWPhotoBrowserShowImageAnimationDuration
        _photoBrowserView.alpha = 0;
        _contentView.alpha = 0;
        [UIView animateWithDuration:0.2 animations:^{
            //将点击的临时imageview动画放大到和目标imageview一样大
            _photoBrowserView.alpha = 1;
            _contentView.alpha = 1;
        } completion:^(BOOL finished) {
            _hasShowedFistView = YES;
        }];
    }
}

- (UIImage *)placeholderImageForIndex:(NSInteger)index
{
    if (_photoBrowserStyle == FWPhotoBrowserStyleDefault) {
        if ([self.delegate respondsToSelector:@selector(photoBrowser:placeholderImageForIndex:)]) {
            return [self.delegate photoBrowser:self placeholderImageForIndex:index];
        }
    } else {
        return nil;
    }
    
    return nil;
}

- (NSURL *)highQualityImageURLForIndex:(NSInteger)index
{
    if (_photoBrowserStyle == FWPhotoBrowserStyleDefault) {
        if ([self.delegate respondsToSelector:@selector(photoBrowser:highQualityImageURLForIndex:)]) {
            return [self.delegate photoBrowser:self highQualityImageURLForIndex:index];
        }
    } else {
        return [NSURL URLWithString:_imageArray[index]];
    }
    return nil;
}


- (void)hidePhotoBrowser
{
    [self prepareForHide];
    [self hideAnimation];
}

- (void)hideAnimation{
//    FWPhotoBrowserView *photoBrowserView = _scrollView.subviews[self.currentImageIndex];
//    UIImageView *currentImageView = photoBrowserView.imageview;
//    NSUInteger currentIndex = currentImageView.tag;
    CGRect targetTemp;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;

    UIView *sourceView = [self getSourceView];
    if (!sourceView) {
        targetTemp = CGRectMake(window.center.x, window.center.y, 0, 0);
    }
    if (_photoBrowserStyle == FWPhotoBrowserStyleDefault) {
        UIView *sourceView = [self getSourceView];
       targetTemp = [self.sourceImagesContainerView convertRect:sourceView.frame toView:self];
    } else {
        //默认回到屏幕中央
        targetTemp = CGRectMake(window.center.x, window.center.y, 0, 0);
    }
    self.window.windowLevel = UIWindowLevelNormal;//显示状态栏
//    NSLog(@"tempVeiw -- %@",NSStringFromCGRect(_tempView.frame));
    [UIView animateWithDuration:FWPhotoBrowserHideImageAnimationDuration animations:^{
        if (_photoBrowserStyle == FWPhotoBrowserStyleDefault) {
            _tempView.transform = CGAffineTransformInvert(self.transform);
        }
        _coverView.alpha = 0;
        _tempView.frame = targetTemp;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [_tempView removeFromSuperview];
        [_contentView removeFromSuperview];
        _tempView = nil;
        _contentView = nil;
        sourceView.hidden = NO;
    }];
}

- (UIView *)getSourceView{
    if (_currentImageIndex <= self.sourceImagesContainerView.subviews.count - 1) {
        UIView *sourceView = self.sourceImagesContainerView.subviews[_currentImageIndex];
        return sourceView;
    }
    return nil;
}

- (void)cancelPrepareForHide{
    [self.coverView removeFromSuperview];
    
    _numLabel.hidden = NO;
    _saveButton.hidden = NO;

    if (self.fromType == 1) {
        _lookOriginalLabel.hidden = YES;
        _shareButton.hidden = YES;
    }else if (self.fromType == 2) {
        _lookOriginalLabel.hidden = YES;
        _shareButton.hidden = NO;
    }else{
        [self dealWithOriginal];
        _shareButton.hidden = NO;
    }
    
    if (_tempView) {
        [_tempView removeFromSuperview];
        _tempView = nil;
    }
    _scrollView.hidden = NO;
    self.backgroundColor = FWPhotoBrowserBackgrounColor;
    _contentView.backgroundColor = FWPhotoBrowserBackgrounColor;
}

- (void)prepareForHide{
    [_contentView insertSubview:self.coverView belowSubview:self];
    _numLabel.hidden = YES;
    _lookOriginalLabel.hidden = YES;
    _shareButton.hidden = YES;
    _saveButton.hidden = YES;

    [self addSubview:self.tempView];
    _photoBrowserView.hidden = YES;
//    NSLog(@"photoBrowserView -- %@",_photoBrowserView);
    self.backgroundColor = [UIColor clearColor];
    _contentView.backgroundColor = [UIColor clearColor];
    UIView *view = [self getSourceView];
    view.hidden = YES;
}

- (void)bounceToOrigin{
    [UIView animateWithDuration:FWPhotoBrowserHideImageAnimationDuration animations:^{
        self.tempView.transform = CGAffineTransformIdentity;
        _coverView.alpha = 1;
    } completion:^(BOOL finished) {
        _numLabel.hidden = NO;
        _saveButton.hidden = NO;

        if (self.fromType == 1) {
            _lookOriginalLabel.hidden = YES;
            _shareButton.hidden = YES;
        }else if(self.fromType == 2) {
            _lookOriginalLabel.hidden = YES;
            _shareButton.hidden = NO;
        }else{
            [self dealWithOriginal];
            _shareButton.hidden = NO;
        }
        
        [_tempView removeFromSuperview];
        [_coverView removeFromSuperview];
        _tempView = nil;
        _coverView = nil;
        _photoBrowserView.hidden = NO;
        self.backgroundColor = FWPhotoBrowserBackgrounColor;
        _contentView.backgroundColor = FWPhotoBrowserBackgrounColor;
        UIView *view = [self getSourceView];
        view.hidden = NO;
    }];
}

#pragma mark - scrollview代理方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int index = (scrollView.contentOffset.x + _scrollView.bounds.size.width * 0.5) / _scrollView.bounds.size.width;
    _numLabel.text = [NSString stringWithFormat:@"%d/%ld", index + 1, (long)self.imageCount];
    _lookOriginalLabel.hidden = YES;
    
    if (self.currentIndex < 0) {
        [self getOriginalPicData:scrollView];
    }
    
    self.currentIndex = index;
    
    long left = index - 1;
    long right = index + 1;
    left = left>0?left : 0;
    right = right>self.imageCount?self.imageCount:right;
    
    for (long i = left; i < right; i++) {
         [self setupImageOfImageViewForIndex:i];
    }
}

//scrollview结束滚动调用
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int autualIndex = scrollView.contentOffset.x  / _scrollView.bounds.size.width;
    //设置当前下标
    self.currentImageIndex = autualIndex;
    self.photoBrowserView = _scrollView.subviews[self.currentImageIndex];
    
    
    //将不是当前imageview的缩放全部还原 (这个方法有些冗余，后期可以改进)
    for (FWPhotoBrowserView *view in _scrollView.subviews) {
        if (view.imageview.tag != autualIndex) {
                view.scrollview.zoomScale = 1.0;
        }
    }
    
    /* 显示逻辑 */
    if (self.sizeArray.count > 0) {
        /* 数组有数据 */
        if (self.sizeArray.count  > self.currentImageIndex) {
            /* 如果当前项小于数组则取值 && 是缩略图 */
            if ([self.sizeArray[self.currentImageIndex] length] > 0) {
                /* 有尺寸缓存 */
                if([((NSString *)self.imageArray[self.currentIndex]) containsString:@"resize"]){
                    /* 缩略图 */
                    _lookOriginalLabel.hidden = NO;
                    _lookOriginalLabel.text = [NSString stringWithFormat:@"查看原图%@",self.sizeArray[self.currentImageIndex]];
                }else{
                    /* 原图 */
                    _lookOriginalLabel.hidden = YES;
                }
            }else{
                /* 无尺寸就取值 */
                [self getOriginalPicData:scrollView];
            }
        }else{
            [self getOriginalPicData:scrollView];
        }
    }else{
        /* 可能开始数组还未初始化结束，或者有其他错误用来兼容 */
        [self getOriginalPicData:scrollView];
    }
}

#pragma mark - > 滚动结束或刚进入显示逻辑
- (void)getOriginalPicData:(UIScrollView *)scrollView{
    
    int index = (scrollView.contentOffset.x + _scrollView.bounds.size.width * 0.5) / _scrollView.bounds.size.width;
    self.currentIndex = index;

    if (index< self.imageArray.count ) {
        [self dealWithOriginal];
    }
}

#pragma mark - > 处理是否显示底部查看原图
- (void)dealWithOriginal{
    
    if (self.fromType != 3) {
        return;
    }
    
    if (self.currentIndex< self.imageArray.count && self.currentIndex >= 0) {

        NSString * urlString = self.imageArray[self.currentIndex];
        if ([urlString containsString:@"resize"]) {
            
            self.startDare = [NSDate date];
            
            if (self.currentImageIndex == 0) {
                /* 如果当前页是第一页 */
                [self requestSizeWithIndex:self.currentImageIndex];
                [self requestSizeWithIndex:self.currentImageIndex+1];
                [self requestSizeWithIndex:self.currentImageIndex+2];
            }else if(self.currentImageIndex == 1){
                /* 如果当前页是第二页 */
                [self requestSizeWithIndex:self.currentImageIndex];

                [self requestSizeWithIndex:self.currentImageIndex-1];
                [self requestSizeWithIndex:self.currentImageIndex+1];
                [self requestSizeWithIndex:self.currentImageIndex+2];
            }else if (self.currentImageIndex == self.imageArray.count-2){
                /* 如果当前页是第倒数第二页 */
                [self requestSizeWithIndex:self.currentImageIndex];

                [self requestSizeWithIndex:self.currentImageIndex-2];
                [self requestSizeWithIndex:self.currentImageIndex-1];
                [self requestSizeWithIndex:self.currentImageIndex+1];
            }else if (self.currentImageIndex == self.imageArray.count-1){
                /* 如果当前页是第倒数第一页 */
                [self requestSizeWithIndex:self.currentImageIndex];

                [self requestSizeWithIndex:self.currentImageIndex-2];
                [self requestSizeWithIndex:self.currentImageIndex-1];
            }else{
                /* 如果当前页是中间页 */
                [self requestSizeWithIndex:self.currentImageIndex];

                [self requestSizeWithIndex:self.currentImageIndex-2];
                [self requestSizeWithIndex:self.currentImageIndex-1];
                [self requestSizeWithIndex:self.currentImageIndex+1];
                [self requestSizeWithIndex:self.currentImageIndex+2];
            }
        }else{
            _lookOriginalLabel.hidden = YES;
        }
    }
}

- (void)requestSizeWithIndex:(NSInteger)index{
    
    if (self.sizeArray.count > index) {
        
        if ([self.sizeArray[index] length] <= 0) {
            if (index<self.originalImageArray.count) {
                [self URLFileSizeWidthURL:self.originalImageArray[index] withParams:@{@"currentImageIndex":@(index).stringValue}];
            }
        }else{
            if (index < self.sizeArray.count) {
                _lookOriginalLabel.hidden = NO;
                _lookOriginalLabel.text = [NSString stringWithFormat:@"查看原图%@",self.sizeArray[index]];
            }
        }
    }else{
        if (index<self.originalImageArray.count) {
            [self URLFileSizeWidthURL:self.originalImageArray[index] withParams:@{@"currentImageIndex":@(index).stringValue}];
        }
    }

}

- (void)URLFileSizeWidthURL:(NSString *)URL withParams:(NSDictionary *)params{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager HEAD:URL parameters:params success:^(NSURLSessionDataTask * _Nonnull task) {
        
        NSDictionary *dic = [(NSHTTPURLResponse *)task.response allHeaderFields];
        CGFloat length = [[dic objectForKey:@"Content-Length"] floatValue];// Content-Length单位是byte，除以1024后是KB

        NSString *size;
        if (length >= 1048576) {//1048576bt = 1M  小于1m的显示KB 大于1m显示M
            size = [NSString stringWithFormat:@"%.2fM",length/1024/1024];
        }else{
            size = [NSString stringWithFormat:@"%.1fKB",length/1024];
        }
        
        if ([params[@"currentImageIndex"] intValue] < self.sizeArray.count) {
            [self.sizeArray replaceObjectAtIndex:[params[@"currentImageIndex"] intValue] withObject:size];
        }
        
        if (self.currentImageIndex == [params[@"currentImageIndex"] intValue]) {
            /* 只有当前页显示，其余页面只是计算值 */
            _lookOriginalLabel.hidden = NO;
            _lookOriginalLabel.text = [NSString stringWithFormat:@"查看原图%@",size];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {}];
}

#pragma mark - > 下载原图
- (void)downloadOriginalPic{
    
    NSString * urlString = self.originalImageArray[self.currentIndex];
    
    DHWeakSelf;
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:urlString] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        float prgress =  (float)((receivedSize *1.00)/(expectedSize * 1.00));
        
        if (prgress <= 0 || receivedSize <= 0) {
            return ;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            _lookOriginalLabel.text = [NSString stringWithFormat:@"%.f%%",prgress*100];
            if (prgress >= 1) {
                _lookOriginalLabel.hidden = YES;
            }else{
                _lookOriginalLabel.hidden = NO;
            }
        });
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString * originalURL = [weakSelf.originalImageArray objectAtIndex:self.currentIndex];
                [weakSelf.imageArray replaceObjectAtIndex:self.currentIndex withObject:originalURL];
                
                FWPhotoBrowserView *view = _scrollView.subviews[weakSelf.currentIndex];
                view.imageview.image = image;
            });
        }
    }];
}

#pragma mark - tap
#pragma mark 单击
- (void)photoClick:(UITapGestureRecognizer *)recognizer
{
    [self hidePhotoBrowser];
}

#pragma mark 双击
- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    FWPhotoBrowserView *view = _scrollView.subviews[self.currentImageIndex];
    CGPoint touchPoint = [recognizer locationInView:self];
    if (view.scrollview.zoomScale <= 1.0) {
        CGFloat scaleX = touchPoint.x + view.scrollview.contentOffset.x;//需要放大的图片的X点
        CGFloat sacleY = touchPoint.y + view.scrollview.contentOffset.y;//需要放大的图片的Y点
        [view.scrollview zoomToRect:CGRectMake(scaleX, sacleY, 10, 10) animated:YES];
    } else {
        [view.scrollview setZoomScale:1.0 animated:YES]; //还原
    }
}

#pragma mark - > 长按
- (void)longPressClick:(UILongPressGestureRecognizer *)recognizer{
   
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // do something
        [self endEditing:YES];
        [self shareButtonOnClick];
    }
}

#pragma mark - > 拖动
- (void)didPan:(UIPanGestureRecognizer *)panGesture {
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(orientation)) {//横屏不允许拉动图片
        return;
    }
    
    //transPoint : 手指在视图上移动的位置（x,y）向下和向右为正，向上和向左为负。
    //locationPoint ： 手指在视图上的位置（x,y）就是手指在视图本身坐标系的位置。
    //velocity： 手指在视图上移动的速度（x,y）, 正负也是代表方向。
    CGPoint transPoint = [panGesture translationInView:self];
//    CGPoint locationPoint = [panGesture locationInView:self];
    CGPoint velocity = [panGesture velocityInView:self];//速度
    
//    NSLog(@"开始拖动");
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan:
        {
            [self prepareForHide];
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            _numLabel.hidden = YES;
            _lookOriginalLabel.hidden = YES;
            _shareButton.hidden = YES;
            _saveButton.hidden = YES;
            
            double delt = 1 - fabs(transPoint.y) / self.frame.size.height;
            delt = MAX(delt, 0);
            double s = MAX(delt, 0.5);
            CGAffineTransform translation = CGAffineTransformMakeTranslation(transPoint.x/s, transPoint.y/s);
            CGAffineTransform scale = CGAffineTransformMakeScale(s, s);
            self.tempView.transform = CGAffineTransformConcat(translation, scale);
            self.coverView.alpha = delt;
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            if (fabs(transPoint.y) > 220 || fabs(velocity.y) > 500) {//退出图片浏览器
                [self hideAnimation];
            } else {//回到原来的位置
                [self bounceToOrigin];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark public methods
- (void)show
{
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = FWPhotoBrowserBackgrounColor;
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    _contentView.center = window.center;
    _contentView.bounds = window.bounds;

    if (iPhoneX) {
        self.frame = CGRectMake(0, kStatusBar_Height,kAPPWidth,KAppHeight - kStatusBar_Height - kBottomSafeHeight);
    } else {
        self.frame = _contentView.bounds;
    }
    window.windowLevel = UIWindowLevelStatusBar+10.0f;//隐藏状态栏
    [_contentView addSubview:self];
    
    [window addSubview:_contentView];
    
    [self performSelector:@selector(onDeviceOrientationChangeWithObserver) withObject:nil afterDelay:FWPhotoBrowserShowImageAnimationDuration + 0.2];
}


@end
