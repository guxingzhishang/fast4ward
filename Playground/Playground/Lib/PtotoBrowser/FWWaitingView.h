//
//  FWWaitingView.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWPhotoBrowserConfig.h"

NS_ASSUME_NONNULL_BEGIN
@interface FWWaitingView : UIView

@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, assign) int mode;

@end

NS_ASSUME_NONNULL_END
