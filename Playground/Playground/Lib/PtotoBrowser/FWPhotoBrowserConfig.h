//
//  FWPhotoBrowserConfig.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

typedef enum {
    FWWaitingViewModeLoopDiagram, // 环形
    FWWaitingViewModePieDiagram // 饼型
} FWWaitingViewMode;

typedef enum {
    FWPhotoBrowserStyleDefault, //复杂类型，带有弹回原位置动画效果
    FWPhotoBrowserStyleSimple // 简单类型
} FWPhotoBrowserStyle;

#define kMinZoomScale 0.6f
#define kMaxZoomScale 2.0f

#define kAPPWidth [UIScreen mainScreen].bounds.size.width
#define KAppHeight [UIScreen mainScreen].bounds.size.height

#define iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
//状态栏高度，iphoneX->44 其他 20
#define kStatusBar_Height [UIApplication sharedApplication].statusBarFrame.size.height
//底部安全距离 iphoneX->34 其他 0
#define kBottomSafeHeight (iPhoneX?34.0f:0.0f)

// 图片保存成功提示文字
#define FWPhotoBrowserSaveImageSuccessText @" 保存成功 ";

// 图片保存失败提示文字
#define FWPhotoBrowserSaveImageFailText @" 保存失败 ";

// browser背景颜色
#define FWPhotoBrowserBackgrounColor [UIColor colorWithRed:0 green:0 blue:0 alpha:1]

// browser中图片间的margin
#define FWPhotoBrowserImageViewMargin 10

//横竖屏切换动画时长
#define kRotateAnimationDuration 0.35f

// browser中显示图片动画时长
#define FWPhotoBrowserShowImageAnimationDuration 0.35f

// browser中隐藏图片动画时长
#define FWPhotoBrowserHideImageAnimationDuration 0.35f

// 图片下载进度指示进度显示样式（FWWaitingViewModeLoopDiagram 环形，FWWaitingViewModePieDiagram 饼型）
#define FWWaitingViewProgressMode FWWaitingViewModeLoopDiagram

// 图片下载进度指示器背景色
#define FWWaitingViewBackgroundColor [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]
//#define FWWaitingViewBackgroundColor [UIColor clearColor]

// 图片下载进度指示器内部控件间的间距
#define FWWaitingViewItemMargin 10

