//
//  FWPhotoBrowser.h
//  Playground
//
//  Created by 孤星之殇 on 2019/10/22.
//  Copyright © 2019 孤星之殇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@class FWButton, FWPhotoBrowser;

//配置小图和大图
@protocol FWPhotoBrowserDelegate <NSObject>
@required
- (UIImage *)photoBrowser:(FWPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index;
- (NSURL *)photoBrowser:(FWPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index;

@end

@interface FWPhotoBrowser : UIView<UIScrollViewDelegate>
//第一种展示方式（退出时能回到原来的位置），需要定义九图控件，下面两个参数必传
@property (nonatomic, weak) UIView *sourceImagesContainerView;
@property (nonatomic, assign) NSInteger imageCount;

@property (nonatomic, weak) UIViewController * vc;
@property (nonatomic,strong) FWFeedListModel * listModel;

/* 1、不显示分享，不显示查看原图  2、显示分享，不带查看原图   3、显示分享（单纯分享图片）带查看原图*/
@property (nonatomic, assign) NSInteger  fromType;

//第二种展示方式（退出时不能回到原来的位置，默认回到屏幕正中央）直接传url,图片url列表必传
@property (nonatomic,strong) NSMutableArray *imageArray; // 要展示的列表
@property (nonatomic,strong) NSArray *smallArray; // 分享时用小图（之所以重新弄个数组，防止展示列表里的数组被替换成原图了）
@property (nonatomic,strong) NSArray *originalImageArray; // 原图

//从第几张图片开始展示，默认 0（第一种或者第二种方式展示都必须传）
@property (nonatomic, assign) int currentImageIndex;
//是否在横屏的时候直接满宽度，而不是满高度，一般是在有长图需求的时候设置为YES(默认值YES)
@property (nonatomic, assign) BOOL isFullWidthForLandScape;
//是否支持横竖屏，默认支持（YES）
@property (nonatomic, assign) BOOL isNeedLandscape;
@property (nonatomic, weak) id<FWPhotoBrowserDelegate> delegate;
- (void)show;


@end

NS_ASSUME_NONNULL_END
