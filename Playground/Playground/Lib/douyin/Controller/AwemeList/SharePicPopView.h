//
//  SharePicPopView.h
//  Douyin
//
//  Created by Qiao Shi on 2018/7/30.
//  Copyright © 2018年 Qiao Shi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "XLCircleProgress.h"


@interface SharePicPopView:UIView<NSURLSessionDelegate>
@property (nonatomic, strong) UIView        *container;
@property (nonatomic, strong) UIButton       *cancel;
@property (nonatomic, weak)   UIViewController *viewcontroller;

@property (nonatomic, strong) UIImage * shareImage;

@property (nonatomic, strong) XLCircleProgress *progressView;
@property(nonatomic,strong) NSURLSessionDownloadTask *downloadTask;

@property (nonatomic, copy) awemeBlock myBlock;// 收藏操作，需要往上一页面传值，所以实例化SharePicPopView后，一定要实现myBlock属性，但是可以传空

/** params要传的参数
 * aweme - > FWFeedListModel (必传)
 * type - > 1视频，2图文，3文章 (必传)
 * hideDelete - > 是否隐藏删除按钮 （选传）
 */
- (instancetype)initWithParams:(NSDictionary *)params withImage:(UIImage *)image;

- (void)show;
- (void)dismiss;

@end
