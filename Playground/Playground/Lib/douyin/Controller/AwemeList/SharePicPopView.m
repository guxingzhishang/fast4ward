//
//  SharePicPopView.m
//  Douyin
//
//  Created by Qiao Shi on 2018/7/30.
//  Copyright © 2018年 Qiao Shi. All rights reserved.
//

#import "SharePicPopView.h"
#import "Masonry.h"
#import "ShareManager.h"
#import "FWFeedBackRequest.h"

@interface SharePicPopView ()

@property (nonatomic, strong) UIImageView * containerView;
@property (nonatomic, strong) UIScrollView *bottomScrollView;
@property (nonatomic, strong) UIImageView * picView;
@property (nonatomic, strong) UILabel * titleLabel;//标题
@property (nonatomic, strong) UIImageView * codeImageView;
@property (nonatomic, strong) UILabel * tipLabel;// 提示
@property (nonatomic, strong) UIImageView * nameImageView;// 4FUN 肆放
@property (nonatomic, strong) UILabel * showLabel;// 长按扫码查看
@property (nonatomic, strong) UIActivityIndicatorView * loading;
@property (nonatomic, strong) NSData *data;
@end

@implementation SharePicPopView

@synthesize containerView;
@synthesize bottomScrollView;
@synthesize picView;
@synthesize titleLabel;
@synthesize codeImageView;
@synthesize tipLabel;
@synthesize nameImageView;
@synthesize showLabel;
@synthesize loading;

- (instancetype)initWithParams:(NSDictionary *)params withImage:(UIImage *)image{
    
    self = [super init];
    if (self) {
        
        self.shareImage = image;
        
        if (!self.shareImage) {
                dispatch_async(dispatch_queue_create(0, 0), ^{
                
                NSArray * imageArray = params[@"imageArray"];
                int index = [params[@"index"] intValue];

                self.data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageArray[index]]];

                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage * image = [UIImage imageWithData:self.data];
                    if (image) {
                        self.shareImage = image; // 取得图片
                    }
                });
            });
        }
        
        NSMutableArray *topIconsName = @[@"icon_profile_share_wechat",
                                         @"icon_profile_share_wxTimeline"].mutableCopy;
        
        NSMutableArray *topTexts = @[@"微信好友",
                                     @"朋友圈"].mutableCopy;
        
        NSMutableArray *bottomIconsName =
        @[@"icon_home_allshare_download"].mutableCopy;
        
        NSMutableArray *bottomTexts = @[@"保存图片"].mutableCopy;
        
        CGFloat containerHeight = 280;
        
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
            // 安装微信不做任何操作
        }else{
            [topTexts removeAllObjects];
            [topIconsName removeAllObjects];
            containerHeight = 190;
        }
        
        if (bottomTexts.count == 0) {
            containerHeight = 190;
        }
        
        self.frame = SCREEN_FRAME;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGuesture:)]];
        _container = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, containerHeight + SafeAreaBottomHeight)];
        _container.backgroundColor = ColorBlackAlpha60;
        [self addSubview:_container];
        
        
        UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:_container.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10.0f, 10.0f)];
        CAShapeLayer* shape = [[CAShapeLayer alloc] init];
        [shape setPath:rounded.CGPath];
        _container.layer.mask = shape;
        
        UIBlurEffect *blurEffect =[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
        visualEffectView.frame = self.bounds;
        visualEffectView.alpha = 1.0f;
        [_container addSubview:visualEffectView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 35)];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        label.text = @"分享到";
        label.textColor = ColorGray;
        label.font = MediumFont;
        [_container addSubview:label];
        
        self.progressView = [[XLCircleProgress alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        self.progressView.center = self.center;
        [DHWindow addDHCustomSubView:self.progressView priority:10000];
        //        [self addSubview:self.progressView];
        self.progressView.hidden = YES;
        
        CGFloat itemWidth = SCREEN_WIDTH/topTexts.count;
        
        UIScrollView *topScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, SCREEN_WIDTH, 90)];
        topScrollView.contentSize = CGSizeMake(itemWidth * topIconsName.count, 80);
        topScrollView.showsHorizontalScrollIndicator = NO;
        topScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 30);
        [_container addSubview:topScrollView];
        
        for (NSInteger i = 0; i < topIconsName.count; i++) {
            ShareItem *item = [[ShareItem alloc] initWithFrame:CGRectMake(itemWidth*i, 0, SCREEN_WIDTH/2, 90)];
            item.icon.image = [UIImage imageNamed:topIconsName[i]];
            item.label.text = topTexts[i];
            item.tag = i+9999;
            [item addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onShareItemTap:)]];
            [item startAnimation:i*0.03f];
            [topScrollView addSubview:item];
        }
        
        UIView *splitLine = [[UIView alloc] initWithFrame:CGRectMake(0, 130, SCREEN_WIDTH, 0.5f)];
        splitLine.backgroundColor = ColorWhiteAlpha10;
        [_container addSubview:splitLine];
        
        if (bottomTexts.count == 0) {
            splitLine.hidden = YES;
        }else{
            splitLine.hidden = NO;
        }
        
        CGFloat bottomY = 135;
        if (topTexts.count == 0) {
            
            splitLine.hidden = YES;
            label.hidden = YES;
            bottomY = 30;
        }else{
            splitLine.hidden = NO;
            label.hidden = NO;
        }
        
        CGFloat bottomItemWidth = SCREEN_WIDTH/2;
     
        bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, bottomY, SCREEN_WIDTH, 90)];
        bottomScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 80);
        bottomScrollView.showsHorizontalScrollIndicator = NO;
        bottomScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 30);
        [_container addSubview:bottomScrollView];
        
        for (int i =0 ; i <bottomTexts.count; i++) {
            
            ShareItem *item = [[ShareItem alloc] initWithFrame:CGRectMake( bottomItemWidth *i, 0,bottomItemWidth, 90)];
            item.icon.image = [UIImage imageNamed:bottomIconsName[i]];
            item.label.text = bottomTexts[i];
            item.tag = i+8888;
            [item startAnimation:i*0.03f];
            [bottomScrollView addSubview:item];
            
            if ([bottomTexts[i] isEqualToString:@"保存图片"]) {
                [item addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(saveItemTap:)]];
            }
        }
        
        _cancel = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_container.frame)-50-SafeAreaBottomHeight, SCREEN_WIDTH, 50 + SafeAreaBottomHeight)];
        [_cancel setTitleEdgeInsets:UIEdgeInsetsMake(-SafeAreaBottomHeight, 0, 0, 0)];
        
        [_cancel setTitle:@"取消" forState:UIControlStateNormal];
        [_cancel setTitleColor:ColorWhite forState:UIControlStateNormal];
        _cancel.titleLabel.font = BigFont;
        
        _cancel.backgroundColor = ColorGrayLight;
        [_container addSubview:_cancel];
        
        UIBezierPath* rounded2 = [UIBezierPath bezierPathWithRoundedRect:_cancel.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10.0f, 10.0f)];
        CAShapeLayer* shape2 = [[CAShapeLayer alloc] init];
        [shape2 setPath:rounded2.CGPath];
        _cancel.layer.mask = shape2;
        [_cancel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGuesture:)]];
        
        
        loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        loading.center = self.center;
        loading.backgroundColor = [UIColor colorWithHexString:@"000000" alpha:1];
        loading.alpha = 0.5;
        [self addSubview:loading];
        [self bringSubviewToFront:loading];
        
    }
    
    return self;
}

#pragma mark - >  分享到各平台
- (void)onShareItemTap:(UITapGestureRecognizer *)sender {
    
    switch (sender.view.tag-9999) {
        case 0:
        {
            loading.hidden = NO;
            [loading startAnimating];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                loading.hidden = YES;
                [loading stopAnimating];
                
                [self shareAtIndex:WXSceneSession];
                [self dismiss];
            });
        }
            break;
        case 1:
        {
            loading.hidden = NO;
            [loading startAnimating];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                loading.hidden = YES;
                [loading stopAnimating];
                
                [self shareAtIndex:WXSceneTimeline];
                [self dismiss];
            });
        }
            break;
        default:
            break;
    }
    
}

#pragma mark - > 分享
- (void)shareAtIndex:(NSInteger)index{
    
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        
        if (index == 0){
            [self WXSendImage:self.shareImage withShareScene:WXSceneSession];
        }else if (index == 1){
            [self WXSendImage:self.shareImage withShareScene:WXSceneTimeline];
        }
    }else{
        [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self.viewcontroller];
    }
}

- (void)WXSendImage:(UIImage *)image withShareScene:(enum WXScene)scene {
    if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        WXImageObject *ext = [WXImageObject object];
        // 小于10MB
        ext.imageData = imageData;
        
        WXMediaMessage *message = [WXMediaMessage message];
        message.mediaObject = ext;
        
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.scene = scene;
        req.message = message;
        [WXApi sendReq:req];
    }else {
        // 提示用户安装微信
        [[FWHudManager sharedManager] showErrorMessage:@"您还没装微信哦~" toController:self.viewController];
    }
}

#pragma mark - > 截屏
- (void)actionForScreenShotWith:(UIView *)aimView savePhoto:(BOOL)savePhoto withSence:(enum WXScene)scene {
    
    if (!aimView) return;
    
    UIGraphicsBeginImageContextWithOptions(aimView.bounds.size, NO, 0.0f);
    [aimView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (savePhoto) {
        UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }else{
        [self WXSendImage:viewImage withShareScene:scene];
    }
}

#pragma mark - >  保存到本地相册
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    if (error) {
        NSLog(@"保存失败，请重试");
    } else {
        NSLog(@"保存成功");
        [[FWHudManager sharedManager] showErrorMessage:@"已保存至相册" toController:self.viewController];
        [self dismiss];
    }
}

#pragma mark - > 保存到相册（下载）
- (void)saveItemTap:(UITapGestureRecognizer *)sender {
    UIImageWriteToSavedPhotosAlbum(self.shareImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewcontroller presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}


//控件本身的代理方法  更新控件样子
- (void)progressOverAndChangeViewContents {
    
    self.progressView.hidden = YES;
    [[FWHudManager sharedManager] showSuccessMessage:@"视频已成功保存至相册" toController:self.viewcontroller];
    
    [DHWindow removeSubView:self.progressView];
    [self dismiss];
}


- (void)handleGuesture:(UITapGestureRecognizer *)sender {
    CGPoint point = [sender locationInView:_container];
    if(![_container.layer containsPoint:point]) {
        [self dismiss];
        return;
    }
    point = [sender locationInView:_cancel];
    if([_cancel.layer containsPoint:point]) {
        [self dismiss];
    }
}

- (void)show {
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [window addSubview:self];
    [UIView animateWithDuration:0.15f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.container.frame;
                         frame.origin.y = frame.origin.y - frame.size.height;
                         self.container.frame = frame;
                     }
                     completion:^(BOOL finished) {
                     }];
}

- (void)dismiss {
    
    [UIView animateWithDuration:0.15f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.container.frame;
                         frame.origin.y = frame.origin.y + frame.size.height;
                         self.container.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

- (UIImageView *)getCurrenImage{
    
    return containerView;
}

@end



