//
//  SharePopView.h
//  Douyin
//
//  Created by Qiao Shi on 2018/7/30.
//  Copyright © 2018年 Qiao Shi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "XLCircleProgress.h"

typedef void(^awemeBlock)(FWFeedListModel * awemeModel);

@interface SharePopView:UIView<NSURLSessionDelegate>
@property (nonatomic, strong) UIView        *container;
@property (nonatomic, strong) UIButton       *cancel;
@property (nonatomic, weak)   UIViewController *viewcontroller;
@property (nonatomic, strong) FWFeedListModel  *aweme;
@property (nonatomic, strong) NSString * type;// 1视频，2图文, 3文章  4问答  5改装
@property (nonatomic, assign) BOOL hideDelete;// 是否隐藏删除
@property (nonatomic, strong) XLCircleProgress *progressView;
@property(nonatomic,strong) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic, strong) UIImage * shareImage;

@property (nonatomic, copy) awemeBlock myBlock;// 收藏操作，需要往上一页面传值，所以实例化SharePopView后，一定要实现myBlock属性，但是可以传空

/** params要传的参数
 * aweme - > FWFeedListModel (必传)
 * type - > 1视频，2图文，3文章 (必传)
 * hideDelete - > 是否隐藏删除按钮 （选传）
 */
- (instancetype)initWithParams:(NSDictionary *)params;

- (void)show;
- (void)dismiss;

- (void)setupQRViewWithModel:(FWFeedListModel *)model;

@end

@interface ShareItem:UIButton
@property (nonatomic, strong) UIImageView   *icon;
@property (nonatomic, strong) UILabel       *label;
-(void)startAnimation:(NSTimeInterval)delayTime;
@end
