//
//  CommentsPopView.m
//  Douyin
//
//  Created by Qiao Shi on 2018/7/30.
//  Copyright © 2018年 Qiao Shi. All rights reserved.
//

#import "CommentsPopView.h"
#import "Masonry.h"
#import "NSNotification+Extension.h"
#import "FWCommentModel.h"
#import "FWLikeRequest.h"

#import "FWCommentHeaderView.h"
#import "FWCommentFooterView.h"
#import "FWCommentDetailViewController.h"
#import "FWCommentCell.h"

#define COMMENT_CELL @"CommentCell"
#define COMMENT_HEADER @"CommentHeader"
#define COMMENT_FOOTER @"CommentFooter"

@interface CommentsPopView () <UITableViewDelegate,UITableViewDataSource, UIGestureRecognizerDelegate,UIScrollViewDelegate, CommentTextViewDelegate,FWCommentHeaderViewDelegate,FWCommentCellDelegate,FWCommentFooterViewDelegate>


@property (nonatomic, assign) NSInteger                        pageIndex;
@property (nonatomic, assign) NSInteger                        pageSize;

@property (nonatomic, strong) UIView                           *container;
@property (nonatomic, strong) FWTableView                      *tableView;
@property (nonatomic, strong) NSMutableArray                   *commentDataSource;
@property (nonatomic, strong) CommentTextView                  *textView;
@property (nonatomic, strong) FWCommentModel                   *commentModel;
@property (nonatomic, strong) NSString * hostComment_id;

/* 用来记录每一个section下的page */
@property (nonatomic, strong) NSMutableDictionary * pageDictonary;
@end

@implementation CommentsPopView

- (NSMutableArray *)locCommentIDArray{
    
    if (!_locCommentIDArray) {
        _locCommentIDArray = [[NSMutableArray alloc] init];
    }
    return _locCommentIDArray;
}

- (NSMutableDictionary *)pageDictonary{
    if (!_pageDictonary) {
        _pageDictonary = [[NSMutableDictionary alloc] init];
    }
    
    return _pageDictonary;
}

- (instancetype)initWithFeedID:(NSString *)feed_id {
    self = [super init];
    if (self) {
        self.frame = SCREEN_FRAME;
        self.userInteractionEnabled = YES;
        
        self.feed_id = feed_id;
        
        _pageIndex = 1;
        _pageSize = 20;
        self.currentRow = -1;
        self.currentSection = -1;
        
        _commentDataSource = [NSMutableArray array];
        
        
        _closeButton = [[UIButton alloc] init];
        _closeButton.frame = CGRectMake(0, 0, SCREEN_WIDTH,  SCREEN_HEIGHT*1/4);
        _closeButton.backgroundColor = FWClearColor;
        [_closeButton addTarget:self action:@selector(closeButtonOnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_closeButton];
        
        _container = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT*3/4)];
        _container.backgroundColor = ColorBlackAlpha60;
        [self addSubview:_container];
        
        
        UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*3/4) byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10.0f, 10.0f)];
        CAShapeLayer* shape = [[CAShapeLayer alloc] init];
        [shape setPath:rounded.CGPath];
        _container.layer.mask = shape;
        
        UIBlurEffect *blurEffect =[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
        visualEffectView.frame = self.bounds;
        visualEffectView.alpha = 1.0f;
        [_container addSubview:visualEffectView];
        
        
        _label = [[UILabel alloc] init];
        _label.textColor = ColorGray;
        _label.text = @"共0条评论";
        _label.font = SmallFont;
        _label.textAlignment = NSTextAlignmentCenter;
        [_container addSubview:_label];
        
        _close = [[UIImageView alloc] init];
        _close.userInteractionEnabled = YES;
        _close.image = [UIImage imageNamed:@"icon_closetopic"];
        _close.contentMode = UIViewContentModeCenter;
        [_close addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonOnClick)]];
        [_container addSubview:_close];
        [_label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.container);
            make.height.mas_equalTo(35);
        }];
        [_close mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.label);
            make.right.equalTo(self.label).inset(10);
            make.width.height.mas_equalTo(30);
        }];
        
        CGFloat bottomPadding = 0;
        if (SCREEN_HEIGHT <812) {
            bottomPadding = 40;
        }else{
            bottomPadding = 20;
        }
        _tableView = [[FWTableView alloc]initWithFrame:CGRectMake(0, 35, SCREEN_WIDTH, SCREEN_HEIGHT*3/4  - 50 - SafeAreaBottomHeight-bottomPadding) style:UITableViewStyleGrouped];
        _tableView.tag = CommentListTag;
        _tableView.backgroundColor = ColorClear;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        [_tableView registerClass:FWCommentCell.class forCellReuseIdentifier:COMMENT_CELL];
        _tableView.isNeedEmptyPlaceHolder = YES;
        _tableView.verticalOffset = -50;
        _tableView.emptyPlaceHolderBackgroundColor = [UIColor clearColor];
        NSString * title = @"暂无评论，快抢沙发~";
        NSDictionary * Attributes = @{
                                      NSFontAttributeName:[UIFont boldSystemFontOfSize:13.0f],
                                      NSForegroundColorAttributeName:[UIColor colorWithHexString:@"969696"]};
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:title attributes:Attributes];
        _tableView.emptyDescriptionString = attributeString;
        [_container addSubview:_tableView];
        
        if (@available(iOS 11.0, *)) {
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
       
        _textView = [CommentTextView new];
        _textView.delegate = self;
        _textView.vc = self.vc;
        
        [self requestDataWithLoadMoredData:NO];
        
        @weakify(self);
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self requestDataWithLoadMoredData:YES];
        }];
    }
    return self;
}

#pragma mark - > 请求评论列表
- (void)requestDataWithLoadMoredData:(BOOL)isLoadMoredData{
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    if (isLoadMoredData == NO) {
        self.pageIndex = 1;
        
        if (self.commentDataSource.count > 0 ) {
            [self.commentDataSource removeAllObjects];
        }
    }else{
        self.pageIndex +=1;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.feed_id,
                              @"p_comment_id":@"0",
                              @"page":@(self.pageIndex).stringValue,
                              @"page_size":@"20",
                              };
    [request startWithParameters:params WithAction:Get_comment_list WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [_tableView.mj_footer endRefreshing];
            
            self.commentModel = [FWCommentModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            for (FWCommentListModel * model in self.commentModel.comment_list) {
                [self.commentDataSource addObject:[self _topicFrameWithTopic:model]];
            }
            
            self.total_count = self.commentModel.all_comment_count;
            [self showCommentCount:self.total_count];
            
            if (self.commentModel.comment_list.count == 0 &&
                self.pageIndex != 1) {
                _tableView.mj_footer = nil;
                [[FWHudManager sharedManager] showErrorMessage:@"没有更多喽~~" toController:self.vc];
            }else{
                [_tableView reloadData];
            }
        }
    }];
}

#pragma mark - > 显示共多少评论
- (void)showCommentCount:(NSString *)count{
    _label.text = [NSString stringWithFormat:@"共%@条评论",count];
}

#pragma mark - > 请求某个section中更多的消息
- (void)requestCurrentDataWithLoadMoredDataWithSection:(NSInteger)section{
    

    NSInteger page = 1;
    
    if ([self.pageDictonary objectForKey:@(section).stringValue]) {
        page = [[self.pageDictonary objectForKey:@(section).stringValue] integerValue] +1;
    }
    NSLog(@"-----%zd",page);

    /* 当前section回复有多少条 ，如果大于3说明 */
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)self.commentDataSource[section];
    
    NSInteger count = topicFrame.commentFrames.count;
    if (count >=3) {
        count = 3;
    }
    
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];

    NSString * filter_comment_ids = @"";
    if (self.locCommentIDArray.count > 0) {
        filter_comment_ids = [self.locCommentIDArray componentsJoinedByString:@","];
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.feed_id,
                              @"p_comment_id":self.p_comment_id,
                              @"page":@(page).stringValue,
                              @"page_size":@"10",
                              @"default_show_count":@(count).stringValue,
                              @"filter_comment_ids":filter_comment_ids,
                              };
    NSLog(@"params===%@",params);
    [request startWithParameters:params WithAction:Get_comment_list WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {

            self.commentModel = [FWCommentModel mj_objectWithKeyValues:[back objectForKey:@"data"]];
            
            if (self.locCommentIDArray.count > 0) {
                /* 清空数组，下次再用 */
                [self.locCommentIDArray removeAllObjects];
            }
            
            if (self.commentModel.comment_list.count <= 0) {
                FWCommentFooterView * view = (FWCommentFooterView *)[self.tableView footerViewForSection:section];
                view.tipLabel.text = @"~ 别闹，我已经到底线了 ~";
                view.showButton.enabled = NO;
                return ;
            }
            
            NSMutableArray * tempArr = @[].mutableCopy;
            for (FWCommentListModel * model in self.commentModel.comment_list) {
                FWTopicCommentFrame *topicFrame = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                topicFrame.topic = model;
                [tempArr addObject:topicFrame];
            }

            FWTopicCommentFrame * topicCommentFrame = (FWTopicCommentFrame *)self.commentDataSource[section];
            NSMutableArray * commentFrames = topicCommentFrame.commentFrames.mutableCopy;
            
            FWTopicCommentFrame * addCommentFrame = (FWTopicCommentFrame *)tempArr.firstObject;
            for (FWCommentFrame * comment in addCommentFrame.commentFrames) {
                
                [commentFrames insertObject:comment atIndex:commentFrames.count];
            }
            topicCommentFrame.topic.has_more = self.commentModel.has_more;
            topicCommentFrame.commentFrames = commentFrames.mutableCopy;
            
            [UIView performWithoutAnimation:^{
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
                [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }];
            
            /* 存起来 */
            [self.pageDictonary setObject:@(page).stringValue forKey:@(section).stringValue];
        }
    }];
}

#pragma mark - > 删除消息
- (void)requestDeleteMessageWithSection:(NSInteger)section WithRow:(NSInteger)row{
    
    FWCommentMessageRequest * request = [[FWCommentMessageRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.commentModel.feed_id,
                              @"comment_id":self.comment_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_del_comment WithDelegate:self.vc completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)self.commentDataSource[section];
            
            if (row >= 0) {
                [topicFrame.commentFrames removeObjectAtIndex:row];
                
                if ([self.locCommentIDArray containsObject:self.comment_id]) {
                    [self.locCommentIDArray removeObject:self.comment_id];
                }
                [UIView performWithoutAnimation:^{
                    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
                    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
                }];
                
                self.total_count = @([self.total_count integerValue] -1).stringValue;

            }else{
                self.total_count = @([self.total_count integerValue] -[topicFrame.topic.reply_total_count integerValue]-1).stringValue;

                [self.commentDataSource removeObjectAtIndex:section];
                [self.tableView reloadData];
            }
            
            [self showCommentCount:self.total_count];

            [[FWHudManager sharedManager] showSuccessMessage:@"删除成功" toController:self.vc];
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
        }
    }];
}

#pragma mark - >  tableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.commentDataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.commentFrames.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    id model = self.commentDataSource[indexPath.section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentCell *cell = [FWCommentCell cellWithTableView:tableView WithID:@"CommentID"];
        
        cell.userInteractionEnabled = YES;
        cell.contentView.userInteractionEnabled = YES;
        cell.backgroundColor = ColorClear;
        cell.contentView.backgroundColor = ColorClear;
        
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];
        cell.commentFrame = commentFrame;
        cell.nameLabel.textColor = FWViewBackgroundColor_FFFFFF;
        cell.likesLabel.textColor = FWViewBackgroundColor_FFFFFF;
        cell.contentLabel.textColor = FWViewBackgroundColor_FFFFFF;
        cell.viewcontroller = self.vc;
        cell.delegate = self;
        cell.photoImageView.tag = 7777+indexPath.row;

        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id model = self.commentDataSource[indexPath.section];
    
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
    FWCommentFrame *commentFrame = topicFrame.commentFrames[indexPath.row];

    if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:commentFrame.comment.uid]) {
        /* 是本人，提醒删除 */
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.comment_id = commentFrame.comment.comment_id;
            [self requestDeleteMessageWithSection:indexPath.section WithRow:indexPath.row];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
    }else{
        /* 不是本人，回复 */
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.hostComment_id = commentFrame.comment.p_comment_id;
            self.reply_comment_id = commentFrame.comment.comment_id;
            
            self.reply_userInfo = commentFrame.comment.user_info;

            [self.textView.textView becomeFirstResponder];
            self.textView.textView.placeholder = [NSString stringWithFormat:@"回复:%@",commentFrame.comment.user_info.nickname];
            /* 回复 */
            self.currentSection = indexPath.section;
            self.currentRow = indexPath.row;
            
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self.vc presentViewController:alertController animated:YES completion:nil];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWCommentHeaderView *headerView = [FWCommentHeaderView headerViewWithTableView:tableView];
        headerView.commentButton.tag = section +30000;
        headerView.avatarView.tag = section +90000;
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        headerView.topicFrame = topicFrame;
        headerView.backgroundColor = ColorClear;
        headerView.contentView.backgroundColor = ColorClear;
        headerView.nicknameLable.textColor = FWViewBackgroundColor_FFFFFF;
        headerView.createTimeLabel.textColor = FWViewBackgroundColor_FFFFFF;
        headerView.thumbNumberLabel.textColor = FWViewBackgroundColor_FFFFFF;
        headerView.contentLabel.textColor = FWViewBackgroundColor_FFFFFF;
        headerView.viewcontroller = self.vc;
        headerView.delegate = self;
        return headerView;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)self.commentDataSource[section];

    NSInteger page = 1;
    if ([self.pageDictonary objectForKey:@(section).stringValue]) {
        page = [[self.pageDictonary objectForKey:@(section).stringValue] integerValue] +1;
    }
    
    FWCommentFooterView * footerView = [FWCommentFooterView footerViewWithTableView:tableView];
    footerView.delegate = self;
    footerView.showButton.tag = section+44444;
    
    if ([topicFrame.topic.reply_total_count intValue] > 3) {
        footerView.tipLabel.hidden = NO;
        
        if ([topicFrame.topic.has_more isEqualToString:@"2"]) {
            footerView.tipLabel.text = @"收起 ▲";
        }else{
            footerView.tipLabel.text = @"展开更多回复 ▼";
        }
    }else{
        footerView.tipLabel.hidden = YES;
    }


    return footerView;
}

- (void)showButtonOnClickWithIndex:(NSInteger)index WithFooter:(nonnull FWCommentFooterView *)footer{
    
    id model = self.commentDataSource[index];
    
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;

    
    if ([topicFrame.topic.has_more isEqualToString:@"2"]) {
        /* 收起 */
        if ([self.pageDictonary objectForKey:@(index).stringValue]) {
            [self.pageDictonary removeObjectForKey:@(index).stringValue];
        }

        NSMutableArray * tempArr = topicFrame.commentFrames.mutableCopy;
        NSInteger tempArrCount = tempArr.count;
        
        if (tempArrCount >3) {
            /* 容错，只有本地数据大于等于3的时候才允许收起 */
            [tempArr removeObjectsInRange:NSMakeRange(3, tempArrCount-3)];
            
            topicFrame.topic.has_more = @"1";
            topicFrame.commentFrames = tempArr.mutableCopy;
            
            [UIView performWithoutAnimation:^{
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
                [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }];
        }else{
            footer.tipLabel.text = @"~ 别闹，我已经到底线了 ~";
            footer.showButton.enabled = NO;
        }
    }else{
        /* 展开查看更多 */

        self.p_comment_id = topicFrame.topic.comment_id;
        [self requestCurrentDataWithLoadMoredDataWithSection:index];
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)self.commentDataSource[section];
    
    if ([topicFrame.topic.reply_total_count intValue]>3) {
        return 40;
    }
    
    return 0.5f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id model = self.commentDataSource[indexPath.section];
    if ([model isKindOfClass:[FWTopicCommentFrame class]]) {
        FWTopicCommentFrame *videoTopicFrame = (FWTopicCommentFrame *)model;
        FWCommentFrame *commentFrame = videoTopicFrame.commentFrames[indexPath.row];

        return commentFrame.cellHeight;
    }
    
    return .1f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    id model = self.commentDataSource[section];
    
    if ([model isKindOfClass:[FWTopicCommentFrame class]])
    {
        // 话题
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;
        return topicFrame.height;
    }
    
    return .1f;
}

#pragma mark - > 关闭控制器
- (void)closeButtonOnClick{
    
    if ([self.delegate respondsToSelector:@selector(closeCommentsPopView:)]) {
        [self.delegate closeCommentsPopView:self.total_count];
    }
    
    [self dismiss];
}

#pragma mark - > 点击回复的姓名
- (void)commentCell:(FWCommentCell *)commentCell didClickedUser:(FWMineInfoModel *)user{
    
    if (nil == user.uid) {
        return;
    }
    FWNewUserInfoViewController * UIVC = [[FWNewUserInfoViewController alloc] init];
    UIVC.user_id = user.uid;
    [self.vc.navigationController pushViewController:UIVC animated:YES];
}

//update method
- (void)show {
    
    [self.vc.view addSubview:self];
    [UIView animateWithDuration:0.15f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.container.frame;
                         frame.origin.y = frame.origin.y - frame.size.height;
                         self.container.frame = frame;
                     }
                     completion:^(BOOL finished) {
                     }];
    [self.vc.view addSubview:self.textView];
}

- (void)dismiss {
    [UIView animateWithDuration:0.15f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.container.frame;
                         frame.origin.y = frame.origin.y + frame.size.height;
                         self.container.frame = frame;
                         
                         [self endEditing:YES];
                         self.textView.textView.text=@"";
                         if(self.textView.textView.text.length<=0){
                             self.textView.textView.placeholder = @"点赞都是套路，评论才是真情";
                         }
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [self.textView dismiss];
                     }];
}

#pragma mark - > 发评论/回复
-(void)onSendText:(NSString *)text {
    
    if ([self.delegate respondsToSelector:@selector(sendTextOnClick:)]) {
        [self.delegate sendTextOnClick:self];
    }
    
    if (text.length<=0) {
        [[FWHudManager sharedManager] showErrorMessage:@"说点什么吧~" toController:self.vc];
        [self.textView endEditing:YES];
        return;
    }
    
    self.reply_comment_id = self.reply_comment_id?self.reply_comment_id:@"0";
    
    NSString * p_comment_ID ;
    if ([self.hostComment_id intValue] > 0) {
        p_comment_ID = self.hostComment_id;
    }else{
        p_comment_ID = self.p_comment_id;
    }
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:@"0"]) {
        
        NSDictionary * params = @{
                                  @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                  @"feed_id":self.commentModel.feed_id,
                                  @"content":text,
                                  @"p_comment_id":p_comment_ID,
                                  @"reply_comment_id":self.reply_comment_id,
                                  };
        
        FWLikeRequest * request = [[FWLikeRequest alloc] init];
        [request startWithParameters:params WithAction:Submit_add_comment WithDelegate:self.vc  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            
            NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
            
            if ([code isEqualToString:NetRespondCodeSuccess]) {
                
                [self endEditing:YES];
                
                self.total_count = @([self.total_count integerValue] +1).stringValue;
                [self showCommentCount:self.total_count];
                
                FWMineInfoModel * infoModel = [[FWMineInfoModel alloc] init];
                infoModel.nickname = [GFStaticData getObjectForKey:kTagUserKeyName];
                infoModel.uid = [GFStaticData getObjectForKey:kTagUserKeyID];
                infoModel.header_url = [GFStaticData getObjectForKey:kTagUserKeyImageUrl];
                
                FWCommentListModel * model = [[FWCommentListModel alloc] init];
                model.comment_id = [[back objectForKey:@"data"] objectForKey:@"comment_id"];
                model.content = text;
                model.user_info = infoModel;
                model.uid = infoModel.uid;
                model.feed_type = @"1";
                model.is_liked = @"2";
                model.like_count_format = @"0";
                model.format_create_time = @"0秒前";
                
                if(self.commentDataSource.count > 0){
                    // 有数据直接插入
                    if (self.currentSection >=0) {
                        /* 评论（回复）评论（一、二级评论） */
                        
                        NSString * comment_id = [[back objectForKey:@"data"] objectForKey:@"comment_id"];
                        [self.locCommentIDArray addObject:comment_id];
                        
                        FWReplyComment * comment = [[FWReplyComment alloc] init];
                        comment.comment_id = comment_id;
                        comment.content = text;
                        comment.user_info = infoModel;
                        comment.reply_user_info = self.reply_userInfo;
                        comment.uid = infoModel.uid;
                        comment.feed_type = @"1";
                        comment.is_liked = @"2";
                        comment.like_count_format = @"0";
                        comment.format_create_time = @"刚刚";
                        comment.is_reply = @"1";
                        comment.fromMessage = @"1";

                        model.reply_list = [NSMutableArray arrayWithObject:comment];
                        NSMutableArray * tempArr = @[].mutableCopy;
                        
                        FWTopicCommentFrame *topic = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                        topic.topic = model;
                        
                        [tempArr addObject:topic];
                        
                        FWTopicCommentFrame * topicCommentFrame = (FWTopicCommentFrame *)self.commentDataSource[self.currentSection];
                        
                        NSMutableArray * commentFrames = topicCommentFrame.commentFrames.mutableCopy;
                        
                        FWTopicCommentFrame * addCommentFrame = (FWTopicCommentFrame *)tempArr.firstObject;
                        for (FWCommentFrame * comment in addCommentFrame.commentFrames) {
                            
                            if (commentFrames.count >= 3) {
                                [commentFrames insertObject:comment atIndex:commentFrames.count];
                            }else {
                                [commentFrames addObject:comment];
                            }
                        }
                        topicCommentFrame.commentFrames = commentFrames.mutableCopy;
                        
                        [UIView performWithoutAnimation:^{
                            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:self.currentSection];
                            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
                        }];
                    }else{
                        /* 评论了该帖子 */
                        
                        FWTopicCommentFrame *topic = [[FWTopicCommentFrame alloc] initWithType:@"1"];
                        topic.topic = model;
                        
                        [self.commentDataSource insertObject:topic atIndex:0];
                        
                        [self.tableView scrollToTop];
                        [self.tableView reloadData];
                    }
                }else{
                    [self requestDataWithLoadMoredData:NO];
                }
                
                
                self.textView.textView.text = @"";
                self.textView.textView.placeholder = @"点赞都是套路，评论才是真情";
                self.hostComment_id = @"0";
                /* 发送成功后，都置为-1 */
                self.currentSection = -1;
                self.currentRow = -1;
                
                [[FWHudManager sharedManager] showErrorMessage:@"评论成功" toController:self.vc];
            }else{
                [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.vc];
            }
        }];
    }
}

#pragma mark - > 评论主评论
- (void)commentButtonClick:(NSInteger)index{
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID] &&
        ![@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        id model = self.commentDataSource[index];
        
        FWTopicCommentFrame *topicFrame = (FWTopicCommentFrame *)model;

        if ([[GFStaticData getObjectForKey:kTagUserKeyID] isEqualToString:topicFrame.topic.uid]) {
            /* 是本人，提醒删除 */
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该条消息" preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.comment_id = topicFrame.topic.comment_id;
                [self requestDeleteMessageWithSection:index WithRow:-1];
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self.vc presentViewController:alertController animated:YES completion:nil];
        }else{
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            }];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"回复" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.hostComment_id = topicFrame.topic.comment_id;
                self.reply_comment_id = topicFrame.topic.comment_id;
                
                [self.textView.textView becomeFirstResponder];
                self.textView.textView.placeholder = [NSString stringWithFormat:@"回复:%@",topicFrame.topic.user_info.nickname];
                
                self.currentSection = index;
                self.currentRow = -1;
            }];
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            [self.vc presentViewController:alertController animated:YES completion:nil];
        }
    }
}

#pragma mark - 辅助方法
- (FWTopicCommentFrame *)_topicFrameWithTopic:(FWCommentListModel *)topic
{
    for (FWReplyComment * comment in topic.reply_list) {
        comment.fromMessage = @"1";
    }
    // 传递话题模型数据，计算所有子控件的frame
    FWTopicCommentFrame *topicFrame = [[FWTopicCommentFrame alloc] initWithType:@"1"];
    topic.fromMessage = @"1";
    topicFrame.topic = topic;
    return topicFrame;
}
@end


#pragma mark - > ********************  CommentTextView  *********************
#pragma TextView

#define LEFT_INSET                 15
#define RIGHT_INSET                60
#define TOP_BOTTOM_INSET           15

//#define LEFT_INSET 16
//#define RIGHT_INSET 50
//#define TOP_BOTTOM_INSET 15
@interface CommentTextView ()<UITextViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, assign) CGFloat            textHeight;
@property (nonatomic, assign) CGFloat            keyboardHeight;
@property (nonatomic, assign) CGFloat            commentViewBgHeight;
@property (nonatomic, strong) UIVisualEffectView *visualEffectView;
@end

@implementation CommentTextView
- (instancetype)init {
    self = [super init];
    if(self) {
        self.frame = SCREEN_FRAME;
        self.backgroundColor = ColorClear;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGuesture:)]];
        
        if (SCREEN_HEIGHT >= 812) {
            _commentViewBgHeight = 40;
        }else{
            _commentViewBgHeight = 20;
        }
        _container = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 50 - _commentViewBgHeight, SCREEN_WIDTH, 65 + _commentViewBgHeight)];
        _container.backgroundColor = ColorBlack;
        [self addSubview:_container];
        
        _keyboardHeight = _commentViewBgHeight;
        
        _textView = [[IQTextView alloc] initWithFrame:CGRectMake(15, 12, SCREEN_WIDTH-30, 36)];
        _textView.placeholder = @"点赞都是套路，评论才是真情…";
        _textView.textColor = FWTextColor_969696;
        _textView.font = SmallFont;
        _textView.returnKeyType = UIReturnKeySend;
        _textView.layer.masksToBounds = YES;
        _textView.layer.cornerRadius = 2;
        _textView.textContainerInset = UIEdgeInsetsMake(10, LEFT_INSET, 0, LEFT_INSET);
        _textView.backgroundColor = [UIColor colorWithHexString:@"F0F0F0" alpha:0.2];
        _textHeight = ceilf(_textView.font.lineHeight);
        [_container addSubview:_textView];
        [_textView addDoneOnKeyboardWithTarget:self action:@selector(doneAction)];

        
        _textView.delegate = self;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}


#pragma mark - > 发送、done
- (void)doneAction{
    
    [_textView addDoneOnKeyboardWithTarget:self action:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_textView addDoneOnKeyboardWithTarget:self action:@selector(doneAction)];
    });
    
    if (_textView.text.length >0 ) {

        if(_delegate) {
            [_delegate onSendText:_textView.text];
            _textView.text = @"";
            _textHeight = ceilf(_textView.font.lineHeight);
            [_textView resignFirstResponder];
        }
    }else{
        [_textView endEditing:YES];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10.0f, 10.0f)];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    _container.layer.mask = shape;
    
    [self updateTextViewFrame];
}


- (void)updateTextViewFrame {
    CGFloat textViewHeight = _keyboardHeight > _commentViewBgHeight ? _textHeight + 2*TOP_BOTTOM_INSET-9 : ceilf(_textView.font.lineHeight) + 2*TOP_BOTTOM_INSET-9;
    _textView.frame = CGRectMake(15, 12, SCREEN_WIDTH-30, textViewHeight);
    _container.frame = CGRectMake(0, SCREEN_HEIGHT - _keyboardHeight - textViewHeight, SCREEN_WIDTH, textViewHeight + _keyboardHeight);
}

//keyboard notification
- (void)keyboardWillShow:(NSNotification *)notification {
    _keyboardHeight = [notification keyBoardHeight]+25;
    [self updateTextViewFrame];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    _keyboardHeight = _commentViewBgHeight;
    [self updateTextViewFrame];
}

//textView delegate
-(void)textViewDidChange:(UITextView *)textView {
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:textView.attributedText];
    
    if(!textView.hasText) {
        _textHeight = ceilf(_textView.font.lineHeight);
    }else {
        _textHeight = [attributedText multiLineSize:SCREEN_WIDTH - LEFT_INSET - RIGHT_INSET].height;
    }
    [self updateTextViewFrame];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [self doneAction];
        return NO;
    }
    return YES;
}

//handle guesture tap
- (void)handleGuesture:(UITapGestureRecognizer *)sender {
    CGPoint point = [sender locationInView:_textView];
    if(![_textView.layer containsPoint:point]) {
        [_textView resignFirstResponder];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    if(hitView == self){
        if(hitView.backgroundColor == ColorClear) {
            return nil;
        }
    }
    return hitView;
}

//update method
- (void)show {

    [self.vc.view addSubview:self];
}

- (void)dismiss {
    [self removeFromSuperview];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

