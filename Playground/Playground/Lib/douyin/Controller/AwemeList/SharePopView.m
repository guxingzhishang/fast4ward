//
//  SharePopView.m
//  Douyin
//
//  Created by Qiao Shi on 2018/7/30.
//  Copyright © 2018年 Qiao Shi. All rights reserved.
//

#import "SharePopView.h"
#import "Masonry.h"
#import "ShareManager.h"
#import "FWFeedBackRequest.h"

@interface SharePopView ()

@property (nonatomic, strong) UIImageView * containerView;
@property (nonatomic, strong) UIScrollView *bottomScrollView;
@property (nonatomic, strong) UIImageView * picView;
@property (nonatomic, strong) UILabel * titleLabel;//标题
@property (nonatomic, strong) UIImageView * codeImageView;
@property (nonatomic, strong) UILabel * tipLabel;// 提示
@property (nonatomic, strong) UIImageView * nameImageView;// 4FUN 肆放
@property (nonatomic, strong) UILabel * showLabel;// 长按扫码查看
@property (nonatomic, strong) UIActivityIndicatorView * loading;
@property (nonatomic, strong) NSString * downloadName;


@end

@implementation SharePopView

@synthesize containerView;
@synthesize bottomScrollView;
@synthesize picView;
@synthesize titleLabel;
@synthesize codeImageView;
@synthesize tipLabel;
@synthesize nameImageView;
@synthesize showLabel;
@synthesize loading;

- (instancetype)initWithParams:(NSDictionary *)params{
    
    self = [super init];
    if (self) {
        
        self.type = params[@"type"]?params[@"type"]:@"1";
        self.hideDelete = [params[@"hideDelete"] boolValue];
        
        BOOL hideDownload = [params[@"hideDownload"] boolValue];
        
        FWFeedListModel * aweme = params[@"aweme"];

        NSInteger currentIndex = [[params objectForKey:@"currentIndex"] integerValue];

        if (currentIndex >= 0) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                NSData *data ;
                if ([aweme.feed_type isEqualToString:@"2"] ||
                    [aweme.feed_type isEqualToString:@"5"] ||
                    [aweme.feed_type isEqualToString:@"6"]) {
                    data = [NSData dataWithContentsOfURL:[NSURL URLWithString:aweme.imgs[currentIndex].img_url]];
                }else if ([aweme.feed_type isEqualToString:@"3"]){
                    data = [NSData dataWithContentsOfURL:[NSURL URLWithString:aweme.forum_albums[currentIndex].pic]];
                }else if([aweme.feed_type isEqualToString:@"4"]){
                 
                    if (aweme.imgs_original.count >0 ) {
                        data = [NSData dataWithContentsOfURL:[NSURL URLWithString:aweme.imgs[currentIndex].img_url]];
                    }
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (data) {
                        self.shareImage = [[UIImage alloc] initWithData:data];
                    }
                });
            });
        }
       

        if ([aweme.feed_type isEqualToString:@"1"]) {
            self.downloadName = @"下载";
        }else if ([aweme.feed_type isEqualToString:@"2"]){
            self.downloadName = @"保存图片";
        }else if ([aweme.feed_type isEqualToString:@"3"]){
            self.downloadName = @"保存图片";
        }else if ([aweme.feed_type isEqualToString:@"4"]){
            self.downloadName = @"保存图片";
        }else {
            self.downloadName = @"保存图片";
        }
        
        NSMutableArray *topIconsName = @[@"icon_profile_share_wechat",
                                         @"icon_profile_share_wxTimeline"].mutableCopy;
        
        NSMutableArray *topTexts = @[@"微信好友",
                                     @"朋友圈"].mutableCopy;
        
        NSMutableArray *bottomIconsName =
        @[@"icon_home_allshare_favourite",
          @"icon_home_allshare_download",
          @"icon_home_allshare_report",
          @"icon_home_allshare_delete"].mutableCopy;
        
        NSMutableArray *bottomTexts = @[@"收藏",
                                        self.downloadName,
                                        @"举报",
                                        @"删除"].mutableCopy;
        
        CGFloat containerHeight = 280;
        
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
            // 安装微信不做任何操作
        }else{
            [topTexts removeAllObjects];
            [topIconsName removeAllObjects];
            containerHeight = 190;
        }
        
        if ([aweme.is_favourited isEqualToString:@"1"]) {
            [bottomTexts replaceObjectAtIndex:0 withObject:@"取消收藏"];
            [bottomIconsName replaceObjectAtIndex:0 withObject:@"icon_home_allshare_unfavourite"];
        }
        
        if (hideDownload) {
            if([self.type isEqualToString: @"2"] ||
               [self.type isEqualToString: @"3"] ||
               [self.type isEqualToString: @"4"] ||
               [self.type isEqualToString: @"5"]){
                if ([bottomTexts containsObject:self.downloadName]) {
                    [bottomTexts removeObject:self.downloadName];
                    [bottomIconsName removeObject:@"icon_home_allshare_download"];
                }
            }
        }
        
        // 别人帖子，显示举报，自己帖子，显示删除
        if (![aweme.uid isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
            if ([bottomTexts containsObject:@"删除"]) {
                [bottomTexts removeObject:@"删除"];
                [bottomIconsName removeObject:@"icon_home_allshare_delete"];
            }
        }else{
            if ([bottomTexts containsObject:@"举报"]) {
                [bottomTexts removeObject:@"举报"];
                [bottomIconsName removeObject:@"icon_home_allshare_report"];
            }
        }
        
        // 大图页隐藏删除
        if (self.hideDelete) {
            if ([bottomTexts containsObject:@"删除"]) {
                [bottomTexts removeObject:@"删除"];
                [bottomIconsName removeObject:@"icon_home_allshare_delete"];
            }
        }
        
        if (bottomTexts.count == 0) {
            containerHeight = 190;
        }
        
        self.frame = SCREEN_FRAME;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGuesture:)]];
        _container = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, containerHeight + SafeAreaBottomHeight)];
        _container.backgroundColor = ColorBlackAlpha60;
        [self addSubview:_container];
        
        
        UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:_container.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10.0f, 10.0f)];
        CAShapeLayer* shape = [[CAShapeLayer alloc] init];
        [shape setPath:rounded.CGPath];
        _container.layer.mask = shape;
        
        UIBlurEffect *blurEffect =[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
        visualEffectView.frame = self.bounds;
        visualEffectView.alpha = 1.0f;
        [_container addSubview:visualEffectView];
        
        [self setupMiniProgramCodeView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 35)];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        label.text = @"分享到";
        label.textColor = ColorGray;
        label.font = MediumFont;
        [_container addSubview:label];
        
        self.progressView = [[XLCircleProgress alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        self.progressView.center = self.center;
        [DHWindow addDHCustomSubView:self.progressView priority:10000];
//        [self addSubview:self.progressView];
        self.progressView.hidden = YES;
        
        CGFloat itemWidth = SCREEN_WIDTH/topTexts.count;
        
        UIScrollView *topScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, SCREEN_WIDTH, 90)];
        topScrollView.contentSize = CGSizeMake(itemWidth * topIconsName.count, 80);
        topScrollView.showsHorizontalScrollIndicator = NO;
        topScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 30);
        [_container addSubview:topScrollView];
        
        for (NSInteger i = 0; i < topIconsName.count; i++) {
            ShareItem *item = [[ShareItem alloc] initWithFrame:CGRectMake(itemWidth*i, 0, SCREEN_WIDTH/2, 90)];
            item.icon.image = [UIImage imageNamed:topIconsName[i]];
            item.label.text = topTexts[i];
            item.tag = i+9999;
            [item addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onShareItemTap:)]];
            [item startAnimation:i*0.03f];
            [topScrollView addSubview:item];
        }
        
        UIView *splitLine = [[UIView alloc] initWithFrame:CGRectMake(0, 130, SCREEN_WIDTH, 0.5f)];
        splitLine.backgroundColor = ColorWhiteAlpha10;
        [_container addSubview:splitLine];
        
        if (bottomTexts.count == 0) {
            splitLine.hidden = YES;
        }else{
            splitLine.hidden = NO;
        }
        
        CGFloat bottomY = 135;
        if (topTexts.count == 0) {
            
            splitLine.hidden = YES;
            label.hidden = YES;
            bottomY = 30;
        }else{
            splitLine.hidden = NO;
            label.hidden = NO;
        }
        
        CGFloat bottomItemWidth = SCREEN_WIDTH/2;
        if (bottomTexts.count >0) {
            bottomItemWidth = SCREEN_WIDTH/bottomTexts.count;
        }
        bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, bottomY, SCREEN_WIDTH, 90)];
        bottomScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 80);
        bottomScrollView.showsHorizontalScrollIndicator = NO;
        bottomScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 30);
        [_container addSubview:bottomScrollView];
        
        for (int i =0 ; i <bottomTexts.count; i++) {
            
            ShareItem *item = [[ShareItem alloc] initWithFrame:CGRectMake( bottomItemWidth *i, 0,bottomItemWidth, 90)];
            item.icon.image = [UIImage imageNamed:bottomIconsName[i]];
            item.label.text = bottomTexts[i];
            item.tag = i+8888;
            [item startAnimation:i*0.03f];
            [bottomScrollView addSubview:item];
            
            if ([bottomTexts[i] isEqualToString:@"收藏"] ||
                [bottomTexts[i] isEqualToString:@"取消收藏"]) {
                
                [item addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(faouriteItemTap:)]];
            }else if ([bottomTexts[i] isEqualToString:self.downloadName]) {
                
                [item addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(saveItemTap:)]];
            }else if ([bottomTexts[i] isEqualToString:@"举报"]){
                
                [item addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jubaoItemTap:)]];
            }else if ([bottomTexts[i] isEqualToString:@"删除"]){
                
                [item addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteItemTap:)]];
            }
        }
        
        _cancel = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_container.frame)-50-SafeAreaBottomHeight, SCREEN_WIDTH, 50 + SafeAreaBottomHeight)];
        [_cancel setTitleEdgeInsets:UIEdgeInsetsMake(-SafeAreaBottomHeight, 0, 0, 0)];
        
        [_cancel setTitle:@"取消" forState:UIControlStateNormal];
        [_cancel setTitleColor:ColorWhite forState:UIControlStateNormal];
        _cancel.titleLabel.font = BigFont;
        
        _cancel.backgroundColor = ColorGrayLight;
        [_container addSubview:_cancel];
        
        UIBezierPath* rounded2 = [UIBezierPath bezierPathWithRoundedRect:_cancel.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10.0f, 10.0f)];
        CAShapeLayer* shape2 = [[CAShapeLayer alloc] init];
        [shape2 setPath:rounded2.CGPath];
        _cancel.layer.mask = shape2;
        [_cancel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGuesture:)]];
        
        
        loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        loading.center = self.center;
        loading.backgroundColor = [UIColor colorWithHexString:@"000000" alpha:1];
        loading.alpha = 0.5;
        [self addSubview:loading];
        [self bringSubviewToFront:loading];

    }
    
    return self;
}

#pragma mark - >  分享到各平台
- (void)onShareItemTap:(UITapGestureRecognizer *)sender {
    
    switch (sender.view.tag-9999) {
        case 0:
        {
            if (![self.aweme.feed_type isEqualToString:@"1"]) {
                if (!self.shareImage) {
                    return;
                }
            }else{
                [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
            }

            [self shareAtIndex:WXSceneSession];
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];

            [self dismiss];
        }
            break;
        case 1:
        {
            if (![self.aweme.feed_type isEqualToString:@"1"]) {
                if (!self.shareImage) {
                    return;
                }
            }else{
                [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
            }
               
            [self shareAtIndex:WXSceneTimeline];
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];

            [self dismiss];
        }
            break;
        default:
            break;
    }

}

#pragma mark - > ************************************
/** 下载视频 */
- (void)startDownLoadVedioWithModel:(FWFeedListModel *)model {

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    self.downloadTask = [session downloadTaskWithURL:[NSURL URLWithString:model.video_info.downloadURL]];
    self.progressView.hidden = NO;

    // 禁止屏幕点击
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(20 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 20s后允许点击，防止网络不好，一直定屏
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        
    });
    
    [self.downloadTask resume];
}

#pragma mark - > 分享
- (void)shareAtIndex:(NSInteger)index{
    
    if ([self.aweme.feed_type isEqualToString:@"3"]) {
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
            if (index == 0)
            {
                [[ShareManager defaultShareManager] sendWXWithModel:self.aweme WithType:WXSceneSession ];
            }else if (index == 1){
                [[ShareManager defaultShareManager] sendWXWithModel:self.aweme WithType:WXSceneTimeline ];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self.viewcontroller];
        }
    }else if([self.aweme.feed_type isEqualToString:@"4"] ||
             [self.aweme.feed_type isEqualToString:@"5"] ||
             [self.aweme.feed_type isEqualToString:@"6"]){
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
            
            FWFeedListModel * model = [[FWFeedListModel alloc] init];

            if (index == 0){
                model.feed_id = @"other";
                model.share_url = self.aweme.share_url;
                model.share_desc = self.aweme.share_desc;
                model.share_title = self.aweme.share_title;
                model.feed_cover = self.aweme.feed_cover;
                [[ShareManager defaultShareManager] sendWXWithModel:self.aweme WithType:0];
            }else if (index == 1){
                model.feed_id = @"other";
                model.share_url = self.aweme.share_url;
                model.share_desc = self.aweme.share_desc;
                model.share_title = self.aweme.share_title;
                model.feed_cover = self.aweme.feed_cover;
                [[ShareManager defaultShareManager] sendWXWithModel:model WithType:1];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self.viewcontroller];
        }
    }else{
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]) {
            NSLog(@"点了1");
            if (index == 0){
                [[ShareManager defaultShareManager] shareToMinProgramObjectWithModel:self.aweme];
            }else if (index == 1){
                
                [[ShareManager defaultShareManager] shareToPengyouquan:containerView WithFeedID:self.aweme.feed_id WithType:Share_feed_id];
            }
        }else{
            [[FWHudManager sharedManager] showErrorMessage:@"还没有安装微信哟~" toController:self.viewcontroller];
        }
    }
}

#pragma mark - > 保存到相册（下载）
- (void)saveItemTap:(UITapGestureRecognizer *)sender {

    if ([self.aweme.feed_type isEqualToString:@"1"]) {
        /* 下载视频 */
        for (ShareItem * item in bottomScrollView.subviews) {
            if ([item isKindOfClass:[UIButton class]]) {
                if ([item.label.text isEqualToString:self.downloadName]) {
                    item.enabled = NO;
                }
            }
        }
        
        [self startDownLoadVedioWithModel:self.aweme];
    }else{
        /* 下载图片 */
        UIImageWriteToSavedPhotosAlbum(self.shareImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    }
}

#pragma mark - >  保存到本地相册
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    if (error) {
        NSLog(@"保存失败，请重试");
    } else {
        NSLog(@"保存成功");
        [[FWHudManager sharedManager] showErrorMessage:@"已保存至相册" toController:self.viewController];
        [self dismiss];
    }
}

- (void)checkLogin{
    
    if (nil == [GFStaticData getObjectForKey:kTagUserKeyID] ||
        [@"0" isEqualToString:[GFStaticData getObjectForKey:kTagUserKeyID]]) {
        
        [GFStaticData clearUserData];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            /* 登录 */
            [[NSNotificationCenter defaultCenter] postNotificationName:DHSwitchRootViewControllerNotification object:nil userInfo:@{@"rootViewController":Login_with_pop}];
        }];
        [alertController addAction:okAction];
        [self.viewcontroller presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
}

#pragma mark - > 收藏
- (void)faouriteItemTap:(UITapGestureRecognizer *)sender {
    
    NSLog(@"----------shoucang");
    [self checkLogin];
    
    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
        if ([self.aweme.is_favourited isEqualToString:@"1"]) {
            
            [self requestFaouriteWithType:@"cancel"];
        }else{
            
            [self requestFaouriteWithType:@"add"];
        }
    }else{
        [self dismiss];
    }
}

- (void)requestFaouriteWithType:(NSString *)favourite_type{
    NSLog(@"点击了");
    if (!favourite_type ||!self.aweme.feed_id) {
        return;
    }
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.aweme.feed_id,
                              @"favourite_type":favourite_type,
                              };
    
    [request startWithParameters:params WithAction:Submit_favourite_feed WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            ShareItem * item = (ShareItem *)[self viewWithTag:8888];

            if ([favourite_type isEqualToString:@"add"]) {
                if (item) {
                    item.label.text = @"取消收藏";
                    item.icon.image = [UIImage imageNamed:@"icon_home_allshare_unfavourite"];
                    self.aweme.is_favourited = @"1";
                }
                [[FWHudManager sharedManager] showSuccessMessage:@"收藏成功" toController:self.viewcontroller];
            }else if ([favourite_type isEqualToString:@"cancel"]){
                if (item) {
                    item.label.text = @"收藏";
                    item.icon.image = [UIImage imageNamed:@"icon_home_allshare_favourite"];
                    self.aweme.is_favourited = @"2";

                }
                [[FWHudManager sharedManager] showSuccessMessage:@"取消收藏成功" toController:self.viewcontroller];
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self dismiss];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewcontroller];
        }
    }];
}

#pragma mark - > 举报
- (void)jubaoItemTap:(UITapGestureRecognizer *)sender {

    [self checkLogin];
    
    [self dismiss];

    if ([GFStaticData getObjectForKey:kTagUserKeyID]) {
       
        FWFeedBackRequest * request = [[FWFeedBackRequest alloc] init];

        NSDictionary * dict = @{@"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                                @"feed_id":self.aweme.feed_id,
                                @"content":@"举报",
                                };
        
        [request startWithParameters:dict WithAction:Submit_feedback  WithDelegate:self completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
            if ([code isEqualToString:NetRespondCodeSuccess]){
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"举报成功,平台将会在24小时之内给出回复" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                }];
                [alertController addAction:okAction];
                [self.viewcontroller presentViewController:alertController animated:YES completion:nil];
            }
        }];
    }
}

#pragma mark - > 删除
- (void)deleteItemTap:(UITapGestureRecognizer *)sender {
   
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"feed_id":self.aweme.feed_id,
                              };
    
    [request startWithParameters:params WithAction:Submit_delete_feed WithDelegate:self.viewcontroller  completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, nil);
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            [[FWHudManager sharedManager] showErrorMessage:@"删除成功" toController:self.viewController];
            [GFStaticData saveObject:self.aweme.feed_id forKey:Delete_Work_FeedID];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteFeedInfo" object:nil userInfo:nil];
            
            [self dismiss];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.viewcontroller.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [[FWHudManager sharedManager] showErrorMessage:[back objectForKey:@"errmsg"] toController:self.viewController];
        }
    }];
}

#pragma mark NSSessionUrlDelegate
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    //下载进度
    CGFloat progress = totalBytesWritten / (double)totalBytesExpectedToWrite;
    dispatch_async(dispatch_get_main_queue(), ^{
        //进行UI操作  设置进度条
        self.progressView.progress = progress;
    });
}
//下载完成 保存到本地相册
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    //1.拿到cache文件夹的路径
    NSString *cache=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject];
    //2,拿到cache文件夹和文件名
    NSString *file=[cache stringByAppendingPathComponent:downloadTask.response.suggestedFilename];
    
    [[NSFileManager defaultManager] moveItemAtURL:location toURL:[NSURL fileURLWithPath:file] error:nil];
    //3，保存视频到相册
    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(file)) {
        //保存相册核心代码
        UISaveVideoAtPathToSavedPhotosAlbum(file, self, nil, nil);
    }
    
    [self progressOverAndChangeViewContents];
}

//控件本身的代理方法  更新控件样子
- (void)progressOverAndChangeViewContents {

    self.progressView.hidden = YES;
    [[FWHudManager sharedManager] showSuccessMessage:@"视频已成功保存至相册" toController:self.viewcontroller];
    
    // 允许屏幕点击
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    [DHWindow removeSubView:self.progressView];
    [self dismiss];
}


- (void)handleGuesture:(UITapGestureRecognizer *)sender {
    CGPoint point = [sender locationInView:_container];
    if(![_container.layer containsPoint:point]) {
        [self dismiss];
        return;
    }
    point = [sender locationInView:_cancel];
    if([_cancel.layer containsPoint:point]) {
        [self dismiss];
    }
}

- (void)show {
    
    [self setupQRViewWithModel:self.aweme];

    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [window addSubview:self];
    [UIView animateWithDuration:0.15f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.container.frame;
                         frame.origin.y = frame.origin.y - frame.size.height;
                         self.container.frame = frame;
                     }
                     completion:^(BOOL finished) {
                     }];
}

- (void)dismiss {
    
    self.myBlock(self.aweme);

    [UIView animateWithDuration:0.15f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.container.frame;
                         frame.origin.y = frame.origin.y + frame.size.height;
                         self.container.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

#pragma mark - > 分享带小程序二维码的视图
- (void)setupMiniProgramCodeView{
    
    if (!containerView) {
        containerView = [[UIImageView alloc] init];
    }
    containerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [_container addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_container).mas_offset(15);
        make.right.mas_equalTo(_container).mas_offset(-15);
        make.bottom.mas_equalTo(_container.mas_top).mas_offset(-500);
        make.width.mas_equalTo(SCREEN_WIDTH-30);
        make.height.mas_greaterThanOrEqualTo((SCREEN_WIDTH-30)*518/(375-30));
    }];
    
    if (!picView) {
        picView = [[UIImageView alloc] init];
    }
    containerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    picView.contentMode = UIViewContentModeScaleAspectFill;
    picView.clipsToBounds = YES;
    [containerView addSubview:picView];
    [picView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(containerView);
        make.width.mas_equalTo(SCREEN_WIDTH-30);
        make.height.mas_equalTo(SCREEN_WIDTH-30);
    }];
    
    if (!titleLabel) {
        titleLabel = [[UILabel alloc] init];
    }
    titleLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    titleLabel.textColor = FWTextColor_222222;
    titleLabel.numberOfLines = 2;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Semibold" size:15];
    [containerView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(20);
        make.left.mas_equalTo(containerView).mas_offset(15);
        make.right.mas_equalTo(containerView).mas_offset(-15);
        make.width.mas_equalTo(SCREEN_WIDTH-30);
        make.top.mas_equalTo(picView.mas_bottom).mas_offset(11);
    }];
    
    if (!nameImageView) {
        nameImageView = [[UIImageView alloc] init];
    }
    nameImageView.image = [UIImage imageNamed:@"share_sifang"];
    [containerView addSubview:nameImageView];
    [nameImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(13);
        make.width.mas_equalTo(70);
        make.left.mas_equalTo(containerView).mas_offset(15);
        make.bottom.mas_equalTo(containerView.mas_bottom).mas_offset(-26);
    }];
    
    if (!tipLabel) {
        tipLabel = [[UILabel alloc] init];
    }
    tipLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    tipLabel.text = @"我在 肆放APP中\n发现了一条精彩内容";
    tipLabel.numberOfLines = 2;
    tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];;
    tipLabel.textColor = FWColor(@"B5B5B5");
    tipLabel.textAlignment = NSTextAlignmentLeft;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:tipLabel.text];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [tipLabel.text length])];
    tipLabel.attributedText = attributedString;
    [containerView addSubview:tipLabel];
    [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(containerView).mas_offset(13);
        make.bottom.mas_equalTo(nameImageView.mas_top).mas_offset(-15);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_equalTo(120);
    }];
    
    if (!codeImageView) {
        codeImageView = [[UIImageView alloc] init];
    }
    codeImageView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [containerView addSubview:codeImageView];
    [codeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(73, 73));
        make.right.mas_equalTo(containerView).mas_offset(-15);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(20);
    }];
    
    if (!showLabel) {
        showLabel = [[UILabel alloc] init];
    }
    showLabel.backgroundColor = FWViewBackgroundColor_FFFFFF;
    showLabel.text = @"长按扫码查看";
    showLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    showLabel.textColor = FWColor(@"727272");
    showLabel.textAlignment = NSTextAlignmentCenter;
    [containerView addSubview:showLabel];
    [showLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(codeImageView);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(codeImageView.mas_bottom).mas_offset(2);
        make.width.mas_greaterThanOrEqualTo(30);
    }];
}


- (void)setupQRViewWithModel:(FWFeedListModel *)model{
    
    if (!model) return;
    
    [self requestShareXcxCodeWithModel:model WithType:@"feed"];
}

#pragma mark - > 请求小程序二维码
- (void)requestShareXcxCodeWithModel:(id)model WithType:(NSString *)qr_type{
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSString * item_id = ((FWFeedListModel *)model).feed_id;
    
    if (!item_id || !qr_type) {
        return;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"item_id":item_id,
                              @"qr_type":qr_type,
                              };
    [request startWithParameters:params WithAction:Get_xcx_qrcode WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            NSString * qr_url = [back objectForKey:@"qr_url"];

            FWFeedListModel * tempModel = (FWFeedListModel *)model;
            
            
            titleLabel.text = tempModel.share_title;
            [picView sd_setImageWithURL:[NSURL URLWithString:tempModel.feed_cover] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
            }];
            [codeImageView sd_setImageWithURL:[NSURL URLWithString:qr_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
            }];
        }
    }];
}

- (UIImageView *)getCurrenImage{
    
    return containerView;
}

@end



#pragma Item view

@implementation ShareItem
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        self.userInteractionEnabled = YES;
        
        _icon = [[UIImageView alloc] init];
        _icon.image = [UIImage imageNamed:@""];
//        _icon.contentMode = UIViewContentModeScaleToFill;
//        _icon.userInteractionEnabled = YES;
        [self addSubview:_icon];
        
        _label = [[UILabel alloc] init];
        _label.text = @"--";
        _label.textColor = ColorWhiteAlpha60;
        _label.font = MediumFont;
        _label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_label];
    }
    return self;
}
-(void)startAnimation:(NSTimeInterval)delayTime {
    CGRect originalFrame = self.frame;
    self.frame = CGRectMake(CGRectGetMinX(originalFrame), 35, originalFrame.size.width, originalFrame.size.height);
    [UIView animateWithDuration:0.9f
                          delay:delayTime
         usingSpringWithDamping:0.5f
          initialSpringVelocity:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.frame = originalFrame;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(48);
        make.centerX.equalTo(self);
        make.top.equalTo(self).offset(10);
    }];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.icon.mas_bottom).offset(10);
    }];
}

@end
