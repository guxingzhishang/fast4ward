//
//  CommentsPopView.h
//  Douyin
//
//  Created by Qiao Shi on 2018/7/30.
//  Copyright © 2018年 Qiao Shi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "FWCommentModel.h"

@class CommentsPopView;
@protocol CommentsPopViewDelegate <NSObject>

- (void)likesOnClick:(CommentsPopView *)popView;
- (void)sendTextOnClick:(CommentsPopView *)popView;
- (void)closeCommentsPopView:(NSString *)commentsCount;

@end

@interface CommentsPopView:UIView
@property (nonatomic, strong) UILabel       *label;
@property (nonatomic, strong) UIImageView   *close;
@property (nonatomic, strong) UIButton      *closeButton;
@property (nonatomic, strong) NSString      *feed_id;
@property (nonatomic, strong) NSString      *reply_comment_id;
@property (nonatomic, strong) NSString      *p_comment_id;
@property (nonatomic, strong) NSString      *comment_id;
@property (nonatomic, strong) NSString      *total_count;

@property (nonatomic, assign) NSInteger  currentSection;
@property (nonatomic, assign) NSInteger  currentRow;

@property (nonatomic, strong) FWMineInfoModel * reply_userInfo;

/* 本地插入二级评论的消息id，用于展开更多时，传给服务器，然后相同commentid的消息不会被返回 */
@property (nonatomic, strong) NSMutableArray * locCommentIDArray;

@property (nonatomic, weak)   UIViewController      *vc;
@property (nonatomic, weak) id<CommentsPopViewDelegate>delegate;

- (instancetype)initWithFeedID:(NSString *)feed_id;
- (void)show;
- (void)dismiss;
@end




@protocol CommentTextViewDelegate
@required
-(void)onSendText:(NSString *)text;

@end


@interface CommentTextView : UIView
@property (nonatomic, retain) UILabel                        *placeholderLabel;
@property (nonatomic, strong) UIView                         *container;
@property (nonatomic, strong) IQTextView                     *textView;
@property (nonatomic, weak) id<CommentTextViewDelegate>    delegate;
@property (nonatomic, weak) UIViewController * vc;

- (void)show;
- (void)dismiss;
@end
