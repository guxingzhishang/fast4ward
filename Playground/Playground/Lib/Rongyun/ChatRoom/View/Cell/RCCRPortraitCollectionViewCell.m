//
//  RCCRPortraitCollectionViewCell.m
//  ChatRoom
//
//  Created by 罗骏 on 2018/5/10.
//  Copyright © 2018年 罗骏. All rights reserved.
//

#import "RCCRPortraitCollectionViewCell.h"

@implementation RCCRPortraitCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.portaitView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
        self.portaitView.layer.cornerRadius = 35/2.0;
        self.portaitView.layer.masksToBounds = YES;
        [self.contentView addSubview:self.portaitView];
    }
    return self;
}

- (void)setDataModel:(NSString *)header_url {
    
    [self.portaitView sd_setImageWithURL:[NSURL URLWithString:header_url] placeholderImage:[UIImage imageNamed:@"placeholder_photo"]];
}

@end
