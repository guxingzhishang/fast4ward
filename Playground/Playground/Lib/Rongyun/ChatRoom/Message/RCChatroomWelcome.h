
#import <RongIMLib/RongIMLib.h>
//#import <RongIMLib/RCMessageContentView.h>


// ,RCMessageContentView
@interface RCChatroomWelcome : RCMessageContent<NSCoding>


@property(nonatomic, copy) NSString *id;



@property(nonatomic, assign) int counts;



@property(nonatomic, assign) int rank;



@property(nonatomic, assign) int level;



@property(nonatomic, copy) NSString *extra;



@end
