//
//  SWQRCodeViewController.h
//  SWQRCode_Objc
//
//  Created by zhuku on 2018/4/4.
//  Copyright © 2018年 selwyn. All rights reserved.
//

#import "SWQRCode.h"

@interface SWQRCodeViewController : FWBaseViewController

@property (nonatomic, strong) SWQRCodeConfig *codeConfig;
@property (nonatomic, strong) NSString *type; // login / ticket_check / driver_info

- (void)sw_handleWithValue:(NSString *)value ;

@end
