//
//  CZHChooseCoverController.m
//  saveCover
//
//  Created by 程召华 on 2017/10/30.
//  Copyright © 2017年 程召华. All rights reserved.
//
//底部显示的个数
#define PHOTP_COUNT  10

#import "CZHChooseCoverController.h"
#import <AVFoundation/AVFoundation.h>
#import "Header.h"
#import "CZHChooseCoverCell.h"
#import <AssetsLibrary/AssetsLibrary.h>

#import "FWPublishPreviewViewController.h"

typedef NS_ENUM(NSInteger, CZHChooseCoverControllerButtonType) {
    //返回
    CZHChooseCoverControllerButtonTypeBack,
    //完成
    CZHChooseCoverControllerButtonTypeComplete
};
static NSString *const ID = @"CZHChooseCoverCell";
@interface CZHChooseCoverController ()<UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
///
@property (nonatomic, weak) UICollectionView *collectionView;
///图片显示
@property (nonatomic, weak) UIImageView *imageView;
///总帧数
@property (nonatomic, assign) CMTimeValue timeValue;
///比例
@property (nonatomic, assign) CMTimeScale timeScale;
///照片数组
@property (nonatomic, strong) NSMutableArray *photoArrays;
@end

@implementation CZHChooseCoverController
@synthesize infoDitct;

- (NSMutableArray *)photoArrays {
    if (!_photoArrays) {
        _photoArrays = [NSMutableArray array];
    }
    return _photoArrays;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self trackPageBegin:@"设置封面页"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self trackPageEnd:@"设置封面页"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNavigationBarTheme_Black];
    
    self.fd_interactivePopDisabled = YES;
//    self.fd_prefersNavigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

    self.title = @"设置封面";

    infoDitct = @{}.mutableCopy;
    
    [self getVideoTotalValueAndScale];
    
    [self setUpView];
    
    [self setupSubViews];
}

- (void)bindViewModel{
    [super bindViewModel];
}

- (void)setupSubViews{
    
    UIButton * finishButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 25)];
    finishButton.titleLabel.font = DHSystemFontOfSize_14;
    finishButton.titleLabel.textAlignment = NSTextAlignmentRight;
    [finishButton setTitle:@"完成" forState:UIControlStateNormal];
    [finishButton setTitleColor:FWTextColor_12101D forState:UIControlStateNormal];
    [finishButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    finishButton.tag = CZHChooseCoverControllerButtonTypeComplete;

    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:finishButton];
    self.navigationItem.rightBarButtonItems = @[rightBtnItem];
}

- (void)getVideoTotalValueAndScale {
    
    AVURLAsset * asset = [AVURLAsset assetWithURL:self.videoPath];
    CMTime  time = [asset duration];
    self.timeValue = time.value;
    self.timeScale = time.timescale;
    
    if (time.value < 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    NSDictionary *opts = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:self.videoPath options:opts];
    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:urlAsset];
    generator.appliesPreferredTrackTransform = YES;
    
    long long baseCount = time.value / PHOTP_COUNT;
    //取出PHOTP_COUNT张图片,存放到数组，用于collectionview
    for (NSInteger i = 0 ; i < PHOTP_COUNT; i++) {
        
        NSError *error = nil;
        CGImageRef img = [generator copyCGImageAtTime:CMTimeMake(i * baseCount, time.timescale) actualTime:NULL error:&error];
        {
            UIImage *image = [UIImage imageWithCGImage:img];
            
            [self.photoArrays addObject:image];
        }
    }
}

- (void)setUpView {
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(20, 20, SCREEN_WIDTH-40, SCREEN_HEIGHT-300);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = YES;
    imageView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:imageView];
    self.imageView = imageView;
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(SCREEN_WIDTH/PHOTP_COUNT, SCREEN_WIDTH/PHOTP_COUNT);
    layout.minimumInteritemSpacing = 0;
    
    CGRect collectionViewF = CGRectMake(0,CGRectGetMaxY(imageView.frame)+50, SCREEN_WIDTH, CZH_ScaleHeight(62.5));
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:collectionViewF collectionViewLayout:layout];
    collectionView.dataSource = self;
    collectionView.backgroundColor = [UIColor clearColor];
    [collectionView registerClass:[CZHChooseCoverCell class] forCellWithReuseIdentifier:ID];
    [self.view addSubview:collectionView];
    self.collectionView  = collectionView;
    
    
    UIImage *selected = [UIImage imageNamed:@"slider_select"];
    UIImage *deselected = [UIImage imageNamed:@"slider_deselect"];
    
    UISlider *slider = [[UISlider alloc] init];
    slider.frame = CGRectMake(0,  CGRectGetMinY(collectionView.frame), SCREEN_WIDTH, CZH_ScaleHeight(62.5));
    
    [slider setThumbImage:deselected forState:UIControlStateNormal];
    [slider setThumbImage:selected forState:UIControlStateHighlighted];
    //透明的图片
    UIImage *image = [CZHTool imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 1)];
    
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    
    slider.maximumValue = self.timeValue;
    slider.minimumValue = 0;
    slider.value = self.timeValue/10;
    [slider addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:slider];

    //手动选取一张
    [self chooseWithTime:1];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photoArrays.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CZHChooseCoverCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];
    cell.coverImage = self.photoArrays[indexPath.item];
    return cell;
}


- (void)valueChange:(UISlider *)sender {
    
    int timeValue = sender.value;
    
    [self chooseWithTime:timeValue];
}


- (void)chooseWithTime:(CMTimeValue)time {
    
    NSDictionary *opts = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:self.videoPath options:opts];
    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:urlAsset];
    generator.appliesPreferredTrackTransform = YES;    
    
    NSError *error = nil;
    CGImageRef img = [generator copyCGImageAtTime:CMTimeMake(time, self.timeScale) actualTime:NULL error:&error];
    {
        UIImage *image = [UIImage imageWithCGImage:img];
        
        self.imageView.image = image;
    }
}


- (UIImage *)convertViewToImage
{
    UIGraphicsBeginImageContext(self.collectionView.bounds.size);
    [self.collectionView drawViewHierarchyInRect:self.collectionView.bounds afterScreenUpdates:YES];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenshot;
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)buttonClick:(UIButton *)sender {
    if (sender.tag == CZHChooseCoverControllerButtonTypeBack) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else if (sender.tag == CZHChooseCoverControllerButtonTypeComplete) {
        
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *dataString = [dateFormatter stringFromDate:date];
        
        //设置一个图片的存储路径
        NSString *imagePath = [NSHomeDirectory() stringByAppendingFormat:@"/tmp/%@.png",dataString];

        //把图片直接保存到指定的路径（同时应该把图片的路径imagePath存起来，下次就可以直接用来取）
        [UIImagePNGRepresentation(self.imageView.image) writeToFile:imagePath atomically:YES];
        
        [infoDitct setObject:imagePath forKey:@"UIImagePath"];

        if (self.isEdit) {
            // 封面图片回调
            if (_coverImageBlock) {
                _coverImageBlock(infoDitct);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            FWPublishPreviewViewController * PVC = [FWPublishPreviewViewController new];
            PVC.filePath = self.filePath;
            PVC.coverImageDict = infoDitct;
            PVC.pathURL = self.videoPath;
            PVC.draft_id = @"0";
            PVC.editType = 1;
            [self.navigationController pushViewController:PVC animated:YES];
        }
    }
}

@end
