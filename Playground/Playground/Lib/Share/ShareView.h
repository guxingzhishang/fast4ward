//
//  ShareView.h
//  StockMaster
//
//  Created by dalikeji on 15/2/9.
//  Copyright (c) 2015年 Rebloom. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "NSString+UIColor.h"
//#import "UIImage+UIColor.h"
//#import "NSTimer+Addition.h"

@protocol ShareViewDelegate <NSObject>
@required

- (void)shareAtIndex:(NSInteger)index;

@end

@interface ShareView : UIView

@property (nonatomic, strong) UIView * mainView;
@property (nonatomic, strong) UIImageView * feetView;
@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UILabel * contentLabel;

@property (nonatomic, assign) BOOL isAlert;

@property (nonatomic, weak) id<ShareViewDelegate>shareDelegate;

+ (ShareView *)defaultShareView;
- (void) showView;
- (void) hideView;

/**
 分享视图

 @param number 有几个图标
 @param _delegate 代理
 @param viewController 哪个控制器
 @param type 类型（1、白底，显示详情；2黑底，没详情）
 */
- (id)initViewWtihNumber:(NSInteger)number Delegate:(id)_delegate WithViewController:(UIViewController*)viewController WithType:(NSInteger)type;

- (UIImageView *)getCurrenImage;

- (void)setupQRViewWithModel:(FWFeedListModel *)model;
- (void)setupUserQRViewWithModel:(FWMineModel *)model;
- (void)setupTopicQRViewWithModel:(FWFeedModel *)model;

@end
