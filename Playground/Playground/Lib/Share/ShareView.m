//
//  ShareView.m
//  StockMaster
//
//  Created by dalikeji on 15/2/9.
//  Copyright (c) 2015年 Rebloom. All rights reserved.
//

#import "ShareView.h"

@interface ShareView ()

@property (nonatomic, strong) UIImageView * containerView;
@property (nonatomic, strong) UIImageView * picView;
@property (nonatomic, strong) UILabel * titleLabel;//标题
@property (nonatomic, strong) UIImageView * codeImageView;
@property (nonatomic, strong) UILabel * tipLabel;// 提示
@property (nonatomic, strong) UIImageView * nameImageView;// 4FUN 肆放
@property (nonatomic, strong) UILabel * showLabel;// 长按扫码查看
@property (nonatomic, strong) UIActivityIndicatorView * loading;
@end

@implementation ShareView
@synthesize shareDelegate;
@synthesize mainView;
@synthesize isAlert;
@synthesize feetView;
@synthesize contentLabel;
@synthesize iconImageView;
@synthesize containerView;
@synthesize picView;
@synthesize titleLabel;
@synthesize nameImageView;
@synthesize codeImageView;
@synthesize tipLabel;
@synthesize showLabel;
@synthesize loading;

+ (ShareView *)defaultShareView
{
    static ShareView * defaultShareView = nil;
    if (!defaultShareView)
    {
        defaultShareView = [[ShareView alloc] init];
    }
    return defaultShareView;
}

- (id)initViewWtihNumber:(NSInteger)number Delegate:(id)_delegate WithViewController:(UIViewController*)viewController WithType:(NSInteger)type
{
    [self clearSubview];
    
    if (self = [super init])
    {
        if(!mainView)
        {
            mainView = [[UIView alloc] init];
        }
        mainView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        mainView.hidden = NO;
        mainView.backgroundColor = [UIColor clearColor];
        [[UIApplication sharedApplication].keyWindow addSubview:mainView];

        
        UIView * topView = [[UIView alloc] init];
        topView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        topView.alpha = 0.5;
        topView.backgroundColor = FWTextColor_000000;
        [mainView addSubview:topView];
        
        UIButton * hideBtn = [[UIButton alloc] init];
        hideBtn.frame = topView.frame;
        hideBtn.tag = 10;
        hideBtn.backgroundColor = [UIColor clearColor];
        [hideBtn addTarget:self action:@selector(hideView) forControlEvents:UIControlEventTouchUpInside
         ];
        [mainView addSubview:hideBtn];
        
        if (!feetView)
        {
            feetView = [[UIImageView alloc] init];
        }
        feetView.backgroundColor = FWTextColor_000000;
        feetView.userInteractionEnabled = YES;
        feetView.hidden = NO;
        [mainView addSubview:feetView];
        if (type == 1) {
            feetView.frame = CGRectMake(0, SCREEN_HEIGHT - 300, SCREEN_WIDTH, 300);
        }else{
            feetView.frame = CGRectMake(0, SCREEN_HEIGHT - 260, SCREEN_WIDTH, 260);
        }
        
        
        UILabel * tipLabel = [[UILabel alloc] init];
        tipLabel.text = @"分享至";
        tipLabel.textAlignment = NSTextAlignmentCenter;
        tipLabel.font = DHSystemFontOfSize_14;
        [feetView addSubview:tipLabel];
        tipLabel.frame = CGRectMake(0, 10, SCREEN_WIDTH, 20);
        
        UIView * contentBgView = [[UIView alloc] init];
        contentBgView.backgroundColor = FWViewBackgroundColor_F3F3F3;
        contentBgView.frame = CGRectMake(30, CGRectGetMaxY(tipLabel.frame)+28, SCREEN_WIDTH-60, 60);
        [feetView addSubview:contentBgView];
        
        if (!iconImageView) {
            iconImageView = [[UIImageView alloc] init];
        }
        iconImageView.frame = CGRectMake(CGRectGetMinX(contentBgView.frame)+10, CGRectGetMinY(contentBgView.frame)+10, 40, 40);
        [feetView addSubview:iconImageView];
        
        if (!contentLabel) {
            contentLabel = [[UILabel alloc] init];
        }
        contentLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame)+10, CGRectGetMinY(iconImageView.frame), SCREEN_WIDTH-130, 40);
        contentLabel.textColor = FWTextColor_000000;
        contentLabel.numberOfLines = 2;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        contentLabel.font = DHSystemFontOfSize_14;
        [feetView addSubview:contentLabel];

        NSArray * titleArray = @[@"微信好友",@"朋友圈",@"QQ",@"QQ空间",@"微博"];
        NSArray * imageArray = @[@"weixin",@"pengyouquan",@"qq",@"qzone",@"weibo"];
        
        for (int i = 0; i< number; i++){
            
            int row = i/number;
            int col = i % number;
            CGFloat btnWidth;
            CGFloat btnHeight;

            if (number < 5) {
                btnWidth = SCREEN_WIDTH/number;
                btnHeight = 110;
            }else{
                btnWidth = SCREEN_WIDTH/4;
                btnHeight = 110;
            }
            
            UIButton * btn = [[UIButton alloc] init];
            btn.tag = i;
            [btn addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
            [feetView addSubview:btn];
            if (type == 1) {
                btn.frame = CGRectMake(col *btnWidth, row *btnHeight+130, btnWidth, btnHeight);
            }else{
                btn.frame = CGRectMake(col *btnWidth, row *btnHeight+60, btnWidth, btnHeight);
            }
            
            UIImageView * imgView = [[UIImageView alloc] init];
            imgView.image = [UIImage imageNamed:imageArray[i]];
            [btn addSubview:imgView];
            [feetView bringSubviewToFront:imgView];
            [imgView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(50, 50));
                make.centerX.mas_equalTo(btn);
                make.top.mas_equalTo(btn).mas_offset(20);
            }];
            
            UILabel * label = [[UILabel alloc] init];
            label.text = titleArray[i];
            label.textColor = FWTextColor_A6A6A6;
            label.textAlignment = NSTextAlignmentCenter;
            label.font = DHSystemFontOfSize_14;
            [btn addSubview:label];
            [label mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(btn);
                make.height.mas_equalTo(20);
                make.centerX.mas_equalTo(btn);
                make.top.mas_equalTo(imgView.mas_bottom).mas_offset(10);
            }];
        }
        
        UIButton * cancelBtn = [[UIButton alloc] init];
        cancelBtn.tag = 10;
        cancelBtn.layer.cornerRadius = 5;
        cancelBtn.layer.masksToBounds = YES;
        if (SCREEN_HEIGHT>=812) {
            cancelBtn.titleEdgeInsets = UIEdgeInsetsMake(-10, 0, 0, 0);
        }
        cancelBtn.titleLabel.font = DHSystemFontOfSize_16;
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBtn addTarget:self action:@selector(hideView) forControlEvents:UIControlEventTouchUpInside];
        [feetView addSubview:cancelBtn];
        
        UIView * lineView = [[UIView alloc] init];
        lineView.backgroundColor = DHViewBackgroundColor_E2E2E2;
        [feetView addSubview:lineView];
        lineView.frame = CGRectMake(0, CGRectGetHeight(feetView.frame)-46, SCREEN_WIDTH, 1);

        if (type == 1) {
            // 白底
            contentBgView.hidden = NO;
            iconImageView.hidden = NO;
            contentLabel.hidden = NO;
            lineView.hidden = NO;
            
            feetView.backgroundColor = FWViewBackgroundColor_FFFFFF;
            tipLabel.textColor = FWTextColor_000000;

            [cancelBtn setBackgroundColor:FWViewBackgroundColor_FFFFFF];
            [cancelBtn setTitleColor:FWTextColor_000000 forState:UIControlStateNormal];
            cancelBtn.frame = CGRectMake(20, CGRectGetHeight(feetView.frame)-44, SCREEN_WIDTH - 40, 44);
        }else{
            // 黑底
            contentBgView.hidden = YES;
            iconImageView.hidden = YES;
            contentLabel.hidden = YES;
            lineView.hidden = YES;

            tipLabel.textColor = FWTextColor_969696;

            [cancelBtn setBackgroundColor:FWTextColor_000000];
            [cancelBtn setTitleColor:FWTextColor_969696 forState:UIControlStateNormal];
            cancelBtn.frame = CGRectMake(0, CGRectGetHeight(feetView.frame)-44-FWSafeBottom, SCREEN_WIDTH, 44+FWSafeBottom);
        }
        
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.5];
        [animation setType: kCATransitionMoveIn];
        [animation setSubtype: kCATransitionFromTop];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        
        [feetView.layer addAnimation:animation forKey:nil];
        
        [self setupMiniProgramCodeView];

        self.shareDelegate = _delegate;
        
        loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        loading.center = mainView.center;
        loading.backgroundColor = [UIColor colorWithHexString:@"000000" alpha:0.8];
        loading.alpha = 0.5;
        [mainView addSubview:loading];
        [mainView bringSubviewToFront:loading];
    }
    
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    return self;
}


- (void)showView
{
    mainView.hidden = NO;
    feetView.hidden = NO;
}

- (void)buttonOnClick:(id)sender
{
    loading.hidden = NO;
    [loading startAnimating];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        loading.hidden = YES;
        [loading stopAnimating];

        if ([self.shareDelegate respondsToSelector:@selector(shareAtIndex:)])
        {
            [self hideView];
            
            UIButton * btn = (UIButton *)sender;
            [self.shareDelegate shareAtIndex:btn.tag];
        }
    });
}

// 点击取消按钮
- (void)hideView{
    
    [self performSelector:@selector(handleTimer) withObject:self afterDelay:0.1];

    [UIView beginAnimations:@"Animation" context:nil];
    [UIView setAnimationDuration:0.4];
    feetView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 202);
    [UIView commitAnimations];
}

-(void)handleTimer
{
    mainView.hidden = YES;
    feetView.hidden = YES;
    feetView.frame = CGRectMake(0, SCREEN_HEIGHT - 202, SCREEN_WIDTH, 202);
    [self clearSubview];
}

// 清除视图块子视图
-(void)clearSubview
{
    if (mainView){
        for (UIView * view in mainView.subviews){
            [view removeFromSuperview];
        }
    }
   
    if (feetView){
        for (UIView * view in feetView.subviews){
            feetView.backgroundColor = [UIColor clearColor];
            [view removeFromSuperview];
        }
    }
    
    if (containerView) {
        for (UIView * view in containerView.subviews){
            [view removeFromSuperview];
        }
    }
}

- (void)setupMiniProgramCodeView{
    
    if (!containerView) {
        containerView = [[UIImageView alloc] init];
    }
    containerView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [mainView addSubview:containerView];
    [containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mainView).mas_offset(15);
        make.right.mas_equalTo(mainView).mas_offset(-15);
        make.bottom.mas_equalTo(mainView.mas_top).mas_offset(-500);
        make.width.mas_equalTo(SCREEN_WIDTH-30);
        make.height.mas_greaterThanOrEqualTo((SCREEN_WIDTH-30)*518/(375-30));
    }];
    
    if (!picView) {
        picView = [[UIImageView alloc] init];
    }
    picView.contentMode = UIViewContentModeScaleAspectFill;
    picView.clipsToBounds = YES;
    [containerView addSubview:picView];
    [picView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(containerView);
        make.width.mas_equalTo(SCREEN_WIDTH-30);
        make.height.mas_equalTo(SCREEN_WIDTH-30);
    }];
    
    if (!titleLabel) {
        titleLabel = [[UILabel alloc] init];
    }
    titleLabel.textColor = FWTextColor_222222;
    titleLabel.numberOfLines = 2;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font =  [UIFont fontWithName:@"PingFangSC-Semibold" size:15];
    [containerView addSubview:titleLabel];
    [titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(20);
        make.left.mas_equalTo(containerView).mas_offset(15);
        make.right.mas_equalTo(containerView).mas_offset(-15);
        make.width.mas_equalTo(SCREEN_WIDTH-30);
        make.top.mas_equalTo(picView.mas_bottom).mas_offset(11);
    }];
    
    if (!nameImageView) {
        nameImageView = [[UIImageView alloc] init];
    }
    nameImageView.image = [UIImage imageNamed:@"share_sifang"];
    [containerView addSubview:nameImageView];
    [nameImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_greaterThanOrEqualTo(13);
        make.width.mas_equalTo(70);
        make.left.mas_equalTo(containerView).mas_offset(15);
        make.bottom.mas_equalTo(containerView.mas_bottom).mas_offset(-26);
    }];
    
    if (!tipLabel) {
        tipLabel = [[UILabel alloc] init];
    }
    tipLabel.text = @"我在 肆放APP中\n发现了一条精彩内容";
    tipLabel.numberOfLines = 2;
    tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];;
    tipLabel.textColor = FWColor(@"B5B5B5");
    tipLabel.textAlignment = NSTextAlignmentLeft;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:tipLabel.text];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [tipLabel.text length])];
    tipLabel.attributedText = attributedString;
    [containerView addSubview:tipLabel];
    [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(containerView).mas_offset(13);
        make.bottom.mas_equalTo(nameImageView.mas_top).mas_offset(-15);
        make.height.mas_greaterThanOrEqualTo(20);
        make.width.mas_equalTo(120);
    }];
    
    if (!codeImageView) {
        codeImageView = [[UIImageView alloc] init];
    }
    codeImageView.backgroundColor = FWViewBackgroundColor_FFFFFF;
    [containerView addSubview:codeImageView];
    [codeImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(73, 73));
        make.right.mas_equalTo(containerView).mas_offset(-15);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(20);
    }];
    
    if (!showLabel) {
        showLabel = [[UILabel alloc] init];
    }
    showLabel.text = @"长按扫码查看";
    showLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:12];
    showLabel.textColor = FWColor(@"727272");
    showLabel.textAlignment = NSTextAlignmentCenter;
    [containerView addSubview:showLabel];
    [showLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(codeImageView);
        make.height.mas_equalTo(20);
        make.top.mas_equalTo(codeImageView.mas_bottom).mas_offset(2);
        make.width.mas_greaterThanOrEqualTo(30);
    }];
}

#pragma mark - > 带小程序的二维码，帖子（视频、图文）分享到朋友圈
- (void)setupQRViewWithModel:(FWFeedListModel *)model{
    
    if (!model) return;
    
    [self requestShareXcxCodeWithModel:model WithType:@"feed"];
}

#pragma mark - > 带小程序的二维码，个人页分享到朋友圈
- (void)setupUserQRViewWithModel:(FWMineModel *)model{
    
    if (!model) return;
    
    [self requestShareXcxCodeWithModel:model WithType:@"user"];
}

#pragma mark - > 带小程序的二维码，话题页分享到朋友圈
- (void)setupTopicQRViewWithModel:(FWFeedModel *)model{
    
    if (!model) return;
    
    [self requestShareXcxCodeWithModel:model WithType:@"tag"];
}

- (void)dealWithTitle:(NSString *)title WithImageURL:(NSString *)imageURL withCode:(NSString *)xcx_qrcode{
    titleLabel.text = title;
    [picView sd_setImageWithURL:[NSURL URLWithString:imageURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
    }];
    [codeImageView sd_setImageWithURL:[NSURL URLWithString:xcx_qrcode] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
    }];
}

#pragma mark - > 请求小程序二维码
- (void)requestShareXcxCodeWithModel:(id)model WithType:(NSString *)qr_type{
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSString * item_id ;
    
    if ([qr_type isEqualToString:@"feed"]) {
        item_id = ((FWFeedListModel *)model).feed_id;
    }else if ([qr_type isEqualToString:@"tag"]){
        item_id = ((FWFeedModel *)model).tag_info.tag_id;
    }else if ([qr_type isEqualToString:@"user"]){
        item_id = ((FWMineModel *)model).user_info.uid;
    }
    
    if (!item_id || !qr_type) {
        return;
    }
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              @"item_id":item_id,
                              @"qr_type":qr_type,
                              };
    [request startWithParameters:params WithAction:Get_xcx_qrcode WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {
        
        NSDictionary * back = HYGET_OBJECT_FROMDIC(responseObject, @"data");
        
        if ([code isEqualToString:NetRespondCodeSuccess]) {
            
            NSString * qr_url = [back objectForKey:@"qr_url"];
            if ([qr_type isEqualToString:@"feed"]) {
                FWFeedListModel * tempModel = (FWFeedListModel *)model;
                
                [self dealWithTitle:tempModel.feed_title WithImageURL:tempModel.feed_cover withCode:qr_url];
            }else if ([qr_type isEqualToString:@"tag"]){
                
                FWFeedModel * tempModel = (FWFeedModel *)model;
                [self dealWithTitle:tempModel.tag_info.share_title WithImageURL:tempModel.tag_info.tag_image withCode:qr_url];
            }else if ([qr_type isEqualToString:@"user"]){
                FWMineModel * tempModel = (FWMineModel *)model;

                [self dealWithTitle:tempModel.user_info.share_title WithImageURL:tempModel.user_info.header_url withCode:qr_url];
            }
        }
    }];
}

- (UIImageView *)getCurrenImage{
    
    return containerView;
}
@end
