//
//  ShareManager.m
//  StockMaster
//
//  Created by dalikeji on 15/2/9.
//  Copyright (c) 2015年 Rebloom. All rights reserved.
//

#import "ShareManager.h"

@implementation ShareManager

+ (ShareManager *)defaultShareManager
{
    static ShareManager * defaultShareManager = nil;
    if (!defaultShareManager)
    {
        defaultShareManager = [[ShareManager alloc] init];
    }
    
    return defaultShareManager;
}

#pragma mark - > 微信分享（H5）
- (void)sendWXWithModel:(FWFeedListModel *)model WithType:(int)type{

    NSString * shareTo ;
    if (type == 0) {
        shareTo = @"weixinchat";
    }else{
        shareTo = @"weixinpyq";
    }
    
    if (model.feed_id.length >0) {
        [self requestShareFeedWithFeedID:model.feed_id WithType:Share_feed_id WithShareTo:shareTo];
    }
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.feed_cover]];
    UIImage * cover = [UIImage imageWithData:data];
    UIImage *thumbImage = [self compressImage:cover toByte:32768];

    WXMediaMessage *message = [WXMediaMessage message];
    message.title = model.share_title;
    message.description = model.share_desc;
    
    [message setThumbImage:thumbImage];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl = [NSString stringWithFormat:@"%@",model.share_url];
    
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = type;
    
    [WXApi sendReq:req];
}

#pragma mark - >  微信个人/好友/话题分享(正常分享)
- (void)sendWXWithUrl:(NSURL*)share_url WithTitle:(NSString*)share_title  WithDesc:(NSString*)share_desc WithType:(int)type WithPicURL:(NSString *)urlstring{
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:urlstring]];
    UIImage *image = [UIImage imageWithData:data];

    WXMediaMessage *message = [WXMediaMessage message];
    message.title = share_title;
    message.description = share_desc;
    [message setThumbImage:image];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl = [NSString stringWithFormat:@"%@",share_url];
    
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = type;
    
    [WXApi sendReq:req];
}

- (void)shareToPengyouquan:(UIView *)aimView WithFeedID:(NSString *)feed_id WithType:(NSString *)type{
    
    if (!aimView) return;
    
    if (feed_id.length >0) {
        [self requestShareFeedWithFeedID:feed_id WithType:type WithShareTo:@"weixinpyq"];
    }
    
    UIGraphicsBeginImageContextWithOptions(aimView.bounds.size, NO, 0.0f);
    [aimView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentPath stringByAppendingPathComponent:@"orc.jpg"];
    NSData *imageData = UIImageJPEGRepresentation(viewImage, 1);
    [imageData writeToFile:filePath atomically:NO];
    
    UIImage *thumbImage = [self compressImage:viewImage toByte:32768];
    
    WXImageObject *ext = [WXImageObject object];
    // 小于10MB
    ext.imageData = imageData;
    
    WXMediaMessage *message = [WXMediaMessage message];
    message.mediaObject = ext;
    // 缩略图 小于32KB
    [message setThumbImage:thumbImage];
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.scene = WXSceneTimeline;
    req.message = message;
    [WXApi sendReq:req];
}

#pragma mark - > 帖子（视频、图文）分享到小程序
- (void)shareToMinProgramObjectWithModel:(FWFeedListModel *)model{
    
    NSLog(@"点了2");

    if (model.feed_id.length >0) {
        [self requestShareFeedWithFeedID:model.feed_id WithType:Share_feed_id WithShareTo:@"weixinchat"];
    }
    
    NSString * path = @"";
    if ([model.feed_type isEqualToString:@"1"]) {
        path = [NSString stringWithFormat:@"pages/show/video/video?feed_id=%@",model.feed_id_encode]; //小程序视频页面的路径
    }else if ([model.feed_type isEqualToString:@"2"]) {
        path = [NSString stringWithFormat:@"pages/show/picture/picture?feed_id=%@",model.feed_id_encode]; //小程序图文页面的路径
    }else if ([model.feed_type isEqualToString:@"3"]) {
        
    }
    NSLog(@"点了3");

    [self SendMessageToMiniProgram:model.feed_cover_nowatermark WithPath:path WtihTitle:model.share_title];
}

#pragma mark - > 个人页分享到小程序
- (void)userShareToMinProgramObjectWithModel:(FWMineModel *)model{
    
    if (model.user_info.uid.length >0) {
        [self requestShareFeedWithFeedID:model.user_info.uid WithType:Share_uid  WithShareTo:@"weixinchat"];
    }
    
    NSString * imageString = model.user_info.header_url;
    NSString * path = [NSString stringWithFormat:@"pages/Homepage/Homepage?uid=%@",model.user_info.uid_enc];
    
    [self SendMessageToMiniProgram:imageString WithPath:path WtihTitle:model.user_info.share_title];
}

#pragma mark - > 话题页分享到小程序
- (void)topicShareToMinProgramObjectWithModel:(FWFeedModel *)model{
    
    if (model.tag_info.tag_id.length >0) {
        [self requestShareFeedWithFeedID:model.tag_info.tag_id WithType:Share_tag_id  WithShareTo:@"weixinchat"];
    }
    
    NSString * imageString = model.tag_info.tag_image;
    NSString * path = [NSString stringWithFormat:@"pages/topic/topic?tag_id=%@",model.tag_info.tag_id_enc];
    
    [self SendMessageToMiniProgram:imageString WithPath:path WtihTitle:model.tag_info.share_title];
}

#pragma mark - > 裁剪图片，作为小程序的封面
- (UIImage *)ct_imageFromImage:(UIImage *)image inRect:(CGRect)rect{
    
    //把像 素rect 转化为 点rect（如无转化则按原图像素取部分图片）
    CGFloat scale = [UIScreen mainScreen].scale;
    CGFloat x= rect.origin.x*scale,y=rect.origin.y*scale,w=rect.size.width*scale,h=rect.size.height*scale;
    CGRect dianRect = CGRectMake(x, y, w, h);
    
    //截取部分图片并生成新图片
    CGImageRef sourceImageRef = [image CGImage];
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, dianRect);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    return newImage;
}

- (void)SendMessageToMiniProgram:(NSString *)imageString WithPath:(NSString *)path WtihTitle:(NSString *)title{
   

    NSData * data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:imageString]];
    
    UIImage *image = [UIImage imageWithData:data];

//    //获取宽高，以小的那一侧为基准
//    CGRect rect;
//    CGFloat fixelW = CGImageGetWidth(image.CGImage);
//    CGFloat fixelH = CGImageGetHeight(image.CGImage);
//    if (fixelW >=fixelH) {
//        rect = CGRectMake((fixelW-fixelH)/2, 0, fixelH*5/4, fixelH);
//    }else{
//        rect = CGRectMake(0, (fixelH-fixelW)/2, fixelW*4/5, fixelW);
//    }
//
//    UIImage * newImage = [self ct_imageFromImage:image inRect:rect];
//    NSData *imageData = UIImageJPEGRepresentation(newImage, 0);
    
    WXMiniProgramObject * wxMiniObject = [WXMiniProgramObject object];
    wxMiniObject.webpageUrl = @"";// 兼容低版本的网页链接
    wxMiniObject.userName = @"gh_98eb10b27635"; // 小程序的原始id
    wxMiniObject.path = path;
    wxMiniObject.miniProgramType = WXMiniENVIRONMENT;
//    wxMiniObject.hdImageData = imageData; //小程序节点高清大图，小于128k
    
    WXMediaMessage * message = [WXMediaMessage message];
    message.title = title;
    message.mediaObject = wxMiniObject;
    message.thumbData = nil;
    /* 兼容旧版本节点的图片，小于32k，新版本优先
                              使用WXMiniProgramObject的hdImageData属性 */
    [message setThumbImage:[self compressImage:image toByte:32768]];
    SendMessageToWXReq * req = [[SendMessageToWXReq alloc] init];
    req.message = message;
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
}

#pragma mark - > 分享统计
- (void)requestShareFeedWithFeedID:(NSString *)feed_id WithType:(NSString *)type WithShareTo:(NSString *)share_to{
    
    FWFollowRequest * request = [[FWFollowRequest alloc] init];
    
    NSDictionary * params = @{
                              @"uid":[GFStaticData getObjectForKey:kTagUserKeyID],
                              type:feed_id,
                              @"share_to":share_to,
                              };
    
    [request startWithParameters:params WithAction:Submit_add_share_count WithDelegate:nil completeAction:^(NSURLSessionDataTask *task, NSString *code, id responseObject, NSError *error) {}];
}



#pragma mark - 压缩图片
- (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength {
    
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    if (data.length < maxLength) return image;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    UIImage *resultImage = [UIImage imageWithData:data];
    if (data.length < maxLength) return resultImage;
    
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
    }
    
    return resultImage;
}

@end
