//
//  ShareManager.h
//  StockMaster
//
//  Created by dalikeji on 15/2/9.
//  Copyright (c) 2015年 Rebloom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

//#import <TencentOpenAPI/QQApiInterfaceObject.h>
//#import <TencentOpenAPI/QQApiInterface.h>
//#import <TencentOpenAPI/TencentOAuth.h>

typedef enum SHAREQQ
{
    SHARETOQQ = 0,
    SHARETOQZONE ,
}_SHAREQQ;

@interface ShareManager : NSObject 

+ (ShareManager *)defaultShareManager;

/**
 * 微信分享（H5形式）
 */
- (void)sendWXWithModel:(FWFeedListModel *)model WithType:(int)type;

/**
 * 个人页、好友页,话题，分享到朋友圈（链接形式）
 */
- (void)sendWXWithUrl:(NSURL*)share_url WithTitle:(NSString*)share_title  WithDesc:(NSString*)share_desc WithType:(int)type WithPicURL:(NSString *)urlstring;

/**
 * 个人页、好友页分享到好友（小程序形式)
 */
- (void)userShareToMinProgramObjectWithModel:(FWMineModel *)model;

/**
 * 话题页分享到好友（小程序形式)
 */
- (void)topicShareToMinProgramObjectWithModel:(FWFeedModel *)model;

/**
 * 图文、视频详情 分享到好友（小程序形式）
 */
- (void)shareToMinProgramObjectWithModel:(FWFeedListModel *)model;

/**
 * 图文、视频详情 分享到朋友圈（图片带小程序）
 */
- (void)shareToPengyouquan:(UIView *)aimView WithFeedID:(NSString *)feed_id WithType:(NSString *)type;

@end
