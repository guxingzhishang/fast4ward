//
//  ColorFliterAuxiliary.h
//  ColorFilter
//
//  Created by tqh on 15/6/24.
//  Copyright (c) 2015年 tqh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColorFliterHead.h"
@interface ColorFliterAuxiliary : NSObject

/**
 *滤镜
 */
+(UIImage *)changeImage:(NSInteger)index imageView:(UIImage *)imageTp;

@end
