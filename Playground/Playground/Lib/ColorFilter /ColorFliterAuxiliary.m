//
//  ColorFliterAuxiliary.m
//  ColorFilter
//
//  Created by tqh on 15/6/24.
//  Copyright (c) 2015年 tqh. All rights reserved.
//

#import "ColorFliterAuxiliary.h"
#import "ImageUtil.h"
#import "ColorMatrix.h"
@implementation ColorFliterAuxiliary

#pragma mark--滤镜
+(UIImage *)changeImage:(NSInteger)index imageView:(UIImage *)imageTp
{
    UIImage *image;
    switch (index) {
        case 0:
        {
            return imageTp;//不用滤镜
        }
            break;
        case 1:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_lomo];
        }
            break;
        case 2:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_heibai];
        }
            break;
        case 3:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_huajiu];
        }
            break;
        case 4:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_gete];
        }
            break;
        case 5:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_ruise];
        }
            break;
        case 6:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_danya];
        }
            break;
        case 7:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_jiuhong];
        }
            break;
        case 8:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_qingning];
        }
            break;
        case 9:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_langman];
        }
            break;
        case 10:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_guangyun];
        }
            break;
        case 11:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_landiao];
            
        }
            break;
        case 12:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_menghuan];
            
        }
            break;
        case 13:
        {
            image = [ImageUtil imageWithImage:imageTp withColorMatrix:colormatrix_yese];
            
        }
    }
    return image;
}

@end
